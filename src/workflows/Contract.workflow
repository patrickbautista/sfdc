<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JVCO_PopulateContractCurrentStartDate</fullName>
        <field>JVCO_CurrentStartDate__c</field>
        <formula>SBQQ__Quote__r.JVCO_CurrentStartDate__c</formula>
        <name>Populate Contract Current Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_Contract_Start_Date</fullName>
        <description>Stamps the start date of the quote to the start date of the  contract</description>
        <field>StartDate</field>
        <formula>SBQQ__Quote__r.JVCO_MinStartDate__c</formula>
        <name>JVCO_Update_Contract_Start_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_Current_Days</fullName>
        <field>JVCO_Days_in_current_contract__c</field>
        <formula>IF(OR(ISBLANK(JVCO_TempStartDate__c),ISBLANK(JVCO_TempEndDate__c),JVCO_TempStartDate__c &gt; EndDate),0 ,
(IF(JVCO_TempEndDate__c &gt; EndDate , EndDate, JVCO_TempEndDate__c) - IF(JVCO_TempStartDate__c &lt; StartDate,StartDate,JVCO_TempStartDate__c))+1)
+
IF(OR(ISBLANK(JVCO_2nd_First_Day_of_Closure__c),ISBLANK(JVCO_2nd_Last_Day_of_Closure__c),JVCO_2nd_First_Day_of_Closure__c &gt; EndDate),0 ,
(IF(JVCO_2nd_Last_Day_of_Closure__c &gt; EndDate , EndDate, JVCO_2nd_Last_Day_of_Closure__c) - IF(JVCO_2nd_First_Day_of_Closure__c &lt; StartDate,StartDate,JVCO_2nd_First_Day_of_Closure__c))+1)
+
IF(OR(ISBLANK(JVCO_3rd_First_Day_of_Closure__c),ISBLANK(JVCO_3rd_Last_Day_of_Closure__c),JVCO_3rd_First_Day_of_Closure__c &gt; EndDate),0 ,
(IF(JVCO_3rd_Last_Day_of_Closure__c &gt; EndDate , EndDate, JVCO_3rd_Last_Day_of_Closure__c) - IF(JVCO_3rd_First_Day_of_Closure__c &lt; StartDate,StartDate,JVCO_3rd_First_Day_of_Closure__c))+1)
+
IF(OR(ISBLANK(JVCO_4th_First_Day_of_Closure__c),ISBLANK(JVCO_4th_Last_Day_of_Closure__c),JVCO_4th_First_Day_of_Closure__c &gt; EndDate),0 ,
(IF(JVCO_4th_Last_Day_of_Closure__c &gt; EndDate , EndDate, JVCO_4th_Last_Day_of_Closure__c) - IF(JVCO_4th_First_Day_of_Closure__c &lt; StartDate,StartDate,JVCO_4th_First_Day_of_Closure__c))+1)
+
IF(OR(ISBLANK(JVCO_5th_First_Day_of_Closure__c),ISBLANK(JVCO_5th_Last_Day_of_Closure__c),JVCO_5th_First_Day_of_Closure__c &gt; EndDate),0 ,
(IF(JVCO_5th_Last_Day_of_Closure__c &gt; EndDate , EndDate, JVCO_5th_Last_Day_of_Closure__c) - IF(JVCO_5th_First_Day_of_Closure__c &lt; StartDate,StartDate,JVCO_5th_First_Day_of_Closure__c))+1)
+
IF(OR(ISBLANK(JVCO_6th_First_Day_of_Closure__c),ISBLANK(JVCO_6th_Last_Day_of_Closure__c),JVCO_6th_First_Day_of_Closure__c &gt; EndDate),0 ,
(IF(JVCO_6th_Last_Day_of_Closure__c &gt; EndDate , EndDate, JVCO_6th_Last_Day_of_Closure__c) - IF(JVCO_6th_First_Day_of_Closure__c &lt; StartDate,StartDate,JVCO_6th_First_Day_of_Closure__c))+1)
+
IF(OR(ISBLANK(JVCO_7th_First_Day_of_Closure__c),ISBLANK(JVCO_7th_Last_Day_of_Closure__c),JVCO_7th_First_Day_of_Closure__c &gt; EndDate),0 ,
(IF(JVCO_7th_Last_Day_of_Closure__c &gt; EndDate , EndDate, JVCO_7th_Last_Day_of_Closure__c) - IF(JVCO_7th_First_Day_of_Closure__c &lt; StartDate,StartDate,JVCO_7th_First_Day_of_Closure__c))+1)
+
IF(OR(ISBLANK(JVCO_8th_First_Day_of_Closure__c),ISBLANK(JVCO_8th_Last_Day_of_Closure__c),JVCO_8th_First_Day_of_Closure__c &gt; EndDate),0 ,
(IF(JVCO_8th_Last_Day_of_Closure__c &gt; EndDate , EndDate, JVCO_8th_Last_Day_of_Closure__c) - IF(JVCO_8th_First_Day_of_Closure__c &lt; StartDate,StartDate,JVCO_8th_First_Day_of_Closure__c))+1)
+
IF(OR(ISBLANK(JVCO_9th_First_Day_of_Closure__c),ISBLANK(JVCO_9th_Last_Day_of_Closure__c),JVCO_9th_First_Day_of_Closure__c &gt; EndDate),0 ,
(IF(JVCO_9th_Last_Day_of_Closure__c &gt; EndDate , EndDate, JVCO_9th_Last_Day_of_Closure__c) - IF(JVCO_9th_First_Day_of_Closure__c &lt; StartDate,StartDate,JVCO_9th_First_Day_of_Closure__c))+1)
+
IF(OR(ISBLANK(JVCO_10th_First_Day_of_Closure__c),ISBLANK(JVCO_10th_Last_Day_of_Closure__c),JVCO_10th_First_Day_of_Closure__c &gt; EndDate),0 ,
(IF(JVCO_10th_Last_Day_of_Closure__c &gt; EndDate , EndDate, JVCO_10th_Last_Day_of_Closure__c) - IF(JVCO_10th_First_Day_of_Closure__c &lt; StartDate,StartDate,JVCO_10th_First_Day_of_Closure__c))+1)
+
IF(OR(ISBLANK(JVCO_11th_First_Day_of_Closure__c),ISBLANK(JVCO_11th_Last_Day_of_Closure__c),JVCO_11th_First_Day_of_Closure__c &gt; EndDate),0 ,
(IF(JVCO_11th_Last_Day_of_Closure__c &gt; EndDate , EndDate, JVCO_11th_Last_Day_of_Closure__c) - IF(JVCO_11th_First_Day_of_Closure__c &lt; StartDate,StartDate,JVCO_11th_First_Day_of_Closure__c))+1)
+
IF(OR(ISBLANK(JVCO_12th_First_Day_of_Closure__c),ISBLANK(JVCO_12th_Last_Day_of_Closure__c),JVCO_12th_First_Day_of_Closure__c &gt; EndDate),0 ,
(IF(JVCO_12th_Last_Day_of_Closure__c &gt; EndDate , EndDate, JVCO_12th_Last_Day_of_Closure__c) - IF(JVCO_12th_First_Day_of_Closure__c &lt; StartDate,StartDate,JVCO_12th_First_Day_of_Closure__c))+1)</formula>
        <name>Update Current Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_Days_in_Next_Contract</fullName>
        <field>JVCO_Days_in_Next_Contract__c</field>
        <formula>JVCO_Lockdown_Total_Days__c - JVCO_Days_in_current_contract__c</formula>
        <name>Update Days in Next Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JVCO Update number of days in lockdown</fullName>
        <actions>
            <name>JVCO_Update_Current_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Update_Days_in_Next_Contract</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(JVCO_TempStartDate__c)
||
ISCHANGED(JVCO_TempEndDate__c )
||
ISCHANGED(JVCO_2nd_First_Day_of_Closure__c )
||
ISCHANGED(JVCO_2nd_Last_Day_of_Closure__c )
||
ISCHANGED(JVCO_3rd_First_Day_of_Closure__c )
||
ISCHANGED(JVCO_3rd_Last_Day_of_Closure__c )
||
ISCHANGED(JVCO_4th_First_Day_of_Closure__c )
||
ISCHANGED(JVCO_4th_Last_Day_of_Closure__c )
||
ISCHANGED(JVCO_5th_First_Day_of_Closure__c) 
||
ISCHANGED(JVCO_5th_Last_Day_of_Closure__c )
||
ISCHANGED(JVCO_6th_First_Day_of_Closure__c )
||
ISCHANGED(JVCO_6th_Last_Day_of_Closure__c )
||
ISCHANGED(JVCO_7th_First_Day_of_Closure__c )
||
ISCHANGED(JVCO_7th_Last_Day_of_Closure__c )
||
ISCHANGED(JVCO_8th_First_Day_of_Closure__c )
||
ISCHANGED(JVCO_8th_Last_Day_of_Closure__c )
||
ISCHANGED(JVCO_9th_First_Day_of_Closure__c )
||
ISCHANGED(JVCO_9th_Last_Day_of_Closure__c )
||
ISCHANGED(JVCO_10th_First_Day_of_Closure__c )
||
ISCHANGED(JVCO_10th_Last_Day_of_Closure__c )
||
ISCHANGED(JVCO_11th_First_Day_of_Closure__c )
||
ISCHANGED(JVCO_11th_Last_Day_of_Closure__c )
||
ISCHANGED(JVCO_12th_First_Day_of_Closure__c )
||
ISCHANGED(JVCO_12th_Last_Day_of_Closure__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_PopulateCurrentStartDate</fullName>
        <actions>
            <name>JVCO_PopulateContractCurrentStartDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate Current Start Date From Quote</description>
        <formula>AND(NOT(ISBLANK(SBQQ__Quote__c)), NOT(ISBLANK(SBQQ__Quote__r.JVCO_CurrentStartDate__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Update_Contract_Start_Date</fullName>
        <actions>
            <name>JVCO_Update_Contract_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Stamps the start date of the quote to the start date of the contract</description>
        <formula>IF(ISNULL(SBQQ__Quote__c), false, IF(StartDate &lt;&gt;  SBQQ__Quote__r.Start_Date__c,true,false))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
