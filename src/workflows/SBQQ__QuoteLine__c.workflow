<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
	<fieldUpdates>
        <fullName>JVCO_EqualizeLGRAndLGRPrior</fullName>
        <field>LiveGrossReceiptsPrior__c</field>
        <formula>BoxOfficeReceipts__c</formula>
        <name>JVCO_EqualizeLGRAndLGRPrior</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_UncheckLGRFlag</fullName>
        <field>LiveGrossReceiptsIsChanged__c</field>
        <literalValue>0</literalValue>
        <name>JVCO_UncheckLGRFlag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_UpdateLiveGrossReceiptsPrior</fullName>
        <field>LiveGrossReceiptsPrior__c</field>
        <formula>PRIORVALUE(BoxOfficeReceipts__c)</formula>
        <name>JVCO_UpdateLiveGrossReceiptsPrior</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Set_Revenue_Type</fullName>
        <field>JVCO_Revenue_Type__c</field>
        <formula>IF(
   ISBLANK( Affiliation__c ),
   IF(
      ISPICKVAL(SBQQ__Quote__r.SBQQ__Type__c, &apos;Quote&apos;),
      &quot;New Business&quot;, 
      &quot;Review&quot;
   ),
   IF(
      ISPICKVAL(Affiliation__r.JVCO_Revenue_Type__c, &apos;Review&apos;), 
      if(
         JVCO_Upsell__c, 
         &quot;Upsell&quot;, 
         &quot;Review&quot;
      ), 
      TEXT(Affiliation__r.JVCO_Revenue_Type__c) 
   )
)</formula>
        <name>Set Revenue Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<rules>
        <fullName>JVCO_ProductLp1AndLp2</fullName>
        <actions>
            <name>JVCO_UpdateLiveGrossReceiptsPrior</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>SBQQ__QuoteLine__c.SBQQ__ProductCode__c</field>
            <operation>equals</operation>
            <value>LP1_PRS_1</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__QuoteLine__c.SBQQ__ProductCode__c</field>
            <operation>equals</operation>
            <value>LP2_PRS_1</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__QuoteLine__c.SBQQ__ProductCode__c</field>
            <operation>equals</operation>
            <value>LP2_PRS_2</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_ProductLp1AndLp2SameLGR</fullName>
        <actions>
            <name>JVCO_EqualizeLGRAndLGRPrior</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_UncheckLGRFlag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(SBQQ__ProductCode__c = &apos;LP1_PRS_1&apos;, SBQQ__ProductCode__c = &apos;LP2_PRS_1&apos;, SBQQ__ProductCode__c = &apos;LP2_PRS_2&apos;), LiveGrossReceiptsIsChanged__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Revenue Type</fullName>
        <actions>
            <name>JVCO_Set_Revenue_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(
   AND(
      ischanged(JVCO_Upsell__c), 
      (JVCO_Revenue_Type__c = &apos;Review&apos;)
   ),
    ISNEW() 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
