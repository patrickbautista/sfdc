<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JVCO_OrderItem_updateSurchargeApplicable</fullName>
        <field>JVCO_Surcharge_Applicable__c</field>
        <literalValue>1</literalValue>
        <name>OrderItem update Surcharge Applicable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JVCO_Update_OrderItem_Surcharge_Applicable</fullName>
        <actions>
            <name>JVCO_OrderItem_updateSurchargeApplicable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(TEXT(Product2.JVCO_Surcharge_Effective_Date__c) &lt;&gt; null, DATETIMEVALUE(Product2.JVCO_Surcharge_Effective_Date__c) &lt; DATETIMEVALUE(JVCO_Start_Date__c) ) &amp;&amp; !JVCO_Surcharge_Applicable__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
