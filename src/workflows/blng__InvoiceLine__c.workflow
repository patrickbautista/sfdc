<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>InvLine_Jitterbit_Extraction_Rule_Text</fullName>
        <field>Jitterbit_Extraction_Rule_Text__c</field>
        <formula>TEXT( Jitterbit_Extraction_Rule__c )</formula>
        <name>InvLine Jitterbit Extraction Rule Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_InvoiceLine_UpdateSurchargeAppli</fullName>
        <field>JVCO_Surcharge_Applicable__c</field>
        <literalValue>1</literalValue>
        <name>InvoiceLine Update Surcharge Applicable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Period_End_Date</fullName>
        <field>JVCO_Period_End_Date__c</field>
        <formula>IF(NOT(blng__Invoice__r.JVCO_Migrated__c),JVCO_End_Date__c,JVCO_Temp_End_Date__c)</formula>
        <name>Update Period End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Period_Start_Date</fullName>
        <field>JVCO_Period_Start_Date__c</field>
        <formula>IF(NOT(blng__Invoice__r.JVCO_Migrated__c),JVCO_Start_Date__c,JVCO_Temp_Start_Date__c)</formula>
        <name>Update Period Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>InvLine_Jitterbit_Extraction_Rule_Update</fullName>
        <actions>
            <name>InvLine_Jitterbit_Extraction_Rule_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the value of Jitterbit Extraction Rule Text whenever the Jitterbit Extraction Rule is updated.</description>
        <formula>ISCHANGED( Jitterbit_Extraction_Rule__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_InvoiceLine_Surcharge_Applicable</fullName>
        <actions>
            <name>JVCO_InvoiceLine_UpdateSurchargeAppli</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(TEXT(blng__Product__r.JVCO_Surcharge_Effective_Date__c) &lt;&gt; null, DATETIMEVALUE(blng__Product__r.JVCO_Surcharge_Effective_Date__c) &lt;   DATETIMEVALUE(JVCO_Start_Date__c) ) &amp;&amp; !JVCO_Surcharge_Applicable__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Invoice Line Period Date</fullName>
        <actions>
            <name>Update_Period_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Period_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(blng__OrderProduct__c  &lt;&gt; null, ISNULL(JVCO_Period_Start_Date__c), ISNULL(JVCO_Period_End_Date__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>