<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JournalLI_Jitterbit_Extraction_Rule_Text</fullName>
        <field>Jitterbit_Extraction_Rule_Text__c</field>
        <formula>TEXT( Jitterbit_Extraction_Rule__c )</formula>
        <name>JournalLI Jitterbit Extraction Rule Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JournalLineItem_Jitterbit_Extraction_Rule_Update</fullName>
        <actions>
            <name>JournalLI_Jitterbit_Extraction_Rule_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the value of Jitterbit Extraction Rule Text whenever the Jitterbit Extraction Rule is updated.</description>
        <formula>ISCHANGED( Jitterbit_Extraction_Rule__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
