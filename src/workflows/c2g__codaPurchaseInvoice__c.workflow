<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Payable_Invoice_Approved</fullName>
        <description>Payable Invoice Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@pplprs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Payable_Invoice_Approved</template>
    </alerts>
    <alerts>
        <fullName>Payable_Invoice_Rejected</fullName>
        <description>Payable Invoice Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@pplprs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Payable_Invoice_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>JVCO_Tick_Approved_Checkbox</fullName>
        <field>ffbext__Approved__c</field>
        <literalValue>1</literalValue>
        <name>Tick Approved Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
