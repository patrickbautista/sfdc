<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Closure_Reason_to_Duplicate_Venue</fullName>
        <field>JVCO_Closure_Reason__c</field>
        <literalValue>Duplicate Venue</literalValue>
        <name>Update Closure Reason to Duplicate Venue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>JVCO_Venue__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Closure_Reason_to_Venue_no_Exist</fullName>
        <field>JVCO_Closure_Reason__c</field>
        <literalValue>Venue no longer exists</literalValue>
        <name>Update Closure Reason to Venue no Exist</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>JVCO_Venue__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Close Venue when closing an Affiliation Reason Duplicate Venue</fullName>
        <actions>
            <name>Update_Closure_Reason_to_Duplicate_Venue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>and(     ispickval(JVCO_Closure_Reason__c, &apos;Duplicate Venue&apos;)    ,     ischanged(JVCO_Closure_Reason__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Close Venue when closing an Affiliation Reason Venue no Exist</fullName>
        <actions>
            <name>Update_Closure_Reason_to_Venue_no_Exist</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>and(     ispickval(JVCO_Closure_Reason__c, &apos;Venue no longer exists&apos;)    ,     ischanged(JVCO_Closure_Reason__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
