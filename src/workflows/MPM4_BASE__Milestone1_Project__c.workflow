<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>JVCO_Project_Update_Chaser</fullName>
        <description>Chase project update</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>X2_Automated_Emails/Project_Update_Chaser</template>
    </alerts>
</Workflow>
