<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Amendment_Quote_has_been_Approved</fullName>
        <description>Amendment Quote has been Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Credit_Note_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amendment_Quote_has_been_Rejected</fullName>
        <description>Amendment Quote has been Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Credit_Note_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Quote_Approved</fullName>
        <description>Amendment Quote has been Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Credit_Note_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Closed_Quote</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Close_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Closed Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Quote_Status_Approved</fullName>
        <field>SBQQ__Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Quote Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Quote_Status_Draft</fullName>
        <field>SBQQ__Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Quote Status Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>JVCO_Record_Type_to_Close_Quote</fullName>
        <description>Update Record Type To Read Only</description>
        <field>RecordTypeId</field>
        <lookupValue>Close_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to Close Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Record_Type_to_Open_Quote</fullName>
        <description>Update Record Type To Edit</description>
        <field>RecordTypeId</field>
        <lookupValue>Open_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to Open Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_SetQuotewithEditLines</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Open_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>JVCO_SetQuotewithEditLines</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Quote_Status_Rejected</fullName>
        <field>SBQQ__Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Quote Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_Accepts_TCs</fullName>
        <description>Update “Accepts T&amp;C’s”</description>
        <field>JVCO_Accept_TCs__c</field>
        <literalValue>1</literalValue>
        <name>JVCO Update “Accepts T&amp;C’s”</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sent_for_Approval_to_False</fullName>
        <description>Update checkbox to FALSE</description>
        <field>Sent_for_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Sent for Approval to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sent_for_Approval_to_True</fullName>
        <description>Update checkbox to TRUE</description>
        <field>Sent_for_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Sent for Approval to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_subscriptionTerm</fullName>
        <field>SBQQ__SubscriptionTerm__c</field>
        <formula>IF(ISPICKVAL(SBQQ__Account__r.JVCO_Review_Frequency__c,&apos;Monthly&apos;),
	1,
IF(ISPICKVAL(SBQQ__Account__r.JVCO_Review_Frequency__c,&apos;Quarterly&apos;),
	3,
IF(ISPICKVAL(SBQQ__Account__r.JVCO_Review_Frequency__c,&apos;Annually&apos;),
	12,
0)))</formula>
        <name>Set subscriptionTerm</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Change_Owner_to_Automated_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>JVCO_Automation_User</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Owner to Automated Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>JVCO_Lock_Quote</fullName>
        <field>JVCO_Locked__c</field>
        <literalValue>1</literalValue>
        <name>Lock Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Unlock_Quote</fullName>
        <field>JVCO_Locked__c</field>
        <literalValue>0</literalValue>
        <name>Unlock Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Open_Quote</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Close_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Open Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ContractOrderCreation_AfterApprov</fullName>
        <description>update field as &quot;Approved&quot;</description>
        <field>ContractOrderCreation_AfterApproval__c</field>
        <formula>&quot;Approved&quot;</formula>
        <name>Update_ContractOrderCreation_AfterApprov</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ContractOrderCreation_AfterReject</fullName>
        <description>Update field to &quot;Rejected&quot; after rejection</description>
        <field>ContractOrderCreation_AfterApproval__c</field>
        <formula>&quot;Rejected&quot;</formula>
        <name>Update_ContractOrderCreation_AfterReject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Invoice_Cancelled_Field</fullName>
        <field>JVCO_Cancelled__c</field>
        <literalValue>0</literalValue>
        <name>Update Invoice Cancelled Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>clear_ContractOrderApproval</fullName>
        <description>clears value in the ContractOrderCreation_AfterApproval__c upon submission of record for approval</description>
        <field>ContractOrderCreation_AfterApproval__c</field>
        <formula>&quot;&quot;</formula>
        <name>clear_ContractOrderApproval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JVCO Update %E2%80%9CAccepts T%26C%E2%80%99s%E2%80%9D</fullName>
        <actions>
            <name>JVCO_Update_Accepts_TCs</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Quote field “Accepts T&amp;C’s” is updated based on when the Customer Account &apos;T&amp;Cs Accepted&apos; = TRUE</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Quote_Set_Term</fullName>
        <actions>
            <name>Set_subscriptionTerm</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>NOT(ISPICKVAL(SBQQ__Account__r.JVCO_Review_Type__c, &apos;Do Not Renew&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_RecordTypeforQuoteStatusClosed</fullName>
        <actions>
            <name>Closed_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__Status__c</field>
            <operation>equals</operation>
            <value>Presented,Accepted,Rejected,Approved,Complete and Invoiced</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_RecordTypeforQuoteStatusDraftCancelled</fullName>
        <actions>
            <name>JVCO_SetQuotewithEditLines</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__Status__c</field>
            <operation>equals</operation>
            <value>Draft,Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Quote Owner</fullName>
        <actions>
            <name>JVCO_Change_Owner_to_Automated_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.OwnerId</field>
            <operation>equals</operation>
            <value>Automation User</value>
        </criteriaItems>
        <description>Change automation user to queue. This allows users to reassign quotes.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>