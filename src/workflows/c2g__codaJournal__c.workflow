<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Journal_Approval_Posted</fullName>
        <description>Journal Approval Posted</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Journal_Approval_Posted</template>
    </alerts>
    <alerts>
        <fullName>Journal_Approval_Rejected</fullName>
        <description>Journal Approval Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Journal_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Journal_Waiting_Approval</fullName>
        <description>Journal Waiting Approval</description>
        <protected>false</protected>
        <recipients>
            <field>JVCO_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Journal_Waiting_Approval</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Approved_Checkbox</fullName>
        <description>Change Approved Checkbox to true</description>
        <field>JVCO_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Update Approved Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Journal_setIsReconciledtoTrue</fullName>
        <description>Set the is Reconciled checkbox to TRUE once the journal record has been reversed.</description>
        <field>JVCO_Is_Reconciled__c</field>
        <literalValue>1</literalValue>
        <name>JVCO_Journal_setIsReconciledtoTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manager_Email</fullName>
        <field>JVCO_Manager_Email__c</field>
        <formula>Owner:User.Manager.Email</formula>
        <name>Update Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JVCO_Journal_CheckisReconciledonJournalReverse</fullName>
        <actions>
            <name>JVCO_Journal_setIsReconciledtoTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Set the isReconciled checkbox when the Journal header has been reversed.</description>
        <formula>!ISBLANK(c2g__SourceJournal__r.Id) 
&amp;&amp;
ISPICKVAL(c2g__Type__c, &quot;Reversing Journal&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
