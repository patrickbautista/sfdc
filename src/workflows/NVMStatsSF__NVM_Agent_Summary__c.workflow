<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Productive_Time_HHMM</fullName>
        <field>Productive_Time_HHMM__c</field>
        <formula>LPAD(TEXT(FLOOR(Productive_Time_Seconds__c/3600)), 2, &quot;0&quot;) + &quot;:&quot; +
LPAD(TEXT(ROUND(MOD(Productive_Time_Seconds__c, 3600)/60 , 0)), 2, &quot;0&quot;)</formula>
        <name>Update Productive Time HH:MM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Total_Extended_Away_Time</fullName>
        <description>Update Total Extended Away Time 
Extended_Away__c+Paperwork__c+Lunch__c+Training__c + In_Meeting__c+Team_Meeting__c+X1_1__c+Research__c</description>
        <field>Total_Extended_Away_Time__c</field>
        <formula>Extended_Away__c+Paperwork__c+Lunch__c+Training__c + In_Meeting__c+Team_Meeting__c+X1_1__c+Research__c</formula>
        <name>Update Total Extended Away Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unproductive_Time_HHMM</fullName>
        <field>Unproductive_Time_HH_MM__c</field>
        <formula>LPAD(TEXT(FLOOR(Unproductive_Time_Seconds__c/3600)), 2, &quot;0&quot;) + &quot;:&quot; +
LPAD(TEXT(ROUND(MOD(Unproductive_Time_Seconds__c, 3600)/60 , 0)), 2, &quot;0&quot;)</formula>
        <name>Update Unproductive Time HH:MM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Agent Summary - Productive%2FUnproductive Time</fullName>
        <actions>
            <name>Update_Productive_Time_HHMM</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Unproductive_Time_HHMM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>NVMStatsSF__NVM_Agent_Summary__c.NVMStatsSF__AgentID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update NVM Custom Fields</fullName>
        <actions>
            <name>Update_Total_Extended_Away_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>NVMStatsSF__NVM_Agent_Summary__c.NVMStatsSF__AgentID__c</field>
            <operation>greaterThan</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>NVMStatsSF__NVM_Agent_Summary__c.NVMStatsSF__AgentID__c</field>
            <operation>lessOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>Update Extended Away and Total State Time HH:MM Values</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>