<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JVCO_Bill_Now</fullName>
        <field>blng__BillNow__c</field>
        <literalValue>1</literalValue>
        <name>Bill Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Order_Status</fullName>
        <field>JVCO_Order_Status__c</field>
        <literalValue>Posted</literalValue>
        <name>Order Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>JVCO_Processed_by_Bill_Now</fullName>
        <field>JVCO_Processed_By_BillNow__c</field>
        <literalValue>1</literalValue>
        <name>Processed by Bill Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Date_Concatenation</fullName>
        <description>Concatenates Order Start Date and End Date</description>
        <field>JVCO_Concat_Order_Date__c</field>
        <formula>TEXT(DAY(JVCO_Licence_Start_Date__c))+&quot;/&quot;+TEXT(MONTH(JVCO_Licence_Start_Date__c))+&quot;/&quot;+TEXT(YEAR(JVCO_Licence_Start_Date__c))&amp;&quot; - &quot;&amp;TEXT(Day(JVCO_Licence_End_Date__c))+&quot;/&quot;+TEXT(MONTH(JVCO_Licence_End_Date__c))+&quot;/&quot;+TEXT(YEAR(JVCO_Licence_End_Date__c))</formula>
        <name>Order Date Concatenation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Activated</fullName>
        <field>Status</field>
        <literalValue>Activated</literalValue>
        <name>Set Status to Activated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>JVCO_Order_ActivateRecord</fullName>
        <actions>
            <name>Set_Status_to_Activated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.SBQQ__PriceCalcStatus__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>notEqual</operation>
            <value>Agency</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>notEqual</operation>
            <value>Key Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>notEqual</operation>
            <value>Jukebox Operator</value>
        </criteriaItems>
        <description>Activates Order record</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Order_CommunityInvoice</fullName>
        <actions>
            <name>JVCO_Order_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status) &amp;&amp; ISPICKVAL(Status, &apos;Activated&apos;) &amp;&amp; ISPICKVAL(CreatedBy.UserType, &apos;PowerPartner&apos;) &amp;&amp; NOT($Permission.JVCO_Do_Not_Auto_Bill_Now)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Start Date and End Date Concatenation</fullName>
        <actions>
            <name>Order_Date_Concatenation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.JVCO_Licence_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order.JVCO_Licence_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Concatenation of start and end dates</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>