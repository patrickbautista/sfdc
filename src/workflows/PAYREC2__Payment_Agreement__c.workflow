<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JVCO_UpdateVRBypass</fullName>
        <field>JVCO_BypassVR__c</field>
        <literalValue>0</literalValue>
        <name>JVCO_UpdateVRBypass</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JVCO_SetBypassVRDefaultValue</fullName>
        <actions>
            <name>JVCO_UpdateVRBypass</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PAYREC2__Payment_Agreement__c.JVCO_BypassVR__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
