<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Auto_Close_Customer_Service_Case_Email</fullName>
        <description>Auto Close Customer Service Case Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>customerservice@pplprs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Service_Auto_Close_Case</template>
    </alerts>
    <alerts>
        <fullName>Email_Collections</fullName>
        <description>Email Collections</description>
        <protected>false</protected>
        <recipients>
            <recipient>JVCO_Credit_Control_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/GDPR_Collections</template>
    </alerts>
    <alerts>
        <fullName>Email_Customer_Service</fullName>
        <description>Email Customer Service</description>
        <protected>false</protected>
        <recipients>
            <recipient>emma.asher@pplprs.co.uk.pplprs</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/GDPR_Customer_Services</template>
    </alerts>
    <alerts>
        <fullName>Email_Trading_Manager</fullName>
        <description>Email Trading Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>bethany.grundy@pplprs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/GDPR_Trading_Manager</template>
    </alerts>
    <alerts>
        <fullName>Email_Workplace_Tech</fullName>
        <description>Email Workplace Tech</description>
        <protected>false</protected>
        <recipients>
            <recipient>kevin.messenger@pplprs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/GDPR_Workplace_Tech</template>
    </alerts>
    <alerts>
        <fullName>GDPR_Data_Gathering</fullName>
        <description>GDPR Data Gathering</description>
        <protected>false</protected>
        <recipients>
            <recipient>GDRP_Data_Gathers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/GDPR_Sys_Admin_email</template>
    </alerts>
    <alerts>
        <fullName>JVCO_Case_RefundApproved</fullName>
        <description>JVCO_Case_RefundApproved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Request_for_Refund_Approved</template>
    </alerts>
    <alerts>
        <fullName>JVCO_Case_RefundRejected</fullName>
        <description>JVCO_Case_RefundRejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Request_for_Refund_Rejected</template>
    </alerts>
    <alerts>
        <fullName>JVCO_Case_Request_for_Refund_Approval</fullName>
        <description>JVCO_Case_Request for Refund Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>JVCO_Finance_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Request_for_Refund_Approval</template>
    </alerts>
    <alerts>
        <fullName>JVCO_Send_Rejection_write_off_case</fullName>
        <description>JVCO Send Rejection write off case</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/JVCO_Write_off_case_rejected</template>
    </alerts>
    <alerts>
        <fullName>JVCO_Write_Off_Approved</fullName>
        <description>JVCO Write Off Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/JVCO_Write_Off_Approved</template>
    </alerts>
    <alerts>
        <fullName>JVCO_Write_off_Case_Rejection_Email</fullName>
        <description>Write off Case Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X2_Automated_Emails/JVCO_Write_off_Case_Rejection_Email</template>
    </alerts>
    <alerts>
        <fullName>Security_Security_Case_Created</fullName>
        <description>Security - Security Case Created</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Security_Case_Created</template>
    </alerts>

    <alerts>
        <fullName>Send_Customer_Survey_on_Case_Closure</fullName>
        <description>Send Customer Survey on Case Closure</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>JVCO_Support_Email_Templates/Send_Customer_Survey</template>
    </alerts>
    <alerts>
        <fullName>System_Admin_Critical_Case_Notification</fullName>
        <description>System Admin - Critical Case Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Critical_Case_Created</template>
    </alerts>
    <alerts>
        <fullName>Ticketing_System_Send_Helpdesk_Request</fullName>
        <ccEmails>helpdesk@pplprs.co.uk</ccEmails>
        <ccEmails>HR@pplprs.co.uk</ccEmails>
        <ccEmails>LearningandDevelopment@pplprs.co.uk</ccEmails>
        <ccEmails>AccountsPayable@pplprs.co.uk</ccEmails>
        <description>Ticketing System - Send Helpdesk Request</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>X2_Automated_Emails/Helpdesk_Request</template>
    </alerts>
    <alerts>
        <fullName>Write_Off_Rejected</fullName>
        <description>Write Off Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/JVCO_Write_off_case_rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Auto_Close_Case_TRUE</fullName>
        <field>Auto_Close_Case__c</field>
        <literalValue>1</literalValue>
        <name>Case - Auto Close Case = TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Close_Case</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Case - Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Closure</fullName>
        <description>Case Status will be updated to closed if the criteria is met</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Case Closure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_Assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>JVCO_Finance</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Case - Update Status - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_CAB_In_Progress</fullName>
        <field>Status</field>
        <literalValue>CAB in Progress</literalValue>
        <name>Case - Update Status - CAB In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_IDEA_Rejected</fullName>
        <field>Status</field>
        <literalValue>Idea Rejected</literalValue>
        <name>Case - Update Status - IDEA Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_Impact_Assesment</fullName>
        <field>Status</field>
        <literalValue>Impact Assessment</literalValue>
        <name>Case - Update Status - Impact Assesment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Case - Update Status - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_Submit_Approval</fullName>
        <field>Status</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Case - Update Status - Submit Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_Triage</fullName>
        <field>Status</field>
        <literalValue>Triage</literalValue>
        <name>Case - Update Status - Triage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GDPR_Case_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>JVCO_Complaints</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>GDPR Case Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GDPR_Case_Status</fullName>
        <field>Status</field>
        <literalValue>Legal Review</literalValue>
        <name>GDPR Case Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GDPR_Update_Due_Date</fullName>
        <description>Update the Due Date on a GDPR case.</description>
        <field>Due_Date__c</field>
        <formula>GDPR_Date_of_Request__c + 30</formula>
        <name>GDPR Update Due Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Assign_Case_to_Collections</fullName>
        <field>OwnerId</field>
        <lookupValue>JVCO_Collections</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Case to Collections</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_AssignCasetoFinance</fullName>
        <field>OwnerId</field>
        <lookupValue>JVCO_Finance</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>JVCO_Case_AssignCasetoFinance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_Owner_to_Finance</fullName>
        <description>Assign Case Owner to Finance</description>
        <field>OwnerId</field>
        <lookupValue>JVCO_Finance</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner to Finance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_RefundApproved</fullName>
        <field>Status</field>
        <literalValue>Refund Approved</literalValue>
        <name>JVCO_Case_RefundApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_RefundRejected</fullName>
        <field>Status</field>
        <literalValue>Refund Rejected</literalValue>
        <name>JVCO_Case_RefundRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_SetCaseOwnertoCollections</fullName>
        <field>OwnerId</field>
        <lookupValue>JVCO_Collections</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>JVCO_Case_SetCaseOwnertoCollections</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_SetCaseResponsibility</fullName>
        <field>JVCO_Case_Responsibility__c</field>
        <literalValue>Parent Legal</literalValue>
        <name>JVCO_Case_SetCaseResponsibility</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_SetCaseResponsibilityToSolicit</fullName>
        <description>Sets the case responsibility to Solicitor when the case is saved with the status Passed to Solicitor</description>
        <field>JVCO_Case_Responsibility__c</field>
        <literalValue>Solicitor</literalValue>
        <name>JVCO_Case_SetCaseResponsibilityToSolicit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_SetOwnerToQueue</fullName>
        <description>Set Case Owner to Internal Enforcement queue when Status is &apos;Returned to Client&apos; and &apos;Assigned Enforcement Advisor&apos; is null</description>
        <field>OwnerId</field>
        <lookupValue>JVCO_Internal_Enforcement</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>JVCO_Case_SetOwnerToQueue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_SetPriorityPicklistToReferred</fullName>
        <field>Priority</field>
        <literalValue>Referred to Parent</literalValue>
        <name>JVCO_Case_SetPriorityPicklistToReferred</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_SetResponsibilityToEnforcement</fullName>
        <field>JVCO_Case_Responsibility__c</field>
        <literalValue>Enforcement</literalValue>
        <name>JVCO_Case_SetResponsibilityToEnforcement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_SetSolicitorDateToToday</fullName>
        <field>JVCO_Date_Referred_to_Solicitor__c</field>
        <formula>TODAY()</formula>
        <name>JVCO_Case_SetSolicitorDateToToday</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_SetStatusToInProgress</fullName>
        <description>Set Case Status field to In Progress from New</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Set Status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_SetSubjectToAcctAndInfReason</fullName>
        <field>Subject</field>
        <formula>Account.Name + &apos; - &apos; +  TEXT(JVCO_Infringement_Reason__c)</formula>
        <name>JVCO_Case_SetSubjectToAcctAndInfReason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_UpdateCaseStatus</fullName>
        <description>Update Case Status to &quot;Refund Requested&quot; once a record has been submitted for approval.</description>
        <field>Status</field>
        <literalValue>Refund Requested</literalValue>
        <name>JVCO_Case_UpdateCaseStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>JVCO_Update_Case_Reason</fullName>
        <field>JVCO_Case_Reason_Level_2__c</field>
        <literalValue>Standard - Customer Service</literalValue>
        <name>Update Case Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_Case_Type_To_Complaint</fullName>
        <description>An email-to-case functionality for Complaints</description>
        <field>Type</field>
        <literalValue>Complaint</literalValue>
        <name>Update Case Type To Complain</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_UpdateOriginToRequestCallback</fullName>
        <description>Updates the Case Origin to Request Callback</description>
        <field>Origin</field>
        <literalValue>Request Callback</literalValue>
        <name>JVCO Case UpdateOriginToRequestCallback</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Case_UpdateReferredToSolicitorDate</fullName>
        <field>JVCO_Date_Referred_to_Solicitor__c</field>
        <name>JVCO_Case_UpdateReferredToSolicitorDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Field_Intial_Enforcement</fullName>
        <field>OwnerId</field>
        <lookupValue>JVCO_Internal_Enforcement</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Field_Update_Finance</fullName>
        <field>OwnerId</field>
        <lookupValue>JVCO_Finance_Team_Leader</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_Follow_up_Task_Created</fullName>
        <description>This will update the Follow up Task Created field checkbox that will serve as indication that a follow-up task has been created.</description>
        <field>JVCO_Followup_Task_Created__c</field>
        <literalValue>1</literalValue>
        <name>Update Follow-up Task Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_to_Head_of_Finance</fullName>
        <field>OwnerId</field>
        <lookupValue>JVCO_Head_of_Finance</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>JVCO Update to Head of Finance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_to_Internal_Enforcement</fullName>
        <field>OwnerId</field>
        <lookupValue>JVCO_Internal_Enforcement</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>JVCO Update to Internal Enforcement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Write_off_Rejected</fullName>
        <field>JVCO_Write_Off_Status__c</field>
        <literalValue>Write Off Rejected</literalValue>
        <name>Write off Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejection</fullName>
        <field>OwnerId</field>
        <lookupValue>JVCO_Internal_Enforcement</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetOwnerToEnforcementQueue</fullName>
        <field>OwnerId</field>
        <lookupValue>JVCO_Internal_Enforcement</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetOwnerToEnforcementQueue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetOwnertoFinanceQueue</fullName>
        <description>Set Case Owner to Finance Queue</description>
        <field>OwnerId</field>
        <lookupValue>JVCO_Finance</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetOwnertoFinanceQueue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status</fullName>
        <field>Status</field>
        <literalValue>Passed to Finance</literalValue>
        <name>Update Case Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Field_to_Finance</fullName>
        <field>OwnerId</field>
        <lookupValue>JVCO_Finance</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Legal_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
     <fieldUpdates>
        <fullName>Update_Status_In_Progress</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Update Status (In progress)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Responded</fullName>
        <field>Status</field>
        <literalValue>Responded</literalValue>
        <name>Update Status - Responded</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
     </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_New</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Update Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Submitted_for_Approval</fullName>
        <field>Status</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Update Status to Submitted for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status</fullName>
        <field>Status</field>
        <literalValue>Legal Review</literalValue>
        <name>Update status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Write_off_Request</fullName>
        <field>JVCO_Write_Off_Status__c</field>
        <literalValue>Write Off Requested</literalValue>
        <name>Update Write off Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Write_Off_Status_Approved</fullName>
        <field>JVCO_Write_Off_Status__c</field>
        <literalValue>Write Off Approved</literalValue>
        <name>Update Write Off Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Write_Off_Status_Rejected</fullName>
        <field>JVCO_Write_Off_Status__c</field>
        <literalValue>Write Off Rejected</literalValue>
        <name>Update Write Off Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Write_off_Approval</fullName>
        <field>JVCO_Write_Off_Status__c</field>
        <literalValue>Write Off Approved</literalValue>
        <name>Write off Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Auto-Close DCA Case</fullName>
        <actions>
            <name>Case_Closure</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Case.JVCO_DCA_Status__c</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.JVCO_Status_Reason_Code__c</field>
            <operation>equals</operation>
            <value>380 - PROMISE TO PAY,390 - DIRECT DEBIT SETUP,430 - PAYMENT PLAN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.JVCO_DCA_Status__c</field>
            <operation>equals</operation>
            <value>Withdrawal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.JVCO_Status_Reason_Code__c</field>
            <operation>equals</operation>
            <value>100 - PAID IN FULL,110 - UNECONOMICAL TO PURSUE,140 - UNWILLING TO PAY,180 - WORK EXHAUSTED,210 - UNABLE TO CONTACT,230 - ACCOUNT CLOSED BY JVCO,240 - END OF PLACEMENT</value>
        </criteriaItems>
        <description>DCA cases will be automatically closed if the criteria is met.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Auto Close Case with No Activity</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Service,Covid Amendments</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>PPL PRS Responded</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>notEqual</operation>
            <value>Complaint</value>
        </criteriaItems>
        <description>Workflow queues a time dependent action to trigger if case remains in status of &apos;PPL Responded&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Auto_Close_Customer_Service_Case_Email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Case_Auto_Close_Case_TRUE</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_Close_Case</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>GDPR Actions Complete</fullName>
        <actions>
            <name>GDPR_Case_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.GDPR_Collections_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GDPR_CS_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GDPR_SysAdmin_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GDPR_Trading_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GDPR_Workplace_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Check when all actions are complete on a GDPR Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GDPR Set Due Date</fullName>
        <actions>
            <name>GDPR_Update_Due_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>GDPR</value>
        </criteriaItems>
        <description>Set the due date on GDPR Requests</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Case_AssignRefundCasetoCollections</fullName>
        <actions>
            <name>JVCO_Case_SetCaseOwnertoCollections</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Refund</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Refund Approved</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Case_AssignRefundCasetoFinance</fullName>
        <actions>
            <name>JVCO_Case_Owner_to_Finance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Refund</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Refund Approved</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Case_CreateInfringementFollowUpTask</fullName>
        <actions>
            <name>JVCO_Update_Follow_up_Task_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_Up_On_Infringement_Case_Progress</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Passed to Solicitors</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.JVCO_Followup_Task_Created__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Creates a follow-up task for the case creator to check on progress of the case two weeks after the case is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Case_DCAQueryOwner</fullName>
        <actions>
            <name>JVCO_Case_SetCaseOwnertoCollections</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populate the case owner to the collections queue.</description>
        <formula>!ISBLANK(Id)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Case_SendCustomerSurvey</fullName>
        <actions>
            <name>Send_Customer_Survey_on_Case_Closure</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.JVCO_Send_Customer_Survey__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Send customer survey on case close whenever the Send Customer Survey checkbox is checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Case_SetInfringementCasePriorityAndCaseResponsibility</fullName>
        <actions>
            <name>JVCO_Case_SetCaseResponsibility</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Case_SetPriorityPicklistToReferred</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.JVCO_Referred_to_Parent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Enforcement - Infringement</value>
        </criteriaItems>
        <description>Used to set the Priority picklist field to Referred to Parent and the Case Responsibility to Parent Legal on cases with the Enforcement - Infringement record type based on the Referred to Parent checkbox being true.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Case_SetOriginToCallback</fullName>
        <actions>
            <name>JVCO_Case_UpdateOriginToRequestCallback</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.JVCO_Request_Callback__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sets the Case Origin of the record when Request Callback checkbox is ticked</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Case_SetOwnerToInternalEnforcementCasesQueue</fullName>
        <actions>
            <name>JVCO_Case_SetOwnerToQueue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Case_SetResponsibilityToEnforcement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Returned to Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.JVCO_Assigned_Enforcement_Advisor__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Used to set the Case Owner to the Internal Enforcement Advisor/Team and the Case Responsibility to Enforcement when the case is saved with the status Returned to Client.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Case_SetReferredToSolicitorDate</fullName>
        <actions>
            <name>JVCO_Case_SetCaseResponsibilityToSolicit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Case_SetSolicitorDateToToday</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.JVCO_Date_Referred_to_Solicitor__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Passed to Solicitors</value>
        </criteriaItems>
        <description>Used to set the Referred to Solicitor Date as today when the case is saved with the status Passed to Solicitor</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Case_SetReferredToSolicitorDateBlank</fullName>
        <actions>
            <name>JVCO_Case_UpdateReferredToSolicitorDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Preparing Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.JVCO_Date_Referred_to_Solicitor__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Used to set the Referred to Solicitor Date as today and the Case Responsibility to Solicitor when the case is saved with the status Passed to Solicitor</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Case_SetStatusToInProgress</fullName>
        <actions>
            <name>JVCO_Case_SetStatusToInProgress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISCHANGED(OwnerId), Text(Status) = &apos;New&apos;,  OR( OwnerId = $User.Id, AND( LEFT(PRIORVALUE(OwnerId),3) = &apos;00G&apos;, LEFT(OwnerId,3) = &apos;005&apos; ) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	<rules>
        <fullName>JVCO_Case_UpdateCaseTypeToComplaint</fullName>
        <actions>
            <name>JVCO_Update_Case_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Update_Case_Type_To_Complaint</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>startsWith</operation>
            <value>Complaints</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Remittance Case Assignment</fullName>
        <actions>
            <name>Case_Owner_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Remittance</value>
        </criteriaItems>
        <description>If the case is raised for Remittance slips or advice , the case needs to be assigned to Finance team.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Security - Security Case Created</fullName>
        <actions>
            <name>Security_Security_Case_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Security</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>System Admin - Critical Case</fullName>
        <actions>
            <name>System_Admin_Critical_Case_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>System Administration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Critical</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Technology Team</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Follow_Up_On_Infringement_Case_Progress</fullName>
        <assignedTo>JVCO_Enforcement_Advisor</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Monitor the progress of the infringement case preparation</description>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Follow Up On Infringement Case Progress</subject>
    </tasks>
</Workflow>