<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JVCO_Campaign_Update_Campaign_Code</fullName>
        <description>Campaign Code is created and updated that concatenates Campaign Type / Start Date / Channel / Venue Type</description>
        <field>JVCO_Campaign_Code__c</field>
        <formula>UPPER(IF( TEXT( Type ) = &quot;New Business&quot;, &quot;NB&quot;, MID( TEXT( Type ) , 1, 2)) &amp; &quot;-&quot; &amp; 
CASE( MONTH(  StartDate ) , 
1, &quot;JAN&quot;, 
2, &quot;FEB&quot;, 
3, &quot;MAR&quot;, 
4, &quot;APR&quot;, 
5, &quot;MAY&quot;, 
6, &quot;JUN&quot;, 
7, &quot;JUL&quot;, 
8, &quot;AUG&quot;, 
9, &quot;SEP&quot;, 
10, &quot;OCT&quot;, 
11, &quot;NOV&quot;, 
&quot;DEC&quot;) &amp; RIGHT(TEXT( YEAR( StartDate ) ), 2) &amp; &quot;-&quot; &amp; 
MID( TEXT( JVCO_Campaign_Channel__c ) , 1, 1) &amp; &quot;-&quot; &amp;
IF(TEXT(Type) != &quot;Churn&quot; &amp;&amp; TEXT(Type) !=&quot;Review&quot;,
MID( TEXT( JVCO_Campaign_Category__c), 1, 4) &amp; &quot;-&quot;, &quot;&quot;) 
&amp; 
JVCO_Campaign_Auto_Number__c)</formula>
        <name>Campaign: Update Campaign Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JVCO_Campaign_Update Campaign Code</fullName>
        <actions>
            <name>JVCO_Campaign_Update_Campaign_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the campaign code of the record when a specific field is edited</description>
        <formula>IF(ISNEW(), TRUE,ISCHANGED( Type ) ||  ISCHANGED( StartDate ) ||  ISCHANGED( JVCO_Campaign_Channel__c )  || ISCHANGED(JVCO_Campaign_Category__c) ||  ISCHANGED( JVCO_Campaign_Auto_Number__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
