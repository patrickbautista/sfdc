<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JVCO_UpdateArtistInternalID</fullName>
        <description>Updates internal ID to artist number.</description>
        <field>JVCO_Internal_Id__c</field>
        <formula>JVCO_Artist_Number__c</formula>
        <name>JVCO_UpdateArtistInternalID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JVCO_UpdateArtistInternalID</fullName>
        <actions>
            <name>JVCO_UpdateArtistInternalID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>JVCO_Artist__c.JVCO_Internal_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
