<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>JVCO_Approved_Payment</fullName>
        <description>Approved Payment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ffxp__ffdc5_emailTemplates/JVCO_Approved_Payment</template>
    </alerts>
    <alerts>
        <fullName>JVCO_Payment_Approval</fullName>
        <description>Approved Payment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ffxp__ffdc5_emailTemplates/JVCO_Approved_Payment</template>
    </alerts>
    <alerts>
        <fullName>JVCO_Rejected_Payment</fullName>
        <description>Rejected Payment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ffxp__ffdc5_emailTemplates/JVCO_Rejected_Payment</template>
    </alerts>
    <fieldUpdates>
        <fullName>JVCO_Status_Approved</fullName>
        <field>JVCO_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>JVCO_Status_Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>