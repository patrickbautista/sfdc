<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JVCO_UpdateVenueInternalID</fullName>
        <field>JVCO_Internal_Id__c</field>
        <formula>JVCO_Venue_Auto_Number__c</formula>
        <name>JVCO_UpdateVenueInternalID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JVCO_UpdateVenueInternalID</fullName>
        <actions>
            <name>JVCO_UpdateVenueInternalID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>JVCO_Venue__c.JVCO_Internal_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update internal id to venue auto number when a record is created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
