<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JVCO_SetRecordType_BankTransfer</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Bank_Transfer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type - Bank Transfer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_SetRecordType_NoBankTransfer</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Non_Bank_Transfer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type - No Bank Transfer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bank_Transfer_To_False</fullName>
        <field>JVCO_Bank_Transfer__c</field>
        <literalValue>0</literalValue>
        <name>Update Bank Transfer To False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JVCO_SetRecordType_BankTransferAllowed</fullName>
        <actions>
            <name>JVCO_SetRecordType_BankTransfer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>c2g__BankAccount__r.JVCO_BankTransferAllowed__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_SetRecordType_BankTransferNotAllowed</fullName>
        <actions>
            <name>JVCO_SetRecordType_NoBankTransfer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>!c2g__BankAccount__r.JVCO_BankTransferAllowed__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Turn Off Bank Transfer</fullName>
        <actions>
            <name>Update_Bank_Transfer_To_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>c2g__codaBankStatement__c.JVCO_Bank_Transfer__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
