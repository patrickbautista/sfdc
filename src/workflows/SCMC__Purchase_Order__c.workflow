<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SendApproved</fullName>
        <description>Send Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SCM_Custom/PO_Approved</template>
    </alerts>
    <alerts>
        <fullName>SendRejected</fullName>
        <description>Send Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SCM_Custom/PO_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>SetAppSteTo0</fullName>
        <field>FFCONS_Approval_Step__c</field>
        <formula>0</formula>
        <name>Set Approval Step to 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetApprovalStep</fullName>
        <field>FFCONS_Approval_Step__c</field>
        <formula>FFCONS_Approval_Step__c + 1</formula>
        <name>Set Approval Step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Open</fullName>
        <field>SCMC__Status__c</field>
        <literalValue>Open</literalValue>
        <name>Set Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Date</fullName>
        <field>FFSCM_ApprovalDate__c</field>
        <formula>TODAY()</formula>
        <name>Update Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
