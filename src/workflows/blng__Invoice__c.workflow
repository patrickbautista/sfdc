<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JVCO_Update_Migrated_Credit_Note</fullName>
        <field>JVCO_Migrated_Credit_Note__c</field>
        <literalValue>1</literalValue>
        <name>Update Migrated Credit Note</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JVCO Update Migrated Credit Note</fullName>
        <actions>
            <name>JVCO_Update_Migrated_Credit_Note</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>JVCO_Sales_Credit_Note__r.JVCO_Migrated_Credit_Note__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
