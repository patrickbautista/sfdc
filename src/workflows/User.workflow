<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Clear_DDI</fullName>
        <description>Clear NVM DDI field.</description>
        <field>NVM_DDI__c</field>
        <name>Clear DDI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NVM_Group</fullName>
        <description>Update NVM Group when Department is populated.</description>
        <field>NVM_Group__c</field>
        <formula>IF ( CONTAINS (Department, &quot;Key Accounts&quot;), &quot;Key Accounts&quot;, 

  IF ( CONTAINS (Department, &quot;New Business - Corporate&quot;),
  &quot;New Business&quot;,

    IF (CONTAINS (Department, &quot;Live&quot;), &quot;Live Music&quot;,&quot;&quot;)))</formula>
        <name>Update NVM Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>User_Deactivation</fullName>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>User Deactivation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Band Member Leaving</fullName>
        <active>true</active>
        <criteriaItems>
            <field>User.Leaving_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Clear_DDI</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>User_Deactivation</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>User.Leaving_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>User - Populate NVM Group</fullName>
        <actions>
            <name>Update_NVM_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Department</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Populate NVM Group for departments Key, Live or New Business.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
