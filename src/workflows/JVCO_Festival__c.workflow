<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Text_Start_Date</fullName>
        <field>JVCO_StartDate_Text__c</field>
        <formula>TEXT( DAY( JVCO_Start_Date__c ))+&apos;/&apos;+ TEXT( MONTH( JVCO_Start_Date__c )) + &apos;/&apos; + TEXT( YEAR( JVCO_Start_Date__c ))</formula>
        <name>Populate Text Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Text Start Date</fullName>
        <actions>
            <name>Populate_Text_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
