<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JVCO_Set_Classified_Date</fullName>
        <field>JVCO_Classified_Date__c</field>
        <formula>IF( TEXT( PRIORVALUE( JVCO_Event_Classification_Status__c ) )   = &quot;Requested&quot;, TODAY(),  JVCO_Classified_Date__c )</formula>
        <name>Set Classified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Set_Requested_Date</fullName>
        <field>JVCO_Requested_Date__c</field>
        <formula>IF( OR(ISNEW(), ISCHANGED(  JVCO_Event_Classification_Status__c  )), TODAY(),  JVCO_Requested_Date__c )</formula>
        <name>Set Requested Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Event Set to Classified</fullName>
        <actions>
            <name>JVCO_Set_Classified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>JVCO_Event__c.JVCO_Event_Classification_Status__c</field>
            <operation>equals</operation>
            <value>Classified</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Event Set to Requested</fullName>
        <actions>
            <name>JVCO_Set_Requested_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>JVCO_Event__c.JVCO_Event_Classification_Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
