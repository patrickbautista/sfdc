<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_No_Auddis_Field_to_False</fullName>
        <description>Sets No_AUDDIS__C to false if a New Instruction has been submitted.</description>
        <field>No_AUDDIS__c</field>
        <literalValue>0</literalValue>
        <name>Update No_Auddis Field to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Income_Direct_Debit__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Update No_Auddis</fullName>
        <actions>
            <name>Update_No_Auddis_Field_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income_Debit_History__c.DD_Status__c</field>
            <operation>equals</operation>
            <value>New Instruction</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income_Debit_History__c.DD_Stage__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <description>Updates No_Auddis to true when a New Instruction has been successfully submitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
