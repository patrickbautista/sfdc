<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>JVCO_Email_Alert_Rejection</fullName>
        <description>Email Alert Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ffxp__ffdc5_emailTemplates/JVCO_Email_Rejection_to_Account_Owner</template>
    </alerts>
    <alerts>
        <fullName>JVCO_Email_Alert_to_Account_Owner</fullName>
        <description>Email Alert to Account Owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ffxp__ffdc5_emailTemplates/JVCO_Email_alert_to_Account_Owner</template>
    </alerts>
    <alerts>
        <fullName>JVCO_Supplier_Submission</fullName>
        <ccEmails>luke.walker@pplprs.co.uk</ccEmails>
        <description>Supplier has been submitted for Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>marie.wilsonknight@pplprs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tim.keep@pplprs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/JVCO_SupplierRequest</template>
    </alerts>
    <alerts>
        <fullName>Requested_for_Account_Termination_Rejected</fullName>
        <description>Requested for Account Termination - Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Termination_of_Customer_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Termination_of_Customer_Account_Approved</fullName>
        <description>Termination of Customer Account - Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Termination_of_Customer</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Dunning_Strategy_Blank</fullName>
        <field>Dunning_Strategy__c</field>
        <name>Account - Dunning Strategy (Blank)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Dunning_Strategy_Business</fullName>
        <field>Dunning_Strategy__c</field>
        <literalValue>Business</literalValue>
        <name>Account - Dunning Strategy (Business)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Dunning_Strategy_Corp</fullName>
        <field>Dunning_Strategy__c</field>
        <literalValue>Corporate</literalValue>
        <name>Account - Dunning Strategy (Corp)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Exclude_Reason_CA_Dunning</fullName>
        <field>JVCO_Exclude_Reason__c</field>
        <literalValue>Customer Account Dunning</literalValue>
        <name>Account - Exclude Reason - CA Dunning</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Lock_Dunning_FALSE</fullName>
        <field>Lock_Dunning_Strategy__c</field>
        <literalValue>0</literalValue>
        <name>Account - Lock Dunning (FALSE)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DocGen_PPL_Gap_Invoice_value</fullName>
        <field>JVCO_DocGen_PPL_Gap_Invoice__c</field>
        <formula>If(JVCO_DocGen_PPL_Gap_Invoice__c = null,1,1)</formula>
        <name>DocGen PPL Gap Invoice value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_AccNameConcat</fullName>
        <field>Name</field>
        <formula>Licence_Account_Number__c + &apos;-&apos; + JVCO_Customer_Account__r.Name +  IF(NOT(ISBLANK(JVCO_Account_Description__c)),
 &apos;-&apos; + JVCO_Account_Description__c, &apos;&apos;)</formula>
        <name>JVCO_AccNameConcat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_AccNumConcat</fullName>
        <field>AccountNumber</field>
        <formula>RIGHT( Licence_Account_Number__c , 8)</formula>
        <name>JVCO_AccNumConcat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_Balance_DCA_2nd_Stage</fullName>
        <field>JVCO_Account_Balance_Passed_DCA_2ndStage__c</field>
        <name>Account Balance DCA 2nd Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_ExcludeFromDunning</fullName>
        <field>ffps_custRem__Exclude_From_Reminder_Process__c</field>
        <literalValue>1</literalValue>
        <name>JVCO_Account_ExcludeFromDunning</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_ExcludeFromDunningReasBlank</fullName>
        <field>JVCO_Exclude_Reason__c</field>
        <name>JVCO_Account_ExcludeFromDunningReasBlank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_PopulateAcctBalSentToDCA</fullName>
        <description>Populate field : Account Balance Sent to DCA whenever Passed to DCA Date = Today</description>
        <field>JVCO_AccountBalance_DCA__c</field>
        <formula>if(DATEVALUE(JVCO_Passed_to_DCA_date__c) = TODAY(), ffps_accbal__Account_Balance__c, null)</formula>
        <name>JVCO_Account_PopulateAcctBalSentToDCA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_RevertAcctBalToZero</fullName>
        <field>JVCO_AccountBalance_DCA__c</field>
        <name>JVCO_Account_RevertAcctBalToZero</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_SetCreditStatustoNotinDebt</fullName>
        <field>JVCO_Credit_Status__c</field>
        <literalValue>Not in Debt</literalValue>
        <name>JVCO_Account_SetCreditStatustoNotinDebt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_SetCreditStatustoPreEnforc</fullName>
        <field>JVCO_Credit_Status__c</field>
        <literalValue>Pre-Enforcement</literalValue>
        <name>JVCO_Account_SetCreditStatustoPreEnforc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_SetPassedToDCADatetoBlank</fullName>
        <field>JVCO_Passed_to_DCA_date__c</field>
        <name>JVCO_Account_SetPassedToDCADatetoBlank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_SetReviewType</fullName>
        <field>JVCO_Review_Type__c</field>
        <literalValue>Do not renew</literalValue>
        <name>JVCO_Account_SetReviewType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_StatusReasonSetasKeyAccount</fullName>
        <field>JVCO_Exclude_Reason__c</field>
        <literalValue>Key Account</literalValue>
        <name>JVCO_Account_StatusReasonSetasKeyAccount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_UpdateCreditStatus</fullName>
        <field>JVCO_Credit_Status__c</field>
        <literalValue>Passed to DCA</literalValue>
        <name>JVCO_Account_UpdateCreditStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_UpdateIncomingStatusDate</fullName>
        <field>JVCO_Incoming_Status_Change_Date__c</field>
        <formula>TODAY()</formula>
        <name>JVCO_Account_UpdateIncomingStatusDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_UpdateIncomingStatusDate2nd</fullName>
        <description>Update for 2nd Stage</description>
        <field>JVCO_Incoming_Status_Change_Date_DCA_2nd__c</field>
        <formula>TODAY()</formula>
        <name>JVCO_Account_UpdateIncomingStatusDate2nd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_UpdateSeverityBlockedDate</fullName>
        <field>ffps_custRem__Reminders_Blocked_Until_Date__c</field>
        <formula>JVCO_Promise_to_Pay__c + 1</formula>
        <name>JVCO_Account_UpdateSeverityBlockedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Account_UpdateStatusChangeDate</fullName>
        <field>JVCO_Status_Change_Date__c</field>
        <formula>TODAY()</formula>
        <name>JVCO_Account_UpdateStatusChangeDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_CreditStatusNotinDebt</fullName>
        <field>JVCO_Credit_Status__c</field>
        <literalValue>Not in Debt</literalValue>
        <name>JVCO_CreditStatusNotinDebt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
     <fieldUpdates>
        <fullName>JVCO_Account_ExcludeFromDunningFalse</fullName>
        <field>ffps_custRem__Exclude_From_Reminder_Process__c</field>
        <literalValue>0</literalValue>
        <name>JVCO_Account_ExcludeFromDunningFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_CreditStatus_InEnforcement</fullName>
        <description>If the Licence Account is In Enforcement, Credit Status is automatically changed to Enforcement - Debt Recovery</description>
        <field>JVCO_Credit_Status__c</field>
        <literalValue>Enforcement - Debt Recovery</literalValue>
        <name>CreditStatus_InEnforcement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_CreditStatus_InInfringmen</fullName>
        <description>If the Licence Account is In Infringement, Credit Status is automatically changed to Enforcement - Infringement</description>
        <field>JVCO_Credit_Status__c</field>
        <literalValue>Enforcement - Infringement</literalValue>
        <name>CreditStatus_InInfringment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Credit_Status_Not_In_Debt</fullName>
        <field>JVCO_Credit_Status__c</field>
        <literalValue>Not in Debt</literalValue>
        <name>Credit Status Not In Debt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_FF_Credit_Status_On_Hold</fullName>
        <field>c2g__CODACreditStatus__c</field>
        <literalValue>On Hold</literalValue>
        <name>FF Credit Status - On Hold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Licence_Account_Active_False</fullName>
        <field>JVCO_Active__c</field>
        <literalValue>0</literalValue>
        <name>Licence Account Active False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Populate_Customer_Account_Number</fullName>
        <field>JVCO_Customer_Account_Number__c</field>
        <formula>&quot;CUS-&quot; &amp; RIGHT(Licence_Account_Number__c,8)</formula>
        <name>Populate Customer Account Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Review_Type_Do_Not_Renew</fullName>
        <field>JVCO_Review_Type__c</field>
        <literalValue>Do not renew</literalValue>
        <name>Review Type Do Not Renew</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>renewalType_DoNotRenew</fullName>
        <field>JVCO_Review_Type__c</field>
        <literalValue>Do not renew</literalValue>
        <name>renewalType=DoNotRenew</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Set_Cust_Acc_Closure_Date_Today</fullName>
        <field>JVCO_Closure_Date__c</field>
        <formula>IF(ISBLANK(JVCO_Closure_Date__c),today(),JVCO_Closure_Date__c)</formula>
        <name>Set Customer Account Closure Date Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Set_Cust_Acc_Inactive</fullName>
        <field>JVCO_Active__c</field>
        <literalValue>0</literalValue>
        <name>Set Customer Account Inactive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Supplier_Status_to_Active</fullName>
        <description>Supplier Status to Active</description>
        <field>JVCO_Supplier_Status__c</field>
        <literalValue>Active</literalValue>
        <name>Supplier Status to Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_UpdateDCA2AccountBalance</fullName>
        <field>JVCO_Account_Balance_Passed_DCA_2ndStage__c</field>
        <formula>IF(DATEVALUE(JVCO_Passed_to_DCA_Date_2nd_Stage__c) = TODAY(), ffps_accbal__Account_Balance__c, null)</formula>
        <name>JVCO_UpdateDCA2AccountBalance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_Credit_Status</fullName>
        <description>Updated Credit status to Approved</description>
        <field>c2g__CODACreditStatus__c</field>
        <literalValue>Credit Allowed</literalValue>
        <name>Update Credit Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_DCA2ndStageLetterDate</fullName>
        <field>JVCO_Passed_to_DCA_2nd_Stage_Batch__c</field>
        <formula>TODAY()</formula>
        <name>JVCO_Update_DCA2ndStageLetterDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_External_Id</fullName>
        <field>c2g__CODAExternalId__c</field>
        <formula>AccountNumber</formula>
        <name>JVCO Update External Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_Sent_Manual_Review_Checkbox</fullName>
        <field>JVCO_Sent_Manual_Review_Notif__c</field>
        <literalValue>1</literalValue>
        <name>Update Sent Manual Review Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_Status_to_Approved</fullName>
        <field>JVCO_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_OutgoingStatChngeDate_2ndStg</fullName>
        <field>JVCO_OutgoingStatus_Change_Date_2ndStage__c</field>
        <formula>TODAY()</formula>
        <name>Update_OutgoingStatusChangeDate-2ndStage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Manually_Passed_Date</fullName>
        <field>Pre_Enforcement_Manually_Passed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Manually Passed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Renewal_Type_to_Manual</fullName>
        <field>JVCO_Review_Type__c</field>
        <literalValue>Manual</literalValue>
        <name>Set Renewal Type to Manual</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Un_check_NVM_Hidden_Dialler_Call_Count</fullName>
        <description>Un-checks NVM Hidden Dialler Call Count Checkbox</description>
        <field>NVM_Hidden_Dialler_Call_Count__c</field>
        <literalValue>0</literalValue>
        <name>Un-check NVM Hidden Dialler Call Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAccountCustomerAccountName</fullName>
        <field>JVCO_Customer_Account_Name__c</field>
        <formula>IF(RecordType.Name == &apos;Licence Account&apos;,  JVCO_Customer_Account__r.Name, Name)</formula>
        <name>UpdateCustomerAccountName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Status</fullName>
        <field>JVCO_Credit_Status__c</field>
        <literalValue>Pre-Enforcement</literalValue>
        <name>Update Credit Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>   
     <fieldUpdates>
        <fullName>Update_Last_Reminder_Level</fullName>
        <field>ffps_custRem__Last_Reminder_Severity_Level__c</field>
        <formula>9</formula>
        <name>Update Last Reminder Level</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NVM_Dialler_Call_Count</fullName>
        <description>Updates NVM Dialler Call Count</description>
        <field>NVM_Dialler_Call_Count__c</field>
        <formula>NVM_Dialler_Call_Count__c + 1</formula>
        <name>Update NVM Dialler Call Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Review_Type</fullName>
        <field>JVCO_Review_Type__c</field>
        <literalValue>Do not renew</literalValue>
        <name>Update Review Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account - Customer Account Dunning</fullName>
        <actions>
            <name>Account_Exclude_Reason_CA_Dunning</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Account_ExcludeFromDunning</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Customer Account is currently in CA Dunning then all LA need to be.</description>
        <formula>(ISNEW()
||
ISCHANGED(ffps_custRem__Exclude_From_Reminder_Process__c))
&amp;&amp;
JVCO_Customer_Account__r.JVCO_Customer_Account_Dunning__c 
&amp;&amp;
RecordType.Name = &quot;Licence Account&quot;
&amp;&amp;
NOT(ffps_custRem__Exclude_From_Reminder_Process__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account - Dunning Strategy %28Blank%29</fullName>
        <actions>
            <name>Account_Dunning_Strategy_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Lock_Dunning_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>(ISCHANGED(ffps_accbal__Account_Balance__c) || ISCHANGED(Dunning_Strategy__c))
&amp;&amp; 
ffps_accbal__Account_Balance__c &lt;= 0 
&amp;&amp; 
(!Lock_Dunning_Strategy__c ||(Lock_Dunning_Strategy__c &amp;&amp; ISPICKVAL(Dunning_Strategy__c,&quot;&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account - Dunning Strategy %28Bus%29</fullName>
        <actions>
            <name>Account_Dunning_Strategy_Business</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Lock_Dunning_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>(ISCHANGED(ffps_accbal__Account_Balance__c) || ISCHANGED(Dunning_Strategy__c))
&amp;&amp;
ffps_accbal__Account_Balance__c &lt;= 3500 &amp;&amp; ffps_accbal__Account_Balance__c &gt; 0
&amp;&amp;
NOT(ISPICKVAL(Dunning_Strategy__c,&quot;Corporate&quot;))
&amp;&amp;
(!Lock_Dunning_Strategy__c ||(Lock_Dunning_Strategy__c &amp;&amp; ISPICKVAL(Dunning_Strategy__c,&quot;&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account - Dunning Strategy %28Corp%29</fullName>
        <actions>
            <name>Account_Dunning_Strategy_Corp</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Lock_Dunning_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>(ISCHANGED(ffps_accbal__Account_Balance__c) || ISCHANGED(Dunning_Strategy__c))
&amp;&amp; 
ffps_accbal__Account_Balance__c &gt; 3500 
&amp;&amp; 
(!Lock_Dunning_Strategy__c ||(Lock_Dunning_Strategy__c &amp;&amp; ISPICKVAL(Dunning_Strategy__c,&quot;&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Do not renew closed Licence Accounts Workflow</fullName>
        <actions>
            <name>JVCO_Licence_Account_Active_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Review_Type_Do_Not_Renew</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  (RecordType.DeveloperName = &apos;JVCO_Licence_Account&apos;),  (not(isblank(text(JVCO_Closure_Reason__c)))),  (ischanged(JVCO_Closure_Reason__c))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DocGen PPL Gap Invoice update</fullName>
        <actions>
            <name>DocGen_PPL_Gap_Invoice_value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.JVCO_Renewal_Scenario__c</field>
            <operation>equals</operation>
            <value>PPL First: Greater Than 60 Days</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_Transition_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting PRS Renewal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_Review_Date__c</field>
            <operation>greaterOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>InEnforcement_CrditStatus</fullName>
        <actions>
            <name>JVCO_CreditStatus_InEnforcement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.JVCO_In_Enforcement__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_In_Infringement__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>If the Licence Account is In Enforcement, Credit Status is automatically changed to Enforcement - Debt - Recovery</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>InInfringement_CrditStatus</fullName>
        <actions>
            <name>JVCO_CreditStatus_InInfringmen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.JVCO_In_Infringement__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_In_Enforcement__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>If the Licence Account is In Infringement, Credit Status is automatically changed to Enforcement - Infringement</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Inactivate Customer Account Workflow</fullName>
        <actions>
            <name>JVCO_Set_Cust_Acc_Closure_Date_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Set_Cust_Acc_Inactive</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(    (RecordType.DeveloperName = &apos;JVCO_Customer_Account&apos;),     (not(isblank(text(JVCO_Closure_Reason__c)))),     (ischanged(JVCO_Closure_Reason__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO Populate External Id</fullName>
        <actions>
            <name>JVCO_Update_External_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISBLANK(c2g__CODAExternalId__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO Update Review Type</fullName>
        <actions>
            <name>Update_Review_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.JVCO_Permit_Holder__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_AccNameConcat</fullName>
        <actions>
            <name>JVCO_AccNameConcat</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_AccNumConcat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Licence Account</value>
        </criteriaItems>
        <description>Updates Account name and Account number for Licence Account</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_CheckIfAgencyIsChangedWithoutValue</fullName>
        <active>false</active>
        <description>A workflow rule to check if JVCO_Agency__c was changed, if the field is empty, the account discount value field value will be set to 0</description>
        <formula>(NOT(ISNEW()) &amp;&amp; ISCHANGED( JVCO_Agency__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_DoNotExcludeFromDunningifAccountBalanceis30Above</fullName>
        <actions>
            <name>JVCO_Account_ExcludeFromDunningFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Account_ExcludeFromDunningReasBlank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8 AND 9 AND 10 AND 11 AND 12 AND 13 AND ( 14 OR 15 )</booleanFilter>
        <criteriaItems>
            <field>Account.ffps_accbal__Account_Balance__c</field>
            <operation>greaterThan</operation>
            <value>30</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Licence Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>notEqual</operation>
            <value>Key Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>notEqual</operation>
            <value>Agency</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_Credit_Status__c</field>
            <operation>notEqual</operation>
            <value>Passed to DCA - 1st Stage,Passed to DCA – 2nd Stage,LV Debt Investigation,Pre DCA 2nd Stage,DCA Review,DCA 2 Review,Pre-Enforcement,Internal DCA,Corporate Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>notEqual</operation>
            <value>Jukebox Operator</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_Manual_pass_to_DCA__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_Manual_Passed_to_DCA_2nd_Stage__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_In_Enforcement__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_In_Infringement__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_In_Insolvency__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_Credit_Status__c</field>
            <operation>notEqual</operation>
            <value>Excluded from Dunning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_Exclude_Reason__c</field>
            <operation>notEqual</operation>
            <value>Live Account,Manually Paused,Direct Debit,Complaint,Query,Promise to Pay</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_Exclude_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ffps_custRem__Exclude_From_Reminder_Process__c</field>
            <operation>notEqual</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_ManualPasstoDCASecondStage</fullName>
        <actions>
            <name>JVCO_Account_Balance_DCA_2nd_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.JVCO_Passed_to_DCA_Date_2nd_Stage__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_PopulateAcctBalSentToDCA</fullName>
        <actions>
            <name>JVCO_Account_PopulateAcctBalSentToDCA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_UpdateDCA2AccountBalance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populate the field &apos;Account Balance sent to DCA&apos; whenever the field Passed to DCA Date = Today.</description>
        <formula>OR((DATEVALUE(JVCO_Passed_to_DCA_date__c)) = TODAY()  &amp;&amp; ISBLANK(JVCO_AccountBalance_DCA__c),
(DATEVALUE(JVCO_Passed_to_DCA_Date_2nd_Stage__c)) = TODAY()  &amp;&amp; ISBLANK(JVCO_Account_Balance_Passed_DCA_2ndStage__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_PopulateCustAccNum</fullName>
        <actions>
            <name>JVCO_Populate_Customer_Account_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Account</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_Remove_Account from Auto-Renewal_if_in_Enforcement</fullName>
        <actions>
            <name>renewalType_DoNotRenew</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.JVCO_In_Enforcement__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_RevertAcctBalSentToDCAToZero</fullName>
        <actions>
            <name>JVCO_Account_RevertAcctBalToZero</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Account_SetCreditStatustoNotinDebt</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Account_SetPassedToDCADatetoBlank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ffps_accbal__Account_Balance__c = 0  &amp;&amp; ISPICKVAL(JVCO_Credit_Status__c, &apos;Passed to DCA&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_SetCreditStatustoPreEnforcement</fullName>
        <actions>
            <name>JVCO_Account_SetCreditStatustoPreEnforc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>RecordType.DeveloperName = &apos;JVCO_Licence_Account&apos; &amp;&amp; ISPICKVAL(JVCO_DCA_Status__c, &apos;Withdrawal&apos;) &amp;&amp; ISPICKVAL(JVCO_Status_Reason_Code__c, &apos;180 - WORK EXHAUSTED&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_SetCreditStatustoPreEnforcementafteraWeek</fullName>
        <active>false</active>
        <formula>RecordType.DeveloperName = &apos;JVCO_Licence_Account&apos; &amp;&amp; ISPICKVAL(JVCO_Credit_Status__c, &apos;Debt - Final demand&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>JVCO_Account_SetCreditStatustoPreEnforc</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.ffps_custRem__Last_Reminder_Generated_Date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit> 
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>JVCO_Account_SetReviewTypeDoNotRenew</fullName>
        <actions>
            <name>JVCO_Account_SetReviewType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Licence Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_In_Infringement__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_In_Enforcement__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_Set_Renewal_Type_to_Manual based on non Auto-Renewable Tariffs</fullName>
        <actions>
            <name>Set_Renewal_Type_to_Manual</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(JVCO_Non_Auto_Renewable_Product_Count__c &gt; 0, not(isblank(JVCO_Non_Auto_Renewable_Product_Count__c )),ISPICKVAL( JVCO_Review_Type__c,&apos;Automatic&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_UpdateCustomerAcctName</fullName>
        <actions>
            <name>UpdateAccountCustomerAccountName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISNEW() || ISCHANGED(Name)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_UpdateIncomingStatusChangeDate</fullName>
        <actions>
            <name>JVCO_Account_UpdateIncomingStatusDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Update_DCA2ndStageLetterDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>(
ISCHANGED(JVCO_Incoming_Status_to_DCA__c)  
&amp;&amp;
NOT(ISPICKVAL(JVCO_Incoming_Status_to_DCA__c,&quot;&quot;))
)

||  
(
ISCHANGED(JVCO_Incoming_Reason_Code__c)
&amp;&amp;
NOT(ISPICKVAL(JVCO_Incoming_Reason_Code__c,&quot;&quot;))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_UpdateIncomingStatusChangeDate_2ndStage</fullName>
        <actions>
            <name>JVCO_Account_UpdateIncomingStatusDate2nd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>(ISCHANGED(JVCO_Incoming_Status_from_DCA_2nd_Stage__c) 
&amp;&amp;
NOT(ISPICKVAL(JVCO_Incoming_Status_from_DCA_2nd_Stage__c,&quot;&quot;))
)
||
(  
ISCHANGED(JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c)
&amp;&amp;
NOT(ISPICKVAL(JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c,&quot;&quot;))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_UpdateRemindersBlockedUntilDate</fullName>
        <actions>
            <name>JVCO_Account_UpdateSeverityBlockedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Promise to Pay date to exclude Licence Account from dunning cycle until date is met</description>
        <formula>((JVCO_Promise_to_Pay__c + 1)&gt;  ffps_custRem__Reminders_Blocked_Until_Date__c)  ||  ISBLANK(ffps_custRem__Reminders_Blocked_Until_Date__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Account_UpdateStatusChangeDate</fullName>
        <actions>
            <name>JVCO_Account_UpdateStatusChangeDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>!ISCHANGED( JVCO_Status_Change_Date__c )
&amp;&amp;
(
ISCHANGED(JVCO_Status_Reason_Code__c)
&amp;&amp;
NOT(ISPICKVAL(JVCO_Status_Reason_Code__c,&quot;&quot;))
)
||  
(
ISCHANGED(JVCO_DCA_Status__c)
&amp;&amp;
NOT(ISPICKVAL(JVCO_DCA_Status__c,&quot;&quot;))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_CreditStatusontoNotinDebt</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.ffps_accbal__Account_Balance__c</field>
            <operation>lessOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JVCO_Credit_Status__c</field>
            <operation>equals</operation>
            <value>Passed to DCA - 1st Stage</value>
        </criteriaItems>
        <description>Updates the Credit Status to Not in Debt. When Credit Status = Not in Debt and Account Balance &lt;= 0.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>JVCO_Account_RevertAcctBalToZero</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>JVCO_Account_SetPassedToDCADatetoBlank</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>JVCO_CreditStatusNotinDebt</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
     <rules>
        <fullName>JVCO_UntickedInfrigementEnforcement_CredStatusNotInDebt</fullName>
        <actions>
            <name>JVCO_Credit_Status_Not_In_Debt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If In Enforcement and In Infringement checkbox has been changed from true to false, the credit status should be changed to &apos;Not In Debt&apos;</description>
        <formula>AND(

    OR(
        AND(
            JVCO_In_Enforcement__c = false
           ),

        AND(
            JVCO_In_Infringement__c = false
           ),

        AND(
            JVCO_In_Enforcement__c = false,
            JVCO_In_Infringement__c = false
            )
      ),

    OR(
          ISPICKVAL(PRIORVALUE( JVCO_Credit_Status__c ),&quot;Enforcement - Infringement&quot;),
          ISPICKVAL(PRIORVALUE( JVCO_Credit_Status__c ),&quot;Enforcement - Debt Recovery&quot;)
        
       )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Update_DCA_2ndStage_OutgoingDate</fullName>
        <actions>
            <name>JVCO_Update_OutgoingStatChngeDate_2ndStg</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>(
ISCHANGED( JVCO_Outgoing_Reason_Code_DCA_2nd_Stage__c ) 
&amp;&amp;
NOT(ISPICKVAL(JVCO_Outgoing_Reason_Code_DCA_2nd_Stage__c,&quot;&quot;))
)
||
(
ISCHANGED( JVCO_Outgoing_Status_to_DCA_2nd_Stage__c )
&amp;&amp;
NOT(ISPICKVAL(JVCO_Outgoing_Status_to_DCA_2nd_Stage__c,&quot;&quot;))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sent Manual Review Checkbox</fullName>
        <actions>
          <name>JVCO_Update_Sent_Manual_Review_Checkbox</name>
          <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR 4) AND (5 OR 6) AND 7</booleanFilter>
        <criteriaItems>
          <field>Account.RecordTypeId</field>
          <operation>equals</operation>
          <value>Licence Account</value>
        </criteriaItems>
        <criteriaItems>
          <field>Account.JVCO_Renewal_Scenario__c</field>
          <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
          <field>Account.ffps_custRem__Preferred_Communication_Channel__c</field>
          <operation>equals</operation>
          <value>Email</value>
        </criteriaItems>
        <criteriaItems>
          <field>Account.ffps_custRem__Preferred_Communication_Channel__c</field>
          <operation>equals</operation>
          <value>Print</value>
        </criteriaItems>
        <criteriaItems>
          <field>Account.Type</field>
          <operation>notEqual</operation>
          <value>Key Account</value>
        </criteriaItems>
        <criteriaItems>
          <field>Account.Type</field>
          <operation>notEqual</operation>
          <value>Agency</value>
        </criteriaItems>
        <criteriaItems>
          <field>Account.JVCO_Sent_Manual_Review_Notifi_Email__c</field>
          <operation>equals</operation>
          <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Manually Passing to Pre-Enforcement</fullName>
        <actions>
          <name>Set_Manually_Passed_Date</name>
          <type>FieldUpdate</type>
        </actions>
        <actions>
          <name>Update_Credit_Status</name>
          <type>FieldUpdate</type>
        </actions>
        <actions>
          <name>Update_Last_Reminder_Level</name>
          <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
          <field>Account.Manually_Pass_to_Pre_Enforcement__c</field>
          <operation>equals</operation>
          <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NVM Dialler Call Count</fullName>
        <actions>
            <name>Update_NVM_Dialler_Call_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.NVM_Hidden_Dialler_Call_Count__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates &apos;NVM Dialler Call Count&apos; and resets &apos;NVM Hidden Dialler Call Count&apos; checkbox to un-ticked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Un_check_NVM_Hidden_Dialler_Call_Count</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.NVM_Time_Acc__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Terminate_the_Customer_Licence</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please proceed to terminate the customer account.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Terminate the Customer Account</subject>
    </tasks>
</Workflow>