<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>JVCO_SendNotificationKAManager</fullName>
        <description>Send Notification to KA Manager</description>
        <protected>false</protected>
        <recipients>
            <field>JVCO_OwnerEmail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@pplprs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/JVCO_KA_Template_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>JVCO_DocQueue_SetClonedtoTrue</fullName>
        <field>JVCO_Clone_DQ__c</field>
        <literalValue>1</literalValue>
        <name>JVCO_DocQueue_SetClonedtoTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_DocQueue_SetRecordTypetoRemittance</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Send_Document_Remittance</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>JVCO_DocQueue_SetRecordTypetoRemittance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_DocumentQueue_SetRecordTypetoEmail</fullName>
        <field>RecordTypeId</field>
        <lookupValue>JVCO_Send_Document</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>JVCO_DocumentQueue_SetRecordTypetoEmail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Document_Queue_UpdateStatus</fullName>
        <field>JVCO_Generation_Status__c</field>
        <literalValue>Request Submitted To Conga</literalValue>
        <name>Document Queue UpdateStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_UpdateAccountOwner</fullName>
        <description>Used by DocGen</description>
        <field>JVCO_OwnerEmail__c</field>
        <formula>JVCO_Related_Account__r.Owner.Email</formula>
        <name>Update Account Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>JVCO_Download</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Transmission</fullName>
        <field>JVCO_Transmission__c</field>
        <literalValue>Letter</literalValue>
        <name>Transmission</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>JVCO_Document_Queue_SendOutboundMessage</fullName>
        <apiVersion>39.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Id</fields>
        <fields>JVCO_Contract__c</fields>
        <fields>JVCO_Query_Ids2__c</fields>
        <fields>JVCO_Query_Ids_Ext_New2__c</fields>
        <fields>JVCO_Query_Ids_Ext_New3__c</fields>
        <fields>JVCO_Query_Ids_Ext_New__c</fields>
        <fields>JVCO_Query_Ids_Ext__c</fields>
        <fields>JVCO_Query_Ids__c</fields>
        <fields>JVCO_Related_Account__c</fields>
        <fields>JVCO_Related_Campaign__c</fields>
        <fields>JVCO_Related_Case__c</fields>
        <fields>JVCO_Related_Credit_Note__c</fields>
        <fields>JVCO_Related_Customer_Account__c</fields>
        <fields>JVCO_Related_Dunning__c</fields>
        <fields>JVCO_Related_Invoice__c</fields>
        <fields>JVCO_Related_Payonomy_Payment_Agreement__c</fields>
        <fields>JVCO_Related_Payonomy_Payment__c</fields>
        <fields>JVCO_Related_Quote__c</fields>
        <fields>JVCO_Related_Reminder__c</fields>
        <fields>JVCO_Related_Remittance__c</fields>
        <fields>JVCO_Related_SalesInvoice__c</fields>
        <fields>JVCO_Template_Ids2__c</fields>
        <fields>JVCO_Template_Ids__c</fields>
        <fields>JVCO_WF_Conga_Formula2__c</fields>
        <fields>JVCO_WF_Conga_Formula__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>deployment.user@accenture.com.prsppl</integrationUser>
        <name>Document Queue SendOutboundMessage</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>JVCO_DocQueue_IdentifyClonedRecord</fullName>
        <actions>
            <name>JVCO_DocQueue_SetClonedtoTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCLONE()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_DocumentQueue_SetRecordTypetoEmail</fullName>
        <actions>
            <name>JVCO_DocumentQueue_SetRecordTypetoEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>JVCO_Document_Queue__c.JVCO_Transmission__c</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_DocumentQueue_SetRecordTypetoRemittance</fullName>
        <actions>
            <name>JVCO_DocQueue_SetRecordTypetoRemittance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>JVCO_Related_Payment_Media_Summary__r.JVCO_Generate_DQ_Record__c = TRUE
&amp;&amp;
JVCO_Related_Payment_Media_Summary__r.JVCO_DQ_Record_Generated__c = FALSE
&amp;&amp;
JVCO_Related_Payment_Media_Summary__r.c2g__Account__r.RecordType.DeveloperName = &apos;Supplier&apos;
&amp;&amp;
OR(ISPICKVAL(JVCO_Related_Payment_Media_Summary__r.c2g__Account__r.ffps_custRem__Preferred_Communication_Channel__c, &apos;Email&apos;),
ISPICKVAL(JVCO_Related_Payment_Media_Summary__r.c2g__Account__r.ffps_custRem__Preferred_Communication_Channel__c, &apos;Print&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Document_Queue_GenerateCongaDocument</fullName>
        <actions>
            <name>JVCO_Document_Queue_UpdateStatus</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Document_Queue_SendOutboundMessage</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>For the document generated automatically, we need an outbound message to be created automatically and sent to Conga to trigger the automatic generation of the document.</description>
        <formula>OR(
  JVCO_Related_Account__c !=null,
  JVCO_Related_Invoice__c !=null,
  JVCO_Related_Quote__c!=null,
  JVCO_Related_Campaign__c !=null,
  JVCO_Related_Credit_Note__c !=null,
  Related_Lead__c !=null,
  JVCO_Related_Payonomy_Payment__c !=null,
  JVCO_Related_Payonomy_Payment_Agreement__c !=null,
  JVCO_Related_Case__c !=null,
  JVCO_Related_Remittance__c !=null,
  JVCO_Related_SalesInvoice__c !=null,
  JVCO_Related_Payment_Media_Summary__c !=null)
 &amp;&amp;  
 AND (
   Text(JVCO_Transmission__c)!=null, 
   Text(JVCO_Format__c) !=null, 
   Text(JVCO_Generation_Status__c) = &apos;To be Generated&apos;, 
  TEXT(JVCO_Scenario__c) !=null )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Generate_Key_Account_Invoices</fullName>
        <actions>
            <name>Record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Transmission</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>JVCO_Document_Queue__c.Scenario_Category__c</field>
            <operation>equals</operation>
            <value>Key Account</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send Notification to KA Manager</fullName>
        <actions>
            <name>JVCO_SendNotificationKAManager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>JVCO_UpdateAccountOwner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>JVCO_Document_Queue__c.JVCO_SendReminderEmail__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
