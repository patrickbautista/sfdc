<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Completion_Date_Update</fullName>
        <description>Update Complete date to today.</description>
        <field>JVCO_Completion_Date__c</field>
        <formula>Now()</formula>
        <name>Completion Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetPublicTrue</fullName>
        <field>IsVisibleInSelfService</field>
        <literalValue>1</literalValue>
        <name>SetPublicTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_Check_Box</fullName>
        <field>JVCO_Task_To_Do__c</field>
        <literalValue>1</literalValue>
        <name>Tick Check Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Completed_By</fullName>
        <description>Set Completed by to User.</description>
        <field>Completed_By__c</field>
        <formula>$User.FirstName +&quot; &quot;+ $User.LastName</formula>
        <name>Update Completed By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Completion Date Update</fullName>
        <actions>
            <name>Completion_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Completed_By</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Task is Complete update completion date to today.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flag Open Tasks</fullName>
        <actions>
            <name>Tick_Check_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <description>When a task is created as open, &quot;Task to do&quot; will be set to True. This will allow reporting of when agents have set follow up tasks and allow us to distinguish between Tasks to be completed and tasks created from Calls, notes and emails.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SetPublicTrueOnCreate</fullName>
        <actions>
            <name>SetPublicTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.IsVisibleInSelfService</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
