<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>JVCO_Email_Alert_AP_Voucher_has_been_rejected</fullName>
        <description>Email Alert AP Voucher has been rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ffxp__ffdc5_emailTemplates/JVCO_AP_Voucher_rejected</template>
    </alerts>
    <alerts>
        <fullName>JVCO_Email_Alert_Dimension_1_Approver</fullName>
        <description>Email Alert Dimension 1 Approver</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ffxp__ffdc5_emailTemplates/JVCO_Email_Alert_Dimension_1_Approver</template>
    </alerts>
    <alerts>
        <fullName>JVCO_Email_Alert_to_AP_Analyst_of_Approved_cost</fullName>
        <description>Email Alert to AP Analyst of Approved cost</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ffxp__ffdc5_emailTemplates/JVCO_Email_alert_to_AP_Analyst_to_inform_them_of_approved_cost</template>
    </alerts>
    <fieldUpdates>
        <fullName>JVCO_Export_Status_changes_to_New</fullName>
        <field>SCMC__Export_Status__c</field>
        <literalValue>New</literalValue>
        <name>Export Status changes to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Export_Status_equals_in_approval</fullName>
        <field>SCMC__Export_Status__c</field>
        <literalValue>In Approval</literalValue>
        <name>Export Status equals in approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Export_Status_to_unapprove</fullName>
        <field>SCMC__Export_Status__c</field>
        <literalValue>Unapproved</literalValue>
        <name>JVCO_Export_Status_to_unapprove</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
