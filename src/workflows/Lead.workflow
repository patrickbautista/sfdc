<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_alert_for_HV_New_Business_Pardot_Leads</fullName>
        <description>Email alert for HV New Business Pardot Leads</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ffxp__ffdc5_emailTemplates/Pardot_Lead_Created</template>
    </alerts>
    <alerts>
        <fullName>SLH_Lead_Status_Updated</fullName>
        <description>SLH Lead Status Updated</description>
        <protected>false</protected>
        <recipients> 
            <recipient>SLH_Lead_Updates</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>noreply@pplprs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/SLH_Status_Change</template>
    </alerts>
    <fieldUpdates>
        <fullName>Date_Assigned_to_Agent_update</fullName>
        <description>Date Assigned to Agent update</description>
        <field>Date_Assigned_to_A__c</field>
        <formula>today()</formula>
        <name>Date Assigned to Agent update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Assignment_Date</fullName>
        <field>Assignment_Date__c</field>
        <formula>TODAY()</formula>
        <name>FU Assignment Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increment_Dialler_Call_Count</fullName>
        <field>NVM_Dialler_Call_Count__c</field>
        <formula>NVM_Dialler_Call_Count__c + 1</formula>
        <name>Increment Dialler Call Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Lead_SetAssignNewToBusiness</fullName>
        <field>JVCO_Assign_To_Default_Queue__c</field>
        <literalValue>0</literalValue>
        <name>JVCO Lead SetAssignNewToBusiness</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Lead_SetFieldVisitRequestedDate</fullName>
        <field>JVCO_Field_Visit_Requested_Date__c</field>
        <formula>IF( JVCO_Field_Visit_Requested__c == true, TODAY(), NULL)</formula>
        <name>JVCO_Lead_SetFieldVisitRequestedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Lead_UpdateLeadStatusToQualified</fullName>
        <field>Status</field>
        <literalValue>Qualified</literalValue>
        <name>JVCO UpdateLeadStatusToQualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Lead_UpdateSoleTrader</fullName>
        <field>JVCO_Account_Name__c</field>
        <formula>FirstName  &amp; &apos; &apos; &amp; LastName</formula>
        <name>JVCO_Lead_UpdateSoleTrader</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Uncheck_Assign_to_New_Business</fullName>
        <field>JVCO_Assign_To_Default_Queue__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Assign to New Business</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Venue_City_to_Address_City</fullName>
        <description>Copy the Venue city information to Address City</description>
        <field>City</field>
        <formula>JVCO_Venue_City__c</formula>
        <name>Venue City to Address City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Venue_County_to_Address_County</fullName>
        <description>Copy the Venue county information to Address county</description>
        <field>State</field>
        <formula>JVCO_Venue_County__c</formula>
        <name>Venue County to Address County</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Venue_PostCode_to_Address_PostCode</fullName>
        <description>Copy the venue postcode information to address postcode</description>
        <field>PostalCode</field>
        <formula>JVCO_Venue_Postcode__c</formula>
        <name>Venue PostCode to Address PostCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Venue_Street_to_Address_Street</fullName>
        <description>Copy the venue street information to address street</description>
        <field>Street</field>
        <formula>JVCO_Venue_Street__c</formula>
        <name>Venue Street to Address Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Pardot_Field_Update</fullName>
        <description>Update Pardot_Lead__c to TRUE</description>
        <field>Pardot_Lead__c</field>
        <literalValue>1</literalValue>
        <name>Lead - Pardot Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Set_LeadSource_To_SetListHub</fullName>
        <field>LeadSource</field>
        <literalValue>Set List Hub</literalValue>
        <name>Set_LeadSource_To_SetListHub</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trigger_Lead_Mop_Up_TRUE</fullName>
        <description>Set&apos;s Trigger Lead Mop up to TRUE</description>
        <field>Trigger_Lead_Mop_Up__c</field>
        <literalValue>1</literalValue>
        <name>Trigger Lead Mop Up - TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_Hidden_Dialler_Call_Count</fullName>
        <field>NVM_Hidden_Dialler_Call_Count__c</field>
        <literalValue>0</literalValue>
        <name>Untick Hidden Dialler Call Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Customer_has_Same_Address_Checkbo</fullName>
        <description>Workflow to update the customer has the same address checkbox to false</description>
        <field>JVCO_Customer_has_same_address__c</field>
        <literalValue>0</literalValue>
        <name>Update Customer has Same Address Checkbo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DoNotCall</fullName>
        <field>DoNotCall</field>
        <literalValue>1</literalValue>
        <name>Update DoNotCall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pardot_Referring_Domain_Field</fullName>
        <field>JVCO_Pardot_Referring_Domain__c</field>
        <formula>IF(FIND(&apos;/&apos;,SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(pi__first_touch_url__c, &apos;http://&apos;, NULL), &apos;https://&apos;, NULL), &apos;www.&apos;, NULL))&gt;0,LEFT(SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(pi__first_touch_url__c, &apos;http://&apos;, NULL), &apos;https://&apos;, NULL), &apos;www.&apos;, NULL),FIND(&apos;/&apos;,SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(pi__first_touch_url__c, &apos;http://&apos;, NULL), &apos;https://&apos;, NULL), &apos;www.&apos;, NULL))-1),SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(pi__first_touch_url__c, &apos;http://&apos;, NULL), &apos;https://&apos;, NULL), &apos;www.&apos;, NULL))</formula>
        <name>Update Pardot Referring Domain Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Worked_Lead_Checkbox</fullName>
        <field>Worked_Lead__c</field>
        <literalValue>1</literalValue>
        <name>Update Worked Lead Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Assignment Date</fullName>
        <actions>
            <name>FU_Assignment_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This will populate the Assignment Date field.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JVCO Lead Update DoNotCall</fullName>
        <actions>
            <name>Update_DoNotCall</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 OR 2) AND 3) OR 4</booleanFilter>
        <criteriaItems>
            <field>Lead.JVCO_TPS_Do_Not_Contact__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_CTPS_Do_Not_Contact__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Outcome__c</field>
            <operation>equals</operation>
            <value>Do Not Contact Again</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Lead_CheckAssignNewToBusiness</fullName>
        <actions>
            <name>JVCO_Lead_SetAssignNewToBusiness</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(AND(ISNEW(), JVCO_Assign_To_Default_Queue__c), AND(ISCHANGED(JVCO_Assign_To_Default_Queue__c ), JVCO_Assign_To_Default_Queue__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Lead_CheckFieldVisitRequestedDate</fullName>
        <actions>
            <name>JVCO_Lead_SetFieldVisitRequestedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Worked_Lead_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checking the box Field Visit Requested and saving the record sets the date in the Field Visit Requested Date to today's date</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Lead_CheckLeadStatusToQualified</fullName>
        <actions>
            <name>JVCO_Lead_UpdateLeadStatusToQualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 4 AND ((5 AND 17) OR (4 AND 16)) AND 7 AND (8 OR 14 OR 15) AND (9 OR 10 OR 11) AND 12 AND 13 AND 14 AND(16 OR(17 AND 2 AND 3 AND 6)) AND 18 AND ((19 AND 20)OR(21 AND 22))  AND 23 AND 24</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Venue_Street__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Venue_Postcode__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Venue_Type__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Venue_Sub_Type__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Account_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LastName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Email</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Phone</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.MobilePhone</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Street</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.PostalCode</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LastName</field>
            <operation>notEqual</operation>
            <value>Unknown</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LastName</field>
            <operation>notEqual</operation>
            <value>unknown</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Activity_Based_Licence__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Activity_Based_Licence__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Preferred_Communication_Method__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Activity_Based_Licence__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Activity_Based_Licence__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Start_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.JVCO_Business_Type_Lead__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
           <field>Lead.Title</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Lead_CheckSoleTrader</fullName>
        <actions>
            <name>JVCO_Lead_UpdateSoleTrader</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.JVCO_Sole_Trader__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Lead_CopyVenueAddress</fullName>
        <actions>
            <name>JVCO_Venue_City_to_Address_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Venue_County_to_Address_County</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Venue_PostCode_to_Address_PostCode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Venue_Street_to_Address_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the checkbox &quot;Customer has the same address&quot; field is ticked, copy the Venue address information to the lead address</description>
        <formula>AND(JVCO_Customer_has_same_address__c = TRUE,  JVCO_Activity_Based_Licence__c = False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Lead_UncheckCustomerhassameAddressfieldforActivitybasedlicence</fullName>
        <actions>
            <name>Update_Customer_has_Same_Address_Checkbo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.JVCO_Activity_Based_Licence__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Workflow to set the customer has same address field to false when the user selects the activity based licence checkbox</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>lead Assignment date</fullName>
        <actions>
            <name>Date_Assigned_to_Agent_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ischanged(OwnerId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Trigger_Lead_Mop_Up</fullName>
        <actions>
            <name>Lead_Pardot_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trigger_Lead_Mop_Up_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.JVCO_Outcome__c</field>
            <operation>equals</operation>
            <value>Fax Machine,Wrong number/fax,Dial Attempts Reached</value>
        </criteriaItems>
        <description>Used for Pardot. When one of the outcomes is selected then tick &apos;Trigger Lead Mop Up&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_Trigger_Lead_Mop_Up_Delay</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.JVCO_Outcome__c</field>
            <operation>equals</operation>
            <value>No Answer,Phone engaged,Not yet dialled</value>
        </criteriaItems>
        <description>Used for Pardot. When one of the outcomes is selected then tick &apos;Trigger Lead Mop Up&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_Pardot_Field_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Trigger_Lead_Mop_Up_TRUE</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
	<rules>
        <fullName>Lead Source - Set List Hub</fullName>
        <actions>
            <name>Set_LeadSource_To_SetListHub</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Set List Hub</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NVM Update Dialler Call Count Lead</fullName>
        <actions>
            <name>Increment_Dialler_Call_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.NVM_Hidden_Dialler_Call_Count__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Untick_Hidden_Dialler_Call_Count</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.NVM_Time_Lead__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pardot Lead Email Alert</fullName>
        <actions>
            <name>Email_alert_for_HV_New_Business_Pardot_Leads</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Pardot User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>High Value Web Leads</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Pardot Lead Sharing</fullName>
        <actions>
            <name>Lead_Pardot_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Pardot User</value>
        </criteriaItems>
        <description>Mark the Pardot field as TRUE so that Pardot has viability of it</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate Pardot Referring Domain</fullName>
        <actions>
            <name>Update_Pardot_Referring_Domain_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.JVCO_Pardot_Referring_Domain__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.pi__first_touch_url__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SLH Status update</fullName>
        <actions>
            <name>SLH_Lead_Status_Updated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (ISCHANGED (Status),  ISPICKVAL(LeadSource,&quot;Set List Hub&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
