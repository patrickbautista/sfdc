<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>JVCO_Email_to_user_after_successful_transaction</fullName>
        <description>Email to user after successful transaction.</description>
        <protected>false</protected>
        <recipients>
            <field>EmailForReceipt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>payments@pplprs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/JVCO_PaymentReceipt</template>
    </alerts>
    <rules>
        <fullName>JVCO_PaymentReceipt</fullName>
        <actions>
            <name>JVCO_Email_to_user_after_successful_transaction</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PAYBASE2__Payment__c.PAYBASE2__Is_Paid__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>After successful payment ,Email will be sent to contact.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
