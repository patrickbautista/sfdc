<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Mailing_City</fullName>
        <description>Use Shipping City from Customer Account as Mailing City.</description>
        <field>MailingCity</field>
        <formula>Account.ShippingCity</formula>
        <name>Update Mailing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mailing_County</fullName>
        <description>Use Shipping County from Customer Account as Mailing County.</description>
        <field>MailingState</field>
        <formula>Account.ShippingState</formula>
        <name>Update Mailing County</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mailing_Postcode</fullName>
        <description>Use Shipping Postcode fro Customer Account as Mailing Postcode.</description>
        <field>MailingPostalCode</field>
        <formula>Account.ShippingPostalCode</formula>
        <name>Update Mailing Postcode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mailing_Street</fullName>
        <description>Use Shipping address for Customer Account as Mailing Address</description>
        <field>MailingStreet</field>
        <formula>Account.ShippingStreet</formula>
        <name>Update Mailing Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Use_Main_Customer_Address</fullName>
        <description>Update Use Main Customer Add to false</description>
        <field>Use_Main_Customer_Address__c</field>
        <literalValue>0</literalValue>
        <name>Update Use Main Customer Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Use Customer Account Address for Contact</fullName>
        <actions>
            <name>Update_Mailing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Mailing_County</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Mailing_Postcode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Mailing_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Use_Main_Customer_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Use_Main_Customer_Address__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Use related Customer Account address for a contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
