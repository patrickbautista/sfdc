<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JVCO_Trans_Line_Dimension_2_Update</fullName>
        <field>JVCO_Dimension2_Name__c</field>
        <formula>c2g__Dimension2__r.Name</formula>
        <name>Trans Line Dimension 2 Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransLine_Jitterbit_Extraction_Rule_Text</fullName>
        <field>Jitterbit_Extraction_Rule_Text__c</field>
        <formula>TEXT( Jitterbit_Extraction_Rule__c )</formula>
        <name>TransLine Jitterbit Extraction Rule Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Dimension 2 Transline update</fullName>
        <actions>
            <name>JVCO_Trans_Line_Dimension_2_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>c2g__codaTransactionLineItem__c.c2g__LineType__c</field>
            <operation>equals</operation>
            <value>Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TransLine_Jitterbit_Extraction_Rule_Update</fullName>
        <actions>
            <name>TransLine_Jitterbit_Extraction_Rule_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the value of Jitterbit Extraction Rule Text whenever the Jitterbit Extraction Rule is updated.</description>
        <formula>ISCHANGED( Jitterbit_Extraction_Rule__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
