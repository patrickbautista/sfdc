<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JVCO_Update_Credit_Status</fullName>
        <field>JVCO_Credit_Status__c</field>
        <formula>TEXT(c2g__Account__r.JVCO_Credit_Status__c)</formula>
        <name>Update Credit Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_Update_DCA_Type</fullName>
        <field>JVCO_DCA_Type__c</field>
        <formula>CASE(JVCO_Credit_Status__c,
&quot;Passed to DCA - 1st Stage&quot;,TEXT(c2g__Account__r.JVCO_DCA_Type__c),
&quot;Passed to DCA – 2nd Stage&quot;,TEXT(c2g__Account__r.JVCO_DCA_Type_2nd_Stage__c),
&quot;&quot;)</formula>
        <name>Update DCA Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Credit Status %26 DCA Fields</fullName>
        <actions>
            <name>JVCO_Update_Credit_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JVCO_Update_DCA_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a cash matching history is created then stamp the credit status and dca fields</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
