<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CredNote_Jitterbit_Extraction_Rule_Tex</fullName>
        <field>Jitterbit_Extraction_Rule_Text__c</field>
        <formula>TEXT( Jitterbit_Extraction_Rule__c )</formula>
        <name>CredNote Jitterbit Extraction Rule Tex</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CredNote_Jitterbit_Extraction_Rule_Text</fullName>
        <field>Jitterbit_Extraction_Rule_Text__c</field>
        <formula>TEXT( Jitterbit_Extraction_Rule__c )</formula>
        <name>CredNote Jitterbit Extraction Rule Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JVCO_UpdateCreditReason</fullName>
        <field>JVCO_Credit_Reason__c</field>
        <literalValue>Surcharge Cancelled</literalValue>
        <name>JVCO_UpdateCreditReason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RepDeptUpdate</fullName>
        <field>SalesRep_Department__c</field>
        <formula>JVCO_Sales_Rep_Department__c</formula>
        <name>RepDeptUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RepManagerUpdate</fullName>
        <field>SalesRep_Manager__c</field>
        <formula>JVCO_Sales_Rep_Manager__c</formula>
        <name>RepManagerUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RepProfileUpdate</fullName>
        <field>SalesRep_Profile__c</field>
        <formula>JVCO_Sales_Rep_Profile__c</formula>
        <name>RepProfileUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RepRoleUpdate</fullName>
        <field>SalesRep_Role__c</field>
        <formula>JVCO_Sales_Rep_Role__c</formula>
        <name>RepRoleUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates> 
    <rules>
        <fullName>CredNote_Jitterbit_Extraction_Rule_Update</fullName>
        <actions>
            <name>CredNote_Jitterbit_Extraction_Rule_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the value of Jitterbit Extraction Rule Text whenever the Jitterbit Extraction Rule is updated.</description>
        <formula>ISCHANGED( Jitterbit_Extraction_Rule__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JVCO_SurchargeCredNote</fullName>
        <actions>
            <name>JVCO_UpdateCreditReason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks if the Credit Note's Invoice is Surchage</description>
        <formula>c2g__Invoice__r.JVCO_Invoice_Type__c = 'Surcharge'</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sales Rep Rule - Sales Credit Note</fullName>
        <actions>
            <name>RepDeptUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RepManagerUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RepProfileUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RepRoleUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3 OR 4) AND 5</booleanFilter>
        <criteriaItems>
            <field>c2g__codaCreditNote__c.SalesRep_Department__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>c2g__codaCreditNote__c.SalesRep_Manager__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>c2g__codaCreditNote__c.SalesRep_Profile__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>c2g__codaCreditNote__c.SalesRep_Role__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>c2g__codaCreditNote__c.JVCO_Sales_Rep__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
