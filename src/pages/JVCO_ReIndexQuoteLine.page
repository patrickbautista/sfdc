<!--
	Name:            JVCO_ReIndexQuoteLine.page 
   	Description:     Page to ReIndex Quote Lines in a Quote

   Date             Version		Author                            	Summary of Changes 
   19-Jul-2018		 0.1		Accenture-reymark.j.l.arlos 		Initial version of the code GREEN-32595
   02-Aug-2018		 0.2		Accenture-reymark.j.l.arlos 		Updated to removed unused lines
-->
<apex:page showHeader="true" sidebar="false" standardController="SBQQ__Quote__c" extensions="JVCO_ReIndexQuoteLineController">
	<apex:form id="mainFrm">
		<apex:sectionHeader title="Quote" subtitle="{!quoteNumber}" />
		<apex:pageBlock title="Quote Lines">
			<div align="Center" style="margin: 10px">
				<apex:outputPanel >
				<apex:actionStatus id="disablebtn1">
                    <apex:facet name="stop">
                        <apex:outputPanel >
                            <apex:commandButton action="{!reIndex}" value="Re-Index" status="disablebtn1" disabled="false" reRender="mainFrm" rendered="{!renderReIndex}"/>
                            <apex:commandButton action="{!returnToQuote}" value="Return to Quote"/>
                        </apex:outputPanel>
                    </apex:facet>

                    <apex:facet name="start">
                        <apex:outputPanel >
                            <apex:commandButton action="{!reIndex}" value="Re-Index" status="disablebtn1" disabled="true" rendered="{!renderReIndex}"/>
                            <apex:commandButton action="{!returnToQuote}" value="Return to Quote" disabled="true" />
                        </apex:outputPanel>
                    </apex:facet>
                </apex:actionStatus>
            	</apex:outputPanel>
            </div>
			<apex:pageBlockSection id="quoteLineTable" collapsible="false" columns="1">
				<apex:pageMessages id="statusPageMessages" escape="false"></apex:pageMessages>
				<apex:pageBlockTable value="{!lstQuoteLineController}" var="obj" rendered="{!renderTable}">
					<apex:column headerValue="Group Name">
						<apex:outputLink value="/{!obj.quoteLineGroupId}" target="_blank">
							{!obj.quoteLineGroupName}
						</apex:outputLink>
					</apex:column>
					<apex:column value="{!obj.numberCtr}" headerValue="Number"/>
					<apex:column headerValue="Product Name">
						<apex:outputLink value="/{!obj.quoteLineId}" target="_blank">
							{!obj.productName}
						</apex:outputLink>
					</apex:column>
					<apex:column value="{!obj.productCode}" headerValue="Product Code"/>
					<apex:column headerValue="Required by Product Name">
						<apex:outputLink value="/{!obj.requiredbyId}" target="_blank">
							{!obj.requiredByProductName}
						</apex:outputLink>
					</apex:column>
				</apex:pageBlockTable>
			</apex:pageBlockSection>
		</apex:pageBlock>
		<apex:outputLabel value="Note: Only use this functionality if the arrangement of Quote Lines is Incorrect" style="font-style: italic; margin: 15px" />
	</apex:form>
</apex:page>
<!-- rendered="{!renderReIndex}"/> rendered="{!!renderNoQuoteLines}" -->