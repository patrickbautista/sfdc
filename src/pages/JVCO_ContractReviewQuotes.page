<apex:page standardController="Account" extensions="JVCO_ContractReviewQuotesExtension" action="{!jobCheck}">
    <script>
    function manualGoBack() {
        goBackToRecord() ;
    }
    </script>
    <apex:form id="f1">
        <apex:actionFunction name="goBackToRecord" action="{!backToRecord}"/>
        
        <apex:pageBlock >  
            <apex:pageBlockSection columns="1" collapsible="false">
                <apex:outputPanel rendered="{!!isContinued && isProcessing}">
                    <div align="center">
                        <apex:commandButton value="  Cancel  " action="{!backToRecord}"/>
                    </div>
                    <br/>
                    <apex:pageMessage severity="info" strength="3">
                        This process is already in progress, please ensure the job has completed before resubmitting.
                    </apex:pageMessage>
                </apex:outputPanel>

                <apex:pageMessages id="statusPageMessages" escape="false"></apex:pageMessages>

                <apex:outputPanel rendered="{!!venueSummarySent && !isProcessing && !queueableRunning && canContinue && kaUsageSummarySent}">              
                    <apex:pageMessage severity="warning" strength="3">
                        The latest Venue Summary document hasn't been generated. Please generate the latest Venue Summary Document first before contracting the review quotes.
                    </apex:pageMessage>
                </apex:outputPanel>

                <apex:outputPanel id="firstmessage">
                    <apex:facet name="header"><span style="font-size: 15px;">Number of Quotes requiring Recalculation: {!uncalculatedQuotesCount}</span></apex:facet>
                    
                    
                    <apex:outputPanel rendered="{!!isContinued && !isProcessing && !queueableRunning && canContinue && kaUsageSummarySent}">
                        <div align="center">
                            <apex:outputPanel style="width: 45%; float:left">
                                <apex:pageBlockTable value="{!uncalculatedQuotesList}" var="quote" id="theTable" rowClasses="odd,even" styleClass="tableClass">
                                    <apex:column >
                                        <apex:facet name="header">Name</apex:facet>
                                        <apex:outputLink value="/{!quote.Id}" target="_blank">{!quote.Name}</apex:outputLink>
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">Type</apex:facet>
                                        <apex:outputText value="{!quote.SBQQ__Type__c}"/>
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">Last Recalculated Time</apex:facet>
                                        <apex:outputField value="{!quote.JVCO_Salesforce_Last_Recalculated_Time__c}"/>
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">Start Date</apex:facet>
                                        
                                        <apex:outputField value="{!quote.Start_Date__c}"/>
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">End Date</apex:facet>
                                        
                                        <apex:outputField value="{!quote.End_Date__c}"/>
                                    </apex:column>
                                </apex:pageBlockTable>
                            </apex:outputPanel>
                            <apex:outputPanel rendered="{!uncalculatedQuotesCount > 0}" style="width: 50%; float:right; margin-left: 5%;">
                                <div style="border: 1px solid black; border-radius: 5px; padding: 10px; text-align: left;">
                                    <span style="font-weight: bold;">You may not contract the quotes whilst the quotes are recalculating:</span><br/>
                                    <br/>
                                    1. If you have recently submitted quotes for recalculating, please check back here in a few minutes.<br/>
                                    2. If you have not recalculated the quotes, please return to the&nbsp;<apex:outputLink value="/{!accountRecord.Id}">Licence Account</apex:outputLink> and recalculate.<br/>
                                    3. If quotes continue to not recalculate, please open those quotes from the list in the Quote Line Editor to identify any issues preventing the quote repricing.
                                </div>
                            </apex:outputPanel>
                            <apex:outputPanel rendered="{!uncalculatedQuotesCount = 0}" style="width: 50%; float:right; margin-left: 5%;">
                                <div style="border: 1px solid black; border-radius: 5px; padding: 10px; text-align: left;">
                                    Quotes have been recalculated and are ready to Contract. Pressing continue will contract {!numberOfRecords} quotes, do you wish to continue?
                                </div>
                            </apex:outputPanel>
                            <table width="100%"><tr>&nbsp;</tr></table> 
                            <apex:outputPanel rendered="{!!tcAccepted}" style="width: 50%; float:right; margin-left: 5%;">
                                <div style="border: 1px solid black; border-radius: 5px; padding: 10px; text-align: left;">
                                    <table style="float: left" border="0" cellpadding="8" cellspacing="0" width="100%">        
                                    <tr >
                                        <td width="50%">
                                        <b><span style="font-size: 11pt;">{!$Label.TermsAndConditionHeader}</span></b><br/>
                                            <ul>
                                                <li>{!$Label.TermsAndConditionText1}</li>
                                                <li>{!$Label.TermsAndConditionText2}</li>
                                                <li>{!$Label.TermsAndConditionText3}</li>
                                                <li>{!$Label.TermsAndConditionText4}</li>
                                            </ul>
                                        </td>
                                    </tr>                       

                                    </table>
                                    <table width="100%"><tr>&nbsp;</tr></table>
                                    <center>
                                        <b>Terms &amp; Conditions Accepted:&nbsp;&nbsp;&nbsp;</b>
                                        <apex:selectList size="1" value="{!selectedTC}"> 
                                            <apex:selectOptions value="{!selectedTCfields}"/>  
                                        </apex:selectList>
                                    </center>
                                </div>
                            </apex:outputPanel>
                            <br/>
                        </div>
                    </apex:outputPanel>
                </apex:outputPanel>          
                <apex:outputPanel id="secondmessage">
                    <apex:outputPanel rendered="{!isContinued}">
                        <apex:actionPoller action="{!backToRecord}" interval="5"/>
                        <div align="center">
                            <apex:pageMessage severity="confirm" strength="3">
                                Job has been created. Click <a href="#" onclick="manualGoBack();">here</a> if the page does not automatically redirect you back to the record.
                            </apex:pageMessage>
                        </div>
                    </apex:outputPanel>
                </apex:outputPanel>
                
                
                <apex:outputPanel id="pagebuttons">
                    <div align="center">
                        <apex:outputPanel rendered="{!!isContinued && !isProcessing && !queueableRunning && canContinue && kaUsageSummarySent}">
                            <apex:actionStatus id="status">
                                <apex:facet name="stop">
                                    <apex:outputPanel rendered="{!(uncalculatedQuotesCount > 0) || (numberOfRecords = 0)}">
                                        <apex:commandButton action="{!queueContractReviewQuotes}" status="status"
                                                            value="  Continue  " disabled="true" rerender="firstmessage,secondmessage,pagebuttons,statusPageMessages"/>&nbsp; &nbsp;
                                        <apex:commandButton value="  Cancel  " action="{!backToRecord}"/>
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!AND((uncalculatedQuotesCount = 0), (numberOfRecords > 0))}">
                                        <apex:commandButton action="{!queueContractReviewQuotes}" status="status" value="  Continue  "
                                                            disabled="false" rerender="firstmessage,secondmessage,pagebuttons,statusPageMessages"/>&nbsp; &nbsp;   
                                        <apex:commandButton value="  Cancel  " action="{!backToRecord}"/>
                                    </apex:outputPanel>
                                </apex:facet> 
                                <apex:facet name="start">
                                    <apex:outputPanel >
                                        <apex:commandButton action="{!queueContractReviewQuotes}" status="status" value="Processing..." disabled="true"/>&nbsp; &nbsp;
                                        <apex:commandButton value="  Cancel  " action="{!backToRecord}"/>
                                    </apex:outputPanel>
                                </apex:facet>
                            </apex:actionStatus>
                        </apex:outputPanel>
                    </div>
                </apex:outputPanel>
                
            </apex:pageBlockSection>
            
        </apex:pageBlock>
    </apex:form>
     <c:JVCO_ApexJobProgressBar myVar="{!apexJobId}" rendered="{!isProcessing}"/>
</apex:page>