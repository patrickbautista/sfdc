<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>The line items on the payable invoice, such as product, unit price, quantity and line total.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>c2g__DateFrom__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Date From</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>c2g__DateTo__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Date To</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>c2g__Dimension1__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Dimension 1</label>
        <referenceTo>c2g__codaDimension1__c</referenceTo>
        <relationshipLabel>Payable Invoice Line Items</relationshipLabel>
        <relationshipName>PurchaseInvoiceLineItems</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__Dimension2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Dimension 2</label>
        <referenceTo>c2g__codaDimension2__c</referenceTo>
        <relationshipLabel>Payable Invoice Line Items</relationshipLabel>
        <relationshipName>PurchaseInvoiceLineItems</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__EditTaxValue__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Edit Tax Value</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>c2g__InputVATCode__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Input Tax Code</label>
        <referenceTo>c2g__codaTaxCode__c</referenceTo>
        <relationshipLabel>Payable Invoice Line Items (Input VAT/GST Code)</relationshipLabel>
        <relationshipName>PurchaseInvoiceLineItemsInput</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__LineDescription__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Line Description</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>c2g__LineNumber__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Line Number</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The product name.</description>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Payable Invoice Line Items</relationshipLabel>
        <relationshipName>PurchaseInvoiceLineItems</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__PurchaseInvoice__c</fullName>
        <deprecated>false</deprecated>
        <description>The auto-generated payable invoice number.</description>
        <externalId>false</externalId>
        <label>Payable Invoice Number</label>
        <referenceTo>c2g__codaPurchaseInvoice__c</referenceTo>
        <relationshipLabel>Payable Invoice Line Items</relationshipLabel>
        <relationshipName>PurchaseInvoiceLineItems</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>c2g__Quantity__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <description>The quantity of product bought.</description>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>18</precision>
        <required>true</required>
        <scale>6</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__TaxRate1__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Input Tax Rate</label>
        <precision>7</precision>
        <required>false</required>
        <scale>3</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__TaxValue1__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Input Tax Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__UnitPrice__c</fullName>
        <deprecated>false</deprecated>
        <description>The unit price for this product.</description>
        <externalId>false</externalId>
        <label>Unit Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>9</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Payable Invoice Line Item</label>
    <nameField>
        <displayFormat>{0}</displayFormat>
        <label>Payable Invoice Line Item ID</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Payable Invoice Line Items</pluralLabel>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
