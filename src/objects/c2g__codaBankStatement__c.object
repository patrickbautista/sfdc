<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>c2g__codahelpbankstatement</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Stores imported bank statement master details.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>JVCO_Bank_Statement_Queueable_Completed__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Bank Statement Queueable Completed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
	  <fullName>JVCO_Bank_Transfer__c</fullName>
        <defaultValue>false</defaultValue>
	   <externalId>false</externalId>
        <label>Bank Transfer</label>
	   <trackTrending>false</trackTrending>
         <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>JVCO_Bank_to_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Account look up</description>
        <externalId>false</externalId>
        <inlineHelpText>Account look up</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Bank Statements</relationshipLabel>
        <relationshipName>Bank_Statements</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>JVCO_Percentage_Complete__c</fullName>
        <externalId>false</externalId>
        <formula>JVCO_Total_Cash_Entries_Processed__c / JVCO_Total_Bank_Statement_Lines__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Percentage Complete</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>JVCO_Progress_Bar__c</fullName>
        <externalId>false</externalId>
        <formula>IMAGE(&apos;/resource/blng__blng_slds/assets/images/InvoiceRun_ProgressBarBorder.png&apos;, &apos;Progress Bar&apos;, 10, ( JVCO_Percentage_Complete__c * 100)) &amp;
IMAGE(&apos;/resource/blng__blng_slds/assets/images/InvoiceRun_ProgressBarFilled.png&apos;, &apos;Progress Border&apos;, 10, (100 - JVCO_Percentage_Complete__c * 100)) &amp;
&apos; &apos; &amp;
TEXT( ROUND(JVCO_Percentage_Complete__c* 100,2)) &amp;
&apos;%&apos;</formula>
        <label>Progress Bar</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>JVCO_Status_Indicator__c</fullName>
        <externalId>false</externalId>
        <formula>IF (
OR (
ISPICKVAL(JVCO_Status__c, &apos;Started&apos;),
ISPICKVAL(JVCO_Status__c, &apos;In Progress&apos;)
),
IMAGE(&apos;/resource/blng__blng_slds/assets/images/InvoiceRun_StatusOrange.png&apos;, &apos;In Progress&apos;, 20, 20),
IF (
ISPICKVAL(JVCO_Status__c, &apos;Completed&apos;),
IMAGE(&apos;/resource/blng__blng_slds/assets/images/InvoiceRun_StatusGreen.png&apos;, &apos;In Progress&apos;, 20, 20),
IF(
ISPICKVAL(JVCO_Status__c, &apos;Completed with Failure&apos;),
IMAGE(&apos;/resource/blng__blng_slds/assets/images/InvoiceRun_StatusRed.png&apos;, &apos;In Progress&apos;, 20, 20),
&apos;&apos;
)
)
)</formula>
        <label>Status Indicator</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>JVCO_Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Started</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>In Progress</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Completed</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Completed with Failure</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>JVCO_Total_Bank_Statement_Lines__c</fullName>
        <externalId>false</externalId>
        <label>Total Bank Statement Lines</label>
        <summaryFilterItems>
            <field>c2g__codaBankStatementLineItem__c.JVCO_Exclude__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryForeignKey>c2g__codaBankStatementLineItem__c.c2g__BankStatement__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>JVCO_Total_Cash_Entries_Processed__c</fullName>
        <externalId>false</externalId>
        <label>Total Cash Entries Processed</label>
        <summaryFilterItems>
            <field>c2g__codaBankStatementLineItem__c.JVCO_Cash_Entry_Processed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>c2g__codaBankStatementLineItem__c.c2g__BankStatement__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>c2g__BankAccount__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Reference to the related bank account. This bank account must have a valid bank statement definition associated with it.</inlineHelpText>
        <label>Bank Account</label>
        <referenceTo>c2g__codaBankAccount__c</referenceTo>
        <relationshipLabel>Bank Statements</relationshipLabel>
        <relationshipName>BankStatements</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__BankReconciliation__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Reference to a related bank reconciliation, if there is one.</inlineHelpText>
        <label>Bank Reconciliation</label>
        <referenceTo>c2g__codaBankReconciliation__c</referenceTo>
        <relationshipLabel>Bank Statements</relationshipLabel>
        <relationshipName>BankStatements</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__ClosingBalance__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Closing balance, as shown on the bank statement, or calculated by the system.</inlineHelpText>
        <label>Closing Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__ExternalId__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>32</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>c2g__OpeningBalance__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Opening balance, taken from the closing balance on the previous (most recent) bank statement, but can be amended.</inlineHelpText>
        <label>Opening Balance</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__OwnerCompany__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Company that owns this record</inlineHelpText>
        <label>Company</label>
        <referenceTo>c2g__codaCompany__c</referenceTo>
        <relationshipLabel>Bank Statements</relationshipLabel>
        <relationshipName>BankStatements</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__Reference__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Reference taken from the bank statement line, such as a check number or payee name.</inlineHelpText>
        <label>Reference</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__StatementDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Date on the bank statement header.</inlineHelpText>
        <label>Statement Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>c2g__Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Status of the bank statement.</inlineHelpText>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Imported</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>In progress</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Reconciled</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Committing</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>c2g__UnitOfWork__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit of Work</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Bank Statement</label>
    <listViews>
        <fullName>Bank_Statement_Imported</fullName>
        <columns>NAME</columns>
        <columns>c2g__OwnerCompany__c</columns>
        <columns>c2g__BankAccount__c</columns>
        <columns>c2g__StatementDate__c</columns>
        <columns>c2g__Reference__c</columns>
        <columns>c2g__OpeningBalance__c</columns>
        <columns>c2g__ClosingBalance__c</columns>
        <columns>c2g__BankReconciliation__c</columns>
        <columns>c2g__Status__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>c2g__Status__c</field>
            <operation>equals</operation>
            <value>Imported</value>
        </filters>
        <label>Bank Statement Imported</label>
    </listViews>
    <listViews>
        <fullName>Bank_Statement_In_Progress</fullName>
        <columns>NAME</columns>
        <columns>c2g__OwnerCompany__c</columns>
        <columns>c2g__BankAccount__c</columns>
        <columns>c2g__StatementDate__c</columns>
        <columns>c2g__Reference__c</columns>
        <columns>c2g__OpeningBalance__c</columns>
        <columns>c2g__ClosingBalance__c</columns>
        <columns>c2g__BankReconciliation__c</columns>
        <columns>c2g__Status__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>c2g__Status__c</field>
            <operation>equals</operation>
            <value>In progress</value>
        </filters>
        <label>Bank Statement In Progress</label>
    </listViews>
    <listViews>
        <fullName>Bank_Statement_Reconciled</fullName>
        <columns>NAME</columns>
        <columns>c2g__OwnerCompany__c</columns>
        <columns>c2g__BankAccount__c</columns>
        <columns>c2g__StatementDate__c</columns>
        <columns>c2g__Reference__c</columns>
        <columns>c2g__OpeningBalance__c</columns>
        <columns>c2g__ClosingBalance__c</columns>
        <columns>c2g__BankReconciliation__c</columns>
        <columns>c2g__Status__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>c2g__Status__c</field>
            <operation>equals</operation>
            <value>Reconciled</value>
        </filters>
        <label>Bank Statement Reconciled</label>
    </listViews>
    <listViews>
        <fullName>c2g__All</fullName>
        <columns>NAME</columns>
        <columns>c2g__OwnerCompany__c</columns>
        <columns>c2g__BankAccount__c</columns>
        <columns>c2g__StatementDate__c</columns>
        <columns>c2g__Reference__c</columns>
        <columns>c2g__OpeningBalance__c</columns>
        <columns>c2g__ClosingBalance__c</columns>
        <columns>c2g__BankReconciliation__c</columns>
        <columns>c2g__Status__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>BS{0000}</displayFormat>
        <label>Bank Statement Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Bank Statements</pluralLabel>
    <recordTypes>
        <fullName>Bank_Transfer</fullName>
        <active>true</active>
        <label>Bank Transfer</label>
        <picklistValues>
            <picklist>JVCO_Status__c</picklist>
            <values>
                <fullName>Completed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Completed with Failure</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>In Progress</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Started</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>c2g__Status__c</picklist>
            <values>
                <fullName>Committing</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Imported</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>In progress</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Reconciled</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Non_Bank_Transfer</fullName>
        <active>true</active>
        <label>Non-Bank Transfer</label>
        <picklistValues>
            <picklist>JVCO_Status__c</picklist>
            <values>
                <fullName>Completed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Completed with Failure</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>In Progress</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Started</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>c2g__Status__c</picklist>
            <values>
                <fullName>Committing</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Imported</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>In progress</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Reconciled</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>c2g__BankAccount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__StatementDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__Reference__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__OpeningBalance__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__ClosingBalance__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__OwnerCompany__c</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <webLinks>
        <fullName>Import</fullName>
        <availability>online</availability>
        <description>The button Import is available while creation a bank statement and allows the user to import the Bank Statement items that will be created as bank statement line items.
During the import, the system will also look up the Account and invoice number for each bank statement line items and generate the appropriate cash entries</description>
        <displayType>button</displayType>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>page</linkType>
        <masterLabel>Import</masterLabel>
        <openType>newWindow</openType>
        <page>c2g__codabankstatementnew</page>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
    </webLinks>
    <webLinks>
        <fullName>c2g__Reconcile</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Reconcile</masterLabel>
        <openType>sidebar</openType>
        <page>c2g__codabankstatementautorecconfirm</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
