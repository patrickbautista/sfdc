<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Active_Direct_Debit_Count__c</fullName>
        <externalId>false</externalId>
        <label>Active Direct Debit Count</label>
        <summaryForeignKey>SMP_DirectDebit_GroupInvoice__c.Invoice_Group__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>DirectDebitInvoiceGroup__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>SMP_DirectDebit_GroupInvoice</label>
        <referenceTo>SMP_DirectDebit_GroupInvoice__c</referenceTo>
        <relationshipLabel>Invoice Groups</relationshipLabel>
        <relationshipName>Invoice_Groups</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Income_Card_Payment__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Income Card Payment</label>
        <referenceTo>Income_Card_Payment__c</referenceTo>
        <relationshipLabel>Invoice Groups</relationshipLabel>
        <relationshipName>Invoice_Groups</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>JVCO_Migrated_Invoice__c</fullName>
        <externalId>false</externalId>
        <label>Migrated Invoice</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CC_SINs__c</fullName>
        <externalId>false</externalId>
        <label>CC SINs</label>
        <length>1300</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>JVCO_Paper_Debit__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Specifies whether direct debit payment is paperless or not</description>
        <externalId>false</externalId>
        <label>Paper Debit?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>JVCO_Payment_Agreement__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup on Invoice Group to identify related Payment Agreement uploaded by Data Migration</description>
        <externalId>false</externalId>
        <label>Payment Agreement</label>
        <referenceTo>PAYREC2__Payment_Agreement__c</referenceTo> 
        <relationshipLabel>Invoice Groups</relationshipLabel>
        <relationshipName>Invoice_Groups</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>JVCO_Payonomy_Payment_Agreement_Ext_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>For data migration to pass through which payonomy payment agreement relates to an invoice</description>
        <externalId>true</externalId>
        <label>Payonomy Payment Agreement Ext Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>JVCO_Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Active</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Retired</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>JVCO_Total_Amount__c</fullName>
        <description>Total Amount of related Sales Invoices</description>
        <externalId>false</externalId>
        <label>Total Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>JVCO_invNumbers__c</fullName>
        <description>invoice numbers selected</description>
        <externalId>false</externalId>
        <inlineHelpText>Invoice numbers selected</inlineHelpText>
        <label>invNumbers</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>JVCO_invoiceNumbers__c</fullName>
        <description>invoice numbers</description>
        <externalId>false</externalId>
        <label>invoiceNumbers</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Invoice Group</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>JVCO_invNumbers__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>GIN-{000000}</displayFormat>
        <label>Invoice Group Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Invoice Groups</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
</CustomObject>
