<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>c2g__codahelpbankstatement</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Stores the line item details of an imported bank statement.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fieldSets>
        <fullName>c2g__BankRecColumns</fullName>
        <availableFields>
            <field>c2g__Balance__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>c2g__ExternalId__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>c2g__LineNumber__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>c2g__BankStatement__r.Name</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>Name</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>c2g__BankStatement__r.c2g__Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <description>In Bank Rec to customize the available columns in the Transaction pane.</description>
        <displayedFields>
            <field>c2g__Description__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Bank Reconciliation Columns</label>
    </fieldSets>
    <fields>
        <fullName>JVCO_Account_Balance__c</fullName>
        <externalId>false</externalId>
        <formula>JVCO_Account__r.ffps_accbal__Account_Balance__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>JVCO_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Account look up</description>
        <externalId>false</externalId>
        <inlineHelpText>Account look up</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Bank Statement Line Items</relationshipLabel>
        <relationshipName>Bank_Statement_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>JVCO_Bank_Account__c</fullName>
        <description>Reference to the related bank account. This bank account must have a valid bank statement definition associated with it.</description>
        <externalId>false</externalId>
        <inlineHelpText>Reference to the related bank account. This bank account must have a valid bank statement definition associated with it.</inlineHelpText>
        <label>Bank Account</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>JVCO_Cash_Entry_Line_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Refer to the cash entry line item</description>
        <externalId>false</externalId>
        <inlineHelpText>Refer to the cash entry line item</inlineHelpText>
        <label>Cash Entry Line Item</label>
        <referenceTo>c2g__codaCashEntryLineItem__c</referenceTo>
        <relationshipLabel>Bank Statement Line Items</relationshipLabel>
        <relationshipName>Bank_Statement_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>JVCO_Cash_Entry_Processed__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Cash Entry Processed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>JVCO_Cash_Entry__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Refer to the cash entry</description>
        <externalId>false</externalId>
        <inlineHelpText>Refer to the cash entry</inlineHelpText>
        <label>Cash Entry</label>
        <referenceTo>c2g__codaCashEntry__c</referenceTo>
        <relationshipLabel>Bank Statement Line Items</relationshipLabel>
        <relationshipName>Bank_Statement_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>JVCO_Exclude__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Exclude</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>JVCO_Processed__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Flagged if record has been processed</description>
        <externalId>false</externalId>
        <inlineHelpText>Flagged if record has been processed</inlineHelpText>
        <label>Processed</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>JVCO_SalesInvoice__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Sales Invoice Lookup</description>
        <externalId>false</externalId>
        <inlineHelpText>Sales Invoice Lookup</inlineHelpText>
        <label>Sales Invoice</label>
        <referenceTo>c2g__codaInvoice__c</referenceTo>
        <relationshipLabel>Bank Statement Line Items</relationshipLabel>
        <relationshipName>Bank_Statement_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>JVCO_Status__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(JVCO_Cash_Entry__r.c2g__Status__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Value of payment or receipt</inlineHelpText>
        <label>Amount</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__Balance__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Balance after payment or receipt</inlineHelpText>
        <label>Balance</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__BankStatement__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Reference to the related bank statement header details.</inlineHelpText>
        <label>Bank Statement</label>
        <referenceTo>c2g__codaBankStatement__c</referenceTo>
        <relationshipLabel>Bank Statement Line Items</relationshipLabel>
        <relationshipName>BankStatementLineItems</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>c2g__Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Date on the bank statement line</inlineHelpText>
        <label>Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>c2g__Description__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Description on the bank statement line. Not used as automatic reconciliation selection criteria.</inlineHelpText>
        <label>Description</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__ExternalId__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>32</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>c2g__LineNumber__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Number of the bank statement line</inlineHelpText>
        <label>Line Number</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__LineStatus__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Status of the bank statement line item</inlineHelpText>
        <label>Line Status</label>
        <picklist>
            <picklistValues>
                <fullName>Imported</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>In Progress</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Reconciled</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>c2g__OwnerCompany__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Company that owns this record</inlineHelpText>
        <label>Company</label>
        <referenceTo>c2g__codaCompany__c</referenceTo>
        <relationshipLabel>Bank Statement Line Items</relationshipLabel>
        <relationshipName>BankStatementLineItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__Reference__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Reference taken from the bank statement line, such as a check number or payee name.</inlineHelpText>
        <label>Reference</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__UnitOfWork__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit of Work</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Bank Statement Line Item</label>
    <nameField>
        <displayFormat>BSL{0000}</displayFormat>
        <label>Bank Statement Line Item Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Bank Statement Line Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <webLinks>
        <fullName>JVCO_PostCashEntries</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Post Cash Entries</masterLabel>
        <openType>sidebar</openType>
        <page>JVCO_CashEntriesPosting</page>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
    </webLinks>
</CustomObject>
