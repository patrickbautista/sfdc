/* ----------------------------------------------------------------------------------------------
   Name: JVCO_ReminderTriggerHandler.cls 
   Description: 

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  12-Jul-2018   0.1         hazel.jurna.m.limot Intial creation 
  ----------------------------------------------------------------------------------------------- */
public class JVCO_ReminderTriggerHandler
{

     public static void afterInsert(List<ffps_custRem__Reminder__c> reminderList){
          updateAccountFinalDemand(reminderList);
     }
     
    /*----------------------------------------------------------------------------------------------
    Author: hazel.jurna.m.limot
    Company: Accenture
    Description: 
    Inputs: List<ffps_custRem__Reminder__c>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    12-Jul-2018 hazel.jurna.m.limot  Initial version of function    
    ----------------------------------------------------------------------------------------------- */
     private static void updateAccountFinalDemand(List<ffps_custRem__Reminder__c> reminderList){ 
     
        Set<Id> accountIds = new Set<Id>();
        Map<Id, List<c2g__codaInvoice__c>> acctToSinvMap = new Map<Id, List<c2g__codaInvoice__c>>();
        for(ffps_custRem__Reminder__c reminder: reminderList){
            accountIds.add(reminder.ffps_custRem__Account__c);
        }
        
        Map<Id, Account> acctMap = new Map<Id, Account>([SELECT Id, JVCO_Final_Demand_Identifier__c, JVCO_Final_Demand_Counter__c 
                                                        FROM Account WHERE Id IN: accountIds]);
                                                        
        for(c2g__codaInvoice__c invoice: [SELECT Id, c2g__PaymentStatus__c, 
                                            c2g__Account__c FROM c2g__codaInvoice__c WHERE c2g__Account__c IN: accountIds AND c2g__PaymentStatus__c = 'Unpaid'
                                            AND (ffps_custRem__Last_Reminder_Severity_Level__c IN (5,6))]){
            if(acctToSinvMap.containskey(invoice.c2g__Account__c)){
                acctToSinvMap.get(invoice.c2g__Account__c).add(invoice);
            }else{
                List<c2g__codaInvoice__c> invoices = new List<c2g__codaInvoice__c>();
                invoices.add(invoice);
                acctToSinvMap.put(invoice.c2g__Account__c, invoices);
            }
        }
        
        List<Account> accToUpdate = new List<Account>();
        
        for(ffps_custRem__Reminder__c reminder: reminderList){
            Integer salesInvoiceCtr = 0;
            
            if(acctToSinvMap.containskey(reminder.ffps_custRem__Account__c)){
                salesInvoiceCtr = acctToSinvMap.get(reminder.ffps_custRem__Account__c).size();
            }
            
            if(acctMap.containskey(reminder.ffps_custRem__Account__c)){
                if(salesInvoiceCtr > acctMap.get(reminder.ffps_custRem__Account__c).JVCO_Final_Demand_Counter__c){
                    //update account JVCO_Final_Demand_Identifier__c to false
                    acctMap.get(reminder.ffps_custRem__Account__c).JVCO_Final_Demand_Identifier__c = false;
                    accToUpdate.add(acctMap.get(reminder.ffps_custRem__Account__c));
                }
            }
        }
        
        if(accToUpdate.size() > 0){
            try{
                Database.update(accToUpdate);
            }catch(Exception e){
                System.debug(e.getMessage());
            }
        }
     }
}