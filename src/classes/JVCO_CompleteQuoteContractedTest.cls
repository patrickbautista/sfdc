@isTest
private class JVCO_CompleteQuoteContractedTest
 {

  @testSetup
  public static void testSetup() { 

    //Test.startTest();

    Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
    insert pb;

    SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
    insert qProcess;

    Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
    insert acc;

    Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
    insert acc2;

    Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
    insert prod;

    blng__BillingRule__c br = JVCO_TestClassObjectBuilder.createBillingRule();
    insert br;

    SBQQ__PriceRule__c price = JVCO_TestClassObjectBuilder.createPriceRule();
    insert price;

    SBQQ__PriceCondition__c priceCon = JVCO_TestClassObjectBuilder.createPriceCondition(price.id);
    insert priceCon;

    List<SBQQ__PriceAction__c> priceAct = JVCO_TestClassObjectBuilder.createPriceAction(price.id);
    insert priceAct;

    SBQQ__ConfigurationAttribute__c configAttr =JVCO_TestClassObjectBuilder.createConfigurationAttribute(prod.id);
    insert configAttr;

    JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
    insert ven;

    JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
    aff.JVCO_End_Date__c = null;
    aff.JVCO_Closure_Reason__c = null;
    insert aff;

    Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
    insert opp;

    SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
    insert q;

    SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(acc2.id, prod.id);
    insert s;

    //Create Quote using quoteController
    //Apexpages.currentPage().getParameters().put('id',acc2.id);
    //Apexpages.StandardSetController std;

    //JVCO_QuoteCreationController quoteController = new JVCO_QuoteCreationController(std);
    //PageReference pageRef = quoteController.quoteCreate();

    //Opportunity opp = new Opportunity();
    //opp =  [SELECT  Id, AccountId, StageName FROM Opportunity Limit 1];
    //System.debug('@@OPPO - ' +  opp);
    //SBQQ__Quote__c q = [SELECT Id, SBQQ__Account__c, SBQQ__Opportunity2__c FROM SBQQ__Quote__c Limit 1];
    //End */
 
    SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c order by CreatedDate DESC limit 1];

    SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, prod.Id);

    Test.startTest();
    insert ql;

    q.Start_Date__c  = date.today();
    q.SBQQ__SubscriptionTerm__c  = 12;
    q.Recalculate__c = true;
    q.SBQQ__Status__c  = 'Approved';
    q.JVCO_Contact_Type__c = 'Email';
    q.Accept_TCs__c = 'yes';
    update q;

    opp.Probability = 100;
    opp.Amount = 10.00;
    opp.StageName = 'Draft';
    update opp;         


    Test.stopTest();

    List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
    settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_Opportunity', Value__c = 'Quote did not successfully complete because the \'Opportunity\' was not provided'));
    settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_ContactType', Value__c = 'Quote did not successfully complete because the \'Contact Type\' was not provided'));
    settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_Primary', Value__c = 'Quote did not successfully complete because the \'Primary\' checkbox was not ticked'));
    settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_StartDate', Value__c = 'Quote did not successfully complete because the \'Start Date\' was not provided'));
    settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_SubscriptionTerm', Value__c = 'Quote did not successfully complete because the \'Subscription Term\' was not provided'));
    settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'ThresholdAndStatusErrorMsg', Value__c = 'You cannot complete the quote if its credit is requiring an approval'));
    settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'UpdateOppAndQuoteFail', Value__c = 'The update did not push through because an error occurred. Details are as follows:'));
    settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'UpdateOppAndQuoteSuccess', Value__c = 'Quote Successfully Completed'));
    settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_AcceptT&Cs', Value__c = 'Quote did not successfully complete because the \'T&C\' was not checked'));
    
    insert settings;
  } 

  

  public static testMethod void testCompleteQuoteContracted(){

    SBQQ__Quote__c q = [select Id, JVCO_Contact_Type__c, Recalculate__c, SBQQ__Opportunity2__c, JVCO_QuoteComplete__c, SBQQ__Status__c from SBQQ__Quote__c limit 1];
    PageReference dumpRef;
    List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
    settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'AmountThreshold', Value__c = '1000.00'));

    

    insert settings;

    update q;

    SBQQ__Quote__c quoteCheck = [SELECT Id, JVCO_QuoteComplete__c, SBQQ__Status__c, SBQQ__NetAmount__c, SBQQ__Opportunity2__c, SBQQ__Opportunity2__r.StageName, SBQQ__Opportunity2__r.Probability FROM SBQQ__Quote__c WHERE Id = :q.Id];

    System.assertEquals(false, quoteCheck.JVCO_QuoteComplete__c);
    System.assertEquals('Approved', quoteCheck.SBQQ__Status__c);

    Opportunity oppCheck = [select Id, StageName, Probability, SBQQ__Contracted__c, SBQQ__PrimaryQuote__c from Opportunity where Id = :q.SBQQ__Opportunity2__c];

    System.assertEquals('Draft', oppCheck.StageName);
    System.assertEquals(100, oppCheck.Probability);
    System.assertEquals(false, oppCheck.SBQQ__Contracted__c);

    test.startTest();

    JVCO_CompleteQuoteContracted testJVCO = new JVCO_CompleteQuoteContracted(oppCheck, quoteCheck, quoteCheck);
    //JVCO_CompleteQuoteContracted testJVCO = new JVCO_CompleteQuoteContracted(quoteCheck.SBQQ__Opportunity2__c,quoteCheck);
    ID jobID = System.enqueueJob(testJVCO);

    test.stopTest();

  }

  public static testMethod void testCompleteQuoteContractedError(){

    SBQQ__Quote__c q = [select Id, JVCO_Contact_Type__c, Recalculate__c, SBQQ__Opportunity2__c, JVCO_QuoteComplete__c, SBQQ__Status__c from SBQQ__Quote__c limit 1];
    PageReference dumpRef;
    List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
    settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'AmountThreshold', Value__c = '1000.00'));

    insert settings;

    update q;

    SBQQ__Quote__c quoteCheck = [SELECT Id, JVCO_QuoteComplete__c, SBQQ__Status__c, SBQQ__NetAmount__c, SBQQ__Opportunity2__r.StageName, SBQQ__Opportunity2__r.Probability FROM SBQQ__Quote__c WHERE Id = :q.Id];

    System.assertEquals(false, quoteCheck.JVCO_QuoteComplete__c);
    System.assertEquals('Approved', quoteCheck.SBQQ__Status__c);

    Opportunity oppCheck = [select Id, StageName, Probability, SBQQ__Contracted__c,SBQQ__PrimaryQuote__c from Opportunity where Id = :q.SBQQ__Opportunity2__c];
    oppCheck.SBQQ__PrimaryQuote__c = null;
    update oppCheck;

    test.startTest();

    //JVCO_CompleteQuoteContracted testJVCO = new JVCO_CompleteQuoteContracted(oppCheck, quoteCheck);
    JVCO_CompleteQuoteContracted testJVCO = new JVCO_CompleteQuoteContracted(oppCheck, quoteCheck, quoteCheck);
    ID jobID = System.enqueueJob(testJVCO);

    test.stopTest();

  }

  public static testMethod void testCompleteQuoteContractedCatch(){

    SBQQ__Quote__c q = [select Id, JVCO_Contact_Type__c, Recalculate__c, SBQQ__Opportunity2__c, JVCO_QuoteComplete__c, SBQQ__Status__c from SBQQ__Quote__c limit 1];
    PageReference dumpRef;
    List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
    settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'AmountThreshold', Value__c = '1000.00'));

    insert settings;

    update q;

    SBQQ__Quote__c quoteCheck = [SELECT Id, JVCO_QuoteComplete__c, SBQQ__Status__c, SBQQ__NetAmount__c, SBQQ__Opportunity2__r.StageName, SBQQ__Opportunity2__r.Probability FROM SBQQ__Quote__c WHERE Id = :q.Id];

    System.assertEquals(false, quoteCheck.JVCO_QuoteComplete__c);
    System.assertEquals('Approved', quoteCheck.SBQQ__Status__c);

    Opportunity oppCheck = [select Id, StageName, Probability, SBQQ__Contracted__c,SBQQ__PrimaryQuote__c from Opportunity where Id = :q.SBQQ__Opportunity2__c];
    oppCheck.SBQQ__PrimaryQuote__c = null;
    update oppCheck;

    test.startTest();

    JVCO_CompleteQuoteContracted testJVCO = new JVCO_CompleteQuoteContracted(oppCheck, quoteCheck, quoteCheck);
    //JVCO_CompleteQuoteContracted testJVCO = new JVCO_CompleteQuoteContracted(null,quoteCheck);
    ID jobID = System.enqueueJob(testJVCO);

    test.stopTest();

  }
 
 
}