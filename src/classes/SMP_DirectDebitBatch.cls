public class SMP_DirectDebitBatch implements Database.AllowsCallouts, Database.Batchable<sObject>, Database.Stateful
{
    List<SMP_PaymentWizardController.paymentWrapper> paymentList {get; set;}
    List<c2g__codaInvoiceInstallmentLineItem__c> invoiceInstallmentList {get; set;}
    
    private final JVCO_General_Settings__c GeneralSettings = JVCO_General_Settings__c.getInstance();
    private final Decimal POSTCREDITNOTESCOPE = GeneralSettings != null ? GeneralSettings.JVCO_Post_Sales_Credit_Note_Scope__c : 20;
    private Set<Id> sInvIdStatefulSet = new Set<Id>();
    private Set<Id> cNoteIdStatefulSet = new Set<Id>();
    public Map<Id, Id> licAndCustAccIdMap = new Map<Id, Id>();
    public Boolean stopMatching;

    public SMP_DirectDebitBatch(){}
    public SMP_DirectDebitBatch(Set<Id> sInvIdStatefulSet, Set<Id> cNoteIdStatefulSet)
    {
        this.sInvIdStatefulSet = sInvIdStatefulSet;
        this.cNoteIdStatefulSet = cNoteIdStatefulSet;
    }

    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT Id, Name, 
                                         c2g__Account__c,
                                         c2g__Account__r.CreatedBy.Email,
                                         c2g__OutstandingValue__c,
                                         c2g__Account__r.JVCO_DD_Payee_Contact__c,
                                         JVCO_Outstanding_Amount_to_Process__c,
                                         JVCO_Sales_Rep__r.Email,JVCO_Sales_Rep__r.Name,
                                         c2g__Account__r.Name, CreatedDate, JVCO_Sales_Rep__r.Manager.Email,
                                         c2g__Account__r.JVCO_Customer_Account__c,
                                         JVCO_Payment_Method__c, JVCO_Invoice_Group__c
                                         FROM c2g__codaInvoice__c
                                         WHERE Id IN : sInvIdStatefulSet]);
    }

    public void execute(Database.BatchableContext bc, List<c2g__codaInvoice__c> scope)
    {   
        ddSetup(scope);
    }

    private void ddSetup(List<c2g__codaInvoice__c> sInvList){
        Set<string> salesInvoiceSet = new Set<String>();
        for(c2g__codaInvoice__c sInv : sInvList)
        {
            salesInvoiceSet.add(sInv.Id);
            if(!licAndCustAccIdMap.containsKey(sInv.c2g__Account__c) && sInv.c2g__Account__c != null && sInv.c2g__Account__r.JVCO_Customer_Account__c != null)
            {
                licAndCustAccIdMap.put(sInv.c2g__Account__c, sInv.c2g__Account__r.JVCO_Customer_Account__c);
            }
        }
        
        if(!salesInvoiceSet.isEmpty())
        {
            updateAndInsertDDLines(salesInvoiceSet);
        }
    }

    public static void updateAndInsertDDLines(Set<String> sInvIdSet){
        

        List<c2g__codaInvoice__c> postedSInvList = [SELECT Id, Name, c2g__Account__r.Name,
                                                    c2g__OutstandingValue__c, CreatedDate,
                                                    JVCO_Outstanding_Amount_to_Process__c,
                                                    JVCO_Sales_Rep__r.Email, JVCO_Sales_Rep__r.Name,
                                                    JVCO_Sales_Rep__r.Manager.Email, JVCO_Invoice_Group__c,
                                                    JVCO_Invoice_Group__r.JVCO_Total_Amount__c, c2g__Account__c,
                                                    JVCO_Payment_Method__c
                                                    FROM c2g__codaInvoice__c
                                                    WHERE Id IN :sInvIdSet
                                                    AND c2g__OutstandingValue__c > 0];

        Map<Id, List<c2g__codaInvoice__c>> accIdToSInvListMap = new Map<Id, List<c2g__codaInvoice__c>>();
        Map<Id, Double> accIdToSInvTotAmt = new Map<Id, Double>();
        for(c2g__codaInvoice__c sInv : postedSInvList)
        {   
            //Account to Sales Invoice List Map
            Id accId = sInv.c2g__Account__c;
            if(!accIdToSInvListMap.containsKey(accId))
            {
                accIdToSInvListMap.put(accId, new List<c2g__codaInvoice__c>{});
            }
            accIdToSInvListMap.get(accId).add(sInv);
            //Account to Sales Invoice Total Amount Map
            if(!accIdToSInvTotAmt.containsKey(sInv.c2g__Account__c))
            {
                accIdToSInvTotAmt.put(accId, 0);
            }
            accIdToSInvTotAmt.put(accId, accIdToSInvTotAmt.get(accId) + sInv.c2g__OutstandingValue__c);
        }
        
        //Smarterpay Added
        List<Income_Direct_Debit__c> directDebitList = null;
        if(!accIdToSInvListMap.isEmpty()){
            directDebitList = [SELECT Id, Name, DD_Next_Collection_Date__c, DD_Ongoing_Collection_Amount__c,
                            Account__r.JVCO_DD_Payee_Contact__c, Number_Of_Payments_Left__c, Amount_Left_To_Collect__c,
                            Number_Of_Payments__c, DD_Collection_Day__c, Account__r.AccountNumber, No_AUDDIS__c, DD_Last_Collected_Date__c, DD_Exception__c, DD_Status__c, DD_Collection_Period__c, Auto_Expire__c
                            FROM Income_Direct_Debit__c
                            WHERE Account__c IN: accIdToSInvListMap.keySet()
                            AND DD_Status__c != 'Cancelled'
                            AND DD_Status__c != 'Expired'
                            AND DD_Status__c != 'First Represent'
                            AND DD_Status__c != 'Second Represent'
                            AND DD_Status__c != 'Third Represent'
                            AND DD_Status__c != 'Cancelled by Payer'
                            AND DD_Status__c != 'Cancelled by Originator'];
        }

        //Smarterpay Added
        Map<Id, Income_Direct_Debit__c> accIdToIncomeDDMap = new Map<Id, Income_Direct_Debit__c>();
        Map<Id, String> ddMandateAccIdToPymtPlanMap = new Map<Id, String>();
        if(directDebitList != null){
            for(Income_Direct_Debit__c dd : directDebitList){
                accIdToIncomeDDMap.put(dd.Account__c, dd);
                ddMandateAccIdToPymtPlanMap.put(dd.Account__c, '');
            }
        }

        Map<Id, JVCO_Invoice_Group__c> accIdToIGMap = new Map<Id, JVCO_Invoice_Group__c>();
        for(c2g__codaInvoice__c salesInv : postedSInvList){
            JVCO_Invoice_Group__c invoiceGroup = new JVCO_Invoice_Group__c();
            invoiceGroup.Id = salesInv.JVCO_Invoice_Group__c;
            invoiceGroup.JVCO_Total_Amount__c = salesInv.JVCO_Invoice_Group__r.JVCO_Total_Amount__c;
            accIdToIGMap.put(salesInv.c2g__Account__c, invoiceGroup);
        }

        // for(Id accId : ddMandateAccIdToPymtPlanMap.keySet())
        // {   
        //     JVCO_Invoice_Group__c ig = new JVCO_Invoice_Group__c();
        //     ig.JVCO_Total_Amount__c = accIdToSInvTotAmt.get(accId);
        //     accIdToIGMap.put(accId, ig);
        // } 
        List<Income_Direct_Debit__c> failedDDList = new List<Income_Direct_Debit__c>();
        
        RenewalMonthLimit__c renewalCustomsetting = RenewalMonthLimit__c.getOrgDefaults();


        List<SMP_PaymentWizardController.ddPaymentWrapper> ddPaymentList = new List<SMP_PaymentWizardController.ddPaymentWrapper>();
        List<c2g__codaInvoiceInstallmentLineItem__c> invoiceInstallmentList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
        List<c2g__codaInvoice__c> salesInvoiceList = new List<c2g__codaInvoice__c>();
        
        for(Id accId : accIdToIGMap.keySet()){
            if(renewalCustomsetting.Amount__c == null)
            {
                throw new RenewalMonthLimitException ('Renewal Month Limit is invalid');
            }
            JVCO_Invoice_Group__c ig = accIdToIGMap.get(accId);
            Double totalAmount = ig.JVCO_Total_Amount__c;
            if(accIdToIncomeDDMap.get(accId).No_AUDDIS__c == null || accIdToIncomeDDMap.get(accId).No_AUDDIS__c == false){
                SMP_PaymentWizardController.ddPaymentWrapper paymentWrapper = new SMP_PaymentWizardController.ddPaymentWrapper();
                if(accIdToIncomeDDMap.get(accId).DD_Status__c == 'New Instruction'){
                    paymentWrapper = calculatePaymentAmounts(accIdToIncomeDDMap.get(accId), totalAmount, ig, accIdToSInvListMap.get(accId), invoiceInstallmentList, ddPaymentList, false, true);
                }else{
                    accIdToIncomeDDMap.get(accId).DD_Status__c = 'Ongoing Collection';
                    paymentWrapper = calculatePaymentAmounts(accIdToIncomeDDMap.get(accId), totalAmount, ig, accIdToSInvListMap.get(accId), invoiceInstallmentList, ddPaymentList, true, true);
                }
                ddPaymentList.add(paymentWrapper);
            }else if(accIdToIncomeDDMap.get(accId).DD_Last_Collected_Date__c.addMonths(Integer.valueOf(renewalCustomsetting.Amount__c)) < Date.today()){
                //invalid dd, too long since last collection
                Income_Direct_Debit__c dd = new Income_Direct_Debit__c();
                dd.Id = accIdToIncomeDDMap.get(accId).Id;
                dd.DD_Status__c = 'Expired';
                failedDDList.add(dd);
                for(c2g__codaInvoice__c invoice : accIdToSInvListMap.get(accId)){
                    invoice.JVCO_Payment_Method__c = null;
                    invoice.JVCO_Invoice_Group__c = null;
                    salesInvoiceList.add(invoice);
                }
            }else{
                accIdToIncomeDDMap.get(accId).DD_Status__c = 'New Instruction';
                SMP_PaymentWizardController.ddPaymentWrapper paymentWrapper = calculatePaymentAmounts(accIdToIncomeDDMap.get(accId), totalAmount, ig, accIdToSInvListMap.get(accId), invoiceInstallmentList, ddPaymentList, false, true);
                ddPaymentList.add(paymentWrapper);
            }
        }

        List<JVCO_Invoice_Group__c> invoiceGroupList = new List<JVCO_Invoice_Group__c>();
        List<SMP_DirectDebit_GroupInvoice__c> ddInvoiceGroupList = new List<SMP_DirectDebit_GroupInvoice__c>();
        List<Income_Direct_Debit__c> directDebitToUpdateList = new List<Income_Direct_Debit__c>();

        for(SMP_PaymentWizardController.ddPaymentWrapper p : ddPaymentList){
            Decimal nextCollectionAmount = 0;
            Integer paymentSize = p.paymentWrapperList.size() - 1;
            Integer i = 0;
            List<c2g__codaInvoiceInstallmentLineItem__c> lineItemList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
            for(SMP_PaymentWizardController.paymentWrapper pw : p.paymentWrapperList){
                invoiceInstallmentList.addAll(pw.lineItemList);
                lineItemList.addAll(pw.lineItemList);
            }
            Set<c2g__codaInvoiceInstallmentLineItem__c> mysetILI1 = new Set<c2g__codaInvoiceInstallmentLineItem__c>();
            List<c2g__codaInvoiceInstallmentLineItem__c> resultILI1 = new List<c2g__codaInvoiceInstallmentLineItem__c>();
            mysetILI1.addAll(lineItemList);
            resultILI1.addAll(mysetILI1);

            p.directDebit = checkDirectDebit(p.directDebit, resultILI1);
            directDebitToUpdateList.add(p.directDebit);
            ddInvoiceGroupList.add(p.ddinvoiceGroup);
            salesInvoiceList.addAll(p.salesInvoiceList);
            invoiceGroupList.add(p.invoiceGroup);
        }

        Set<c2g__codaInvoiceInstallmentLineItem__c> mysetILI = new Set<c2g__codaInvoiceInstallmentLineItem__c>();
        List<c2g__codaInvoiceInstallmentLineItem__c> resultILI = new List<c2g__codaInvoiceInstallmentLineItem__c>();
        mysetILI.addAll(invoiceInstallmentList);
        resultILI.addAll(mysetILI);

        directDebitToUpdateList.addAll(failedDDList);

        update invoiceGroupList;
        update salesInvoiceList;
        insert ddInvoiceGroupList;
        insert resultILI;        
        update directDebitToUpdateList;
    }

    public static Income_Direct_Debit__c checkDirectDebit(Income_Direct_Debit__c dd, List<c2g__codaInvoiceInstallmentLineItem__c> iliList){
        
        if(iliList.Size() == 0){
            dd.DD_Ongoing_Collection_Amount__c = 0;
            dd.DD_Next_Collection_Date__c = null;
            if(dd.DD_Status__c != 'Cancelled'){
                dd.DD_Status__c = 'On Hold';
            }
            if(dd.Auto_Expire__c == true)
            {
                dd.DD_Status__c = 'Expired';
            }
            dd.Number_Of_Payments_Left__c = 0;
        }
        else{
            if(dd.DD_Status__c == 'New Instruction' || dd.DD_Status__c == 'First Collection')
            {
                if(dd.DD_Exception__c == false && dd.DD_Status__c == 'New Instruction'){
                    Decimal firstCollectionAmount = 0;
                    for(c2g__codaInvoiceInstallmentLineItem__c ili :iliList){
                        firstCollectionAmount += ili.c2g__Amount__c;
                    }
                    dd.DD_First_Collection_Date__c = iliList[0].c2g__DueDate__c;
                    dd.DD_First_Collection_Amount__c = firstCollectionAmount;
                    //dd.Send_DD_Documentation__c = 'Direct Debit Confirmation';
                }
            }

            Decimal ongoingCollectionAmount = 0;
            Integer paymentsLeft = 0;
            for(c2g__codaInvoiceInstallmentLineItem__c ili :iliList){
                paymentsLeft ++;
                ongoingCollectionAmount += ili.c2g__Amount__c;
            }
            dd.DD_Ongoing_Collection_Amount__c = ongoingCollectionAmount;
            dd.DD_Next_Collection_Date__c = iliList[0].c2g__DueDate__c;
            dd.Number_Of_Payments_Left__c = paymentsLeft;
            dd.DD_Last_Collected_Date__c = date.today();
            System.debug('nextCollection####' + iliList[0].c2g__DueDate__c);
        }

        return dd;
    }

    public static SMP_PaymentWizardController.ddPaymentWrapper addToCurrentDirectDebit(SMP_PaymentWizardController.ddPaymentWrapper ddPaymentWrapper){
        for(c2g__codaInvoice__c si :ddPaymentWrapper.salesInvoiceList){
            si.JVCO_Invoice_Group__c = ddPaymentWrapper.invoiceGroup.Id;
        }

        SMP_DirectDebit_GroupInvoice__c ddGin = new SMP_DirectDebit_GroupInvoice__c();
        ddGin.Invoice_Group__c = ddPaymentWrapper.invoiceGroup.Id;
        ddGin.Income_Direct_Debit__c = ddPaymentWrapper.directDebit.Id;

        ddPaymentWrapper.ddinvoiceGroup = ddGin;

        return ddPaymentWrapper;
    }

    public static Boolean IsWeekendDay(Datetime dateParam){
       boolean result     = false;
        
       //Recover the day of the week
       Date startOfWeek   = dateParam.date().toStartOfWeek();
       Integer dayOfWeek  = dateParam.day() - startOfWeek.day();
           
       result = dayOfWeek == 0 || dayOfWeek == 6 ? true : false;
        
       return result;
    } 

    public static Datetime AddBusinessDays(Datetime StartDate, integer BusinessDaysToAdd ){
       //Add or decrease in BusinessDaysToAdd days 
       Datetime finalDate = StartDate;
       
       integer direction = BusinessDaysToAdd < 0 ? -1 : 1;

        while(BusinessDaysToAdd != 0)
        {
            finalDate = finalDate.AddDays(direction);            
            
            if (!isWeekendDay(finalDate))
            {
                BusinessDaysToAdd -= direction;
            }
        }

        return finalDate;
    } 

    public static SMP_PaymentWizardController.ddPaymentWrapper calculatePaymentAmounts(Income_Direct_Debit__c directDebit, Double totalAmount, JVCO_Invoice_Group__c invoiceGroup, List<c2g__codaInvoice__c> updatedSInvList, List<c2g__codaInvoiceInstallmentLineItem__c> invoiceInstallmentList, List<SMP_PaymentWizardController.ddPaymentWrapper> ddPaymentList, Boolean sendDocument, Boolean recalculateDates){
        List<SMP_DDServiceHandler.CDWrapperObject> cdWrapperList = new List<SMP_DDServiceHandler.CDWrapperObject>();
        Integer range = Integer.valueOf(directDebit.Number_Of_Payments__c);
        Date collectionDay = Date.newInstance(date.today().year(), date.today().month(), integer.valueOf(directDebit.DD_Collection_Day__c));
        system.debug('333bfore' + collectionDay);
        if(collectionDay < directDebit.DD_Next_Collection_Date__c){

            collectionDay = directDebit.DD_Next_Collection_Date__c;
        }
        
        system.debug('333middle' + collectionDay);
        if(recalculateDates){
            if(collectionDay <= AddBusinessDays(date.today(), 10)){
                collectionDay = Date.newInstance(date.today().year(), date.today().month(), integer.valueOf(collectionDay.day()));
                collectionDay = collectionDay.addMonths(1);
            }
        }
        system.debug('333after' + collectionDay);
        //directDebit.Amount_Left_To_Collect__c += totalAmount;
        if(sendDocument){
            directDebit.Send_DD_Documentation__c = 'Direct Debit Instruction Change';
        }
        
        for(Integer i = 0; i < range + 1; i++){
            SMP_DDServiceHandler.CDWrapperObject paymentDateObject = new SMP_DDServiceHandler.CDWrapperObject();
            if(i == 0){
                paymentDateObject.sd = string.valueOf(collectionDay);
            }else{
                paymentDateObject.sd = string.valueOf(collectionDay.addMonths(i));
            }
            
            cdWrapperList.add(paymentDateObject);

        }

        SMP_DDServiceHandler.CDWrapper cdWrapper = new SMP_DDServiceHandler.CDWrapper(cdWrapperList, 'Monthly', 
                                                                                        string.valueOf(range), string.valueOf(collectionDay.day()));

        if(directDebit.DD_Collection_Period__c == 'Weekly'){
            cdWrapper = new SMP_DDServiceHandler.CDWrapper(cdWrapperList, 'Weekly', string.valueOf(range), string.valueOf(collectionDay.day()));
        }

        SMP_DDServiceNextCollectionModelList response = SMP_DDServiceHandler.getNextCollectionDateList(cdWrapper);

        if(response.ProcessListNewDDFirstCollectionDateResult.size() > 1){
            if(response.ProcessListNewDDFirstCollectionDateResult[0].FirstCollectionDate == response.ProcessListNewDDFirstCollectionDateResult[1].FirstCollectionDate){
                response.ProcessListNewDDFirstCollectionDateResult.remove(0);
            }else{
                response.ProcessListNewDDFirstCollectionDateResult.remove(range);
            }
        }
        
        Decimal paymentAmountCheck = 0;
        Integer i = range;
        List<SMP_PaymentWizardController.paymentWrapper> paymentList = new List<SMP_PaymentWizardController.paymentWrapper>();
        List<Date> paymentDateList = new List<Date>();
        for(SMP_DDServiceNextCollectionModelList.ProcessListNewDDFirstCollectionDateResult idd : response.ProcessListNewDDFirstCollectionDateResult){
            System.debug('##### idd: ' + idd);
            if(string.isNotBlank(idd.FirstCollectionDate)){
                SMP_PaymentWizardController.paymentWrapper payment = new SMP_PaymentWizardController.paymentWrapper();
                String[] strDate = idd.FirstCollectionDate.split('-');
                Integer myIntDate = integer.valueOf(strDate[2]);
                Integer myIntMonth = integer.valueOf(strDate[1]);
                Integer myIntYear = integer.valueOf(strDate[0]);

                payment.paymentDate = Date.newInstance(myIntYear, myIntMonth, myIntDate);

                if(i == 0){
                    collectionDay = payment.paymentDate;
                }

                if(range == 1){
                    payment.paymentAmount = totalAmount / range;
                }else{
                    payment.paymentAmount = totalAmount / range;
                }
                payment.paymentAmount = payment.paymentAmount.setScale(2);
                paymentList.add(payment);
                paymentDateList.add(payment.paymentDate);
                paymentAmountCheck += payment.paymentAmount;

                i++;
            }
        }

        if(totalAmount > paymentAmountCheck){
            Decimal remainder = totalAmount - paymentAmountCheck;
            paymentList[0].paymentAmount += remainder;
        }else if(totalAmount < paymentAmountCheck){
            Decimal remainder = paymentAmountCheck - totalAmount;
            paymentList[0].paymentAmount -= remainder;
        }

        List<c2g__codaInvoice__c> invoiceList = new List<c2g__codaInvoice__c>();
        for(c2g__codaInvoice__c invoice :updatedSInvList){
            if(invoice.JVCO_Invoice_Group__c == invoiceGroup.Id){
                invoiceList.add(invoice);
            }
        }
        paymentList.addAll(createLineItems(paymentList, range, invoiceList, totalAmount,invoiceInstallmentList));

        SMP_PaymentWizardController.ddPaymentWrapper ddPaymentWrapper = new SMP_PaymentWizardController.ddPaymentWrapper();
        ddPaymentWrapper.paymentWrapperList = paymentList;
        ddPaymentWrapper.directDebit = directDebit;
        ddPaymentWrapper.invoiceGroup = invoiceGroup;
        ddPaymentWrapper.salesInvoiceList = updatedSInvList;
        
        ddPaymentWrapper = addToCurrentDirectDebit(ddPaymentWrapper);
        

        for(SMP_PaymentWizardController.paymentWrapper payment : paymentList){
            invoiceInstallmentList.addAll(payment.lineItemList);
        }

        return ddPaymentWrapper;
    }

    public static List<SMP_PaymentWizardController.paymentWrapper> createLineItems(List<SMP_PaymentWizardController.paymentWrapper> paymentList, 
                                                                                Integer range, List<c2g__codaInvoice__c> invoiceList,
                                                                                Double totalAmount, List<c2g__codaInvoiceInstallmentLineItem__c> invoiceInstallmentList){
        Integer count = 0;

        for(SMP_PaymentWizardController.paymentWrapper payment : paymentList){
            Decimal paymentAmountCheck = 0;
            payment.lineItemList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
            for(c2g__codaInvoice__c salesInvoice :invoiceList){
                c2g__codaInvoiceInstallmentLineItem__c installmentLineItem = new c2g__codaInvoiceInstallmentLineItem__c();
                Decimal percentage = (totalAmount / payment.paymentAmount);
                Decimal paymentAmount = (payment.paymentAmount * percentage);
                System.debug('###pa' + paymentAmount);
                if(range == 0){
                    installmentLineItem.c2g__Amount__c = paymentAmount;
                }else{
                    installmentLineItem.c2g__Amount__c = paymentAmount / range;
                }
                paymentAmountCheck += installmentLineItem.c2g__Amount__c;
                installmentLineItem.c2g__DueDate__c = payment.paymentDate;
                installmentLineItem.c2g__Invoice__c = salesInvoice.Id;
                installmentLineItem.JVCO_Payment_Status__c = 'Unpaid';
                
                invoiceInstallmentList.add(installmentLineItem);
                payment.lineItemList.add(installmentLineItem);
            }
            count++;
        }

        for(SMP_PaymentWizardController.paymentWrapper payment : paymentList){
            Decimal paymentAmountCheck = 0;
            for(c2g__codaInvoiceInstallmentLineItem__c ili:payment.lineItemList){
                ili.c2g__Amount__c = ili.c2g__Amount__c.setScale(2);
                paymentAmountCheck += ili.c2g__Amount__c;
            }

            if(paymentAmountCheck > payment.paymentAmount){
                Decimal remainder = paymentAmountCheck - payment.paymentAmount;
                payment.lineItemList = apportionLineItemAmounts(remainder, payment.lineItemList, payment.paymentAmount, true);
            }else if(payment.paymentAmount > paymentAmountCheck){
                Decimal remainder = payment.paymentAmount - paymentAmountCheck;
                payment.lineItemList = apportionLineItemAmounts(remainder, payment.lineItemList, payment.paymentAmount, false);
            }
        }

        for(c2g__codaInvoice__c salesInvoice :invoiceList){
            Decimal counter = 0;
            Decimal outstanding = salesInvoice.c2g__OutstandingValue__c;
            for(c2g__codaInvoiceInstallmentLineItem__c li :invoiceInstallmentList){
                if(li.c2g__Invoice__c == salesInvoice.Id){
                    counter += li.c2g__Amount__c;
                }
            }

            if(counter > outstanding){
                for(c2g__codaInvoiceInstallmentLineItem__c li :invoiceInstallmentList){
                    if(li.c2g__Invoice__c == salesInvoice.Id){
                        li.c2g__Amount__c -= counter - outstanding;
                        li.c2g__Amount__c = li.c2g__Amount__c.setScale(2);
                        break;
                    }
                }
            }else if(counter < outstanding){
                for(c2g__codaInvoiceInstallmentLineItem__c li :invoiceInstallmentList){
                    if(li.c2g__Invoice__c == salesInvoice.Id){
                        li.c2g__Amount__c += outstanding - counter;
                        li.c2g__Amount__c = li.c2g__Amount__c.setScale(2);
                        break;
                    }
                }
            }
        }

        for(SMP_PaymentWizardController.paymentWrapper payment : paymentList){
            Decimal paymentAmount = 0;
            for(c2g__codaInvoiceInstallmentLineItem__c li :payment.lineItemList){
                paymentAmount += li.c2g__Amount__c;
            }
            payment.paymentAmount = paymentAmount.setScale(2);
        }

        return paymentList;
    }

    public static List<c2g__codaInvoiceInstallmentLineItem__c> apportionLineItemAmounts(Decimal remainder, List<c2g__codaInvoiceInstallmentLineItem__c> lineItemList, Decimal lineAmount, Boolean subtract){
        System.debug('##remainder :' + remainder);
        System.debug('##liList :' + lineItemList);
        System.debug('##lAmount :' + lineAmount);
        System.debug('##subtract : ' + subtract);

        if(subtract){
            lineItemList[0].c2g__Amount__c = lineItemList[0].c2g__Amount__c + remainder;
            lineItemList[0].c2g__Amount__c = lineItemList[0].c2g__Amount__c.setScale(2);
        }else{
            lineItemList[0].c2g__Amount__c = lineItemList[0].c2g__Amount__c - remainder;
            lineItemList[0].c2g__Amount__c = lineItemList[0].c2g__Amount__c.setScale(2);
        }

        return lineItemList;
    }
    
    public void finish(Database.BatchableContext bc)
    {
        if(!cNoteIdStatefulSet.isEmpty())
        {
            JVCO_PostSalesCreditNoteBatch pscnb = new JVCO_PostSalesCreditNoteBatch(cNoteIdStatefulSet);
            pscnb.stopMatching = stopMatching;
            pscnb.licAndCustAccIdMap = licAndCustAccIdMap;
            Database.executeBatch(pscnb, Integer.valueOf(POSTCREDITNOTESCOPE));
        }
        else if(!licAndCustAccIdMap.isEmpty()){
            System.enqueueJob(new JVCO_UpdateSalesInvoiceQueueable(licAndCustAccIdMap));
        }
    }

    
}