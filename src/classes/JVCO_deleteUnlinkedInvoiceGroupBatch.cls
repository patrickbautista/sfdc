/* ----------------------------------------------------------------------------------------------------------
Name:            JVCO_deleteUnlinkedInvoiceGroupBatch.cls
Description:     Delete Unlinked Invoice Group Batch class
Test class:      JVCO_WebsitePaymentschudlerTest.cls

Date             Version     Author                 Summary of Changes
-----------      -------     -----------------      -------------------------------------------
09-Nov-2018      0.1         patrick.t.bautista     Initial version of the code
06-Mar-2019      0.2         patrick.t.bautista     GREEN-34320 - Change functionality instead of deleting invoice
                                                    group change status to retired
---------------------------------------------------------------------------------------------------------- */
public class JVCO_deleteUnlinkedInvoiceGroupBatch /*implements Database.Batchable<sObject>*/
{
    //Retrieve Custom settings
    /*private final JVCO_General_Settings__c GENERAL_SETTINGS = JVCO_General_Settings__c.getInstance();
    private final Decimal PROCESSINGLIMIT = GENERAL_SETTINGS != null ? GENERAL_SETTINGS.JVCO_PayonomyPaymentProcessingBatchLimit__c : 20;
    
    public JVCO_deleteUnlinkedInvoiceGroupBatch(){}
    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        //Get Invoice Group without associated PPA
        return Database.getQueryLocator([SELECT id, Name, JVCO_Status__c
                                         FROM JVCO_Invoice_Group__c 
                                         Where id NOT IN (select JVCO_Invoice_Group__c from PAYREC2__Payment_Agreement__c 
                                                          where JVCO_Invoice_Group__c != null)
                                         AND JVCO_Payment_Agreement__c = null 
                                         AND JVCO_Status__c != 'Retired' 
                                         ORDER BY CreatedDate DESC LIMIT 50000]);
    }
    public void execute(Database.BatchableContext bc, List<JVCO_Invoice_Group__c> scope)
    {
        //Get Invoice Group and remove Invoice Group associated to SagePay
        Map<String, JVCO_Invoice_Group__c> invGrpMap = new Map<String, JVCO_Invoice_Group__c>();
        //List of invoice group to be updated
        List<JVCO_Invoice_Group__c> invGrpList = new List<JVCO_Invoice_Group__c>();
        for(JVCO_Invoice_Group__c invGroup : scope)
        {
            invGrpMap.put(invGroup.Name, invGroup);
        }
        if(!invGrpMap.isEmpty())
        {
            for(PAYBASE2__Payment__c payment : [select PAYCP2__Payment_Description__c 
                                                from PAYBASE2__Payment__c
                                                where PAYCP2__Payment_Description__c IN : invGrpMap.keySet()])
            {
                if(invGrpMap.containsKey(payment.PAYCP2__Payment_Description__c))
                {
                    invGrpMap.remove(payment.PAYCP2__Payment_Description__c);
                }
            }
            for(JVCO_Invoice_Group__c invGrp: invGrpMap.values())
            {
                invGrp.JVCO_Status__c = 'Retired';
                invGrpList.add(invGrp);
            }
            //Upate Invoice Group without assiciated PPA and Sagepay
            update invGrpList;
        }
    }
    public void finish(Database.BatchableContext bc)
    {
        //Calling SagePay and DD Batch Job
        Database.executebatch(new JVCO_PayonomyPaymentProcessingBatch('SagePay'), Integer.valueOf(PROCESSINGLIMIT));
    }*/
}