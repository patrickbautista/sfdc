/* ----------------------------------------------------------------------------------------------
Name: JVCO_UpdateInvoiceGroupField.cls 
Description: Batch class that updates the invoice group field on the sales invoice object and Payonomy Payment Agreement object
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
20-Sept-2017  0.1         desiree.m.quijada     Initial creation
28-Sept-2017  0.2         desiree.m.quijada     Check and split migrated invoice with :
11-Jan-2018   0.3         desiree.m.quijada     GREEN-26781
----------------------------------------------------------------------------------------------- */

global class JVCO_UpdateInvoiceGroupField implements Database.Batchable<sObject> {
    
    String query = 'SELECT Id, Name, JVCO_Migrated_Invoice__c, JVCO_Payonomy_Payment_Agreement_Ext_Id__c FROM JVCO_Invoice_Group__c WHERE JVCO_Migrated_Invoice__c != NULL AND JVCO_Payonomy_Payment_Agreement_Ext_Id__c != NULL' ;

    public static List<JVCO_Invoice_Group__c> invoiceGroupList;

    public static void init()
    {   
        Set<String> migratedInvoicesSet = new Set<String>();
        Set<String> payonomyExternalIdSet = new Set<String>();
        List<c2g__codaInvoice__c> salesInvoiceUpdateList = new List<c2g__codaInvoice__c>();
        List<PAYREC2__Payment_Agreement__c> payonomyUpdateList = new List<PAYREC2__Payment_Agreement__c>();
        Map<String, Id> migInvoiceMap = new Map<String, Id>();
        
        if(!invoiceGroupList.isEmpty()){
            for(JVCO_Invoice_Group__c ig : invoiceGroupList){
                //migratedInvoicesSet.add(ig.JVCO_Migrated_Invoice__c);
                payonomyExternalIdSet.add(ig.JVCO_Payonomy_Payment_Agreement_Ext_Id__c);
                if(ig.JVCO_Migrated_Invoice__c.contains(';')){
                    List <String> migratedInvoice = ig.JVCO_Migrated_Invoice__c.split(';');
                    for(String m : migratedInvoice){
                        migratedInvoicesSet.add(m);
                        migInvoiceMap.put(m, ig.Id);
                    }
                    
                }
                else{
                    migratedInvoicesSet.add(ig.JVCO_Migrated_Invoice__c);
                    migInvoiceMap.put(ig.JVCO_Migrated_Invoice__c, ig.Id);
                }   
            }
        }


        System.debug('Migrated invoices' +migratedInvoicesSet);
        System.debug('Payonomy External Id' +payonomyExternalIdSet);
        System.debug('migInvoiceMap' +migInvoiceMap);

        List<c2g__codaInvoice__c> salesInvoiceList = new List<c2g__codaInvoice__c>();
        List<PAYREC2__Payment_Agreement__c> payonomyList = new List<PAYREC2__Payment_Agreement__c>();

        salesInvoiceList = [SELECT ID, Name, JVCO_Invoice_Legacy_Number__c, JVCO_Invoice_Group__c FROM c2g__codaInvoice__c WHERE JVCO_Invoice_Legacy_Number__c IN :migratedInvoicesSet];
        payonomyList = [SELECT ID, Name, JVCO_Temp_External_Id__c, PAYREC2__Description__c, JVCO_Invoice_Group__c FROM PAYREC2__Payment_Agreement__c WHERE JVCO_Temp_External_Id__c IN :payonomyExternalIdSet];


        for(c2g__codaInvoice__c si: salesInvoiceList)
        {
            if(migInvoiceMap.containsKey(si.JVCO_Invoice_Legacy_Number__c))
            {
                si.JVCO_Invoice_Group__c = migInvoiceMap.get(si.JVCO_Invoice_Legacy_Number__c);
                salesInvoiceUpdateList.add(si);
            }
        }



        for(JVCO_Invoice_Group__c invoicegroup : invoiceGroupList)
        {
            for(PAYREC2__Payment_Agreement__c ppy: payonomyList)
            {
                if(invoicegroup.JVCO_Payonomy_Payment_Agreement_Ext_Id__c == ppy.JVCO_Temp_External_Id__c)
                {
                    ppy.JVCO_Invoice_Group__c = invoicegroup.id;
                    //start GREEN-26781
                    ppy.PAYREC2__Description__c = invoicegroup.Name;
                    //end GREEN-26781
                    payonomyUpdateList.add(ppy);
                }
            }
        }       

        System.debug('salesInvoiceUpdateList >> ' + salesInvoiceUpdateList);
        System.debug('payonomyUpdateList >> ' + payonomyUpdateList);
        
        if(!salesInvoiceUpdateList.isEmpty()){
                update salesInvoiceUpdateList; 
                  
        }
        
        if(!payonomyUpdateList.isEmpty()){
                update payonomyUpdateList; 
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<JVCO_Invoice_Group__c> scope) {

        invoiceGroupList = scope;
        init();
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('Done');
    }
    
}