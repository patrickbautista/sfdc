public class JVCO_MatchToCashReceiptLogic extends JVCO_MatchingReferenceDocumentWrapper implements JVCO_MatchingReferenceDocumentInterface
{	
	private Set<String> accountReferenceSet = new Set<String>();
	private Map<Id, List<c2g__codaCashEntryLineItem__c>> accIdToCashEntryLineMap = new Map<Id, List<c2g__codaCashEntryLineItem__c>>();

	private JVCO_MatchToCashReceiptLogic(){}

	public JVCO_MatchToCashReceiptLogic(Map<Id, String> transLineIdToRefDocMap, Map<Id, c2g__codaTransactionLineItem__c> transLineMap)
	{
		super.transLineIdToRefDocMap = transLineIdToRefDocMap;
		super.transLineMap = transLineMap;
	}

	public void execute()
	{
		stopperMethod();
		setAccIdToCashEntryLineMap();
		setMatchingReferenceTransactionLine();
		callMatchingAPI();
	}

	private void setAccIdToCashEntryLineMap()
	{
		for(c2g__codaCashEntryLineItem__c cashEntryLine : [SELECT Name,
															c2g__Account__c, 
															c2g__AccountReference__c,
															c2g__CashEntryValue__c
															FROM c2g__codaCashEntryLineItem__c
															WHERE Name IN : transLineIdToRefDocMap.values()])
		{
			if(!accIdToCashEntryLineMap.containsKey(cashEntryLine.c2g__Account__c))
			{
				accIdToCashEntryLineMap.put(cashEntryLine.c2g__Account__c, new List<c2g__codaCashEntryLineItem__c>());
			}
			accIdToCashEntryLineMap.get(cashEntryLine.c2g__Account__c).add(cashEntryLine);
			//Add Cash Line Reference
			accountReferenceSet.add(cashEntryLine.c2g__AccountReference__c);
		}
	}

	private void setMatchingReferenceTransactionLine()
	{
		//Get all Transaction Line account with the given Reference Document
		//Map it to Reference Document - refDocToMatchingRefTransLineMap
		for(c2g__codaTransactionLineItem__c referenceTransLine : [SELECT Id, Name,
																	c2g__Account__c,
																	c2g__AccountValue__c,
																	c2g__LineReference__c,
																	c2g__AccountOutstandingValue__c
																	FROM c2g__codaTransactionLineItem__c
																	WHERE c2g__MatchingStatus__c = 'Available'
						                                            AND c2g__LineType__c = 'Account'
						                                            AND c2g__LineDescription__c != 'Not for Matching'
						                                            AND c2g__Account__c IN : accIdToCashEntryLineMap.keySet()
						                                            AND c2g__LineReference__c IN : accountReferenceSet
						                                            AND c2g__Transaction__r.c2g__TransactionType__c = 'Cash'
						                                            AND c2g__AccountOutstandingValue__c < 0])
		{
			if(accIdToCashEntryLineMap.containsKey(referenceTransLine.c2g__Account__c))
			{
				for(c2g__codaCashEntryLineItem__c cashEntryLine : accIdToCashEntryLineMap.get(referenceTransLine.c2g__Account__c))
				{
					if(cashEntryLine.c2g__AccountReference__c == referenceTransLine.c2g__LineReference__c &&
						Math.abs(cashEntryLine.c2g__CashEntryValue__c) == Math.abs(referenceTransLine.c2g__AccountValue__c))
					{
						refDocToMatchingRefTransLineMap.put(cashEntryLine.Name, referenceTransLine);
					}
					
				}
			}
		}
	}

	private void stopperMethod()
	{
		//JVCO_FFUtil.stopCashTransferLogic = true;
	}
}