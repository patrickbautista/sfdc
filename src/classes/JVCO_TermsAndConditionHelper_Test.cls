/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_TermsAndConditionHelper_Test.cls 
   Description:     Test class for JVCO_TermsAndConditionHelper.cls

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
   04-July-2018       0.1       jules.osberg.a.pablo              Initial version of the code              
   ---------------------------------------------------------------------------------------------------------- */
@isTest
private class  JVCO_TermsAndConditionHelper_Test {
	@testSetup static void setupTestData() 
	{
		SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
		insert qProcess;
		
		Pricebook2 pb = JVCO_TestClassObjectBuilder.createPricebook();
		insert pb;

		Product2 prod1 = JVCO_TestClassObjectBuilder.createProduct();
		insert prod1;
		
		Account a1 = JVCO_TestClassObjectBuilder.createCustomerAccount();
		insert a1;

		Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
		insert a2;
		
		Opportunity o = JVCO_TestClassObjectBuilder.createOpportunity(a2);
		o.CloseDate = System.today() - 5;
		o.StageName = 'Draft';
		insert o;
		
		SBQQ__Quote__c q = new SBQQ__Quote__c();
		q.SBQQ__Type__c = 'Amendment';
		q.SBQQ__Status__c = 'Draft';
		q.SBQQ__Primary__c = true;
		q.SBQQ__Account__c = a2.Id;
		q.SBQQ__Opportunity2__c = o.Id;
		q.SBQQ__StartDate__c = System.today() - 5;
		q.SBQQ__EndDate__c = System.today().addMonths(12);
		q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
		insert q;

		o.SBQQ__PrimaryQuote__c = q.Id;
		update o;
		
		SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
		ql.SBQQ__Quote__c = q.Id;
		ql.SBQQ__Product__c = prod1.Id;
		ql.SBQQ__Quantity__c = 1;
		ql.SBQQ__Number__c = 1;
		ql.SBQQ__PricingMethod__c = 'List';
		ql.SBQQ__ListPrice__c = 10;
		ql.SBQQ__CustomerPrice__c = 10;
		ql.SBQQ__NetPrice__c = 10;
		ql.SBQQ__SpecialPrice__c = 10;
		ql.SBQQ__RegularPrice__c = 10;
		ql.SBQQ__UnitCost__c = 10;
		ql.SBQQ__ProratedListPrice__c = 10;
		ql.SBQQ__SpecialPriceType__c = 'Custom';
		insert ql;
		
		JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();           
		Insert  venueRecord;   
 
		JVCO_Affiliation__c  affilRecord = JVCO_TestClassObjectBuilder.createAffiliation(a2.id, venueRecord.id);

		Insert  affilRecord;     
		
		SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
		quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
		quoteLineGroupRecord.JVCO_IsComplete__c = true;    
		quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
		Insert  quoteLineGroupRecord;	
	}

	
	public static testMethod void testgetselectedTCfieldsNull() {
		List<Selectoption> picklistValue = new List<selectoption>();
        picklistValue.add(new selectOption('', '--None--'));
        picklistValue.add(new selectOption('Yes', 'Yes'));
        picklistValue.add(new selectOption('No', 'No'));

        JVCO_TermsAndConditionHelper termsAndConditionHelper = new JVCO_TermsAndConditionHelper();

        Test.startTest();
        System.assertEquals(picklistValue, termsAndConditionHelper.getselectedTCfields(''));
        Test.stopTest();
	}

	public static testMethod void testgetselectedTCfieldsNo() {
		List<Selectoption> picklistValue = new List<selectoption>();
        picklistValue.add(new selectOption('Yes', 'Yes'));
        picklistValue.add(new selectOption('No', 'No'));

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        List<Account> accountList = new List<Account>([SELECT Id, JVCO_Contract_Batch_Size__c, JVCO_Customer_Account__c FROM Account WHERE RecordTypeId = :licenceRT ORDER BY CreatedDate DESC LIMIT 1]);

        JVCO_TermsAndConditionHelper termsAndConditionHelper = new JVCO_TermsAndConditionHelper();
        PageReference orderPage = new PageReference('/' + accountList[0].id);

        Test.startTest();
        System.assertEquals(FALSE, termsAndConditionHelper.tcIsYes('No'));
        System.assertEquals(picklistValue, termsAndConditionHelper.getselectedTCfields('No'));
        //System.assertEquals(orderPage, termsAndConditionHelper.tcUpdateAction('No', orderPage));
        Test.stopTest();
	}

	public static testMethod void testUpdateCustomerTC() {
		Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
    	List<Account> accountList = new List<Account>([SELECT Id, JVCO_Contract_Batch_Size__c, JVCO_Customer_Account__c FROM Account WHERE RecordTypeId = :licenceRT ORDER BY CreatedDate DESC LIMIT 1]);
    	List<Account> customerAccountList = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
		
    	JVCO_TermsAndConditionHelper termsAndConditionHelper = new JVCO_TermsAndConditionHelper();
    	PageReference orderPage = new PageReference('/' + accountList[0].id);

        Test.startTest();
        System.assertEquals(TRUE, termsAndConditionHelper.tcIsYes('Yes'));
        System.assertEquals(TRUE, termsAndConditionHelper.updateCustomerTC('Yes', customerAccountList[0].TCs_Accepted__c, customerAccountList[0].Id));
        //System.assertEquals(NULL, termsAndConditionHelper.tcUpdateAction('Yes', orderPage));
        Test.stopTest();
	}

	public static testMethod void testBipassTCWindow() {
		Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
    	List<Account> accountList = new List<Account>([SELECT Id, JVCO_Contract_Batch_Size__c, JVCO_Customer_Account__c FROM Account WHERE RecordTypeId = :licenceRT ORDER BY CreatedDate DESC LIMIT 1]);
    	List<Account> customerAccountList = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
		SBQQ__Quote__c qtRec = [Select Id, SBQQ__Primary__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c,SBQQ__Status__c From SBQQ__Quote__c Limit 1];
        qtRec.JVCO_Single_Society__c = TRUE;
        update qtRec;

        List<Opportunity> oppCheck = [select Id, SBQQ__PrimaryQuote__c from Opportunity where AccountId = :accountList[0].Id];

    	JVCO_TermsAndConditionHelper termsAndConditionHelper = new JVCO_TermsAndConditionHelper();	

    	Test.startTest();
        System.assertEquals(TRUE, termsAndConditionHelper.bipassTCWindow(oppCheck, FALSE));
        Test.stopTest();
	}

	public static testMethod void testBipassTCWindowInvalid() {
		Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
    	List<Account> accountList = new List<Account>([SELECT Id, Name, JVCO_Contract_Batch_Size__c, JVCO_Customer_Account__c FROM Account WHERE RecordTypeId = :licenceRT ORDER BY CreatedDate DESC LIMIT 1]);
    	List<Account> customerAccountList = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
		SBQQ__Quote__c qtRec = [Select Id, SBQQ__Primary__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c,SBQQ__Status__c From SBQQ__Quote__c Limit 1];
        qtRec.JVCO_Single_Society__c = TRUE;
        update qtRec;

        Opportunity o = JVCO_TestClassObjectBuilder.createOpportunity(accountList[0]);
		o.CloseDate = System.today() - 5;
		o.StageName = 'Draft';
		insert o;
		
		SBQQ__Quote__c q = new SBQQ__Quote__c();
		q.SBQQ__Type__c = 'Amendment';
		q.SBQQ__Status__c = 'Draft';
		q.SBQQ__Primary__c = true;
		q.SBQQ__Account__c = accountList[0].Id;
		q.SBQQ__Opportunity2__c = o.Id;
		q.SBQQ__StartDate__c = System.today() - 5;
		q.SBQQ__EndDate__c = System.today().addMonths(12);
		q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
		insert q;

		o.SBQQ__PrimaryQuote__c = q.Id;
		update o;

        List<Opportunity> oppCheck = [select Id, SBQQ__PrimaryQuote__c from Opportunity where AccountId = :accountList[0].Id];

    	JVCO_TermsAndConditionHelper termsAndConditionHelper = new JVCO_TermsAndConditionHelper();	

    	Test.startTest();
        System.assertEquals(FALSE, termsAndConditionHelper.bipassTCWindow(oppCheck, FALSE));
        Test.stopTest();
	}
}