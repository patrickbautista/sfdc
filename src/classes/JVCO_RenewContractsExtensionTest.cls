@isTest
public class JVCO_RenewContractsExtensionTest  {

     @testSetup
    private static void testSetup(){
        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.Id);
        insert acc2;
                
        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, null, null, null);
        insert q;

        SBQQ__Quote__c q2 = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, null, null, null);
        insert q2;
 
        Contract c = JVCO_TestClassObjectBuilder.createContract(acc2.id, q.id);
        insert c;

        Contract c2 = JVCO_TestClassObjectBuilder.createContract(acc2.id, q2.id);
        insert c2;
    }

    @isTest
    public static void renewContracts()
    {
        Id licenseId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
        List<Contract> contList = [Select Id from contract];
        List<Account> accList = [Select Id from Account where RecordTypeId = :licenseId];
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.JVCO_RenewalContracts')); 
        System.currentPageReference().getParameters().put('retURL', 'test');
        System.currentPageReference().getParameters().put('id', accList[0].Id);

        System.debug('Number of Contracts = ' + contList.size());

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(contList);
        sc.setSelected(contList);
        JVCO_RenewContractsExtension renewalExt = new JVCO_RenewContractsExtension(sc);
        renewalExt.masterContractId = contList[0].id;
        renewalExt.onRenew();
        
        //Call onRenew method again to cover the newly added error message
        renewalExt.onRenew();
        for(Contract cont:contList)
        {
            cont.SBQQ__RenewalQuoted__c = true;
        }
        
        update contList;
        renewalExt.onRenew();
        test.stoptest();
    }

    @isTest
    public static void testKARenewCon()
    {
        Id licenseId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
        List<Contract> contList = [Select Id from contract];
        Account accList = [Select Id, Type from Account where RecordTypeId = :licenseId];
        Test.startTest();

        accList.Type = 'Key Account';
        update accList;

        List<Account> accListAfterUpdate = [Select Id, Type from Account where Id = :accList.Id];

        system.assertEquals(1,accListAfterUpdate.size());
        
        Test.setCurrentPageReference(new PageReference('Page.JVCO_RenewalContracts')); 
        System.currentPageReference().getParameters().put('retURL', 'test');
        System.currentPageReference().getParameters().put('id', accListAfterUpdate[0].Id);

        System.debug('Number of Contracts = ' + contList.size());

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(contList);
        sc.setSelected(contList);
        JVCO_RenewContractsExtension renewalExt = new JVCO_RenewContractsExtension(sc);
        renewalExt.masterContractId = contList[0].id;
        renewalExt.onRenew();
        
        //Call onRenew method again to cover the newly added error message
        renewalExt.onRenew();
        for(Contract cont:contList)
        {
            cont.SBQQ__RenewalQuoted__c = true;
        }
        
        update contList;
        renewalExt.onRenew();
        test.stoptest();
    }

}