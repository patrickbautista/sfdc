/* ----------------------------------------------------------------------------------------------
    Name: JVCO_RecreateSalesInvoiceInstallmentline.cls 
    Test Class: JVCO_RecreateSInvInstalmentLineTest.cls
    Description: Batch class that creates deleted instalment lines and update due date of 
    overdue instalment lines

    Date         Version     Author              Summary of Changes 

    -----------  -------     -----------------   -----------------------------------------
    01-Aug-2018   0.1        patrick.t.bautista  Intial creation
----------------------------------------------------------------------------------------------- */

public class JVCO_RecreateSalesInvoiceInstallmentline implements Database.Batchable<sObject>
{
    private Set<id> sInvIdSet;
    public static boolean shouldBeAdjusted;
    public static Map<String,List<Date>> instlLineMap = new Map<String,List<Date>>();
    private static Set<Date> gbHolidayDateSet = new Set<Date>();
    public JVCO_RecreateSalesInvoiceInstallmentline(){
    }
    public JVCO_RecreateSalesInvoiceInstallmentline(Set<id> sInvIdSet)
    {
        this.sInvIdSet = sInvIdSet;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([SELECT id, name, JVCO_Payment_Method__c, JVCO_Number_of_Installment_Line_Items__c, JVCO_Invoice_Group__c, 
                                         JVCO_Invoice_Group__r.JVCO_Payment_Agreement__c, JVCO_Invoice_Group__r.createddate, 
                                         c2g__PaymentStatus__c, JVCO_Invoice_Group__r.Name
                                         FROM c2g__codaInvoice__c 
                                         WHERE JVCO_Invoice_Group__c NOT in (null, '') 
                                         AND JVCO_Payment_Method__c='Direct Debit'
                                         AND id IN : sInvIdSet]);
    }
    public void execute(Database.BatchableContext BC, List<c2g__codaInvoice__c> scope)
    {
        Set<id> invIdSet = new Set<Id>();
        List<String>  agreements = new List<String>();
        for(c2g__codaInvoice__c sInv : scope)
        {
            agreements.add(sInv.JVCO_Invoice_Group__r.Name);
            invIdSet.add(sInv.id);
        }
        Map<Id,PAYREC2__Payment_Agreement__c> agreementMap = new Map<Id,PAYREC2__Payment_Agreement__c>(
            [SELECT Id, PAYREC2__Payment_Schedule__c, PAYREC2__First_Collection_Amount__c, PAYREC2__Total_Collected__c,
             PAYREC2__Ongoing_Collection_Amount__c, PAYREC2__Final_Collection_Amount__c,
             PAYREC2__Description__c, JVCO_Invoice_Group__c, JVCO_Invoice_Group__r.id
             FROM PAYREC2__Payment_Agreement__c
             WHERE PAYREC2__Description__c IN : agreements]);
        
        Map<Id,PAYREC2__Payment_Schedule__c> scheduleMap = new Map<Id,PAYREC2__Payment_Schedule__c>(
            [SELECT Id, PAYREC2__Day__c, PAYREC2__Max__c, PAYFISH3__First_Collection_Interval__c
             FROM PAYREC2__Payment_Schedule__c]);
        
        for(c2g__codaInvoiceInstallmentLineItem__c instLine : [SELECT c2g__Invoice__r.Name, c2g__DueDate__c, JVCO_Paid_Amount__c
                                                               from c2g__codaInvoiceInstallmentLineItem__c 
                                                               WHERE c2g__Invoice__c IN:scope
                                                              ])
        {
            if(instlLineMap.containsKey(instLine.c2g__Invoice__r.Name))
            {
                List<Date> insDate = instlLineMap.get(instLine.c2g__Invoice__r.Name);
                insDate.add(instLine.c2g__DueDate__c);
                instlLineMap.put(instLine.c2g__Invoice__r.Name, insDate);
            }
            else
            {
                instlLineMap.put(instLine.c2g__Invoice__r.Name, new List<Date>{instLine.c2g__DueDate__c});
            }
        }
        shouldBeAdjusted = FALSE;
        for (Id aId : agreementMap.keySet())
        {
            PAYREC2__Payment_Agreement__c agreement = agreementMap.get(aId);
            PAYREC2__Payment_Schedule__c sched = scheduleMap.get(agreement.PAYREC2__Payment_Schedule__c);
            Double totalAmount = computeTotalAmount(agreement, sched.PAYREC2__Max__c);
            createInstalmentLineItem(agreement, sched, totalAmount, Integer.valueOf(sched.PAYREC2__Max__c), agreement.JVCO_Invoice_Group__r.id);
        }
        updateDueDate(invIdSet);
    }
    public static Double computeTotalAmount(PAYREC2__Payment_Agreement__c agreement,
                                            Decimal numberOfInstalments)
    {
        // Checks if First and/or Final Collection were set or not
        if (agreement.PAYREC2__First_Collection_Amount__c == null)
        {
            agreement.PAYREC2__First_Collection_Amount__c = agreement.PAYREC2__Ongoing_Collection_Amount__c;
        }
        if (agreement.PAYREC2__Final_Collection_Amount__c == null)
        {
            agreement.PAYREC2__Final_Collection_Amount__c = agreement.PAYREC2__Ongoing_Collection_Amount__c;
        }
        return (agreement.PAYREC2__First_Collection_Amount__c
                + agreement.PAYREC2__Final_Collection_Amount__c
                + ((numberOfInstalments - 2) * agreement.PAYREC2__Ongoing_Collection_Amount__c));
    }
    public static void createInstalmentLineItem(PAYREC2__Payment_Agreement__c agreement, PAYREC2__Payment_Schedule__c sched,
                                                Double totalAmount, Integer nthInstalment, id invGrp)
    {
        
        List<c2g__codaInvoiceInstallmentLineItem__c> newItemList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
        for (c2g__codaInvoice__c invoice : [SELECT id, JVCO_Invoice_Group__r.id, c2g__OutstandingValue__c, Name,
                                            JVCO_Invoice_Group__r.JVCO_Payment_Agreement__r.CreatedDate, JVCO_Outstanding_Amount_to_Process__c
                                            FROM c2g__codaInvoice__c 
                                            WHERE JVCO_Invoice_Group__r.id =: invGrp
                                            AND JVCO_Invoice_Group__r.JVCO_Payment_Agreement__r.CreatedDate != null])
        { 
            for(Integer i = 0; i < nthInstalment; i++)
            {
                Date getFirstCollectionDate = computeDueDate(Integer.valueOf(sched.PAYREC2__Day__c), Integer.valueOf(sched.PAYFISH3__First_Collection_Interval__c),
                                                    invoice.JVCO_Invoice_Group__r.JVCO_Payment_Agreement__r.CreatedDate);
                c2g__codaInvoiceInstallmentLineItem__c item = new c2g__codaInvoiceInstallmentLineItem__c();
                item.c2g__Invoice__c = invoice.Id;
                item.c2g__Amount__c = computePaidAmount(agreement, sched, totalAmount, invoice.JVCO_Outstanding_Amount_to_Process__c != null ? invoice.JVCO_Outstanding_Amount_to_Process__c : invoice.c2g__OutstandingValue__c, i+1) ;
                item.c2g__DueDate__c = getFirstCollectionDate.addMonths(i);
                if(item.c2g__Amount__c > 0 && (item.c2g__Amount__c * (i + 1) == agreement.PAYREC2__Total_Collected__c))
                {
                    item.JVCO_Paid_Amount__c = item.c2g__Amount__c;
                    item.JVCO_Payment_Status__c = 'Paid';
                }
                else if(item.c2g__Amount__c > 0 && (item.c2g__Amount__c * (i + 1) < agreement.PAYREC2__Total_Collected__c))
                {
                    item.JVCO_Paid_Amount__c = agreement.PAYREC2__Total_Collected__c - (item.c2g__Amount__c * (i + 1));
                    item.JVCO_Payment_Status__c = 'Part-Paid';
                }
                if(instlLineMap.get(invoice.Name) == null || !instlLineMap.get(invoice.Name).contains(item.c2g__DueDate__c))
                {
                    newItemList.add(item);
                }
            }
        }
        if(!newItemList.isEmpty())
        {
            insert newItemList;
        }
    }
    public static void updateDueDate(Set<Id> invIdSet)
    {
        Set<c2g__codaInvoiceInstallmentLineItem__c> instalmentLineToUpdate = new Set<c2g__codaInvoiceInstallmentLineItem__c>();
        for(c2g__codaInvoiceInstallmentLineItem__c instLine : [SELECT c2g__Invoice__r.Name, c2g__DueDate__c, JVCO_Paid_Amount__c,
                                                               JVCO_Payment_Status__c
                                                               from c2g__codaInvoiceInstallmentLineItem__c 
                                                               WHERE c2g__Invoice__c IN:invIdSet
                                                               AND JVCO_Payment_Status__c != 'Paid'
                                                              ])
        {
            if(instLine.c2g__DueDate__c < Date.Today())
            {
                instLine.c2g__DueDate__c = instLine.c2g__DueDate__c.addMonths(1);
                instalmentLineToUpdate.add(instLine);
            }
        }
        if(!instalmentLineToUpdate.isEmpty())
        {
            update new List<c2g__codaInvoiceInstallmentLineItem__c>(instalmentLineToUpdate);
        }
    }
    public static Double computePaidAmount(PAYREC2__Payment_Agreement__c agreement,
                                           PAYREC2__Payment_Schedule__c sched, Double totalAmount, Decimal invoiceAmount,
                                           Integer nthInstalment)
    {
        if (nthInstalment == 1)
        {
            if (agreement.PAYREC2__First_Collection_Amount__c == null)
            {
                return ((agreement.PAYREC2__Ongoing_Collection_Amount__c / totalAmount) * invoiceAmount).setScale(2);
            } else {
                return ((agreement.PAYREC2__First_Collection_Amount__c / totalAmount) * invoiceAmount).setScale(2);
            }
        } else if (nthInstalment == sched.PAYREC2__Max__c) {
            if (agreement.PAYREC2__Final_Collection_Amount__c == null)
            {
                return ((agreement.PAYREC2__Ongoing_Collection_Amount__c / totalAmount) * invoiceAmount).setScale(2);
            } else {
                return ((agreement.PAYREC2__Final_Collection_Amount__c / totalAmount) * invoiceAmount).setScale(2);
            }
        } else {
            return ((agreement.PAYREC2__Ongoing_Collection_Amount__c / totalAmount) * invoiceAmount).setScale(2);
        }
    }
    public static Date computeDueDate(Integer day, Integer collectionInterval, Datetime todays)
    {
        Integer monthAdded = todays.day() > day ? 1 : 0;
        DateTime dT = todays;
        Date today = date.newinstance(dT.year(), dT.month(), dT.day());
        Date dueDate = Date.newInstance(today.year(), today.month() + monthAdded, day);

        Date computedCollectionDate = today;
        Date firstMonday = Date.newInstance(1990, 1, 1);
        Integer workingDays = 0;
        while(workingDays < collectionInterval)
        {
            computedCollectionDate = computedCollectionDate.addDays(1);
            if(Math.mod(firstMonday.daysBetween(computedCollectionDate), 7) < 5)
            {  
                if(!gbHolidayDateSet.contains(computedCollectionDate) || 
                    (gbHolidayDateSet.contains(computedCollectionDate) && 
                        workingDays == (collectionInterval - 1)))
                {
                    workingDays++;
                } 
            }
        }
        return computedCollectionDate > dueDate ? dueDate.addMonths(1) : dueDate;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Creates a GB Holiday Sets
        Inputs: Date, Integer
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        03-Aug-2018 franz.g.a.dimaapi    GREEN-32568 - Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static void generateHolidays(Date d, Integer addedMonth)
    {
        if(gbHolidayDateSet.isEmpty())
        {
            for(JVCO_GBHoliday__c gbHoliday : [SELECT Id, JVCO_HolidayDate__c
                                                FROM JVCO_GBHoliday__c 
                                                WHERE JVCO_HolidayDate__c >= :d
                                                AND JVCO_HolidayDate__c <= :d.addMonths(3)
                                                AND JVCO_Is_Weekend__c = false])
            {
                gbHolidayDateSet.add(gbHoliday.JVCO_HolidayDate__c);
            }
        }    
    }
    public void finish(Database.BatchableContext BC){}
}