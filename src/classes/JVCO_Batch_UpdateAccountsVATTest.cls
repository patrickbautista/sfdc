/*
Created by: Desiree Quijada
Date Created: 01-August-2017
Date Modified: 03-August-2017
Details: Test class for class JVCO_Batch_UpdateAccountsVAT
Version: v1.0
*/

@isTest
private class JVCO_Batch_UpdateAccountsVATTest {
    @testSetup 
    static void createTestData() 
    {
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue;
        User u = [SELECT ID, NAME FROM USER WHERE NAME = 'Data Migration'];
        System.assertequals('Data Migration', u.Name, 'Not a data migration user');
        System.runAs(new User(Id=u.id))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTaxCode__c'); 
            listQueue.add(q2);
            queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaDimension1__c'); 
            listQueue.add(q3);
            insert listQueue;
            
            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }
        
        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
        testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc.Dimension_1_Required__c = false;
        testGeneralLedgerAcc.Dimension_2_Required__c = false;
        testGeneralLedgerAcc.Dimension_3_Required__c = false;
        testGeneralLedgerAcc.Dimension_4_Required__c = false;
        testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
        testGeneralLedgerAcc.ownerid = testGroup.Id;

        insert testGeneralLedgerAcc;
        
        //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }
        
        
        //insert tax Code - GB-O-CNV-STD
        List<c2g__codaTaxCode__c> taxCodeList = new List<c2g__codaTaxCode__c>();
        c2g__codaTaxCode__c tax1 = new c2g__codaTaxCode__c();
        tax1.Name = 'GB-O-CNV-STD';
        tax1.c2g__Description__c = 'testData';
        tax1.c2g__GeneralLedgerAccount__c = testGeneralLedgerAcc.id;
        tax1.ffvat__NetBox__c = 'Box 6';
        tax1.ffvat__TaxBox__c = 'Box 1';
        taxCodeList.add(tax1);
        
        //insert tax Code - GB-O-CNV-EXMPT 
        c2g__codaTaxCode__c tax2 = new c2g__codaTaxCode__c();
        tax2.Name = 'GB-O-CNV-EXMPT';
        tax2.c2g__Description__c = 'testData';
        tax2.c2g__GeneralLedgerAccount__c = testGeneralLedgerAcc.id;
        tax2.ffvat__NetBox__c = 'Box 6';
        tax2.ffvat__TaxBox__c = 'Box 1';
        taxCodeList.add(tax2);
        
        //insert tax Code - GB-O-CNV-ZERO
        c2g__codaTaxCode__c tax3 = new c2g__codaTaxCode__c();
        tax3.Name = 'GB-O-CNV-ZERO';
        tax3.c2g__Description__c = 'testData';
        tax3.c2g__GeneralLedgerAccount__c = testGeneralLedgerAcc.id;
        tax3.ffvat__NetBox__c = 'Box 6';
        tax3.ffvat__TaxBox__c = 'Box 1';
        taxCodeList.add(tax3);
        
        //insert tax Code - GB-O-STD-LIC 
        c2g__codaTaxCode__c tax4 = new c2g__codaTaxCode__c();
        tax4.Name = 'GB-O-STD-LIC';
        tax4.c2g__Description__c = 'testData';
        tax4.c2g__GeneralLedgerAccount__c = testGeneralLedgerAcc.id;
        tax4.ffvat__NetBox__c = 'Box 6';
        tax4.ffvat__TaxBox__c = 'Box 1';
        taxCodeList.add(tax4);
        
        //insert tax Code - GB-O-LIC-EXMPT
        c2g__codaTaxCode__c tax5 = new c2g__codaTaxCode__c();
        tax5.Name = 'GB-O-LIC-EXMPT';
        tax5.c2g__Description__c = 'testData';
        tax5.c2g__GeneralLedgerAccount__c = testGeneralLedgerAcc.id;
        tax5.ffvat__NetBox__c = 'Box 6';
        tax5.ffvat__TaxBox__c = 'Box 1';
        taxCodeList.add(tax5);
        
        //insert tax Code - GB-O-LIC-ZERO
        c2g__codaTaxCode__c tax6 = new c2g__codaTaxCode__c();
        tax6.Name = 'GB-O-LIC-ZERO';
        tax6.c2g__Description__c = 'testData';
        tax6.c2g__GeneralLedgerAccount__c = testGeneralLedgerAcc.id;
        tax6.ffvat__NetBox__c = 'Box 6';
        tax6.ffvat__TaxBox__c = 'Box 1';
        taxCodeList.add(tax6);
        insert taxCodeList;
        
        
        User dmUser = [SELECT ID, NAME FROM USER WHERE NAME = 'Data Migration'];
       
        System.runAs(new User(Id=dmUser.id))
        {
        
        //Create Account
        List<Account> custAccList = new List<Account>();
        
        Account testAccCust = new Account(Name='Test Customer Account');
        testAccCust.AccountNumber = '999999';
        testAccCust.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust.Type='Customer';
        testAccCust.RecordTypeId = accountRecordTypes.get('Customer Account');
        custAccList.add(testAccCust);
        
        //Create Account
        Account testAccCust2 = new Account(Name='Test Customer Account');
        testAccCust2.AccountNumber = '999888';
        testAccCust2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust2.Type='Customer';
        testAccCust2.RecordTypeId = accountRecordTypes.get('Customer Account');
        custAccList.add(testAccCust2);
        
        //Create Account
        Account testAccCust3 = new Account(Name='Test Customer Account');
        testAccCust3.AccountNumber = '999777';
        testAccCust3.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust3.Type='Customer';
        testAccCust3.RecordTypeId = accountRecordTypes.get('Customer Account');
        custAccList.add(testAccCust3);
        insert custAccList;
        
        //create contacts for licence account 2 and 3
        List<Contact> conList = new List<Contact>();
        Contact c = new Contact();
        c.LastName = 'Contact2';
        c.FirstName = 'Test2';
        c.Email = 'test2@test.com';
        c.AccountId = testAccCust2.id;
        conList.add(c);        
        
        Contact c1 = new Contact();
        c1.LastName = 'Contact3';
        c1.FirstName = 'Test3';
        c1.Email = 'test3@test.com';
        c1.AccountId = testAccCust3.id;
        conList.add(c1);
        insert conList;              
        
        List<Account> custAcc = [SELECT ID, NAME FROM Account LIMIT 3];
        c2g__codaTaxCode__c taxZERO = [SELECT ID, NAME FROM c2g__codaTaxCode__c WHERE Name = 'GB-O-CNV-ZERO'];
        c2g__codaTaxCode__c taxSTD = [SELECT ID, NAME FROM c2g__codaTaxCode__c WHERE Name = 'GB-O-CNV-STD'];
        c2g__codaTaxCode__c taxEXMPT = [SELECT ID, NAME FROM c2g__codaTaxCode__c WHERE Name = 'GB-O-CNV-EXMPT'];
        
        List <Account> accountlist = new List<Account>();
        Account testAcc = JVCO_TestClassObjectBuilder.createLicenceAccount(testAccCust.id);
        testAcc.AccountNumber = '987654';
        testAcc.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAcc.c2g__CODAAllowDeleteInUse__c = false;
        testAcc.c2g__CODACreditLimitEnabled__c = false;
        testAcc.c2g__CODACreditStatus__c = 'Credit Allowed';
        testAcc.c2g__CODADaysOffset1__c = 0.0;
        testAcc.c2g__CODADiscount1__c = 0.0;
        testAcc.c2g__CODAFederallyReportable1099__c = false;
        testAcc.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testAcc.c2g__CODAIntercompanyAccount__c = false;
        testAcc.c2g__CODASalesTaxStatus__c = 'Taxable';
        testAcc.c2g__CODAOutputVATCode__c = taxSTD.id;
        testAcc.JVCO_Customer_Account__c = custAcc[0].id;
        accountlist.add(testAcc);
        
        Account testAcc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(testAccCust2.id);
        testAcc2.AccountNumber = '998154';
        testAcc2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAcc2.c2g__CODAAllowDeleteInUse__c = false;
        testAcc2.c2g__CODACreditLimitEnabled__c = false;
        testAcc2.c2g__CODACreditStatus__c = 'Credit Allowed';
        testAcc2.c2g__CODADaysOffset1__c = 0.0;
        testAcc2.c2g__CODADiscount1__c = 0.0;
        testAcc2.c2g__CODAFederallyReportable1099__c = false;
        testAcc2.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testAcc2.c2g__CODAIntercompanyAccount__c = false;
        testAcc2.c2g__CODASalesTaxStatus__c = 'Taxable';
        testAcc2.c2g__CODAOutputVATCode__c = taxZERO.id;
        testAcc2.JVCO_Customer_Account__c = custAcc[1].id;
        testAcc2.JVCO_Billing_Contact__c = c.id;
        testAcc2.JVCO_Licence_Holder__c = c.id;
        testAcc2.JVCO_Review_Contact__c = c.id;
        accountlist.add(testAcc2);
        
        Account testAcc3 = JVCO_TestClassObjectBuilder.createLicenceAccount(testAccCust3.id);
        testAcc3.AccountNumber = '998156';
        testAcc3.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAcc3.c2g__CODAAllowDeleteInUse__c = false;
        testAcc3.c2g__CODACreditLimitEnabled__c = false;
        testAcc3.c2g__CODACreditStatus__c = 'Credit Allowed';
        testAcc3.c2g__CODADaysOffset1__c = 0.0;
        testAcc3.c2g__CODADiscount1__c = 0.0;
        testAcc3.c2g__CODAFederallyReportable1099__c = false;
        testAcc3.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testAcc3.c2g__CODAIntercompanyAccount__c = false;
        testAcc3.c2g__CODASalesTaxStatus__c = 'Taxable';
        testAcc3.c2g__CODAOutputVATCode__c = taxEXMPT.id;
        testAcc3.JVCO_Customer_Account__c = custAcc[2].id;
        testAcc3.JVCO_Billing_Contact__c = c1.id;
        testAcc3.JVCO_Licence_Holder__c = c1.id;
        testAcc3.JVCO_Review_Contact__c = c1.id;
        accountlist.add(testAcc3);
        insert accountList;
        }

        
    }
    
    @isTest
    static void testVATMapping()
    {    
        List<Account> acc = [SELECT ID, Name, c2g__CODAOutputVATCode__c, Account.c2g__CODAOutputVATCode__r.Name FROM Account where c2g__CODAOutputVATCode__R.Name = 'GB-O-CNV-STD' OR c2g__CODAOutputVATCode__R.Name = 'GB-O-CNV-EXMPT' OR c2g__CODAOutputVATCode__R.Name = 'GB-O-CNV-ZERO'];
        System.debug(acc);
        Test.startTest();
        JVCO_Batch_UpdateAccountsVAT b = new JVCO_Batch_UpdateAccountsVAT();
        Database.executeBatch(b);
        Test.stopTest();
    }
}