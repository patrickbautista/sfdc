/**************************************************************
* @Author: 
* @Company: Accenture
* @Description:
* @Created Date: Test Class for JVCO_TransferToCashWrapperClass
* @Revisions:
*      <Name>                     <Date>          <Description>
*      Jason Mactal               10.18.2017      Edited Test Data to Avoid Validation Error
*      filip.bezak@accenture.com 28-May-2018     GREEN-31894 - TCP Incorrect value for Cash Matching journals
*************************************************************/
@isTest
private class JVCO_TransferToCashWrapperClassTest
{
    
}