/* --------------------------------------------------------------------------------------------------
    Name:            JVCO_PayonomyPaymentCreditCardScheduler.cls 
    Description:     Class that handles JVCO_PayonomyPaymentProcessingBatch for Sage Pay
                     GREEN-24853    
    Test class:      

    Date             Version     Author                Summary of Changes 
    -----------      -------     -----------------     --------------------
    16-Oct-2017       0.1        franz.g.a.dimaapi      Intial draft
    09-Feb-2018       0.2        franz.g.a.dimaapi      GREEN-29542    
------------------------------------------------------------------------------------------------ */
public class JVCO_PayonomyPaymentCreditCardScheduler /*implements Schedulable*/{
    
    /*private final String CREDIT_CARD = 'SagePay';
    private final JVCO_General_Settings__c GENERAL_SETTINGS = JVCO_General_Settings__c.getInstance();
    private final Decimal PROCESSINGLIMIT = GENERAL_SETTINGS != null ? GENERAL_SETTINGS.JVCO_PayonomyPaymentProcessingBatchLimit__c : 20;

    public void execute(SchedulableContext sc)
    { 
        Database.executeBatch(new JVCO_PayonomyPaymentProcessingBatch(CREDIT_CARD), Integer.valueOf(PROCESSINGLIMIT));
    }

    private static String getSchedulerExpression(DateTime dt) 
    {
         return '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();    
    }

    public static void callExecuteMomentarily()  
    {
        System.schedule('JVCO_PayonomyPaymentCreditCardScheduler', getSchedulerExpression(DateTime.Now().addSeconds(5)), new JVCO_PayonomyPaymentCreditCardScheduler());
    }*/
}