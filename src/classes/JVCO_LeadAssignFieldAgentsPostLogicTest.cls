/* ----------------------------------------------------------------------------------------------
   Name: JVCO_VenueAssignFieldAgentsPostLogicTest.cls 
   Description: Test Class for JVCO_LeadAssignFieldAgentsPostCodeLogic

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  08-Sep-2016   0.1         mark.glenn.b.ayson  Intial creation
  ----------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_LeadAssignFieldAgentsPostLogicTest {
    
    /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: Sets up the lead record
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    08-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    @testSetup static void setUpTestData()
    {
        Lead leadRecord = new Lead();
        leadRecord.Status = 'Open';
        leadRecord.FirstName = 'Pippa';
        leadRecord.LastName = 'Ayson';
        leadRecord.Company = 'Kumon Pub';
        leadRecord.Email = 'testEmail@domain.com';

        // Query the profile System Administrator
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        // Create User
        User u = new User(Alias = 'utest', Email='unit.test@unit.test.com',
                          EmailEncodingKey='UTF-8', LastName='Unit Test', 
                          LanguageLocaleKey='en_US', LocaleSIdKey='en_GB', ProfileId = p.Id,
                          TimezoneSIdKey='Europe/London', Username='unit.test@unit.test.com',
                          Division='Legal', Department='Legal');

        insert u;        

        System.runAs(u)
        {
            // Check if the Queue exist
            List<Group> groupToCheckList = [SELECT Id FROM Group WHERE Name LIKE 'Unresolved Addresses%'];

            // If the Queue does not exist, create the queue
            if (groupToCheckList.size() < 1)
            {
                List<Group> groupList = new List<Group>{
                    (new Group(Name='Unresolved Addresses', type='Queue'))
                };
                insert groupList;

                List<QueueSObject> queueList = new List<QueueSObject>();
                for (Group g : groupList)
                {
                    queueList.add(new QueueSObject(QueueID=g.id, SobjectType='Lead'));
                    queueList.add(new QueueSObject(QueueID=g.id, SobjectType='JVCO_Venue__c'));
                }
                insert queueList;
            }

            // Create Venue record
            insert leadRecord;
        }
    }

    /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: A test method with a valid postcode and field visit checkbox is ticked
                 The record also has a field agent and the owner ID will be updated
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    08-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    @isTest static void testRecordHasOwner() 
    {
        Lead leadRecord = [SELECT ID, status, FirstName, LastName, Company
                             FROM Lead
                             WHERE Company = 'Kumon Pub'
                                AND LastName = 'Ayson'
                                AND FirstName = 'Pippa'];

        leadRecord.Title = 'Doctor';
        leadRecord.Email = 'email@email.com';
        leadRecord.JVCO_Venue_Street__c = 'London';
        leadRecord.JVCO_Venue_City__c = 'London';
        leadRecord.JVCO_Venue_County__c = 'London';
        leadRecord.JVCO_Venue_Postcode__c = 'WA2 1AA';
        leadRecord.JVCO_Venue_Country__c = 'United Kingdom';
        leadRecord.Industry = 'Retail';
        leadRecord.JVCO_Venue_Type__c = 'Public House/Bar';
        leadRecord.JVCO_Venue_Sub_Type__c = 'Sports Bar';
        leadRecord.Industry = 'Retail';
        leadRecord.JVCO_Customer_has_same_address__c = true;
        leadRecord.JVCO_Field_Visit_Requested__c = true;
        //leadRecord.JVCO_Field_Visit_Reason__c = 'Churn';
        leadRecord.JVCO_Field_Visit_Requested_Date__c = date.today();
        leadRecord.JVCO_Field_Visit_Description__c = 'Check location';

        test.startTest();
     
        update leadRecord;
        
        test.stopTest();

        //Id validationUser = [SELECT ID
        //                       FROM User
        //                       WHERE username = 'james.melville@accenture.com.prsppl.dev1'].Id;

        Lead updatedRecord = [SELECT ID, ownerId, JVCO_Region__c
                             FROM Lead
                             WHERE Company = 'Kumon Pub'
                                AND LastName = 'Ayson'
                                AND FirstName = 'Pippa'];

     //   System.assertEquals(validationUser, updatedRecord.ownerId, 'Assertion failed - Incorrect Owner ID');
     //   System.assertEquals('Scotland', updatedRecord.JVCO_Region__c, 'Assertion failed - Incorrect Region');
    }

    /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: A test method with a valid postcode and field visit checkbox is ticked
                 The record does not match any field sales and will be updated to Queue
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    07-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    @isTest static void testRecordHasQueueAsOwner() 
    {
        Lead leadRecord = [SELECT ID, status, FirstName, LastName, Company
                             FROM Lead
                             WHERE Company = 'Kumon Pub'
                                AND LastName = 'Ayson'
                                AND FirstName = 'Pippa'];

        leadRecord.Title = 'Doctor';
        leadRecord.Email = 'email@email.com';
        leadRecord.JVCO_Venue_Street__c = 'London';
        leadRecord.JVCO_Venue_City__c = 'London';
        leadRecord.JVCO_Venue_County__c = 'London';
        leadRecord.JVCO_Venue_Postcode__c = 'WA2 1AA';
        leadRecord.JVCO_Venue_Country__c = 'United Kingdom';
        leadRecord.Industry = 'Retail';
        leadRecord.JVCO_Venue_Type__c = 'Public House/Bar';
        leadRecord.JVCO_Venue_Sub_Type__c = 'Sports Bar';
        leadRecord.Industry = 'Retail';
        leadRecord.JVCO_Customer_has_same_address__c = true;
        leadRecord.JVCO_Field_Visit_Requested__c = true;
        //leadRecord.JVCO_Field_Visit_Reason__c = 'Churn';
        leadRecord.JVCO_Field_Visit_Requested_Date__c = date.today();
        leadRecord.JVCO_Field_Visit_Description__c = 'Check location';

        test.startTest();
        update leadRecord;
        test.stopTest();

        Id validationUser = [SELECT Id 
                               FROM Group 
                               WHERE Name = 'Unresolved Addresses'].Id;

        Lead updatedRecord = [SELECT ID, ownerId, JVCO_Region__c
                             FROM Lead
                             WHERE Company = 'Kumon Pub'
                                AND LastName = 'Ayson'
                                AND FirstName = 'Pippa'];

        System.assertNotEquals(validationUser, updatedRecord.ownerId, 'Assertion failed - Incorrect Owner ID');        
    }

    /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: A test method that will insert a new venue record with an unknown postcode
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    08-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
  @isTest static void testRecordManualInsertWithUnknownPostcode() 
  {
        Lead leadRecord = new Lead();
        leadRecord.Status = 'Open';
        leadRecord.FirstName = 'Pippa';
        leadRecord.LastName = 'Ayson';
        leadRecord.Company = 'Pippa Company 2';
        leadRecord.Title = 'Doctor';
        leadRecord.Email = 'email@email.com';
        leadRecord.JVCO_Venue_Street__c = 'London';
        leadRecord.JVCO_Venue_City__c = 'London';
        leadRecord.JVCO_Venue_County__c = 'London';
        leadRecord.JVCO_Venue_Postcode__c = 'WA2 1AA';
        leadRecord.JVCO_Venue_Country__c = 'United Kingdom';
        leadRecord.Industry = 'Retail';
        leadRecord.JVCO_Venue_Type__c = 'Public House/Bar';
        leadRecord.JVCO_Venue_Sub_Type__c = 'Sports Bar';
        leadRecord.Industry = 'Retail';
        leadRecord.JVCO_Customer_has_same_address__c = true;
        leadRecord.JVCO_Field_Visit_Requested__c = true;
        //leadRecord.JVCO_Field_Visit_Reason__c = 'Churn';
        leadRecord.JVCO_Field_Visit_Requested_Date__c = date.today();
        leadRecord.JVCO_Field_Visit_Description__c = 'Check location';

        test.startTest();
        insert leadRecord;
        test.stopTest();

        Id validationUser =  [SELECT Id 
                               FROM Group 
                               WHERE Name = 'Unresolved Addresses'].Id;

        Lead insertedRecord = [SELECT ID, ownerId
                                        FROM Lead
                                        WHERE Company = 'Pippa Company 2'
                                            AND LastName = 'Ayson'
                                            AND FirstName = 'Pippa'];
        System.assertNotEquals(validationUser, insertedRecord.ownerId, 'Assertion failed - Incorrect Owner ID');
  }

  /* ----------------------------------------------------------------------------------------------
        Author: mark.glenn.b.ayson
        Company: Accenture
        Description: A test method with a valid postcode and field visit checkbox is ticked
                     The record does not match any field sales and will be updated to Queue
                     The checkbox will be unticked
        Inputs: N/A
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        08-Sep-2016 mark.glenn.b.ayson  Initial version of function
      ----------------------------------------------------------------------------------------------- */
    @isTest static void testRecordCheckIfThereIsAChangeInTheTwoFields() 
    {
        Lead leadRecord = [SELECT ID, status, FirstName, LastName, Company
                             FROM Lead
                             WHERE Company = 'Kumon Pub'
                                AND LastName = 'Ayson'
                                AND FirstName = 'Pippa'];
        leadRecord.Title = 'Doctor';
        leadRecord.Email = 'email@email.com';
        leadRecord.JVCO_Venue_Street__c = 'London';
        leadRecord.JVCO_Venue_City__c = 'London';
        leadRecord.JVCO_Venue_County__c = 'London';
        leadRecord.JVCO_Venue_Postcode__c = 'WA2 1AA';
        leadRecord.JVCO_Venue_Country__c = 'United Kingdom';
        leadRecord.Industry = 'Retail';
        leadRecord.JVCO_Venue_Type__c = 'Public House/Bar';
        leadRecord.JVCO_Venue_Sub_Type__c = 'Sports Bar';
        leadRecord.JVCO_Customer_has_same_address__c = true;
        leadRecord.JVCO_Field_Visit_Requested__c = true;
        //leadRecord.JVCO_Field_Visit_Reason__c = 'Churn';
        leadRecord.JVCO_Field_Visit_Requested_Date__c = date.today();
        leadRecord.JVCO_Field_Visit_Description__c = 'Check location';

        Id validationUser =  [SELECT Id 
                               FROM Group 
                               WHERE Name = 'Unresolved Addresses'].Id;
        test.startTest();
        update leadRecord; 

        Lead updatedRecord = [SELECT ID, ownerId
                                     FROM Lead
                                     WHERE ID = :leadRecord.Id];

        updatedRecord.JVCO_Field_Visit_Requested__c = false;


        update updatedRecord;
        test.stopTest();

        Lead finalRecord = [SELECT ID, ownerId
                                     FROM Lead
                                     WHERE ID = :updatedRecord.Id];
        
        System.assertNotEquals(validationUser, updatedRecord.ownerId, 'Assertion failed - Incorrect Owner ID');
    }

    /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: A test method that will insert a new venue record with an known postcode
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    08-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
  @isTest static void testRecordManualInsertWithKnownPostcode() 
  {
        Lead leadRecord = new Lead();
        leadRecord.Status = 'Open';
        leadRecord.FirstName = 'Pippa';
        leadRecord.LastName = 'Ayson';
        leadRecord.Company = 'Pippa Company 2';
        leadRecord.Title = 'Doctor';
        leadRecord.Email = 'email@email.com';
        leadRecord.JVCO_Venue_Street__c = 'London';
        leadRecord.JVCO_Venue_City__c = 'London';
        leadRecord.JVCO_Venue_County__c = 'London';
        leadRecord.JVCO_Venue_Postcode__c = 'WA2 1AA';
        leadRecord.JVCO_Venue_Country__c = 'United Kingdom';
        leadRecord.Industry = 'Retail';
        leadRecord.JVCO_Venue_Type__c = 'Public House/Bar';
        leadRecord.JVCO_Venue_Sub_Type__c = 'Sports Bar';
        leadRecord.JVCO_Customer_has_same_address__c = true;
        leadRecord.JVCO_Field_Visit_Requested__c = true;
        //leadRecord.JVCO_Field_Visit_Reason__c = 'Churn';
        leadRecord.JVCO_Field_Visit_Requested_Date__c = date.today();
        leadRecord.JVCO_Field_Visit_Description__c = 'Check location';

        test.startTest();
        insert leadRecord;
        test.stopTest();

        //Id validationUser =  [SELECT ID
        //                       FROM User
        //                       WHERE username = 'james.melville@accenture.com.prsppl.dev1'].Id;

        Lead insertedRecord = [SELECT ID, ownerId
                                        FROM Lead
                                        WHERE Company = 'Pippa Company 2'
                                            AND LastName = 'Ayson'
                                            AND FirstName = 'Pippa'];

     //   System.assertEquals(validationUser, insertedRecord.ownerId, 'Assertion failed - Incorrect Owner ID');
  }
}