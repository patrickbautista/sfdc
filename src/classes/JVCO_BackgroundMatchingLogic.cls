/* -------------------------------------------------------------------------------------------------------
    Name:            JVCO_BackgroundMatchingLogic.cls 
    Description:     Logic Handler for JVCO_BackgroundMatchingLogic
    Test class:      

    Date            Version     Author                             Summary of Changes
    -----------     -------     -----------------                  ----------------------------------------
    28-Apr-2017     0.1         franz.g.a.dimaapi                  Intial draft
    02-May-2017     0.2         franz.g.a.dimaapi                  Added All Basic Matching Criteria
    04-May-2017     0.3         franz.g.a.dimaapi                  Added Write-Off functionality for Cash-To-Invoice Matching
    31-Jul-2017     0.4         franz.g.a.dimaapi                  Update Journal Matching Logic
    03-Jul-2017     0.5         franz.g.a.dimaapi                  Added CreditNote Matching with Reference
    04-Aug-2017     0.6         mel.andrei.b.santos                Changed DML statement to database.insert as per Ticket GREEN-17932 - Mel Andrei Santos 03-08-2017
    22-Aug-2017     0.7         mel.andrei.b.santos                Added try catch method for error handling in inserting error log records
    24-Aug-2017     0.8         mel.andrei.b.santos                Refactored inputting of fields
    14-Sep-2017     0.9         mel.andrei.b.Santos                Removed the if conditions as the null check in the error logs as per James
    29-Sep-2017     1.0         jules.osberg.a.pablo               Added GREEN-23561 fix for List Duplicates
    06-Dec-2017     1.1         csaba.feher                        Added GREEN-24996 fix for multiple invoice match
    13-Dec-2017     1.2         john.patrick.valdez                Removed Email Function
    10-Jan-2018     1.3         filip.bezak@accenture.com          GREEN-28083 - TCP - AP Bank Account Used
    07-Feb-2018     1.4         filip.bezak@accenture.com          GREEN-29925 - TC2P - Logic for calculation of C->CR->C on Journals implemented
    07-Feb-2018     1.5         filip.bezak@accenture.com          Logic for preventing the creation of Journal when matching against Sales Invoice without Billing Invoice is performed
    22-Feb-2018     1.6         franz.g.a.dimaapi                  Commented out all Sales CreditNote Functionality 
    07-Mar-2018     1.7         mary.ann.a.ruelan                  GREEN-30711, updated setSalesInstallmentLIMap to consider Invoice Legacy Number in Account Reference
    10-Jun-2019     1.8         patrick.t.bautista                 GREEN-34639, Remove callTransferToCash() method
------------------------------------------------------------------------------------------------------- */
public class JVCO_BackgroundMatchingLogic
{   
    //Prevent the trigger to call this logic
    public static Boolean stopMatching = false;
    //Transaction Line of Matching From
    @TestVisible
    private static Map<Id, c2g__codaTransactionLineItem__c> matchFromtransLineMap = new Map<Id, c2g__codaTransactionLineItem__c>();
    //Transaction Line of Matching To by Line Reference
    @TestVisible
    private static Map<String, c2g__codaTransactionLineItem__c> matchToLineReferenceMap = new Map<String, c2g__codaTransactionLineItem__c>();
    //Map By Account Id to List of Transaction Line
    @TestVisible
    private static Map<Id, List<c2g__codaTransactionLineItem__c>> matchToTransLineByAccMap = new Map<Id, List<c2g__codaTransactionLineItem__c>>();
    //Map By Sales Invoice Name to List of InstallmentItem
    @TestVisible
    private static Map<String, List<c2g__codaInvoiceInstallmentLineItem__c>> installmentLineByInvMap = new Map<String, List<c2g__codaInvoiceInstallmentLineItem__c>>();
    
    public static Map<Id, Id> licAndCustAccIdMap = new Map<Id, Id>();
    //Account Id Set
    @TestVisible
    private static Set<Id> accIdSet = new Set<Id>();
    //Journal Line Reference Set
    @TestVisible
    private static Set<String> journalLineReferenceSet = new Set<String>();
    //Cash Entry Line Reference Set
    @TestVisible
    private static Set<String> cashLineReferenceSet = new Set<String>();
    //Credit Note Reference Set
    @TestVisible
    private static Set<String> creditNoteReferenceSet = new Set<String>();

    //List of Background Matching Wrapper
    @TestVisible
    private static List<JVCO_BackgroundMatchingWrapper> bgmWrapperList = new List<JVCO_BackgroundMatchingWrapper>();
    //Write Off Threshold
    private static final Double WRITE_OFF_LIMIT = Math.abs(c2g__codaCashMatchingSettings__c.getInstance().JVCO_Write_off_Limit__c);
    private static Map<String, GLA_for_Cash_Transfer__c> allowedAccountsForCashTransferList;

    /* --------------------------------------------------------------------------------------------------
        Author:         franz.g.a.dimaapi@accenture.com
        Company:        Accenture
        Description:    This method is the controller of this class calling
                       all the necessary functions for matching

        Inputs:         newTliMap - Map of newly insert Transaction Line Item
        Returns:        
        <Date>          <Authors Name>             <Brief Description of Change> 
        28-Apr-2017     franz.g.a.dimaapi          Initial version of the code
    ------------------------------------------------------------------------------------------------------*/
    public static void matchingLogic(Map<Id, c2g__codaTransactionLineItem__c> newTliMap, Boolean isBatch){
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        setMatchFromTransLineMap(newTliMap, isBatch);
        if(!matchFromtransLineMap.isEmpty()){
            setupMatchDetails();
            if(!matchToLineReferenceMap.isEmpty() || !matchToTransLineByAccMap.isEmpty())
            {
                if(allowedAccountsForCashTransferList == null)
                {
                    allowedAccountsForCashTransferList = GLA_for_Cash_Transfer__c.getAll();
                }
                setupBackgroundMatchingWrapper();
                runMatchingAPI();
            }   
        }  
    }
    
    /* --------------------------------------------------------------------------------------------------
        Author:         franz.g.a.dimaapi@accenture.com
        Company:        Accenture
        Description:    This method setups the map to match to

        Inputs:         
        Returns:        
        <Date>          <Authors Name>             <Brief Description of Change>
        28-Apr-2017     franz.g.a.dimaapi          Initial version of the code
        07-Feb-2018     filip.bezak@accenture.com  Added line with sInv2BInvListMap
    ------------------------------------------------------------------------------------------------------*/
    private static void setupMatchDetails()
    {
        setReferenceSet();
        if(!journalLineReferenceSet.isEmpty() || !cashLineReferenceSet.isEmpty() || !creditNoteReferenceSet.isEmpty())
        {
            setMatchToTransLineMap();
            //setSalesInstallmentLIMap();
        }
        if(!accIdSet.isEmpty())
        {
            setMatchToTransLineByAccMap();
        }
    }

    /* --------------------------------------------------------------------------------------------------
        Author:         franz.g.a.dimaapi@accenture.com
        Company:        Accenture
        Description:    This method is the controller for running the FF Matching API and Transfer to Cash

        Inputs:         
        Returns:        
        <Date>          <Authors Name>              <Brief Description of Change> 
        28-Apr-2017     franz.g.a.dimaapi           Initial version of the code
        03-May-2017     franz.g.a.dimaapi           Add Transfer to Cash Functionality
        08-May-2017     franz.g.a.dimaapi           Add Sales InstallmentLine Functionality for Cash to Inv
        09-May-2017     franz.g.a.dimaapi           Add Background Matching Email
        03-Aug-2017     mel.andrei.b.santos         Changed DML statement to database.insert as per Ticket GREEN-17932 - Mel Andrei Santos 03-08-2017
        22-Aug-2017     mel.andrei.b.santos         Added try catch method for error handling in inserting error log records
        14-Sep-2017     mel.andrei.b.Santos         Removed the if conditions as the null check in the error logs
        06-Dec-2017     csaba.feher                 Modifying if condition for setting up to transline for GREEN-24996
        10-Jan-2018     filip.bezak@accenture.com   GREEN-28083 - TCP - AP Bank Account Used - transfer to cash not called for all matches
    ------------------------------------------------------------------------------------------------------*/
    private static void runMatchingAPI()
    {
        for(JVCO_BackgroundMatchingWrapper bgmWrapper : bgmWrapperList)
        {
            //If Match By Account update the matchToTransline
            if(bgmWrapper.isFromAccount && bgmWrapper.matchToTransLine == null)
            {
                bgmWrapper.matchToTransLine = bgmWrapper.getMatchToTransLineByAcc();
            }
            //Run FFMatchingApi and return the matching reference Id
            licAndCustAccIdMap = bgmWrapper.runMatchingAPI();
        }
    }

    /* --------------------------------------------------------------------------------------------------
        Author:         franz.g.a.dimaapi@accenture.com
        Company:        Accenture
        Description:    This method setup the Wrapper Class of this class

        Inputs:         
        Returns:        
        <Date>          <Authors Name>             <Brief Description of Change> 
        28-Apr-2017     franz.g.a.dimaapi          Initial version of the code
        04-May-2017     franz.g.a.dimaapi          Add Write Off Functionality
        25-Jul-2017     franz.g.a.dimaapi          Update Match by Acc Reference Logic
        11-Aug-2017     franz.g.a.dimaapi          Moved the Logic for setting up backgroundmatchingwrapper for each transaction type
    ------------------------------------------------------------------------------------------------------*/
    private static void setupBackgroundMatchingWrapper()
    {
        for(c2g__codaTransactionLineItem__c tli : matchFromtransLineMap.values())
        {
            JVCO_BackgroundMatchingWrapper bgmWrapper = new JVCO_BackgroundMatchingWrapper();
            //Transaction Line Item To Match From
            bgmWrapper.matchFromTransLine = tli;
            //Cash
            if(tli.c2g__Transaction__r.c2g__TransactionType__c == 'Cash')
            {
                String cashType = tli.c2g__Transaction__r.c2g__CashEntry__r.c2g__Type__c;
                if(cashType == 'Receipt' || cashType == 'Refund')
                {
                    setBackgroundMatchingWrapperForCash(bgmWrapper, tli, cashType);
                    bgmWrapperList.add(bgmWrapper);
                }   
            //Journal
            }else if(tli.c2g__Transaction__r.c2g__TransactionType__c == 'Journal')
            {
                setBackgroundMatchingWrapperForJournal(bgmWrapper, tli);
                bgmWrapperList.add(bgmWrapper);
            //Invoice
            }else if(tli.c2g__Transaction__r.c2g__TransactionType__c == 'Invoice')
            {
                setBackgroundMatchingWrapperForInvoice(bgmWrapper, tli);
                bgmWrapperList.add(bgmWrapper);
            }
        }   
    }
    
     /* --------------------------------------------------------------------------------------------------
        Author:         franz.g.a.dimaapi@accenture.com
        Company:        Accenture
        Description:    This method setup the matchFromtransLineMap

        Inputs:         transLineIdSet - Set of Transaction Line Id
        Returns:        
        <Date>          <Authors Name>             <Brief Description of Change> 
        28-Apr-2017     franz.g.a.dimaapi          Initial version of the code
        10-Jan-2018     filip.bezak@accenture.com  GREEN-28083 - TCP - AP Bank Account Used
        02-Feb-2018     filip.bezak@accenture.com  Removed Credit Note
    ------------------------------------------------------------------------------------------------------*/
    private static void setMatchFromTransLineMap(Map<Id, c2g__codaTransactionLineItem__c> newTliMap, Boolean isBatch)
    {
        if(isBatch){
            matchFromtransLineMap = newTliMap; 
        }else{
        matchFromtransLineMap = new Map<Id, c2g__codaTransactionLineItem__c>([SELECT Id, Name, c2g__AccountOutstandingValue__c,
                                                                              c2g__Account__c, 
                                                                              c2g__Account__r.JVCO_Customer_Account__c,
                                                                              c2g__LineReference__c,
                                                                              c2g__OwnerCompany__r.c2g__CustomerWriteOff__c,
                                                                              c2g__Transaction__r.c2g__Period__c,
                                                                              c2g__Transaction__r.c2g__Period__r.c2g__Closed__c,
                                                                              c2g__Transaction__r.c2g__TransactionType__c,
                                                                              c2g__Transaction__r.c2g__DocumentDescription__c,
                                                                              c2g__Transaction__r.c2g__CashEntry__r.c2g__Type__c,
                                                                              c2g__Transaction__r.c2g__SalesInvoice__c,
                                                                              c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Invoice_Legacy_Number__c,
                                                                              c2g__Transaction__r.c2g__SalesCreditNote__c,
                                                                              c2g__Transaction__r.c2g__SalesCreditNote__r.c2g__Invoice__c,
                                                                              c2g__Transaction__r.c2g__SalesCreditNote__r.c2g__Invoice__r.Name,
                                                                              c2g__Transaction__r.c2g__Journal__r.c2g__Type__c,
                                                                              c2g__Transaction__r.c2g__CashEntry__r.c2g__BankAccount__r.c2g__GeneralLedgerAccount__r.Name,
                                                                              c2g__LineDescription__c 
                                                                              FROM c2g__codaTransactionLineItem__c 
                                                                              WHERE Id IN :newTliMap.keySet()
                                                                              AND c2g__MatchingStatus__c IN ('Available')
                                                                              AND c2g__AccountValue__c != 0
                                                                              AND c2g__LineType__c IN ('Account')
                                                                              AND c2g__Transaction__r.c2g__TransactionType__c IN ('Cash','Invoice','Journal')]);
        }
    }
    
    /* --------------------------------------------------------------------------------------------------
        Author:         franz.g.a.dimaapi@accenture.com
        Company:        Accenture
        Description:    This method setup cashLineReferenceSet
                                         accIdSet
                                         journalLineReferenceSet

        Inputs:         
        Returns:        
        <Date>          <Authors Name>             <Brief Description of Change> 
        28-Apr-2017     franz.g.a.dimaapi   Initial version of the code
        03-May-2017     franz.g.a.dimaapi   Added criteria to not get the Transfer Cash Transactions
    ------------------------------------------------------------------------------------------------------*/
    @TestVisible
    private static void setReferenceSet()
    {
        for(c2g__codaTransactionLineItem__c tli : matchFromtransLineMap.values())
        {
            //Cash
            if(tli.c2g__Transaction__r.c2g__TransactionType__c == 'Cash')
            {
                //Receipt
                if(tli.c2g__Transaction__r.c2g__CashEntry__r.c2g__Type__c == 'Receipt')
                {
                    //Check if Account Reference is available
                    if(tli.c2g__LineReference__c != null)
                    {
                        cashLineReferenceSet.add(tli.c2g__LineReference__c);
                    }else{
                        accIdSet.add(tli.c2g__Account__c);  
                    }
                //Refund
                }else if(tli.c2g__Transaction__r.c2g__CashEntry__r.c2g__Type__c == 'Refund')
                {
                    accIdSet.add(tli.c2g__Account__c);
                }
            //Journal
            //Check If it's not for Transfer To Cash and only for Manual Journal
            }else if(tli.c2g__Transaction__r.c2g__TransactionType__c == 'Journal' &&
                tli.c2g__Transaction__r.c2g__Journal__r.c2g__Type__c == 'Manual Journal' &&
                tli.c2g__Transaction__r.c2g__DocumentDescription__c != 'Transfer Cash to Parent')
            {
                //Check if Line Reference is available
                if(tli.c2g__LineReference__c != null)
                {
                    journalLineReferenceSet.add(tli.c2g__LineReference__c);
                }
                else{
                    accIdSet.add(tli.c2g__Account__c);
                }
            //Invoice
            }else
            {
                accIdSet.add(tli.c2g__Account__c);
            }
        }
    }

    /* --------------------------------------------------------------------------------------------------
        Author:         franz.g.a.dimaapi@accenture.com
        Company:        Accenture
        Description:    This method setup matchToLineReferenceMap

        Inputs:         
        Returns:        
        <Date>          <Authors Name>             <Brief Description of Change> 
        28-Apr-2017     franz.g.a.dimaapi          Initial version of the code
        07-Feb-2018     filip.bezak@accenture.com  Populating the sInvSet
    ------------------------------------------------------------------------------------------------------*/
    private static void setMatchToTransLineMap()
    {
        for(c2g__codaTransactionLineItem__c tli : [SELECT Id, Name, c2g__Account__c,
                                                   c2g__Account__r.JVCO_Customer_Account__c,
                                                   c2g__LineDescription__c,
                                                   c2g__AccountOutstandingValue__c,
                                                   c2g__Transaction__r.c2g__Period__c,
                                                   c2g__Transaction__r.c2g__Period__r.c2g__Closed__c,
                                                   c2g__Transaction__r.c2g__TransactionType__c,
                                                   c2g__Transaction__r.c2g__CashEntry__r.c2g__Type__c,
                                                   c2g__Transaction__r.c2g__SalesInvoice__r.Name,
                                                   c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Invoice_Legacy_Number__c
                                                   From c2g__codaTransactionLineItem__c 
                                                   WHERE (
                                                       c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Invoice_Legacy_Number__c IN :cashLineReferenceSet
                                                       OR c2g__Transaction__r.c2g__SalesInvoice__r.Name IN :cashLineReferenceSet
                                                       OR c2g__Transaction__r.c2g__SalesInvoice__r.Name IN :journalLineReferenceSet
                                                       OR c2g__Transaction__r.c2g__SalesInvoice__r.Name IN :creditNoteReferenceSet
                                                   )
                                                   AND c2g__MatchingStatus__c IN ('Available')
                                                   AND c2g__AccountValue__c != 0
                                                   AND c2g__LineType__c IN ('Account')
                                                   AND c2g__Transaction__r.c2g__TransactionType__c 
                                                   IN ('Invoice')])
        {
            //Set Map for Sales Invoice Name to Transaction Line Account
            matchToLineReferenceMap.put(tli.c2g__Transaction__r.c2g__SalesInvoice__r.Name, tli);
            //Set Map for Sales Invoice Legacy Number to Transaction Line Account
            if(tli.c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Invoice_Legacy_Number__c != null)
            {
                matchToLineReferenceMap.put(tli.c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Invoice_Legacy_Number__c, tli);
            }
        }
    }

    /* --------------------------------------------------------------------------------------------------
       Author:         franz.g.a.dimaapi@accenture.com
       Company:        Accenture
       Description:    This method setup matchToTransLineByAccMap
                                         
       Inputs:         
       Returns:        
       <Date>          <Authors Name>             <Brief Description of Change> 
       28-Apr-2017     franz.g.a.dimaapi          Initial version of the code
       06-Dec-2017     Csaba Feher                Including c2g__Transaction__r.c2g__SalesInvoice__r.Name in the query
       07-Feb-2018     filip.bezak@accenture.com  Populating the sInvSet
    ------------------------------------------------------------------------------------------------------*/
    private static void setMatchToTransLineByAccMap()
    {
        for(c2g__codaTransactionLineItem__c tli : [SELECT Id, Name, c2g__Account__c,
                                                   c2g__Account__r.JVCO_Customer_Account__c,
                                                   c2g__AccountOutstandingValue__c,
                                                   c2g__LineDescription__c,
                                                   c2g__Transaction__r.c2g__Period__c,
                                                   c2g__Transaction__r.c2g__Period__r.c2g__Closed__c,
                                                   c2g__Transaction__r.c2g__TransactionType__c,
                                                   c2g__Transaction__r.c2g__SalesInvoice__r.Name,
                                                   c2g__Transaction__r.c2g__Journal__r.c2g__Type__c,
                                                   c2g__Transaction__r.c2g__Journal__r.c2g__JournalDescription__c,
                                                   c2g__Transaction__r.c2g__CashEntry__r.c2g__BankAccount__r.c2g__GeneralLedgerAccount__r.Name
                                                   From c2g__codaTransactionLineItem__c 
                                                   WHERE c2g__Account__c IN :accIdSet
                                                   AND c2g__MatchingStatus__c IN ('Available')
                                                   AND c2g__AccountValue__c != 0
                                                   AND c2g__LineType__c IN ('Account')
                                                   AND c2g__Transaction__r.c2g__TransactionType__c
                                                   IN ('Cash','Invoice','Credit Note','Journal')])
        {
            //If the Id doesn't exist add the Id plus a new List
            if(!matchToTransLineByAccMap.containsKey(tli.c2g__Account__c))
            {
                matchToTransLineByAccMap.put(tli.c2g__Account__c, new List<c2g__codaTransactionLineItem__c>{});
            }
            matchToTransLineByAccMap.get(tli.c2g__Account__c).add(tli);
        }
    }
    
    /* --------------------------------------------------------------------------------------------------
        Author:         franz.g.a.dimaapi@accenture.com
        Company:        Accenture
        Description:    This method setup the Wrapper Class For Cash

        Inputs:         JVCO_BackgroundMatchingWrapper wrapper
                       c2g__codaTransactionLineItem__c matchFromTransaction
                       String cashType
        Returns:        
        <Date>          <Authors Name>              <Brief Description of Change> 
        11-Aug-2017     franz.g.a.dimaapi           Initial version of the code
        06-Dec-2017     Csaba Feher                 Adding setBackgroundMatchingWrapperForMultipleInvoice call for GREEN-24996
    ------------------------------------------------------------------------------------------------------*/
    @TestVisible
    private static void setBackgroundMatchingWrapperForCash(JVCO_BackgroundMatchingWrapper bgmWrapper, c2g__codaTransactionLineItem__c tli, String cashType)
    {
        //Receipt
        if(cashType == 'Receipt')
        {
            //Transaction Line Item to Match To
            c2g__codaTransactionLineItem__c matchToTransLine = null;
            bgmWrapper.matchFromType = cashType;
            bgmWrapper.writeOffLimit = WRITE_OFF_LIMIT;
            
            //Check if the Invoice Reference is existing
            if(tli.c2g__LineReference__c != null)
            {
                matchToTransLine = matchToLineReferenceMap.get(tli.c2g__LineReference__c);
            }
            //Match By Reference if Reference Found
            if(matchToTransLine != null)
            {
                bgmWrapper.isMatchByAccRef = true;
                bgmWrapper.matchToTransLine = matchToTransLine;
            //Match By Account if Reference can't found
            }else
            {
                bgmWrapper.isFromAccount = true;
                bgmWrapper.matchToTransLineByAccList = matchToTransLineByAccMap.get(tli.c2g__Account__c);
            }
        //Refund
        }else
        {
            bgmWrapper.matchFroMType = cashType;
            bgmWrapper.isFromAccount = true;
            bgmWrapper.matchToTransLineByAccList = matchToTransLineByAccMap.get(tli.c2g__Account__c);
        }
    }

    /* --------------------------------------------------------------------------------------------------
        Author:         franz.g.a.dimaapi@accenture.com
        Company:        Accenture
        Description:    This method setup the Wrapper Class for Journal

        Inputs:         JVCO_BackgroundMatchingWrapper wrapper
                       c2g__codaTransactionLineItem__c matchFromTransaction        
        Returns:        
        <Date>         <Authors Name>             <Brief Description of Change> 
        11-Aug-2017    franz.g.a.dimaapi          Initial version of the code
    ------------------------------------------------------------------------------------------------------*/
    @TestVisible
    private static void setBackgroundMatchingWrapperForJournal(JVCO_BackgroundMatchingWrapper bgmWrapper, c2g__codaTransactionLineItem__c tli)
    {   
        //Transaction Line Item to Match To
        c2g__codaTransactionLineItem__c matchToTransLine = null;

        //Check if the Invoice Reference is existing
        if(tli.c2g__LineReference__c != null){
            matchToTransLine = matchToLineReferenceMap.get(tli.c2g__LineReference__c);
        }

        //Match By Reference if Reference Found
        if(matchToTransLine != null)
        {
            bgmWrapper.matchToTransLine = matchToTransLine;
        //Match By Account if Reference can't found
        }else
        {
            bgmWrapper.isFromAccount = true;
            bgmWrapper.matchToTransLineByAccList = matchToTransLineByAccMap.get(tli.c2g__Account__c);
        }
        bgmWrapper.matchFromType = 'Journal';
    }

    /* --------------------------------------------------------------------------------------------------
        Author:         franz.g.a.dimaapi@accenture.com
        Company:        Accenture
        Description:    This method setup the Wrapper Class For Invoice

        Inputs:         JVCO_BackgroundMatchingWrapper wrapper
                       c2g__codaTransactionLineItem__c matchFromTransaction        
        Returns:        
        <Date>          <Authors Name>             <Brief Description of Change> 
        11-Aug-2017     franz.g.a.dimaapi          Initial version of the code
    ------------------------------------------------------------------------------------------------------*/
    @TestVisible
    private static void setBackgroundMatchingWrapperForInvoice(JVCO_BackgroundMatchingWrapper bgmWrapper, c2g__codaTransactionLineItem__c tli)
    {
        bgmWrapper.isFromAccount = true;
        bgmWrapper.matchToTransLineByAccList = matchToTransLineByAccMap.get(tli.c2g__Account__c);
        bgmWrapper.matchFromType = 'Invoice';
    }
}