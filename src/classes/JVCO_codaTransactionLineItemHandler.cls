/* --------------------------------------------------------------------------------------------------
    Name:            JVCO_codaTransactionLineItemHandler.cls 
    Description:     Trigger Handler Class for trigger JVCO_codaTransactionLineItem.trigger

    Date            Version     Author                             Summary of Changes 
    -----------     -------     -----------------                  -----------------------------------
    12-Dec-2016     0.1         franz.g.a.dimaapi@accenture.com    Intial draft
    02-Feb-2017     1.0         franz.g.a.dimaapi@accenture.com    Added the Installment Line Item Functionality
    19-May-2017     2.0         franz.g.a.dimaapi@accenture.com    Moved the Logic to a Logic class
    03-Jun-2017     3.0         recuerdo.b.bregente@accenture.com  Added invocation for update Account Balance logic on after insert and before insert
    26-Jul-2017     3.1         franz.g.a.dimaapi@accenture.com    Updated the afterInsert calling the Batch class instead of the direct Class
    03-Jul-2017     3.2         franz.g.a.dimaapi@accenture.com    Change the Logic and Add Batch Checker
    27-Sep-2017     3.3         franz.g.a.dimaapi@accenture.com    Implement Queuable Logic for line <= 50 TLI
    26-Oct-2017     4.0         r.p.valencia.iii@accenture.com     Added beforeInsert, beforeUpdate, and setOnHoldFlag methods
    26-Oct-2017     4.1         mary.ann.a.ruelan@accenture.com    updated afterUpdate to update sales Invoices related to transaction lines
    04-Jul-2018     4.2         franz.g.a.dimaapi                  GREEN-32622 - Upgrade to Queueable Match, Refactor Class
    09-Jul-2019     4.3         franz.g.a.dimaapi                  GREEN-34730 - Refactor, Removed Update Account Balance and Transaction On Hold
    07-Dec-2020     4.4         patrick.t.bautista                 GREEN-36089 - Disable account trigger to improve performance
------------------------------------------------------------------------------------------------ */
public class JVCO_codaTransactionLineItemHandler
{
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_codaTransactionLineItemTrigger__c : false;

    public static void beforeInsert(List<c2g__codaTransactionLineItem__c> transactLineList){
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
    }

    public static void afterInsert(Map<Id, c2g__codaTransactionLineItem__c> newMap)
    {
        if(!skipTrigger) 
        {
            System.debug(logginglevel.ERROR,'### JVCO_codaTransactionLineItemHandler afterInsert START CPU Time: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime());//jason.e.mactal added to monitor CPU Time
            JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
            JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
            if(!JVCO_BackgroundMatchingLogic.stopMatching)
            {
                Map<Id, c2g__codaTransactionLineItem__c> filteredTLIMap = getFilteredTransactionLine(newMap);
                //Filter Transaction Line for Matching
                if(!filteredTLIMap.isEmpty())
                {
                    System.enqueueJob(new JVCO_BackgroundMatching_Queueable(filteredTLIMap));
                }
            }
            System.debug(logginglevel.ERROR,'### JVCO_codaTransactionLineItemHandler afterInsert END CPU Time: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime());//jason.e.mactal added to monitor CPU Time
        }
    }

    public static void beforeUpdate(List<c2g__codaTransactionLineItem__c> transactLineList, Map<Id, c2g__codaTransactionLineItem__c> transactOldMap){
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
    }

    public static void afterUpdate(Map<Id, c2g__codaTransactionLineItem__c> newMap, Map<Id, c2g__codaTransactionLineItem__c> oldMap)
    {
        if(!skipTrigger) 
        {
            JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
            JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
            JVCO_FFUtil.stopCodaInvoiceHandlerAfterUpdate = true;
            JVCO_FFUtil.updateSalesInvoiceRecordType(newMap.values(), oldMap);
        }
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Filtered all Transaction Lines to Type = Account and Status = Available
        Inputs: Map<Id, c2g__codaTransactionLineItem__c>
        Returns: Map<Id, c2g__codaTransactionLineItem__c>
        <Date>       <Authors Name>      <Brief Description of Change> 
        27-Sep-2017 franz.g.a.dimaapi    Initial version of function
        13-Jul-2019 franz.g.a.dimaapi    GREEN-34494 - Changed Filter
    ----------------------------------------------------------------------------------------------- */
    private static Map<Id, c2g__codaTransactionLineItem__c> getFilteredTransactionLine(Map<Id, c2g__codaTransactionLineItem__c> newMap)
    {
        return new Map<Id, c2g__codaTransactionLineItem__c>([SELECT Id,
                                                             c2g__Account__c,
                                                             c2g__Account__r.JVCO_Customer_Account__c
                                                             FROM c2g__codaTransactionLineItem__c
                                                             WHERE c2g__MatchingStatus__c = 'Available'
                                                             AND c2g__LineType__c = 'Account'
                                                             AND c2g__HomeValue__c != 0
                                                             AND c2g__Account__r.Name != :Label.JVCO_Account_Unidentified
                                                             AND Id IN :newMap.keySet()]);
    }
}