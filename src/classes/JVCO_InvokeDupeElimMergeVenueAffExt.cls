public with sharing class JVCO_InvokeDupeElimMergeVenueAffExt {

    public class DupeElimRedirectException extends Exception {}

    private List<JVCO_Affiliation__c> affiliationList { get; set; }

    public JVCO_InvokeDupeElimMergeVenueAffExt(ApexPages.StandardSetController stdSetController){
        affiliationList = (List<JVCO_Affiliation__c>)stdSetController.getSelected();
    }

    public PageReference redirectVenueToDupeElimPage() {

        Profile profile = [Select Name from Profile Where Id = :UserInfo.getProfileId()];
        if(!'Licensing User'.equals(profile.Name) && !'System Administrator'.equals(profile.Name)){
            return new PageReference('/apex/InsufficientPrivileges');
        }

        List<String> venueIdList = new List<String>();
        if (!affiliationList.isEmpty()) {
            for (JVCO_Affiliation__c a : queryVenues(affiliationList))
            {
                venueIdList.add(a.Id);
            }
        }
        return new PageReference(getDestinationUrl(venueIdList, true));
    }

    public PageReference redirectVenueaffiliationToDupeElimPage() {

        Profile profile = [Select Name from Profile Where Id = :UserInfo.getProfileId()];
        if(!'Licensing User'.equals(profile.Name) && !'System Administrator'.equals(profile.Name)
        	&& !'Live Music User'.equals(profile.Name)){
            return new PageReference('/apex/InsufficientPrivileges');
        }
        
        List<String> venueIdList = new List<String>();
        if (!affiliationList.isEmpty()) {
            for (JVCO_Affiliation__c a : queryVenues(affiliationList))
            {
                venueIdList.add(a.JVCO_Venue__r.Id);
            }
        }
        Set<String> venueSet = new Set<String>();
        venueSet.addAll(venueIdList);
        venueIdList = new List<String>();
        venueIdList.addAll(venueSet);
        return new PageReference(getDestinationUrl(venueIdList, false));
    }

    private List<JVCO_Affiliation__c> queryVenues(List<JVCO_Affiliation__c> affiliationList) {
        return [SELECT Id,JVCO_Venue__c,JVCO_Venue__r.name,JVCO_Venue__r.Id,JVCO_Venue__r.JVCO_Transition_Venue_Id__c, Name FROM JVCO_Affiliation__c WHERE Id  IN:affiliationList LIMIT 3];
    }

    private String getDestinationUrl(List<String> venueNamesList, Boolean isHideFilter) {

        String names = '';
        String destination = '';
        if (isHideFilter) {
            destination = '/apex/JVCO_AccountMerge?find=true&object={0}&field1={1}&limit={2}&hideFilter=1';
        } else {
            destination = '/apex/JVCO_SearchMerge?find=true&object={0}&field1={1}&limit={2}&field2={5}&op2={6}&field3={7}&op3={8}';
        }
        if (!venueNamesList.isEmpty()) {
            names = String.join(venueNamesList, ', ');
            destination = destination + '&op1={3}&value1={4}';
        }

        String[] arguments;
        if (!venueNamesList.isEmpty() && venueNamesList.size() > 1)
        {
          arguments = new String[]{ 'JVCO_Venue__c', 'JVCO_Transition_Venue_Id__c', '3', 'contains', names};
        }
        else
        {
          arguments = new String[]{ 'JVCO_Venue__c', 'JVCO_Transition_Venue_Id__c', '3', 'equals', names};
        }
        return String.format(destination, arguments);
    }

}