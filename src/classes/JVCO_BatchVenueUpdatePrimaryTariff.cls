/*
 * Author: Filip Bezak
 * Starts a batch that populates missing Primary Tariffs on Venues where trigger did not fire during the data load
 */
global class JVCO_BatchVenueUpdatePrimaryTariff implements Database.Batchable<sObject>
{
    String query = 'SELECT Id,JVCO_Primary_Tariff__c , JVCO_Venue_Type__c FROM JVCO_Venue__c WHERE JVCO_Primary_Tariff__c = null AND JVCO_Venue_Type__c != null';
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<JVCO_Venue__c> venuesList)
    {
        List<JVCO_Venue__c> newVenuesList = new List<JVCO_Venue__c>();
        
        //retrieve the Primary Tariff mapping with venue type
        Map<String, JVCO_Primary_Tariff_Mapping__c> settings = JVCO_Primary_Tariff_Mapping__c.getAll();
        for (JVCO_Venue__c v : venuesList)
        {
            if (settings.containsKey(v.JVCO_Venue_Type__c))
            {
                v.JVCO_Primary_Tariff__c= settings.get(v.JVCO_Venue_Type__c).JVCO_Primary_Tariff__c;
                newVenuesList.add(v);         
            }
        }
        update newVenuesList;
    }
    global void finish(Database.BatchableContext BC)
    {       
    }
}