/***********************************************************************************************************
 * Author:  filip.bezak@accenture.com
 * Description: One-time script to update old Transaction Line Items
 * Input params: overrideQuery - without this param all Transaction Line Items are taken into calculation
 ***********************************************************************************************************/
global class JVCO_RecalculateOldTLI_Batch /*implements Database.Batchable<SObject>*/
{
    /*private String query = 'SELECT Id FROM c2g__codaTransactionLineItem__c WHERE c2g__LineType__c = \'Account\'';
    
    global JVCO_RecalculateOldTLI_Batch(String overrideQuery)
    {
		query = overrideQuery;
    }
    
    global JVCO_RecalculateOldTLI_Batch()
    {
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('Filip -> query: ' + query);
		return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
        Set<Id> transLineIdSet = new Set<Id>();
        for(SObject scopeRec : scope)
        {
            transLineIdSet.add(scopeRec.Id);
        }
        c2g__codaTransactionLineItem__c[] tliToUpdateList = new List<c2g__codaTransactionLineItem__c>();
        c2g__codaTransactionLineItem__c transLineItem;
        for(AggregateResult cashMatchHistAgrRec : [SELECT c2g__TransactionLineItem__c transLineItem,
                                                   SUM(c2g__HomeValue__c) totalHomeValue
                                                   FROM c2g__codaCashMatchingHistory__c
                                                   WHERE c2g__TransactionLineItem__c in : transLineIdSet
                                                   GROUP BY c2g__TransactionLineItem__c])
        {
            transLineItem = new c2g__codaTransactionLineItem__c();
            transLineItem.Id = (Id)cashMatchHistAgrRec.get('transLineItem');
            transLineItem.JVCO_Cash_Matching_History_Total__c = (Decimal)cashMatchHistAgrRec.get('totalHomeValue');
            tliToUpdateList.add(transLineItem);
        }
        if(!tliToUpdateList.isEmpty())
        {
            Database.SaveResult[] srList = Database.update(tliToUpdateList, false);
            for(Database.SaveResult sr : srList)
            {
                for(Database.Error errRec : sr.getErrors())
                {
                    JVCO_CashTransferLogic.createErrorLogRecord(sr.getId(),
                                                                errRec.getStatusCode(),
                                                                errRec.getMessage(),
                                                                'TC2P Logic');
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
    }*/
}