/*---------------------------------------------------------------------------------------------
Name: JVCO_DunningResetAccountStatusBatch.cls 
Description: Batch class that updates LA that has Last Reminder and Severity Level Populated
Date         Version      Author                 Summary of Changes 
-----------  -------     -----------------     ------------------------------------------------
04-Jul-2018   0.1         patrick.t.bautista     Initial creation
07-Nov-2018   0.2         patrick.t.bautista     Redesign Batch with additional criteria GREEN-31746
07-Feb-2019   0.3         patrick.t.bautista     GREEN-34299 - Restrict blank Incoming Reason Code
06-Mar-2019   0.4         patrick.t.bautista     GREEN-32652 - Set Balance needs to review to false
10-Oct-2019   0.5         patrick.t.bautista     GREEN-35006 - Added picklist value Pre DCA 1st Stage
30-Sep-2020   0.6         luke.walker            GREEN-35945 - Remove redundant functionality and updated query
19-Jan-2021   0.7         luke.walker            GREEN-36218 - Added new Credit Status which need to be included
----------------------------------------------------------------------------------------------- */
public class JVCO_DunningResetAccountStatusBatch  implements Database.Batchable<sObject>, Database.Stateful{
    
    //Variable for number of Successful records
    private Integer numberOfAccountsSuccessfullyUpdated = 0;
    //Variable for number of Errored records
    private Integer numberOfAccountsErrors = 0;
    //Retrieve Custom Settings Name "Reset Account Status Settings"
    final JVCO_ResetAccountStatusSettings__c DUNNING_SETTINGS = JVCO_ResetAccountStatusSettings__c.getOrgDefaults();
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        //Get Limit from the Custom Settings
        Integer intLimit = Integer.valueof(DUNNING_SETTINGS.JVCO_Limit__c) == null ? 0 : Integer.valueof(DUNNING_SETTINGS.JVCO_Limit__c);
       
        //Query returns with a limit
        if(intLimit != 0)
        {
            
            //Get Licence Account without Sales Invoice not Paid and Overdue
            return Database.getQueryLocator([SELECT id, ffps_custRem__Last_Reminder_Severity_Level__c,
                                             RecordType.Name, ffps_custRem__Reminders_Blocked_Until_Date__c,
                                             JVCO_Credit_Status__c, ffps_accbal__Account_Balance__c,
                                             JVCO_Incoming_Status_from_DCA_2nd_Stage__c, JVCO_Status_Reason_Code__c,
                                             JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c, JVCO_DCA_Status__c,
                                             ffps_custRem__Exclude_From_Reminder_Process__c, LastModifiedDate,
                                             JVCO_Dunning_Reset_Checker__c, JVCO_Passed_to_DCA_date__c,
                                             JVCO_Passed_to_DCA_Date_2nd_Stage__c, JVCO_Exclude_Reason__c,
                                             ffps_custRem__Balance_Needs_Review__c
                                             FROM Account 
                                             WHERE ((JVCO_Credit_Status__c IN  ('Excluded from Dunning', 'Not in Debt', 'Debt - 1st stage', 'Debt - 2nd stage', 
                                                                                'Debt - 3rd stage', 'Debt - Final demand', 'Pre-Enforcement', 'Internal Review', 
                                                                                'Low Value Debt', 'LV Debt Investigation', 'Pre DCA 1st Stage', 'Pre DCA 2nd Stage', 
                                                                                'DCA Review', 'CA Debt - Statement Level 1', 'CA Debt - Statement Level 2', 
                                                                                'CA Debt - Statement Level 3', 'CA Debt - Statement Level 4', 'CA Debt - Debt Review', 
                                                                                'Business overdue 2', 'Business overdue 3', 'Business overdue Final Demand', 'DCA 2 Review',
                                                                                'Corporate Review')
                                                                                AND (ffps_accbal__Account_Balance__c <= 30 OR ffps_custRem__Balance_Needs_Review__c = TRUE))
                                                                                OR 
                                                                                (JVCO_Credit_Status__c IN ('Passed to DCA - 1st Stage', 'Passed to DCA – 2nd Stage','Internal DCA') AND ffps_accbal__Account_Balance__c <= 0))
                                            AND ffps_custRem__Last_Reminder_Severity_Level__c > 0
                                            AND RecordType.Name = 'Licence Account'
                                            AND JVCO_In_Enforcement__c = FALSE
                                            AND JVCO_In_Infringement__c = FALSE                                             
                                            ORDER BY LastModifiedDate DESC
                                            LIMIT : intLimit]);
        }
        //Query returns without a limit
        else
        {
            return Database.getQueryLocator([SELECT id, ffps_custRem__Last_Reminder_Severity_Level__c,
                                             RecordType.Name, ffps_custRem__Reminders_Blocked_Until_Date__c,
                                             JVCO_Credit_Status__c, ffps_accbal__Account_Balance__c,
                                             JVCO_Incoming_Status_from_DCA_2nd_Stage__c, JVCO_Status_Reason_Code__c,
                                             JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c, JVCO_DCA_Status__c,
                                             ffps_custRem__Exclude_From_Reminder_Process__c, LastModifiedDate,
                                             JVCO_Dunning_Reset_Checker__c, JVCO_Passed_to_DCA_date__c,
                                             JVCO_Passed_to_DCA_Date_2nd_Stage__c, JVCO_Exclude_Reason__c,
                                             ffps_custRem__Balance_Needs_Review__c
                                             FROM Account 
                                             WHERE ((JVCO_Credit_Status__c IN  ('Excluded from Dunning', 'Not in Debt', 'Debt - 1st stage', 'Debt - 2nd stage', 
                                                                                'Debt - 3rd stage', 'Debt - Final demand', 'Pre-Enforcement', 'Internal Review', 
                                                                                'Low Value Debt', 'LV Debt Investigation', 'Pre DCA 1st Stage', 'Pre DCA 2nd Stage', 
                                                                                'DCA Review', 'CA Debt - Statement Level 1', 'CA Debt - Statement Level 2', 
                                                                                'CA Debt - Statement Level 3', 'CA Debt - Statement Level 4', 'CA Debt - Debt Review', 
                                                                                'Business overdue 2', 'Business overdue 3', 'Business overdue Final Demand', 'DCA 2 Review',
                                                                                'Corporate Review')
                                                                                AND (ffps_accbal__Account_Balance__c <= 30 OR ffps_custRem__Balance_Needs_Review__c = TRUE))
                                                                                OR 
                                                                                (JVCO_Credit_Status__c IN ('Passed to DCA - 1st Stage', 'Passed to DCA – 2nd Stage','Internal DCA') AND ffps_accbal__Account_Balance__c <= 0))
                                            AND ffps_custRem__Last_Reminder_Severity_Level__c > 0
                                            AND RecordType.Name = 'Licence Account'
                                            AND JVCO_In_Enforcement__c = FALSE
                                            AND JVCO_In_Infringement__c = FALSE                                             
                                            ORDER BY LastModifiedDate DESC
                                            ]);
                
        }
        
    }
    public void execute(Database.BatchableContext BC, List<Account> scope)
    {
        //List of Custom Logs to be Insert
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        //List of Accounts to be updated
        Set<Account> accToUpdateSet = new Set<Account>();
    
        for(Account acctToUpdate: scope)
        {
            if(acctToUpdate.ffps_custRem__Balance_Needs_Review__c && acctToUpdate.ffps_accbal__Account_Balance__c > 30){
                //If Balance needs review is true and account balance over £30 then reset balance needs review but don't reset everything
                acctToUpdate.ffps_custRem__Balance_Needs_Review__c = FALSE;
            } else {
                //To clear DCA fields and trigger it through process builder and to fix historical data in which Credit Status = Not in Debt
                acctToUpdate.JVCO_Dunning_Reset_Checker__c = TRUE;
                acctToUpdate.JVCO_Credit_Status__c = 'Not in Debt';
            }

            //GREEN-34322 - Reset exclude reason if it's a DCA exclude reason
            if(acctToUpdate.JVCO_Exclude_Reason__c == 'Passed to DCA – 2nd Stage' || acctToUpdate.JVCO_Exclude_Reason__c == 'Sent to DCA'){
                  acctToUpdate.ffps_custRem__Exclude_From_Reminder_Process__c = false;
                  acctToUpdate.JVCO_Exclude_Reason__c = null;
            } 
            accToUpdateSet.add(acctToUpdate);
         }
        
        if(!accToUpdateSet.isEmpty())
        {
            try
            {
                update new List<Account>(accToUpdateSet);
                numberOfAccountsSuccessfullyUpdated += accToUpdateSet.size();
            }catch(Exception e)
            {
                JVCO_ErrorUtil errorUtil = new JVCO_ErrorUtil();
                errorUtil.logErrorFromDunningBatch(accToUpdateSet, 'Reset Account Dunning Batch', 'ERROR-001', e.getMessage());
                numberOfAccountsErrors += accToUpdateSet.size();
            }
        }
    }
    public void finish(Database.BatchableContext BC)
    {
        //Get Email from Custom Settings
        string dunningEmail = DUNNING_SETTINGS.JVCO_EmailAddress__c;
        String emailMsg = 'The Reset Account Status batch has completed. Any errors can be viewed in the Custom Logs tab.';
        //Email Content to Send
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {dunningEmail});
        mail.setReplyTo('no-reply@salesforce.com');
        mail.setSenderDisplayName('Reset Account Status Batch');
        mail.setSubject('Reset Account Status Batch');
        mail.setHTMLBody('<i>' + emailMsg + '<br/>' + '<br/>' + 'Successful Account Updates: '+ numberOfAccountsSuccessfullyUpdated 
                         +'<br/>'+ 'Errored Account Updates: ' + numberOfAccountsErrors + '<i/>');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    }
}