/* ------------------------------------------------------------------------------------------------
   Name:            JVCO_GenerateRenewalQuotes_Queueable
   Description:     Queueable implementation for JVCO_GenerateRenewalQuotesBatch

   Date            Version     Author            		    Summary of Changes 
   -----------     -------     -----------------            ---------------------------------------
   09-Jul-2018     0.1         jules.osberg.a.pablo 		Initial Version
------------------------------------------------------------------------------------------------ */
public class JVCO_GenerateRenewalQuotes_Queueable implements Queueable {
	private Map<Id, Contract> contractMap;
	private Set<Id> contractIds;
	private Account accountRecord;
	private String queueableErrorMessage;
	private Integer quoteCount;
	private Integer errorQuoteCount;
	private Integer qblTempCtr = 0;
    
	public JVCO_GenerateRenewalQuotes_Queueable(Account accountRecord) {
		contractMap = new Map<Id, Contract>((List<Contract>)Database.query(JVCO_GenerateRenewalQuotesHelper.getQueryString(accountRecord)));
        contractIds = new Set<Id>();
		this.accountRecord = accountRecord;
		queueableErrorMessage = '';
		quoteCount = 0;
		errorQuoteCount = 0;
	}

	public JVCO_GenerateRenewalQuotes_Queueable(Map<Id, Contract> contractMap, Set<Id> contractIds, Account accountRecord, string queueableErrorMessage, Integer quoteCount, Integer errorQuoteCount, Integer qblTempCtr) {
		this.contractMap = contractMap;
		this.contractIds = contractIds;
		this.accountRecord = accountRecord;
		this.queueableErrorMessage = queueableErrorMessage;
		this.quoteCount = quoteCount;
		this.errorQuoteCount = errorQuoteCount;
		this.qblTempCtr = qblTempCtr;
	}



	public void execute(QueueableContext context) {

		System.debug('Passes in execute');

		//JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
		//JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;

		List<JVCO_KATempHolder__c> kaTempList = [SELECT JVCO_AccountId__c, JVCO_ContId__c, JVCO_OppId__c, JVCO_GeneratedRenewal__c, JVCO_ProcessCompleted__c FROM JVCO_KATempHolder__c WHERE JVCO_AccountId__c =: accountRecord.Id AND JVCO_GeneratedRenewal__c = true ORDER BY ID ASC];

		if(!kaTempList.isEmpty()){

            for(JVCO_KATempHolder__c kaTempRec : kaTempList){

                contractIds.add(kaTempRec.JVCO_ContId__c);

                if(!contractMap.isEmpty() && contractMap.containsKey(kaTempRec.JVCO_ContId__c)){

                    contractMap.remove(kaTempRec.JVCO_ContId__c);
                }
            }
		}
		
		//Code for checking if Account is to abort Queueable job
		List<Account> accForAbort = [SELECT Id, JVCO_AbortRunningQueueable__c FROM Account WHERE Id =: accountRecord.Id LIMIT 1];
        Boolean abortJob = false;

        if(!accForAbort.isEmpty()){
            abortJob = accForAbort[0].JVCO_AbortRunningQueueable__c;
		}

		Boolean contLoop = true;
        List<JVCO_KATempHolder__c> kaTempHolderList = new List<JVCO_KATempHolder__c>();
		
		do{

            if(!contractMap.isEmpty()){

                if(contractMap.values()[0].SBQQ__RenewalQuoted__c){
        
                    JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
                    kaTempRec.Name = contractMap.values()[0].Id;
					kaTempRec.JVCO_AccountId__c = contractMap.values()[0].AccountId;
					kaTempRec.JVCO_ContId__c = contractMap.values()[0].Id;
                    kaTempRec.JVCO_OppId__c = contractMap.values()[0].SBQQ__Opportunity__c;
                    kaTempRec.JVCO_GeneratedRenewal__c = true;
                    kaTempHolderList.add(kaTempRec);

                    contractIds.add(contractMap.values()[0].Id);
                    contractMap.remove(contractMap.values()[0].Id);
                }
                else{
                    contLoop = false;
                }
            }
            else{
                contLoop = false;
            }
        }
        while(contLoop);

        if(!kaTempHolderList.isEmpty()){

            upsert kaTempHolderList Name;
        }
        
        if(!contractMap.isEmpty())
        {
        	Contract contractRecord = contractMap.values()[0];
	        System.debug('@@contractRecord: ' + contractRecord);
	        List<Contract> tempContractList = new List<Contract>();
	        tempContractList.add(contractMap.values()[0]);

			System.debug('Temp Cont List Size: ' + tempContractList.size());

	        String errorMessage = JVCO_GenerateRenewalQuotesHelper.executeRenewalQuotes(tempContractList, accountRecord);
	        if(errorMessage == ''){
	        	quoteCount++;
        	}else{
        		errorQuoteCount++;
        		queueableErrorMessage += '<li>' + errorMessage + '</li>';
        	}

            contractIds.add(contractRecord.Id);
	        contractMap.remove(contractRecord.Id);
	        
		}
		
		if(abortJob){

            JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
			kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':GenerateRenewals';
            kaTempRec.JVCO_ErrorMessage__c = 'Queueable Job Aborted: ' + System.now();

            upsert kaTempRec Name;

            accForAbort[0].JVCO_AbortRunningQueueable__c = false;
            accForAbort[0].JVCO_GenerateRenewQuotesQueueable__c = '';

            update accForAbort[0];
		}
		else{

			//On 4th Queueable, instead of calling again, this will schedule the class instead
            if(qblTempCtr < 3){
				qblTempCtr++;

				//Recursion until the map is empty
				if(!Test.isRunningTest()) {

					if(!contractMap.isEmpty()){

						System.enqueueJob(new JVCO_GenerateRenewalQuotes_Queueable(contractMap, contractIds, accountRecord, queueableErrorMessage, quoteCount, errorQuoteCount, qblTempCtr));
					} 
					else{
						System.enqueueJob(new JVCO_GenerateBlankQuotes_Queueable(contractIds, accountRecord, queueableErrorMessage, quoteCount, errorQuoteCount));
						//JVCO_GenerateBlankQuotes_Queueable(Set<Id> contractIds, Account accountRecord, String queueableErrorMessage, Integer quoteCount, Integer errorQuoteCount){

						//Delete all KA Temp Records
						List<JVCO_KATempHolder__c> kaListForDelete = [SELECT Id FROM JVCO_KATempHolder__c WHERE JVCO_AccountId__c =: accountRecord.Id];

						if(!kaTempList.isEmpty()){
			
							delete kaListForDelete;
						}

						//Remove Queueable Job value
						Account acc = new Account();
						acc.Id = accountRecord.Id;
						acc.JVCO_GenerateRenewQuotesQueueable__c = null;
						acc.JVCO_GenerateRenewQuotesLastSubmitted__c = DateTime.now();
						update acc;
					}
				}
			}
			else{
				
				JVCO_KeyAccountHelperSchedulable kaHelperSched = new JVCO_KeyAccountHelperSchedulable(contractMap, contractIds, accountRecord, queueableErrorMessage, quoteCount, errorQuoteCount, 'GenerateRenewals');

				Integer schedulerBuffer = (Integer)JVCO_KABatchSetting__c.getOrgDefaults().JVCO_SchedulerBuffer__c != null ? (Integer)JVCO_KABatchSetting__c.getOrgDefaults().JVCO_SchedulerBuffer__c : 45;

                Datetime dt = DateTime.now().addSeconds(schedulerBuffer);
                String cronExp = '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();

                while(dt <= DateTime.now()){

                    dt = dt.addSeconds(schedulerBuffer);
                }

                Id schedId = System.schedule('Key Account Helper Sched: Generate Renew ' + accountRecord.Id, cronExp, kaHelperSched);

                JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
                kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':GenerateRenewals';
                kaTempRec.JVCO_JobId__c = schedId;
                kaTempRec.JVCO_ErrorMessage__c = '';

                upsert kaTempRec Name;
			}
		}
	}
}