/* ----------------------------------------------------------------------------------------------
    Name: JVCO_NewEventExtension 
    Description: Extension Class for JVCO_NewEventExtension page

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    19-Oct-2017     0.1         Rolando Valencia      Intial creation
    17-Nov-2017     0.2         Anna Gonzales         Added function to escape apostrophe on the Venue's Name
    13-Mar-2018     0.3         Jules Pablo           Added GREEN-30722 Fix. Added default value for Festival Field
	05-Feb-2021		0.4			luke.walker			  Updated to be compatible with Lightning GREEN-36286
----------------------------------------------------------------------------------------------- */
public class JVCO_NewEventExtension 
{
    public JVCO_Event__c eventRecord;
    public string festivalName='';
    String venueURL = '';
    String venueName = '';
    String venueId = '';
    String festivalFieldId = 'CF00N5800000CjXCZ';
    String festivalID = '';
    String festivalURL = '';

    public JVCO_NewEventExtension(ApexPages.StandardController stdController) 
    {
        this.eventRecord = (JVCO_Event__c)stdController.getRecord();
        
        if(eventRecord.JVCO_Festival__c != null){
            festivalName = eventRecord.JVCO_Festival__r.Name;
            festivalID = eventRecord.JVCO_Festival__c;
            festivalURL = getReturnFestivalName();
        }
        
        if(eventRecord.JVCO_Venue__c != null)
        {
            JVCO_Venue__c venue = [SELECT Id, Name FROM JVCO_Venue__c WHERE Id =: eventrecord.JVCO_Venue__c LIMIT 1];
            venueId = eventRecord.JVCO_Venue__c; 
            venueName = venue.name;
            venueURL = getReturnVenueName(venueId, venueName);
        }
        
    }

    public PageReference CreateEvent(){
        
        String uiTheme = UserInfo.getUiThemeDisplayed();
        
        //Create URL for Lightning or Classic
		if(uiTheme == 'Theme4d' || uiTheme == 'Theme4u'){
          system.debug('Lightning Page Reference');
          PageReference lightPR = new PageReference('/lightning/o/JVCO_Event__c/new?defaultFieldValues=Name=.,JVCO_Venue__c='+venueID+',JVCO_Festival__c='+festivalID+'&nooverride=1');
          aura.redirect(lightPR);
          return lightPR;
        } else {
          system.debug('Classic Page Reference');
          PageReference classPR = new PageReference('/'+getObjectPrefixID('JVCO_Event__c')+'/e?nooverride=1'+'&Name=.'+venueURL+festivalURL);
          classPR.setRedirect(true);
          return classPR;
        }
    }
    
    public static String getObjectPrefixID(String obj){
    String keyPrefix = '';

    Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe() ;
    Schema.SObjectType s = m.get(obj) ;
    system.debug('Sobject Type: '+ s);
    Schema.DescribeSObjectResult r = s.getDescribe() ;
    keyPrefix = r.getKeyPrefix();
    system.debug('keyPrefix: '+ keyPrefix);
    return keyPrefix;
    }
 
    public string getReturnVenueName(String venID, String venName){
        string vURL='';
        string vId;
        string vName;
        
        //Create Venue ID value for classic URL hack
        vId = '&CF';
        vId += JVCO_EventFieldIds__c.getOrgDefaults().JVCO_Venue_ID__c;
        vId += '_lkid=';
        vId += venID;
            
		//Create Venue Name value for classic URL hack
        vName = '&CF';
        vName += JVCO_EventFieldIds__c.getOrgDefaults().JVCO_Venue_ID__c;
        vName += '=';
        vName += venName.replace('\'', '\\\''); //to escape and replace apostrophe
        
        vURL = vID+vName;
        return vURL;
    }
    
    public string getReturnFestivalName()
    {
        if(festivalName == null)
        {
            List<JVCO_Festival__c> festivalListCheck = [SELECT Name FROM JVCO_Festival__c WHERE Id = :eventRecord.JVCO_Festival__c LIMIT 1];
            System.debug('###festivalListCheck:' + festivalListCheck);
            System.debug('###eventRecord.JVCO_Festival__c:' + eventRecord.JVCO_Festival__c);
            if(festivalListCheck.size() > 0) {
                for(JVCO_Festival__c fest :festivalListCheck){
                    festivalName = fest.Name.contains('\'') ? fest.Name.replace('\'','%27') : fest.Name; //mariel.m.buena GREEN-33727 To escape and replace apostrophe
                }
                festivalName = appendHrefParameter(festivalFieldId,festivalName);
            }
        }

        return festivalName;
    }

    public string appendHrefParameter(string parameter, string value){
        return '&' + parameter + '=' + value;
    }
}