public class JVCO_AcquisitionsExtension
{
    public Boolean disableFinish { get; set; }
    public string searchFieldValue{get;set;}

    public JVCO_Affiliation__c overarchingValuesBinder { get; set; }
    public Account accountRecord { get; set; }
    //public Account parentAccount { get; set; }

    public String venueSelectorIdFromPage {get;set;}
    public String venueSelectedIdFromPage {get;set;}

    private JVCO_VenueTransfer venueTransfer { get; set; }
    public Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferCandidatesMap { get; set; }
    public Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsMap { get; set; }
    public Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsDisplayMap { get; set; }
    //public Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsToPassMap { get; set; } // Commented jules.osberg.a.pablo     19/10/2017      GREEN-24918
    public Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsCheckerMap { get; set; }
    //Start LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
    public Map<String, Date> mapTempIdJVCOAffiliationDate {get; set;}
    public Map<String, Boolean> mapTempIdJVCOAffiliationCheckBox {get; set;}
    public Map<String, String> mapTempIdJVCOAffiliationCustRefNum {get; set;}
    //End LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
    public List<String> candidateFieldsVenue { get; set; }
    public List<String> candidateFieldsAffiliation { get; set; }
    public List<String> selectedFieldsVenue { get; set; }
    public List<String> selectedFieldsAffiliation { get; set; }
    public List<String> additionalFieldsAffilation { get; set; }
    public List<Contract> contractsToAmendList { get; set; }

    public String overarchingAffiliationValues { get; set; }
    public String affiliationToBeRemovedFromTs { get; set; }
    public Boolean isAmended { get; set; }
    public Boolean hasAmendments { get; set; }
    public Boolean hasAffiliations { get; set; }
    public Set<String> filterIdSet { get; set; }
    public Boolean isAgency { get; set; }
    public Integer stepNumber {get; set;}
    //Transient Savepoint sp;
    public Integer loopCtr {get; set;}
    public Integer processSize { get; set; }
    public Integer venueProgress { get; set; }
    public Integer venueSize { get; set; }
    public Integer venuePrprcnt { get; set; }

    public Boolean processQueueable = (Boolean)JVCO_General_Settings__c.getOrgDefaults().JVCO_Acquisition_Queueable__c; //GREEN-34628

    public List<JVCO_VenueTransfer.FieldSetWrapper> overarchingFieldNamesList {
        get {
            return venueTransfer.convertToFieldSetWrapperList(venueTransfer.getAffiliationAcquisitionsDefaultsFields());
        }
    }

    public List<JVCO_VenueTransfer.FieldSetWrapper> selectedAffiliationsFieldNamesList {
        get {
            return venueTransfer.convertToFieldSetWrapperList(venueTransfer.getAffiliationAcquisitionsSelectedFields());
        }
    }

    public List<JVCO_VenueTransfer.FieldSetWrapper> selectedVenuesFieldNamesList {
        get {
            return venueTransfer.convertToFieldSetWrapperList(venueTransfer.getVenueAcquisitionsSelectedFields());
        }
    }

    public List<JVCO_VenueTransfer.FieldSetWrapper> selectorVenuesFieldNamesList {
        get {
            return venueTransfer.convertToFieldSetWrapperList(venueTransfer.getVenueAcquisitionsSelectorFields());
        }
    }

    public List<JVCO_VenueTransfer.FieldSetWrapper> selectorAccountsFieldNamesList {
        get {
            return venueTransfer.convertToFieldSetWrapperList(venueTransfer.getAccountAcquisitionsSelectorFields());
        }
    }
    
    public List<JVCO_VenueTransfer.FieldSetWrapper> additionalFieldsList {
        get {
            return venueTransfer.convertToFieldSetWrapperList(venueTransfer.getAffiliationAdditionalFields());
        }
    }

    public JVCO_AcquisitionsExtension(ApexPages.StandardController stdController) 
    {
        accountRecord = (Account)stdController.getRecord();
        accountRecord = [select Id, Name, JVCO_In_Enforcement__c, JVCO_Customer_Account__c, JVCO_Customer_Account__r.Type, JVCO_VenAcquisitionQueueFlag__c from Account where Id = :accountRecord.Id];
        //parentAccount = [select Id, Name, Type from Account where Id =: accountRecord.JVCO_Customer_Account__c];
        /*if(parentAccount.Type == 'Agency') {
            isAgency = true;
        } else {
            isAgency = false;
        }*/
        
        String caType = '';
        if(accountRecord.JVCO_Customer_Account__r.Type != null && accountRecord.JVCO_Customer_Account__r.Type != '') {
            caType = accountRecord.JVCO_Customer_Account__r.Type;
            caType = caType.trim();
        }
        
        if(caType.equalsIgnoreCase('agency')) {
            isAgency = true;
        } else {
            isAgency = false;
        }
        
        overarchingValuesBinder = new JVCO_Affiliation__c();
        overarchingValuesBinder.JVCO_Start_Date__c = date.today();

        venueTransfer = new JVCO_VenueTransfer();
        transferCandidatesMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
        transferSelectionsMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
        transferSelectionsDisplayMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
        //transferSelectionsToPassMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>(); // Commented jules.osberg.a.pablo     19/10/2017      GREEN-24918
        transferSelectionsCheckerMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
        //Start LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
        mapTempIdJVCOAffiliationDate = new Map<String, Date>();
        mapTempIdJVCOAffiliationCheckBox = new Map<String, Boolean>();
        mapTempIdJVCOAffiliationCustRefNum = new Map<String, String>();
        //End LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
        candidateFieldsVenue = new List<String>(venueTransfer.getCandidateVenueFieldsForAcquisition());
        candidateFieldsAffiliation = new List<String>(venueTransfer.getCandidateAffiliationFieldsForAcquisition());
        selectedFieldsVenue = new List<String>(venueTransfer.getSelectedVenueFieldsForAcquisition());
        selectedFieldsAffiliation = new List<String>(venueTransfer.getDefaultsAffiliationFieldsForAcquisition());
        additionalFieldsAffilation = new List<String>(venueTransfer.getAdditionalFieldsForDisplay());
        contractsToAmendList = new List<Contract>();

        venueSelectorIdFromPage = '';
        venueSelectedIdFromPage = '';
        overarchingAffiliationValues = '';
        disableFinish = false;
        isAmended = false;
        hasAmendments = false;
        hasAffiliations = false;
        filterIdSet = new Set<String>();
        stepNumber = 1;
        loopCtr = 0;

        // START    raus.k.b.ablaza     28/09/2017      GREEN-23464     Removed affiliation query
        /*
        for(JVCO_Affiliation__c affRec : [select Id from JVCO_Affiliation__c where JVCO_Account__c =:  accountRecord.Id]) {
            filterIdSet.add(affRec.Id);
        }
        */
        // END    raus.k.b.ablaza     28/09/2017      GREEN-23464     Removed affiliation query

        venueProgress = 0;
        venueSize = 0;
        venuePrprcnt = 0;
        if((Integer)JVCO_Constants__c.getOrgDefaults().JVCO_Acquisitions_Process_Size__c != null) {
            processSize = (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_Acquisitions_Process_Size__c;
        } else {
            processSize = 5;
        }

    } 

    public boolean getShowPanelIfInEnforcement()
    {
        return accountRecord.JVCO_In_Enforcement__c;
    }

    public void search()
    {
        try {
            // TODO: replace hardcoded accountid. get it from the query params.
            // TODO: replace new Set<Id>() with an actual set of affiliation IDs. these ids are to excluded from the search because they already exist in the transferSelections collection.
            System.debug('JUDS: searchFieldValue:' + searchFieldValue + ' accountRecord.Id:' + accountRecord.Id);
            /*if(transferSelectionsMap.size() > 0) {
                filterIdSet.addAll(transferSelectionsMap.keySet());
            }
            
            if(transferSelectionsDisplayMap.size() > 0) {
                filterIdSet.addAll(transferSelectionsDisplayMap.keySet());
            }*/
            
            transferCandidatesMap.clear();
            Map<String, JVCO_VenueTransfer.JVCO_TransferObject> tempMapHolder = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
            Set<Id> venuesSelected = new Set<Id>();
            
            tempMapHolder.clear();
            venuesSelected.clear();
            
            if(!transferSelectionsDisplayMap.isEmpty()) {
                for(JVCO_VenueTransfer.JVCO_TransferObject tsdmVal : transferSelectionsDisplayMap.values()) {
                    venuesSelected.add(tsdmVal.venueRecord.Id);
                }
            }
            
            if(!transferSelectionsMap.isEmpty()) {
                for(JVCO_VenueTransfer.JVCO_TransferObject tsdmVal : transferSelectionsMap.values()) {
                    venuesSelected.add(tsdmVal.venueRecord.Id);
                }
            }
            
            tempMapHolder.putAll(venueTransfer.searchAffiliations(JVCO_VenueTransfer.TransferType.ACQUISITION, searchFieldValue, accountRecord.Id, filterIdSet, isAgency));
            System.debug('JUDS: tempMapHolder:' + tempMapHolder);
            if(!tempMapHolder.isEmpty()) {
                for(String tempMapKey : tempMapHolder.keySet()) {
                    JVCO_VenueTransfer.JVCO_TransferObject tempMapVal = tempMapHolder.get(tempMapKey);
                    if(!venuesSelected.contains(tempMapVal.venueRecord.Id) && !venuesSelected.contains(tempMapVal.venueRecord.Id)) {
                        transferCandidatesMap.put(tempMapKey, tempMapVal);
                    }
                }
            }
            //transferCandidatesMap.putAll(venueTransfer.searchAffiliations(JVCO_VenueTransfer.TransferType.ACQUISITION, searchFieldValue, accountRecord.Id, filterIdSet, isAgency));
            System.debug('JUDS: transferCandidatesMap:' + transferCandidatesMap);
            System.debug('JUDS: transferSelectionsMap:' + transferSelectionsMap);
        } catch (Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
    }

    public PageReference receiveSelectorValues()
    {     
        System.debug('venueSelectorIdFromPage: ' + (List<String>)System.JSON.deserialize(venueSelectorIdFromPage, List<String>.class));
        System.debug('venueSelectorIdFromPage String: ' + venueSelectorIdFromPage);
        for(String affiliationId : (List<String>)System.JSON.deserialize(venueSelectorIdFromPage, List<String>.class))
        {
            System.debug('DBG: affiliationId >>> ' + affiliationId);
            transferSelectionsMap.put(affiliationId, transferCandidatesMap.get(affiliationId));
            transferCandidatesMap.remove(affiliationId);
        }

        System.debug('DBG: transferSelectionsMap after putting to transferSelectionsMap >>> ' + transferSelectionsMap);
        System.debug('DBG: transferCandidatesMap after putting to transferSelectionsMap >>> ' + transferCandidatesMap);

        if(!transferSelectionsMap.isEmpty()){
            disableFinish = true;
        }
        else{
            disableFinish = false; 
        }

        return null;
    }

    public PageReference receiveSelectedValues()
    {
        System.debug('venueSelectedIdFromPage: ' + (List<String>)System.JSON.deserialize(venueSelectedIdFromPage, List<String>.class));
        for(String affiliationId : (List<String>)System.JSON.deserialize(venueSelectedIdFromPage, List<String>.class))
        {
            System.debug('DBG: affiliationId >>> ' + affiliationId);
            System.debug('DBG: inside receiveSelectedValues - transferCandidatesMap >>> ' + transferCandidatesMap);
            System.debug('DBG: inside receiveSelectedValues - transferSelectionsMap >>> ' + transferSelectionsMap);
            transferCandidatesMap.put(affiliationId, transferSelectionsMap.get(affiliationId));
            transferSelectionsMap.remove(affiliationId);
        }

        if(!transferSelectionsMap.isEmpty()){
            disableFinish = true;
        }
        else{
            disableFinish = false; 
        }

        return null;
    }

    public PageReference addToList() {

        try {
            System.debug('JUDS TEST overarchingAffiliationValues: ' + overarchingAffiliationValues);
            //transferSelectionsMap = venueTransfer.applyOverarchingValuesToTransferSelections(transferSelectionsMap, overarchingAffiliationValues);

            // START    raus.k.b.ablaza     18/09/2017      GREEN-23144     Check if there is a missing start date
            Map<String, JVCO_VenueTransfer.JVCO_TransferObject> tmpTransferSelectionsDisplayMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
            tmpTransferSelectionsDisplayMap.putAll(venueTransfer.applyOverarchingValuesToTransferSelections(JVCO_VenueTransfer.TransferType.ACQUISITION, transferSelectionsMap, overarchingValuesBinder, ''));

            String strErrDateChecker = 'Please put a start date before adding to list.';
            Boolean errDateFlag = false;
            for(String str : tmpTransferSelectionsDisplayMap.keySet()){
                if(String.isBlank(String.valueOf(tmpTransferSelectionsDisplayMap.get(str).affiliationRecord.get('JVCO_Start_Date__c')))){
                    errDateFlag = true;
                }
            }

            if(errDateFlag){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, strErrDateChecker));
                tmpTransferSelectionsDisplayMap.clear();
            }
            else{
                transferSelectionsDisplayMap.putAll(tmpTransferSelectionsDisplayMap);
                transferSelectionsMap.clear();
                if(transferSelectionsDisplayMap.size() > 0) {
                    hasAffiliations = true;              
                }
                venueSize = transferSelectionsDisplayMap.size();
                updateVenueProgress(); 
            }
            // END    raus.k.b.ablaza     18/09/2017      GREEN-23144     Check if there is a missing start date

            System.debug('DBG: ts >>> ' + transferSelectionsMap);
        } catch (Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }

        return null;

    }

    public PageReference removeFromTransferSelections() {
        transferSelectionsDisplayMap.remove(affiliationToBeRemovedFromTs);
        if(transferSelectionsDisplayMap.size() < 1) {
            hasAffiliations = false;
        }
        return null;
    }

    public PageReference confirmAcquisition(){
        ApexPages.getMessages().clear(); 
        venueSize = transferSelectionsDisplayMap.size();
        updateVenueProgress();   
        stepNumber = 2;
        return null;
    }
    
    public PageReference confirmAcquisition2(){
        Savepoint sp = Database.setSavepoint();
        

        if(!processQueueable)
        {
            //26-Jun-2019     mel.andrei.b.santos GREEN-34628 - created queueueable class(JVCO_Acquisitions_Queueable) based on this method 
            updateVenueProgress();

            transferSelectionsCheckerMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();

            //Start LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
            if(!mapTempIdJVCOAffiliationDate.isEmpty()) {
                for(String kAffli: mapTempIdJVCOAffiliationDate.keySet()){
                    transferSelectionsDisplayMap.get(kAffli).affiliationRecord.JVCO_Start_Date__c = Date.valueOf(mapTempIdJVCOAffiliationDate.get(kAffli));
                }
            }
            
            if(!mapTempIdJVCOAffiliationCheckBox.isEmpty()) {
                for(String kAffli: mapTempIdJVCOAffiliationCheckBox.keySet()){
                    transferSelectionsDisplayMap.get(kAffli).affiliationRecord.JVCO_Omit_Quote_Line_Group__c = Boolean.valueOf(mapTempIdJVCOAffiliationCheckBox.get(kAffli));
                }
            }

            if(!mapTempIdJVCOAffiliationCustRefNum.isEmpty()){
                for(String kAffli: mapTempIdJVCOAffiliationCustRefNum.keySet()){
                    transferSelectionsDisplayMap.get(kAffli).affiliationRecord.JVCO_Customer_Reference_Number__c = String.valueOf(mapTempIdJVCOAffiliationCustRefNum.get(kAffli));
                }
            }
            //End LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values

            if(transferSelectionsDisplayMap.size() > 0) {    
                // START    jules.osberg.a.pablo     19/10/2017      GREEN-24918 Fixed issue with affiliation start dates incorrectly cascading to all affilations
                Integer ctr2 = 0;
                
                for(String tsdmKey : transferSelectionsDisplayMap.keySet()) {
                    if(ctr2 < processSize){
                        transferSelectionsCheckerMap.put(tsdmKey, transferSelectionsDisplayMap.remove(tsdmKey));
                        ctr2++;
                    }
                }

                //Start LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
                mapTempIdJVCOAffiliationDate.clear();
                if(transferSelectionsDisplayMap.size() > 0) {  
                    for(String affKey: transferSelectionsDisplayMap.keySet()){
                        mapTempIdJVCOAffiliationDate.put(affKey,Date.valueOf(transferSelectionsDisplayMap.get(affKey).affiliationRecord.JVCO_Start_Date__c));
                    }
                }

                mapTempIdJVCOAffiliationCheckBox.clear();
                if(transferSelectionsDisplayMap.size() > 0) {  
                    for(String affKey: transferSelectionsDisplayMap.keySet()){
                        mapTempIdJVCOAffiliationCheckBox.put(affKey,Boolean.valueOf(transferSelectionsDisplayMap.get(affKey).affiliationRecord.JVCO_Omit_Quote_Line_Group__c));
                    }
                }

                mapTempIdJVCOAffiliationCustRefNum.clear();
                if(transferSelectionsDisplayMap.size() > 0){
                    for(String affKey: transferSelectionsDisplayMap.keySet()){
                        mapTempIdJVCOAffiliationCustRefNum.put(affKey, String.valueOf(transferSelectionsDisplayMap.get(affKey).affiliationRecord.JVCO_Customer_Reference_Number__c));
                    }
                }
                 //End LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
                Boolean retAcquisition = venueTransfer.confirmAcquisition(accountRecord, isAgency, transferSelectionsCheckerMap, false);
                
                //if(!transferSelectionsToPassMap.isEmpty()) { // Commented jules.osberg.a.pablo     19/10/2017      GREEN-24918
                if(!transferSelectionsCheckerMap.isEmpty()) {
                    if(retAcquisition) {
                        if(!transferSelectionsDisplayMap.isEmpty()) {
                            stepNumber = 2;
                        } else {
                            stepNumber = 3;
                        }
                    } else {
                        stepNumber = 1;

                        // START    GREEN-26096     raus.k.b.ablaza     02-14-2018      return value of transferSelectionsDisplayMap
                        transferSelectionsDisplayMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
                        
                        for(String tsdmKey : transferSelectionsCheckerMap.keySet()) {
                            transferSelectionsDisplayMap.put(tsdmKey, transferSelectionsCheckerMap.remove(tsdmKey));
                        }
                        // END    GREEN-26096     raus.k.b.ablaza     02-14-2018      return value of transferSelectionsDisplayMap

                        Database.rollback(sp);
                    }   
                }
            } else {
                stepNumber = 3;
            }

            loopCtr++;

        }

        else
        {
            if(accountRecord.JVCO_VenAcquisitionQueueFlag__c == true)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Venue Acquisition for ' + accountRecord.Name + ' is currently processing, please wait for the job to finish.'));
                stepNumber = 1;
            }
            else
            {
                Id jobId = System.enqueueJob(new JVCO_Acquisitions_Queueable(accountRecord, isAgency, mapTempIdJVCOAffiliationDate, mapTempIdJVCOAffiliationCheckBox, mapTempIdJVCOAffiliationCustRefNum, transferSelectionsDisplayMap,  processSize));
                accountRecord.JVCO_VenAcquisitionQueueFlag__c = true;
                update accountRecord;
                stepNumber = 3;
            }
            
        }

        return null;

    }
    
    public PageReference confirmAcquisition3(){
        PageReference pr;
        pr = new PageReference('/'+accountRecord.Id);
        pr.setRedirect(true);
        return pr;
    }

    private void updateVenueProgress() {
        if(((loopCtr+1) * processSize) > venueSize) {
            venueProgress = venueSize;
        } else {
            venueProgress = (loopCtr+1) * processSize;
        }
    }
}