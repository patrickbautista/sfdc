/*
Created by: Desiree Quijada
Date Created: 28-July-2017
Date Modified: 28-July-2017
Details: Test class for class JVCO_Batch_InDispute
Version: v1.0
*/
@isTest
public class JVCO_Batch_InDispute2_Test {
    //@isTest
    public static testMethod void inDispute2(){
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
            listQueue.add(q2);
            queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
            listQueue.add(q3);
            queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
            listQueue.add(q4);
            queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
            listQueue.add(q5);
            queuesobject q6 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankAccount__c'); 
            listQueue.add(q6);
            queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q7);
            queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatement__c'); 
            listQueue.add(q8);      
            queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntry__c'); 
            listQueue.add(q9);  
            queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='PAYBASE2__Payment__c'); 
            listQueue.add(q10);
            queuesobject q11 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransaction__c'); 
            listQueue.add(q11);
            //queuesobject q12 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransactionLineItem__c'); 
            //listQueue.add(q12);
            insert listQueue;
            
            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }
        
        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
        testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc.Dimension_1_Required__c = false;
        testGeneralLedgerAcc.Dimension_2_Required__c = false;
        testGeneralLedgerAcc.Dimension_3_Required__c = false;
        testGeneralLedgerAcc.Dimension_4_Required__c = false;
        testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
        testGeneralLedgerAcc.ownerid = testGroup.Id;
        
        insert testGeneralLedgerAcc;
        
        
        //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
            for(RecordType rt: rtypes)
        {
            accountRecordTypes.put(rt.Name,rt.Id);
        }
        
        /*c2g__codaTaxCode__c taxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedgerAcc.id);
insert taxCode;*/
        
        
        //Create Account
        Account testAccCust = new Account(Name='Test Customer Account');
        testAccCust.AccountNumber = '999999';
        testAccCust.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust.Type='Key Account';
        testAccCust.RecordTypeId = accountRecordTypes.get('Customer Account');
        insert testAccCust;
        
        Account testAcc = JVCO_TestClassObjectBuilder.createLicenceAccount(testAccCust.id);
        testAcc.AccountNumber = '987654';
        testAcc.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAcc.c2g__CODAAllowDeleteInUse__c = false;
        testAcc.c2g__CODACreditLimitEnabled__c = false;
        testAcc.c2g__CODACreditStatus__c = 'Credit Allowed';
        testAcc.c2g__CODADaysOffset1__c = 0.0;
        testAcc.c2g__CODADiscount1__c = 0.0;
        testAcc.c2g__CODAFederallyReportable1099__c = false;
        testAcc.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testAcc.c2g__CODAIntercompanyAccount__c = false;
        testAcc.c2g__CODASalesTaxStatus__c = 'Taxable';
        testAcc.JVCO_Customer_Account__c = testAccCust.id;
        testAcc.RecordTypeId = accountRecordTypes.get('Licence Account');
        testAcc.JVCO_Pre_Contract_Confirmation__c = false;
        testAcc.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        testAcc.TCs_Accepted__c = 'Yes'; 
        //testAcc.c2g__CODAOutputVATCode__c = taxCode.id;
        insert testAcc;
        
       
        
        c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
        testCompany.c2g__CashMatchingCurrencyMode__c = 'Account';
        testCompany.c2g__Country__c = 'GB';
        testCompany.c2g__ECCountryCode__c = 'GB';
        testCompany.c2g__UnitOfWork__c = 1.0;
        testCompany.Name = 'Test Company';
        testCompany.ownerid = testGroup.Id;
        
        insert testCompany;
        
        c2g__codaAccountingCurrency__c accCurrency = new c2g__codaAccountingCurrency__c();
        accCurrency.c2g__OwnerCompany__c = testCompany.id;
        accCurrency.c2g__DecimalPlaces__c = 2;
        accCurrency.Name = 'GBP';
        accCurrency.c2g__Dual__c = true ;
        accCurrency.ownerid = testGroup.Id;
        accCurrency.c2g__Home__c = true;
        accCurrency.c2g__UnitOfWork__c = 2.0;
        insert accCurrency;
        
        c2g__codaBankAccount__c testBankAccount = new c2g__codaBankAccount__c();
        testBankAccount.c2g__AccountName__c = 'Test Bank Account';
        testBankAccount.c2g__AccountNumber__c = '987654';
        testBankAccount.c2g__BankName__c = 'Barclays Bank - Current';
        testBankAccount.c2g__GeneralLedgerAccount__c = testGeneralLedgerAcc.id;
        testBankAccount.Name = 'Test Bank Account 1';
        testBankAccount.c2g__OwnerCompany__c = testCompany.Id;
        testBankAccount.c2g__ReportingCode__c = 'BARC-CURR-JV';
        testBankAccount.c2g__BankAccountCurrency__c = accCurrency.id;
        insert testBankAccount;
        
        testCompany.c2g__BankAccount__c = testBankAccount.id;
        update testCompany;
        
        c2g__codaYear__c yr= new c2g__codaYear__c();
        yr.Name ='2016';
        yr.c2g__AutomaticPeriodList__c =  true;
        yr.c2g__OwnerCompany__c = testCompany.id;
        yr.c2g__ExternalId__c = 'yzsd1234';
        yr.c2g__NumberOfPeriods__c =11;
        yr.c2g__StartDate__c =  system.today() - 10;
        yr.c2g__Status__c = 'Open';
        yr.c2g__PeriodCalculationBasis__c = '445';
        yr.c2g__YearEndMode__c = 'Full Accounting Code' ; 
        yr.c2g__UnitOfWork__c = 12;
        yr.ownerid = testGroup.Id;
        insert yr;
        
        c2g__codaPeriod__c testPeriod = new c2g__codaPeriod__c();
        testPeriod.c2g__OwnerCompany__c = testCompany.id;
        testPeriod.c2g__StartDate__c = System.today()-10;
        testPeriod.c2g__EndDate__c = System.today()+10;
        testPeriod.c2g__PeriodNumber__c ='123';
        testPeriod.c2g__Description__c ='test Desc';
        testPeriod.c2g__PeriodGroup__c = 'Q1';
        testPeriod.c2g__PeriodNumber__c = '1';
        testPeriod.c2g__YearName__c = yr.id;
        insert testPeriod;
        
        c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c();
        userCompany.c2g__Company__c = testCompany.id;
        userCompany.c2g__User__c = userInfo.getUserId();
        userCompany.c2g__ExternalId__c = 'ABCDE1234567876';
        userCompany.c2g__UnitOfWork__c = 111 ;
        insert userCompany;
        
        //Create Sales Invoice
        c2g__codaInvoice__c testInvoice = new c2g__codaInvoice__c();
        testInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testInvoice.c2g__InvoiceDate__c = date.today().addDays(-7);
        testInvoice.c2g__DueDate__c = date.today().addDays(-7);
        testInvoice.c2g__Account__c = testAccCust.Id;
        testInvoice.JVCO_Customer_Type__c = 'New Business';
        testInvoice.c2g__OwnerCompany__c = testCompany.id;
        testInvoice.ownerid = testGroup.Id;
        testInvoice.JVCO_Invoice_Legacy_Number__c = 'SIN00001';
        testInvoice.ffps_custRem__In_Dispute__c = false;
        testInvoice.JVCO_Surcharge_Generated__c = false;
        insert testInvoice;
        
        //Create Sales Invoice
        c2g__codaInvoice__c testInvoice2 = new c2g__codaInvoice__c();
        testInvoice2.c2g__InvoiceCurrency__c = accCurrency.Id;
        testInvoice2.c2g__InvoiceDate__c = date.today().addDays(-7);
        testInvoice2.c2g__DueDate__c = date.today().addDays(-7);
        testInvoice2.c2g__Account__c = testAccCust.Id;
        testInvoice2.JVCO_Customer_Type__c = 'New Business';
        testInvoice2.c2g__OwnerCompany__c = testCompany.id;
        testInvoice2.ownerid = testGroup.Id;
        testInvoice2.JVCO_Invoice_Legacy_Number__c = 'SIN00002';
        testInvoice2.ffps_custRem__In_Dispute__c = true;
        testInvoice2.JVCO_Surcharge_Generated__c = false;
        testInvoice2.JVCO_Cancelled__c = false;
        insert testInvoice2;
        
        
        //insert invoice1
        blng__Invoice__c invoice1 = new blng__Invoice__c();
        invoice1.blng__Account__c = testAcc.id;
        invoice1.JVCO_Sales_Invoice__c = testInvoice.id;
        invoice1.JVCO_Original_Invoice__r = null;
        invoice1.JVCO_Temp_In_Dispute__c = true;
        invoice1.JVCO_Surcharge_Generated__c = true;
        insert invoice1;
        
        //insert invoice2
        blng__Invoice__c invoice2 = new blng__Invoice__c();
        invoice2.blng__Account__c = testAcc.id;
        invoice2.JVCO_Sales_Invoice__c = testInvoice2.id;
        invoice2.JVCO_Original_Invoice__c = invoice1.id;
        invoice1.JVCO_Temp_In_Dispute__c = null;
        invoice1.JVCO_Surcharge_Generated__c = null;
        insert invoice2;
        
        Test.startTest();
        Database.executeBatch(new JVCO_Batch_InDispute2());
        
        Test.stopTest();
    }
    
}