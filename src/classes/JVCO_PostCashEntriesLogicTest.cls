@isTest
public class JVCO_PostCashEntriesLogicTest 
{
    @testSetup 
    static void createTestData() 
    {
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        List<JVCO_BackgroundMatching__c> bgMatchingList = new List<JVCO_BackgroundMatching__c>();
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Trading Currency', JVCO_Default_Value__c = 'GBP'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Receivable Control', JVCO_Default_Value__c = '10040'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Output Vat Code', JVCO_Default_Value__c = 'GB-O-STD-LIC'));
        insert bgMatchingList;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
    
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        insert nonConvGLA;
        
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2();
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim3();
        
        Test.startTest();
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        JVCO_FFUtil.stopCodaCreditNoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountDLRSHandler = true;
        JVCO_FFUtil.stopCashTransferLogic = true;
        JVCO_FFUtil.stopCodaTransasctionLineItemHandlerAfterUpdate = true;
        JVCO_FFUtil.stopCodaTransasctionHandlerAfterUpdate = true;
        
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_TestClassHelper.setGeneralSettingsCS();

        c2g__codaCashEntry__c cEntry = new c2g__codaCashEntry__c();
        cEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cEntry.c2g__BankAccount__c = bAcc.Id;
        cEntry.c2g__CashEntryCurrency__c = accCurrency.Id;
        insert cEntry;
        
        c2g__codaCashEntryLineItem__c cLine = new c2g__codaCashEntryLineItem__c();
        cLine.c2g__Account__c = licAcc.Id;
        cLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cLine.c2g__CashEntryValue__c = 12000;
        cLine.c2g__CashEntry__c = cEntry.Id;
        insert cLine;
        Test.stopTest();
    }

    @isTest
    static void testPostCashEntry()
    {        
        List<c2g__codaCashEntry__c> cashEntryList = [SELECT Id FROM c2g__codaCashEntry__c];
        Test.startTest();
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(cashEntryList);
        ssc.setSelected(cashEntryList);
        JVCO_PostCashEntriesLogic pcel = new JVCO_PostCashEntriesLogic(ssc);

        PageReference pageRef = Page.JVCO_PostCashEntries; // Add your VF page Name here
        pageRef.getParameters().put('id', String.valueOf(cashEntryList[0].Id));
        Test.setCurrentPage(pageRef);
        pcel.postCashEntries();
        Test.stopTest();    
    }

    @isTest
    static void testBulkPostCashEntry()
    {
        JVCO_PostCashEntriesQueueable.doChainJob = false;
        List<c2g__codaCashEntry__c> cashEntryList = [SELECT Id FROM c2g__codaCashEntry__c];
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(cashEntryList);
        ssc.setSelected(cashEntryList);
        JVCO_PostCashEntriesLogic pcel = new JVCO_PostCashEntriesLogic(ssc);
        pcel.bulkPostCashEntries(); 
    }

    @isTest
    static void testDuplicatePosting()
    {        
        List<c2g__codaCashEntry__c> cashEntryList = [SELECT Id FROM c2g__codaCashEntry__c];
        Test.startTest();
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(cashEntryList);
        ssc.setSelected(cashEntryList);
        JVCO_PostCashEntriesLogic pcel = new JVCO_PostCashEntriesLogic(ssc);

        PageReference pageRef = Page.JVCO_PostCashEntries; // Add your VF page Name here
        pageRef.getParameters().put('id', String.valueOf(cashEntryList[0].Id));
        Test.setCurrentPage(pageRef);
        pcel.postCashEntries();
        pcel.postCashEntries();
        Test.stopTest();    
    }

    @isTest
    static void testPostCashEntryWithoutLines()
    {        
        c2g__codaBankAccount__c bAcc = [SELECT Id FROM c2g__codaBankAccount__c order by CreatedDate DESC limit 1];
        c2g__codaAccountingCurrency__c accCurrency = [SELECT Id FROM c2g__codaAccountingCurrency__c LIMIT 1];
        c2g__codaCashEntry__c cEntry = new c2g__codaCashEntry__c();
        cEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cEntry.c2g__BankAccount__c = bAcc.Id;
        cEntry.c2g__CashEntryCurrency__c = accCurrency.Id;
        insert cEntry;
        List<c2g__codaCashEntry__c> cashEntryList = new List<c2g__codaCashEntry__c>();
        cashEntryList.add(cEntry);

        Test.startTest();
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(cashEntryList);
        ssc.setSelected(cashEntryList);
        JVCO_PostCashEntriesLogic pcel = new JVCO_PostCashEntriesLogic(ssc);

        PageReference pageRef = Page.JVCO_PostCashEntries; // Add your VF page Name here
        pageRef.getParameters().put('id', String.valueOf(cashEntryList[0].Id));
        Test.setCurrentPage(pageRef);
        pcel.postCashEntries();
        Test.stopTest();    
    }

    @isTest
    static void testReturnToCashEntry()
    {        
        List<c2g__codaCashEntry__c> cashEntryList = [SELECT Id FROM c2g__codaCashEntry__c];
        Test.startTest();
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(cashEntryList);
        ssc.setSelected(cashEntryList);
        JVCO_PostCashEntriesLogic pcel = new JVCO_PostCashEntriesLogic(ssc);
        pcel.returntoCashEntry();
        Test.stopTest();    
    }

    @isTest
    static void testGetTransactionLineMap()
    {
        List<c2g__codaCashEntry__c> cashEntryList = [SELECT Id FROM c2g__codaCashEntry__c];
        List<List<c2g__codaCashEntry__c>> listOfCashEntryList = new List<List<c2g__codaCashEntry__c>>();
        listOfCashEntryList.add(cashEntryList);
        JVCO_PostCashEntriesQueueable pceq = new JVCO_PostCashEntriesQueueable(null, null);
        pceq.getTransactionLineMap(listOfCashEntryList);
    }
}