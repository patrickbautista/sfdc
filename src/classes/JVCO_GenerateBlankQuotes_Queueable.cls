/* ----------------------------------------------------------------------------------------------
    Name: JVCO_GenerateBlankQuotes_Queueable
    Description: Queueable implementation for JVCO_GenerateBlankQuotesBatch

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    08-Aug-2018     0.1         Jules Pablo         Intial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_GenerateBlankQuotes_Queueable implements Queueable {
	private Map<Id, JVCO_Affiliation__c> affilationsForCreation;
	private Date endDate;
	private Account accountRecord;
	private String queueableErrorMessage;
	private Integer blankQuoteCount;
	private Integer quoteCount;
	private Integer errorQuoteCount;

	public JVCO_GenerateBlankQuotes_Queueable(Set<Id> contractIds, Account accountRecord, String queueableErrorMessage, Integer quoteCount, Integer errorQuoteCount){
		List<SBQQ__Quote__c> generatedAmendmentQuotes = [select Id, Name, End_Date__c from SBQQ__Quote__c where SBQQ__MasterContract__c in: contractIds];
        endDate = NULL;
        this.accountRecord = accountRecord;
        this.queueableErrorMessage = queueableErrorMessage;
        this.quoteCount = quoteCount;
        this.errorQuoteCount = errorQuoteCount;
        blankQuoteCount = 0;

        if(!generatedAmendmentQuotes.isEmpty()) {
            Integer loopCtr = 0;
            for(SBQQ__Quote__c qRec : generatedAmendmentQuotes) {
                if(loopCtr == 0) {
                    endDate = qRec.End_Date__c;
                } else {
                    if(qRec.End_Date__c > endDate) {
                        endDate = qRec.End_Date__c;
                    }
                }
                loopCtr++;
            }
        }

        if (endDate == NULL) {
            affilationsForCreation = new Map<Id, JVCO_Affiliation__c>([select Id, Name, JVCO_Venue__r.Name, JVCO_Account__c, JVCO_Venue__c, JVCO_Start_Date__c, JVCO_End_Date__c, JVCO_Omit_Quote_Line_Group__c from JVCO_Affiliation__c where JVCO_Omit_Quote_Line_Group__c = FALSE and JVCO_Account__c = :accountRecord.Id and JVCO_End_Date__c = null AND Id NOT in (Select Affiliation__c from SBQQ__Subscription__c where SBQQ__Contract__r.JVCO_Renewal_Generated__c = false and SBQQ__Contract__r.AccountId = :accountRecord.Id) AND Id NOT in (Select JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c where SBQQ__Quote__r.SBQQ__Status__c = 'Draft' and SBQQ__Quote__r.SBQQ__Account__c = :accountRecord.Id)]); //Add GREEN-24997 jules.osberg.a.pablo
        } else {
            affilationsForCreation = new Map<Id, JVCO_Affiliation__c>([select Id, Name, JVCO_Venue__r.Name, JVCO_Account__c, JVCO_Venue__c, JVCO_Start_Date__c, JVCO_End_Date__c, JVCO_Omit_Quote_Line_Group__c from JVCO_Affiliation__c where JVCO_Omit_Quote_Line_Group__c = FALSE and JVCO_Account__c = :accountRecord.Id and JVCO_Start_Date__c <= :endDate and JVCO_End_Date__c = null AND Id NOT in (Select Affiliation__c from SBQQ__Subscription__c where SBQQ__Contract__r.JVCO_Renewal_Generated__c = false and SBQQ__Contract__r.AccountId = :accountRecord.Id) AND Id NOT in (Select JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c where SBQQ__Quote__r.SBQQ__Status__c = 'Draft' and SBQQ__Quote__r.SBQQ__Account__c = :accountRecord.Id)]); //02-Nov-2017 GREEN-25156 jules.osberg.a.pablo
        }
	}

	public JVCO_GenerateBlankQuotes_Queueable(Map<Id, JVCO_Affiliation__c> affilationsForCreation, Date endDate, Account accountRecord, String queueableErrorMessage, Integer quoteCount, Integer errorQuoteCount, Integer blankQuoteCount) {
		this.affilationsForCreation = affilationsForCreation;
		this.endDate = endDate;
		this.accountRecord = accountRecord;
		this.queueableErrorMessage = queueableErrorMessage;
		this.quoteCount = quoteCount;
        this.errorQuoteCount = errorQuoteCount;
        this.blankQuoteCount = blankQuoteCount;
	}

	public void execute(QueueableContext context) {
        //start 06-11-2017 mariel.m.buena GREEN-24769 Querying pricebook to map it to opportunity
        Map<Id, Pricebook2> pricebookId = new Map<Id, Pricebook2>([Select Id, Name
                                                                    From Pricebook2
                                                                    Where isStandard = true and isActive = true]);
        //end

        //start GREEN-25866 20-11-2017 jules.osberg.a.pablo
        Id pbId = pricebookId.values().size() > 0 ? pricebookId.values().get(0).Id:Null;

        if(!affilationsForCreation.isEmpty())
        {
        	JVCO_Affiliation__c affiliationRecord = affilationsForCreation.values()[0];

	        List<JVCO_Affiliation__c> tempAffilationsForCreation = new List<JVCO_Affiliation__c>();
        	tempAffilationsForCreation.add(affiliationRecord);

            JVCO_KeyAccountQuote.createQuote(tempAffilationsForCreation, pbId,  endDate, accountRecord); 
            blankQuoteCount++;
            affilationsForCreation.remove(affiliationRecord.Id);

            if(!Test.isRunningTest()) {
	           System.enqueueJob(new JVCO_GenerateBlankQuotes_Queueable(affilationsForCreation, endDate, accountRecord, queueableErrorMessage, quoteCount, errorQuoteCount, blankQuoteCount));
    	    }
        } else {
    		User currentUser = [SELECT Email FROM USER WHERE Id = :UserInfo.getUserId() LIMIT 1];
    		JVCO_KeyAccountQuote.sendEmailNotification(accountRecord, blankQuoteCount, 0, queueableErrorMessage, quoteCount, errorQuoteCount, currentUser.Email, true,null);
            
    	}
	}
}