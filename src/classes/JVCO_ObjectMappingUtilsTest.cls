@isTest
private class JVCO_ObjectMappingUtilsTest
{
	@testSetup
    private static void setupData()
    {
    	Test.StartTest();
		
		Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
		insert acc;

		Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
		insert acc2;

	    Lead l = JVCO_TestClassObjectBuilder.createLead();
	    insert l;
	}

	@isTest
	static void createAccount()
	{
		string LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
		string CustomerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
    	
    	Test.startTest();
    	Lead l = [SELECT Id, JVCO_Sole_Trader__c FROM Lead order by CreatedDate DESC limit 1];
    	Account a = [SELECT Id FROM Account WHERE RecordTypeId = :LicenceRT order by CreatedDate DESC limit 1];
		Test.StopTest();

		Account aObj = JVCO_LeadMappingLogic.createAccount(l, a.id);
 		System.AssertEquals(CustomerRT, aObj.RecordTypeId, 'Record Type ID was not changed to Customer Account');
	}  

	//Create Contact Obj
	@isTest
	static void createContact()
	{
		string LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    	
    	Test.startTest();
    	Lead l = [SELECT Id, JVCO_Sole_Trader__c FROM Lead order by CreatedDate DESC limit 1];
    	Account a = [SELECT Id FROM Account WHERE RecordTypeId = :LicenceRT order by CreatedDate DESC limit 1];
		Test.StopTest();

		Contact cObj = JVCO_LeadMappingLogic.createContact(l, a.id);
 		System.Assert(!String.isEmpty(cObj.LastName), 'No Contact was created'); 
	}      
}