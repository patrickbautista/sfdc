/* --------------------------------------------------------------------------------------------------
    Name:            JVCO_Payment_Flag_Sched.cls 
    Description:     Class that handles JVCO_Payment_Flag_Batch
    Test class:      

    Date             Version     Author                Summary of Changes 
    -----------      -------     -----------------     --------------------
    23-Apr-2018       0.1        franz.g.a.dimaapi      Intial draft
------------------------------------------------------------------------------------------------ */
public class JVCO_Payment_Flag_Sched implements Schedulable{
    
    public void execute(SchedulableContext sc)
    { 
        Database.executeBatch(new JVCO_Payment_Flag_Batch(), 1);
    }

    private static String getSchedulerExpression(DateTime dt) 
    {
         return '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();    
    }

    public static void callExecuteMomentarily()  
    {
        System.schedule('JVCO_Payment_Flag_Sched', getSchedulerExpression(DateTime.Now().addSeconds(5)), new JVCO_Payment_Flag_Sched());
    }
}