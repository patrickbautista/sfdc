/*--------------------------------------------------------------------------------------
Name: JVCO_LateDDIReminderScheduler.cls 
Description: Scheduler to execute JVCO_LateDDIReminder
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
23-Sep-2020  0.1         luke.walker     	   Initial creation
----------------------------------------------------------------------------------------*/

global class JVCO_LateDDIReminderScheduler implements Schedulable {
    
    public static JVCO_Constants__c constants = [SELECT id, JVCO_LateDDIReminder_Batch_Size__c FROM JVCO_Constants__c LIMIT 1];
    public static integer BatchSize = Integer.valueOf(constants.JVCO_LateDDIReminder_Batch_Size__c == null ? 20 : constants.JVCO_LateDDIReminder_Batch_Size__c);
    
    global void execute(SchedulableContext sc){        
    	Database.executeBatch(new JVCO_LateDDIReminder(), BatchSize);
    }
}