/* ----------------------------------------------------------------------------------------------------------
Name:            JVCO_EventTriggerHandlerTest.cls 
Description:     Test class for the trigger handler for JVCO_EventTrigger.trigger
Test class:      

Date             Version     Author                            Summary of Changes 
-----------      -------     -----------------                 -------------------------------------------
2-Nov-2017      0.1        Accenture-r.p.valencia.iii         Initial version of code
24-Jan-2018     0.2        Accenture-apple.j.a.manayaga       Added testreqDateNull GREEN-28077
---------------------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_EventTriggerHandlerTest
{
  @testSetup
    private static void setupData(){
       	JVCO_TestClassObjectBuilder.createPrimaryTariffMapping();
       	
        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
        acc2.JVCO_Live__c = true;
        insert acc2;

        JVCO_Venue__c ven = new JVCO_Venue__c();
        ven.Name = 'Sample Venue';
        ven.JVCO_Email__c = 'JVCO_QuoteTriggerHandlerTest@email.com';
        ven.External_Id__c = 'JVCO_QTHTestsampleaccexid';
        ven.JVCO_Field_Visit_Requested__c  = true;
        ven.JVCO_Field_Visit_Requested_Date__c = date.today();
        ven.JVCO_Postcode__c = 'BL2 1AA';

        insert ven;  


        JVCO_Artist__c art = new JVCO_Artist__c();
        art.Name = 'ArtistTest';
        insert art;

        JVCO_Event__c evt = new JVCO_Event__c();
        evt.Name = 'evtTest';
        evt.JVCO_Artist__c = art.Id;
        evt.JVCO_Venue__c = ven.Id;
        evt.JVCO_Event_Type__C = 'Concert – Qualifies for Old Rate';
        evt.JVCO_Event_Start_Date__c = date.today();
        evt.JVCO_Event_End_Date__c = date.today().addDays(30);
        evt.JVCO_Event_Classification_Status__c = null;
        evt.JVCO_Classification_Required_By_Date__c = null;
        evt.License_Account__c=acc2.id;
        evt.Headline_Type__c = 'No Headliner';
        evt.JVCO_Number_of_Set_Lists_Expected__c = 2;
        insert evt;

    }

  @isTest
  static void testEvtNameUpdate() 
  {
    JVCO_Event__c evt2 = new JVCO_Event__c();
    evt2 = [SELECT Id, Name FROM JVCO_Event__c order by CreatedDate DESC limit 1];
    evt2.Name = 'test';

    JVCO_Venue__c ven2 = new JVCO_Venue__c();
    JVCO_Artist__c art2 = new JVCO_Artist__c();


    Test.StartTest();
    update evt2;
    evt2 = [SELECT Id, Name, JVCO_Event_Start_Date__c FROM JVCO_Event__c order by CreatedDate DESC limit 1];
    ven2 = [SELECT Id, Name FROM JVCO_Venue__c order by CreatedDate DESC limit 1];
    art2 = [SELECT Id, Name FROM JVCO_Artist__c order by CreatedDate DESC limit 1];


    System.AssertEquals(evt2.Name, string.valueof(evt2.JVCO_Event_Start_Date__c.Day()) + '/' + string.valueof(evt2.JVCO_Event_Start_Date__c.Month()) + '/' + string.valueof(evt2.JVCO_Event_Start_Date__c.Year()) + '-' + art2.Name +'-' + ven2.Name);
    Test.StopTest();
  }

  @isTest
  static void testEvtNameOverride() 
  {
    JVCO_Event__c evt2 = new JVCO_Event__c();
    evt2 = [SELECT Id, Name FROM JVCO_Event__c order by CreatedDate DESC limit 1];
    evt2.Name = 'test';
    evt2.JVCO_Event_Name_Override__c = 'testOverride';

    Test.StartTest();
    update evt2;
    evt2 = [SELECT Id, Name FROM JVCO_Event__c order by CreatedDate DESC limit 1];

    System.AssertEquals(evt2.Name, 'testOverride');
    Test.StopTest();
  }

  @isTest
  static void testreqDateNull()
  {
    JVCO_Event__c evt2 = new JVCO_Event__c();
    evt2 = [SELECT Id, Name, JVCO_Classification_Required_By_Date__c, JVCO_Event_Classification_Status__c FROM JVCO_Event__c limit 1];
    evt2.Name = 'test';
    evt2.JVCO_Event_Classification_Status__c = 'Requested';
    

    Test.StartTest();
    update evt2;
    evt2 = [SELECT Id, Name, JVCO_Classification_Required_By_Date__c FROM JVCO_Event__c limit 1];
   
      
      Date expectedDate = System.today() + 7;
      Date classDate = evt2.JVCO_Classification_Required_By_Date__c;
      
      System.assertEquals(classDate, expectedDate);      
      Test.StopTest();
  }

}