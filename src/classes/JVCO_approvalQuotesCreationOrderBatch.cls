public class JVCO_approvalQuotesCreationOrderBatch implements Database.Batchable<sObject> 
{
    public Set<Id> oppIdSetForOrdring = new Set<Id>();
    public JVCO_approvalQuotesCreationOrderBatch(Set<Id> oppidSet) {
        oppIdSetForOrdring = oppidSet;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        system.debug('approvalprocessOrdering: '+oppIdSetForOrdring);
        return Database.getQueryLocator([SELECT Id, SBQQ__Ordered__c FROM Opportunity
                                         WHERE SBQQ__Contracted__c = TRUE
                                         AND Id IN: oppIdSetForOrdring]);
    }

    public void execute(Database.BatchableContext BC, List<Opportunity> oppToUpdateList )
    {   
        JVCO_OrderTriggerHandler.approvalCreateOrder = True;
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        Integer MaxOrderRetries;
        MaxOrderRetries = (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_MaxOrderRetries__c != null ? 
                          (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_MaxOrderRetries__c : 3;
        Integer j = 0;
        for (Opportunity opp : oppToUpdateList)
        {
            opp.SBQQ__Ordered__c = TRUE;
        }

        do 
        { 
            errLogList.clear();
            if (!oppToUpdateList.isEmpty())
            {
                System.debug('oppToUpdateList: ' + oppToUpdateList);
                
                List<Database.SaveResult> res = Database.update(oppToUpdateList,false);
                for(Integer i = 0; i < oppToUpdateList.size(); i++)
                {
                    Database.SaveResult srOppList = res[i];
                    Opportunity origrecord = oppToUpdateList[i];
                    if(!srOppList.isSuccess())
                    {
                        System.debug('Update opportunity fail: ');
                        for(Database.Error objErr:srOppList.getErrors())
                        {
                            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                            errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                            errLog.ffps_custRem__Related_Object_Key__c= string.valueof(origrecord.ID);
                            errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                            errLog.ffps_custRem__Message__c = 'ApprovalProcessAutomaticOrdering';
                            errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                            errLog.JVCO_Number_of_Tries__c = j + 1;
                            errLogList.add(errLog);       
                        }
                    }
                }

                if(!errLogList.isempty())
                {
                    try
                    {
                        insert errLogList;
                    }
                    catch(Exception ex)
                    {
                        System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                        
                    }
                } 
                j++;
            }
        }
        
        while(errLogList.size() > 0 && j < MaxOrderRetries);
    }
    
    public void finish(Database.BatchableContext BC)
    {
        
    }
    
}