public with sharing class SMPDeferralWizardController
{

    public List<directDebitWrapper> directDebitWrapperList {get; set;}
    public Integer stepNo {get; set;}
    public Account account {get; set;}
    public Income_Direct_Debit__c directDebit {get; set;}
    public List<Income_Direct_Debit__c> accountDDs {get; set;}
    public Boolean showBackonFirstPage {get; set;}
    public Integer monthsForDefer {get; set;}
    public recordsResults results  {get; set;}
    public List<SelectOption> months {get; set;}
    public Integer maxMonths  {get; set;}
    public List<dateAndAmount> datesPreviewList {get; set;}
    public Boolean dateSelected {get; set;}
    public Boolean recordSelected {get; set;}
    public Boolean processStarted {get; set;}

    public SMPDeferralWizardController(Apexpages.StandardController standardController)
    {
        processStarted = false;
        dateSelected = true;
        recordSelected = true;
        SMP_Months_To_Defer__c CS = SMP_Months_To_Defer__c.getInstance();
        maxMonths = Integer.valueOf(CS.Months__c);
        months = new List<SelectOption>();

        for (Integer i = 0; i <= maxMonths; i++) 
        {
            months.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
        }
        system.debug('#####months'+ months);

        directDebit = (Income_Direct_Debit__c)standardController.getRecord();

        directDebit = [SELECT DD_Next_Collection_Date__c, DD_Collection_Period__c, Id FROM Income_Direct_Debit__c WHERE Id = :directDebit.Id];
        system.debug('#####directDebit'+ directDebit);

        showBackonFirstPage = false;
        stepNo = 2;
        monthsForDefer = 0;
        //collectAllRecords();        
        updateDates();

    }

    public void updateDates()
    {               
        collectAllRecords();

        system.debug('######updateDates()');
        system.debug('######monthsForDefer ' + monthsForDefer);
        system.debug('######results' + results);
        system.debug('######results.Sales_Installment_Line_Items' + results.Sales_Installment_Line_Items);
        dateSelected = false;
        datesPreviewList =  new List<dateAndAmount>();

        for (c2g__codaInvoiceInstallmentLineItem__c var : results.Sales_Installment_Line_Items) 
        {
            SMP_DDServiceNextCollectionModel model = new SMP_DDServiceNextCollectionModel();
            SMP_DDServiceNextCollectionModel response = SMP_DDServiceHandler.getNextCollectionDate(String.valueOf(var.c2g__DueDate__c.Day()),var.c2g__DueDate__c.addMonths(Integer.valueOf(monthsForDefer)), results.Income_Direct_Debit.DD_Collection_Period__c, '5');
            system.debug('######response'+ response);
            //var.c2g__DueDate__c = Date.valueOf(response.ProcessNewDDFirstCollectionDateResult.FirstCollectionDate);
            dateAndAmount temp = new dateAndAmount();
            temp.Id = var.Id;
            temp.amount = var.c2g__Amount__c;

            if(!Test.isRunningTest())
            {
                temp.collectionDate = Date.valueOf(response.ProcessNewDDFirstCollectionDateResult.FirstCollectionDate);
                var.c2g__DueDate__c = Date.valueOf(response.ProcessNewDDFirstCollectionDateResult.FirstCollectionDate);
            }
            else 
            {
                temp.collectionDate = var.c2g__DueDate__c;
                var.c2g__DueDate__c = var.c2g__DueDate__c;
            }
            datesPreviewList.add(temp);
        }
        //update results.Sales_Installment_Line_Items;

        Map<Id, Income_Direct_Debit__c> ddMap = new Map<Id, Income_Direct_Debit__c>();

        results.Income_Direct_Debit.Send_DD_Documentation__c = 'Direct Debit Instruction Change';
        
        system.debug('##### results.Income_Direct_Debit' + results.Income_Direct_Debit);
        List<dateAndAmount> finalDates = new List<dateAndAmount>();
        List<Date> dates = new List<Date>();

        for (dateAndAmount variable : datesPreviewList) 
        {
            if(!listContains(dates, variable.collectionDate))
            {
                dates.add(variable.collectionDate);
            }

        }
        dateAndAmount temp = null;
        for (Date dateVar : dates)  
        {
            temp = new dateAndAmount();
            temp.collectionDate = dateVar;
            temp.amount = 0;
            for (dateAndAmount variable : datesPreviewList) 
            {
                if(variable.collectionDate == dateVar)
                {
                    temp.amount += variable.amount;  
                }
            }
            finalDates.add(temp);
        }

        datesPreviewList = finalDates;

        datesPreviewList = sortDates(datesPreviewList);
        system.debug('#####datesPreviewList '+ datesPreviewList);
      
        // update results.Income_Direct_Debit;

    }
    public void collectAllRecords()
    {
        List<Id> ddIds = new List<Id>();
        ddIds.add(directDebit.Id);
            
        system.debug('##### ddIds ' + ddIds);

        List<SMP_DirectDebit_GroupInvoice__c> DDGroupInvoice = [SELECT Income_Direct_Debit__c, Id, Invoice_Group__c, JVCO_Number_of_Paid_Installments__c, Name FROM SMP_DirectDebit_GroupInvoice__c WHERE Income_Direct_Debit__c IN :ddIds];
        system.debug('##### DDGroupInvoice ' + DDGroupInvoice);

        List<Id> ddGroupInvoiceIds = new List<Id>();

        for (SMP_DirectDebit_GroupInvoice__c var : DDGroupInvoice) 
        {
            ddGroupInvoiceIds.add(var.Invoice_Group__c);
        }

        List<c2g__codaInvoice__c> saleInvoices = [SELECT Id, JVCO_Invoice_Group__c, c2g__FirstDueDate__c, JVCO_First_Due_Date__c, JVCO_Latest_Payment_Date__c,c2g__InvoiceTotal__c  FROM c2g__codaInvoice__c  WHERE JVCO_Invoice_Group__c IN : ddGroupInvoiceIds];
        system.debug('##### saleInvoices ' + saleInvoices);

        List<Id> saleInvoiceIDS = new List<Id>();

        for (c2g__codaInvoice__c var : saleInvoices) 
        {
            saleInvoiceIDS.add(var.Id);
        }
        
        List<c2g__codaInvoiceInstallmentLineItem__c> lineItems = new List<c2g__codaInvoiceInstallmentLineItem__c>();
        Date todaysDate = Date.today();
        for(c2g__codaInvoiceInstallmentLineItem__c sli : [SELECT Id, c2g__DueDate__c, 
                                                          c2g__Amount__c, c2g__Invoice__r.c2g__InvoiceTotal__c 
                                                          FROM c2g__codaInvoiceInstallmentLineItem__c 
                                                          WHERE c2g__Invoice__c IN : saleInvoiceIDS 
                                                          AND JVCO_Payment_Status__c != 'Paid'])
        {
            if(todaysDate.daysBetween(sli.c2g__DueDate__c) > 4){
                lineItems.add(sli);
            }
        }
        system.debug('##### lineItems ' + lineItems);

        results = new recordsResults();

        results.Income_Direct_Debit = directDebit;
        results.Sales_Invoices = saleInvoices;
        results.Sales_Installment_Line_Items = lineItems;
        results.SMP_DirectDebit_GroupInvoice = DDGroupInvoice;
        system.debug('##### results ' + results);
    }
    public Pagereference cancel()
    {
        Pagereference ref = new Pagereference('/' + directDebit.Id);
        ref.setRedirect(true);
        return ref;
    }
    Boolean listContains(Object[] source, Object target) {
        return (new Set<Object>(source)).contains(target);
    }
    public static List<dateAndAmount> removeObjectFromList(List<dateAndAmount> lst, dateAndAmount o) {
        List<dateAndAmount> result = new List<dateAndAmount>();
        system.debug('##### o '+ o);
        system.debug('##### lst '+ lst);

        for (dateAndAmount variable : lst) 
        {
            system.debug('##### variable.Id '+ variable.Id);
            system.debug('##### o.id '+ o.id);

           if(variable.Id != o.id)
           {
            system.debug('##### added ');

                result.add(variable);
           }
        }
        system.debug('##### result ' + result);

        return result;
    }
    public List<dateAndAmount> sortDates(List<dateAndAmount> pList)
    {
       system.debug('##### pList '+ pList);
       Integer size = pList.size();
       List<Integer> blackListed = new List<Integer>();
       List<dateAndAmount> sortedDates = new List<dateAndAmount>();
       dateAndAmount temp = null;
       for (Integer index = 0; index < size; index++) 
       {
        system.debug('##### pList[index] '+ pList[index]);
        system.debug('##### index '+ index);

            Integer current = 0;
            for (dateAndAmount secondDate : pList) 
            {            
                system.debug('##### secondDate '+ secondDate);
 
                if(pList[index].collectionDate < secondDate.collectionDate && listContains(blackListed, current))
                {
                    current = index;
                }
               
            }
            if(index == size-1)
            {
                current = size-1;
            }
            sortedDates.add(pList[current]);
            blackListed.add(current);
            system.debug('##### blackListed '+ blackListed);

       }
       system.debug('##### sortedDates '+ sortedDates);
       return sortedDates;
    }
    public void enabledNext()
    {
        recordSelected = false;
    }
    public void next()
    {            

        stepNo++;
        system.debug('##### stepNo '+ stepNo);

        if(stepNo > 1)
        {
            showBackonFirstPage = true;
        }
        if(stepNo == 2)
        {
            // collectAllRecords();
        }
        if(stepNo == 3)
        {
            // updateDates();
        }
    }
    public void back()
    {
        stepNo--;
        if(stepNo == 1)
        {
            showBackonFirstPage = false;
        }
    }
    public Pagereference process()
    {
        if(monthsForDefer == 0)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Months to defer must be over 0'));
            return null;
        }

        if(processStarted)
        {
            return null;
        }
        processStarted = true;
        stepNo++;
        // List<c2g__codaInvoiceInstallmentLineItem__c> lineItems = new List<c2g__codaInvoiceInstallmentLineItem__c>();
        // for (Integer i = 0; i < results.Sales_Installment_Line_Items.size(); i++) 
        // {
        //     c2g__codaInvoiceInstallmentLineItem__c temp = new c2g__codaInvoiceInstallmentLineItem__c();
        //     temp.Id = results.Sales_Installment_Line_Items[i].Id;
        //     temp.c2g__DueDate__c = results.Sales_Installment_Line_Items[i].collectionDate;

        //     lineItems.add(temp);
        // }
        update results.Sales_Installment_Line_Items;

        if(!Test.isRunningTest()){
            results.Income_Direct_Debit.DD_Next_Collection_Date__c = results.Sales_Installment_Line_Items[results.Sales_Installment_Line_Items.size()-1].c2g__DueDate__c;
        }
        // system.debug('#### DD_Next_Collection_Date__c ' + results.Income_Direct_Debit.DD_Next_Collection_Date__c);

        update results.Income_Direct_Debit;

        system.debug('#### update ' + results.Sales_Installment_Line_Items);

        Pagereference pg = new Pagereference('/' + results.Income_Direct_Debit.Id);

        return pg;

    }

    public class directDebitWrapper
    {
        public String Id {get; set;}
        public String name {get; set;}
        public Date firstCollectionDate {get; set;}
        public Decimal firstCollectionAmount {get; set;}
        public Boolean isDeferralable {get; set;}
        public Boolean selected {get; set;}

    }
    public class recordsResults
    {
        List<SMP_DirectDebit_GroupInvoice__c> SMP_DirectDebit_GroupInvoice;
        List<c2g__codaInvoice__c> Sales_Invoices;
        List<c2g__codaInvoiceInstallmentLineItem__c> Sales_Installment_Line_Items;
        Income_Direct_Debit__c Income_Direct_Debit; 
    }
    public class ddWrapper
    {
        public Income_Direct_Debit__c directDebit {get; set;}
        public List<JVCO_Invoice_Group__c> invoiceGroupList {get; set;}
        public List<c2g__codaInvoiceInstallmentLineItem__c> iliList {get; set;}
        public Decimal finalPaymentAmount {get; set;}
        public Decimal nextCollectionAmount {get; set;}
        public Decimal amountLeftToCollect {get; set;}
    }
    public class dateAndAmount
    {
        public String Id {get; set;}
        public Date collectionDate {get; set;}
        public Decimal amount {get; set;}
    }
}