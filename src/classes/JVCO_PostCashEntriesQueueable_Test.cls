/* ----------------------------------------------------------------------------------------------
Name: JVCO_PostCashEntriesQueueable_Test.cls 
Description: Test Class for JVCO_PostCashEntriesBatch          
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
13-09-2017   0.1         James Melville        Intial creation
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_PostCashEntriesQueueable_Test {
    
    /* ----------------------------------------------------------------------------------------------
    Author: james.melville
    Company: Accenture
    Description: Sets up the needed records for testing
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    11-May-2017 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
@testSetup static void createTestData() 
    {
        List<RecordType> rtypesCENLIH = [SELECT Name, Id FROM RecordType WHERE sObjectType='JVCO_CashEntryLineItemHistory__c' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> RecordTypesCENLIH  = new Map<String,String>{};
        for(RecordType rt: rtypesCENLIH)
        {
          RecordTypesCENLIH.put(rt.Name,rt.Id);
        }
        List<CashEntryLineItemHistory_CS__c> settingsCENLIH = new List<CashEntryLineItemHistory_CS__c>();
        settingsCENLIH.add(new CashEntryLineItemHistory_CS__c(Name = 'Matched', Value__c = RecordTypesCENLIH.get('Matched Cash')));
        settingsCENLIH.add(new CashEntryLineItemHistory_CS__c(Name = 'Reversed', Value__c = RecordTypesCENLIH.get('Reversed Cash')));
        insert settingsCENLIH;
        
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        //Settings
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_codaCreditNoteTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        insert dt;
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        c2g__codaDimension2__c dim2 = JVCO_TestClassHelper.setDim2('PPL');
        insert dim2;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        JVCO_SLA_Constants__c cs = JVCO_TestClassObjectBuilder.createSLAConstants();
        insert cs;

        Entitlement e = JVCO_TestClassObjectBuilder.createEntitlement(licAcc.id);
        insert e;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Test.startTest();
        List<c2g__codaInvoice__c> sInvList = new List<c2g__codaInvoice__c>();
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        sInvList.add(sInv);
        c2g__codaInvoice__c sInv2 = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        sInvList.add(sInv2);
        insert sInvList;
        
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv2.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);

        //Create Cash Entry
        c2g__codaCashEntry__c testCashEntry = new c2g__codaCashEntry__c();
        testCashEntry.c2g__PaymentMethod__c = 'Electronic';
        testCashEntry.c2g__OwnerCompany__c = comp.id;
        testCashEntry.ownerid = testGroup.Id;
        testCashEntry.c2g__Status__c = 'In Progress';
        testCashEntry.c2g__Date__c = Date.today();
        testCashEntry.c2g__Value__c = 100.00;
        insert testCashEntry;
        
        //Create Cash Entry
        c2g__codaCashEntry__c testCashEntry2 = new c2g__codaCashEntry__c();
        testCashEntry2.c2g__PaymentMethod__c = 'Electronic';
        testCashEntry2.c2g__OwnerCompany__c = comp.id;
        testCashEntry2.ownerid = testGroup.Id;
        testCashEntry2.c2g__Status__c = 'In Progress';
        testCashEntry2.c2g__Date__c = Date.today();
        testCashEntry2.c2g__Value__c = 100.00;
        insert testCashEntry2;
        
        List<c2g__codaInvoice__c> invoiceList = [select id, Name from c2g__codaInvoice__c limit 2];
        //Create Cash Entry Line Item
        List<c2g__codaCashEntryLineItem__c> cashEntryLIList = new List<c2g__codaCashEntryLineItem__c>();
        c2g__codaCashEntryLineItem__c cashEntryLineItem1 = new c2g__codaCashEntryLineItem__c();
        cashEntryLineItem1.c2g__CashEntry__c = testCashEntry.id;
        cashEntryLineItem1.c2g__Account__c = licAcc.id;
        cashEntryLineItem1.c2g__CashEntryValue__c = 100;
        cashEntryLineItem1.c2g__LineNumber__c = 1;
        cashEntryLineItem1.c2g__AccountReference__c = invoiceList[0].Name;
        cashEntryLineItem1.c2g__AccountPaymentMethod__c = 'Cheque';
        cashEntryLineItem1.c2g__OwnerCompany__c = comp.id;
        cashEntryLineItem1.JVCO_Temp_AccountReference__c = 'SIN00001';
        cashEntryLineItem1.JVCO_CaseRecordType_Insolvency__c = '';
        cashEntryLIList.add(cashEntryLineItem1);

        c2g__codaCashEntryLineItem__c cashEntryLineItem2 = new c2g__codaCashEntryLineItem__c();
        cashEntryLineItem2.c2g__CashEntry__c = testCashEntry2.id;
        cashEntryLineItem2.c2g__Account__c = licAcc.id;
        cashEntryLineItem2.c2g__CashEntryValue__c = 100;
        cashEntryLineItem2.c2g__LineNumber__c = 2;
        cashEntryLineItem2.c2g__AccountReference__c = invoiceList[1].Name;
        cashEntryLineItem2.c2g__AccountPaymentMethod__c = 'Cheque';
        cashEntryLineItem2.c2g__OwnerCompany__c = comp.id;
        cashEntryLineItem2.JVCO_Temp_AccountReference__c = 'SIN00002';
        cashEntryLineItem2.JVCO_CaseRecordType_Insolvency__c = '';
        cashEntryLIList.add(cashEntryLineItem2);
        insert cashEntryLIList;
        Test.stopTest();
        
        
    }

    @isTest 
    public static void testPostBulkCashEntries()
    {   
        //Turn off Background Matching
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        Test.startTest();

        JVCO_PostCashEntryQueueable.start();

        Test.stopTest();

        //system.assertEquals(true,false,[SELECT Id, blng__ErrorCode__c, blng__FullErrorLog__c FROM blng__ErrorLog__c]);

        c2g__codaCashEntry__c postedCashEntry = [SELECT Id, c2g__Status__c 
                                                 FROM c2g__codaCashEntry__c 
                                                 LIMIT 1];
                                                 
        //system.assertEquals('Posted',postedCashEntry.c2g__Status__c);

        
    }
    
    @isTest static void testPostCashEntryOverrideQueryString()
    {
        //Turn off Background Matching
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        String query = 'SELECT Id FROM c2g__codaCashEntry__c WHERE c2g__Status__c = \'In Progress\' AND Id IN (SELECT c2g__CashEntry__c FROM c2g__codaCashEntryLineItem__c WHERE c2g__Account__c != null AND c2g__CashEntry__r.c2g__Status__c = \'In Progress\' ) ORDER BY c2g__Account__c';
        Test.startTest();
        JVCO_PostCashEntryQueueable.start(query);
        Test.stopTest();
        
    }
    
    @isTest static void testPostCashEntryoverrideBatchSize()
    {
        //Turn off Background Matching
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        String query = 'SELECT Id FROM c2g__codaCashEntry__c WHERE c2g__Status__c = \'In Progress\' AND Id IN (SELECT c2g__CashEntry__c FROM c2g__codaCashEntryLineItem__c WHERE c2g__Account__c != null AND c2g__CashEntry__r.c2g__Status__c = \'In Progress\' ) ORDER BY c2g__Account__c';
        Test.startTest();
        JVCO_PostCashEntryQueueable.start(query,2);
        Test.stopTest();
    }
    
    @isTest static void testPostCashEntryoverrideBatchSizeOnly()
    {
        //Turn off Background Matching
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        Test.startTest();
        JVCO_PostCashEntryQueueable.start(2);
        Test.stopTest();
        
    }
    
    @isTest static void testPostCashEntryoverrideBatchSizeQueueableLimit()
    {
        //Turn off Background Matching
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        Test.startTest();
        JVCO_PostCashEntryQueueable.start(2,5);
        Test.stopTest();
        
    }
    
    @isTest static void testErrorLogs()
    {  
        Id testId = [SELECT Id FROM c2g__codaCashEntry__c LIMIT 1].Id;
        Test.startTest();
        ffps_custRem__Custom_Log__c blngError = JVCO_PostCashEntryQueueable.createErrorLogs('test','test', testId);
        Test.stopTest();
    }
}