/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_BatchJobHelper.cls 
   Description:     Helper for KA Batches to show validation when nearing flex queue limits or when there are multiple large batch jobs
   Test class:      JVCO_BatchJobHelperTest.cls 

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  11-Oct-2018   0.1         rhys.j.c.dela.cruz	Intial creation
---------------------------------------------------------------------------------------------------------- */

public class JVCO_BatchJobHelper{

	public JVCO_BatchJobHelper(){
		
	}

	public static Boolean checkLargeJobs(Integer recordsToProcess){
		
		//Check how many will be processed and then validation if there are already (largeJobThreshold) large jobs are processing.

		if(JVCO_BatchJobHelperSettings__c.getOrgDefaults().JVCO_BatchJobHelperActive__c){

			Boolean isLargeJob = false;
			Boolean isAboveFlexLimit = aboveFlexLimit();

			//If Above Flex Limit, return false to prevent run of Batch
			if(isAboveFlexLimit){
				return false;
			}
			else{

				Integer recordJobItemThreshold = (Integer)JVCO_BatchJobHelperSettings__c.getOrgDefaults().JVCO_NoOfJobItemsTreshold__c != null ? (Integer)JVCO_BatchJobHelperSettings__c.getOrgDefaults().JVCO_NoOfJobItemsTreshold__c : 10;

				Integer largeJobThreshold = (Integer)JVCO_BatchJobHelperSettings__c.getOrgDefaults().JVCO_LargeJobThreshold__c != null ? (Integer)JVCO_BatchJobHelperSettings__c.getOrgDefaults().JVCO_LargeJobThreshold__c : 4;

				List<AsyncApexJob> apexJobsList = new List<AsyncApexJob>([SELECT Id, TotalJobItems FROM AsyncApexJob WHERE JobType = 'BatchApex' AND Status = 'Processing' AND TotalJobItems >: recordJobItemThreshold]);

				if(recordsToProcess > recordJobItemThreshold){
					isLargeJob = true;
				}

				//If Jobs processing are already above largeJobThreshold, prevent run of Batch
				if(apexJobsList.size() >= largeJobThreshold && isLargeJob){
					return false;
				}
				else{
					return true;
				}	
			}
		}
		else{
			return true;
		}
	}

	public static Boolean aboveFlexLimit(){
		//Check if Flex Queue above limit
		//Custom setting for limit

		Integer flexQueueLimit = (Integer)JVCO_BatchJobHelperSettings__c.getOrgDefaults().JVCO_FlexQueueLimit__c != null ? (Integer)JVCO_BatchJobHelperSettings__c.getOrgDefaults().JVCO_FlexQueueLimit__c : 90;

		Integer countOfJobs = [SELECT Count() FROM AsyncApexJob WHERE JobType= 'BatchApex' AND Status IN ('Processing','Preparing','Queued', 'Holding')];
		if(countOfJobs >= flexQueueLimit){
			return true;	
		}
		else{
			return false;
		}
	}
}