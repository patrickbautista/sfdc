/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CompleteRenewalQuoteBatchTest
   Description: Test Class of Complete Renewal Quote Batch

   Date         Version     Author                          Summary of Changes 
   -----------  -------     -----------------               -----------------------------------------
  05-Jul-2017   0.1         Accenture-erica.jane.w.matias   Intial creation
  14-Nov-2017   0.2         Accenture-rhys.j.c.dela.cruz    Adjusted testSetup
  
  ----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_CompleteRenewalQuoteBatchTest
{
    @testSetup static void insertValues()
    {
        
        List<JVCO_Constants__c> custSetting = new List<JVCO_Constants__c>();
        custSetting.add(new JVCO_Constants__c(JVCO_Quote_Recalculation_Time_Buffer__c = 5));
        insert custSetting;
        
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();  
        insert qProcess;

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Account acc =  JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.JVCO_Renewal_Scenario__c = '';
        insert acc2;

        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_End_Date__c = null;
        aff.JVCO_Closure_Reason__c = null;
        insert aff;
        
        Contract con = JVCO_TestClassObjectBuilder.createContract(acc2.Id, null);
        con.StartDate = Date.newInstance(2019, 05, 01);
        con.EndDate =  Date.newInstance(2020, 04, 31);
        con.JVCO_TempStartDate__c = Date.newInstance(2020, 04, 01);
        con.JVCO_TempEndDate__c = Date.newInstance(2020, 04, 15);
        insert con;

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        opp.JVCO_OpportunityCancelled__c = false;
        opp.SBQQ__Contracted__c = false;
        opp.SBQQ__RenewedContract__c = con.Id;
        insert opp;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, 'Annually');
        q.JVCO_Quote_Auto_Renewed__c = true;
        q.SBQQ__Status__c = 'Draft';
        q.Recalculate__c = false;
        q.SBQQ__Type__c = 'Quote'; 
        q.SBQQ__Primary__c = true;    
        insert q;
        SBQQ.TriggerControl.disable();

        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];
       
        q.SBQQ__Type__c = 'Renewal';
        q.Recalculate__c = true;
        update q;
        
        //SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, prod.Id);

        ql.CreatedDate = Datetime.Now();
        //ql.JVCO_Salesforce_Last_Recalculated_Time__c = Datetime.Now().addDays(3);
        ql.JVCO_Steelbrick_Last_Recalculated_Time__c = Datetime.Now().addDays(-1);
        ql.SPVAvailability__c = true;
        insert ql;
		SBQQ.TriggerControl.enable();
        //q.SBQQ__Type__c = 'Renewal';
        //q.JVCO_Quote_Auto_Renewed__c = true;
        //q.SBQQ__Status__c = 'Draft';
        //q.Recalculate__c = true;
        //ql.JVCO_Salesforce_Last_Recalculated_Time__c = Datetime.Now().addDays(3);
        //update q;

        //system.assertEquals(null, [select id, SBQQ__LineItemCount__c, JVCO_Recalculated__c from  sbqq__quote__c where id = :q.id]);

        List<JVCO_Renewal_Settings__c> settings = new List<JVCO_Renewal_Settings__c>();
        settings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Completion', Days__c = 0.0));
        settings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Generation', Days__c = 29));
        insert settings;
        

    }

    @isTest
    static void testCompleteRenewalQuoteBatch()
    {
        SBQQ__Quote__c q = [select Id, SBQQ__Status__c,SBQQ__Type__c,JVCO_Quote_Auto_Renewed__c,JVCO_Transition_Renewal_Scenario__c,SBQQ__LineItemCount__c, JVCO_Recalculated__c,SBQQ__NetAmount__c, JVCO_Last_Recalculated_Time_Delta__c  from SBQQ__Quote__c  limit 1];
        Opportunity o = [select id,JVCO_OpportunityCancelled__c from Opportunity limit 1];

        q.JVCO_Quote_Auto_Renewed__c = true;
        q.Recalculate__c = true;
        update q;

        //system.assertEquals(14, q.JVCO_Last_Recalculated_Time_Delta__c, 'Check last recalculated');
        system.assertEquals('Renewal', q.SBQQ__Type__c, 'Quote not Renewal');
        system.assertEquals(True, q.JVCO_Quote_Auto_Renewed__c, 'Quote not auto renewed');
        system.assertEquals('Draft', q.SBQQ__Status__c, 'Quote not draft');
        //system.assertEquals('', q.JVCO_Transition_Renewal_Scenario__c, 'Quote JVCO_Transition_Renewal_Scenario__c not blank');
        //system.assertNotEquals(0, q.SBQQ__LineItemCount__c, 'quote no quote line');
        system.assertEquals(True, q.JVCO_Recalculated__c, 'Quote not calculated');
        system.assertEquals(False, o.JVCO_OpportunityCancelled__c, 'Opportunity cancelled');
        system.assertNotEquals(0, q.SBQQ__NetAmount__c, '0 net amount');
		    
        Test.startTest(); 
        SBQQ.TriggerControl.disable();
        database.executeBatch(new JVCO_CompleteRenewalQuoteBatch());
        Test.stopTest();

        //Opportunity o = [select Id, StageName from Opportunity limit 1];
        //SBQQ__Quote__c q = [select Id, SBQQ__Status__c from SBQQ__Quote__c  limit 1];

        //system.assertEquals('Closed Won', o.StageName, 'Opportunity was not updated');
   
        //system.assertEquals('Complete and Invoiced', q.SBQQ__Status__c, 'Quote was not updated');
         
    }

    @isTest
    static void testCompleteRenewalQuoteBatchString()
    {
        SBQQ__Quote__c q = [select Id, SBQQ__Status__c from SBQQ__Quote__c  limit 1];

        
        Test.startTest();
        SBQQ.TriggerControl.disable();
        database.executeBatch(new JVCO_CompleteRenewalQuoteBatch(q.id));
        Test.stopTest();

        Opportunity o = [select Id, StageName from Opportunity limit 1];
        SBQQ__Quote__c q2 = [select Id, SBQQ__Status__c from SBQQ__Quote__c  limit 1];

        //system.assertEquals('Closed Won', o.StageName, 'Opportunity was not updated');
   
        //system.assertEquals('Complete and Invoiced', q2.SBQQ__Status__c, 'Quote was not updated');
         
    }
    
    @isTest
    static void testCompleteRenewalQuoteBatchList()
    {
        SBQQ__Quote__c q = [select Id, SBQQ__Status__c from SBQQ__Quote__c  limit 1];
		
        
        Test.startTest();
        SBQQ.TriggerControl.disable();
        database.executeBatch(new JVCO_CompleteRenewalQuoteBatch(q.Id));
        Test.stopTest();

        Opportunity o = [select Id, StageName from Opportunity limit 1];
        SBQQ__Quote__c q2 = [select Id, SBQQ__Status__c from SBQQ__Quote__c  limit 1];

        //system.assertEquals('Closed Won', o.StageName, 'Opportunity was not updated');
   
        //system.assertEquals('Complete and Invoiced', q2.SBQQ__Status__c, 'Quote was not updated');
         
    }

    @isTest
    static void testCompletreneweRenewalQuoteBatchSet()
    {
        list<SBQQ__Quote__c> l_q = [select Id from SBQQ__Quote__c  limit 1];
        set<id> set_id = new set<id>();

        for (SBQQ__Quote__c quote : l_q)
        {
            set_id.add(quote.id);
        }

        /*system.assertEquals(null, [SELECT ID, SBQQ__Type__c, JVCO_Quote_Auto_Renewed__c, SBQQ__Status__c, SBQQ__Opportunity2__c, JVCO_Transition_Renewal_Scenario__c, SBQQ__LineItemCount__c, JVCO_Recalculated__c  FROM SBQQ__Quote__c]);

        system.assertEquals(null,[SELECT ID, SBQQ__Type__c, JVCO_Quote_Auto_Renewed__c, SBQQ__Status__c, SBQQ__Opportunity2__c  FROM SBQQ__Quote__c 
                WHERE SBQQ__Type__c = 'Renewal'  AND JVCO_Quote_Auto_Renewed__c = false AND SBQQ__Status__c = 'Draft'  AND (JVCO_Transition_Renewal_Scenario__c = '' OR JVCO_Transition_Renewal_Scenario__c = 'PPL First: Greater Than 60 Days' OR JVCO_Transition_Renewal_Scenario__c = 'Single Society - PPL' OR JVCO_Transition_Renewal_Scenario__c = 'Single Society - PRS')  AND SBQQ__LineItemCount__c > 0 AND JVCO_Recalculated__c = TRUE]);
        */
		
        Test.startTest();
        SBQQ.TriggerControl.disable();
        database.executeBatch(new JVCO_CompleteRenewalQuoteBatch(set_id));
        Test.stopTest();

        Opportunity o = [select Id, StageName from Opportunity limit 1];
        SBQQ__Quote__c q2 = [select Id, SBQQ__Status__c from SBQQ__Quote__c  limit 1];
        //system.assertEquals('Closed Won', o.StageName, 'Opportunity was not updated');
   
        //system.assertEquals('Complete and Invoiced', q2.SBQQ__Status__c, 'Quote was not updated');
         
    }
}