@isTest
private class JVCO_CampaignHandlerTest
{
	@testSetUp static void setUpTestData(){

		// Setup for Campaign Related operations
        List<Campaign> newCampaignList = new List<Campaign>{
            (createCampaign('Campaign_ChannelUpdatedxx1', 'Email', null)),
            (createCampaign('Campaign_PartnerUpdatedxx2', 'Telesales', 'Ventrica')),
            (createCampaign('Campaign_AddInvalidMembersxx', 'Telesales', 'MarketMakers'))
        };
        insert newCampaignList;
	}

	static Campaign createCampaign(String name, String channel, String partner)
    {
        Date startDate = Date.newInstance(2016, 10, 10);
        Date endDate = Date.newInstance(2016, 11, 11);

        return (new Campaign(Name=name, Type='Churn',
                JVCO_Campaign_Channel__c=channel, JVCO_Campaign_Category__c='Shop',
                JVCO_Telesales_Partner__c=partner, StartDate=startDate, EndDate=endDate));
    }

	@isTest
	static void afterUpdate_Test()
	{
		list<Campaign> campList = [select id, JVCO_Campaign_Channel__c, JVCO_Telesales_Partner__c from Campaign where name like 'Campaign_%'];
		map<id, Campaign> oldCampList = new map<id, Campaign>(campList);


		test.startTest();

		for(Campaign c: campList){
			if(c.jvco_campaign_channel__c == 'Email'){
				c.jvco_campaign_channel__c = 'Telesales';
				c.jvco_telesales_partner__c = 'MarketMakers';
			}else{
				c.jvco_campaign_channel__c = 'Email';
				c.jvco_telesales_partner__c = null;
			}
		}
		update campList;

		for(campaign c: campList){
			system.assertEquals(c.jvco_campaign_channel__c,oldCampList.get(c.id).jvco_campaign_channel__c,'is Not Channel Equal');
			system.assertEquals(c.jvco_telesales_partner__c, oldCampList.get(c.id).JVCO_Telesales_Partner__c,'is Not Telesales Equal');
		}



		//JVCO_CampaignHandler.afterUpdate(campList, oldCampList);


	}
}