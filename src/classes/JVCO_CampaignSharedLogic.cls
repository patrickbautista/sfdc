/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CampaignSharedLogic.cls 
   Description: Logic class common for Campaign and CampaignMember

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  06-Sep-2016   0.1         ryan.i.r.limlingan  Intial creation
  07-Sep-2016   0.2         ryan.i.r.limlingan  Added more functions common to Campaign and
                                                CampaignMember
  14-Sep-2016   0.3         ryan.i.r.limlingan  Added check to prevent multiple firing of trigger;
                                                Declared constant variables;
                                                Added Test.isRunning to hit lines for error handling
  19-Sep-2016   0.4         ryan.i.r.limlingan  Modified how trigger from Campaign is handled
  20-Sep-2016   0.5         ryan.i.r.limlingan  Changed return type of constructQueueNameIdMap()
  ----------------------------------------------------------------------------------------------- */
public with sharing class JVCO_CampaignSharedLogic {

    // Variable that contains all Campaign Ids that have already
    // been updated to prevent multiple calls to the trigger
    static Set<Campaign> updatedCampaignSet = null;
    // Map of Telesales Partner and its Queue Id
    static Map<String, Id> queueNameIdMap = new Map<String, Id>();
    // List of String that stores information on records that failed to update
    public static List<String> errorsList = new List<String>();
    // Variable that checks if error should be forced for coverage testing
    public static Boolean shouldInvalidate = FALSE;
    // Number of records to force error
    public static final Integer NUM_RECORDS_TO_FAIL = 100;

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Constructs the Map for Telesales Partners and their Id
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    06-Sep-2016 ryan.i.r.limlingan  Initial version of function
    07-Sep-2016 ryan.i.r.limlingan  Modified to dynamically get picklist values
    20-Sep-2016 ryan.i.r.limlingan  Modified return type
  ----------------------------------------------------------------------------------------------- */
    public static void constructQueueNameIdMap()
    {
        if (queueNameIdMap.isEmpty())
        {
            // Get Picklist values of Campaign.JVCO_Telesales_Partner__c
            Schema.DescribeFieldResult fr = Campaign.JVCO_Telesales_Partner__c.getDescribe();
            List<Schema.PicklistEntry> ple = fr.getPicklistValues();

            List<String> partnerNames = new List<String>();
            for (Schema.PicklistEntry partner : ple)
            {
                partnerNames.add(partner.getValue());
            }

            // Retrieve Telesales Partner Queues
            List<Group> telesalesPartners = [SELECT Id, Name FROM Group
                                             WHERE Type = 'Queue' AND Name IN :partnerNames];

            // Populate map
            for (Group partner : telesalesPartners)
            {
                queueNameIdMap.put(partner.Name, partner.Id);
            }
        }
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Retrieves CampaignIds of CampaignMembers
    Inputs: List<CampaignMember>
    Returns: Set<Id>
    <Date>      <Authors Name>      <Brief Description of Change> 
    06-Sep-2016 ryan.i.r.limlingan  Initial version of function
    07-Sep-2016 ryan.i.r.limlingan  Changed parameter to List<CampaignMember>
  ----------------------------------------------------------------------------------------------- */
    public static Set<Id> retrieveCampaignIds(List<CampaignMember> campaignMemberList)
    {
        Set<Id> campaignIdSet = new Set<Id>();

        for (CampaignMember cm : campaignMemberList)
        {
            campaignIdSet.add(cm.CampaignId);
        }
        return campaignIdSet;
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Checks the Campaign if there are CampaignMember records to be updated
    Inputs: Set<Campaign>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    07-Sep-2016 ryan.i.r.limlingan  Initial version of function
    19-Sep-2016 ryan.i.r.limlingan  Added checking to prevent multiple firing of trigger;
  ----------------------------------------------------------------------------------------------- */
    public static void checkMembersFromCampaign(Set<Campaign> campaignSet)
    {
        // Check if Set has already been populated or not
        if (updatedCampaignSet == null)
        {
            updatedCampaignSet = new Set<Campaign>(campaignSet);
        } else {
            campaignSet.removeAll(updatedCampaignSet);
        }

        List<CampaignMember> campaignMemberList = [SELECT Id, CampaignId, LeadId FROM CampaignMember
                                                   WHERE CampaignId IN :campaignSet];
        if (campaignMemberList.size() > 0)
        {
            Database.executeBatch(new JVCO_CampaignMemberReassignmentBatch(campaignMemberList));
        }
    }

 /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Function that reassigns the Leads of a Campaign to the corresponding Queue
    Inputs: List<Campaign>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    06-Sep-2016 ryan.i.r.limlingan  Initial version of function
    07-Sep-2016 ryan.i.r.limlingan  Refactored some code to shorten number of lines
    14-Sep-2016 ryan.i.r.limlingan  Added Test.isRunningTest() for coverage;
                                    Placed most logic under a condition
  ----------------------------------------------------------------------------------------------- */
    public static void updateCampaignMemberOwner(List<CampaignMember> campaignMemberList)
    {
        // Stores the CampaignIds of CampaignMembers retrieved
        Set<Id> campaignIdSet = retrieveCampaignIds(campaignMemberList);

        if (!campaignIdSet.isEmpty())
        {
            // Mapping of corresponding Telesales Partner to every Campaign
            Map<Id, String> campaignIdPartnerMap = new Map<Id, String>();
            // Mapping of Leads to their Campaign
            Map<Id, Id> leadIdCampaignIdMap = new Map<Id, Id>();
            // Stores the LeadIds of CampaignMembers retrieved
            Set<Id> leadIdSet = new Set<Id>();
            // Call function to construct the map that will be referenced later on
            constructQueueNameIdMap();

            // Get Telesales Partner of every Campaign
            for (Campaign c : [SELECT Id, JVCO_Telesales_Partner__c
                               FROM Campaign WHERE Id IN :campaignIdSet])
            {
                campaignIdPartnerMap.put(c.Id, c.JVCO_Telesales_Partner__c);
            }

            // Get LeadId of every CampaignMember with CampaignId in campaignIdSet
            for (CampaignMember cm : campaignMemberList)
            {
                leadIdSet.add(cm.LeadId);
                leadIdCampaignIdMap.put(cm.LeadId, cm.CampaignId);
            }

            // Populate list of Lead records to be updated
            List<Lead> leadsToUpdateList = new List<Lead>();
            for (Lead l : [SELECT Id, OwnerId FROM Lead WHERE Id IN :leadIdSet])
            {
                // Reassign Lead to corresponding Telesales Partner Queue
                Id campaignId = leadIdCampaignIdMap.get(l.Id);
                String telesalesPartner = campaignIdPartnerMap.get(campaignId);
                l.OwnerId = queueNameIdMap.get(telesalesPartner);
                //l.LeadSource = telesalesPartner;
                leadsToUpdateList.add(l);
            }

            if (Test.isRunningTest() && shouldInvalidate)
            {
                Integer i = 0;
                for (Lead leadToInvalidate : leadsToUpdateList)
                {
                    // Only invalidate first 100 records
                    if (i++ == NUM_RECORDS_TO_FAIL) break;
                    // Force error by assigning a LeadId to OwnerId
                    leadToInvalidate.OwnerId = leadToInvalidate.Id;
                }
            }

            // Check if there are Lead records to be updated
            if (leadsToUpdateList.size() > 0)
            {
                List<Database.SaveResult> resultList = Database.update(leadsToUpdateList, FALSE);
                checkUpdateResults(resultList, leadsToUpdateList);
            }
        }
    }
    
/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Collates the records that have failed during the update 
    Inputs: List<Database.SaveResult>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    08-Sep-2016 ryan.i.r.limlingan  Initial version of function
    14-Sep-2016 ryan.i.r.limlingan  Removed checking of size of resultList
  ----------------------------------------------------------------------------------------------- */
    public static void checkUpdateResults(List<Database.SaveResult> resultList,
                                          List<Lead> updatedLeadsList)
    {
        String errMessage = '';
        for(Integer i = 0; i < updatedLeadsList.size(); i++){
            // Error was encountered during update
            if (!resultList.get(i).isSuccess())
            {
                errMessage = updatedLeadsList.get(i).Id + ': ';
                for (Database.Error err : resultList.get(i).getErrors())
                {
                    errMessage += err.getMessage() + '; ';
                }
                errMessage += '\n';
                errorsList.add(errMessage);
            }
         }
    }
}