/*----------------------------------------------------------------------------------------------
Name: JVCO_GenerateCustomerAccountReminders.cls 
Test class: JVCO_GenerateCustomerActRemindersTest.cls
Description: Aggregate Batch class that updates Custom Account Reminder Level
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
18-Oct-2019  0.1         patrick.t.bautista     Initial creation
29-Oct-2019  0.2         rhys.j.c.dela.cruz     GREEN-34955 - Added call for JVCO_ResetCustomerAccountReminder batch
----------------------------------------------------------------------------------------------- */
public class JVCO_GenerateCustomerAccountReminders implements Database.Batchable<AggregateResult>, Database.Stateful{
    
    //Retrieve Custom Settings for dunning
    final JVCO_Customer_Account_Dunning_Settings__c DUNNING_SETTINGS = JVCO_Customer_Account_Dunning_Settings__c.getOrgDefaults();
    
    public Iterable<AggregateResult> start(Database.BatchableContext BC)
    {
        //Get overdue on custom settings and if null set default as 2
        Integer overdueInt = DUNNING_SETTINGS.JVCO_Overdue__c == null ? 2 : Integer.valueOf(DUNNING_SETTINGS.JVCO_Overdue__c);
        //Date today - given custom settings integer
        Date JVCO_LAST_N_DAYS = Date.today() - overdueInt;
        //Store query on string
        string query = 'SELECT c2g__Account__r.JVCO_Customer_Account__r.id actId '
            +'FROM c2g__codaInvoice__c '
            +'WHERE c2g__DueDate__c <=: JVCO_LAST_N_DAYS '
            +'AND ffps_custRem__Exclude_From_Reminder_Process__c = FALSE '
            +'AND c2g__NetTotal__c > 0'
            +'AND c2g__PaymentStatus__c IN (\'Unpaid\', \'Part Paid\') '
            +'AND c2g__Account__r.JVCO_Customer_Account__r.JVCO_Customer_Account_Dunning__c = TRUE '
            +'AND c2g__Account__r.JVCO_Customer_Account__r.ffps_custRem__Exclude_From_Reminder_Process__c = FALSE '
            +'AND ((c2g__Account__r.JVCO_Customer_Account__r.ffps_custRem__Last_Reminder_Severity_Level__c < 5 '
            +'      AND c2g__Account__r.JVCO_Customer_Account__r.ffps_custRem__Reminders_Blocked_Until_Date__c <= TODAY) '
            +'     OR c2g__Account__r.JVCO_Customer_Account__r.ffps_custRem__Last_Reminder_Severity_Level__c =NULL '
            +' OR (c2g__Account__r.JVCO_Customer_Account__r.ffps_custRem__Last_Reminder_Severity_Level__c = 0 AND c2g__Account__r.JVCO_Customer_Account__r.ffps_custRem__Reminders_Blocked_Until_Date__c = null)) '
            +'GROUP BY c2g__Account__r.JVCO_Customer_Account__r.id';
        
        return new AggregateResultIterable(query, JVCO_LAST_N_DAYS);
    }
    public void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        Set<id> accountIDSet = new set<Id>();
        List<Account> accountListToUpdate = new List<Account>();
        //Loop Aggregate result and get account id
        for(sObject s : scope){
            if(!accountIDSet.contains((id)s.get('actId'))){
                accountIDSet.add((id)s.get('actId'));
            }
        }
        //Loop customer accounts and update last reminder level
        for(Account acct : [select id, ffps_custRem__Last_Reminder_Severity_Level__c 
                            from Account
                            where id IN: accountIDSet])
        {
            acct.ffps_custRem__Last_Reminder_Severity_Level__c = acct.ffps_custRem__Last_Reminder_Severity_Level__c == null ? 0 : acct.ffps_custRem__Last_Reminder_Severity_Level__c;
            acct.ffps_custRem__Last_Reminder_Severity_Level__c = acct.ffps_custRem__Last_Reminder_Severity_Level__c + 1;
            accountListToUpdate.add(acct);
        }
        update accountListToUpdate;
    }
    public void finish(Database.BatchableContext BC)
    {
        final Decimal batchSize = DUNNING_SETTINGS.JVCO_Batch_Size__c == NUll ? 20 : DUNNING_SETTINGS.JVCO_Batch_Size__c;
        JVCO_ResetCustomerAccountReminders resetCustAccBatch = new JVCO_ResetCustomerAccountReminders();
        Database.executeBatch(resetCustAccBatch, Integer.valueOf(batchSize));
    }
}