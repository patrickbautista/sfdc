/* ----------------------------------------------------------------------------------------------
    Name: JVCO_blngInvoiceLineHandler.cls 
    Description: Handler Class for Billing Invoice Line

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    06-Jul-2017   0.1         franz.g.a.dimaapi   Initial creation
    14-Sep-2017   0.2         franz.g.a.dimaapi   create a copy of dimension 2 formula field GREEN-21231
    05-11-2017    0.3         zara.h.ferjani      Changes made to fix GREEN-25146 issue 
    19-Dec-2017  0.3         chun.yin.tang        Add disabling trigger to Blng invoice handler
  ----------------------------------------------------------------------------------------------- */
public class JVCO_blngInvoiceLineHandler
{
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_blngInvoiceLineTrigger__c : false;
    
    public static void beforeInsert(List<blng__InvoiceLine__c> newBInvLineList)
    {   
        if(!JVCO_FFUtil.stopBlngInvoiceLineHandlerBeforeInsert && !skipTrigger)
        {
            JVCO_BillingLineUtil.populateVenueAndAffiliation(newBInvLineList);
            JVCO_BillingLineUtil.updateDimension2(newBInvLineList);   
            JVCO_BillingLineUtil.updateTrueUpFlag(newBInvLineList);
            JVCO_BillingLineUtil.populateBInvLineOrderProduct(newBInvLineList);
            JVCO_BillingLineUtil.populateTempBudgetCategory(newBInvLineList);
        }
    }
    public static void afterUpdate(List<blng__InvoiceLine__c> newList, Map<id,blng__InvoiceLine__c> oldMap)
    {
        if(!skipTrigger){
            List<JVCO_Invoice_Line_Paid_Amount_Tracker__c> invLinePATList = new List<JVCO_Invoice_Line_Paid_Amount_Tracker__c>();
            for(blng__InvoiceLine__c invLine : [SELECT id, JVCO_Paid_Amount__c, JVCO_Paid_Date__c,
                                                JVCO_Percentage_Paid__c, 
                                                blng__Invoice__r.JVCO_Week__c,
                                                blng__Invoice__r.JVCO_Distribution_Status__c, 
                                                JVCO_Dynamic_Paid_Amount__c,
                                                blng__Invoice__r.JVCO_Payment_Status__c,
                                                blng__Invoice__r.JVCO_Status_Change_Date__c,
                                                JVCO_Paid_Amount_Tracker__c,
                                                blng__TotalAmount__c,
                                                JVCO_Line_Tax_Amount__c
                                                FROM blng__InvoiceLine__c
                                                WHERE id IN: newList])
            {
                if(invLine.JVCO_Paid_Amount__c != oldMap.get(invLine.id).JVCO_Paid_Amount__c)
                {
                    JVCO_Invoice_Line_Paid_Amount_Tracker__c invPAT = insertInvoiceLineTrackerFields(invLine);
                    invLinePATList.add(invPAT);
                }
            }
            insert invLinePATList;
        }
    }
    public static JVCO_Invoice_Line_Paid_Amount_Tracker__c insertInvoiceLineTrackerFields(blng__InvoiceLine__c bInvLine)
    {
        JVCO_Invoice_Line_Paid_Amount_Tracker__c invPAT = new JVCO_Invoice_Line_Paid_Amount_Tracker__c();
        //invPAT.JVCO_Invoice_Line__c = bInvLine.id;
        invPAT.JVCO_Paid_Date__c = bInvLine.JVCO_Paid_Date__c;
        invPAT.JVCO_Percentage_Paid__c = bInvline.JVCO_Percentage_Paid__c;
        invPAT.JVCO_Week__c = bInvline.blng__Invoice__r.JVCO_Week__c;
        invPAT.JVCO_Distribution_Status__c = bInvline.blng__Invoice__r.JVCO_Distribution_Status__c;
        invPAT.JVCO_Dynamic_Paid_Amount__c = bInvLine.JVCO_Paid_Amount_Tracker__c;
        invPAT.JVCO_Paid_Amount__c = bInvLine.JVCO_Dynamic_Paid_Amount__c + bInvline.JVCO_Line_Tax_Amount__c;
        invPAT.JVCO_Total_Amount__c = bInvLine.blng__TotalAmount__c;
        invPAT.JVCO_Payment_Status__c = bInvLine.blng__Invoice__r.JVCO_Payment_Status__c;
        invPAT.JVCO_Status_Change_Date__c = bInvLine.blng__Invoice__r.JVCO_Status_Change_Date__c;
        return invPAT;
    }
}