@isTest
private class JVCO_SalesInvoiceHandlerTest {
    @testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        List<c2g__codaDimension2__c> dim2List = new List<c2g__codaDimension2__c>();
        c2g__codaDimension2__c pplDim2 = JVCO_TestClassHelper.setDim2('PPL');
        dim2List.add(pplDim2);
        c2g__codaDimension2__c prsDim2 = JVCO_TestClassHelper.setDim2('PRS');
        dim2List.add(prsDim2);
        c2g__codaDimension2__c vplDim2 = JVCO_TestClassHelper.setDim2('VPL');
        dim2List.add(vplDim2);
        insert dim2List;

        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();
        insert dim3;

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;

        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        List<Order> orderList = new List<Order>();
        orderList.add(o);

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_General_Settings__c GeneralSettings = new JVCO_General_Settings__c();
        GeneralSettings.JVCO_Post_Sales_Credit_Note_Scope__c = 1;
        GeneralSettings.JVCO_Disable_mismatch_parent__c = true;
        insert GeneralSettings;
        
        Test.startTest();
        JVCO_Invoice_Group__c invGrp = new JVCO_Invoice_Group__c();
        invGrp.JVCO_Status__c = 'Active';
        insert invGrp;
        
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        sInv.JVCO_Invoice_Group__c = invGrp.id;       
        insert sInv;
        
        invGrp.CC_SINs__c = sInv.Name + ',';
        update invGrp;
        
        List<c2g__codaInvoiceLineItem__c> sInvLineList = new List<c2g__codaInvoiceLineItem__c>();
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, pplDim2.Id, p.Id));
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, prsDim2.Id, p.Id));
        insert sInvLineList;
        
        c2g__codaInvoiceInstallmentLineItem__c invoiceInstalmentLine = new c2g__codaInvoiceInstallmentLineItem__c();
        invoiceInstalmentLine.c2g__Amount__c = 12;
        invoiceInstalmentLine.c2g__DueDate__c = Date.today();
        invoiceInstalmentLine.JVCO_Paid_Amount__c = 12;
        invoiceInstalmentLine.JVCO_Payment_Status__c = 'Unpaid';
        invoiceInstalmentLine.c2g__Invoice__c = sInv.id;
        insert invoiceInstalmentLine;
        
        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        bInv.JVCO_Sales_Invoice__c = sInv.Id;
        insert bInv;
        blng__InvoiceLine__c bInvLine1 = JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);
        blng__InvoiceLine__c bInvLine2 = JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);
        List<blng__InvoiceLine__c> bInvLineList = new List<blng__InvoiceLine__c>();
        bInvLineList.add(bInvLine1);
        bInvLineList.add(bInvLine2);
        insert bInvLineList;
        
        c2g__codaCreditNote__c cred = JVCO_TestClassHelper.getCreditNote(licAcc.Id);
        insert cred;
        List<c2g__codaCreditNoteLineItem__c> credLineList = new List<c2g__codaCreditNoteLineItem__c>();
        credLineList.add(JVCO_TestClassHelper.getCreditNoteLine(cred.Id, dim1.id, dim3.Id, taxCode.Id, pplDim2.Id, p.Id, 100));
        insert credLineList;
        
        Set<Id> sInvIdSet = new Set<Id>();
        Set<Id> creNoteSet = new Set<Id>();
        sInvIdSet.add(sInv.Id);
        creNoteSet.add(cred.Id);
        //Run Post Batch
        JVCO_PostSalesInvoiceBatch pSIB = new JVCO_PostSalesInvoiceBatch(sInvIdSet, creNoteSet);
        psIB.stopMatching = true;

        Database.executeBatch(pSIB);      
        Test.stopTest();
        
        Income_Direct_Debit__c incomeDD = new Income_Direct_Debit__c();
        incomeDD.Account__c = licAcc.Id;
        incomeDD.DD_Collection_Day__c = '1';
        incomeDD.DD_Collection_Period__c = 'Monthly';
        incomeDD.DD_Collection_Stretch__c = '1';
        incomeDD.DD_Collection_Type__c = 'Fixed';
        incomeDD.DD_Collection_Reference__c = 'Monthly Subscription';
        incomeDD.DD_Status__c = 'New Instruction';
        incomeDD.DD_Bank_Account_Number__c = '12345678';
        incomeDD.DD_Bank_Sort_Code__c = '123456';
        incomeDD.Number_Of_Payments__c = 2;
        insert incomeDD;
        
        SMP_DirectDebit_GroupInvoice__c IDDInvGroup = new SMP_DirectDebit_GroupInvoice__c();
        IDDInvGroup.Income_Direct_Debit__c = incomeDD.Id;
        IDDInvGroup.Invoice_Group__c = invGrp.Id;
        insert IDDInvGroup;
        
        Income_Debit_History__c incomeDDHistory = new Income_Debit_History__c();
        incomeDDHistory.Amount__c = 100;
        incomeDDHistory.DD_Status__c = 'First Collection';
        incomeDDHistory.Income_Direct_Debit__c = incomeDD.Id;
        incomeDDHistory.DD_Stage__c = 'Submitted';
        incomeDDHistory.DD_Collection_Date__c = Date.today();
        insert incomeDDHistory;
    }
    @isTest
    static void testAfterUpdate()
    {
        Account testAccount = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c != null LIMIT 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];
        c2g__codaPeriod__c testPeriod  = [select id from c2g__codaPeriod__c limit 1];
        c2g__codaAccountingCurrency__c accCurrency  = [select id from c2g__codaAccountingCurrency__c limit 1];
        
        c2g__codaInvoice__c sInv = new c2g__codaInvoice__c();
        sInv.c2g__InvoiceCurrency__c = accCurrency.Id;
        sInv.c2g__InvoiceDate__c = date.today();
        sInv.c2g__DueDate__c = date.today().addMonths(7);
        sInv.c2g__Account__c = testAccount.id;
        sInv.JVCO_Customer_Type__c = 'New Business';
        sInv.c2g__OwnerCompany__c = testCompany.id;
        sInv.JVCO_Generate_Payment_Schedule__c = false;
        sInv.c2g__Period__c = testPeriod.id;
        insert sInv;
        
        c2g__codaInvoice__c invoice = [select id,JVCO_Original_Invoice__c, JVCO_Invoice_Legacy_Number__c,
                                     JVCO_Invoice_Type__c,JVCO_Reference_Document__c,
                                     JVCO_Generate_Payment_Schedule__c,JVCO_Number_of_Payments__c,
                                     JVCO_Payment_Method__c,JVCO_First_Due_Date__c
                                     from c2g__codaInvoice__c limit 1];
        invoice.JVCO_Original_Invoice__c = sInv.Id;
        invoice.JVCO_Invoice_Legacy_Number__c = 'PRS:421892180';
        invoice.JVCO_Invoice_Type__c = 'Surcharge';
        invoice.JVCO_Invoice_Group__c = null;
        update invoice;
    }
    @isTest
    static void testRestrictNotEqualParents()
    {
        c2g__codaCreditNote__c cred = [select id, Name from c2g__codaCreditNote__c Limit 1];
        c2g__codaInvoice__c invoice = [select id, JVCO_Reference_Document__c from c2g__codaInvoice__c limit 1];
        
        try {
            invoice.JVCO_Reference_Document__c = cred.Name;
            update invoice;
        } catch (Exception e) {
            // assert
        }
    }
}