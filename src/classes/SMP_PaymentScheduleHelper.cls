public with sharing class SMP_PaymentScheduleHelper {

    public static List<Income_Direct_Debit__c> checkDirectDebit(List<Income_Direct_Debit__c> directDebits){
        Set<Id> ddIds = new Set<Id>();

        for(Income_Direct_Debit__c dd : directDebits){
            ddIds.add(dd.Id);
        }

        List<Income_Direct_Debit__c> directDebitList = [SELECT Id, Name, DD_Last_Collected_Date__c, DD_Ongoing_Collection_Amount__c,
                                                            DD_Account_Number_OK__c, DD_Sort_Code_OK__c, DD_Bank_Name__c, DD_Branch_Name__c,
                                                            DD_Bank_Account_Name__c, DD_Bank_Account_Number__c, DD_Bank_Sort_Code__c, DD_Status__c,
                                                            DD_Exception__c, Auto_Expire__c, (SELECT Id, Invoice_Group__c
                                                            FROM Direct_Debit_Group_Invoice_s__r) 
                                                            FROM Income_Direct_Debit__c WHERE Id IN :ddIds];
        system.debug('#### directDebitList '+directDebitList);

        List<SMP_DirectDebit_GroupInvoice__c> ddGroupInvoiceList = new List<SMP_DirectDebit_GroupInvoice__c>();
        ddGroupInvoiceList = [SELECT Id, Invoice_Group__c, Income_Direct_Debit__c FROM SMP_DirectDebit_GroupInvoice__c WHERE Income_Direct_Debit__c IN: ddIds];
 
        Set<Id> igSet = new Set<Id>();
        for(SMP_DirectDebit_GroupInvoice__c idd: ddGroupInvoiceList){
            igSet.add(idd.Invoice_Group__c);
        }


        List<c2g__codaInvoiceInstallmentLineItem__c> installmentLineItemList = [SELECT Id, c2g__Invoice__r.JVCO_Invoice_Group__c, c2g__Amount__c,
                                                                                    c2g__DueDate__c 
                                                                                    FROM c2g__codaInvoiceInstallmentLineItem__c 
                                                                                    WHERE c2g__DueDate__c > :Date.Today() AND JVCO_Payment_Status__c = 'Unpaid'
                                                                                    AND c2g__Invoice__r.JVCO_Invoice_Group__c IN :igSet
                                                                                    ORDER BY c2g__DueDate__c ASC];
        system.debug('#### installmentLineItemList '+installmentLineItemList);

        List<ddWrapper> ddWrapperList = new List<ddWrapper>();
        for(Income_Direct_Debit__c dd :directDebitList){
            ddWrapper wrapper = new ddWrapper();
            wrapper.directDebit = dd;
            List<c2g__codaInvoiceInstallmentLineItem__c> iliList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
            for(SMP_DirectDebit_GroupInvoice__c ddInvoice :dd.Direct_Debit_Group_Invoice_s__r){
                for(c2g__codaInvoiceInstallmentLineItem__c installmentLineItem : installmentLineItemList){
                    if(ddInvoice.Invoice_Group__c == installmentLineItem.c2g__Invoice__r.JVCO_Invoice_Group__c){
                        iliList.add(installmentLineItem);
                    }
                }
            }
            wrapper.installmentLineItemList = iliList;
            ddWrapperList.add(wrapper);
        }

        List<Income_Direct_Debit__c> ddtoUpdateList = new List<Income_Direct_Debit__c>();

        for(ddWrapper wrapper :ddWrapperList){
            ddtoUpdateList.add(SMP_PaymentScheduleHelper.checkDirectDebit(wrapper.directDebit, wrapper.installmentLineItemList));
        }

        return ddtoUpdateList;
    }

    public static Income_Direct_Debit__c checkDirectDebit(Income_Direct_Debit__c dd, List<c2g__codaInvoiceInstallmentLineItem__c> iliList){
        
        if(iliList.Size() == 0){
            dd.DD_Ongoing_Collection_Amount__c = 0;
            dd.DD_Next_Collection_Date__c = null;
            if(dd.DD_Status__c != 'Cancelled'){
                dd.DD_Status__c = 'On Hold';
            }
            if(dd.Auto_Expire__c == true)
            {
                dd.DD_Status__c = 'Expired';
            }

            dd.Number_Of_Payments_Left__c = 0;
        }
        else{
            if(dd.DD_Status__c == 'New Instruction' || dd.DD_Status__c == 'First Collection')
            {
                if(dd.DD_Exception__c == false && dd.DD_Status__c == 'New Instruction'){
                    Decimal firstCollectionAmount = 0;
                    for(c2g__codaInvoiceInstallmentLineItem__c ili :iliList){
                        firstCollectionAmount += ili.c2g__Amount__c;
                    }
                    dd.DD_First_Collection_Date__c = iliList[0].c2g__DueDate__c;
                    dd.DD_First_Collection_Amount__c = firstCollectionAmount;
                    //dd.Send_DD_Documentation__c = 'Direct Debit Confirmation';
                }
            }

            Decimal ongoingCollectionAmount = 0;
            Integer paymentsLeft = 0;
            Date dateSort;
            for(c2g__codaInvoiceInstallmentLineItem__c ili :iliList){
                if(dateSort == null || dateSort != ili.c2g__DueDate__c){
                    paymentsLeft ++;
                    dateSort = ili.c2g__DueDate__c;
                }
                ongoingCollectionAmount += ili.c2g__Amount__c;
            }
            dd.DD_Ongoing_Collection_Amount__c = ongoingCollectionAmount;
            dd.DD_Next_Collection_Date__c = iliList[0].c2g__DueDate__c;
            dd.Number_Of_Payments_Left__c = paymentsLeft;
            dd.DD_Last_Collected_Date__c = date.today();
            System.debug('nextCollection####' + iliList[0].c2g__DueDate__c);
        }

        return dd;
    }

    public static List<TriggerHandler_CardPaymentHistory.CardPaymentSalesInvoiceWrapper> checkCardPaymentHistory(List<TriggerHandler_CardPaymentHistory.CardPaymentSalesInvoiceWrapper> wrapperList){
        List<Id> salesInvoiceIdList = new List<Id>();
        for(TriggerHandler_CardPaymentHistory.CardPaymentSalesInvoiceWrapper wrapper :wrapperList){
            salesInvoiceIdList.addAll(wrapper.salesInvoiceIdList);
        }


        List<c2g__codaInvoiceInstallmentLineItem__c> installmentLineItemList = [SELECT Id, c2g__Invoice__r.JVCO_Invoice_Group__c, c2g__Amount__c,
                                                                                    c2g__DueDate__c, c2g__Invoice__c 
                                                                                    FROM c2g__codaInvoiceInstallmentLineItem__c 
                                                                                    WHERE c2g__DueDate__c > :Date.Today() AND JVCO_Payment_Status__c = 'Unpaid'
                                                                                    AND c2g__Invoice__c IN :salesInvoiceIdList
                                                                                    ORDER BY c2g__DueDate__c ASC];
        system.debug('#### installmentLineItemList '+installmentLineItemList);
        for(TriggerHandler_CardPaymentHistory.CardPaymentSalesInvoiceWrapper wrapper: wrapperList){
            List<c2g__codaInvoiceInstallmentLineItem__c> lineItemList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
            Boolean firstItem = false;
            Decimal amount = 0;
            for(c2g__codaInvoiceInstallmentLineItem__c ili : installmentLineItemList){
                if(wrapper.salesInvoiceIdList.contains(ili.c2g__Invoice__c)){
                    lineItemList.add(ili);
                    if(!firstItem){
                        wrapper.cardPayment.RP_Next_Payment_Date__c = ili.c2g__DueDate__c;
                        amount += ili.c2g__Amount__c;
                    }
                    firstItem = true;
                }
            }
            wrapper.installmentLineItemList = lineItemList;
            //wrapper.cardPayment.RP_Amount__c = amount;

            if(amount == 0){
                wrapper.cardPayment.RP_Amount__c = 0;
                wrapper.cardPayment.RP_Enabled__c = false;
                wrapper.cardPayment.RP_Next_Payment_Date__c = null;
            }
        }
        
        return wrapperList;
    }

    public static List<c2g__codaInvoiceInstallmentLineItem__c> processSuccessfulDDHistories(Income_Debit_History__c directDebitHistory){
        List<c2g__codaInvoiceInstallmentLineItem__c> installmentLineItemList = getInstallmentLineItemsForHistory(directDebitHistory); 

        for(c2g__codaInvoiceInstallmentLineItem__c installmentLineItem :installmentLineItemList){
            installmentLineItem.JVCO_Payment_Status__c = 'Paid';
        }

        return installmentLineItemList;
    }

    public static List<c2g__codaInvoiceInstallmentLineItem__c> processFailedDDHistories(Income_Debit_History__c directDebitHistory){
        List<c2g__codaInvoiceInstallmentLineItem__c> installmentLineItemList = getInstallmentLineItemsForHistory(directDebitHistory); 

        for(c2g__codaInvoiceInstallmentLineItem__c installmentLineItem :installmentLineItemList){
            installmentLineItem.JVCO_Payment_Status__c = 'Unpaid';
        }

        return installmentLineItemList;
    }

    public static List<c2g__codaInvoiceInstallmentLineItem__c> getInstallmentLineItemsForHistory(Income_Debit_History__c directDebitHistory){
        List<Income_Direct_Debit__c> directDebitList = [SELECT Id, Name, DD_Last_Collected_Date__c, DD_Ongoing_Collection_Amount__c,
                                                            DD_Account_Number_OK__c, DD_Sort_Code_OK__c, DD_Bank_Name__c, DD_Branch_Name__c,
                                                            DD_Bank_Account_Name__c, DD_Bank_Account_Number__c, DD_Bank_Sort_Code__c,
                                                            (SELECT Id, Invoice_Group__c
                                                            FROM Direct_Debit_Group_Invoice_s__r LIMIT 1) 
                                                            FROM Income_Direct_Debit__c WHERE Id IN (SELECT Income_Direct_Debit__c 
                                                            FROM Income_Debit_History__c)];
                                                            
        List<SMP_DirectDebit_GroupInvoice__c> ddGroupInvoiceList = new List<SMP_DirectDebit_GroupInvoice__c>();
        ddGroupInvoiceList = [SELECT Id, Invoice_Group__c, Income_Direct_Debit__c FROM SMP_DirectDebit_GroupInvoice__c WHERE Income_Direct_Debit__c =: directDebitHistory.Income_Direct_Debit__c];
 
        Set<Id> igSet = new Set<Id>();
        for(SMP_DirectDebit_GroupInvoice__c idd: ddGroupInvoiceList){
            igSet.add(idd.Invoice_Group__c);
        }
        List<c2g__codaInvoiceInstallmentLineItem__c> installmentLineItemList = [SELECT Id, c2g__Invoice__r.JVCO_Invoice_Group__c, c2g__Amount__c,
                                                                                    c2g__DueDate__c, c2g__Invoice__c 
                                                                                    FROM c2g__codaInvoiceInstallmentLineItem__c 
                                                                                    WHERE c2g__DueDate__c > :Date.Today() AND JVCO_Payment_Status__c = 'Unpaid'
                                                                                    AND c2g__Invoice__r.JVCO_Invoice_Group__c IN :igSet
                                                                                    ORDER BY c2g__DueDate__c ASC];
        return installmentLineItemList;
    }

    public class ddWrapper{
        public Income_Direct_Debit__c directDebit {get; set;}
        public List<c2g__codaInvoiceInstallmentLineItem__c> installmentLineItemList {get; set;}
    }
}