/* ----------------------------------------------------------------------------------------------
   Name: JVCO_caseWriteOffLogic.cls 
   Description: Handler class for Case object

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  24-Feb-2017   0.1         franz.g.a.dimaapi   Intial creation
  27-Feb-2017   0.2         franz.g.a.dimaapi   Added Implementation for CreditNote and CashEntryLineItem
  29-Nov-2017   0.3         jason.e.mactal      Modified updateCaseTotalWriteOffFields() method - GREEN-26137
  05-Dec-2017   0.4         michael.alamag      Add additional condition to the query in updateCashEntryLineItemCaseId Method - GREEN-26606
  10-Dec-2017   0.5         franz.g.a.dimaapi   Refactor Class, removed uneccessary logic and variables
  09-Aug-2018   0.6         patrick.t.bautista  GREEN-32999 Updated Calcuation of Paid amount and get all the decimal places
  ----------------------------------------------------------------------------------------------- */
public with sharing class JVCO_caseAccountWriteOffLogic
{   
    private static Set<Id> ownerIdSet = new Set<Id>();
    private static Map<Id, Id> newAccountMap = new Map<Id, Id>();
    private static Set<Id> caseOwnerIdSet = new Set<Id>();

    public static void updateCaseId(List<Case> caseList)
    {
        //Setup AccountMapping and OwnerIdSet
        for(Case c : caseList)
        {
            newAccountMap.put(c.AccountId, c.id);
            caseOwnerIdSet.add(c.OwnerId);
        }
        //Setup Queue Owner Id
        for(GroupMember gm : [SELECT UserOrGroupId, GroupId 
                                FROM GroupMember 
                                WHERE UserOrGroupId IN :caseOwnerIdSet])
        {
            ownerIdSet.add(gm.GroupId);
        }
        updateSalesInvoiceCaseId(caseList);
        updateCreditNoteCaseId(caseList);
        updateCashEntryLineItemCaseId(caseList);
    }

    private static void updateSalesInvoiceCaseId(List<Case> caseList)
    {   
        List<c2g__codaInvoice__c> updatedInvoice = new List<c2g__codaInvoice__c>();
        for(c2g__codaInvoice__c invoice : [SELECT id, c2g__Account__c, c2g__Account__r.JVCO_Customer_Account__c, JVCO_Case_Id__c, 
                                            JVCO_Case_Id__r.Id, c2g__Account__r.JVCO_Customer_Account__r.Id, c2g__PaymentStatus__c
                                            FROM c2g__codaInvoice__c 
                                            WHERE c2g__Account__r.JVCO_Customer_Account__r.Id in :newAccountMap.keySet()
                                            // GREEN-14150 - 05/04/2017 - Kristoffer.d.martin - START
                                            AND c2g__PaymentStatus__c IN ('Unpaid','Part Paid')
                                            AND OwnerId IN :ownerIdSet])
        {
            if(invoice.c2g__Account__r.JVCO_Customer_Account__c!=null)
            {   
                invoice.JVCO_Case_Id__c = newAccountMap.get(invoice.c2g__Account__r.JVCO_Customer_Account__r.Id);
                updatedInvoice.add(invoice);
            }
        }

        if(!updatedInvoice.isEmpty())
        {   
            update updatedInvoice;
        }                                

    }

    private static void updateCreditNoteCaseId(List<Case> caseList)
    {
        List<c2g__codaCreditNote__c> updatedCreditNote = new List<c2g__codaCreditNote__c>();
        for(c2g__codaCreditNote__c creditNote : [SELECT id, c2g__Account__c, c2g__Account__r.JVCO_Customer_Account__c, 
                                                JVCO_Case_Id__c, JVCO_Case_Id__r.Id, c2g__Account__r.JVCO_Customer_Account__r.Id, c2g__PaymentStatus__c
                                                FROM c2g__codaCreditNote__c where c2g__Account__r.JVCO_Customer_Account__r.Id in :newAccountMap.keySet()
                                                // GREEN-21796 - 08/15/2017 - patrick.t.bautista - START
                                                AND c2g__PaymentStatus__c IN ('Unpaid','Part Paid')
                                                // GREEN-21796 - 08/15/2017 - patrick.t.bautista - END
                                                AND OwnerId IN :ownerIdSet])
        {
            if(creditNote.c2g__Account__r.JVCO_Customer_Account__c!=null)
            {   
                creditNote.JVCO_Case_Id__c = newAccountMap.get(creditNote.c2g__Account__r.JVCO_Customer_Account__r.Id);
                updatedCreditNote.add(creditNote);
            }
        }

        if(!updatedCreditNote.isEmpty())
        {   
            update updatedCreditNote;
        }            

    }

    private static void updateCashEntryLineItemCaseId(List<Case> caseList)
    {                          
        List<c2g__codaCashEntryLineItem__c> updatedCashEntryLine = new List<c2g__codaCashEntryLineItem__c>();
        for(c2g__codaCashEntryLineItem__c cashEntryLine : [SELECT id, c2g__Account__c, c2g__Account__r.JVCO_Customer_Account__c, 
                                                            JVCO_Case_Id__c, JVCO_Case_Id__r.Id, c2g__Account__r.JVCO_Customer_Account__r.Id, c2g__CashEntry__r.OwnerId
                                                            FROM c2g__codaCashEntryLineItem__c 
                                                            WHERE c2g__Account__r.JVCO_Customer_Account__r.Id IN :newAccountMap.keySet()
                                                            AND c2g__CashEntry__r.OwnerId IN :ownerIdSet
                                                            AND c2g__CashEntry__r.c2g__Status__c != 'In Progress'])
        {
            if(cashEntryLine.c2g__Account__r.JVCO_Customer_Account__c!=null)
            {   
                cashEntryLine.JVCO_Case_Id__c = newAccountMap.get(cashEntryLine.c2g__Account__r.JVCO_Customer_Account__r.Id);
                updatedCashEntryLine.add(cashEntryLine);
            }
        }

        if(!updatedCashEntryLine.isEmpty())
        {   
            //Add a try clause to Avoid CashEntries Issue
            try{
                update updatedCashEntryLine;                
            }catch(Exception e){
                System.debug('@CashEntry Insert Failed'+e.getMessage() + e.getCause());
            }        
        }                       
    }
    
    public static void updateCaseTotalWriteOffFields(Map<Id, Case> newCaseMap)
    {

        for(Case newCase : newCaseMap.values())
        {
            newCase.JVCO_Total_PPL_Write_Off_Sales_Invoice__c = 0;
            newCase.JVCO_Total_PRS_Write_Off_Sales_Invoice__c = 0;
            newCase.JVCO_Total_VPL_Write_Off_Sales_Invoice__c = 0;
            newCase.JVCO_Total_PRS_Write_Off_Credit_Notes__c = 0;
            newCase.JVCO_Total_PPL_Write_Off_Credit_Notes__c = 0;
            newCase.JVCO_Total_VPL_Write_Off_Credit_Notes__c = 0;
            newCase.JVCO_Cash_Entry_Write_Off__c = 0;
            newCase.JVCO_Sales_Invoice_VAT_total__c = 0;//GREEN-26137
            newCase.JVCO_Credit_Note_VAT_total__c = 0;//GREEN-26137
        }

        // Update Sales Invoice
        for(c2g__codaInvoice__c invoice : [SELECT id, name, JVCO_TotalPRSWriteOff__c, c2g__OutstandingValue__c,
                                            JVCO_TotalPPLWriteOff__c, JVCO_TotalVPLWriteOff__c, c2g__InvoiceTotal__c,
                                            c2g__InvoiceStatus__c, JVCO_Case_ID__c, JVCO_Total_Payment_Received__c,
                                            (SELECT id, name, JVCO_PaidAmount__c, c2g__TaxValue1__c,
                                                c2g__Dimension2__r.Name, c2g__NetValue__c
                                                FROM c2g__InvoiceLineItems__r
                                            WHERE c2g__Dimension2__r.Name IN ('PRS','PPL','VPL'))
                                            FROM c2g__codaInvoice__c 
                                            WHERE c2g__InvoiceStatus__c = 'Complete' 
                                            AND JVCO_Case_ID__c in :newCaseMap.keySet()])
        {
            Double PaidAmount = 0.00;
            for(c2g__codaInvoiceLineItem__c invoiceLine : invoice.c2g__InvoiceLineItems__r)
            {
                if(invoiceLine.c2g__Dimension2__r.Name == 'PPL')
                {
                    PaidAmount = invoice.JVCO_Total_Payment_Received__c !=0 ? ((invoice.JVCO_Total_Payment_Received__c / invoice.c2g__InvoiceTotal__c) * invoiceLine.c2g__NetValue__c) : 0;

                    newCaseMap.get(invoice.JVCO_Case_ID__c).JVCO_Total_PPL_Write_Off_Sales_Invoice__c += 
                    invoiceLine.c2g__NetValue__c - PaidAmount;//GREEN-26137
                    newCaseMap.get(invoice.JVCO_Case_ID__c).JVCO_Sales_Invoice_VAT_total__c +=
                    (invoice.c2g__OutstandingValue__c*invoiceLine.c2g__TaxValue1__c)/invoice.c2g__InvoiceTotal__c;//GREEN-26137


                }
                else if(invoiceLine.c2g__Dimension2__r.Name =='PRS')
                {
                    PaidAmount = invoice.JVCO_Total_Payment_Received__c !=0 ? ((invoice.JVCO_Total_Payment_Received__c / invoice.c2g__InvoiceTotal__c) * invoiceLine.c2g__NetValue__c) : 0;

                    newCaseMap.get(invoice.JVCO_Case_ID__c).JVCO_Total_PRS_Write_Off_Sales_Invoice__c += 
                    invoiceLine.c2g__NetValue__c - PaidAmount;//GREEN-26137
                    newCaseMap.get(invoice.JVCO_Case_ID__c).JVCO_Sales_Invoice_VAT_total__c +=
                    (invoice.c2g__OutstandingValue__c*invoiceLine.c2g__TaxValue1__c)/invoice.c2g__InvoiceTotal__c;//GREEN-26137
                }
                else 
                {
                    PaidAmount = invoice.JVCO_Total_Payment_Received__c !=0 ? ((invoice.JVCO_Total_Payment_Received__c / invoice.c2g__InvoiceTotal__c) * invoiceLine.c2g__NetValue__c) : 0;

                    newCaseMap.get(invoice.JVCO_Case_ID__c).JVCO_Total_VPL_Write_Off_Sales_Invoice__c += 
                    invoiceLine.c2g__NetValue__c - PaidAmount;//GREEN-26137
                    newCaseMap.get(invoice.JVCO_Case_ID__c).JVCO_Sales_Invoice_VAT_total__c += 
                    (invoice.c2g__OutstandingValue__c*invoiceLine.c2g__TaxValue1__c)/invoice.c2g__InvoiceTotal__c;//GREEN-26137
                }
            }
        }

        // Update Credit Note
        for(c2g__codaCreditNote__c creditNote : [SELECT id, name, JVCO_TotalPRSWriteOff__c, c2g__OutstandingValue__c, 
                                                JVCO_TotalPPLWriteOff__c, JVCO_TotalVPLWriteOff__c, c2g__CreditNoteTotal__c,
                                                c2g__CreditNoteStatus__c, JVCO_Case_ID__c, 
                                                (SELECT id, name, JVCO_Outstanding_Line_Value__c, c2g__TaxValue1__c,
                                                    c2g__Dimension2__r.Name, c2g__NetValue__c
                                                    FROM c2g__CreditNoteLineItems__r
                                                WHERE c2g__Dimension2__r.Name IN ('PRS','PPL','VPL'))
                                                FROM c2g__codaCreditNote__c 
                                                WHERE c2g__CreditNoteStatus__c = 'Complete' 
                                                AND JVCO_Case_ID__c in :newCaseMap.keySet()])
        {
            for(c2g__codaCreditNoteLineItem__c creditNoteLine : creditNote.c2g__CreditNoteLineItems__r)
            {   
                 if(creditNoteLine.c2g__Dimension2__r.Name == 'PPL')
                {
                    newCaseMap.get(creditNote.JVCO_Case_ID__c).JVCO_Total_PPL_Write_Off_Credit_Notes__c += 
                    creditNoteLine.JVCO_Outstanding_Line_Value__c;//GREEN-26137
                    newCaseMap.get(creditNote.JVCO_Case_ID__c).JVCO_Credit_Note_VAT_total__c += 
                    (-creditNote.c2g__OutstandingValue__c*creditNoteLine.c2g__TaxValue1__c)/creditNote.c2g__CreditNoteTotal__c;//GREEN-26137
                }
                else if(creditNoteLine.c2g__Dimension2__r.Name =='PRS')
                {
                    newCaseMap.get(creditNote.JVCO_Case_ID__c).JVCO_Total_PRS_Write_Off_Credit_Notes__c += 
                    creditNoteLine.JVCO_Outstanding_Line_Value__c;//GREEN-26137
                    newCaseMap.get(creditNote.JVCO_Case_ID__c).JVCO_Credit_Note_VAT_total__c += 
                    (-creditNote.c2g__OutstandingValue__c*creditNoteLine.c2g__TaxValue1__c)/creditNote.c2g__CreditNoteTotal__c;//GREEN-26137
                }
                else 
                {
                    newCaseMap.get(creditNote.JVCO_Case_ID__c).JVCO_Total_VPL_Write_Off_Credit_Notes__c += 
                    creditNoteLine.JVCO_Outstanding_Line_Value__c;//GREEN-26137
                    newCaseMap.get(creditNote.JVCO_Case_ID__c).JVCO_Credit_Note_VAT_total__c += 
                    (-creditNote.c2g__OutstandingValue__c*creditNoteLine.c2g__TaxValue1__c)/creditNote.c2g__CreditNoteTotal__c;//GREEN-26137
                }
            }
        }

        // Update Cash Entry Line
        for(c2g__codaCashEntryLineItem__c cashEntryLineItem : [SELECT id, name, c2g__CashEntry__r.c2g__Status__c, 
                                                                c2g__CashEntryValue__c, JVCO_Case_ID__c
                                                                FROM c2g__codaCashEntryLineItem__c
                                                                WHERE c2g__CashEntry__r.c2g__Status__c = 'Complete'
                                                                AND JVCO_Case_ID__c IN :newCaseMap.keySet()])
        {
            newCaseMap.get(cashEntryLineItem.JVCO_Case_ID__c).JVCO_Cash_Entry_Write_Off__c += cashEntryLineItem.c2g__CashEntryValue__c;
        }
    }

}