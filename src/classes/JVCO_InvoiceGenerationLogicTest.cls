@isTest
private class JVCO_InvoiceGenerationLogicTest
{
    /*@testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        RenewalMonthLimit__c renewalCustomsetting = new RenewalMonthLimit__c();
        renewalCustomsetting.Amount__c = 3;
        insert renewalCustomsetting;
        
        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        Test.startTest();
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        Test.stopTest();
        
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        o.JVCO_Processed_By_BillNow__c = true;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        List<Order> orderList = new List<Order>();
        orderList.add(o);
        
        JVCO_TestClassHelper.setGeneralSettingsCS();
        
        List<JVCO_TransitionManualReviewNotifSettings__c> settings = new List<JVCO_TransitionManualReviewNotifSettings__c>();
        settings.add(new JVCO_TransitionManualReviewNotifSettings__c(Name = 'Limit', Limit__c = '3000'));
        insert settings;
        
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        insert bInv;
        blng__InvoiceLine__c bInvLine = JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id,0, false);
        bInvLine.JVCO_Surcharge_Applicable__c = true;
        insert bInvLine;
        Database.executeBatch(new JVCO_OrderMultipleBillNowBatch(orderList));
        
    }

    @isTest
    static void testSurchargeGenerationBatch()
    {
        List<c2g__codaInvoice__c> sInvList = [SELECT id, name, c2g__PaymentStatus__c, 
                                             JVCO_Protected__c, JVCO_Cancelled__c, ffps_custRem__In_Dispute__c, 
                                             JVCO_Stop_Surcharge__c, JVCO_Surcharge_Generation_Date__c,
                                             JVCO_Promise_to_Pay__c, JVCO_Exempt_from_Surcharge__c,
                                             c2g__Account__r.JVCO_In_Infringement__c, JVCO_Surcharge_Generated__c,
                                             c2g__Account__r.JVCO_In_Enforcement__c, JVCO_Invoice_Type__c, 
                                             JVCO_Surcharge_Tariffs__c 
                                             FROM c2g__codaInvoice__c];
        JVCO_TestClassHelper.setSurchargeCS();
        JVCO_SurchargeGeneration_Batch sb = new JVCO_SurchargeGeneration_Batch();
        Test.startTest();
        Database.QueryLocator ql = sb.start(null);
        sb.execute(null, sInvList);
        sb.finish(null);
        Test.stopTest();
    }

    
    @isTest
    static void testSingleSurchargeGenerationBatch()
    {
        delete [SELECT ID FROM JVCO_Order_Group__c];
        List<c2g__codaInvoice__c> sInvList = [SELECT id, name, c2g__PaymentStatus__c, 
                                             JVCO_Protected__c, JVCO_Cancelled__c, ffps_custRem__In_Dispute__c, 
                                             JVCO_Stop_Surcharge__c, JVCO_Surcharge_Generation_Date__c,
                                             JVCO_Promise_to_Pay__c, JVCO_Exempt_from_Surcharge__c,
                                             c2g__Account__r.JVCO_In_Infringement__c, JVCO_Surcharge_Generated__c,
                                             c2g__Account__r.JVCO_In_Enforcement__c, JVCO_Invoice_Type__c, 
                                             JVCO_Surcharge_Tariffs__c 
                                             FROM c2g__codaInvoice__c];
        JVCO_TestClassHelper.setSurchargeCS();
        JVCO_SurchargeGeneration_Batch sb = new JVCO_SurchargeGeneration_Batch(sInvList[0].Id);
        Test.startTest();
        Database.QueryLocator ql = sb.start(null);
        sb.execute(null, sInvList);
        sb.finish(null);
        Test.stopTest();
    }

    @isTest
    static void testCreateInvoiceFromDataMig()
    {
        List<blng__Invoice__c> bInvList = [SELECT Id, JVCO_Invoice_Legacy_Number__c FROM blng__Invoice__c];
        bInvList[0].JVCO_Invoice_Legacy_Number__c = 'PPL:INV:9999999';
        update bInvList;
        Test.startTest();
        JVCO_InvoiceGenerationLogic.generateInvoiceFromDatamig(bInvList);
        Test.stopTest();
    }
    
    @isTest
    static void testSingleOrderInvoice()
    {
        List<blng__Invoice__c> bInvList = [SELECT Id, blng__Account__c FROM blng__Invoice__c];
        
        Income_Direct_Debit__c incomeDD = new Income_Direct_Debit__c();
        incomeDD.Account__c = bInvList[0].blng__Account__c;
        incomeDD.DD_Collection_Day__c = '1';
        incomeDD.DD_Collection_Period__c = 'Monthly';
        incomeDD.DD_Collection_Stretch__c = '1';
        incomeDD.DD_Collection_Type__c = 'Fixed';
        incomeDD.DD_Collection_Reference__c = 'Monthly Subscription';
        incomeDD.DD_Status__c = 'New Instruction';
        incomeDD.DD_Bank_Account_Number__c = '12345678';
        incomeDD.DD_Bank_Sort_Code__c = '123456';
        incomeDD.Number_Of_Payments__c = 2;
        insert incomeDD;
        
        String responseBody = '{"ProcessListNewDDFirstCollectionDateResult":[{"Error":"","FirstCollectionDate":"11-05-2017"},{"Error":"","FirstCollectionDate":"11-05-2017"}]}';
        HttpRestCallOutMock fakeResponse = new HttpRestCallOutMock(200, responseBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        Test.startTest();
        JVCO_InvoiceGenerationLogic.generateInvoiceFromSingleOrder(bInvList);
        Test.stopTest();
    }

    @isTest
    static void testMigratedEventsBatch()
    {
        JVCO_MigratedEvents_Batch mb = new JVCO_MigratedEvents_Batch();
        Test.startTest();
        Database.QueryLocator ql = mb.start(null);
        mb.execute(null, new List<blng__Invoice__c>());
        mb.finish(null);
        Test.stopTest();
    }

    @isTest
    static void testErrorLogUtil()
    {
        JVCO_ErrorUtil.logError(UserInfo.getUserId(), 'hotdog', 'hamburger', 'Invoice Scheduler');
        List<JVCO_Order_Group__c> orderGroupList = [SELECT Id FROM JVCO_Order_Group__c];
        JVCO_ErrorUtil errorUtil = new JVCO_ErrorUtil();
        errorUtil.logErrorFromOrderGroup(orderGroupList, 'Invoice Scheduler', 'hotdog', 'hamburger');
    }

    @isTest
    static void testManualGenerateInstallment()
    {
        c2g__codaInvoice__c sInv = [SELECT id, JVCO_Generate_Payment_Schedule__c,
                                    JVCO_Number_of_Payments__c, JVCO_First_Due_Date__c
                                    FROM c2g__codaInvoice__c LIMIT 1];
        sInv.JVCO_Generate_Payment_Schedule__c = true;
        sInv.JVCO_Number_of_Payments__c = 3;
        sInv.JVCO_First_Due_Date__c = System.today().addDays(5);
        sInv.JVCO_Payment_Method__c = 'Direct Debit';
        update sInv;
    }

    @isTest
    static void testPostCashEntry()
    {   
        c2g__codaInvoice__c sInv = [SELECT id, Name, c2g__Account__c
                                    FROM c2g__codaInvoice__c LIMIT 1];
        c2g__codaCashEntry__c cEntry = new c2g__codaCashEntry__c();
        cEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cEntry.ffcash__DeriveCurrency__c = true;
        cEntry.ffcash__DerivePeriod__c = true;
        insert cEntry;
        
        c2g__codaCashEntryLineItem__c cLine = new c2g__codaCashEntryLineItem__c();
        cLine.c2g__Account__c = sInv.c2g__Account__c;
        cLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cLine.c2g__CashEntryValue__c = 100;
        cLine.c2g__CashEntry__c = cEntry.Id;
        cLine.c2g__AccountReference__c = sInv.Name;
        insert cLine;
        
        Set<Id> cEntryIdSet = new Set<Id>();
        cEntryIdSet.add(cEntry.Id);
        
        Test.startTest();
        Database.executeBatch(new JVCO_CashEntriesPostingLogicBatch(cEntryIdSet));
        Test.stopTest();
        
    }
    
    /*@isTest
    static void testUpdateSurchargeValueOfRelatedInvoice()
    {
        c2g__codaInvoice__c sInv = [SELECT id, Name, c2g__Account__c
                                    FROM c2g__codaInvoice__c LIMIT 1];
        Map<Id, c2g__codaInvoice__c> sInvList = new Map<Id, c2g__codaInvoice__c>();
        c2g__codaInvoice__c surSInv = new c2g__codaInvoice__c();
        surSInv.JVCO_Invoice_Type__c = 'Surcharge';
        surSInv.JVCO_Original_Invoice__c = sInv.Id;
        surSInv.JVCO_Invoice_Legacy_Number__c = '1234567890';
        sInvList.put(sInv.Id, surSInv);
        Test.startTest();
        JVCO_SalesInvoiceHandler.afterInsert(sInvList);
        Test.stopTest();
    }*/

    /*@isTest
    static void testCreateDDFromDDMandateBatch()
    {
        List<c2g__codaInvoice__c> sInvList = [SELECT id, Name, c2g__Account__c, c2g__OutstandingValue__c FROM c2g__codaInvoice__c LIMIT 1];
        Set<Id> sInvIdSet = new Set<Id>();
        for(c2g__codaInvoice__c sInv : sInvList)
        {
            sInvIdSet.add(sInv.Id);
        }
        Id paybankId = JVCO_TestClassHelper.getPayBank();
        Id ddMandateId = JVCO_TestClassHelper.getDDMandate(sInvList[0].c2g__Account__c);
        Id paySchedId = JVCO_TestClassHelper.getPaySched(payBankId);
        Test.startTest();
        JVCO_CreateDDFromDDMandateBatch ddB = new JVCO_CreateDDFromDDMandateBatch(sInvIdSet);
        Database.QueryLocator ql = ddB.start(null);
        ddB.execute(null, sInvList);
        ddB.finish(null);
        Test.stopTest();
    }

    @isTest
    static void testRecreateSInvInstlLineBatch()
    {
        List<c2g__codaInvoice__c> sInvList = [SELECT id, name, JVCO_Payment_Method__c, JVCO_Number_of_Installment_Line_Items__c, JVCO_Invoice_Group__c, 
                                              JVCO_Invoice_Group__r.JVCO_Payment_Agreement__c, JVCO_Invoice_Group__r.createddate, 
                                              c2g__PaymentStatus__c, JVCO_Invoice_Group__r.Name, c2g__Account__c,
                                              c2g__OutstandingValue__c, c2g__Account__r.JVCO_DD_Mandate_Checker__c,
                                              JVCO_Outstanding_Amount_to_Process__c
                                              FROM c2g__codaInvoice__c LIMIT 1];
        Set<Id> sInvIdSet = new Set<Id>();
        for(c2g__codaInvoice__c sInv : sInvList)
        {
            sInvIdSet.add(sInv.Id);
        }
        Id paybankId = JVCO_TestClassHelper.getPayBank();
        Id ddMandateId = JVCO_TestClassHelper.getDDMandate(sInvList[0].c2g__Account__c);
        Id paySchedId = JVCO_TestClassHelper.getPaySched(payBankId);
        Test.startTest();
        JVCO_CreateDDFromDDMandateBatch ddB = new JVCO_CreateDDFromDDMandateBatch(sInvIdSet);
        Database.QueryLocator ql = ddB.start(null);
        ddB.execute(null, sInvList);
        ddB.finish(null);
        JVCO_RecreateSalesInvoiceInstallmentline rss = new JVCO_RecreateSalesInvoiceInstallmentline(sInvIdSet);
        rss.execute(null, sInvList);
        rss.finish(null);
        Test.stopTest();
    }

    @isTest
    static void testOverdueInstallmentBatch()
    {
        c2g__codaInvoice__c sInv = [SELECT id, JVCO_Generate_Payment_Schedule__c,
                                    JVCO_Number_of_Payments__c, JVCO_First_Due_Date__c
                                    FROM c2g__codaInvoice__c LIMIT 1];
        sInv.JVCO_Generate_Payment_Schedule__c = true;
        sInv.JVCO_Number_of_Payments__c = 3;
        sInv.JVCO_First_Due_Date__c = System.today().addDays(5);
        sInv.JVCO_Payment_Method__c = 'Cheque';
        update sInv;

        Test.startTest();
        List<c2g__codaInvoiceInstallmentLineItem__c> invoiceInstallmentLineList = [SELECT Id, c2g__Invoice__c
                                                                                    FROM c2g__codaInvoiceInstallmentLineItem__c];
        JVCO_OverdueInstalment_Batch oib = new JVCO_OverdueInstalment_Batch();
        oib.execute(null, invoiceInstallmentLineList);
        oib.finish(null);
        Test.stopTest();
    }

    @isTest
    static void testSingleOverdueInstallmentBatch()
    {
        c2g__codaInvoice__c sInv = [SELECT id, JVCO_Generate_Payment_Schedule__c,
                                    JVCO_Number_of_Payments__c, JVCO_First_Due_Date__c
                                    FROM c2g__codaInvoice__c LIMIT 1];
        sInv.JVCO_Generate_Payment_Schedule__c = true;
        sInv.JVCO_Number_of_Payments__c = 3;
        sInv.JVCO_First_Due_Date__c = System.today().addDays(5);
        sInv.JVCO_Payment_Method__c = 'Cheque';
        update sInv;

        Test.startTest();
        List<c2g__codaInvoiceInstallmentLineItem__c> invoiceInstallmentLineList = [SELECT Id, c2g__Invoice__c
                                                                                    FROM c2g__codaInvoiceInstallmentLineItem__c];
        Database.executeBatch(new JVCO_OverdueInstalment_Batch());
        JVCO_OverdueInstalment_Batch oib = new JVCO_OverdueInstalment_Batch(sInv.Id);
        Database.QueryLocator ql = oib.start(null);
        oib.execute(null, invoiceInstallmentLineList);
        oib.finish(null);
        Test.stopTest();
    }

    @isTest
    static void testCreateInstallmentLineErrorLog()
    {
        c2g__codaInvoice__c sInv = [SELECT id, JVCO_Generate_Payment_Schedule__c,
                                    JVCO_Number_of_Payments__c, JVCO_First_Due_Date__c
                                    FROM c2g__codaInvoice__c LIMIT 1];
        sInv.JVCO_Generate_Payment_Schedule__c = true;
        sInv.JVCO_Number_of_Payments__c = 3;
        sInv.JVCO_First_Due_Date__c = System.today().addDays(5);
        sInv.JVCO_Payment_Method__c = 'Cheque';
        update sInv;

        Test.startTest();
        List<c2g__codaInvoiceInstallmentLineItem__c> invoiceInstallmentLineList = [SELECT Id, c2g__Invoice__c
                                                                                    FROM c2g__codaInvoiceInstallmentLineItem__c];
        JVCO_OverdueInstalmentHandler.createInstallmentLineErrorLog(invoiceInstallmentLineList, 'hotdog');
        Test.stopTest();
    }*/
}