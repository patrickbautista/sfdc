/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CreditCancellationLogic.cls 
   Description: Class for c2g__codaCreditNote__c to cancel credit note and produce invoice
   Test Class: JVCO_CreditCancellationLogicTest

   Date         Version     Author                     Summary of Changes 
   -----------  -------     -----------------          -----------------------------------------
   19-Feb-2021  0.1         patrick.t.bautista         Intial creation
  ----------------------------------------------------------------------------------------------- */
public class JVCO_CreditCancellationLogic implements Database.Batchable<sObject>, Database.Stateful
{
    private Boolean sInvIsCreated = false;
    private Boolean isSubscriptionBatch = false;
    private c2g__codaCreditNote__c cNoteToBatch;
    final JVCO_General_Settings__c GeneralSettings = JVCO_General_Settings__c.getInstance();
    final Decimal cancelCredBatchSize = GeneralSettings != null ? GeneralSettings.JVCO_Cancel_Credit_Note_batch_size__c : 100;
    private Set<Id> orderIdSet = new Set<Id>();
    private Set<Id> quoteIdSet = new Set<Id>();
    private Set<Id> oppIdSet = new Set<Id>();
    private c2g__codaInvoice__c sInv = new c2g__codaInvoice__c();
    private Map<String, c2g__codaInvoiceLineItem__c> dim2ToInvoiceLineMap = new Map<String, c2g__codaInvoiceLineItem__c>();
    private Map<Id, String> dim2Map = new Map<Id,String>();
    public JVCO_CreditCancellationLogic(c2g__codaCreditNote__c cNoteToBatch)
    {
        this.cNoteToBatch = cNoteToBatch;
    }
    
    public JVCO_CreditCancellationLogic(Set<Id> quoteIdSet)
    {
        this.quoteIdSet = quoteIdSet;
        this.isSubscriptionBatch = true;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        if(!isSubscriptionBatch){
            return Database.getQueryLocator([SELECT Id, Name,
                                             JVCO_Sales_Credit_Note__c, JVCO_Invoice_Type__c,
                                             SBQQ__Quote__c, AccountId,
                                             SBQQ__Quote__r.SBQQ__Opportunity2__c,
                                             JVCO_Total_Amount__c, JVCO_Sales_Invoice__c,
                                             Pricebook2Id, EffectiveDate, EndDate, TotalAmount,
                                             JVCO_Licence_End_Date__c, JVCO_Licence_Start_Date__c,
                                             JVCO_PPL_Total_Amount__c, JVCO_PRS_Total_Amount__c,
                                             JVCO_VPL_Total_Amount__c,
                                             JVCO_Temp_End_Date__c, JVCO_Temp_Start_Date__c,
                                             JVCO_Order_Status__c
                                             FROM Order
                                             WHERE JVCO_Sales_Credit_Note__c =: cNoteToBatch.Id
                                             ORDER BY Id DESC]);
        }else{
            return Database.getQueryLocator([SELECT Id, SBQQ__QuoteLine__r.Terminate__c,
                                             SBQQ__QuoteLine__r.SBQQ__UpgradedSubscription__c
                                             FROM SBQQ__Subscription__c
                                             WHERE SBQQ__QuoteLine__r.SBQQ__Quote__c IN : quoteIdSet
                                             AND SBQQ__QuoteLine__r.SBQQ__Quote__r.SBQQ__Type__c IN ('Amendment')]);
        }
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        stopperMethod();
        if(!isSubscriptionBatch){
            getDim2();
            createSalesInvoice();
            cloneOrderAndOrderLines((List<Order>)scope);
            cancelBilling((List<Order>)scope);
            if(!orderIdSet.isEmpty())
            {   
                cancelOrder();
                cancelQuote();
                cancelOpportunity();
            }
        }
        else{
            deleteContractAndSubscription((List<SBQQ__Subscription__c>)scope);
        }
    }
    
    private void cancelBilling(List<Order> orderList)
    {
        orderIdSet = new Set<Id>();
        oppIdSet = new Set<Id>();
        List<Order> updatedOrderList = new List<Order>();
        for(Order ord : orderList)
        {
            Order o = new Order();
            o.Id = ord.Id;
            o.JVCO_Cancelled__c = true;
            //o.JVCO_Sales_Invoice__c = null;
            o.OpportunityId = null;
            o.SBQQ__Quote__c = null;
            updatedOrderList.add(o);
            
            orderIdSet.add(ord.Id);
            quoteIdSet.add(ord.SBQQ__Quote__c);
            oppIdSet.add(ord.SBQQ__Quote__r.SBQQ__Opportunity2__c);
        }
        update updatedOrderList;
    }
    
    private void cancelOrder()
    {
        //Start 30-Jan-2019 mel.andrei.b.santos  GREEN-33826
        List<OrderItem> updateOrderProductList = new List<OrderItem>();
        List<OrderItem> orderProductList = [SELECT id, SBQQ__QuoteLine__c, blng__HoldBilling__c, 
                                            SBQQ__Activated__c, blng__BilledAmountwithouttax__c, blng__BilledTax__c  
                                            FROM OrderItem WHERE orderid IN : orderIdSet];

        for(OrderItem orderprod : orderProductList)
        {
            orderprod.SBQQ__QuoteLine__c = null;
            orderprod.blng__HoldBilling__c = 'Yes';
            orderprod.SBQQ__Activated__c = false;
            orderprod.blng__BilledTax__c = 0;
            orderprod.blng__BilledAmountwithouttax__c = 0;
            updateOrderProductList.add(orderprod);
        }
        update updateOrderProductList;
    }
    
    private void cancelQuote()
    {
        List<SBQQ__Quote__c> updatedQuoteList = new List<SBQQ__Quote__c>();
        for (Id qId : quoteIdSet)
        {   
            SBQQ__Quote__c q = new SBQQ__Quote__c();
            q.Id = qId;
            q.EditLinesFieldSetName__c = 'Cancelled_Invoice';//GREEN-18635 - franz.g.a.dimaapi
            q.JVCO_QuoteComplete__c = false;
            q.JVCO_Cancelled__c = true;
            //q.JVCO_Sales_Invoice__c = null;//Set to NULL - GREEN-22873 - jasper.j.figueroa
            //q.JVCO_Prior_Sales_Invoice__c = sInvId;
            q.SBQQ__Status__c = 'Draft';//Changed to Draft - GREEN-22487 - franz.g.a.dimaapi - 08/26/2017
            q.RecordTypeId = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('Open Quote').getRecordTypeId();
            updatedQuoteList.add(q);
        }
        update updatedQuoteList;
    }
    
    private void cancelOpportunity()
    {
        List<Opportunity> updatedOppList = new List<Opportunity>();
        for(Id oppId : oppIdSet)
        {
            Opportunity opp = new Opportunity();
            opp.Id = oppId;
            opp.StageName = 'Quoting';
            opp.Probability = 50;
            opp.JVCO_Invoiced__c = FALSE;
            opp.SBQQ__Ordered__c = FALSE;
            opp.SBQQ__Contracted__c = FALSE;
            opp.JVCO_OpportunityCancelled__c = TRUE;
            updatedOppList.add(opp);
        }
        update updatedOppList;
    }
    
    private void deleteContractAndSubscription(List<SBQQ__Subscription__c> subscriptionList)
    {
        //Delete Subscription for Amendment
        Set<SBQQ__Subscription__c> terminatedSubscriptionSet = new Set<SBQQ__Subscription__c>();
        for(SBQQ__Subscription__c subscription : subscriptionList)
        {
            if(subscription.SBQQ__QuoteLine__r.Terminate__c && subscription.SBQQ__QuoteLine__r.SBQQ__UpgradedSubscription__c != null)
            {
                SBQQ__Subscription__c terminatedSubscription = new SBQQ__Subscription__c();
                terminatedSubscription.Id = subscription.SBQQ__QuoteLine__r.SBQQ__UpgradedSubscription__c;
                terminatedSubscription.SBQQ__TerminatedDate__c = null;
                terminatedSubscriptionSet.add(terminatedSubscription);
            }
        }
        //Update Terminated Subscription
        update new List<SBQQ__Subscription__c>(terminatedSubscriptionSet);
        delete subscriptionList;
        //Delete Contract
        delete [SELECT Id FROM Contract WHERE SBQQ__Quote__c IN :quoteIdSet];
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Create Credit Note Mapping
        Inputs: List<c2g__codaInvoice__c>, Map<Id, c2g__codaCreditNote__c>
        Returns: c2g__codaCreditNote__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void createSalesInvoice()
    {
        if(!sInvIsCreated){
            createInvoice();
            createInvoiceLine();
            cNoteToBatch.JVCO_Credit_Note_Cancelled__c = true;
            sInvIsCreated = true;
            update cNoteToBatch;
        }
    }
    
    private void cloneOrderAndOrderLines(List<Order> orderList){
        
        Map<Id, List<OrderItem>> ordIdToOrdLineListMap = getOrderLineRecordsByOrderId(orderList);
        
        List<Order> orderCloneList = new List<Order>();
        List<OrderItem> orderLineCloneList = new List<OrderItem>();
        
        for(Order ord : orderList){
            if(ordIdToOrdLineListMap.containsKey(ord.Id)){
                Order ordClone = ord.clone(false, true, false, false);
                ordClone.JVCO_Invoice_Type__c = 'Credit Cancellation';
                ordClone.JVCO_Original_Order__c = ord.Id;
                ordClone.JVCO_Sales_Invoice__c = sInv.Id;
                ordClone.JVCO_Sales_Credit_Note__c = null;
                ordClone.SBQQ__Quote__c = null;
                //ordClone.blng__DueDate__c = Date.today();
                ordClone.EffectiveDate = Date.today();
                ordClone.Status = 'Draft';
                ordClone.JVCO_PPL_Total_Amount__c = ordClone.JVCO_PPL_Total_Amount__c != null ? ordClone.JVCO_PPL_Total_Amount__c * -1 : 0;
                ordClone.JVCO_PRS_Total_Amount__c = ordClone.JVCO_PRS_Total_Amount__c != null ? ordClone.JVCO_PRS_Total_Amount__c * -1 : 0;
                ordClone.JVCO_VPL_Total_Amount__c = ordClone.JVCO_VPL_Total_Amount__c != null ? ordClone.JVCO_VPL_Total_Amount__c * -1 : 0;
                ordClone.JVCO_Order_Status__c = 'Posted';
                orderCloneList.add(ordClone);
                for(OrderItem ordLine : ordIdToOrdLineListMap.get(ord.Id)){
                    //Clone Invoice Line
                    OrderItem ordLineClone = ordLine.clone(false, true, false, false);
                    ordLineClone.ServiceDate = Date.today();
                    ordLineClone.EndDate = Date.today();
                    ordLineClone.SBQQ__QuoteLine__c = null;
                    ordLineClone.Quantity = ordLineClone.Quantity * -1;  
                    orderLineCloneList.add(ordLineClone);
                }
            }
        }
        
        insert orderCloneList;
        
        List<OrderItem> updatedOrderLineCloneList = new List<OrderItem>();
        List<Order> updateOrderStatusList = new List<Order>();
        
        for(Order ordClone : orderCloneList)
        {
            ordClone.Status = 'Activated';
            updateOrderStatusList.add(ordClone);
            for(OrderItem ordLineClone : orderLineCloneList)
            {
                if(ordLineClone.OrderId == ordClone.JVCO_Original_Order__c){
                    ordLineClone.OrderId = ordClone.id;
                    if(ordLineClone.JVCO_Dimension_2__c != null && dim2ToInvoiceLineMap.containsKey(ordLineClone.JVCO_Dimension_2__c) 
                       && dim2ToInvoiceLineMap.get(ordLineClone.JVCO_Dimension_2__c) != null){
                        ordLineClone.JVCO_Sales_Invoice_Line_Item__c = dim2ToInvoiceLineMap.get(ordLineClone.JVCO_Dimension_2__c).Id;
                    }
                    updatedOrderLineCloneList.add(ordLineClone);
                }
            }    
        }
        insert updatedOrderLineCloneList;
        update updateOrderStatusList;
    }
    
    private static Map<Id, List<OrderItem>> getOrderLineRecordsByOrderId(List<Order> orderList)
    {   
        Map<Id, List<OrderItem>> ordIdToOrdLineListMap = new Map<Id, List<OrderItem>>();
        for(OrderItem ordItem : [SELECT Id,SBQQ__BookingsIndicator__c, 
                                 PricebookEntryId, ListPrice, SBQQ__QuoteLine__r.Terminate__c,
                                 SBQQ__BillingFrequency__c,SBQQ__BillingType__c,
                                 SBQQ__ChargeType__c,SBQQ__SubscriptionPricing__c,
                                 TotalPrice, UnitPrice, Quantity, Product2Id,
                                 ServiceDate , EndDate, OrderId,
                                 JVCO_Surcharge_Applicable__c,
                                 JVCO_Dimension_2__c, JVCO_Venue__c,
                                 JVCO_Total_Amount__c,
                                 SBQQ__DefaultSubscriptionTerm__c,
                                 JVCO_Temp_End_Date__c,
                                 JVCO_Temp_Start_Date__c,
                                 Affiliation__c
                                 FROM OrderItem
                                 WHERE OrderId IN :orderList])
        {
            //If the id doesn't exist then add the Id plus a new List
            if(!ordIdToOrdLineListMap.containsKey(ordItem.OrderId))
            {
                ordIdToOrdLineListMap.put(ordItem.OrderId, new List<OrderItem>{});
            }
            ordIdToOrdLineListMap.get(ordItem.OrderId).add(ordItem);
        }
        return ordIdToOrdLineListMap;
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Create Aggregate Mapping for Credit Note Line
        Inputs: List<c2g__codaInvoice__c>, Map<Id, c2g__codaCreditNote__c>
        Returns: c2g__codaCreditNote__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void createInvoiceLine()
    {   
        List<c2g__codaInvoiceLineItem__c> sInvLineItemList = new List<c2g__codaInvoiceLineItem__c>();
        for(AggregateResult ar : [SELECT c2g__CreditNote__c credId,
                                  c2g__Dimension1__c dim1Id,
                                  c2g__Dimension2__c dim2Id, 
                                  c2g__Dimension3__c dim3Id,
                                  c2g__Product__c prodId, 
                                  c2g__TaxCode1__c taxCodeId,
                                  SUM(c2g__TaxValue1__c)totalTaxValue,
                                  SUM(c2g__NetValue__c)totalNetValue
                                  FROM c2g__codaCreditNoteLineItem__c
                                  WHERE c2g__CreditNote__c =: cNoteToBatch.Id
                                  GROUP BY c2g__CreditNote__c,
                                  c2g__Dimension2__c, 
                                  c2g__Dimension1__c,
                                  c2g__Dimension3__c, 
                                  c2g__Product__c, 
                                  c2g__TaxCode1__c])
        {
            sInvLineItemList.add(createInvoiceLineItem(ar, sInv.Id));
        }
        insert sInvLineItemList;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Credit Note
        Inputs: Id, Date, Date
        Returns: c2g__codaCreditNote__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void createInvoice()
    {
        sInv.c2g__Account__c = cNoteToBatch.c2g__Account__c;
        sInv.c2g__InvoiceDate__c = System.today();
        sInv.c2g__DueDate__c = System.today();
        sInv.c2g__DeriveCurrency__c = true;
        sInv.c2g__DerivePeriod__c = true;
        sInv.c2g__DeriveDueDate__c = false;
        sInv.JVCO_Credit_Reason__c = 'Cancelled Credit Note';
        sInv.JVCO_Licence_Start_Date__c = cNoteToBatch.JVCO_Period_Start_Date__c;
        sInv.JVCO_Licence_End_Date__c = cNoteToBatch.JVCO_Period_End_Date__c;
        sInv.JVCO_Invoice_Type__c = 'Credit Cancellation';
        sInv.JVCO_Sales_Rep__c = cNoteToBatch.JVCO_Sales_Rep__c;
        insert sInv;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Credit Note Line
        Inputs: AggregateResult, Id
        Returns: c2g__codaCreditNoteLineItem__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private c2g__codaInvoiceLineItem__c createInvoiceLineItem(AggregateResult ar, Id sInvId)
    {
        c2g__codaInvoiceLineItem__c sInvLine = new c2g__codaInvoiceLineItem__c();
        sInvLine.c2g__Invoice__c = sInvId;
        sInvLine.c2g__Dimension1__c = (Id)ar.get('dim1Id');
        sInvLine.c2g__Dimension3__c = (Id)ar.get('dim3Id');
        sInvLine.c2g__TaxCode1__c = (Id)ar.get('taxCodeId');
        sInvLine.c2g__DeriveLineNumber__c = true;
        sInvLine.c2g__DeriveUnitPriceFromProduct__c = false;
        sInvLine.c2g__CalculateTaxValue1FromRate__c = false;
        sInvLine.c2g__TaxValue1__c = (Decimal)ar.get('totalTaxValue');
        sInvLine.c2g__Quantity__c = 1;
        sInvLine.c2g__UnitPrice__c = (Decimal)ar.get('totalNetValue');
        sInvLine.c2g__Dimension2__c = (Id)ar.get('dim2Id');
        sInvLine.c2g__Product__c = (Id)ar.get('prodId');
        if(dim2Map.containsKey(sInvLine.c2g__Dimension2__c)){
            dim2ToInvoiceLineMap.put(dim2Map.get(sInvLine.c2g__Dimension2__c), sInvLine);
        }
        return sInvLine;
    }
    
    private void stopperMethod()
    {   
        JVCO_FFUtil.stopAccountDLRSHandler = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = false;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        JVCO_FFUtil.stopBlngInvoiceLineHandlerBeforeInsert = true;
        JVCO_FFUtil.stopCodaInvoiceHandlerAfterUpdate = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_FFUtil.stopContractHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopContractHandlerAfterUpdate = true;
        JVCO_FFUtil.stopQuoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopQuoteHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopOpportunityHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopOrderAfterUpdateHandler = true;
        JVCO_FFUtil.stopOrderBeforeUpdateHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractDeletion = true;
        JVCO_FFUtil.stopSubscriptionDeletion = true;
        JVCO_FFUtil.stopSubscriptionTrigger = true;
        JVCO_FFUtil.stopSubscriptionDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceTrigger = true;
        JVCO_FFUtil.stopBillNowFuction = true;
        SBQQ.TriggerControl.disable();
        
    }
    
    public void getDim2(){
        
        dim2Map = new Map<Id,String>();
        
        for(c2g__codaDimension2__c dim2 : [SELECT Id, Name, c2g__ReportingCode__c 
                                           FROM c2g__codaDimension2__c 
                                           WHERE Name LIKE 'PRS'
                                           OR Name LIKE 'PPL'
                                           OR Name LIKE 'VPL'
                                           OR NAME LIKE 'JVCO'])
        {
            dim2Map.put(dim2.Id, dim2.Name);
        }
    }
    
    public void finish(Database.BatchableContext BC)
    {
        if(!Test.isRunningTest() && !isSubscriptionBatch){
            Database.executeBatch(new JVCO_CreditCancellationLogic(quoteIdSet), Integer.valueOf(cancelCredBatchSize));
        }
    }
}