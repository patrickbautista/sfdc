global with sharing class SMP_DDServiceHandler {

    public static final String MESSAGE_CALLOUTERROR = 'Error Calling Income Systems Service.';    
    public static SmarterPay_Direct_Debit_Settings__c directDebitConfig = SmarterPay_Direct_Debit_Settings__c.getInstance();

    webservice static SMP_DDServiceNextCollectionModel getNextCollectionDate(String collectionDayIn, Date startDateIn, String collectionPeriodIn, String numberOfDaysToWaitFromNewInstructionIn)
    {
        System.debug('#### getNextCollectionDate Started');
        System.debug('#### collectionDayIn: ' + collectionDayIn);
        System.debug('#### startDateIn: ' + startDateIn);
        System.debug('#### collectionPeriodIn: ' + collectionPeriodIn);
        System.debug('#### numberOfDaysToWaitFromNewInstructionIn: ' + numberOfDaysToWaitFromNewInstructionIn);

        String dateString = '';

        if(startDateIn != null){
            dateString = startDateIn.year() + '-' + startDateIn.month() + '-' + startDateIn.day();
        }
        System.debug('startDate' + dateString);

        DateWrapperObject dateWrapper = new DateWrapperObject();
        dateWrapper.CollectionDay = collectionDayIn;
        dateWrapper.StartDate = dateString;
        dateWrapper.CollectionPeriod = collectionDayIn;
        dateWrapper.NumberOfDaysToWaitFromNewInstruction = numberOfDaysToWaitFromNewInstructionIn;

        return getNextCollectionDateWrapper(dateWrapper);
    }

    public static SMP_DDServiceNextCollectionModel getNextCollectionDateWrapper(DateWrapperObject dateWrapper)
    {
        System.debug('#### getNextCollectionDate Started');
        System.debug('#### collectionDayIn: ' + dateWrapper.CollectionDay);
        System.debug('#### startDateIn: ' + dateWrapper.StartDate);
        System.debug('#### collectionPeriodIn: ' + dateWrapper.CollectionPeriod);
        System.debug('#### numberOfDaysToWaitFromNewInstructionIn: ' + dateWrapper.NumberOfDaysToWaitFromNewInstruction);

        SMP_DDServiceNextCollectionModel parsedResponse;
        String endpointURL = '';
       
        try{
            endpointURL = string.valueOf(directDebitConfig.Calculate_Next_Collection_Date_Endpoint__c);
            
        }catch(Exception e){}

        String dateString = '';

        System.debug('startDate' + dateString);

        String jsonString = JSON.serialize(dateWrapper);
        System.Debug('######' + jsonString);

        jsonString = jsonString.replaceAll('DateWrapper', 'Date');     
        jsonString = jsonString.replaceAll('(\\s+)', ' ');        
        
        endpointURL = endpointURL + EncodingUtil.urlEncode(jsonString, 'UTF-8');
        
        try{
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            req.setMethod('GET'); 
            req.setEndPoint(endpointURL);      
            HttpResponse resp;
            resp = http.send(req);
            System.Debug('resp.getBody()'+resp.getBody());
            
            if(resp.getStatusCode() == 200)
            {
                parsedResponse = SMP_DDServiceNextCollectionModel.parse(resp.getBody());
                System.debug(parsedResponse);
            }

            System.debug('#### parsedResponse: ' + parsedResponse);

            return parsedResponse;
        }
        catch(Exception e)
        {
            parsedResponse = new SMP_DDServiceNextCollectionModel();
            parsedResponse.ProcessNewDDFirstCollectionDateResult.Error = MESSAGE_CALLOUTERROR;

            System.debug('#### parsedResponse: ' + parsedResponse);
            System.debug('#### debug e: ' + e);

            return parsedResponse;
        }
                
    }

    public static SMP_DDServiceNextCollectionModelList getNextCollectionDateList(CDWrapper cdWrapper)
    {
        System.debug('#### getNextCollectionDate Started');
        System.debug('#### wrapperList: ' + cdWrapper);

        SMP_DDServiceNextCollectionModelList parsedResponse;
        String endpointURL = '';
       
        try{
            endpointURL = string.valueOf(directDebitConfig.List_Next_Collection_Date_Endpoint__c);
            
        }catch(Exception e){}

        String dateString = '';

        System.debug('startDate' + dateString);

        String jsonString = JSON.serialize(cdWrapper);
        System.Debug('######' + jsonString);
     
        jsonString = jsonString.replaceAll('(\\s+)', ' ');      

        System.Debug('###### json string: ' + jsonString);
        
        endpointURL = endpointURL + EncodingUtil.urlEncode(jsonString, 'UTF-8');
        
        System.Debug('###### endpoint url' + endpointURL);

        try{
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            req.setMethod('GET'); 
            req.setEndPoint(endpointURL);      
            HttpResponse resp;
            resp = http.send(req);
            System.Debug('resp.getBody()'+resp.getBody());
            
            if(resp.getStatusCode() == 200)
            {

                parsedResponse = SMP_DDServiceNextCollectionModelList.parse(resp.getBody());
                
                System.debug(parsedResponse);
            }

            System.debug('#### parsedResponse: ' + parsedResponse);

            return parsedResponse;
        }
        catch(Exception e)
        {
            //parsedResponse = new List<SMP_DDServiceNextCollectionModelList>();
            // for(SMP_DDServiceNextCollectionModelList idd : parsedResponse){
            //     idd.ProcessListNewDDFirstCollectionDateResult.Error = MESSAGE_CALLOUTERROR;
            // }
            if(Test.isRunningTest()){
                parsedResponse = SMP_DDServiceNextCollectionModelList.parse('{"ProcessListNewDDFirstCollectionDateResult":[{"Error":"","FirstCollectionDate":"' + date.today().year() + '-' + date.today().Month() + '-' + date.today().Year() + '"},{"Error":"","FirstCollectionDate":"' + date.today().year() + '-' + date.today().Month() + '-' + date.today().Year() + '"}]}');
            }
            System.debug('#### parsedResponse: ' + parsedResponse);
            System.debug('#### debug e: ' + e);

            return parsedResponse;
        }
                
    }

    /**
    * @description Handles calling the Income Systems Bank Account Checker service.
    * @param sortCode Bank account sort code.
    * @param accountNo Bank account number.
    * @return SMP_DDServiceBankAccountModel Model/Wrapper that encapsulates the response from the Income Systems Bank Account Checker.
    */
    webservice static SMP_DDServiceBankAccountModel getBankDetails(String sortCode, String accountNo)
    {        
        System.debug('#### getBankDetails');

        SMP_DDServiceBankAccountModel parsedResponse;    
        String endpointURL = '';
        String userId = '';
       
        try{
            endpointURL = string.valueOf(directDebitConfig.Bank_Account_Checker_Endpoint__c);
            userId = string.valueOf(directDebitConfig.User_Id__c);            
        }
        catch(Exception e){
            System.debug('#### Couldn\'t find setup records!');
            System.debug('#### Exception: ' + e);
        }

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('SortCode', sortCode);
        gen.writeStringField('AccountNo', accountNo);
        gen.writeStringField('UserID', userId);
        gen.writeEndObject();

        String jsonString = gen.getAsString();
        jsonString = jsonString.replaceAll('(\\s+)', ' ');        
        
        endpointURL = endpointURL + EncodingUtil.urlEncode(jsonString, 'UTF-8');
        
        try{
            HttpRequest req = new HttpRequest();

            Http http = new Http();
            req.setMethod('GET'); 
            req.setEndPoint(endpointURL);

            HttpResponse resp;
            resp = http.send(req);

            System.debug('#### Response Body: ' + resp.getBody());
            System.debug('#### Response Status Code: ' + resp.getStatusCode());
                    
            if(resp.getStatusCode() == 200)
            {        
                parsedResponse = SMP_DDServiceBankAccountModel.parse(resp.getBody());            
            } 
            
            System.debug('#### Result: ' + parsedResponse);
            return parsedResponse; 
        }
        catch(Exception e)
        {
            parsedResponse = new SMP_DDServiceBankAccountModel();
            parsedResponse.ProcessMessageResult.Error = MESSAGE_CALLOUTERROR;

            return parsedResponse; 
        }
    }

    //Date wrapper to enable serialization to json before callout
    public class DateWrapperObject{
        public String CollectionDay {get; set;}
        public String StartDate {get; set;}
        public String CollectionPeriod {get; set;}
        public String NumberOfDaysToWaitFromNewInstruction{get; set;}
    }
    public class DateWrapper{
        public List<DateWrapperObject> DateWrapper {get; set;}

        public DateWrapper(List<DateWrapperObject> wrapperIn){
            DateWrapper = wrapperIn;
        }
    }
    //CDWrapper for list methods to reduce the size of the rest call, Character limit is 2000 
    //which we hit if we have 12 payment dates to send this class below should handle up to 24, 
    //possibly more...
    public class CDWrapperObject{
        public String sd {get; set;}
    }
    public class CDWrapper{
        public String cp {get; set;}
        public String nodtw{get; set;}
        public String cd {get; set;}
        public List<CDWrapperObject> CDWrapper {get; set;}

        public CDWrapper(List<CDWrapperObject> cdwrapperIn, String cpIn, String nodtwIn, String cdIn){
            cp = cpIn;
            nodtw = nodtwIn;
            cd = cdIn;
            CDWrapper = cdwrapperIn;
        }
    }
}