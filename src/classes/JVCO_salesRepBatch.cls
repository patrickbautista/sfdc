/* ----------------------------------------------------------------------------------------------
    Name: JVCO_salesRepBatch
    Description: Batch Class for updating Sales Rep in Sales Invoice and Sales Credit Note

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    02-May-2018     0.1         john.patrick.valdez      Intial creation
    12-Jul-2018     0.2         john.patrick.valdez      Modified Surcharge Sales Rep
----------------------------------------------------------------------------------------------- */
public class JVCO_salesRepBatch /*implements Database.Batchable<sObject>*/{

    /*private id bInvoice;
    public JVCO_salesRepBatch(){}
    public JVCO_salesRepBatch(id bInvoice)
    {

        this.bInvoice = bInvoice;

    }
    public Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator([SELECT Id, blng__Order__r.SBQQ__Quote__r.SBQQ__SalesRep__c,
                                         JVCO_Sales_Invoice__c, JVCO_Sales_Credit_Note__c,
                                         JVCO_Sales_Invoice__r.id ,JVCO_Sales_Credit_Note__r.id,
                                         JVCO_Migrated__c, JVCO_Invoice_Type__c, 
                                         JVCO_Original_Invoice__r.CreatedBy.Name,
                                         JVCO_Original_Invoice__r.CreatedById, 
                                         blng__Order__r.SBQQ__Quote__c,
                                         CreatedBy.isActive,CreatedById, 
                                         JVCO_Sales_Credit_Note__r.c2g__Invoice__r.JVCO_Invoice_Type__c 
                                         FROM blng__Invoice__c
                                         WHERE ((JVCO_Sales_Invoice__r.c2g__InvoiceStatus__c = 'Complete'
                                          AND JVCO_Sales_Invoice__r.JVCO_Sales_Rep__c = '')
                                         OR (JVCO_Sales_Credit_Note__r.c2g__CreditNoteStatus__c = 'Complete'
                                             AND JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__c = '')
                                         OR (JVCO_Sales_Invoice__r.c2g__InvoiceStatus__c = 'Complete'
                                          AND JVCO_Sales_Invoice__r.JVCO_Sales_Rep__r.Name != 'Surcharge User'
                                            AND JVCO_Invoice_Type__c = 'Surcharge')
                                         OR (JVCO_Sales_Credit_Note__r.c2g__CreditNoteStatus__c = 'Complete'
                                             AND JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__r.Name != 'Surcharge User'
                                            AND JVCO_Sales_Credit_Note__r.c2g__Invoice__r.JVCO_Invoice_Type__c = 'Surcharge'))
                                         AND CreatedBy.isActive = true]);
     }
    public void execute(Database.BatchableContext BC, List<blng__Invoice__c> scope)
    {
        Map<Id, c2g__codaInvoice__c> updatedSInvMap = new Map<Id, c2g__codaInvoice__c>();
        Map<Id, c2g__codaCreditNote__c> updatedSCNoteMap = new Map<Id, c2g__codaCreditNote__c>();
        List<User> surUserList = [SELECT Id, Name, Department, UserRole.Name, Profile.Name, Manager.Name 
                                  FROM User 
                                  WHERE Name = 'Surcharge User'];
        for(blng__Invoice__c bInv : scope)
        {
            //Billing Invoice with Sales Invoice
            if(bInv.JVCO_Sales_Invoice__c != null && bInv.JVCO_Sales_Credit_Note__c == null)
            {
                c2g__codaInvoice__c newInvoice = new c2g__codaInvoice__c();
                newInvoice.Id = bInv.JVCO_Sales_Invoice__r.id;
                if (bInv.JVCO_Invoice_Type__c == 'Surcharge')
                {
                    newInvoice.JVCO_Sales_Rep__c = surUserList[0].Id;
                    newInvoice.SalesRep_Department__c = surUserList[0].Department;
                    newInvoice.SalesRep_Role__c = surUserList[0].UserRole.Name;
                    newInvoice.SalesRep_Profile__c = surUserList[0].Profile.Name;
                    newInvoice.SalesRep_Manager__c = surUserList[0].Manager.Name; 
                }
                else if(bInv.JVCO_Migrated__c)                    
                {
                    newInvoice.JVCO_Sales_Rep__c = bInv.CreatedById;
                }    
                else
                {  
                    newInvoice.JVCO_Sales_Rep__c = bInv.blng__Order__c != null && bInv.blng__Order__r.SBQQ__Quote__c != null ? bInv.blng__Order__r.SBQQ__Quote__r.SBQQ__SalesRep__c : bInv.CreatedbyId;
                }                
                updatedSInvMap.put(newInvoice.Id, newInvoice);
            }
            //Billing Invoice with Sales Credit Note
            if(bInv.JVCO_Sales_Credit_Note__c != null && bInv.JVCO_Sales_Invoice__c == null)
            {
                c2g__codaCreditNote__c newCreditNote = new c2g__codaCreditNote__c();
                newCreditNote.Id = bInv.JVCO_Sales_Credit_Note__r.id;
                if (bInv.JVCO_Sales_Credit_Note__r.c2g__Invoice__r.JVCO_Invoice_Type__c == 'Surcharge')
                {
                    newCreditNote.JVCO_Sales_Rep__c = surUserList[0].Id;
                    newCreditNote.SalesRep_Department__c = surUserList[0].Department;
                    newCreditNote.SalesRep_Role__c = surUserList[0].UserRole.Name;
                    newCreditNote.SalesRep_Profile__c = surUserList[0].Profile.Name;
                    newCreditNote.SalesRep_Manager__c = surUserList[0].Manager.Name; 
                }
                else if(bInv.JVCO_Migrated__c)
                {
                    newCreditNote.JVCO_Sales_Rep__c = bInv.CreatedById;
                }
                else
                {
                    newCreditNote.JVCO_Sales_Rep__c = bInv.blng__Order__c != null && bInv.blng__Order__r.SBQQ__Quote__c != null ? bInv.blng__Order__r.SBQQ__Quote__r.SBQQ__SalesRep__c : bInv.CreatedbyId;
                }
                updatedSCnoteMap.put(newCreditNote.Id, newCreditNote);
            }

            //Billing Invoice with Sales Invoice and Sales Credit Note
            if(bInv.JVCO_Sales_Invoice__c != null && bInv.JVCO_Sales_Credit_Note__c != null)
            {
                c2g__codaInvoice__c newInvoice = new c2g__codaInvoice__c();
                newInvoice.Id = bInv.JVCO_Sales_Invoice__r.id;
                if (bInv.JVCO_Invoice_Type__c == 'Surcharge')
                {
                    newInvoice.JVCO_Sales_Rep__c = surUserList[0].Id;
                    newInvoice.SalesRep_Department__c = surUserList[0].Department;
                    newInvoice.SalesRep_Role__c = surUserList[0].UserRole.Name;
                    newInvoice.SalesRep_Profile__c = surUserList[0].Profile.Name;
                    newInvoice.SalesRep_Manager__c = surUserList[0].Manager.Name; 
                }
                else if(bInv.JVCO_Migrated__c)                    
                {
                    newInvoice.JVCO_Sales_Rep__c = bInv.CreatedById;
                }  
                else
                {  
                    newInvoice.JVCO_Sales_Rep__c = bInv.blng__Order__c != null && bInv.blng__Order__r.SBQQ__Quote__c != null ? bInv.blng__Order__r.SBQQ__Quote__r.SBQQ__SalesRep__c : bInv.CreatedbyId;
                }                
                updatedSInvMap.put(newInvoice.Id, newInvoice);
                
                c2g__codaCreditNote__c newCreditNote = new c2g__codaCreditNote__c();
                newCreditNote.Id = bInv.JVCO_Sales_Credit_Note__r.id;
                if (bInv.JVCO_Sales_Credit_Note__r.c2g__Invoice__r.JVCO_Invoice_Type__c == 'Surcharge')
                {
                    newCreditNote.JVCO_Sales_Rep__c = surUserList[0].Id;
                    newCreditNote.SalesRep_Department__c = surUserList[0].Department;
                    newCreditNote.SalesRep_Role__c = surUserList[0].UserRole.Name;
                    newCreditNote.SalesRep_Profile__c = surUserList[0].Profile.Name;
                    newCreditNote.SalesRep_Manager__c = surUserList[0].Manager.Name; 
                }
                else if(bInv.JVCO_Migrated__c)
                {
                    newCreditNote.JVCO_Sales_Rep__c = bInv.CreatedById;
                }
                else
                {
                    newCreditNote.JVCO_Sales_Rep__c = bInv.blng__Order__c != null && bInv.blng__Order__r.SBQQ__Quote__c != null ? bInv.blng__Order__r.SBQQ__Quote__r.SBQQ__SalesRep__c : bInv.CreatedbyId;
                }
                updatedSCnoteMap.put(newCreditNote.Id, newCreditNote);  
            }
        }
        if(!updatedSInvMap.isEmpty())
        {
            update updatedSInvMap.values();
        }
        if(!updatedSCNoteMap.isEmpty())
        {
            update updatedSCNoteMap.values();
        }
    }
    public void finish(Database.BatchableContext BC){}*/
}