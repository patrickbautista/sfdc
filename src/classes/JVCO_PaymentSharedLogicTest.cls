/**************************************************************
 * @Author: 
 * @Company: Accenture
 * @Description: Test Class for JVCO_PaymentSharedLogic
 * @Created Date:
 * @Revisions:
 *      <Name>              <Date>          <Description>
 *      jason.e.mactal      11.27.2017      Modified to increase coverage
 *************************************************************/
@isTest
private class JVCO_PaymentSharedLogicTest
{
   /*@testSetup 
    static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_FFUtil.stopCodaCreditNoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        
        Test.startTest();
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;

        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        test.stopTest();

        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        List<Order> orderList = new List<Order>();
        orderList.add(o);

        JVCO_TestClassHelper.setGeneralSettingsCS();
        Id paybankId = JVCO_TestClassHelper.getPayBank();
        Id ddMandateId = JVCO_TestClassHelper.getDDMandate(licAcc.Id);
        Id paySchedId = JVCO_TestClassHelper.getPaySched(payBankId);

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        insert bInv;
        blng__InvoiceLine__c bInvLine = JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id,0, false);
        bInvLine.JVCO_Surcharge_Applicable__c = true;
        insert bInvLine;
        Database.executeBatch(new JVCO_OrderMultipleBillNowBatch(orderList));
    }

    @isTest
    static void creditCardRedirectTest()
    {
        Account licAcc = [select id, Name,JVCO_CC_Amount__c, JVCO_Invoice_Group__c,JVCO_Invoice_Group_Id__c from Account where RecordType.Name = 'Licence Account' limit 1];
        Order testOrder = [SELECT ID FROM Order WHERE AccountId =: licAcc.id LIMIT 1];
        OrderItem testOrderItem = [SELECT ID FROM OrderItem WHERE OrderId = :testOrder.id];
        Product2 testProd = [SELECT Id FROM Product2 LIMIT 1];
        Test.startTest();

        list<c2g__codaInvoice__c> selectedInvoices = [select id, name from c2g__codaInvoice__c];

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(selectedInvoices);
        sc.setSelected(selectedInvoices);
        JVCO_CreditCardRedirectLogic ccLogic = new JVCO_CreditCardRedirectLogic(sc);
        try{
            ccLogic.init();
        }catch(exception e){
        }

        test.stopTest();
    }

    @isTest
    static void updateLicenceAccountTest()
    {
        Account licAcc = [select id, Name,JVCO_CC_Amount__c, JVCO_Invoice_Group__c,JVCO_Invoice_Group_Id__c,
                          JVCO_DD_Amount__c, JVCO_Payment_Schedule__c, JVCO_First_Collection_Amount__c, 
                          JVCO_Ongoing_Collection_Amount__c, JVCO_Final_Collection_Amount__c,
                          JVCO_Direct_Debit_Collection_Day__c, JVCO_Direct_Debit_Number_of_Instalments__c
                          from Account where RecordType.Name = 'Licence Account' limit 1];
        Order testOrder = [SELECT ID FROM Order WHERE AccountId =: licAcc.id LIMIT 1];
        OrderItem testOrderItem = [SELECT ID FROM OrderItem WHERE OrderId = :testOrder.id];
        Product2 testProd = [SELECT Id FROM Product2 LIMIT 1];
        Test.startTest();
        list<c2g__codaInvoice__c> selectedInvoices = [SELECT c2g__InvoiceStatus__c, c2g__Account__r.JVCO_Credit_Status__c,
                                 c2g__InvoiceTotal__c, JVCO_Invoice_Group__c, c2g__PaymentStatus__c,
                                 ffps_custRem__In_Dispute__c, JVCO_Invoice_Group__r.Name FROM c2g__codaInvoice__c LIMIT 1]; 
        
        JVCO_PaymentSharedLogic paymentLogic = new JVCO_PaymentSharedLogic(selectedInvoices,JVCO_PaymentSharedLogic.PaymentMode.Debit);
        decimal amt = 100.00;
        paymentLogic.firstColAmt = amt;
        paymentLogic.ongoingColAmt = amt;
        paymentLogic.finalColAmt = amt;
        paymentLogic.paymentSched = [SELECT Name FROM PAYREC2__Payment_Schedule__c LIMIT 1].Name;
        paymentLogic.createInvoiceGroup('Debit', TRUE);
        paymentLogic.updateLicenceAccount([SELECT Id, Name, JVCO_Total_Amount__c FROM JVCO_Invoice_Group__c LIMIT 1], licAcc);
    
        test.stopTest();

        Account accCheck = [SELECT Id, JVCO_First_Collection_Amount__c, JVCO_Ongoing_Collection_Amount__c, JVCO_Final_Collection_Amount__c FROM Account WHERE Id =: licAcc.id];
        system.assertEquals(amt, accCheck.JVCO_First_Collection_Amount__c);
        system.assertEquals(amt, accCheck.JVCO_Ongoing_Collection_Amount__c);
        system.assertEquals(amt, accCheck.JVCO_Final_Collection_Amount__c);
    }

    @isTest
    static void processPaymentDirectDebitTest()
    {

        Account licAcc = [select id, Name,JVCO_CC_Amount__c, JVCO_Invoice_Group__c,JVCO_Invoice_Group_Id__c from Account where RecordType.Name = 'Licence Account' limit 1];
        Order testOrder = [SELECT ID FROM Order WHERE AccountId =: licAcc.id LIMIT 1];
        OrderItem testOrderItem = [SELECT ID FROM OrderItem WHERE OrderId = :testOrder.id];
        Product2 testProd = [SELECT Id FROM Product2 LIMIT 1];
        Group testGroup = [select id from Group limit 1];
        List<RecordType> rtypes1 = [SELECT Name, Id FROM RecordType WHERE sObjectType='PAYBASE2__Payment__c' AND isActive=true];
        c2g__codaBankAccount__c bAcc = [SELECT Id FROM c2g__codaBankAccount__c LIMIT 1];

        Test.startTest();
        
        list<c2g__codaInvoice__c> selectedInvoices = [SELECT c2g__InvoiceStatus__c, c2g__Account__r.JVCO_Credit_Status__c,
                                c2g__InvoiceTotal__c, JVCO_Invoice_Group__c, c2g__PaymentStatus__c,
                                ffps_custRem__In_Dispute__c, JVCO_Invoice_Group__r.Name FROM c2g__codaInvoice__c];    

        JVCO_PaymentSharedLogic paymentLogic = new JVCO_PaymentSharedLogic(selectedInvoices,JVCO_PaymentSharedLogic.PaymentMode.Debit);
        paymentLogic.paymentSched = [SELECT Name FROM PAYREC2__Payment_Schedule__c LIMIT 1].Name;
        JVCO_Invoice_Group__c invGroup = paymentLogic.createInvoiceGroup('SagePay', TRUE);
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> paymentRecordTypes = new Map<String,String>{};
        for(RecordType rt1: rtypes1)
        {
          paymentRecordTypes.put(rt1.Name,rt1.Id);
        }
        
        list<PAYBASE2__Payment__c> paybasePaymentList = new list<PAYBASE2__Payment__c>();
        PAYBASE2__Payment__c paybasePayment = new PAYBASE2__Payment__c();
        paybasePayment.RecordTypeId = paymentRecordTypes.get('SagePay');
        paybasePayment.Name = 'Test Payment 01';
        paybasePayment.PAYBASE2__Status__c = 'AUTHORISED';
        paybasePayment.PAYBASE2__Is_Paid__c = true;
        paybasePayment.PAYBASE2__Amount_Refunded__c = 100;
        paybasePayment.PAYCP2__Payment_Description__c = invGroup.Name;
        paybasePayment.PAYREC2__Is_Actual_Collection__c = true;
        paybasePayment.PAYSPCP1__Redirect_Parent__c= true;
        paybasePayment.PAYSPCP1__Gift_Aid__c= true;
        paybasePayment.PAYBASE2__Amount__c = 10.00;
        paybasePaymentList.add(paybasePayment);
        insert paybasePaymentList;

        JVCO_PaymentSharedLogic.processPayment('SagePay', paybasePaymentList);
        
        Test.stopTest();

        //c2g__codaCashEntryLineItem__c cashEntryPaymentSatusCheck = [SELECT JVCO_Payment_Status__c FROM c2g__codaCashEntryLineItem__c WHERE c2g__CashEntry__c =: cEntry.id LIMIT 1];
        //system.assertEquals('Completed', string.valueOf(cashEntryPaymentSatusCheck));


    }*/
}