/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CaseUtils.cls 
   Description: Utility class for Case object

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  24-Oct-2016   0.1         ryan.i.r.limlingan  Initial creation
  25-Oct-2016   0.2         ryan.i.r.limlingan  Renamed initCustomSettings() to loadCustomSettings()
  												Removed other static variables
  ----------------------------------------------------------------------------------------------- */
public with sharing class JVCO_CaseUtils {

	// Static variables for Custom Settings
	public static JVCO_SLA_Constants__c slaConstants;

    /* ------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: This method loads the Custom Setting to the static variable
    Input: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    24-Oct-2016 ryan.i.r.limlingan  Initial version of function
    24-Oct-2016 ryan.i.r.limlingan  Renamed function and removed other variables
  ----------------------------------------------------------------------------------------------- */
    public static void loadCustomSettings()
    {
    	if (slaConstants == null)
    	{
    		slaConstants = JVCO_SLA_Constants__c.getInstance('JVCO_SLA_Custom_Settings');
    	}
    }

    /* ------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Function that constructs the Map<Id, Datetime> to be used in updating the SLA
    			 Violation time of a milestone
    Input: List<Id>, String
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    24-Oct-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public static void constructCaseIdTargetDateMap(List<Id> caseIdList, String milestoneName,
    												Map<Id, Datetime> idTargetDateMap)
    {
        List<CaseMilestone> milestones = [SELECT Id, CaseId, TargetDate
                                          FROM CaseMilestone
                                          WHERE CaseId IN :caseIdList
                                          AND MilestoneType.Name LIKE :milestoneName];

        for (CaseMilestone cm : milestones)
        {
        	idTargetDateMap.put(cm.CaseId, cm.TargetDate);
        }

    }

}