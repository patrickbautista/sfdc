/* ----------------------------------------------------------------------------------------------
    Name: JVCO_PayonomyPaymentHandler.cls 
    Description: Handler class for PAYBASE2__Payment__c object

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    15-Dec-2016   0.1         ryan.i.r.limlingan  Intial creation
    06-Apr-2018   0.2         mary.ann.a.ruelan   GREEN-29781 - Transferred handling Refer to Payer first and second failure to before update
    01-Jun-2018   0.3         franz.g.a.dimaapi   GREEN-19510 - added linkInvoiceGroupToSalesInvoiceByDescription
    10-Oct-2018   0.5         john.patrick.valdez GREEN-33650 - added unlinkInvoiceGroupCancelled
    26-Nov-2018   0.6         franz.g.a.dimaapi   GREEN-33773 - Added beforeDelete handler
----------------------------------------------------------------------------------------------- */
public class JVCO_PayonomyPaymentHandler
{
    /*public static Boolean stopPaymentDeletion = true;
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_PayonomyPaymentTrigger__c : false;
    private static final Id sagepayRecTypeId = Schema.SObjectType.PAYBASE2__Payment__c.getRecordTypeInfosByName().get('SagePay').getRecordTypeId();
    
    /* ------------------------------------------------------------------------------------------   
        Author: patrick.t.bautista
        Company: Accenture
        Description: This method handles beforeInsert trigger events for PAYBASE2__Payment__c
        Input: List<PAYBASE2__Payment__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        17-Dec-2018 patrick.t.bautista   Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public static void beforeInsert(List<PAYBASE2__Payment__c> newPaymentList)
    {
        if(!skipTrigger) 
        {
            provideEmailAddress(newPaymentList);
        }
    }
    /* ------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: This method handles afterInsert trigger events for PAYBASE2__Payment__c
        Input: List<PAYBASE2__Payment__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        15-Dec-2016 ryan.i.r.limlingan  Initial version of function
        04-Jan-2017 ryan.i.r.limlingan  Modified for Direct Debit payments, instead of Credit Card
        01-Jun-2018 franz.g.a.dimaapi   Fixed GREEN-19510
    ----------------------------------------------------------------------------------------------- */
    /*public static void afterInsert(List<PAYBASE2__Payment__c> newPaymentList)
    {
        if(!skipTrigger) 
        { 
            linkInvoiceGroupToSalesInvoiceByDescription(newPaymentList);
        }
    }

    /* ------------------------------------------------------------------------------------------
        Author: kristoffer.d.martin
        Company: Accenture
        Description: This method handles afterInsert trigger events for PAYBASE2__Payment__c
        Input: List<PAYBASE2__Payment__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        15-Dec-2016 ryan.i.r.limlingan  Initial version of function
        04-Jan-2017 ryan.i.r.limlingan  Modified for Direct Debit payments, instead of Credit Card
        06-Apr-2017 mary.ann.a.ruelan   Transferred updating Refer to Payer first and second failure to before update
    ----------------------------------------------------------------------------------------------- */
    /*public static void afterUpdate(List<PAYBASE2__Payment__c> newPaymentList, Map<Id,PAYBASE2__Payment__c> oldPaymentsMap)
    {
        if(!skipTrigger) 
        {
            JVCO_PayonomyPaymentFailureLogic.processPaymentFailure(newPaymentList);
            updateCancelledInvoiceGroupToRetired(newPaymentList,oldPaymentsMap);
        }       
    }

    /* ------------------------------------------------------------------------------------------   
        Author: mary.ann.a.ruelan
        Company: Accenture
        Description: This method handles beforeUpdate trigger events for PAYBASE2__Payment__c
        Input: List<PAYBASE2__Payment__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Apr-2018 mary.ann.a.ruelan   Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public static void beforeUpdate(List<PAYBASE2__Payment__c> newPaymentList)
    {
        if(!skipTrigger) 
        {           
            updateAgreementCount(newPaymentList);
        }      
    }
    
    /* ------------------------------------------------------------------------------------------   
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: This method handles beforeDelete trigger events for PAYBASE2__Payment__c
        Input: List<PAYBASE2__Payment__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        26-Nov-2018 franz.g.a.dimaapi   GREEN-33773 - Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public static void beforeDelete(List<PAYBASE2__Payment__c> oldPaymentList)
    {
        if(!skipTrigger) 
        {
            preventPaymentDeletion(oldPaymentList);
        }
    }
    
    /* ------------------------------------------------------------------------------------------   
        Author: patrick.t.bautista
        Company: Accenture
        Description: This update needed fields to trigger Email sending when it is valid
        Input: List<PAYBASE2__Payment__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        14-Dec-2018 patrick.t.bautista  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public static void provideEmailAddress(List<PAYBASE2__Payment__c> newPaymentList)
    {
        Set<Id> agreementIdSet = new Set<Id>();
        List<String> paymentDesList = new List<String>();
        Set<id> accountIdSet = new Set<id>();
        for(PAYBASE2__Payment__c payment : newPaymentList)
        {
            if(payment.PAYCP2__Payment_Description__c != NULL && payment.PAYBASE2__Created_From_Account__c != NULL
               && payment.RecordTypeId == sagepayRecTypeId)
            {
                paymentDesList.add(payment.PAYCP2__Payment_Description__c);
                accountIdSet.add(payment.PAYBASE2__Created_From_Account__c);
            }
        }
        Map<id, Account> acctToEmailMap = new Map<id,Account>([SELECT id, JVCO_Billing_Contact__r.Email 
                                                               FROM Account 
                                                               WHERE id IN : accountIdSet
                                                               AND JVCO_Billing_Contact__c != NULL]);
        //Collection of records process by Credit/Debit payments
        Map<String, String> agreementOrDescriptionToCCSINsMap = new Map<String, String>();
        for(JVCO_Invoice_Group__c invGrp : [SELECT id, Name, CC_SINs__c, JVCO_Payment_Agreement__c
                                            FROM JVCO_Invoice_Group__c
                                            WHERE Name IN : paymentDesList
                                           ])
        {
            //Store when it is process by Credit Card
            agreementOrDescriptionToCCSINsMap.put(invGrp.Name, invGrp.CC_SINs__c);
        }
        for(PAYBASE2__Payment__c payment : newPaymentList)
        {
            if(payment.PAYBASE2__Created_From_Account__c != NULL
               && acctToEmailMap.containsKey(payment.PAYBASE2__Created_From_Account__c) 
               && payment.PAYCP2__Payment_Description__c != NULL
               && agreementOrDescriptionToCCSINsMap.containsKey(payment.PAYCP2__Payment_Description__c) 
               && payment.EmailForReceipt__c == NULL)
            {
                //Update when the process is Credit Card
                payment.EmailForReceipt__c = payment.PAYCP2__Email__c == NULL && 
                    acctToEmailMap.get(payment.PAYBASE2__Created_From_Account__c).JVCO_Billing_Contact__r.Email != NULL? 
                    acctToEmailMap.get(payment.PAYBASE2__Created_From_Account__c).JVCO_Billing_Contact__r.Email 
                    : payment.PAYCP2__Email__c;
                if(payment.JVCO_All_Sales_Invoice__c == NULL)
                {
                    payment.JVCO_All_Sales_Invoice__c = agreementOrDescriptionToCCSINsMap.get(payment.PAYCP2__Payment_Description__c).replaceAll(',',', ').removeEnd(', ');
                }
            }
        }
    }
    /* ------------------------------------------------------------------------------------------   
        Author: mary.ann.a.ruelan
        Company: Accenture
        Description: This updates agreement.Count_0_Fail__c and payment.PAYBASE2__Status_Text__c for the Refer to Payer/ARUDD 0 first and second failure
        Input: List<PAYBASE2__Payment__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Apr-2018 mary.ann.a.ruelan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public static void updateAgreementCount(List<PAYBASE2__Payment__c> paymentList)
    {
        List<id> payAgrIdList = new List<id>();
        List<PAYBASE2__Payment__c> arudd0paymentsList = new List<PAYBASE2__Payment__c>();
        for (PAYBASE2__Payment__c p : paymentList)
        {
            //get payments and agreement only for status text = Refer to Payer or ARUDD 0 
            if(p.PAYREC2__Payment_Agreement__c != null && (p.PAYBASE2__Status_Text__c == 'ARUDD 0' ||  p.PAYBASE2__Status_Text__c == 'Refer to Payer'))
            {
                payAgrIdList.add(p.PAYREC2__Payment_Agreement__c);
                arudd0paymentsList.add(p);
            }
        }
        if(!payAgrIdList.isEmpty())
        {
            Map<Id,PAYREC2__Payment_Agreement__c> IdToPayAgrMap =  new Map<Id, PAYREC2__Payment_Agreement__c>([Select Count_0_Fail__c FROM PAYREC2__Payment_Agreement__c WHERE id IN :payAgrIdList]);                  
            for(PAYBASE2__Payment__c p : arudd0paymentsList)
            {                
                if(IdToPayAgrMap.get(p.PAYREC2__Payment_Agreement__c).Count_0_Fail__c==NULL)
                {
                    //null handling
                    IdToPayAgrMap.get(p.PAYREC2__Payment_Agreement__c).Count_0_Fail__c=0;
                }
                else if(IdToPayAgrMap.get(p.PAYREC2__Payment_Agreement__c).Count_0_Fail__c > 0)
                {
                    p.PAYBASE2__Status_Text__c = 'ARUDD 0 (2nd fail)';
                }                    
                IdToPayAgrMap.get(p.PAYREC2__Payment_Agreement__c).Count_0_Fail__c+=1;             
            }
            update IdToPayAgrMap.values();            
        }
    }
    
    /* ------------------------------------------------------------------------------------------   
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Link Invoice Group to Sales Invoice
        Input: List<PAYBASE2__Payment__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        01-Jun-2018 franz.g.a.dimaapi   Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*private static void linkInvoiceGroupToSalesInvoiceByDescription(List<PAYBASE2__Payment__c> paymentList)
    {
        List<String> invoiceGroupNameList = new List<String>();
        for(PAYBASE2__Payment__c payment : paymentList)
        {
            if(payment.RecordTypeId == sagepayRecTypeId)
            {
                invoiceGroupNameList.add(payment.PAYCP2__Payment_Description__c);
            }
        }
        
        if(!invoiceGroupNameList.isEmpty())
        {
            Map<Id, String> igIdToSInvNamesMap = new Map<Id, String>();
            for(JVCO_Invoice_Group__c ig : [SELECT Id, Name, CC_SINs__c 
                                            FROM JVCO_Invoice_Group__c
                                            WHERE Name IN :invoiceGroupNameList])
            {
                if(ig.CC_SINs__c != null)
                {
                    igIdToSInvNamesMap.put(ig.Id, ig.CC_SINs__c);
                }    
            }
            
            if(!igIdToSInvNamesMap.isEmpty())
            {
                Map<String, Id> sInvNameToInvoiceGroupIdMap = new Map<String, Id>();
                for(Id igId: igIdToSInvNamesMap.keySet())
                {
                    String sInvNameList = igIdToSInvNamesMap.get(igId);
                    for(String sInvName : sInvNameList.split(','))
                    {
                        sInvNameToInvoiceGroupIdMap.put(sInvName, igId);
                    }                                
                }
                
                List<c2g__codaInvoice__c> sInvList = [SELECT Id, Name, 
                                                      JVCO_Invoice_Group__c
                                                      FROM c2g__codaInvoice__c
                                                      WHERE Name IN : sInvNameToInvoiceGroupIdMap.keySet()];
                for(c2g__codaInvoice__c sInv : sInvList)
                {
                    sInv.JVCO_Invoice_Group__c = sInvNameToInvoiceGroupIdMap.get(sInv.Name);
                }
                
                if(!sInvList.isEmpty())
                {
                    update sInvList;
                }
            }
        }    
    }
    
    /* ------------------------------------------------------------------------------------------   
    Author: john.patrick.valdez
    Company: Accenture
    Description: GREEN-33650 - Delete Invoice Group (GIN) if payment aborted part way through
    Input: List<PAYBASE2__Payment__c>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    09-Oct-2018 john.patrick.valdez   Initial version of function
    18-Oct-2018 john.patrick.valdez   Delete invoice group insted of updating sales invoice only
    06-Mar-2019 patrick.t.bautista    GREEN-34320 - Change functionality instead of deleting invoice
                                      group change status to retired
    ----------------------------------------------------------------------------------------------- */
    /*private static void updateCancelledInvoiceGroupToRetired(List<PAYBASE2__Payment__c> paymentToList, Map<id,PAYBASE2__Payment__c> oldpaymentMap)
    {   
        Set<String> invGrpSet = new Set<String>();
        for(PAYBASE2__Payment__c payment : paymentToList)
        {
            if(payment.PAYCP2__Payment_Description__c != '' && payment.PAYCP2__Payment_Description__c != NULL
               && payment.RecordTypeId == sagepayRecTypeId && payment.PAYBASE2__Status__c != oldpaymentMap.get(payment.id).PAYBASE2__Status__c
               && payment.PAYBASE2__Status__c == 'CANCELLED') 
            {
                invGrpSet.add(payment.PAYCP2__Payment_Description__c);
            }
        }
        //Update invoice group to retired status
        List<JVCO_Invoice_Group__c> invGrpToUpdateList = new List<JVCO_Invoice_Group__c>();
        for(JVCO_Invoice_Group__c invGrp: [SELECT Id, Name, JVCO_Status__c
                                                       FROM JVCO_Invoice_Group__c
                                                       WHERE Name IN: invGrpSet])
        {
            invGrp.JVCO_Status__c = 'Retired';
            invGrpToUpdateList.add(invGrp);
        }
        if(!invGrpToUpdateList.isEmpty())
        {
            update invGrpToUpdateList;
        }
    }
    
    /* ------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: This method prevents the deletion of the Payonomy Payment records.
        Input: List<PAYBASE2__Payment__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        26-Nov-2018 franz.g.a.dimaapi   GREEN-33773 - Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*private static void preventPaymentDeletion(List<PAYBASE2__Payment__c> paymentList)
    {
        if(stopPaymentDeletion)
        {
            List<Profile> profileList = [SELECT Id FROM Profile 
                                        WHERE Id = :UserInfo.getProfileId()
                                        AND Name IN ('System Administrator','Finance')];
            if(profileList.isEmpty())
            {
                for(PAYBASE2__Payment__c payment : paymentList)
                {
                    payment.addError('You Cannot Delete Payment');
                }
            }
        }
    }*/
}