@isTest
public class JVCO_ApexJobProgressBarControllerTest {
    @testSetup static void setupTestData() 
    {
        SBQQ.TriggerControl.disable();
        Product2 prod1 = new Product2();
        prod1.Name = 'Test Product 18023-1';
        prod1.ProductCode = 'TP 18023-1';
        prod1.IsActive = true;
        prod1.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        prod1.SBQQ__SubscriptionTerm__c = 12;
        insert prod1;

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        JVCO_Venue__c v1 = JVCO_TestClassObjectBuilder.createVenue();
        v1.Name = 'Some Venue for Testing 1';
        insert v1;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 18023-C';
        a1.Type = 'Key Account';
        insert a1;
        
        Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
        a2.RecordTypeId = licenceRT;
        a2.Name = 'Test Account 18023-L';
        a2.JVCO_Customer_Account__c = a1.Id;
        a2.Type = 'Key Account';
        insert a2;
        

        
        JVCO_Affiliation__c aff1 = new JVCO_Affiliation__c();
        aff1.Name = 'Some Aff 1';
        aff1.JVCO_Account__c = a2.Id;
        aff1.JVCO_Venue__c = v1.Id;
        aff1.JVCO_Start_Date__c = System.today() - 10;
        aff1.JVCO_Status__c = 'Trading';
        aff1.JVCO_Venue_Name__c = 'Some Venue for Testing 1';
        insert aff1;

        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 18023';
        o.AccountId = a2.Id;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Amendment';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__SubscriptionTerm__c = 12;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.Start_Date__c = System.today() - 5;
        q.End_Date__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        q.SBQQ__SubscriptionTerm__c = 12;
        q.SBQQ__MasterContract__c = null;
        insert q;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        insert ql;

        
        Contract c1 = new Contract();
        c1.JVCO_Renewal_Generated__c = false;  
        c1.AccountID = a2.Id;
        c1.StartDate = System.today() - 5;
        c1.ContractTerm = 12;
        c1.EndDate = System.today().addMonths(12);
        c1.Name = 'Test Contract 18023-L';
        c1.ForceDeferred__c = 1;
        c1.SBQQ__Quote__c = q.Id;
        c1.SBQQ__Opportunity__c = o.Id;
        c1.AccountId = a2.Id;
        insert c1;    
        
    }
    
        private static testMethod void testMethodJVCO_ApexJobProgressBarController4 () 
    { 
       Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        JVCO_ApexJobProgressBarController progbar = new JVCO_ApexJobProgressBarController();
        progbar.batchIdString = database.executeBatch(new JVCO_GenerateAmendmentQuotesBatch(a), 1);
        
        
        Test.startTest();
        progbar.getJobs();
        Test.stopTest();
        
    } 
    
    private static testMethod void testMethodJVCO_ApexJobProgressBarController () 
    {    
        Test.startTest();
        JVCO_ApexJobProgressBarController progbar = new JVCO_ApexJobProgressBarController();
        Test.stopTest();
        system.assertEquals('not_started', progbar.batchStatus);
    }
    
    private static testMethod void testMethodJVCO_ApexJobProgressBarController2 ()
    {
        Test.startTest();
        JVCO_ApexJobProgressBarController progbar = new JVCO_ApexJobProgressBarController();
        progbar.batchStatus = 'processing';
        progbar.getShowProgressBar();
        Test.stopTest();
        system.assertEquals('processing', progbar.batchStatus);
    }
    
       private static testMethod void testMethodJVCO_ApexJobProgressBarController3 ()
    {
        Test.startTest();
        JVCO_ApexJobProgressBarController progbar = new JVCO_ApexJobProgressBarController();
        progbar.batchStatus = 'finished';
        progbar.getShowProgressBar();
        Test.stopTest();
        system.assertNotEquals('processing', progbar.batchStatus);
    }
    

    
    private static testMethod void testMethodJVCO_ApexJobProgressBarController5 () 
    {
       JVCO_ApexJobProgressBarController progbar = new JVCO_ApexJobProgressBarController();
       progbar.batchStatus = 'FINISHED';
        Test.startTest();
        progbar.updateProgress();
        Test.stopTest();
        system.assertEquals('COMPLETED', progbar.message);
    }
    
    private static testMethod void testMethodCheckJob(){

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        JVCO_ApexJobProgressBarController progbar = new JVCO_ApexJobProgressBarController();
        Test.startTest();
        progbar.batchIdString = database.executeBatch(new JVCO_GenerateAmendmentQuotesBatch(a), 1);
        Test.stopTest();
        a.JVCO_ContractRenewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_ContractReviewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_GenerateRenewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_GenerateReviewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_OrderRenewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_OrderReviewQuotesApexJob__c = progbar.batchIdString;
        update a;
        progbar.updateAcc(progbar.batchIdString, a.Id);
    }

    private static testMethod void testMethodCheckJob2(){

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        JVCO_ApexJobProgressBarController progbar = new JVCO_ApexJobProgressBarController();
        Test.startTest();
        progbar.batchIdString = database.executeBatch(new JVCO_GenerateAmendmentQuotesBatch(a), 1);
        Test.stopTest();
        a.JVCO_ContractReviewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_GenerateRenewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_GenerateReviewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_OrderRenewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_OrderReviewQuotesApexJob__c = progbar.batchIdString;
        update a;
        progbar.updateAcc(progbar.batchIdString, a.Id);
    }

    private static testMethod void testMethodCheckJob3(){

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        JVCO_ApexJobProgressBarController progbar = new JVCO_ApexJobProgressBarController();
        Test.startTest();
        progbar.batchIdString = database.executeBatch(new JVCO_GenerateAmendmentQuotesBatch(a), 1);
        Test.stopTest();
        a.JVCO_GenerateRenewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_GenerateReviewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_OrderRenewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_OrderReviewQuotesApexJob__c = progbar.batchIdString;
        update a;
        progbar.updateAcc(progbar.batchIdString, a.Id);
    }

    private static testMethod void testMethodCheckJob4(){

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        JVCO_ApexJobProgressBarController progbar = new JVCO_ApexJobProgressBarController();
        Test.startTest();
        progbar.batchIdString = database.executeBatch(new JVCO_GenerateAmendmentQuotesBatch(a), 1);
        Test.stopTest();
        a.JVCO_GenerateReviewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_OrderRenewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_OrderReviewQuotesApexJob__c = progbar.batchIdString;
        update a;
        progbar.updateAcc(progbar.batchIdString, a.Id);
    }

    private static testMethod void testMethodCheckJob5(){

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        JVCO_ApexJobProgressBarController progbar = new JVCO_ApexJobProgressBarController();
        Test.startTest();
        progbar.batchIdString = database.executeBatch(new JVCO_GenerateAmendmentQuotesBatch(a), 1);
        Test.stopTest();
        a.JVCO_OrderRenewQuotesApexJob__c = progbar.batchIdString;
        a.JVCO_OrderReviewQuotesApexJob__c = progbar.batchIdString;
        update a;
        progbar.updateAcc(progbar.batchIdString, a.Id);
    }

    private static testMethod void testMethodCheckJob6(){

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        JVCO_ApexJobProgressBarController progbar = new JVCO_ApexJobProgressBarController();
        Test.startTest();
        progbar.batchIdString = database.executeBatch(new JVCO_GenerateAmendmentQuotesBatch(a), 1);
        Test.stopTest();
        a.JVCO_OrderReviewQuotesApexJob__c = progbar.batchIdString;
        update a;
        progbar.updateAcc(progbar.batchIdString, a.Id);
    }   
}