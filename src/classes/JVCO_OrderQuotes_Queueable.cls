/* ------------------------------------------------------------------------------------------------
   Name:            JVCO_OrderQuotes_Queueable
   Description:     Queueable implementation for JVCO_OrderQuotesBatch

   Date            Version     Author            		    Summary of Changes 
   -----------     -------     -----------------            ---------------------------------------
   09-Jul-2018     0.1         jules.osberg.a.pablo 		Initial Version
   27-Sept-2018    0.2         rhys.j.c.dela.cruz           GREEN-33618 - Additional changes to imrpove Queueable, Added Queueable Status
------------------------------------------------------------------------------------------------ */
public class JVCO_OrderQuotes_Queueable implements Queueable {
	private Map<Id, Opportunity> opportunityMap;
	private Account accountRecord;
	//New Map
	private Map<String, String> quoteStringMap = new Map<String, String>();
	private Integer tempOriginalSize;
	private Boolean isReview;
	private Integer qblTempCtr = 0;

	public JVCO_OrderQuotes_Queueable(Account accountRecord, Set<Id> opportunitySetId, Boolean isReview) {
		opportunityMap = new Map<Id, Opportunity>((List<Opportunity>)Database.query(JVCO_OrderQuotesHelper.getQueryString(accountRecord, opportunitySetId)));
		this.accountRecord = accountRecord;
		tempOriginalSize = opportunityMap.size();
		this.isReview = isReview;
	}

	public JVCO_OrderQuotes_Queueable(Map<Id, Opportunity> opportunityMap, Account accountRecord, Map<String, String> quoteStringMap, Integer tempOriginalSize, Boolean isReview, Integer qblTempCtr) {
		this.opportunityMap = opportunityMap;
		this.accountRecord = accountRecord;
		this.quoteStringMap = quoteStringMap;
		this.tempOriginalSize = tempOriginalSize;
		this.isReview = isReview;
		this.qblTempCtr = qblTempCtr;
	}

	public void execute(QueueableContext context) {

        //28-June-2019 rhys.j.dela.cruz GREEN-33618 - New field on Account to Abort Running Queueable Job
        List<Account> accForAbort = [SELECT Id, JVCO_AbortRunningQueueable__c, JVCO_ContractRenewQuotesQueueable__c FROM Account WHERE Id =: accountRecord.Id LIMIT 1];
        Boolean abortJob = false;

        if(!accForAbort.isEmpty()){
            abortJob = accForAbort[0].JVCO_AbortRunningQueueable__c;
        }

        if(!opportunityMap.isEmpty()) {
        	Opportunity opportunityRecord = opportunityMap.values()[0];
	        System.debug('@@opportunityRecord: ' + opportunityRecord);
	        List<Opportunity> tempOpportunityList = new List<Opportunity>();
	        tempOpportunityList.add(opportunityMap.values()[0]);
	        Map<String, String> tempQuoteMap = new Map<String, String>();

	        tempQuoteMap.putAll(JVCO_OrderQuotesHelper.executeOrderQuotes(tempOpportunityList, quoteStringMap));
	        quoteStringMap.putAll(tempQuoteMap);

	        opportunityMap.remove(opportunityRecord.Id);
        }


        if(abortJob){

            JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
            kaTempRec.JVCO_ErrorMessage__c = 'Queueable Job Aborted: ' + System.now();

            accForAbort[0].JVCO_AbortRunningQueueable__c = false;

            if(isReview){

                kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractReview';
                accForAbort[0].JVCO_OrderReviewQuotesQueueable__c = '';
            }
            else{

                kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractRenew';
                accForAbort[0].JVCO_OrderRenewQuotesQueueable__c = '';
            }

            upsert kaTempRec Name;
            update accForAbort[0];
        }
        else if(!opportunityMap.isEmpty() && !abortJob){

        	//On 4th Queueable, instead of calling again, this will schedule the class instead
        	if(qblTempCtr < 3){	
        		qblTempCtr++;

        		if(!Test.isRunningTest()){

        			Id queueableId = System.enqueueJob(new JVCO_OrderQuotes_Queueable(opportunityMap, accountRecord, quoteStringMap, tempOriginalSize, isReview, qblTempCtr));

        			JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
                    if(isReview){
                        kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractReview';
                    }
                    else{
                        kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractRenew';   
                    }
                    kaTempRec.JVCO_JobId__c = queueableId;
                    kaTempRec.JVCO_ErrorMessage__c = '';

                    upsert kaTempRec Name;
        		}
        	}
        	else{

                JVCO_KeyAccountHelperSchedulable kaHelperSched = new JVCO_KeyAccountHelperSchedulable(opportunityMap, accountRecord, quoteStringMap, tempOriginalSize, isReview, 'OrderQuotes');

                Integer schedulerBuffer = (Integer)JVCO_KABatchSetting__c.getOrgDefaults().JVCO_SchedulerBuffer__c != null ? (Integer)JVCO_KABatchSetting__c.getOrgDefaults().JVCO_SchedulerBuffer__c : 45;

                Datetime dt = DateTime.now().addSeconds(schedulerBuffer);
                String cronExp = '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();

                while(dt <= DateTime.now()){

                    dt = dt.addSeconds(schedulerBuffer);
                }

                Id schedId = System.schedule('Key Account Helper Sched: Order Quotes ' + accountRecord.Id, cronExp, kaHelperSched);

                JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
                if(isReview){
                    kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractReview';
                }
                else{
                    kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractRenew';   
                }
                kaTempRec.JVCO_JobId__c = schedId;
                kaTempRec.JVCO_ErrorMessage__c = '';

                upsert kaTempRec Name;
            }
        } 
        else{
        	JVCO_OrderQuotesHelper.sendOrderingEmail(accountRecord, tempOriginalSize, true, false, quoteStringMap, isReview);

        	//Delete all KA Temp Records
        	List<JVCO_KATempHolder__c> kaTempList = [SELECT Id FROM JVCO_KATempHolder__c WHERE JVCO_AccountId__c =: accountRecord.Id];

        	if(!kaTempList.isEmpty()){

        		delete kaTempList;
        	}
        }
	}
}