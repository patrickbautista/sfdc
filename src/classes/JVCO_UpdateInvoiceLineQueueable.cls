public class JVCO_UpdateInvoiceLineQueueable implements Queueable
{
    private final JVCO_General_Settings__c generalSettings = JVCO_General_Settings__c.getInstance();
    private final Decimal BATCHRECORDLIMIT = generalSettings.JVCO_UpdateInvoiceLineQueueable_Limit__c;
    private Map<Id, OrderItem> updatedOrderItemMap;
    private Map<Integer, List<OrderItem>> batchNumberToBInvLineListMap;
    private Integer batchNumber = 1;

    public JVCO_UpdateInvoiceLineQueueable(Map<Id, OrderItem> updatedOrderItemMap)
    {
        this.updatedOrderItemMap = updatedOrderItemMap;
        batchNumberToBInvLineListMap = new Map<Integer, List<OrderItem>>();
    }

    public JVCO_UpdateInvoiceLineQueueable(Map<Integer, List<OrderItem>> batchNumberToBInvLineListMap, Integer batchNumber)
    {
        this.batchNumberToBInvLineListMap = batchNumberToBInvLineListMap;
        this.batchNumber = batchNumber;
    }

    public void execute(QueueableContext context)
    {
        if(updatedOrderItemMap != null)
        {
            Integer batchNumberCounter = 1;
            Integer recordSize = 0;
            for(OrderItem ordLine : updatedOrderItemMap.values())
            {
                if(recordSize == BATCHRECORDLIMIT)
                {
                    batchNumberCounter++;
                    recordSize = 0;
                }

                recordSize++;
                if(!batchNumberToBInvLineListMap.containsKey(batchNumberCounter))
                {
                    batchNumberToBInvLineListMap.put(batchNumberCounter, new List<OrderItem>());
                }
                batchNumberToBInvLineListMap.get(batchNumberCounter).add(ordLine);
            }
        }
        
        update batchNumberToBInvLineListMap.get(batchNumber);
        if(++batchNumber <= batchNumberToBInvLineListMap.size())
        {
            System.enqueueJob(new JVCO_UpdateInvoiceLineQueueable(batchNumberToBInvLineListMap, batchNumber));
        }
    }
}