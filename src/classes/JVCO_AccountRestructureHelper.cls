/* -------------------------------------------------------------------------------------
    Name: JVCO_AccountRestructureHelper
    Description: Helper class for JVCO_AccountRestructureBatch

    Date            Version     Author                Summary of Changes 
    -----------     -------     -----------------     ----------------------------------
    10-May-2017     0.1         jules.osberg.a.pablo  Intial creation
------------------------------------------------------------------------------------- */
public class JVCO_AccountRestructureHelper {
    public JVCO_AccountRestructureHelper(){

    }


    public string getQueryString(Id CustomerAccountId){
        return 'SELECT Id, JVCO_Account__c,JVCO_NewAccountId__c FROM JVCO_Affiliation__c WHERE JVCO_Account__r.JVCO_Customer_Account__c = '+ '\'' + (string)CustomerAccountId + '\'' + 'AND JVCO_NewAccountId__c != null ORDER BY JVCO_Account__c';
    }

    public List<JVCO_Affiliation__c> runValidationsOnAffiliations(List<JVCO_Affiliation__c> affiliationList, Id customerAccountId){
        List<JVCO_Affiliation__c> validAffiliationList = new List<JVCO_Affiliation__c>(); 
        List<JVCO_Affiliation__c> inValidAffiliationList = new List<JVCO_Affiliation__c>(); 
        Set<String> newAccountIdSet = new Set<String>();
        Set<String> validAccIdTestSet = new Set<String>();
        Set<String> invalidAccIdTestSet = new Set<String>();

        for(JVCO_Affiliation__c aff :affiliationList){          
            newAccountIdSet.add(String.valueOf(aff.JVCO_NewAccountId__c).substring(0, 15));
        }

        for(Account licAcc :[SELECT Id FROM Account WHERE JVCO_Live__c = false AND JVCO_Customer_Account__c = :customerAccountId AND Id IN :newAccountIdSet]){
            validAccIdTestSet.add(String.valueOf(licAcc.Id).substring(0, 15));
        }
 
        if(newAccountIdSet.size() != validAccIdTestSet.size()){
            invalidAccIdTestSet = newAccountIdSet.clone();
            System.debug('### invalidAccIdTestSet:' + invalidAccIdTestSet);
            System.debug('### validAccIdTestSet:' + validAccIdTestSet);
            invalidAccIdTestSet.removeall(validAccIdTestSet);
            System.debug('### invalidAccIdTestSet:' + invalidAccIdTestSet);

            for(JVCO_Affiliation__c aff :affiliationList){
                if(invalidAccIdTestSet.contains(aff.JVCO_NewAccountId__c)){
                    JVCO_AccountRestructureBatch.invalidAccountIdMap.put(aff.Id, aff.JVCO_NewAccountId__c);
                    inValidAffiliationList.add(aff);
                }
            }
        }

        for(JVCO_Affiliation__c aff :affiliationList){
            if(validAccIdTestSet.contains(String.valueOf(aff.JVCO_NewAccountId__c).substring(0, 15)) && !invalidAccIdTestSet.contains(String.valueOf(aff.JVCO_NewAccountId__c).substring(0, 15))){
                validAffiliationList.add(aff);
            }
        }

        System.debug('### Invalid Id:' + invalidAccIdTestSet);
        System.debug('### Valid Id:' + validAffiliationList);

        return validAffiliationList;
    }

    public String processValidAffiliations(List<JVCO_Affiliation__c> affiliationList, Id customerAccountId){
        String errorMessage = '';
        Set<Id> affiliationIdSet = new Set<Id>();
        Set<Id> quoteLineIdSet = new Set<Id>();
        Set<Id> quoteLineGroupIdSet = new Set<Id>();
        Set<Id> quoteIdSet = new Set<Id>();
        Set<Id> opportunityIdSet = new Set<Id>();
        Set<Id> contactIdSet = new Set<Id>();
        Set<Id> newAccountIdSet = new Set<Id>();

        Map<Id, JVCO_Affiliation__c> newAccountIdMap = new Map<Id, JVCO_Affiliation__c>();
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>(); 

        List<sObject> lstObject = new List<sObject>();

        for(JVCO_Affiliation__c aff :affiliationList){
            affiliationIdSet.add(Id.valueOf(aff.Id));
            Id accountKeyId = aff.JVCO_Account__c;
            newAccountIdSet.add(Id.valueOf(aff.JVCO_NewAccountId__c));
            aff.JVCO_Account__c = Id.valueOf(aff.JVCO_NewAccountId__c);
            aff.JVCO_NewAccountId__c = '';
            aff.JVCO_IsAccountRestructure__c = true;

            //updateAffiliationList.add(aff);
            lstObject.add((sObject)aff);
            newAccountIdMap.put(accountKeyId, aff);
            
        }

        List<SBQQ__QuoteLine__c> quoteLineList = [SELECT Id, SBQQ__Group__c, SBQQ__Quote__c FROM SBQQ__QuoteLine__c WHERE Affiliation__c IN :affiliationIdSet AND (SBQQ__Quote__r.SBQQ__Status__c = 'Draft' OR SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Renewal_Generated__c = false) AND SBQQ__Quote__r.JVCO_Cust_Account__c = :customerAccountId]; 
        for(SBQQ__QuoteLine__c ql :quoteLineList){
            quoteLineGroupIdSet.add(ql.SBQQ__Group__c);
            quoteIdSet.add(ql.SBQQ__Quote__c);
        }

        List<SBQQ__QuoteLineGroup__c> quoteLineGroupList = [SELECT Id, SBQQ__Account__c FROM SBQQ__QuoteLineGroup__c WHERE Id IN :quoteLineGroupIdSet]; 
        for(SBQQ__QuoteLineGroup__c qlg :quoteLineGroupList){
            qlg.SBQQ__Account__c = newAccountIdMap.get(qlg.SBQQ__Account__c).JVCO_Account__c;
            lstObject.add((sObject)qlg);
        }

        List<SBQQ__Quote__c> quoteList = [SELECT Id, SBQQ__Opportunity2__c, SBQQ__Account__c FROM SBQQ__Quote__c WHERE Id IN :quoteIdSet];
        for(SBQQ__Quote__c quote :quoteList){
            opportunityIdSet.add(quote.SBQQ__Opportunity2__c);
            quote.SBQQ__Account__c = newAccountIdMap.get(quote.SBQQ__Account__c).JVCO_Account__c;
            lstObject.add((sObject)quote);
        }

        List<Opportunity> opportunityList = [SELECT Id, AccountId FROM Opportunity WHERE Id IN :opportunityIdSet];
        for(Opportunity opp :opportunityList){
            opp.AccountId = newAccountIdMap.get(opp.AccountId).JVCO_Account__c;
            lstObject.add((sObject)opp);
        }

        List<SBQQ__Subscription__c> subscriptionList = [SELECT Id, SBQQ__Contract__c, SBQQ__Account__c FROM SBQQ__Subscription__c WHERE Affiliation__c IN :affiliationIdSet AND SBQQ__Contract__r.JVCO_Renewal_Generated__c = false AND SBQQ__Account__r.JVCO_Customer_Account__c = :customerAccountId]; 
        
        
        
        for(SBQQ__Subscription__c sub :subscriptionList){
            contactIdSet.add(sub.SBQQ__Contract__c);
            sub.SBQQ__Account__c = newAccountIdMap.get(sub.SBQQ__Account__c).JVCO_Account__c;
            lstObject.add((sObject)sub);
        }
    
        List<Contract> contractList = [SELECT Id, AccountId FROM Contract WHERE Id IN :contactIdSet];
        for(Contract cont :contractList){
            cont.AccountId = newAccountIdMap.get(cont.AccountId).JVCO_Account__c;
            lstObject.add((sObject)cont);
        }

        Savepoint sp = Database.setSavepoint();
        //if(Test.isRunningTest()) { 
        //    lstObject.add(new SBQQ__Quote__c());
        //}
        List<Database.SaveResult> res = Database.update(lstObject,false); 
        for(Integer i = 0; i < lstObject.size(); i++) {
            Database.SaveResult srOppList = res[i];
            sObject origrecord = lstObject[i];
            if(!srOppList.isSuccess()) {
                Database.rollback(sp);
                System.debug('Update sObject fail: ');
                for(Database.Error objErr:srOppList.getErrors()) {
                    ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();

                    /*
                    errLog.Name = String.valueOf(origrecord.ID);
                    errLog.JVCO_FailedRecordObjectReference__c = String.valueOf(origrecord.ID);
                    errLog.blng__FullErrorLog__c = string.valueof(objErr.getMessage());
                    errLog.blng__ErrorCode__c = string.valueof(objErr.getStatusCode());
                    errLog.JVCO_ErrorBatchName__c = 'JVCO_AccountRestructureBatch: Update Record';
                    */       
                    errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(origrecord.ID);
                    errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                    errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                    errLog.ffps_custRem__Message__c = 'JVCO_AccountRestructureBatch: Update Record';

                    errorMessage += '<li>Record: ' + String.valueOf(origrecord.ID) 
                                  + '<br>Status Code: '  + string.valueof(objErr.getStatusCode()) 
                                  + '<br>Cause: '+ string.valueof(objErr.getMessage())+'</li>';
                    errLogList.add(errLog);   

                }
            }
        }

        if(!errLogList.isempty()) {
            try {
                insert errLogList;
            } catch(Exception e) {
                errorMessage = '<li>ErrorLineNumber: '; 
                errorMessage += e.getLineNumber() != Null ? String.ValueOf(e.getLineNumber()):'None';
                errorMessage += '<br>Cause: ' + e.getCause() + '<br>ErrMsg: ' + e.getMessage() + '</li>';
                System.debug('## Error Message: ' +errorMessage);      
            }
        }

        return errorMessage;
    }
}