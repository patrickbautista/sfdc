/* ----------------------------------------------------------------------------------------------
    Name: JVCO_InvoiceGenerationLogic.cls 
    Description: Logic class for Invoice cancellation

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    31-Jul-2017   0.1        franz.g.a.dimaapi   Intial creation
    15-Aug-2017   0.2        franz.g.a.dimaapi   Moved BackgroundMatching Email Function
    13-Dec-2017   0.3        john.patrick.valdez Removed Email Function
    08-Dec-2018   0.4        filip.bezak@accenture.com - GREEN-27940 - added 2 new logic stoppers
    30-Oct-2020   0.5        sean.g.perez        GREEN-35801 - Issue with Card Payment History not generating on a payment using PCI Pal
 ----------------------------------------------------------------------------------------------- */
public class JVCO_FFUtil
{

    //Map Account Name to Tax Registration Number
    private static Map<String, String> taxRegNumMap;

    //Handler Stopper
    public static Boolean stopAccountDLRSHandler = false;
    public static Boolean stopAccountHandlerAfterUpdate = false;
    public static Boolean stopAccountHandlerBeforeUpdate = false;
    public static Boolean stopBlngInvoiceDLRSHandler = true;
    public static Boolean stopBlngInvoiceHandlerAfterUpdate = true;
    public static Boolean stopBlngInvoiceLineHandlerBeforeInsert = false;
    public static Boolean stopCardPaymentSMPAfterInsert = false;    
    public static Boolean stopCodaCreditNoteHandlerAfterUpdate = false;
    public static Boolean stopCodaInvoiceDLRSHandler = false;
    public static Boolean stopCodaInvoiceHandlerAfterUpdate = false;
    public static Boolean stopCodaTransasctionLineItemHandlerAfterUpdate = false;
    public static Boolean stopCodaTransasctionHandlerAfterUpdate = false;
    public static Boolean stopCodaTransactionHandlerSMP = false;
    public static Boolean stopContractHandlerAfterUpdate = false;
    public static Boolean stopContractHandlerBeforeDelete = false;
    public static Boolean stopContractHandlerBeforeUpdate = false;
    public static Boolean stopContractItemDLRSHandler = false;
    public static Boolean stopQuoteHandlerAfterUpdate = false;
    public static Boolean stopQuoteHandlerBeforeUpdate = false;
    public static Boolean stopOpportunityHandlerAfterUpdate = false;
    public static Boolean stopOpportunityHandlerBeforeUpdate = false;
    public static Boolean stopOrderAfterUpdateHandler = false;
    public static Boolean stopOrderBeforeUpdateHandler = false;
    public static Boolean stopOrderItemDLRSHandler = false;
    public static Boolean allowOnlyOrderAndOrderItemUpdate = false;
    //Logic Stopper
    public static Boolean stopSubscriptionDeletion = false;
    public static Boolean stopContractDeletion = false;
    public static Boolean stopCashTransferLogic = false;
    public static Boolean stopInvoiceCancellation = false;
    public static Boolean stopSubscriptionTrigger = false;
    public static Boolean stopSubscriptionDLRSHandler = false;
    public static Boolean stopBlngInvoiceTrigger = false;
    public static Boolean stopBillNowFuction = false;
    
    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Return Tax Registration Number base on billing invoice parent(Dimension 2)
        Inputs: List<blng__InvoiceLine__c>
        Returns: String
        <Date>      <Authors Name>      <Brief Description of Change> 
        10-Jul-2017 franz.g.a.Dimaapi    Initial version of function
        31-Jul-2017 franz.g.a.Dimaapi    Transfer the code here
    ----------------------------------------------------------------------------------------------- */
    public static String getTaxRegistrationNumber(List<blng__InvoiceLine__c> blngInvLineList)
    {
        Integer prsCounter = 0;
        Integer pplCounter = 0;
        Integer vplCounter = 0;

        //Count the dimension2 of the line items and return empty string if theres a different Parent in Line
        for(blng__InvoiceLine__c blngInvLine : blngInvLineList)
        {   
            if(blngInvLine.JVCO_Dimension_2__c == 'PRS')
            {
                if(pplCounter > 0 || vplCounter > 0)
                {
                    return '';
                }
                prsCounter++;
            }else if(blngInvLine.JVCO_Dimension_2__c == 'PPL')
            {
                if(prsCounter > 0 || vplCounter > 0)
                {
                    return '';
                }
                pplCounter++;
            }else if(blngInvLine.JVCO_Dimension_2__c == 'VPL')
            {
                if(pplCounter > 0 || prsCounter > 0)
                {
                    return '';
                }
                vplCounter++;

            }else{
                return '';
            }
        }
    
        /* Return Tax Registration Number base on the counter above*/
        if(prsCounter > 0)
        {
            return taxRegNumMap.get('PRS');
        }else if(pplCounter > 0)
        {
            return taxRegNumMap.get('PPL');
        }else if(vplCounter >0)
        {
            return taxRegNumMap.get('VPL');
        }else{
            return '';
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: patrick.t.bautista
        Company: Accenture
        Description: Return Tax Registration Number base on Contract Temp ID = PRS
        Inputs: 
        Returns:
        <Date>      <Authors Name>      <Brief Description of Change> 
        11-nov-2017 patrick.t.bautista  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static String getTaxPRSRegNum()
    {
        return taxRegNumMap.get('PRS');
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Construct Supplier Account Tax Registration Number (Name, TaxRegNumber)
        Inputs: 
        Returns:
        <Date>      <Authors Name>      <Brief Description of Change> 
        10-Jul-2017 franz.g.a.Dimaapi    Initial version of function
        31-Jul-2017 franz.g.a.Dimaapi    Transfer the code here
    ----------------------------------------------------------------------------------------------- */
    public static void constructSupplierRecordTaxRegistrationNumber()
    {
        if(taxRegNumMap == null)
        {
            taxRegNumMap = new Map<String, String>();                        
            for(Account a : [SELECT Id, Name, c2g__CODAVATRegistrationNumber__c
                            FROM Account 
                            WHERE Name IN ('PPL','PRS','VPL')])
            {
                taxRegNumMap.put(a.Name, a.c2g__CODAVATRegistrationNumber__c);
            }
        }   
    }
      
    /* ----------------------------------------------------------------------------------------------
        Author: mary.ann.a.ruelan
        Company: Accenture
        Description: updates recordType on Sales Invoice when transaction lines are updated, invoice becomes paid/unpaid
        Inputs: List<blng__InvoiceLine__c>, Map<Id,c2g__codaInvoice__c> 
        Returns: String
        <Date>      <Authors Name>      <Brief Description of Change> 
        15-Nov-2017 mary.ann.a.ruelan    GREEN-24809 - Initial version of function.
        12-Dec-2019 franz.g.a.dimaapi    GREEN-34982 - Added Latest Payment Date
    ----------------------------------------------------------------------------------------------- */
    public static void updateSalesInvoiceRecordType(List<c2g__codaTransactionLineItem__c> transactionLineList, Map<Id, c2g__codaTransactionLineItem__c> oldMap)
    {
        Set<Id> transactionLineIdSet = new Set<Id>();
        for(c2g__codaTransactionLineItem__c transLine : transactionLineList)
        {
            if(transLine.c2g__LineType__c == 'Account' && transLine.c2g__AccountValue__c > 0 &&
                oldMap.get(transLine.Id).c2g__AccountOutstandingValue__c != transLine.c2g__AccountOutstandingValue__c)
            {
                transactionLineIdSet.add(transLine.Id);
            }
        }
        if(!transactionLineIdSet.isEmpty())
        {
            Id unpaidInvoiceRecTypeId = Schema.SObjectType.c2g__codaInvoice__c.getRecordTypeInfosByName().get('Unpaid Invoice').getRecordTypeId();
            Id paidInvoiceRecTypeId = Schema.SObjectType.c2g__codaInvoice__c.getRecordTypeInfosByName().get('Paid Invoice').getRecordTypeId();
            Id partPaidInvoiceRecTypeId = Schema.SObjectType.c2g__codaInvoice__c.getRecordTypeInfosByName().get('Part Paid Invoice').getRecordTypeId();
            
            List<c2g__codaInvoice__c> sInvList = new List<c2g__codaInvoice__c>();
            for(c2g__codaTransactionLineItem__c tli : [SELECT c2g__Transaction__r.c2g__SalesInvoice__c, c2g__MatchingStatus__c,
                                                        c2g__AccountOutstandingValue__c, c2g__AccountValue__c
                                                        FROM c2g__codaTransactionLineItem__c
                                                        WHERE Id IN : transactionLineIdSet
                                                        AND c2g__Transaction__r.c2g__TransactionType__c = 'Invoice'])
            {
                c2g__codaInvoice__c sInv = new c2g__codaInvoice__c();
                sInv.Id = tli.c2g__Transaction__r.c2g__SalesInvoice__c;
                if(tli.c2g__AccountOutstandingValue__c == 0)
                {
                    sInv.RecordTypeId = paidInvoiceRecTypeId;
                    sInv.JVCO_FullPaymentDate__c = System.Today();
                    sInv.JVCO_Latest_Payment_Date__c = System.today();
                }else
                {
                    if(tli.c2g__AccountOutstandingValue__c != tli.c2g__AccountValue__c)
                    {
                        sInv.RecordTypeId = partPaidInvoiceRecTypeId;
                        sInv.JVCO_Latest_Payment_Date__c = System.today();
                    }else
                    {
                        sInv.RecordTypeId = unpaidInvoiceRecTypeId;
                        sInv.JVCO_Latest_Payment_Date__c = null;
                    }
                    sInv.JVCO_FullPaymentDate__c = null;
                }
                sInvList.add(sInv);
            }
            update sInvList;   
        }
    }

    public static Id getOpenPeriodId()
    {
        Id openPeriodId = null;
        try
        {
            return openPeriodId = [SELECT ID
                                    FROM c2g__codaPeriod__c 
                                    WHERE c2g__Closed__c = FALSE
                                    AND c2g__AR__c = FALSE
                                    AND c2g__Cash__c = FALSE
                                    AND c2g__EndDate__c >= TODAY
                                    AND c2g__OwnerCompany__r.Name = 'PPL PRS Ltd.'
                                    ORDER BY NAME ASC
                                    LIMIT 1].Id;
        }catch(Exception e){}

        return openPeriodId;
    }

}