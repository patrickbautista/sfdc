/*
 * Author: eu.rey.t.cadag@accenture.com
 * Date created: 01-10-2017
 * Description: 
 *
 <Date>          <Authors Name>                      <Brief Description of Change> 
 18-Apr-2017     mel.andrei.b.santos                  Updated code based on GREEN-14892,(added JVCO_Event_Name__c in the query and sorting of results )
 18-Apr-2017     rhys.j.c.dela.cruz                   Updated query code based on GREEN-15023 (added JVCO_Tariff_Code__c != null)
 18-Apr-2017     ma.princess.b.nacepo                 Added JVCO_Event_Invoiced__c filter on the queryString (GREEN-15022)
 05-Jun-2017     reymark.j.l.arlos                    Added condition where JVCO_Event_Start_Date__c is greater or equal to quote start date (GREEN-17253)
 04-Jul-2017     mel.andrei.b.santos                  Updated query code based on GREEN-18737 (removed JVCO_Event_Classification_Status__c)
 29-Aug-2017     mel.andrei.b.santos                  Updated Query code based on GREEN-18737(events based on classification status and tariff code)
 13-Sep-2017     reymark.j.l.arlos                    Remove JVCO_Event_Invoiced__c filter on the queryString (GREEN-23026)
 19-Sep-2017     eu.rey.t.cadag                       update the soql query to include filter of Account affiliated event and venue
 29-Sep-2017     reymark.j.l.arlos                    update soql query to include filter of Account Type to filter what events will be available for import events
 28-Oct-2017     mel.andrei.b.santos                  updated SetOfVenue from set to map to also include affliation startdate for excluding events prior to affiliation as per GREEN-24993
 17-Jan-2018     rhys.j.c.dela.cruz                   Updated query for the Events to remove change when Quote is of type Renewal
*/
public with sharing class JVCO_ImportEventsLogic 
{
    public static List<JVCO_Event__c> loadEvents(SBQQ__Quote__c Quote)
    {
        List<JVCO_Event__c> lstEvents = new List<JVCO_Event__c>();
        List<JVCO_Event__c> lstEventsEqualToPromoter = new List<JVCO_Event__c>();
        List<JVCO_Event__c> lstEventsVenueAffiliateAcct = new List<JVCO_Event__c>();
        String queryString = 'SELECT Id, JVCO_Event_Start_Date__c, JVCO_Event_Quoted__c, JVCO_Event_Classification_Status__c, JVCO_Promoter__c, JVCO_Venue__c, JVCO_Artist__r.Name, JVCO_Percentage_Controlled_Required__c, JVCO_Invoice_Paid__c, JVCO_Tariff_Code__c , JVCO_Event_Name__c, JVCO_LCS__c, JVCO_Event_Type__c';
         
        if(Quote.SBQQ__Account__c != null && Quote.End_Date__c != null)
        {
            //start 29-Sep-2017 reymark.j.l.arlos
            Set<Id> SetOfEventId = excludeSelectedEvent(Quote);
            Map<Id, Date> SetOfVenue = retrieveVenue(Quote); //28-10-2017 mel.andrei.b.santos updated SetOfVenue from set to map to also include affliation startdate for excluding events prior to affiliation as per GREEN-24993
            //end
            Set<ID> setofVenueToQuery = SetOfVenue.keyset();
            List<Schema.FieldSetMember> fieldSetMemberList =  JVCO_ImportEventsLogic.readFieldSet('JVCO_EventsFieldSet','JVCO_Event__c');
            
            for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
            {
                queryString += ', ' + fieldSetMemberObj.getFieldPath();
            }
            Date dt = Quote.End_Date__c;
            string  st = String.valueOf(dt);
            // start 13-Sep-2017 reymark.j.l.arlos removed the conversion of quote start date to string
            // start 05-Jun-2017 reymark.j.l.arlos converts the Quote Start Date to string
            //Date quoteStartDate = Quote.Start_Date__c;
            //string quoteSD = String.valueOf(quoteStartDate);
            //end
            //end
            queryString += ' FROM JVCO_Event__c WHERE (JVCO_Event_End_Date__c <= ' + st ;
            //start 13-Sep-2017 reymark.j.l.arlos removed the condition that checks if JVCO_Event_Start_Date__c is greater or equal to quote start date
            //queryString += ' AND  JVCO_Event_Start_Date__c >= ' + quoteSD; //05-Jun-2017 reymark.j.l.arlos added condition where JVCO_Event_Start_Date__c is greater or equal to quote start date
            //end
            queryString += ' AND  JVCO_CancelledEvent__c = false ';
            // start 04-Jul-2017 mel.andrei.b.santos removed query for JVCO_Event_Classification_Status__c and changed JVCO_Event_Classification_Status__c = null to JVCO_Event_Classification_Status__c != \'Classified\'
           // queryString += ' AND  (JVCO_Event_Classification_Status__c = \'Classified\' OR JVCO_Event_Classification_Status__c = \'Insufficient Information\') '; 
            queryString += ' AND  (JVCO_Event_Classification_Status__c = \'Classified\' OR JVCO_Event_Classification_Status__c = \'Insufficient Information\' OR JVCO_Event_Classification_Status__c = \'Requested\' OR JVCO_Event_Classification_Status__c = null) ';         
            //queryString += ' AND  JVCO_Invoice_Paid__c = FALSE AND ((JVCO_Event_Classification_Status__c = \'Classified\' AND JVCO_Tariff_Code__c != null) OR (JVCO_Event_Classification_Status__c != \'Classified\' AND JVCO_Tariff_Code__c != null)) '; Start 29-Aug-2017 mel.andrei.b.santos Updated querry
            queryString += ' AND  JVCO_Invoice_Paid__c = FALSE '; //end 29-Aug-2017 mel.andrei.b.santos 
            // end
            queryString += ' AND  JVCO_Event_Invoiced__c = FALSE ';
            //queryString += ' AND  JVCO_Promoter__c = \'' + String.ValueOf(Quote.SBQQ__Account__c) + '\' AND JVCO_Promoter__c <> null) ';

            //start 29-Sep-2017 reymark.j.l.arlos

            if(Quote.SBQQ__Account__r.Type == 'Promoter')
            {
                queryString += ' AND  JVCO_Promoter__c = \'' + String.ValueOf(Quote.SBQQ__Account__c) + '\' AND JVCO_Promoter__c <> null) ';
            }
            else if(Quote.SBQQ__Account__r.Type != 'Promoter')
            {
                queryString += ' AND JVCO_Venue__c In: setofVenueToQuery )';
            }
            //end
            String Type = Quote.SBQQ__Type__c;
            queryString += ' ORDER BY JVCO_Event_Start_Date__c DESC';

            system.debug('@@@queryString: ' + queryString.replace('XXX','setofVenueToQuery'));
            For(JVCO_Event__c evt: Database.query(queryString.replace('XXX','setofVenueToQuery')))
            {
                if(evt !=null)
                {
                    if(!evt.JVCO_Event_Quoted__c)
                    {   
                        //start 29-Sep-2017 reymark.j.l.arlos
                        if(Quote.SBQQ__Account__r.Type == 'Promoter')
                        {   
                            if(evt.JVCO_Promoter__c != null && evt.JVCO_Promoter__c == Quote.SBQQ__Account__c)
                            {
                                if(SetOfEventId.size() > 0)
                                {
                                    if(!SetOfEventId.contains(evt.Id))
                                        lstEventsEqualToPromoter.add(evt);
                                }else
                                    lstEventsEqualToPromoter.add(evt);
                            }
                        }

                        else if(Quote.SBQQ__Account__r.Type != 'Promoter')
                        {   
                            if(SetOfVenue.size()>0)
                            {
                                if(SetOfVenue.containskey(evt.JVCO_Venue__c))
                                {
                                    if(evt.JVCO_Promoter__c == null) //|| evt.JVCO_Promoter__c == Quote.SBQQ__Account__c)
                                    {
                                        if(SetOfEventId.size() > 0)
                                        {
                                            if(!SetOfEventId.contains(evt.Id))
                                            {
                                                if(SetOfVenue.containskey(evt.JVCO_Venue__c))
                                                {
                                                    //28-10-2017 mel.andrei.b.santos updated SetOfVenue from set to map to also include affliation startdate for excluding events prior to affiliation as per GREEN-24993
                                                    if(evt.JVCO_Event_Start_Date__c >= SetOfVenue.get(evt.JVCO_Venue__c))
                                                    {
                                                        lstEventsVenueAffiliateAcct.add(evt);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if(SetOfVenue.containskey(evt.JVCO_Venue__c))
                                            {
                                                //28-10-2017 mel.andrei.b.santos updated SetOfVenue from set to map to also include affliation startdate for excluding events prior to affiliation as per GREEN-24993
                                                if(evt.JVCO_Event_Start_Date__c >= SetOfVenue.get(evt.JVCO_Venue__c))
                                                {
                                                    lstEventsVenueAffiliateAcct.add(evt);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //end
                    }
                }
            }
            system.debug('@@lstEvents' + lstEvents + '_' );
        }
            System.debug('DBG: >>> ' + lstEvents.size());
        if(lstEventsEqualToPromoter.size() > 0)
            return lstEventsEqualToPromoter;
            
        return lstEventsVenueAffiliateAcct;
    }
    
    private static Set<Id> excludeSelectedEvent(SBQQ__Quote__c Quote){
        Set<Id> SetOfEventId = new Set<Id>();
        for(SBQQ__QuoteLine__c line:[SELECT Id, JVCO_Event__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =:  Quote.Id ])
        {
            If(line.JVCO_Event__c != null)
                SetOfEventId.add(line.JVCO_Event__c);
        }    
            
        return SetOfEventId;
    }
    
    private static Map<Id, Date> retrieveVenue(SBQQ__Quote__c Quote)
    {
        Map<Id, Date> SetOfVenueId = new Map<Id, Date>();
        //28-10-2017 mel.andrei.b.santos updated SetOfVenue from set to map to also include affliation startdate for excluding events prior to affiliation as per GREEN-24993
        for(JVCO_Affiliation__c line:[SELECT Id, JVCO_Venue__c, JVCO_Start_Date__c FROM JVCO_Affiliation__c WHERE JVCO_Account__c =:  Quote.SBQQ__Account__c])
        {
            If(line.JVCO_Venue__c != null)
            {
                SetOfVenueId.put(line.JVCO_Venue__c, line.JVCO_Start_Date__c);
            }
        }
        return SetOfVenueId;
    }

    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName)
    {
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
    
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        return fieldSetObj.getFields(); 
    } 
    /*
     * Author: eu.rey.t.cadag@accenture.com
     * Description: GREEN-8995: Import Events
     * Date Created: 17 JAN 2017
    */
    public static void updateEventQuoteDetails(Map<Id, JVCO_Event__c> quoteLineEventMap, List<SBQQ__QuoteLine__c> lstQuoteLine)
    {
        system.debug('@@@quoteLineEventMap: ' + quoteLineEventMap);
        system.debug('@@@lstQuoteLine: ' + lstQuoteLine);
        Map<Id, JVCO_Event__c> mapofEventsUpdate = new Map<Id, JVCO_Event__c>();
        if(lstQuoteLine != NULL){
            for(SBQQ__QuoteLine__c ql: lstQuoteLine)
            {
                if(quoteLineEventMap.containsKey(ql.Id) && quoteLineEventMap.get(ql.Id) != null)
                {
                    JVCO_Event__c evt = quoteLineEventMap.get(ql.Id);
                    evt.JVCO_Royalty_Amount__c = ql.SBQQ__NetTotal__c;
                    evt.JVCO_Event_Quoted__c = true;
                    if(!mapofEventsUpdate.containsKey(evt.Id))
                        mapofEventsUpdate.put(evt.Id, evt);
                }
            }
        }
        else{
            /* Author: r.p.valencia.iii
             * Description: GREEN-8995 update for deleting quoteline with events,
             * Updates JVCO_Event_Quoted__c field to false upon deletion of quote line
             * Date Created: 15-May-2017
            */ 
            for(JVCO_Event__c evts: quoteLineEventMap.values()){
                evts.JVCO_Event_Quoted__c = false;
                evts.JVCO_Royalty_Amount__c = null; // mariel.m.buena GREEN-20972 08-Aug-2017
                if(!mapofEventsUpdate.containsKey(evts.Id))
                {
                    mapofEventsUpdate.put(evts.Id, evts);
                }
            }
        }
        
        if(mapofEventsUpdate.size() > 0)
        {
            database.saveresult[] results = database.update(mapofEventsUpdate.values(), false);
            showRecordInfoResultOfDml(results);
        }
    }
    
    public static void showRecordInfoResultOfDml(database.saveresult[] results)
    {
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted account. Account ID: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Fields that affected this error: ' + err.getFields());
                }
            }
        }
    }
    // END GREEN-8995 Import Events
}