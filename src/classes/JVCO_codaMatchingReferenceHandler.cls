/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_codaMatchingReferenceHandler.cls 
   Description:     Trigger Handler for Matching Reference
   Test class:      

   Date            Version     Author                             Summary of Changes 
   -----------     -------     -----------------                  -----------------------------------
   22-Mar-2017     0.1         franz.g.a.dimaapi@accenture.com    Intial draft
   04-Jul-2017     1.1         franz.g.a.dimaapi@accenture.com    Ignore Payable Invoice
   06-Oct-2017     1.2         franz.g.a.dimaapi@accenture.com    Add checker if not came from batch or future
   07-Dec-2017     1.3         filip.bezak@acceture.com           GREEN-26594 - removed checker for batch context
   19-Dec-2017     1.4       chun.yin.tang            Add disabling trigger to Matching Refence Handler
   18-Dec-2018     1.5         filip.bezak@accenture.com          GREEN-28083 - TCP - AP Bank Account Used - logic of filterListForTransferToCash changed
  ------------------------------------------------------------------------------------------------ */
public class JVCO_codaMatchingReferenceHandler {
	public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_codaMatchingReferenceTrigger__c : false;      
	
    public static void afterInsert(Map<Id, c2g__codaMatchingReference__c> matchingReferenceMap)
    {
        if(!skipTrigger)
        {
	        if(!JVCO_FFUtil.stopCashTransferLogic && !System.isFuture())                        //GREEN-26594 - filip.bezak@accenture.com - as we need journals to be created even while batch is matching
	        {
                //deductWriteOffValue(matchingReferenceMap.keySet());
            }
        }
    }
    
/* --------------------------------------------------------------------------------------------------
   Author:         jasper.j.figueroa@accenture.com
   Company:        Accenture
   Description:    This method deducts the write-off value from transaction line items

   Inputs:         Set<Id> matchRefIdSet    

   Date            Version     Author                             Summary of Changes 
   -----------     -------     -----------------                  -----------------------------------
   09-Dec-2017     0.1         jasper.j.figueroa@accenture.com    Intial version of the code
  ------------------------------------------------------------------------------------------------ */
    public static void deductWriteOffValue(Set<Id> matchRefIdSet)
    {
        Decimal writeOffAccountValue = 0;
        Set<Id> transIdSet= new Set<Id>();
        List<c2g__codaTransactionLineItem__c> transactionLineItemToUpdateList = new List<c2g__codaTransactionLineItem__c>();
        
        List<c2g__codaCashMatchingHistory__c> mHistList = [SELECT Id, c2g__TransactionLineItem__c,
                                                           c2g__TransactionLineItem__r.c2g__Transaction__c,
                                                           c2g__Action__c, c2g__AccountValue__c,
                                                           c2g__MatchingReference__c
                                                           FROM c2g__codaCashMatchingHistory__c
                                                           WHERE c2g__MatchingReference__c IN :matchRefIdSet];                                               

        for(c2g__codaCashMatchingHistory__c matchingHistory : mHistList)
        {
            if(matchingHistory.c2g__Action__c == 'Write-off' ||
                (matchingHistory.c2g__Action__c == 'Undo Match' && 
                    matchingHistory.JVCO_Is_Writeoff__c != null))
            {
                writeOffAccountValue = matchingHistory.c2g__AccountValue__c;
            }
            transIdSet.add(matchingHistory.c2g__TransactionLineItem__r.c2g__Transaction__c);
        }
        
        if(writeOffAccountValue != 0 && !transIdSet.isEmpty())
        {
            List<c2g__codaTransaction__c> transactionList = [SELECT Id, Name, c2g__TransactionType__c,
                                                            c2g__AccountTotal__c,
                                                            (SELECT Id, Name, JVCO_Paid_Amount__c,
                                                                c2g__LineType__c, JVCO_Invoice_Total_Write_Off__c,
                                                                c2g__AccountValue__c 
                                                                FROM c2g__TransactionLineItems__r
                                                            WHERE c2g__LineType__c IN ('Analysis','Tax','Account')
                                                            ORDER BY c2g__LineType__c ASC)
                                                            FROM c2g__codaTransaction__c
                                                            WHERE Id in :transIdSet];
        
            for(c2g__codaTransaction__c trans : transactionList)
            {
                if(trans.c2g__TransactionType__c == 'Invoice' || trans.c2g__TransactionType__c == 'Credit Note')
                {
                    for(c2g__codaTransactionLineItem__c tli : trans.c2g__TransactionLineItems__r)
                    { 
                        Decimal percentagePaid = (tli.c2g__AccountValue__c / trans.c2g__AccountTotal__c) * -1;
                        if(tli.c2g__LineType__c == 'Analysis')
                        {
                            tli.JVCO_Paid_Amount__c += (writeOffAccountValue * percentagePaid);
                        }
                        if(tli.c2g__LineType__c == 'Tax')
                        {
                            tli.JVCO_Paid_Amount__c += (writeOffAccountValue * percentagePaid);
                        }
                        if(tli.c2g__LineType__c == 'Account')
                        {
                            tli.JVCO_Invoice_Total_Write_Off__c = writeOffAccountValue;
                        }
                        transactionLineItemToUpdateList.add(tli);
                    }  
                }    
            }
            update transactionLineItemToUpdateList;
        }    
    }
}