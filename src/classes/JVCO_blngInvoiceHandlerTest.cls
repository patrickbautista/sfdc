/**********************************************************************************
 * @Author: 
 * @Company: Accenture
 * @Description:
 * @Created Date:
 * @Revisions:
 *      <Name>              <Date>          <Description>
 *      jason.e.mactal      10.31.2017      Changed to reference static methods
 * 
 **********************************************************************************/
@isTest
private class JVCO_blngInvoiceHandlerTest
{
    /*@testSetup static void setupTestDate()
    {
        JVCO_TestClassHelper.createBillingConfig();

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        Account licAcc3 = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc3.AccountNumber = '832022';
        LicAcc3.JVCO_Live__c = true;
        insert licAcc3; 

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Test.startTest();
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        JVCO_Venue__c ven = JVCO_TestClassHelper.createVenue();
        insert ven;
        
        JVCO_Event__c event = new JVCO_Event__c();
        event.JVCO_Venue__c = ven.id;
        event.JVCO_Tariff_Code__c = 'LC';
        event.JVCO_Event_Type__c = 'Concert - Qualifies for Old Rate';
        event.JVCO_Event_Start_Date__c = date.today();
        event.JVCO_Event_End_Date__c = date.today() + 7;
        event.License_Account__c=licAcc3.id;
        event.Headline_Type__c = 'No Headliner';
        event.JVCO_Number_of_Set_Lists_Expected__c = 2;
        insert event;
        
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        ql.JVCO_Event__c = event.id;
        ql.JVCO_Venue__c = ven.id;
        insert ql;
        
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        Test.stopTest();
         
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        OrderItem oi1 = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi1;

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        //blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        //insert bInv;

    }

    @isTest
    static void createInvoiceTest()
    {
        
        Account testAccount = [select id from Account limit 1];
        Order testOrder = [select id from Order limit 1];
        
        blng__Invoice__c testInvoice = new blng__Invoice__c();
        testInvoice.blng__Account__c = testAccount.id;
        testInvoice.blng__Order__c = testOrder.id;
        testInvoice.blng__InvoiceDate__c = Date.today();
        testInvoice.blng__InvoiceStatus__c = 'Draft';
        testInvoice.JVCO_Invoice_Type__c = 'Standard';

        Test.startTest();

        insert testInvoice;

        blng__Invoice__c selectInvoice = [select id from blng__Invoice__c limit 1];

        system.assertEquals(testInvoice.id, selectInvoice.id, 'Error ');

        Test.stopTest();

    }

    @isTest
    static void updateInvoiceTest()
    {
        
        Account testAccount = [select id from Account limit 1];
        Order testOrder = [select id from Order limit 1];

        blng__Invoice__c testInvoice = new blng__Invoice__c();
        testInvoice.blng__Account__c = testAccount.id;
        testInvoice.blng__Order__c = testOrder.id;
        testInvoice.blng__InvoiceDate__c = Date.today();
        testInvoice.blng__InvoiceStatus__c = 'Draft';
        testInvoice.JVCO_Invoice_Type__c = 'Standard';

        insert testInvoice;

        test.StartTest();

        //testInvoice.JVCO_Payment_Status__c = 'Not Applicable';
        testInvoice.JVCO_Distribution_Status__c  = 'Fully Paid';
        testInvoice.JVCO_Review_Type__c = 'Automatic';
        update testInvoice;

                blng__Invoice__c selectedInvoice = [select id, JVCO_Review_Type__c from blng__Invoice__c where id = :testInvoice.id];
                system.assertEquals('Automatic',selectedInvoice.JVCO_Review_Type__c,'Error');

        test.StopTest();

    }

    @isTest
    static void postInvoiceQueueable()
    {
            
            Account testAccount = [select id from Account limit 1];
            Order testOrder = [select id from Order limit 1];

            blng__Invoice__c testInvoice = new blng__Invoice__c();
            testInvoice.blng__Account__c = testAccount.id;
            testInvoice.blng__Order__c = testOrder.id;
            testInvoice.blng__InvoiceDate__c = Date.today();
            testInvoice.blng__InvoiceStatus__c = 'Draft';
            testInvoice.JVCO_Invoice_Type__c = 'Standard';

            Test.startTest();

            insert testInvoice;

            //JVCO_PostInvoiceQueueable testQueue = new JVCO_PostInvoiceQueueable();
            JVCO_PostInvoiceQueueable.start();
            //blng__Invoice__c selectInvoice = [select id from blng__Invoice__c limit 1];

            //system.assertEquals(testInvoice.id, selectInvoice.id, 'Error ');

            Test.stopTest();

    }

    @isTest
    static void postInvoiceQueueableSPP()
    {
            
            Account testAccount = [select id from Account limit 1];
            Order testOrder = [select id from Order limit 1];

            String qString = 'SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = \'Draft\' ORDER BY blng__Account__c LIMIT 50000';

            blng__Invoice__c testInvoice = new blng__Invoice__c();
            testInvoice.blng__Account__c = testAccount.id;
            testInvoice.blng__Order__c = testOrder.id;
            testInvoice.blng__InvoiceDate__c = Date.today();
            testInvoice.blng__InvoiceStatus__c = 'Draft';
            testInvoice.JVCO_Invoice_Type__c = 'Standard';

            Test.startTest();

            insert testInvoice;

            //JVCO_PostInvoiceQueueable testQueue = new JVCO_PostInvoiceQueueable();
            JVCO_PostInvoiceQueueable.start(qString, 50);
            //blng__Invoice__c selectInvoice = [select id from blng__Invoice__c limit 1];

            //system.assertEquals(testInvoice.id, selectInvoice.id, 'Error ');

            Test.stopTest();

    }

    @isTest
    static void postInvoiceQueueableSPPP()
    {
            
            Account testAccount = [select id from Account limit 1];
            Order testOrder = [select id from Order limit 1];

            String qString = 'SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = \'Draft\' ORDER BY blng__Account__c LIMIT 50000';

            blng__Invoice__c testInvoice = new blng__Invoice__c();
            testInvoice.blng__Account__c = testAccount.id;
            testInvoice.blng__Order__c = testOrder.id;
            testInvoice.blng__InvoiceDate__c = Date.today();
            testInvoice.blng__InvoiceStatus__c = 'Draft';
            testInvoice.JVCO_Invoice_Type__c = 'Standard';

            Test.startTest();

            insert testInvoice;

            //JVCO_PostInvoiceQueueable testQueue = new JVCO_PostInvoiceQueueable();
            JVCO_PostInvoiceQueueable.start(qString, 50, 50);
            //blng__Invoice__c selectInvoice = [select id from blng__Invoice__c limit 1];

            //system.assertEquals(testInvoice.id, selectInvoice.id, 'Error ');

            Test.stopTest();

    }
    @isTest
    static void updateunmatchedandcancelledinvoice()
    {
        
        Account testAccount = [select id from Account limit 1];
        Order testOrder = [select id from Order limit 1];
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        OrderItem oi = [SELECT Id FROM OrderItem LIMIT 1];  
        
        blng__Invoice__c testInvoice = new blng__Invoice__c();
        testInvoice.blng__Account__c = testAccount.id;
        testInvoice.blng__Order__c = testOrder.id;
        testInvoice.blng__InvoiceDate__c = Date.today();
        testInvoice.blng__InvoiceStatus__c = 'Draft';
        testInvoice.JVCO_Invoice_Type__c = 'Standard';
        insert testInvoice;
        
        blng__InvoiceLine__c bInvLine = JVCO_TestClassHelper.setBInvLine(testInvoice.Id, p.Id, oi.Id, 0, false);
        insert bInvLine;
        
        test.StartTest();
        testInvoice.JVCO_Distribution_Status__c  = 'Unmatched and Cancelled';
        testInvoice.JVCO_Review_Type__c = 'Automatic';
        update testInvoice;

        blng__Invoice__c selectedInvoice = [select id, JVCO_Review_Type__c from blng__Invoice__c where id = :testInvoice.id];
        system.assertEquals('Automatic',selectedInvoice.JVCO_Review_Type__c,'Error');

        test.StopTest();

    }
    
    @isTest
    static void testUpdateBlngLineToCredNoteField()
    {
        Account testAccount = [select id from Account limit 1];
        Order testOrder = [select id from Order limit 1];
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        OrderItem oi = [SELECT Id FROM OrderItem LIMIT 1];  
        
        blng__Invoice__c testInvoice = new blng__Invoice__c();
        testInvoice.blng__Account__c = testAccount.id;
        testInvoice.blng__Order__c = testOrder.id;
        testInvoice.blng__InvoiceDate__c = Date.today();
        testInvoice.blng__InvoiceStatus__c = 'Draft';
        testInvoice.JVCO_Invoice_Type__c = 'Standard';
        insert testInvoice;
        
        blng__InvoiceLine__c bInvLine = JVCO_TestClassHelper.setBInvLine(testInvoice.Id, p.Id, oi.Id, 0, false);
        insert bInvLine;
        
        Test.StartTest();
        bInvLine.JVCO_Paid_Amount__c = 100;
        //update bInvLine;

        Test.StopTest();
    }*/
}