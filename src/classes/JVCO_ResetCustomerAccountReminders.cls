/*----------------------------------------------------------------------------------------------
Name: JVCO_ResetCustomerAccountReminders.cls 
Test class: JVCO_ResetCustomerAccountRemindersTest.cls
Description: GREEN-34955 - Aggregate Batch that updates Custom Account Reminder Level to 0, chained to JVCO_GenerateCustomerAccountReminder
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
28-Oct-2019  0.1         rhys.j.c.dela.cruz     Initial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_ResetCustomerAccountReminders implements Database.Batchable<Id>{

    public Iterable<Id> start(Database.BatchableContext BC){
        
        //Date twoDays = Date.today().addDays(-2);
        String addtlCriteria = 'AND c2g__NetTotal__c > 0 '
            +'AND c2g__Account__r.JVCO_Customer_Account__r.JVCO_Customer_Account_Dunning__c = TRUE '
            +'AND ((c2g__Account__r.JVCO_Customer_Account__r.ffps_custRem__Last_Reminder_Severity_Level__c > 0 '
            +'      AND c2g__Account__r.JVCO_Customer_Account__r.ffps_custRem__Reminders_Blocked_Until_Date__c <= TODAY) '
            +'     OR c2g__Account__r.JVCO_Customer_Account__r.ffps_custRem__Last_Reminder_Severity_Level__c != NULL) '
            +'GROUP BY c2g__Account__r.JVCO_Customer_Account__r.id';

        //Query 1
        String custAccsToResetString = 'SELECT c2g__Account__r.JVCO_Customer_Account__r.id acctId '
            +'FROM c2g__codaInvoice__c '
            +'WHERE ((c2g__DueDate__c >= LAST_N_DAYS:1 and c2g__DueDate__c <= today AND c2g__PaymentStatus__c IN (\'Unpaid\', \'Part Paid\') ) OR c2g__PaymentStatus__c = \'Paid\') '
            + addtlCriteria;

        List<AggregateResult> custAccAggList = Database.query(custAccsToResetString);
        List<AggregateResult> accIdAggListForRemove = new List<AggregateResult>();
        Set<Id> custAccIdSet = new Set<Id>();
        Set<Id> custAccIdForRemoveSet = new Set<Id>();

        for(AggregateResult aggRes : custAccAggList){

            custAccIdSet.add((Id)aggRes.get('acctId'));
        }

         if(!custAccIdSet.isEmpty()){

             //Query 2
             String custAccStringFilter = 'SELECT c2g__Account__r.JVCO_Customer_Account__r.id acctId '
                 +'FROM c2g__codaInvoice__c '
                 +'WHERE c2g__DueDate__c < last_n_days:1 AND c2g__PaymentStatus__c IN (\'Unpaid\', \'Part Paid\') ' 
                 +'AND c2g__Account__r.JVCO_Customer_Account__r.id IN: custAccIdSet ' 
                 + addtlCriteria;

             accIdAggListForRemove =  Database.query(custAccStringFilter);
         }

         if(!accIdAggListForRemove.isEmpty()){

             for(AggregateResult aggRes : accIdAggListForRemove){

                 custAccIdForRemoveSet.add((Id)aggRes.get('acctId'));
             }
         }


         if(!custAccIdForRemoveSet.isEmpty()){

             custAccIdSet.removeAll(custAccIdForRemoveSet);
         }

        List<Id> custAccIdList = new List<Id>(custAccIdSet);

        return custAccIdList;
    }

    public void execute(Database.BatchableContext BC, List<Id> scope){
        
        Set<Id> custAccIdForQuery = new Set<Id>(scope);
        List<Account> accList = [SELECT Id, ffps_custRem__Last_Reminder_Severity_Level__c FROM Account WHERE Id IN: custAccIdForQuery];
        List<Account> accForUpdate = new List<Account>();
        
        for(Account acc : accList){

            Account accRec = new Account();
            accRec.Id = acc.Id;
            accRec.ffps_custRem__Last_Reminder_Severity_Level__c = 0;
            accForUpdate.add(accRec);
        }

        if(!accForUpdate.isEmpty()){

            update accForUpdate;
        }
    }

    public void finish(Database.BatchableContext BC){
        
    }
}