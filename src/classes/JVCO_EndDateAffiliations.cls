global class JVCO_EndDateAffiliations implements Database.Batchable<sObject>{
    
    public Set<Id> affId = new Set<Id>();
    public String accountName;
    public String errorMessage;
    
    global JVCO_EndDateAffiliations(Set<Id> affiliationIdSet){
        affId = affiliationIdSet;
        errorMessage = '';
        accountName = '';
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC){
        String query = '';
        
        query = 'Select Id, JVCO_Closure_Reason__c, JVCO_End_Date__c, JVCO_Account__c,JVCO_Account__r.Name, ' + 
                    'JVCO_Account__r.JVCO_Closure_Reason__c, JVCO_Account__r.JVCO_Closure_Date__c  ' + 
                    'From JVCO_Affiliation__c ' + 
                    'Where JVCO_End_Date__c = Null ';
        
        
        if(!affId.isEmpty()){
            String strIds = '';
            
            for(Id i : affId){
                strIds += '\'' + i + '\',';
            }

            strIds = strIds.substring(0, strIds.length() - 1);

            query += ' and Id in ('+strIds+') ';
        }
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<JVCO_Affiliation__c> affList){
        for(JVCO_Affiliation__c affRec : affList){
            affRec.JVCO_Closure_Reason__c = affRec.JVCO_Account__r.JVCO_Closure_Reason__c;
            affRec.JVCO_End_Date__c = affRec.JVCO_Account__r.JVCO_Closure_Date__c;
            accountName = affRec.JVCO_Account__r.Name;
        }
        try{
            update affList;    
        }catch (Exception ex) {
            System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
            errorMessage = ex.getMessage() + ex.getCause() + ex.getStackTraceString();
        }
        
    }
    
    global void finish(Database.BatchableContext BC){

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setReplyTo('no-reply@salesforce.com');
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject('End Date Affiliations Batch Process Completed');
        mail.setTargetObjectId(UserInfo.getUserId());
        String emailMsg = '';
        if(errorMessage != ''){
            emailMsg +='<html><body>An error occurred whilst closing the affiliations, please contact your administrator with the below error messages:' + 
            '<br><br>' + errorMessage ;
        }else{
            emailMsg +='<html><body>All affiliations have now been closed for Account ' + accountName;
        }

        emailMsg += '</body></html>';
        mail.setSaveAsActivity(false);
        mail.setHTMLBody(emailMsg);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}