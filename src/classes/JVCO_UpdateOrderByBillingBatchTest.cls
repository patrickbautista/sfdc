@isTest
public class JVCO_UpdateOrderByBillingBatchTest {
    
    @testSetup 
    static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        //JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        
        RenewalMonthLimit__c renewalCustomsetting = new RenewalMonthLimit__c();
        renewalCustomsetting.Amount__c = 3;
        insert renewalCustomsetting;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;
        
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        List<c2g__codaDimension2__c> dim2List = new List<c2g__codaDimension2__c>();
        c2g__codaDimension2__c pplDim2 = JVCO_TestClassHelper.setDim2('PPL');
        dim2List.add(pplDim2);
        c2g__codaDimension2__c prsDim2 = JVCO_TestClassHelper.setDim2('PRS');
        dim2List.add(prsDim2);
        c2g__codaDimension2__c vplDim2 = JVCO_TestClassHelper.setDim2('VPL');
        dim2List.add(vplDim2);
        insert dim2List;
        
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();
        insert dim3;
        
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_General_Settings__c GeneralSettings = new JVCO_General_Settings__c();
        GeneralSettings.JVCO_Post_Sales_Credit_Note_Scope__c = 1;
        insert GeneralSettings;
        
        Test.startTest();
        
        JVCO_Invoice_Group__c invoiceGroup = new JVCO_Invoice_Group__c();
        invoiceGroup.JVCO_Total_Amount__c = 12;
        insert invoiceGroup;
        
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        sInv.JVCO_Invoice_Group__c = invoiceGroup.Id;
        insert sInv;
        List<c2g__codaInvoiceLineItem__c> sInvLineList = new List<c2g__codaInvoiceLineItem__c>();
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, pplDim2.Id, p.Id));
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, prsDim2.Id, p.Id));
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, vplDim2.Id, p.Id));
        insert sInvLineList;
        
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        o.JVCO_Sales_Invoice__c = sInv.Id;
        insert o;
        
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        oi.JVCO_Sales_Invoice_Line_Item__c = sInvLineList[0].Id;
        insert oi;
        
        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        insert bInv;
        
        blng__InvoiceLine__c bInvLine = JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, -50, true);
        bInvLine.Dimension_2s__c = 'PPL';
        JVCO_FFutil.stopBlngInvoiceLineHandlerBeforeInsert = true;
        insert bInvLine;
        
        Test.stopTest();
    }
    
    @isTest
    static void testOrdersBatchWithoutLimit(){
        List<Integer> yearList = new List<Integer>();
        yearList.add(date.today().year());
        List<Integer> monthList = new List<Integer>();
        monthList.add(date.today().month());
        Database.executeBatch(new JVCO_UpdateOrderByBillingBatch(yearList,monthList));
    }
    @isTest
    static void testOrderItemBatchWithoutLimit(){
        List<Integer> yearList = new List<Integer>();
        yearList.add(date.today().year());
        List<Integer> monthList = new List<Integer>();
        monthList.add(date.today().month());
        Database.executeBatch(new JVCO_UpdateOrderItemByBillingBatch(yearList,monthList));
    }
}