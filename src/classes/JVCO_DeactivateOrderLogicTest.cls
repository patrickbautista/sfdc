@isTest
private class JVCO_DeactivateOrderLogicTest
{
    @testSetup static void setupTestDate(){
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        Test.startTest();
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        //custAcc.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        c.MailingPostalCode = 'ne6 5ls';
        c.MailingCity = 'Test City';
        c.MailingCountry = 'Test Country';
        c.MailingState = 'Test State';
        c.MailingStreet = 'Test Street';
        c.FirstName = 'Test Name';
        c.LastName = 'Test Last Name';
        insert c;
        
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc.AccountNumber = '832020';
        licAcc.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
        insert licAcc;  

        Account licAcc2 = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc2.AccountNumber = '832021';
        insert licAcc2; 

        Account licAcc3 = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc3.AccountNumber = '832022';
        LicAcc3.JVCO_Live__c = true;
        insert licAcc3; 

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        JVCO_Venue__c ven = JVCO_TestClassHelper.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassHelper.createAffiliation(licAcc.Id, ven.Id);
        insert aff;


        
        JVCO_Event__c event = new JVCO_Event__c();
        event.JVCO_Venue__c = ven.id;
        event.JVCO_Tariff_Code__c = 'LC';
        event.JVCO_Event_Type__c = 'Concert - Qualifies for Old Rate';
        event.JVCO_Event_Start_Date__c = date.today();
        event.JVCO_Event_End_Date__c = date.today() + 7;
        event.License_Account__c=licAcc3.id;
        event.Headline_Type__c = 'No Headliner';
        event.JVCO_Number_of_Set_Lists_Expected__c = 2;
        insert event;
        
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        q.JVCO_Cust_Account__c = custAcc.Id;
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        ql.Affiliation__c = aff.id;
        ql.JVCO_Event__c = event.id;
        ql.JVCO_Venue__c = ven.id;
        insert ql;
        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = aff.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;   


        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;

        SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(licAcc.id, p.id);
      s.SBQQ__Contract__c = contr.Id;
      s.Affiliation__c = aff.Id;
      s.SBQQ__QuoteLine__c = ql.Id;
      insert s;

        q.SBQQ__MasterContract__c = contr.Id;
        update q;
        
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopOrderBeforeUpdateHandler = true;
        JVCO_FFUtil.stopOrderAfterUpdateHandler = true;

    }

    @isTest
    static void OrderDeActivate()
    {

        Test.startTest();

        Account testAccount = [select id from account order by CreatedDate DESC limit 1];
        system.debug('TestAccount' + testAccount);

        Product2 testProd = [select id from product2 order by CreatedDate DESC limit 1];

        Opportunity testOpp = [select id, StageName, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity order by CreatedDate DESC limit 1];

        SBQQ__Quote__c testQuote = [select id, SBQQ__NetAmount__c, Recalculate__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c order by CreatedDate DESC limit 1];
        
        SBQQ__QuoteLine__c ql = [select id,
                                    SBQQ__Quote__c, 
                                    SBQQ__SpecialPriceType__c, 
                                    SBQQ__ListPrice__c, 
                                    SBQQ__CustomerPrice__c,
                                    SBQQ__NetPrice__c,
                                    SBQQ__SpecialPrice__c,
                                    SBQQ__RegularPrice__c,
                                    SBQQ__UnitCost__c,
                                    SBQQ__ProratedListPrice__c
                                 from SBQQ__QuoteLine__c order by CreatedDate DESC limit 1];

        Order testOrder = new Order();
        testorder.Accountid = testAccount.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAccount.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        insert testOrder;  



        PriceBookEntry testPB = [select id from PriceBookEntry order by CreatedDate DESC limit 1];

        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = 100.00;
        testOrderItem.EndDate = date.today().addMonths(5);
        insert testOrderItem;

        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
        update testOrderItem;
        

        List<Order> listOrder = new list<Order>();
        listOrder.add(testOrder);

        

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(sc);
        actOrder.activate();

        ApexPages.StandardSetController sc2 = new ApexPages.StandardSetController(listOrder);
        sc2.setSelected(listOrder);
        JVCO_DeactivateOrderLogic deactOrder = new JVCO_deactivateOrderLogic(sc2);
        deactOrder.deactivate();

        Order selectOrder = [select id, Status from Order where id = :testOrder.id];
        system.assertEquals('Draft',selectOrder.status,'Error in selectedOrder Status');

        test.stoptest();
    }

    @isTest
    static void OrderDeActivate_Invalid()
    {

        Test.startTest();

        Account testAccount = [select id from account order by CreatedDate DESC limit 1];
        system.debug('TestAccount' + testAccount);

        Product2 testProd = [select id from product2 order by CreatedDate DESC limit 1];

        Opportunity testOpp = [select id, StageName, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity order by CreatedDate DESC limit 1];

        SBQQ__Quote__c testQuote = [select id, SBQQ__NetAmount__c, Recalculate__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c order by CreatedDate DESC limit 1];
        
        SBQQ__QuoteLine__c ql = [select id, 
                                    SBQQ__SpecialPriceType__c, 
                                    SBQQ__ListPrice__c, 
                                    SBQQ__CustomerPrice__c,
                                    SBQQ__NetPrice__c,
                                    SBQQ__SpecialPrice__c,
                                    SBQQ__RegularPrice__c,
                                    SBQQ__UnitCost__c,
                                    SBQQ__ProratedListPrice__c
                                 from SBQQ__QuoteLine__c order by CreatedDate DESC limit 1];

        Order testOrder = new Order();
        testorder.Accountid = testAccount.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAccount.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        insert testOrder;  



        PriceBookEntry testPB = [select id from PriceBookEntry order by CreatedDate DESC limit 1];

        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = 100.00;
        testOrderItem.EndDate = date.today().addMonths(5);
        insert testOrderItem;

        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
        update testOrderItem;

        blng__Invoice__c testInvoice = new blng__Invoice__c();
        testInvoice.blng__Account__c = testAccount.id;
        testInvoice.blng__Order__c = testOrder.id;
        testInvoice.blng__InvoiceDate__c = Date.today();
        testInvoice.blng__InvoiceStatus__c = 'Draft';
        testInvoice.JVCO_Invoice_Type__c = 'Standard';
        insert testInvoice;

        blng__InvoiceLine__c testInvoiceLine = new blng__InvoiceLine__c();
        testInvoiceLine.blng__Invoice__c = testInvoice.id;
        testInvoiceLine.blng__OrderProduct__c = testOrderItem.id;
        testInvoiceLine.blng__Product__c = testProd.id;
        testInvoiceLine.blng__Quantity__c = 2.0;
        //testInvoiceLine.JVCO_Affiliation__c = testAffil.id;
        //testInvoiceLine.JVCO_Venue__c = testVenue.id;
        testInvoiceLine.blng__InvoiceLineState__c = 'Regular';
        /*insert testInvoiceLine;
        

        List<Order> listOrder = new list<Order>();
        listOrder.add(testOrder);

        

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(sc);
        actOrder.activate();

        ApexPages.StandardSetController sc2 = new ApexPages.StandardSetController(listOrder);
        sc2.setSelected(listOrder);
        JVCO_DeactivateOrderLogic deactOrder = new JVCO_deactivateOrderLogic(sc2);
        deactOrder.deactivate();

        Order selectOrder = [select id, Status from Order where id = :testOrder.id];
        //system.assertEquals('Draft',selectOrder.status,'Error in selectedOrder Status');

        test.stoptest();
        */
    }

    @isTest
    static void OrderDeActivate_NoneSelected()
    {

        Account testAccount = [select id from account order by CreatedDate DESC limit 1];
        system.debug('TestAccount' + testAccount);

        Product2 testProd = [select id from product2 order by CreatedDate DESC limit 1];

        Opportunity testOpp = [select id, StageName, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity order by CreatedDate DESC limit 1];

        SBQQ__Quote__c testQuote = [select id, SBQQ__NetAmount__c, Recalculate__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c order by CreatedDate DESC limit 1];
        
        SBQQ__QuoteLine__c ql = [select id, 
                                    SBQQ__SpecialPriceType__c, 
                                    SBQQ__ListPrice__c, 
                                    SBQQ__CustomerPrice__c,
                                    SBQQ__NetPrice__c,
                                    SBQQ__SpecialPrice__c,
                                    SBQQ__RegularPrice__c,
                                    SBQQ__UnitCost__c,
                                    SBQQ__ProratedListPrice__c
                                 from SBQQ__QuoteLine__c order by CreatedDate DESC limit 1];

        Order testOrder = new Order();
        testorder.Accountid = testAccount.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAccount.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        insert testOrder;  


        
        PriceBookEntry testPB = [select id from PriceBookEntry order by CreatedDate DESC limit 1];
        
        Test.startTest();
        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = 100.00;
        testOrderItem.EndDate = date.today().addMonths(5);

        
        insert testOrderItem;

        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
        update testOrderItem;
        

        List<Order> listOrder = new list<Order>();
        listOrder.add(testOrder);

        

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(sc);
        actOrder.activate();

        ApexPages.StandardSetController sc2 = new ApexPages.StandardSetController(listOrder);
        //sc2.setSelected(listOrder);
        JVCO_DeactivateOrderLogic deactOrder = new JVCO_deactivateOrderLogic(sc2);
        deactOrder.deactivate();

        Order selectOrder = [select id, Status from Order where id = :testOrder.id];
        system.assertEquals('Activated',selectOrder.status,'Error in selectedOrder Status');

        test.stoptest();
    }

    @isTest
    static void OrderDeActivate_ReturnToAccount()
    {

        Account testAccount = [select id from account order by CreatedDate DESC limit 1];
        system.debug('TestAccount' + testAccount);

        Product2 testProd = [select id from product2 order by CreatedDate DESC limit 1];

        Opportunity testOpp = [select id, StageName, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity order by CreatedDate DESC limit 1];

        SBQQ__Quote__c testQuote = [select id, SBQQ__NetAmount__c, Recalculate__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c order by CreatedDate DESC limit 1];
        
        SBQQ__QuoteLine__c ql = [select id, 
                                    SBQQ__SpecialPriceType__c, 
                                    SBQQ__ListPrice__c, 
                                    SBQQ__CustomerPrice__c,
                                    SBQQ__NetPrice__c,
                                    SBQQ__SpecialPrice__c,
                                    SBQQ__RegularPrice__c,
                                    SBQQ__UnitCost__c,
                                    SBQQ__ProratedListPrice__c
                                 from SBQQ__QuoteLine__c order by CreatedDate DESC limit 1];

        Order testOrder = new Order();
        testorder.Accountid = testAccount.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAccount.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        insert testOrder;  



        PriceBookEntry testPB = [select id from PriceBookEntry order by CreatedDate DESC limit 1];
        
        Test.startTest();
        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = 100.00;
        testOrderItem.EndDate = date.today().addMonths(5);

        
        insert testOrderItem;

        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
        update testOrderItem;
        
        List<Order> listOrder = new list<Order>();
        listOrder.add(testOrder);
        
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(sc);
        actOrder.activate();

        ApexPages.StandardSetController sc2 = new ApexPages.StandardSetController(listOrder);
        //sc2.setSelected(listOrder);
        JVCO_DeactivateOrderLogic deactOrder = new JVCO_deactivateOrderLogic(sc2);
        deactOrder.returnToAccount();

        //Order selectOrder = [select id, Status from Order where id = :testOrder.id];
        //system.assertEquals('Activated',selectOrder.status,'Error in selectedOrder Status');

        test.stoptest();
    }
}