/* ------------------------------------------------------------------------------------------------
   Name:            JVCO_ActivateAllOrderQueueable
   Description:     Queueable implementation for Activate All Javascript button

   Date            Version     Author                    Summary of Changes 
   -----------     -------     -----------------            ---------------------------------------
   19-Sep-2018     0.1         jules.osberg.a.pablo     Initial Version
   06-Dec-2018     0.2         rhys.j.c.dela.cruz           GREEN-33362 - Edited criteria to accept Not Needed value
------------------------------------------------------------------------------------------------ */
public class JVCO_ActivateAllOrderQueueable implements Queueable {
  private Map<Id, Order> orderMap;
  private Boolean isActivateOrder;
  @testVisible private static Boolean testError = false;

  public JVCO_ActivateAllOrderQueueable(Map<Id, Order> orderMap, Boolean isActivateOrder) {
    this.orderMap = orderMap;
    this.isActivateOrder = isActivateOrder;
  }
  public void execute(QueueableContext context) {
    Integer processSize = 20;
    if((Integer)JVCO_Constants__c.getOrgDefaults().JVCO_Activate_Order_Batch_Size__c != null) {
            processSize = (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_Activate_Order_Batch_Size__c;
        }

        if((processSize > orderMap.size()) || Test.isRunningTest()){
          processSize = orderMap.size();
        }

    if(isActivateOrder) {
          if(!orderMap.isEmpty()) {
            List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
            List<Order> orderList = new List<Order>();
            for(Integer i=0; i<processSize; i++) {
              orderList.add(orderMap.values()[i]);
            }
           
            Savepoint sp = Database.setSavepoint();
        List<sObject> updateOrderList = new List<sObject>();
        for(Order orderRecord :orderList) {
          if(orderRecord.SBQQ__PriceCalcStatus__c == 'Completed' || orderRecord.SBQQ__PriceCalcStatus__c == 'Not Needed') {
                orderRecord.Status = 'Activated';
                updateOrderList.add(orderRecord);
            }  
        }
        if(Test.isRunningTest() && testError){ 
                    updateOrderList.add(new Opportunity());
                  updateOrderList.add(new SBQQ__Quote__c());
                  updateOrderList.add(new SBQQ__QuoteLineGroup__c());                             
                }
        List<Database.SaveResult> res = Database.update(updateOrderList,false);
              for(Integer i = 0; i < updateOrderList.size(); i++) {
                  Database.SaveResult srOppList = res[i];
                  sObject origrecord = updateOrderList[i];
                  if(!srOppList.isSuccess()) {
                      Database.rollback(sp);
                      System.debug('Update sObject fail: ');
                      for(Database.Error objErr:srOppList.getErrors()) {
                          ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                          /*
                          errLog.Name = String.valueOf(origrecord.ID);
                          errLog.JVCO_FailedRecordObjectReference__c = String.valueOf(origrecord.ID);
                          errLog.blng__FullErrorLog__c = string.valueof(objErr.getMessage());
                          errLog.blng__ErrorCode__c = string.valueof(objErr.getStatusCode());
                          errLog.JVCO_ErrorBatchName__c = 'JVCO_ActivateAllOrderExtension';
                          */

                          errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(origrecord.ID);
                          errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                          errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                          errLog.ffps_custRem__Message__c = 'JVCO_ActivateAllOrderExtension';
                          errLogList.add(errLog);   

                      }
                  }
              }

              if(!errLogList.isempty()) {
                  try {
                      insert errLogList;
                  } catch(Exception ex) {
                      System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                       
                  }   
              } 

        for(sObject oRecord :updateOrderList) {
            orderMap.remove(oRecord.Id);
          }
        }
        
        if(!orderMap.isEmpty()) {
              if(!Test.isRunningTest()) {
               System.enqueueJob(new JVCO_ActivateAllOrderQueueable(orderMap, true));
              }
          } 
      } else {
          if(!orderMap.isEmpty()) {
          List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
            List<Order> orderList = new List<Order>();
            for(Integer i=0; i<processSize; i++) {
              orderList.add(orderMap.values()[i]);
            }
           
            Savepoint sp = Database.setSavepoint();
          List<sObject> updateOrderList = new List<sObject>();
          for(Order orderRecord :orderList) {
             orderRecord.Status = 'Draft';
             updateOrderList.add(orderRecord);
          }
          if(Test.isRunningTest() && testError){ 
                    updateOrderList.add(new Opportunity());
                  updateOrderList.add(new SBQQ__Quote__c());
                  updateOrderList.add(new SBQQ__QuoteLineGroup__c());                             
                }
          List<Database.SaveResult> res = Database.update(updateOrderList,false);
            for(Integer i = 0; i < updateOrderList.size(); i++) {
                Database.SaveResult srOppList = res[i];
                sObject origrecord = updateOrderList[i];
                if(!srOppList.isSuccess()) {
                    Database.rollback(sp);
                    System.debug('Update sObject fail: ');
                    for(Database.Error objErr:srOppList.getErrors()) {
                        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                        /*
                        errLog.Name = String.valueOf(origrecord.ID);
                        errLog.JVCO_FailedRecordObjectReference__c = String.valueOf(origrecord.ID);
                        errLog.blng__FullErrorLog__c = string.valueof(objErr.getMessage());
                        errLog.blng__ErrorCode__c = string.valueof(objErr.getStatusCode());
                        errLog.JVCO_ErrorBatchName__c = 'JVCO_ActivateAllOrderExtension: Deactivate All Orders';
                        */
                        errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(origrecord.ID);
                        errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                        errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                        errLog.ffps_custRem__Message__c = 'JVCO_ActivateAllOrderExtension: Deactivate All Orders';
                        errLogList.add(errLog);   
                    }
                }
            }

            if(!errLogList.isempty()) {
                try {
                    insert errLogList;
                } catch(Exception ex) {
                    System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());           
                }   
            }

            for(sObject oRecord :updateOrderList) {
            orderMap.remove(oRecord.Id);
          }
        }

        if(!orderMap.isEmpty()) {
              if(!Test.isRunningTest()) {
               System.enqueueJob(new JVCO_ActivateAllOrderQueueable(orderMap, false));
              }
          }
      }
  }
}