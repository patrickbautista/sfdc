/* ----------------------------------------------------------------------------------------------
   Name: JVCO_codaCreditNoteHandler.cls 
   Description: Handler class for c2g__codaCreditNote__c trigger

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  27-Mar-2017   0.1         ryan.i.r.limlingan  Intial creation
  19-Dec-2017   0.2         chun.yin.tang       Add disabling trigger to credit note Handler
  02-Dec-2019   0.2         patrick.t.bautista  GREEN-35106 - redesign validation for mismatch parent matching
  ----------------------------------------------------------------------------------------------- */
public with sharing class JVCO_codaCreditNoteHandler
{
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_codaCreditNoteTrigger__c : false;
    public static Boolean DisableMismatchParent = JVCO_General_Settings__c.getInstance().JVCO_Disable_mismatch_parent__c;
    /* ------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: This method handles afterInsert trigger events for c2g__codaCreditNote__c
    Input: List<c2g__codaCreditNote__c>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    27-Mar-2017 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public static void afterInsert(List<c2g__codaCreditNote__c> creditNoteList){

        if(!skipTrigger)
        {
            rollupNetTotalOfTerminatedSubscription(creditNoteList,null);
        }
    }

    /* ------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: This method handles afterUpdate trigger events for c2g__codaCreditNote__c
    Input: List<c2g__codaCreditNote__c>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    27-Mar-2017 ryan.i.r.limlingan  Initial version of function
    17-Apr-2017 ryan.i.r.limlingan  Added customisation for the case when Credit Note is created
                                    from Convert to Credit Note button to populate Venue fields
    25-Aug-2017 franz.g.a.dimaapi   Moved linkToBillingInvoice from afterInsert to afterUpdate
    02-Feb-2018 mary.ann.a.ruelan  Moved linkToBillingInvoice to from afterUpdate to beforeUpdate
    ----------------------------------------------------------------------------------------------- */
    public static void afterUpdate(List<c2g__codaCreditNote__c> creditNoteList, Map<id,c2g__codaCreditNote__c> oldCreditNoteList){

        if(!skipTrigger)
        {
            rollupNetTotalOfTerminatedSubscription(creditNoteList,oldCreditNoteList);
        }


    }


    /* ------------------------------------------------------------------------------------------
    Author: robert.j.b.lacatan
    Company: Accenture
    Description: This method handles afterDelete trigger events for c2g__codaCreditNote__c
    Input: Map<c2g__codaCreditNote__c>
    Returns: void
    <Date>        <Authors Name>      <Brief Description of Change> 
    13-July-2018  robert.j.b.lacatan  Initial version of function
    
    ----------------------------------------------------------------------------------------------- */
    public static void afterDelete(Map<id,c2g__codaCreditNote__c> oldCreditNoteList){
        
        if(!skipTrigger)
        {
            rollupNetTotalOfTerminatedSubscription(null,oldCreditNoteList);
        }
    }
    
    /* ------------------------------------------------------------------------------------------
    Author: mary.ann.a.ruelan
    Company: Accenture
    Description: This method handles before update trigger events for c2g__codaCreditNote__c
    Input: List<c2g__codaCreditNote__c>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    02-Feb-2018 mary.ann.a.ruelan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static void beforeUpdate(List<c2g__codaCreditNote__c> creditNoteList, Map<id, c2g__codaCreditNote__c> oldCreditNoteMap){
        if(!skipTrigger){
            if(DisableMismatchParent){
                restrictDifferentTransLineParent(creditNoteList, oldCreditNoteMap);
            }
            system.debug(logginglevel.ERROR,'### JVCO_codaCreditNoteHandler afterUpdate START CPU Time: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime());
            if(!JVCO_FFUtil.stopInvoiceCancellation)
            {
               JVCO_InvoiceCancellationLogic.linkToBillingInvoice(creditNoteList); 
               populateSalesRepForManualCreditNotes(creditNoteList);
            }
            system.debug(logginglevel.ERROR,'### JVCO_codaCreditNoteHandler afterUpdate END CPU Time: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime());
        }
    }

    /* ----------------------------------------------------------------------------------------------------------
        Author:         robert.j.b.lacatan
        Company:        Accenture
        Description:    Method to sum up net total of all terminated subscription
        Inputs:         
        <Date>          <Authors Name>                      <Brief Description of Change> 
        13-July-2018     robert.j.b.lacatan                   Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public static void rollupNetTotalOfTerminatedSubscription(List<c2g__codaCreditNote__c> creditNoteList, Map<id,c2g__codaCreditNote__c> oldCreditNoteList){

        Set<id> licAccSet = new Set<id>();
        List<Account> accListToUPdate = new List<Account>();
        
        if(creditNoteList!=null)
        {   
            if(oldCreditNoteList!=null)
            {
                for(c2g__codaCreditNote__c creditNoteRec: creditNoteList)
                {
                    if(creditNoteRec.JVCO_Credit_Reason__c != oldCreditNoteList.get(creditNoteRec.id).JVCO_Credit_Reason__c && (creditNoteRec.JVCO_Credit_Reason__c == 'Licence cancelled' || oldCreditNoteList.get(creditNoteRec.id).JVCO_Credit_Reason__c == 'Licence cancelled'))
                    {
                        licAccSet.add(creditNoteRec.c2g__Account__c);
                    }
                }
            }else
            {
                for(c2g__codaCreditNote__c creditNoteRec: creditNoteList)
                {
                    if(creditNoteRec.JVCO_Credit_Reason__c == 'Licence cancelled')
                    {
                        licAccSet.add(creditNoteRec.c2g__Account__c);
                    }
                }
            }
            
        }else
        {
            for(c2g__codaCreditNote__c creditNoteRec: oldCreditNoteList.values())
            {
                if(creditNoteRec.JVCO_Credit_Reason__c == 'Licence cancelled')
                {
                    licAccSet.add(creditNoteRec.c2g__Account__c);
                }
            }  
        }
        if(!licAccSet.isEmpty())
        {
            for(AggregateResult ar: [SELECT c2g__Account__c, SUM(c2g__CreditNoteTotal__c) sumCreditTotal FROM c2g__codaCreditNote__c WHERE c2g__Account__c IN :licAccSet AND JVCO_Credit_Reason__c = 'Licence cancelled' GROUP BY c2g__Account__c])
            {
                Account upAcc = new Account();
                upAcc.id = ((Id)ar.get('c2g__Account__c'));
                upAcc.Total_Credit_note__c = ((Decimal)ar.get('sumCreditTotal'));
                accListToUPdate.add(upAcc);
            }
        }

        if(!accListToUPdate.isEmpty())
        {
            update accListToUPdate;
        }
    }

/* ----------------------------------------------------------------------------------------------------------
        Author:         john.patrick.valdez
        Company:        Accenture
        Description:    Method to populate sales rep for manually created sales credit notes
        Inputs:         
        <Date>          <Authors Name>                      <Brief Description of Change> 
        29-Aug-2018     john.patrick.valdez                   Initial version of the code / GREEN-33055
    ----------------------------------------------------------------------------------------------------------*/
    private static void populateSalesRepForManualCreditNotes(List<c2g__codaCreditNote__c> creditNoteList){
        
        for(c2g__codaCreditNote__c creditNoteRec: creditNoteList)
        {
            if(creditNoteRec.JVCO_Sales_Rep__c == null && creditNoteRec.c2g__CreditNoteStatus__c == 'Complete')
            {
                creditNoteRec.JVCO_Sales_Rep__c = creditNoteRec.CreatedById;
            }
        }
    }
    /* ------------------------------------------------------------------------------------------
        Author: patrick.t.bautista@accenture.com
        Description: This method throws an error when Reference Doc parents is not equal to SCR
        Input: List<c2g__codaInvoice__c>
        Returns: void
        <Date>          <Authors Name>                  <Brief Description of Change> 
        23-Aug-2018     patrick.t.bautista              Initial version of function for GREEN-34945
    --------------------------------------------------------------------------------------------- */
    public static void restrictDifferentTransLineParent(List<c2g__codaCreditNote__c> creditNoteList, Map<Id, c2g__codaCreditNote__c> oldCreditNoteMap)
    {        
        List<String> invoiceOrCredStrList = new List<String>();
        for(c2g__codaCreditNote__c cred: creditNoteList){
            if(cred.JVCO_Reference_Document__c != null
               && cred.JVCO_Reference_Document__c != oldCreditNoteMap.get(cred.id).JVCO_Reference_Document__c
               && cred.JVCO_Reference_Document__c.contains('SIN'))
            {
                invoiceOrCredStrList.add(cred.JVCO_Reference_Document__c);
                invoiceOrCredStrList.add(cred.Name);
            }
        }
        //Get Parents Mapping
       Map<String,Map<String,Double>> invOrCredToListOfParentMap = JVCO_SalesInvoiceHandler.getInvoiceOrCredParentMap(invoiceOrCredStrList);
        //Throws error if parents are not equal
        if(!invOrCredToListOfParentMap.isEmpty()){
            for(c2g__codaCreditNote__c cred: creditNoteList){
                if(invOrCredToListOfParentMap.containsKey(cred.JVCO_Reference_Document__c) 
                   && invOrCredToListOfParentMap.containsKey(cred.Name))
                {
                    for(String credParentstr : invOrCredToListOfParentMap.get(cred.Name).keySet())
                    {
                        if(!invOrCredToListOfParentMap.get(cred.JVCO_Reference_Document__c).containsKey(credParentstr)
                           || invOrCredToListOfParentMap.get(cred.Name).get(credParentstr)
                           > invOrCredToListOfParentMap.get(cred.JVCO_Reference_Document__c).get(credParentstr))
                        {
                            cred.addError(Label.JVCO_RestrictParentNotEqual);
                        }
                    }
                }
            }
        }
    }
}