/**
* Created by: Accenture
* Created Date: 15.03.2018
* Description: APEX Batch class to update ‘JVCO_Generate_DD_Doc_Flag__c' check boxes to send Payonomy Payment Agreement  notification
*
*  Change Description 
*  
*  Enterprise ID                 Date                   Desc
*  
*
**/
global class JVCO_PayonomyPaymentAgrNotification  implements Database.batchable<Sobject>,Database.AllowsCallouts, Schedulable{

    public Id payonomyPaymentAgrID;    
    public List<String> generateDDDocPickLstVal = null;
    public Set<Id> payonomyPaymentAgrIDSet = null;
    
    /**
    * This method retrieves retail and current contract records
    */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String soqlLimt  = null;
        generateDDDocPickLstVal  = new List<String>();
        
        List<JVCO_PPL_PPRS_App_Config__mdt>  appConfigLst = [Select DeveloperName,MasterLabel, Value__c from JVCO_PPL_PPRS_App_Config__mdt where  DeveloperName = :'Payment_Agreement_Batch_Query_Limit'];
        
        if(appConfigLst.size() > 0)
        {
            soqlLimt = appConfigLst.get(0).Value__c;        
        }
        appConfigLst = [Select DeveloperName,MasterLabel, Value__c from JVCO_PPL_PPRS_App_Config__mdt where  DeveloperName = :'Payment_Agre_Gen_DD_Doc_PicklIst_Val'];        
        
        if(appConfigLst.size() > 0)
        {
            if(appConfigLst.get(0).Value__c != null && appConfigLst.get(0).Value__c != '')
            {
                generateDDDocPickLstVal.addALL(appConfigLst.get(0).Value__c.split(';') );
            }    
        }
        
        String query= 'Select JVCO_Ready_For_DD_Doc_Gen__c  from PAYREC2__Payment_Agreement__c where JVCO_Generate_DD_Doc_Flag__c in :generateDDDocPickLstVal and JVCO_Ready_For_DD_Doc_Gen__c  = false  ';
        // Add the soql limit to the SOQL
      
        if(!String.isBlank(String.valueOf(payonomyPaymentAgrID)))
        {
            query += ' and id = \''+payonomyPaymentAgrID+'\' ';
        }
        
        if(payonomyPaymentAgrIDSet != null)
        {
            query += ' and id  in :payonomyPaymentAgrIDSet ';
        }

        if(soqlLimt != null)
        {
            query = query + ' Limit ' + soqlLimt;
        }        
        system.debug('query--' + query)  ;  
        return Database.getQueryLocator(query);
    }
    // adhoc testing for single id
    global JVCO_PayonomyPaymentAgrNotification(Id ppId)
    {
        payonomyPaymentAgrID = ppId;
    }
    //constructors for adhoc testing
    global JVCO_PayonomyPaymentAgrNotification(){}

    // adhoc testing for multiple ids
    global JVCO_PayonomyPaymentAgrNotification(Set<Id> idSet)
    {
        payonomyPaymentAgrIDSet = new Set<Id>();
        if(idSet!= null)
        {
            payonomyPaymentAgrIDSet= idSet;
        }               
    }

    
    /**
    * This method executes the retrieved contract records
    */
    global void execute(SchedulableContext sc)
    {
        JVCO_PayonomyPaymentAgrNotification  batch = new JVCO_PayonomyPaymentAgrNotification();
        Database.executeBatch(batch);
    }
    
    /**
    * This method executes the retrieved contract records
    */
    global void execute(Database.BatchableContext BC, List<Sobject> scope)
    {
        System.debug('scope: '+ scope);
        List<PAYREC2__Payment_Agreement__c> accLst = new List<PAYREC2__Payment_Agreement__c>();
        for(Sobject sobj: scope)
        {
            PAYREC2__Payment_Agreement__c accObj  = (PAYREC2__Payment_Agreement__c)sobj;
            accObj.JVCO_Ready_For_DD_Doc_Gen__c  = true;   
            accLst.add(accObj);   
        }
        try
        {
             List<Database.SaveResult> uResults = Database.update(accLst,false);
             String errMessage = '';
             for (Database.SaveResult sr : uResults ) 
             {
                if (sr.isSuccess()) 
                {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted account. Account ID: ' + sr.getId());
                }
                else 
                {
                    // Operation failed, so get all errors                
                 /*   for(Database.Error err : sr.getErrors()) 
                    {                      
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Account fields that affected this error: ' + err.getFields());
                    }
*/
                }
            }  
        }
        catch(Exception e)
        {
            System.debug('Error: ' + e);
        }
    }    
    /**
    * This method will execute after all batched are processed
    */
    global void finish(Database.BatchableContext BC)
    {
        System.debug('Batch job is completed.');
    }
}