/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class JVCO_MergeRecordTest {

    public static id CustomerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
    private static List<Account> licAccounts;       

    @testSetup static void TestClassSetup()
    {
        //     c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
        //     testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
        //     testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
        //     testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
        //     testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        //     testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
        //     testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
        //     testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
        //     testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
        //     testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
        //     testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
        //     testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
        //     testGeneralLedgerAcc.Dimension_1_Required__c = false;
        //     testGeneralLedgerAcc.Dimension_2_Required__c = false;
        //     testGeneralLedgerAcc.Dimension_3_Required__c = false;
        //     testGeneralLedgerAcc.Dimension_4_Required__c = false;
        //     testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
        //   //  testGeneralLedgerAcc.ownerid = testGroup.Id;
        //     insert testGeneralLedgerAcc;

        //     c2g__codaTaxCode__c taxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedgerAcc.id);
        //     insert taxCode;

        //Test Create Data
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='JVCO_Venue__c'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q2);
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }

        JVCO_Venue__c venue = new JVCO_Venue__c();
        venue.Name = 'test venue 1';
        venue.JVCO_Field_Visit_Requested__c = false;
        venue.JVCO_Street__c = 'testStreet';
        venue.JVCO_City__c = 'testCoty';
        venue.JVCO_Postcode__c ='TN32 4SL';
        venue.JVCO_Country__c ='UK';

        insert venue;
        
        JVCO_Venue__c venue2 = new JVCO_Venue__c();
        venue2.Name = 'update venue';
        venue2.JVCO_Field_Visit_Requested__c = false;
        venue2.JVCO_Street__c = 'updateStreet';
        venue2.JVCO_City__c = 'updateCoty';
        venue2.JVCO_Postcode__c ='TN32 4SL';
        venue2.JVCO_Country__c ='UK';

        insert venue2;
        
        //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
        testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc.Dimension_1_Required__c = false;
        testGeneralLedgerAcc.Dimension_2_Required__c = true;
        testGeneralLedgerAcc.Dimension_3_Required__c = false;
        testGeneralLedgerAcc.Dimension_4_Required__c = false;
        testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
        testGeneralLedgerAcc.ownerid = testGroup.Id;
        testGeneralLedgerAcc.c2g__ReportingCode__c = '30030';
        insert testGeneralLedgerAcc;
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedgerAcc.id);
        insert taxCode;

        //Create Account
        Account testAccCust = new Account(Name='Test Customer Account');
        testAccCust.AccountNumber = '999999';
        testAccCust.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust.Type='Customer';
        testAccCust.RecordTypeId = accountRecordTypes.get('Customer Account');
        insert testAccCust;
        
        Account testAccCust2 = new Account(Name='Test Customer Account 2');
        testAccCust2.AccountNumber = '2222222';
        testAccCust2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust2.Type='Customer';
        testAccCust2.RecordTypeId = accountRecordTypes.get('Customer Account');
        insert testAccCust2;

         Contact c = JVCO_TestClassObjectBuilder.createContact(testAccCust.id);
        insert c;

        Contact con = JVCO_TestClassObjectBuilder.createContact(testAccCust2.id);
        con.LastName = 'LastName';
        con.FirstName = 'FirstName';
        con.Email = 'test@hotmail.com';
        insert con;

        
        Account testLicAcc1 = new Account(Name='Test Lic Account 1');
        testLicAcc1.AccountNumber = '987654';
        testLicAcc1.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testLicAcc1.c2g__CODAAllowDeleteInUse__c = false;
        testLicAcc1.c2g__CODACreditLimitEnabled__c = false;
        testLicAcc1.c2g__CODACreditStatus__c = 'Credit Allowed';
        testLicAcc1.c2g__CODADaysOffset1__c = 0.0;
        testLicAcc1.c2g__CODADiscount1__c = 0.0;
        testLicAcc1.c2g__CODAFederallyReportable1099__c = false;
        testLicAcc1.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testLicAcc1.c2g__CODAIntercompanyAccount__c = false;
        testLicAcc1.c2g__CODASalesTaxStatus__c = 'Taxable';
        testLicAcc1.JVCO_Customer_Account__c = testAccCust.id;
        testLicAcc1.RecordTypeId = accountRecordTypes.get('Licence Account');
        testLicAcc1.c2g__CODAOutputVATCode__c = taxCode.id;
        testLicAcc1.JVCO_Preferred_Contact_Method__c = 'Email';
        testLicAcc1.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        testLicAcc1.JVCO_Billing_Contact__c = c.id;
        testLicAcc1.JVCO_Licence_Holder__c = c.id;
        testLicAcc1.JVCO_Review_Contact__c = c.id;
        insert testLicAcc1;

        Account testLicAcc2 = new Account(Name='Test License Account 2');
        testLicAcc2.AccountNumber = '987623';
        testLicAcc2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testLicAcc2.c2g__CODAAllowDeleteInUse__c = false;
        testLicAcc2.c2g__CODACreditLimitEnabled__c = false;
        testLicAcc2.c2g__CODACreditStatus__c = 'Credit Allowed';
        testLicAcc2.c2g__CODADaysOffset1__c = 0.0;
        testLicAcc2.c2g__CODADiscount1__c = 0.0;
        testLicAcc2.c2g__CODAFederallyReportable1099__c = false;
        testLicAcc2.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testLicAcc2.c2g__CODAIntercompanyAccount__c = false;
        testLicAcc2.c2g__CODASalesTaxStatus__c = 'Taxable';
        testLicAcc2.JVCO_Customer_Account__c = testAccCust.id;
        testLicAcc2.RecordTypeId = accountRecordTypes.get('Licence Account');
        testLicAcc2.c2g__CODAOutputVATCode__c = taxCode.id;
        testLicAcc2.JVCO_Preferred_Contact_Method__c = 'Email';
        testLicAcc2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        testLicAcc2.JVCO_Billing_Contact__c = c.id;
        testLicAcc2.JVCO_Licence_Holder__c = c.id;
        testLicAcc2.JVCO_Review_Contact__c = c.id;
        insert testLicAcc2;
        
        licAccounts = new List<Account>();
        licAccounts.add(testLicAcc1);
        licAccounts.add(testLicAcc2);
        
        JVCO_Affiliation__c aff = new JVCO_Affiliation__c();
        aff.JVCO_Account__c = testLicAcc1.Id;
        aff.JVCO_Venue__c = venue.Id;
        aff.JVCO_Start_Date__c = Date.Today();
        insert aff;
        
        Opportunity opp = new Opportunity();
        opp.AccountId = testLicAcc2.Id;
        opp.Name = 'Opp test';
        opp.StageName = 'Quoting';
        opp.CloseDate = System.Today().addDays(5);
        insert opp;
        
        List<JVCO_BackgroundMatching__c> bgMatchingList = new List<JVCO_BackgroundMatching__c>();
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Trading Currency', JVCO_Default_Value__c = 'GBP'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Receivable Control', JVCO_Default_Value__c = '10040'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Output Vat Code', JVCO_Default_Value__c = 'GB-O-STD-LIC'));
        
        insert bgMatchingList;
        
        List<c2g__codaDimension3__c> dimensions = new List<c2g__codaDimension3__c>();
        dimensions.add(new c2g__codaDimension3__c(Name = 'Key', c2g__ReportingCode__c = 'Key'));
        dimensions.add(new c2g__codaDimension3__c(Name = 'Non Key', c2g__ReportingCode__c = 'Non Key'));
        insert dimensions;
    }

    static {
        JVCO_MergeRecord.debug = true;
    }

    public static testMethod void testErrorHandling() {
        // c2g__codaTaxCode__c taxCode = [SELECT Id FROM c2g__codaTaxCode__c order by CreatedDate DESC limit 1];
         
        // List<Account> accs = new List<Account>();
        // accs.add(new Account(Name='master', c2g__CODAOutputVATCode__c = taxCode.id, recordTypeId = CustomerRT));
        // accs.add(new Account(Name='slave1', c2g__CODAOutputVATCode__c = taxCode.id, recordTypeId = CustomerRT));
        // accs.add(new Account(Name='slave2', c2g__CODAOutputVATCode__c = taxCode.id, recordTypeId = CustomerRT));
        // accs.add(new Account(Name='slave3', c2g__CODAOutputVATCode__c = taxCode.id, recordTypeId = CustomerRT));
        // insert(accs);

        List<Account> accs = [SELECT Id, Name FROM Account LIMIT 2];

        List<SObject> cons = new List<Contact>();
        cons.add(new Contact(LastName=accs[0].name, FirstName='contact', accountId=accs[0].Id));
        insert(cons);

        try {
            JVCO_MergeRecord.mergeSObject(new Account[] {accs[0]});
        } catch (Exception e) {
            System.debug(e);
        }

        try {
            JVCO_MergeRecord.mergeSObject(accs, true);
        } catch (Exception e) {
            System.debug(e);
        }
    }

    public static testMethod void testMergeAccount() {
        
        //c2g__codaTaxCode__c taxCode = [SELECT Id FROM c2g__codaTaxCode__c order by CreatedDate DESC limit 1];
        //List<Account> accs = new List<Accoun>();
        
        List<Account> accs = [SELECT Id, Name FROM Account LIMIT 2];

        //accs.add(new Account(Name='master', c2g__CODAOutputVATCode__c = taxCode.id, recordTypeId = CustomerRT));
        //accs.add(new Account(Name='slave1', c2g__CODAOutputVATCode__c = taxCode.id, recordTypeId = CustomerRT));
        //accs.add(new Account(Name='slave2', c2g__CODAOutputVATCode__c = taxCode.id, recordTypeId = CustomerRT));
        //insert(accs);
        List<Contact> cons = new List<Contact>();
        List<Note> notes = new List<Note>();
        for (Account a : accs) {
           cons.add(new Contact(LastName=a.name, FirstName='contact', accountId=a.Id));
           notes.add(new Note(Title=a.name, parentId=a.Id));
        }
        insert(cons);
        insert(notes);
        /*
        List<Case> cases = new List<Case>();
        for (Contact c : cons) {
            cases.add(new Case(contactId=c.Id));
        }
        insert(cases);
        */

        JVCO_SchemaInfo schema = new JVCO_SchemaInfo();
        schema.getObjects();
        schema.getFields('Account');
        schema.getNameField(Account.sObjectType);
        schema.getChildRelationships(Account.sObjectType);
        schema.getNonReparentableChild(Account.sObjectType);
        schema.getPersonAccFields();
            
            
        Test.startTest();
        
        //JVCO_MergeRecord.mergeSObject(accs, true, new String[] {'Notes'});
        Test.stopTest();
    }

    public static testMethod void testMergeVenue() {

        List<SObject> sObjectList = new List<SObject>();
        List<JVCO_Venue__c> venueList = [SELECT Id, Name FROM JVCO_Venue__c LIMIT 2];
        sObjectList.addAll((List<sObject>)venueList);

        
        JVCO_Artist__c art = new JVCO_Artist__c();
        art.Name = 'Artest';
        insert art;

        JVCO_Event__c evt = new JVCO_Event__c();
        evt.Name = 'Test Evt';
        evt.JVCO_Artist__c = art.Id;
        evt.JVCO_Venue__c = venueList[1].Id;
        evt.JVCO_Event_Type__c = 'Concert';
        evt.JVCO_Event_Start_Date__c = Date.today();
        evt.JVCO_Event_End_Date__c = Date.today().addYears(1);
        evt.JVCO_Event_Name_Override__c = '.';
        evt.JVCO_Tariff_Code__c = 'BF';
        evt.Headline_Type__c = 'No Headliner';
        evt.JVCO_Number_of_Set_Lists_Expected__c = 2;
        insert evt;

        Test.startTest();
        JVCO_MergeRecord.mergeSObject(sObjectList, true, new String []{'Events__r'});
        Test.stopTest();
    }

    public static testMethod void testMergeVenue2() {

        List<SObject> sObjectList = new List<SObject>();
        List<JVCO_Venue__c> venueList = [SELECT Id, Name FROM JVCO_Venue__c LIMIT 2];
        sObjectList.addAll((List<sObject>)venueList);

        
        JVCO_Artist__c art = new JVCO_Artist__c();
        art.Name = 'Artest';
        insert art;

        JVCO_Event__c evt = new JVCO_Event__c();
        evt.Name = 'Test Evt';
        evt.JVCO_Artist__c = art.Id;
        evt.JVCO_Venue__c = venueList[1].Id;
        evt.JVCO_Event_Type__c = 'Concert';
        evt.JVCO_Event_Start_Date__c = Date.today();
        evt.JVCO_Event_End_Date__c = Date.today().addYears(1);
        evt.JVCO_Event_Name_Override__c = '.';
        evt.JVCO_Tariff_Code__c = 'BF';
        evt.JVCO_EventStDate_Text__c = '';
        evt.Headline_Type__c = 'No Headliner';
        evt.JVCO_Number_of_Set_Lists_Expected__c = 2;
        insert evt;

        Test.startTest();
        User keyAccMngr = JVCO_TestClassObjectBuilder.createUser('Custom Read Only');
        insert keyAccMngr;

        System.runAs(keyAccMngr){
            try{
                JVCO_MergeRecord.mergeSObject(sObjectList, true, new String []{'Events__r'});
            }
            catch(Exception e){}
        }
        Test.stopTest();
    }

    public static testMethod void testMergeVenue3() {

        List<SObject> sObjectList = new List<SObject>();
        List<JVCO_Venue__c> venueList = [SELECT Id, Name FROM JVCO_Venue__c LIMIT 2];
        sObjectList.addAll((List<sObject>)venueList);

        
        JVCO_Artist__c art = new JVCO_Artist__c();
        art.Name = 'Artest';
        insert art;

        JVCO_Event__c evt = new JVCO_Event__c();
        evt.Name = 'Test Evt';
        evt.JVCO_Artist__c = art.Id;
        evt.JVCO_Venue__c = venueList[1].Id;
        evt.JVCO_Event_Type__c = 'Concert';
        evt.JVCO_Event_Start_Date__c = Date.today();
        evt.JVCO_Event_End_Date__c = Date.today().addYears(1);
        evt.JVCO_Event_Name_Override__c = '.';
        evt.JVCO_Tariff_Code__c = 'BF';
        evt.JVCO_EventStDate_Text__c = '';
        evt.Headline_Type__c = 'No Headliner';
        evt.JVCO_Number_of_Set_Lists_Expected__c = 2;
        insert evt;

        List<JVCO_Event__c> evtList = new List<JVCO_Event__c>();
        evtList.add(evt);

        List<SObject> sObjectList2 = new List<SObject>();
        sObjectList2.addAll((List<sObject>)evtList);

        Test.startTest();
        User keyAccMngr = JVCO_TestClassObjectBuilder.createUser('Custom Read Only');
        insert keyAccMngr;

        System.runAs(keyAccMngr){
            //JVCO_MergeRecord.mergeSObject(sObjectList, true, new String []{'Events__r'});
            JVCO_MergeRecord.reparentChild(sObjectList, sObjectList);
        }
        Test.stopTest();
    }

    /*
    public static testMethod void testMergeContact() {
        Account acc1 = new Account(Name='acc1');
        Account acc2 = new Account(Name='acc2');
        insert(new Account[] {acc1, acc2});
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(LastName=acc1.name, FirstName='contact', accountId=acc1.Id));
        cons.add(new Contact(LastName=acc2.name, FirstName='contact1', accountId=acc2.Id));
        cons.add(new Contact(LastName=acc2.name, FirstName='contact2', accountId=acc2.Id));
        insert(cons);
        List<Case> cases = new List<Case>();
        for (Contact c : cons) {
            cases.add(new Case(contactId=c.Id));
        }
        insert(cases);
        JVCO_MergeRecord.mergeSObject(cons, true);
    }

    public static testMethod void testMergeOpportunity() {
        Account acc1 = new Account(Name='acc1');
        Account acc2 = new Account(Name='acc2');
        insert(new Account[] {acc1, acc2});
        List<Opportunity> opps = new List<Opportunity>();
        opps.add(new Opportunity(Name=acc1.name + ' opp1', StageName='Open', CloseDate=System.today(), accountId=acc1.Id));
        opps.add(new Opportunity(Name=acc2.name + ' opp1', StageName='Open', CloseDate=System.today(), accountId=acc2.Id));
        opps.add(new Opportunity(Name=acc2.name + ' opp2', StageName='Open', CloseDate=System.today(), accountId=acc2.Id));
        insert(opps);

        Pricebook2 pb = [select Id from Pricebook2 where isStandard=true];
        Product2 prod = new Product2(Name='TestProd', IsActive=true, CanUseQuantitySchedule=true, CanUseRevenueSchedule=true);
        insert(prod);
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id=pb.Id, Product2Id=prod.Id, IsActive=true, UnitPrice=10);
        insert(pbe);

        List<OpportunityLineItem> lineItems = new List<OpportunityLineItem>();
        for (Opportunity o : opps) {
            lineItems.add(new OpportunityLineItem(OpportunityId=o.Id, PriceBookEntryId=pbe.Id, Quantity=1, UnitPrice=10));
        }
        insert(lineItems);
        List<OpportunityLineItemSchedule> schedules = new List<OpportunityLineItemSchedule>();
        for (OpportunityLineItem oli : lineItems) {
            schedules.add(new OpportunityLineItemSchedule(OpportunityLineItemId=oli.Id, Type='Both', Quantity=1.0, Revenue=100.0, ScheduleDate=System.today()));
            schedules.add(new OpportunityLineItemSchedule(OpportunityLineItemId=oli.Id, Type='Both', Quantity=1.0, Revenue=100.0, ScheduleDate=System.today()+1));
        }
        insert(schedules);

        JVCO_MergeRecord.mergeSObject(opps, true);
    }

    public static testMethod void testMergeCustomObject() {
        Account acc1 = new Account(Name='acc1');
        Account acc2 = new Account(Name='acc2');
        insert(new Account[] {acc1, acc2});
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(LastName=acc1.name, FirstName='contact1', accountId=acc1.Id));
        cons.add(new Contact(LastName=acc2.name, FirstName='contact2', accountId=acc2.Id));
        insert(cons);

        List<My_Obj__c> objs = new List<My_Obj__c>();
        Integer i = 1;
        for (Contact c : cons) {
            objs.add(new My_Obj__c(name='obj_'+ i++, account__c=c.accountId, contact__c=c.Id));
        }
        insert(objs);
        JVCO_MergeRecord.mergeSObject(objs);

        JVCO_MergeRecord.mergeSObject(new Account[] {acc1, acc2});
    }
    */
}