@isTest
private class JVCO_LinkPositiveInvoiceTest {

  @testSetup static void setTestData(){
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
            listQueue.add(q2);
            queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
            listQueue.add(q3);
            queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
            listQueue.add(q4);
            queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
            listQueue.add(q5);
            queuesobject q6 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankAccount__c'); 
            listQueue.add(q6);
            queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q7);
            queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatement__c'); 
            listQueue.add(q8);
            queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransaction__c'); 
            listQueue.add(q9);
            queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCreditNote__c'); 
            listQueue.add(q10);
            queuesobject q11 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
            listQueue.add(q11);
            //queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntry__c'); 
            //listQueue.add(q9);
            //queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntryLineItem__c'); 
            //listQueue.add(q10);
            //queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatementLineItem__c'); 
            //listQueue.add(q10);
            //queuesobject q12 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatementLineItem__c'); 
            //listQueue.add(q12);           
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }

        
        Test.startTest();
        c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
        testCompany.c2g__CashMatchingCurrencyMode__c = 'Account';
        testCompany.c2g__Country__c = 'GB';
        testCompany.c2g__ECCountryCode__c = 'GB';
        testCompany.c2g__UnitOfWork__c = 1.0;
        testCompany.Name = 'Test Company';
        testCompany.ownerid = testGroup.Id;
        insert testCompany;

        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc10040 = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc10040.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc10040.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc10040.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc10040.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc10040.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc10040.c2g__ReportingCode__c = '10040';
        testGeneralLedgerAcc10040.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc10040.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc10040.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc10040.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc10040.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc10040.Dimension_1_Required__c = false;
        testGeneralLedgerAcc10040.Dimension_2_Required__c = false;
        testGeneralLedgerAcc10040.Dimension_3_Required__c = false;
        testGeneralLedgerAcc10040.Dimension_4_Required__c = false;
        testGeneralLedgerAcc10040.Name = '10040 - Accounts receivables control';
        testGeneralLedgerAcc10040.ownerid = testGroup.Id;
        insert testGeneralLedgerAcc10040;

        //Create General Ledger Account for Income Control
        c2g__codaGeneralLedgerAccount__c testIcGla = new c2g__codaGeneralLedgerAccount__c();
        testIcGla.c2g__ReportingCode__c = '30030';
        testIcGla.c2g__TrialBalance1__c = 'Liabilities';
        testIcGla.c2g__TrialBalance2__c = 'Income Control';
        testIcGla.c2g__TrialBalance3__c = 'Short-term income control account';
        testIcGla.c2g__Type__c = 'Balance Sheet';
        testIcGla.Name = '30030 - Income Control';
        testIcGla.ownerid = testGroup.Id;
        insert testIcGla;

        c2g__codaGeneralLedgerAccount__c testGeneralLedger = JVCO_TestClassObjectBuilder.createGeneralLedger();
        insert testGeneralLedger;

        c2g__codaTaxCode__c testTaxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedger.Id);
        insert testTaxCode;

        c2g__codaTaxRate__c testTaxRate = new c2g__codaTaxRate__c();
        testTaxRate.c2g__Rate__c = 20.0;
        testTaxRate.c2g__UnitOfWork__c = 1.0;
        testTaxRate.c2g__TaxCode__c = testTaxCode.id;
        insert testTaxRate;

        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        insert dt;

        c2g__codaCashMatchingSettings__c settings = new c2g__codaCashMatchingSettings__c();
        settings.JVCO_Write_off_Limit__c = 1.00;
        insert settings;

        c2g__codaDimension2__c testDimension2 = new c2g__codaDimension2__c();
        testDimension2.name = 'Paid at PPL';
        testDimension2.c2g__ReportingCode__c = 'Paid at PPL';
        testDimension2.c2g__UnitOfWork__c = 1.0;
        insert testDimension2;

        c2g__codaDimension2__c testDimension3 = new c2g__codaDimension2__c();
        testDimension3.name = 'Non Key';
        testDimension3.c2g__ReportingCode__c = 'Non Key';
        testDimension3.c2g__UnitOfWork__c = 2.0;
        insert testDimension3;

        c2g__codaAccountingCurrency__c accCurrency = new c2g__codaAccountingCurrency__c();
        accCurrency.c2g__OwnerCompany__c = testCompany.id;
        accCurrency.c2g__DecimalPlaces__c = 2;
        accCurrency.Name = 'GBP';
        accCurrency.c2g__Dual__c = true ;
        accCurrency.ownerid = testGroup.Id;
        accCurrency.c2g__Home__c = true;
        accCurrency.c2g__UnitOfWork__c = 2.0;
        insert accCurrency;

        Product2 prod1 = new Product2();
        prod1.Name = 'PPL Product';
        prod1.SBQQ__ChargeType__c= 'One-Time';
        prod1.JVCO_Society__c = 'PPL';
        prod1.ProductCode = 'PPLPP085';
        prod1.IsActive = true;
        prod1.c2g__CODASalesRevenueAccount__c = testIcGla.Id;
        insert prod1;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Customer Account 83202-C';
        a1.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc10040.id;
        a1.Type = 'Key Account';
        insert a1;

        //Create Contact
        Contact testContact = JVCO_TestClassObjectBuilder.createContact(a1.Id);
        testContact.Firstname = 'Test';
        testContact.LastName = 'TestContact';
        insert testContact;
        
        Account a2 = new Account();
        a2.RecordTypeId = licenceRT;
        a2.Name = 'Test Account 83202-L';

        a2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
        a2.JVCO_Customer_Account__c = a1.id;
        a2.c2g__CODAOutputVATCode__c = testTaxCode.id;
        a2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc10040.id;
        //a2.JVCO_Customer_Account__c = a1.Id;
        a2.JVCO_Preferred_Contact_Method__c = 'Email';
        a2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        a2.JVCO_Billing_Contact__c = testContact.id;
        a2.JVCO_Licence_Holder__c = testContact.id;
        a2.JVCO_Review_Contact__c = testContact.id;
        a2.Type = 'Key Account';
        insert a2;

        JVCO_SLA_Constants__c cs = JVCO_TestClassObjectBuilder.createSLAConstants();
        insert cs;

        Entitlement testEntitlement = JVCO_TestClassObjectBuilder.createEntitlement(a2.id);
        insert testEntitlement;

        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();
        venueRecord.Name = 'test1';   
        venueRecord.JVCO_Postcode__c = 'TN32 4SL';           
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = new JVCO_Affiliation__c();
        affilRecord.JVCO_Account__c = a2.Id;
        affilRecord.JVCO_Venue__c = venueRecord.id;
        affilRecord.JVCO_Start_Date__c = system.today();      
        Insert  affilRecord;
        
        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 83202';
        o.AccountId = a2.Id;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o; 

        Pricebook2 testPB2 = new Pricebook2();
        testPB2.name = 'Standard Price Book';
        testPB2.IsActive = true;
        insert testPB2;        

        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Renewal';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        insert q;


        //SBQQ__QuoteLineGroup__c testQLG = [select id, JVCO_Affiliated_Venue__c, JVCO_Venue__c from SBQQ__QuoteLineGroup__c order by CreatedDate DESC limit 1];
        JVCO_TestClassObjectBuilder.createBillingConfig(); //jason

        Contract contr = new Contract();
        contr.AccountId = a2.Id;
        contr.Status = 'Draft';
        contr.StartDate = Date.today();
        contr.ContractTerm = 12;
        contr.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
        contr.SBQQ__RenewalQuoted__c = FALSE;
        contr.SBQQ__Quote__c = q.id;
        contr.SBQQ__Opportunity__c = o.id;
        insert contr;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        ql.SBQQ__ChargeType__c= 'One-Time';
        insert ql;

        PriceBookEntry testPB = new PriceBookEntry();
        testPB.Pricebook2Id = test.getStandardPriceBookId();
        testPB.product2id = prod1.id;
        testPB.IsActive = true;
        testPB.UnitPrice = 1000.0;
        insert testPB;

        //system.assertEquals(0.00, testOrderItem.SBQQ__TotalAmount__c,'Error');


        c2g__codaYear__c yr= new c2g__codaYear__c();
        yr.Name ='2016';
        yr.c2g__AutomaticPeriodList__c =  true;
        yr.c2g__OwnerCompany__c = testCompany.id;
        yr.c2g__ExternalId__c = 'yzsd1234';
        yr.c2g__NumberOfPeriods__c =11;
        yr.c2g__StartDate__c =  system.today() - 10;
        yr.c2g__Status__c = 'Open';
        yr.c2g__PeriodCalculationBasis__c = '445';
        yr.c2g__YearEndMode__c = 'Full Accounting Code' ; 
        yr.c2g__UnitOfWork__c = 12;
        yr.ownerid = testGroup.Id;
        insert yr;

        c2g__codaPeriod__c testPeriod = new c2g__codaPeriod__c();
        testPeriod.c2g__OwnerCompany__c = testCompany.id;
        testPeriod.c2g__StartDate__c = System.today()-10;
        testPeriod.c2g__EndDate__c = System.today()+10;
        testPeriod.c2g__PeriodNumber__c ='123';
        testPeriod.c2g__Description__c ='test Desc';
        testPeriod.c2g__PeriodGroup__c = 'Q1';
        testPeriod.c2g__PeriodNumber__c = '1';
        testPeriod.c2g__YearName__c = yr.id;
        insert testPeriod;

        c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c();
        userCompany.c2g__Company__c = testCompany.id;
        userCompany.c2g__User__c = userInfo.getUserId();
        userCompany.c2g__ExternalId__c = 'ABCDE1234567876';
        userCompany.c2g__UnitOfWork__c = 111 ;
        insert userCompany;
        
        Test.stopTest();


    }

    @isTest
    static void linkInvoice(){

        

        List<RecordType> rtypeInvoice = [SELECT Name, Id FROM RecordType WHERE sObjectType='c2g__codaInvoice__c' AND isActive=true];
        Map<String,String> invoiceRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypeInvoice)
                {
                  invoiceRecordTypes.put(rt.Name,rt.Id);
                }

        id customerId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        id licenceId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 

        Account testCustomerAccount = [select id, name from Account where RecordTypeId = :customerId order by CreatedDate DESC limit 1];
        Account testLicenceAccount = [select id from Account where RecordTypeId = :licenceId order by CreatedDate DESC limit 1];

        Entitlement testEntitlement= [select id from Entitlement order by CreatedDate DESC limit 1];
        Id userId = UserInfo.getUserId();
        GroupMember testGM = [SELECT UserOrGroupId, GroupId FROM GroupMember where UserOrGroupId = :userId limit 1];

        SBQQ__Quote__c testQuote = [SELECT Id FROM SBQQ__Quote__c order by CreatedDate DESC limit 1];
        PriceBookEntry testPB = [SELECT Id FROM PriceBookEntry order by CreatedDate DESC limit 1];
        SBQQ__QuoteLine__c testQuoteLine = [SELECT Id FROM SBQQ__QuoteLine__c order by CreatedDate DESC limit 1];

        Contract testContract = [select id from Contract order by CreatedDate DESC limit 1];
        c2g__codaCompany__c testCompany = [select id, ownerid from c2g__codaCompany__c order by CreatedDate DESC limit 1];
        c2g__codaPeriod__c testPeriod  = [select id from c2g__codaPeriod__c order by CreatedDate DESC limit 1];
        c2g__codaAccountingCurrency__c accCurrency  = [select id from c2g__codaAccountingCurrency__c order by CreatedDate DESC limit 1];
        Product2 testProduct = [select id from Product2 order by CreatedDate DESC limit 1];
        //JVCO_Venue__c testVenue = [select id from JVCO_Venue__c order by CreatedDate DESC limit 1];
        JVCO_Affiliation__c testAffil = [select id, JVCO_Venue__c from JVCO_Affiliation__c order by CreatedDate DESC limit 1];
        //SBQQ__QuoteLineGroup__c testQLG = [select id, JVCO_Affiliated_Venue__c, JVCO_Venue__c from SBQQ__QuoteLineGroup__c order by CreatedDate DESC limit 1];

                test.startTest();

                Order testOrder = new Order();
                testorder.Accountid = testLicenceAccount.id;
                testorder.effectivedate = date.today();
                testOrder.SBQQ__Quote__c = testQuote.id;
                testOrder.blng__BillingAccount__c = testLicenceAccount.id;
                testOrder.SBQQ__PaymentTerm__c = 'Net 30';
                testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
                testOrder.status = 'Draft';
                testOrder.Pricebook2Id = test.getStandardPriceBookId();
                insert testOrder;  

                OrderItem testOrderItem = new OrderItem();
                testOrderItem.OrderId = testOrder.id;
                testOrderItem.PriceBookEntryId = testPB.Id;
                testOrderItem.SBQQ__QuoteLine__c = testQuoteLine.id;
                testOrderItem.SBQQ__ChargeType__c = 'One-Time';
                testOrderItem.Quantity = 100.0;
                testOrderItem.UnitPrice = 100.00;
                testOrderItem.EndDate = date.today().addMonths(5);
                //testOrderItem.blng__OverrideNextBillingDate__c = null;
                //testOrderItem.blng__BillThroughDateOverride__c = null;
                insert testOrderItem;

                testOrderItem.blng__OverrideNextBillingDate__c = null;
                testOrderItem.blng__BillThroughDateOverride__c = null;
                testOrderItem.SBQQ__TotalAmount__c = 1000;
                update testOrderItem;

                c2g__codaInvoice__c testSalesInvoice = new c2g__codaInvoice__c();
                testSalesInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
                testSalesInvoice.c2g__InvoiceDate__c = date.today();
                testSalesInvoice.c2g__DueDate__c = date.today().addMonths(7);
                testSalesInvoice.c2g__Account__c = testLicenceAccount.id;
                testSalesInvoice.JVCO_Customer_Type__c = 'New Business';
                testSalesInvoice.c2g__OwnerCompany__c = testCompany.id;
                testSalesInvoice.JVCO_Generate_Payment_Schedule__c = false;
                testSalesInvoice.c2g__Period__c = testPeriod.id;
                testSalesInvoice.JVCO_Invoice_Legacy_Number__c = '001';
                testSalesInvoice.RecordTypeId = invoiceRecordTypes.get('Unpaid Invoice');
                testSalesInvoice.OwnerId = testGM.GroupId;
                insert testSalesInvoice;

                c2g__codaInvoiceLineItem__c testInvoiceItem = new c2g__codaInvoiceLineItem__c();
                testInvoiceItem.c2g__Product__c = testProduct.id;
                testInvoiceItem.c2g__OwnerCompany__c = testCompany.id;
                testInvoiceItem.c2g__Invoice__c = testSalesInvoice.id;
                //testInvoiceItem.c2g__TaxCode1__c = testTaxCode.id;
                //testInvoiceItem.cg2__Quantity__c = 1.0;
                testInvoiceItem.c2g__UnitPrice__c = 200.0;
                insert testInvoiceItem;


                //testSalesInvoice.JVCO_Generate_Payment_Schedule__c = true;
                //testSalesInvoice.JVCO_Number_of_Payments__c = 5.0 ;
                //testSalesInvoice.JVCO_First_Due_Date__c = date.today().addDays(5); 
                //update testSalesInvoice;


                blng__Invoice__c testInvoice = new blng__Invoice__c();
                testInvoice.blng__Account__c = testOrder.accountid;
                testInvoice.blng__Order__c = testOrder.id;
                testInvoice.blng__InvoiceDate__c = Date.today();
                testInvoice.blng__InvoiceStatus__c = 'Draft';
                testInvoice.JVCO_Invoice_Type__c = 'Standard';
                testInvoice.JVCO_Original_Invoice_Cancelled__c = true;
                testInvoice.JVCO_Sales_Credit_Note__c = null;
                testInvoice.JVCO_Sales_Invoice__c = testSalesInvoice.id;
                insert testInvoice;

                

                testInvoice.JVCO_Temp_Legacy_Contracts__c = 'TEST-001';
                testInvoice.JVCO_Legacy_Contracts__c = 'TEST-001';
                testContract.JVCO_Contract_Temp_External_ID__c = 'TEST-001';
                update testContract;

                blng__InvoiceLine__c testInvoiceLine = new blng__InvoiceLine__c();
                testInvoiceLine.blng__Product__c = testProduct.id;
                testInvoiceLine.blng__StartDate__c = date.today();
                testInvoiceLine.JVCO_Affiliation__c = testAffil.id;
                testInvoiceLine.JVCO_Venue__c =testAffil.JVCO_Venue__c;
                testInvoiceLine.blng__Invoice__c = testInvoice.id;
                testInvoiceLine.blng__OrderProduct__c = testOrderItem.id;
                testInvoiceLine.blng__TotalAmount__c = -10000.0;
                testInvoiceLine.JVCO_Dynamic_Paid_Amount__c = 100.0;
                testInvoiceLine.JVCO_Paid_Amount__c = 150.0;
                testInvoiceLine.blng__Subtotal__c = -10000.0;
                testInvoiceLine.JVCO_Surcharge_Applicable__c = true;
                insert testInvoiceLine;



                c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
                c2g.CODAAPICommon_10_0.Context contextInvoice = new c2g.CODAAPICommon_10_0.Context();
                c2g.CODAAPICommon.Reference invoiceRef = c2g.CODAAPICommon.getRef(testSalesInvoice.Id, null); 
                //c2g.CODAAPISalesInvoice_10_0.PostInvoice(contextInvoice, invoiceRef);


                c2g__codaInvoice__c selectSalesInvoice = [select id, OwnerId, c2g__PaymentStatus__c, c2g__Account__c, c2g__InvoiceCurrency__c, c2g__Period__c from c2g__codaInvoice__c order by CreatedDate DESC limit 1];
                //system.assertEquals(null, selectSalesInvoice.c2g__PaymentStatus__c);
                //system.assertEquals(testGM.GroupId, UserInfo.getUserId(), 'Curre User: ' + UserInfo.getUserId() + ' GM: ' + testGM.GroupId + 'SalesInvoice ' + selectSalesInvoice.OwnerId );

                
                //c2g__codaCreditNote__c testCreditNote = [select id, c2g__Invoice__c, JVCO_Temp_Link_Positive_Invoice__c from c2g__codaCreditNote__c order by CreatedDate DESC limit 1];
                //testCreditNote.JVCO_Temp_Link_Positive_Invoice__c = '001';
                //update testCreditNote;
                 c2g__codaCreditNote__c testCreditNote = new c2g__codaCreditNote__c();
                testCreditNote.c2g__Invoice__c = selectSalesInvoice.Id;
                testCreditNote.c2g__Account__c = selectSalesInvoice.c2g__Account__c;
                testCreditNote.c2g__DueDate__c = Date.today();
                testCreditNote.c2g__CreditNoteCurrency__c = selectSalesInvoice.c2g__InvoiceCurrency__c;
                testCreditNote.c2g__Period__c = selectSalesInvoice.c2g__Period__c;
                testCreditNote.c2g__CreditNoteDate__c = Date.today();
                testCreditNote.JVCO_Credit_Reason__c = 'Invoice amendment';
                testCreditNote.JVCO_Temp_Link_Positive_Invoice__c = '001';
                insert testCreditNote;
                
                testInvoice.JVCO_Sales_Credit_Note__c = testCreditNote.id;
                testInvoice.blng__InvoiceStatus__c = 'Posted';
                update testInvoice;

                //OrderItem testOrderItem = [select id, blng__NextBillingDate__c from OrderItem order by CreatedDate DESC limit 1];
                testOrderItem.blng__NextBillingDate__c = system.today();
                testOrderItem.blng__NextChargeDate__c = system.today().addDays(10);
                update testOrderItem;

                list<blng__Invoice__c> listBlngInvoice = new list<blng__Invoice__c>();
                listBlngInvoice.add(testInvoice);

                
                //system.assertEquals(null, testOrderItem.blng__NextBillingDate__c , testOrderItem.blng__NextChargeDate__c + ' s ' + testInvoice.JVCO_Sales_Invoice__c + ' c ' + testInvoice.JVCO_Sales_Credit_Note__c + ' ' + testInvoice.blng__InvoiceStatus__c );
                //system.assertNotEquals(null, testOrderItem.blng__NextBillingDate__c , testOrderItem.blng__NextChargeDate__c);

                //blng__Invoice__c selInvoice = new blng__Invoice__c();
                //selInvoice = [select id, blng__Subtotal__c,JVCO_Total_Amount__c from blng__Invoice__c order by CreatedDate DESC limit 1];

                
                JVCO_LinkPositiveInvoice obj = new JVCO_LinkPositiveInvoice();
                Database.executeBatch(obj);

        test.StopTest();
    }
}