@isTest
private class TriggerHandler_CardPaymentHistoryTest
{
    @isTest
    private static void testOnBeforeInsert()
    {
        TriggerHandler_CardPaymentHistory.OnBeforeInsert(createTestBeforeInsertList());
    }

    private static List<Income_Card_Payment_History__c> createTestBeforeInsertList()
    {   
        Account acc = new Account();
        acc.Name = 'Test';
        insert acc;
        
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        //insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        //insert vatGLA;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        //insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        //insert taxRate;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
		
        JVCO_Invoice_Group__c invoiceGroup = new JVCO_Invoice_Group__c(
                                        JVCO_Total_Amount__c = 10,
                                        JVCO_Paper_Debit__c = false);
        insert invoiceGroup;
        
        c2g__codaInvoice__c testSalesInvoice = JVCO_TestClassHelper.getSalesInvoice(acc.Id);
        testSalesInvoice.c2g__OwnerCompany__c = comp.Id;
        testSalesInvoice.JVCO_Payment_Method__c = 'Direct Debit';
        testSalesInvoice.JVCO_Invoice_Group__c = invoiceGroup.Id;
        insert testSalesInvoice;
        
        Income_Card_Payment__c testCardPayment = new Income_Card_Payment__c(); 
        testCardPayment.Payment_Reason__c = testSalesInvoice.Id;
        testCardPayment.Account__c = acc.Id;
        insert testCardPayment;

        Income_Card_Payment_History__c testHistory = new Income_Card_Payment_History__c();
        testHistory.Invoices_Paid__c = 'Invoice';
        testHistory.Payment_Status__c = 'Authorised';
        testHistory.Transaction_Type__c = 'Payment';
        testHistory.Income_Card_Payment__c = testCardPayment.Id;
        insert testHistory;

        List<Income_Card_Payment_History__c> historyList = new List<Income_Card_Payment_History__c>();

        historyList.add(testHistory);

        return historyList;
    }
}