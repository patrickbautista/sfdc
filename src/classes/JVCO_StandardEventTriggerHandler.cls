/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_StandardEventTriggerHandler.cls 
   Description:     Handler Class for JVCO_StandardEventTrigger
   Test class:      JVCO_StandardEventTriggerHandlerTest.cls 

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
  05-Apr-2018       0.1         Accenture-recuerdo.b.bregente     Initial draft           
  ---------------------------------------------------------------------------------------------------------- */
Public Class JVCO_StandardEventTriggerHandler{
    
    public static void onBeforeInsert(List<Event> events){
        List<Event> eventToProcessForRestriction = new List<Event>();
        Set<Id> accountIdsRestriction = new Set<Id>();
        
        for(Event eventRec : events){
            if(eventRec.WhatId <> null){
                accountIdsRestriction.add(eventRec.WhatId);
                eventToProcessForRestriction.add(eventRec);
            }
        }
        restrictOnInfringementOrEnforcement(eventToProcessForRestriction, accountIdsRestriction);
    }
    
    public static void onBeforeUpdate(List<Event> events){
        List<Event> eventToProcessForRestriction = new List<Event>();
        Set<Id> accountIdsRestriction = new Set<Id>();
        
        for(Event eventRec : events){
            if(eventRec.WhatId <> null){
                accountIdsRestriction.add(eventRec.WhatId);
                eventToProcessForRestriction.add(eventRec);
            }
        }
        restrictUpdateAndDelete(eventToProcessForRestriction, accountIdsRestriction, 'Updating');
    }
    
    public static void onBeforeDelete(List<Event> events){
        List<Event> eventToProcessForRestriction = new List<Event>();
        Set<Id> accountIdsRestriction = new Set<Id>();
        
        for(Event eventRec : events){
            if(eventRec.WhatId <> null){
                accountIdsRestriction.add(eventRec.WhatId);
                eventToProcessForRestriction.add(eventRec);
            }
        }
        restrictUpdateAndDelete(eventToProcessForRestriction, accountIdsRestriction, 'Deleting');
    }
    
    private static void restrictOnInfringementOrEnforcement(List<Event> events, Set<Id> accountIdsRestriction){
        Map<Id, Account> restrictedAccountsMap = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN :accountIdsRestriction AND 
                                                    (JVCO_In_Enforcement__c = true OR JVCO_In_Infringement__c = true) AND 
                                                    (JVCO_Credit_Status__c = 'Enforcement - Debt Recovery' OR JVCO_Credit_Status__c = 'Enforcement - Infringement')]);
        
                                                            
        Boolean isRestricted = (restrictedAccountsMap.size() > 0 && checkUserIfRestricted());
       
        for(Event eventRec : events){
            if(isRestricted && restrictedAccountsMap.containsKey(eventRec.WhatId)){
                eventRec.addError('Creating Event on Accounts marked "In Enforcement" or "In Infringement" is not allowed.');
            }
        }
    }
    
    private static void restrictUpdateAndDelete(List<Event> events, Set<Id> accountIdsRestriction, String operation){
        Map<Id, Account> restrictedAccountsMap = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN :accountIdsRestriction AND 
                                                    (JVCO_In_Enforcement__c = true OR JVCO_In_Infringement__c = true) AND 
                                                    (JVCO_Credit_Status__c = 'Enforcement - Debt Recovery' OR JVCO_Credit_Status__c = 'Enforcement - Infringement')]);
        
                                                            
        Boolean isRestricted = (restrictedAccountsMap.size() > 0 && checkUserIfRestricted());
       
        for(Event eventRec : events){
            if(isRestricted && restrictedAccountsMap.containsKey(eventRec.WhatId)){
                eventRec.addError(operation + ' Event on Accounts marked "In Enforcement" or "In Infringement" is not allowed.');
            }
        }
    }
    
    
    private static Boolean checkUserIfRestricted(){
        String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
        Map<Id,PermissionSet> enAdPermSet = new Map<Id,PermissionSet>([SELECT Id FROM PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors']);
        List<PermissionSetAssignment> permissionSetList = [SELECT Id FROM PermissionSetAssignment WHERE 
                                                            AssigneeId = :UserInfo.getUserId() AND PermissionSetId IN :enAdPermSet.keySet()];
                                                            
        return (profileName <> 'System Administrator' && permissionSetList.isEmpty());
    }
}