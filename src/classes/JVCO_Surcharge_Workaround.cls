/* ----------------------------------------------------------------------------------------------
    Name: JVCO_Surcharge_Workaround
    Description: Batch for Surcharge Workaround

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    17-Apr-2018  0.1         franz.g.a.dimaapi     Intial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_Surcharge_Workaround implements Database.Batchable<sObject>
{
    private Set<Id> sInvIdSet;

    public JVCO_Surcharge_Workaround(){}
    public JVCO_Surcharge_Workaround(Set<Id> sInvIdSet)
    {
        this.sInvIdSet = sInvIdSet;
    }

    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
        if(!sInvIdSet.isEmpty())
        {
            return Database.getQueryLocator([SELECT Id FROM c2g__codaInvoice__c
                                            WHERE Id IN : sInvIdSet
                                            ]);
        }else
        {
            return null;
        }
        
    }

    public void execute(Database.BatchableContext BC, List<c2g__codaInvoice__c> sInvList) 
    {   
        surchargeWorkaroundLogic(sInvList);
    }
    
    private void surchargeWorkaroundLogic(List<c2g__codaInvoice__c> sInvList)
    {
        Map<Id, blng__Invoice__c> bInvMap = new Map<Id, blng__Invoice__c>(
                                            [SELECT Id, JVCO_Original_Invoice__r.JVCO_Sales_Invoice__c
                                            FROM blng__Invoice__c 
                                            WHERE JVCO_Invoice_Type__c = 'Surcharge'
                                            AND JVCO_Sales_Invoice__c = NULL
                                            AND JVCO_Total_Amount__c > 0
                                            AND JVCO_Original_Invoice__c != NULL
                                            AND blng__Order__c != NULL
                                            AND blng__Order__r.JVCO_Order_Group__c != NULL
                                            AND JVCO_Original_Invoice__r.JVCO_Sales_Invoice__c != NULL
                                            AND JVCO_Original_Invoice__r.JVCO_Sales_Invoice__c IN : sInvList
                                            ORDER BY JVCO_Original_Invoice__r.JVCO_Sales_Invoice__c
                                            ]);
        if(!bInvMap.isEmpty())
        {
            //JVCO_InvoiceGenerationLogic.generateSurchargeInvoiceFromOrderGroup(bInvMap);
        }
    }

    public void finish(Database.BatchableContext BC) 
    {
            
    }
  
}