/* ----------------------------------------------------------------------------------------------
Name: JVCO_LicAccGenerateAmendmentBatchTest
Description: Test Class of JVCO_LicAccGenerateAmendmentBatch

Date         Version     Author                          Summary of Changes 
-----------  -------     -----------------               -----------------------------------------
16-Jul-2020   0.1         Accenture-rhys.j.c.dela.cruz   Intial creation
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_LicAccGenerateAmendmentBatchTest
{
    @testSetup static void setupTestData() 
    {
        List<JVCO_Renewal_Settings__c> dateSettings = new List<JVCO_Renewal_Settings__c>();
        dateSettings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Completion', Days__c = 1.0));
        dateSettings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Generation', Days__c = 29));
        insert dateSettings;

        List<JVCO_Primary_Tariff_Mapping__c> settings = new List<JVCO_Primary_Tariff_Mapping__c>();
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Airport'));
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Aircraft'));
        insert settings;
        
        JVCO_TestClassObjectBuilder.createBillingConfig();

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        Product2 prod1 = new Product2();
        prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;

        Product2 prod2 = new Product2();
        prod2 = JVCO_TestClassObjectBuilder.createProduct();
        prod2.Proratable__c = true;
        insert prod2;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 83202-C';
        a1.Type = 'Key Account';

        c2g__codaDimension1__c dim = new c2g__codaDimension1__c();
        dim.c2g__ReportingCode__c = 'BC008TEST';
        dim.Name = 'General Purpose';
        insert dim;

        a1.c2g__CODADimension1__c = dim.Id;

        insert a1;
        
        Account a2 = new Account();
        a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.Id);
        insert a2;

        Pricebook2 testPB2 = new Pricebook2();
        testPB2.name = 'Standard Price Book';
        testPB2.IsActive = true;
        insert testPB2;
        
        PriceBookEntry pbe = new PriceBookEntry();
        pbe= JVCO_TestClassObjectBuilder.createPriceBookEntry(prod1.Id);
        insert pbe;
        
        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 83202';
        o.AccountId = a2.Id;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;
        
        Date tempStartDate = System.today().addMonths(-12) - 5;
        Date tempEndDate =  System.today() - 6;

        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Quote';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = tempStartDate;
        q.Start_Date__c = tempStartDate;
        q.End_Date__c = tempEndDate;
        q.SBQQ__EndDate__c = tempEndDate;
        q.SBQQ__ExpirationDate__c = tempEndDate;
        insert q;
        
        Test.startTest();

        // SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        // ql.SBQQ__Quote__c = q.Id;
        // ql.SBQQ__Product__c = prod1.Id;
        // ql.SBQQ__Quantity__c = 1;
        // ql.SBQQ__Number__c = 1;
        // ql.SBQQ__PricingMethod__c = 'List';
        // ql.SBQQ__ListPrice__c = 10;
        // ql.SBQQ__CustomerPrice__c = 10;
        // ql.SBQQ__NetPrice__c = 10;
        // ql.SBQQ__SpecialPrice__c = 10;
        // ql.SBQQ__RegularPrice__c = 10;
        // ql.SBQQ__UnitCost__c = 10;
        // ql.SBQQ__ProratedListPrice__c = 10;
        // ql.SBQQ__SpecialPriceType__c = 'Custom';
        // q.SBQQ__StartDate__c = System.today() - 5;
        // q.SBQQ__EndDate__c = System.today().addMonths(12);
        // insert ql;
        
        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();
        venueRecord.Name = 'test1';              
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = new JVCO_Affiliation__c();
        affilRecord.JVCO_Account__c = a2.Id;
        affilRecord.JVCO_Venue__c = venueRecord.id;   
        affilRecord.JVCO_Start_Date__c = system.today();   
        Insert  affilRecord;   
        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;   

        Opportunity opp = new Opportunity();
        opp.Id = o.Id;
        opp.Amount = 10;
        opp.StageName = 'Closed Won';
        opp.SBQQ__Contracted__c = true;
        update opp;

        String cat = 'AA';
        JVCO_Venue__c nvenue2 = new JVCO_Venue__c();
        nvenue2.Name = 'Test Venue '+cat;
        nvenue2.JVCO_Lead_Source__c = 'Churn';
        nvenue2.JVCO_Venue_Type__c = 'Airport';
        nvenue2.JVCO_Venue_Sub_Type__c = 'Airport';
        nvenue2.JVCO_Budget_Category__c = 'Transports & Terminals';
        nvenue2.JVCO_City__c = 'City'+cat;
        nvenue2.JVCO_Country__c = 'United Kingdom';
        nvenue2.JVCO_Street__c = 'Street'+cat;
        nvenue2.JVCO_Postcode__c = 'N'+cat+' 7TH';
        insert nvenue2;

        JVCO_Affiliation__c  affilRecord2 = new JVCO_Affiliation__c();
        affilRecord2.JVCO_Account__c = a2.Id;
        affilRecord2.JVCO_Venue__c = nvenue2.id;   
        affilRecord2.JVCO_Start_Date__c = system.today().addMonths(3);
        insert affilRecord2;
       
        cat = 'BB';
        JVCO_Venue__c nvenue3 = new JVCO_Venue__c();
        nvenue3.Name = 'Test Venue '+cat;
        nvenue3.JVCO_Lead_Source__c = 'Churn';
        nvenue3.JVCO_Venue_Type__c = 'Airport';
        nvenue3.JVCO_Venue_Sub_Type__c = 'Airport';
        nvenue3.JVCO_Budget_Category__c = 'Transports & Terminals';
        nvenue3.JVCO_City__c = 'City'+cat;
        nvenue3.JVCO_Country__c = 'United Kingdom';
        nvenue3.JVCO_Street__c = 'Street'+cat;
        nvenue3.JVCO_Postcode__c = 'N'+cat+' 7TH';
        insert nvenue3;

        JVCO_Affiliation__c  affilRecord3 = new JVCO_Affiliation__c();
        affilRecord3.JVCO_Account__c = a2.Id;
        affilRecord3.JVCO_Venue__c = nvenue3.id;   
        affilRecord3.JVCO_Start_Date__c = system.today().addMonths(13);
        insert affilRecord3;

        Contract c1 = new Contract();
        c1.SBQQ__RenewalQuoted__c = false;  
        c1.AccountID = a2.Id;
        c1.StartDate = tempStartDate;
        c1.ContractTerm = 12;
        c1.SBQQ__RenewalQuoted__c = false;
        c1.JVCO_Renewal_Generated__c = false;
        c1.JVCO_RenewableQuantity__c = 1;
        c1.EndDate = tempEndDate;
        c1.Name = 'Test Contract 18023-L';
        c1.SBQQ__Quote__c = q.Id;
        c1.SBQQ__Opportunity__c = o.Id;
        c1.AccountId = a2.Id;
        c1.JVCO_TempStartDate__c = c1.StartDate.addMonths(3);
        c1.JVCO_TempEndDate__c = c1.EndDate.addMonths(4);
        insert c1;

        SBQQ__Subscription__c sub = new SBQQ__Subscription__c();
        sub.SBQQ__Contract__c = c1.Id;
        sub.SBQQ__Account__c = a2.Id;
        sub.SBQQ__Quantity__c = 300;
        sub.SBQQ__Product__c = prod1.Id;
        sub.SBQQ__ListPrice__c = 1;
        sub.SBQQ__NetPrice__c = 300;
        sub.SBQQ__RenewalQuantity__c = 300;
        sub.ChargeYear__c = 'Current Year';
        sub.SBQQ__BillingFrequency__c = 'Annual';
        sub.SBQQ__BillingType__c = 'Advance';
        sub.SBQQ__ChargeType__c = 'Recurring';
        sub.SBQQ__CustomerPrice__c = 1;
        sub.SBQQ__ProrateMultiplier__c = 1;
        sub.SBQQ__RegularPrice__c = 1;
        sub.LicenceFee__c = 300;
        sub.Start_Date__c = c1.StartDate;
        sub.SBQQ__SubscriptionStartDate__c = c1.StartDate;
        sub.SBQQ__SubscriptionEndDate__c = c1.EndDate;
        sub.End_Date__c = c1.EndDate;

        Test.stopTest();
    }

    private static testMethod void testMethodGenerateAmendmentQuotesBatch() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a1 = [select Id, Name, Licence_Account_Number__c from Account where RecordTypeId =: licenceRT limit 1];
        
        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_LicAccGenerateAmendmentBatch(), 1);
        Test.stopTest();
    }

    private static testMethod void testMethodGenerateAmendmentQuotesBatch2() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        //Account a1 = [select Id, Name, Licence_Account_Number__c from Account where RecordTypeId =: licenceRT limit 1];
        Product2 prodRec = [SELECT Id, Proratable__c FROM Product2 LIMIT 1];
        prodRec.Proratable__c = true;
        update prodRec;
        
        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_LicAccGenerateAmendmentBatch(), 1);
        Test.stopTest();
    }

    private static testMethod void testMethodGenerateAmendmentQuotesBatch3() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        //Account a1 = [select Id, Name, Licence_Account_Number__c from Account where RecordTypeId =: licenceRT limit 1];
        Product2 prodRec = [SELECT Id, Proratable__c FROM Product2 LIMIT 1];
        prodRec.Proratable__c = true;
        update prodRec;
        
        Test.startTest();
        JVCO_LicAccGenerateAmendmentBatch licAccSched = new JVCO_LicAccGenerateAmendmentBatch ();   
        String chron = '0 0 23 * * ?';        
        system.schedule('Test Sched', chron, licAccSched);
        Test.stopTest();
        
    }
}