@isTest
private class JVCO_KeyAccountHelperSchedulableTest{

	@isTest 
	static void testContractReviewSched(){

		Account accRec = new Account();

		Boolean isReview = false;

		Integer errorQuoteCount = 0;
		Integer opportunityOriginalSize = 0;
		Integer quoteCount = 2;
		Integer tempOriginalSize = 0;

		Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
		Map<String, String> quoteStringMap = new Map<String, String>();

		Set<Id> opportunityIdSet = new Set<Id>();

		String qType = 'ContractReview';
		String queueableErrorMessage = '';
		
		Test.startTest();

		JVCO_KeyAccountHelperSchedulable keyAccHelperSched = new JVCO_KeyAccountHelperSchedulable(oppMap, accRec, quoteCount, errorQuoteCount, queueableErrorMessage, opportunityIdSet, opportunityOriginalSize, qType);
		String sch = '0 0 23 * * ?';
		system.schedule('Test KA Helper Sched', sch, keyAccHelperSched);

		Test.stopTest();
	}
	
	@isTest 
	static void testContractRenewSched(){
		
		Account accRec = new Account();

		Boolean isReview = false;

		Integer errorQuoteCount = 0;
		Integer opportunityOriginalSize = 0;
		Integer quoteCount = 2;
		Integer tempOriginalSize = 0;

		Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
		Map<String, String> quoteStringMap = new Map<String, String>();

		Set<Id> opportunityIdSet = new Set<Id>();

		String qType = 'ContractRenew';
		String queueableErrorMessage = '';
		
		Test.startTest();

		JVCO_KeyAccountHelperSchedulable keyAccHelperSched = new JVCO_KeyAccountHelperSchedulable(oppMap, accRec, quoteCount, errorQuoteCount, queueableErrorMessage, opportunityIdSet, opportunityOriginalSize, qType);
		String sch = '0 0 23 * * ?';
		system.schedule('Test KA Helper Sched', sch, keyAccHelperSched);

		Test.stopTest();
	}

	@isTest
	static void testOrderQuotesSched(){
		
		Account accRec = new Account();

		Boolean isReview = false;

		Integer errorQuoteCount = 0;
		Integer opportunityOriginalSize = 0;
		Integer quoteCount = 2;
		Integer tempOriginalSize = 0;

		Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
		Map<String, String> quoteStringMap = new Map<String, String>();

		Set<Id> opportunityIdSet = new Set<Id>();

		String qType = 'OrderQuotes';
		String queueableErrorMessage = '';
		
		Test.startTest();

		JVCO_KeyAccountHelperSchedulable keyAccHelperSched = new JVCO_KeyAccountHelperSchedulable(oppMap, accRec, quoteStringMap, tempOriginalSize, isReview, qType);
		String sch = '0 0 23 * * ?';
		system.schedule('Test KA Helper Sched', sch, keyAccHelperSched);

		Test.stopTest();
	}
}