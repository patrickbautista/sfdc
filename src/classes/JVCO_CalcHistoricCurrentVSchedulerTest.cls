/* ----------------------------------------------------------------------------------------------
    Name: JVCO_CalcHistoricCurrentVSchedulerTest
    Description: Test Class of JVCO_CalcHistoricCurrentValuesScheduler

    Date            Version         Author                                  Summary of Changes 
    -----------     -------     ----------------------          -----------------------------------------
    05-Jul-2018     0.1         Accenture-reymark.j.l.arlos     Intial creation
    20-Sep-2018     0.2         Accenture-reymark.j.l.arlos     Updated to match the new requirements GREEN-33361
----------------------------------------------------------------------------------------------- */
@isTest
public with sharing class JVCO_CalcHistoricCurrentVSchedulerTest
{
    @testSetup
    private static void testSetup()
    {
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        insert dt;  
        
        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.JVCO_NextRenewalDate__c = Date.parse('31/12/2018');
        insert acc2;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, null, null, null);
        insert q;

        SBQQ__Quote__c q2 = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, null, null, null);
        q2.SBQQ__PriceBook__c = NULL;
        insert q2;
  
        Contract c = JVCO_TestClassObjectBuilder.createContract(acc2.id, q.id);
        insert c;

        Contract c2 = JVCO_TestClassObjectBuilder.createContract(acc2.id, q.id);
        insert c2;

        Contract c3 = JVCO_TestClassObjectBuilder.createContract(acc2.id, q2.id);
        insert c3;

        JVCO_HistoricCurrentValuesSetting__c settings = new JVCO_HistoricCurrentValuesSetting__c();
        settings.JVCO_DaysCovered__c = 7;
        settings.JVCO_EnableInitialDataUpdate__c = false;
        settings.JVCO_AccountRecordTypeName__c = 'Licence Account';
        settings.JVCO_SchedulerBatchSize__c = 50;
        insert settings;
    }

    @isTest
    static void testStampCurrentValues()
    {
        String LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Account accLicence = [Select Id, JVCO_Customer_Account__c From Account where RecordTypeId = :LicenceRT limit 1];
        accLicence.JVCO_Review_Frequency__c = 'Bi-annually';
        update accLicence;

        Account accDelete = JVCO_TestClassObjectBuilder.createLicenceAccount(accLicence.JVCO_Customer_Account__c);
        accDelete.JVCO_Review_Frequency__c = 'Bi-annually';
        insert accDelete;
        
        List<Contract> contList = [SELECT Id, AccountId, StartDate, EndDate FROM Contract WHERE AccountId =:accLicence.Id order by CreatedDate DESC limit 3];

        Integer startDate = -182;
        Integer endDate = 1;
        for(Contract cont : contList)
        {
            date sd = date.today().addDays(startDate);
            date ed = date.today().addDays(endDate);
            cont.StartDate = sd;
            cont.EndDate = ed;
            cont.AccountId = accLicence.Id;
            cont.JVCO_RenewableQuantity__c = 60;
            startDate--;
            endDate++;
        }

        update contList;

        Product2 proRecPRS = JVCO_TestClassObjectBuilder.createProduct();
        proRecPRS.JVCO_Society__c = 'PRS';
        insert proRecPRS; 

        Product2 proRecPPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecPPL.JVCO_Society__c = 'PPL';
        insert proRecPPL;

        Product2 proRecVPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecVPL.JVCO_Society__c = 'VPL';
        insert proRecVPL;

        List<SBQQ__Subscription__c> subList = new List<SBQQ__Subscription__c>();

        for(Contract cont : contList)
        {
            SBQQ__Subscription__c subsPRS = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecPRS.Id);
            subsPRS.SBQQ__Contract__c = cont.Id;
            subsPRS.SBQQ__ListPrice__c = 10;
            subsPRS.SBQQ__RenewalQuantity__c = 10;
            subsPRS.SBQQ__Quantity__c = 10;
            subsPRS.SBQQ__NetPrice__c = 10;
            subsPRS.SBQQ__TerminatedDate__c = null;
            subsPRS.Start_Date__c = cont.StartDate;
            subsPRS.End_Date__c = cont.EndDate;
            subsPRS.SBQQ__SubscriptionStartDate__c = cont.StartDate;
            subsPRS.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsPRS.ChargeYear__c = 'Current Year';
            subList.add(subsPRS);

            SBQQ__Subscription__c subsPPL = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecPPL.Id);
            subsPPL.SBQQ__Contract__c = cont.Id;
            subsPPL.SBQQ__ListPrice__c = 10;
            subsPPL.SBQQ__RenewalQuantity__c = 20;
            subsPPL.SBQQ__Quantity__c = 20;
            subsPPL.SBQQ__NetPrice__c = 10;
            subsPPL.SBQQ__TerminatedDate__c = null;
            subsPPL.Start_Date__c = cont.StartDate;
            subsPPL.End_Date__c = cont.EndDate;
            subsPPL.SBQQ__SubscriptionStartDate__c =cont.StartDate;
            subsPPL.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsPPL.ChargeYear__c = 'Current Year';
            subList.add(subsPPL);

            SBQQ__Subscription__c subsVPL = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecVPL.Id);
            subsVPL.SBQQ__Contract__c = cont.Id;
            subsVPL.SBQQ__ListPrice__c = 10;
            subsVPL.SBQQ__RenewalQuantity__c = 30;
            subsVPL.SBQQ__Quantity__c = 30;
            subsVPL.SBQQ__NetPrice__c = 10;
            subsVPL.SBQQ__TerminatedDate__c = null;
            subsVPL.Start_Date__c = cont.StartDate;
            subsVPL.End_Date__c = cont.EndDate;
            subsVPL.SBQQ__SubscriptionStartDate__c =cont.StartDate;
            subsVPL.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsVPL.ChargeYear__c = 'Current Year';
            subList.add(subsVPL);
        }

        Test.startTest();
        insert subList;

        Database.MergeResult result = Database.merge(accLicence, accDelete, false);

        if(result.isSuccess())
        {
            JVCO_CalcHistoricCurrentValuesScheduler.CallExecuteMomentarily();
            Test.stopTest();
        }

        Account accLicence1 = [Select Id, JVCO_PRS_Current_Value_Total__c, JVCO_PPL_Current_Value_Total__c, JVCO_VPL_Current_Value_Total__c From Account where Id = :accLicence.Id limit 1];

        system.assertEquals(300,accLicence1.JVCO_PRS_Current_Value_Total__c);
        system.assertEquals(600,accLicence1.JVCO_PPL_Current_Value_Total__c);
        system.assertEquals(900,accLicence1.JVCO_VPL_Current_Value_Total__c);

    }

    
}