/* ----------------------------------------------------------------------------------------------
   Name: JVCO_PaymentSharedLogic.cls 
   Description: Shared logic between Credit Card and Direct Debit Payments

    Date         Version    Author              Summary of Changes 
    -----------  -----      ----------------   -----------------------------------------
    28-Dec-2016   0.1       ryan.i.r.limlingan  Intial creation
    09-Jan-2017   0.2       ryan.i.r.limlingan  Changed constructor paramters; Added common validation check used for CC and DD
    01-Feb-2017   0.3       ryan.i.r.limlingan  Added logic around Payment processing
    16-Feb-2017   0.4       ryan.i.r.limlingan  Added computeValueCC function
    28-Mar-2017   0.5       ryan.i.r.limlingan  Changed construction of URL redirection to Payonomy
    17-Apr-2017   0.4       ryan.i.r.limlingan  Changed data type of variables used in computing for collection amounts
    20-Apr-2018   0.5       mary.ann.a.ruelan   GREEN-31487. Added creating of cash entries and updated logic to create cash entries only if CLIs will be created
    07-Aug-2018   0.6       patrick.t.bautista  GREEN-32866 amended CC computaion and it is now based on CC Outstanding Mapping Object that stores all the amount
    19-Nov-2018   0.7       patrick.t.bautista  GREEN-33651 refactor class on processing DD/CC
    29-Nov-2018   0.5       patrick.t.bautista  GREEN-34038 - Removed isListValid and hasPermission method
----------------------------------------------------------------------------------------------- */
public class JVCO_PaymentSharedLogic
{
    /* INVOICE PROCESSING VARIABLES */
    // Selected Invoice records from Account page
    /*private List<c2g__codaInvoice__c> selectedInvoices;
    // Used to determine the type of Payment made
    public enum PaymentMode{
        Credit,
        Debit
    }
    private PaymentMode mode;
    public String paymentSched; // Populated from the calling class
    // Variables for Collection Amounts
    // Integer change to Decimal
    public Decimal firstColAmt; 
    public Decimal ongoingColAmt;
    public Decimal finalColAmt;
    // Custom Setting used to store the Field Id to be used for URL construction
    private JVCO_Payment_Agreement_URL_Constants__c urlConstants;

    /* PAYMENT PROCESSING VARIABLES */
    //public static c2g__codaCashEntry__c cashEntry;

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Class constructor for Invoice related functionalities
        Inputs: List<c2g__codaInvoice__c>, PaymentMode
        Returns: N/A
        <Date>      <Authors Name>      <Brief Description of Change> 
        09-Jan-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public JVCO_PaymentSharedLogic(List<c2g__codaInvoice__c> selectedInvoices, PaymentMode mode)
    {
        this.selectedInvoices = selectedInvoices;
        this.mode = mode;
        urlConstants = JVCO_Payment_Agreement_URL_Constants__c.getInstance('JVCO_Agreement_URL');
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Creates an Invoice Group record to relate Sales Invoice record to
        Inputs: String, Boolean
        Returns: JVCO_Invoice_Group__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        28-Dec-2016 ryan.i.r.limlingan  Initial version of function
        03-Feb-2017 ryan.i.r.limlingan  Used Invoice Outstanding Value instead of Total Value
        07-Mar-2017 ryan.i.r.limlingan  Added paymentMethod parameter to update the Payment Method
                                        of selected Sales Invoice records
        20-11-2017 patrick.t.bautista   Added Field CC_SINs__c for populating all selected Sales invoice on it
    ----------------------------------------------------------------------------------------------- */
    /*public JVCO_Invoice_Group__c createInvoiceGroup(String paymentMethod, Boolean isPaperDebit)
    {
        // Construct map of selected invoice records with the needed fields
        Map<Id, c2g__codaInvoice__c> invoiceMap = new Map<Id, c2g__codaInvoice__c>(
                                                            [SELECT JVCO_Invoice_Group__c, 
                                                            JVCO_Payment_Method__c, Name, 
                                                            JVCO_Outstanding_Amount_to_Process__c, 
                                                            c2g__OutstandingValue__c
                                                            FROM c2g__codaInvoice__c 
                                                            WHERE Id IN :selectedInvoices]);
        List<JVCO_CC_Outstanding_Mapping__c> ccOutsToUpdate = new List<JVCO_CC_Outstanding_Mapping__c>();
        Double totalAmount = 0.00;
        String slsInvName = '';
        for (Id invId : invoiceMap.keySet())
        {
            c2g__codaInvoice__c invoice = invoiceMap.get(invId);
            totalAmount += invoice.c2g__OutstandingValue__c;
            slsInvName += invoice.Name + ',';
            //Create a Record to used as mapping for two or more CC/DD payments to get their outstanding values
            JVCO_CC_Outstanding_Mapping__c ccOutsAmt = 
                new JVCO_CC_Outstanding_Mapping__c(JVCO_CC_Outstanding_Value__c = invoice.c2g__OutstandingValue__c,
                                                   JVCO_Sales_Invoice_Name__c = invoice.Name);
            ccOutsToUpdate.add(ccOutsAmt);
            // Direct Debit
            if(paymentMethod.contains('Direct Debit'))
            {
                invoice.JVCO_Outstanding_Amount_to_Process__c = invoice.c2g__OutstandingValue__c;
            }
            invoice.JVCO_Payment_Method__c = paymentMethod;
            invoiceMap.put(invoice.id, invoice);
        }
        
        // Create Invoice Group record using the computed totalAmount
        JVCO_Invoice_Group__c invoiceGroup = new JVCO_Invoice_Group__c(
                                        JVCO_Total_Amount__c = totalAmount,
                                        JVCO_Paper_Debit__c = isPaperDebit,
                                        CC_SINs__c = slsInvName);
        insert invoiceGroup;
        // Insert the list of mapping
        if(!ccOutsToUpdate.isEmpty())
        {
            for(JVCO_CC_Outstanding_Mapping__c ccOuts : ccOutsToUpdate)
            {
                ccOuts.JVCO_CC_Invoice_Group__c = invoiceGroup.id;
            }
            insert ccOutsToUpdate;
        }

        try
        {
            update invoiceMap.values();
        }catch (Exception e){}
        
        return [SELECT Id, Name, JVCO_Total_Amount__c FROM JVCO_Invoice_Group__c WHERE Id = :invoiceGroup.Id];
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Updates Licence Account record based on Invoice Group
        Inputs: JVCO_Invoice_Group__c
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        28-Dec-2016 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public void updateLicenceAccount(JVCO_Invoice_Group__c invoiceGroup, Account licenceAccount)
    {
        if(mode == PaymentMode.Credit)
        {
            licenceAccount.JVCO_CC_Amount__c = invoiceGroup.JVCO_Total_Amount__c;
        }else
        {
            PAYREC2__Payment_Schedule__c pSched = [SELECT Id, PAYREC2__Day__c,
                                                   PAYREC2__Max__c
                                                   FROM PAYREC2__Payment_Schedule__c
                                                   WHERE Name = :paymentSched];
            licenceAccount.JVCO_DD_Amount__c = invoiceGroup.JVCO_Total_Amount__c;
            licenceAccount.JVCO_Payment_Schedule__c = pSched.Id;
            licenceAccount.JVCO_First_Collection_Amount__c = firstColAmt;
            licenceAccount.JVCO_Ongoing_Collection_Amount__c = ongoingColAmt;
            licenceAccount.JVCO_Final_Collection_Amount__c = finalColAmt;
            licenceAccount.JVCO_Direct_Debit_Collection_Day__c = Integer.valueOf(pSched.PAYREC2__Day__c);
            licenceAccount.JVCO_Direct_Debit_Number_of_Instalments__c = pSched.PAYREC2__Max__c;
        }
        licenceAccount.JVCO_Invoice_Group__c = invoiceGroup.Name;
        licenceAccount.JVCO_Invoice_Group_Id__c = invoiceGroup.Id;
        update licenceAccount;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Redirects to the apporpriate Payonomy page, depending on PaymentMode
        Inputs: N/A
        Returns: PageReference
        <Date>      <Authors Name>      <Brief Description of Change> 
        28-Dec-2016 ryan.i.r.limlingan  Initial version of function
        28-Mar-2016 ryan.i.r.limlingan  Added call to constructURL()
    ----------------------------------------------------------------------------------------------- */
    /*public PageReference redirectToPayonomy(Account acct)
    {
        PageReference page = new PageReference(constructURL(acct));
        if(mode == PaymentMode.Debit)
        {
            page.getParameters().put(urlConstants.JVCO_AccountId__c + '_lkid', acct.id);

        }
        page.getParameters().put('retURL', '/' + acct.id);
        page.getParameters().put('scontrolCaching', '1');
        page.getParameters().put('sfdc.override', '1');
        return page;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Dynamically constructs the URL when accessing Payonomy pages
        Inputs: N/A
        Returns: PageReference
        <Date>      <Authors Name>      <Brief Description of Change> 
        28-Mar-2017 ryan.i.r.limlingan  Initial version of function
        9-Apr-2018 eric.van.horssen    Removing line which gets instance id (GREEN-31250)
    ------------------------------------------------------------------------------------------------ */
    /*public String constructURL(Account licenceAccount)
    {        
        List<String> parts = ApexPages.currentPage().getHeaders().get('Host').split('\\.');
        return (mode == PaymentMode.Credit)
            ?'/apex/payspcp1__SagePay_Make_Single_Payment?' + urlConstants.JVCO_AccountId__c + '=' + licenceAccount.Name
            :'/apex/payfish3__CreateDirectDebit?' + urlConstants.JVCO_AccountId__c + '=' + licenceAccount.Name;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Creates corresponding Cash Entry and Cash Entry Line Item records for payments
        Inputs: Id, String, List<PAYBASE2__Payment__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        01-Feb-2017 ryan.i.r.limlingan  Initial version of function
        28-Feb-2017 ryan.i.r.limlingan  Added a String parameter for Payment Method
        07-Mar-2017 ryan.i.r.limlingan  Refactored code to remove String parameter for Payment Method
        20-Nov-2017 patrick.t.bautista  Added Fixed on GREEN-23263 for multiple SagePay payments
        12-Dec-2017 franz.g.a.dimaapi   Refactor and fix GREEN-26812
        04-Apr-2018 mary.ann.a.ruelan   Updated to create cash entries only if CLIs will be created. Fix to GREEN-31487
    ----------------------------------------------------------------------------------------------- */
    /*public static Boolean processPayment(String rType, List<PAYBASE2__Payment__c> payments)
    {
        // Get needed fields from Payment to be used for Cash Entry record creation
        Map<Id,PAYBASE2__Payment__c> paymentMap = new Map<Id,PAYBASE2__Payment__c>(
                                                        [SELECT Id, Name, JVCO_AccountId__c, PAYCP2__Vendor_Transaction_Id__c,
                                                         PAYCP2__Payment_Description__c, PAYBASE2__Amount__c, PAYREC2__Payment_Agreement__c,
                                                         PAYBASE2__Pay_Date__c
                                                         FROM PAYBASE2__Payment__c WHERE Id IN :payments]);
        // Map for CC and DD
        Map<Id, String> paymentIdToPaymentDesMap = new Map<Id, String>();
        // Map of Invoice Group and Payment Agreement
        Map<Id, Id> invGrpToDirectDebitMap = new Map<Id, Id>();
        for(PAYBASE2__Payment__c payment : paymentMap.values())
        {
            // Store SagePay
            if(rType.equals('SagePay') && payment.PAYCP2__Payment_Description__c != null)
            {
                paymentIdToPaymentDesMap.put(payment.Id, payment.PAYCP2__Payment_Description__c);
            //Direct debit
            }else
            {
                invGrpToDirectDebitMap.put(payment.Id, payment.PAYREC2__Payment_Agreement__c);
            }
        }

        // Map of Agreements to get Payment Description
        Map<Id,PAYREC2__Payment_Agreement__c> agreementMap = new Map<Id,PAYREC2__Payment_Agreement__c>(
                                                                        [SELECT JVCO_Invoice_Group__r.Name, 
                                                                        PAYREC2__Description__c, JVCO_Invoice_Group__c
                                                                        FROM PAYREC2__Payment_Agreement__c
                                                                        WHERE Id IN :invGrpToDirectDebitMap.values()]);
        if(!invGrpToDirectDebitMap.isEmpty())
        {
            for(Id pId : invGrpToDirectDebitMap.keyset())
            {
                if(invGrpToDirectDebitMap.containsKey(pId) && agreementMap.containsKey(invGrpToDirectDebitMap.get(pId)))
                {
                    PAYREC2__Payment_Agreement__c paymentAgrmt = agreementMap.get(invGrpToDirectDebitMap.get(pId));
                    if(agreementMap.containsKey(invGrpToDirectDebitMap.get(pId)) && paymentAgrmt.JVCO_Invoice_Group__c != null)
                    {
                        paymentIdToPaymentDesMap.put(pId, paymentAgrmt.JVCO_Invoice_Group__r.Name);
                    }
                }
            }
        }
        // Map of Invoice Group and All related Sales Invoice
        Map<String, List<String>> invGrpToSinvNameMap = new Map<String, List<String>>();
        Set<String> sInvNameSetStr = new Set<String>();
        for(JVCO_Invoice_Group__c invGrp : [SELECT Id, CC_SINs__c, Name
                                             FROM JVCO_Invoice_Group__c 
                                             WHERE Name IN: paymentIdToPaymentDesMap.values()])
        {
            if(!String.isBlank(invGrp.CC_SINs__c))
            {
                for(String ccsins : invGrp.CC_SINs__c.split(','))
                {
                    sInvNameSetStr.add(ccsins);
                    if(!invGrpToSinvNameMap.containsKey(invGrp.Name))
                    {
                        invGrpToSinvNameMap.put(invGrp.Name, new List<String>());
                    }
                    invGrpToSinvNameMap.get(invGrp.Name).add(ccsins);
                }
            }
        }
        // Used to get Total Amount from Invoice Group for computation of Cash Entry line Item value
        Map<String, JVCO_Invoice_Group__c> grpInvMap = getGrpInv(paymentIdToPaymentDesMap.values());

        //Map for CC Outstanding mapping and store Invoice Group, Sales Invoice Name and Outstanding values
        Map<Id, Map<String, Double>> invGrpToSinvToOutsvalMap = new Map<Id, Map<String, Double>>();
        List<String> sInvNameStrList = new List<String>();
        for(JVCO_CC_Outstanding_Mapping__c ccOuts :  [SELECT Id, JVCO_CC_Invoice_Group__c, JVCO_CC_Outstanding_Value__c,
                                                      JVCO_CC_Invoice_Group__r.Name, JVCO_Sales_Invoice_Name__c 
                                                      FROM JVCO_CC_Outstanding_Mapping__c
                                                      WHERE JVCO_CC_Invoice_Group__r.Name IN: invGrpToSinvNameMap.keySet()])
        {
            if(!invGrpToSinvToOutsvalMap.containsKey(ccOuts.JVCO_CC_Invoice_Group__c))
            {
                invGrpToSinvToOutsvalMap.put(ccOuts.JVCO_CC_Invoice_Group__c, new Map<String, double>());
            }
            if(!invGrpToSinvToOutsvalMap.get(ccOuts.JVCO_CC_Invoice_Group__c).containsKey(ccOuts.JVCO_Sales_Invoice_Name__c))
            {
                invGrpToSinvToOutsvalMap.get(ccOuts.JVCO_CC_Invoice_Group__c).put(ccOuts.JVCO_Sales_Invoice_Name__c, ccOuts.JVCO_CC_Outstanding_Value__c);
                sInvNameStrList.add(ccOuts.JVCO_Sales_Invoice_Name__c);
            }
        }
        // Mapping for Getting Sales Invoice
        Map<String, c2g__codaInvoice__c> slsinvsNameMap = new Map<string,c2g__codaInvoice__c>();
        List<c2g__codaCashEntryLineItem__c> itemList = new List<c2g__codaCashEntryLineItem__c>();
        for(c2g__codaInvoice__c invoice: [SELECT Id, Name, JVCO_Invoice_Group__r.Name, c2g__OutstandingValue__c,
                                          JVCO_Outstanding_Amount_to_Process__c, JVCO_Total_SalesInvoice_Installment_Line__c
                                          FROM c2g__codaInvoice__c 
                                          WHERE Name IN : sInvNameSetStr])
        {
            if(rType == 'SagePay' && sInvNameStrList.contains(invoice.Name))
            {
                slsinvsNameMap.put(invoice.Name, invoice);
            }else if(rType == 'Direct Debit Collection' && invoice.JVCO_Outstanding_Amount_to_Process__c != NULL)
            {
                slsinvsNameMap.put(invoice.Name, invoice);
            }
        }

        //Roll Back if Get's an issue
        SavePoint sp = Database.setSavePoint();
        if(!slsinvsNameMap.isEmpty() && createCashEntry(rType))
        {
            // Process Sales Invoice based on Related Payments and Invoice Group
            for(PAYBASE2__Payment__c payment : paymentMap.values())
            {
                //Get Payment Description
                String ingGrpNameStr = paymentIdToPaymentDesMap.get(payment.id);
                if(invGrpToSinvNameMap.containsKey(ingGrpNameStr))
                {
                    for(String sInvStr : invGrpToSinvNameMap.get(ingGrpNameStr))
                    {
                        if(slsinvsNameMap.containsKey(sInvStr) && grpInvMap.containsKey(ingGrpNameStr))
                        {
                            c2g__codaInvoice__c invoice = slsinvsNameMap.get(sInvStr);
                            JVCO_Invoice_Group__c invGrp = grpInvMap.get(ingGrpNameStr);
                            Decimal sInvOutsAmt = rType == 'SagePay' ? invoice.c2g__OutstandingValue__c : 
                            invoice.JVCO_Outstanding_Amount_to_Process__c == null ? invoice.JVCO_Total_SalesInvoice_Installment_Line__c : invoice.JVCO_Outstanding_Amount_to_Process__c;
                            // Calculate value CC or DD
                            Double val = computeValueCCDD(invGrp, payment, invoice.Name, sInvOutsAmt, invGrpToSinvToOutsvalMap);
                            if(val > 0)
                            {
                                c2g__codaCashEntryLineItem__c cashEntryLI = getCashEntryLine(cashEntry, payment, invoice, val);
                                itemList.add(cashEntryLI);
                            }
                        }
                    }
                } 
            }
        }
        if(!itemList.isEmpty())
        {
            try
            {
                insert itemList;
                return true;
            }catch(Exception e)
            {
                Database.rollBack(sp);
                insert getErrorLog(null, 'CG-001', e.getMessage(), 'Cash Entries Generation');
            }
        }else
        {
            if(cashEntry != null)
            {
                Database.rollBack(sp);
                insert getErrorLog(null, 'CG-001', 'No Cash Entry Line will be created', 'Cash Entries Generation');
            }
        }
        return false;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Computes Cash Entry Line Item value for Credit Card and Direct Debit
        Inputs: Map<String,JVCO_Invoice_Group__c>, Map<Id,PAYREC2__Payment_Agreement__c>,
                Decimal, PAYBASE2__Payment__c
        Returns: Double
        <Date>      <Authors Name>      <Brief Description of Change> 
        16-Feb-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public static Double computeValueCCDD(JVCO_Invoice_Group__c invGrp, PAYBASE2__Payment__c payment, String sInvName, Decimal sInvOutsAmt, 
                                        Map<id, Map<String, Double>> invGrpToSinvToOutsvalMap)
    {
        // Get Outstanding Value
        Double outstandingValue = (invGrpToSinvToOutsvalMap.containsKey(invGrp.id) &&
                                  invGrpToSinvToOutsvalMap.get(invGrp.id).containsKey(sInvName))? 
                                  invGrpToSinvToOutsvalMap.get(invGrp.id).get(sInvName):
                                  sInvOutsAmt;
        return ((payment.PAYBASE2__Amount__c / invGrp.JVCO_Total_Amount__c) * outstandingValue).setScale(2);
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Constructs a map of JVCO_Invoice_Group__c
        Inputs: Id
        Returns: Map<String,JVCO_Invoice_Group__c>
        <Date>      <Authors Name>      <Brief Description of Change> 
        02-Feb-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public static Map<String,JVCO_Invoice_Group__c> getGrpInv(List<String> keySet)
    {
        Map<String,JVCO_Invoice_Group__c> invGrpMap = new Map<String,JVCO_Invoice_Group__c>();
        for (JVCO_Invoice_Group__c gin : [SELECT Id, Name, JVCO_Total_Amount__c
                                            FROM JVCO_Invoice_Group__c
                                            WHERE Name IN :keySet])
        {
            invGrpMap.put(gin.Name, gin);
        }
        return invGrpMap;
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Generate Company Vat Registration Number
        Inputs: Set<Id>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        12-Dec-2017 franz.g.a.dimaapi    Initial version of function
        06-Feb-2019 patrick.t.bautista   GREEN-33725 - Stamping of Payment Date on Cash entry line
    ----------------------------------------------------------------------------------------------- */
    /*private static c2g__codaCashEntryLineItem__c getCashEntryLine(c2g__codaCashEntry__c cashEntry, PAYBASE2__Payment__c payment, c2g__codaInvoice__c invoice, Double val)
    {
        c2g__codaCashEntryLineItem__c cashEntryLI = new c2g__codaCashEntryLineItem__c();
        cashEntryLI.c2g__CashEntry__c = cashEntry.Id;
        cashEntryLI.c2g__Account__c = payment.JVCO_AccountId__c;
        cashEntryLI.c2g__LineDescription__c = invoice.c2g__OutstandingValue__c < val ? 'Not for Matching' : payment.PAYCP2__Vendor_Transaction_Id__c;
        cashEntryLI.c2g__CashEntryValue__c = val;
        cashEntryLI.c2g__AccountPaymentMethod__c = cashEntry.c2g__PaymentMethod__c;
        cashEntryLI.c2g__AccountReference__c = invoice.Name;
        cashEntryLI.JVCO_Payonomy_Payment__c = payment.Id;
        cashEntryLI.ffcash__DeriveLineNumber__c = TRUE;
        cashEntryLI.JVCO_Payment_Status__c = 'Completed';
        cashEntryLI.JVCO_Payonomy_Pay_Date__c = payment.PAYBASE2__Pay_Date__c;
        return cashEntryLI;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Generate Company Vat Registration Number
        Inputs: Set<Id>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        20-Apr-2018 mary.ann.a.ruelan    GREEN-31487. Transferred creating of cash entries here from processing batch
    ----------------------------------------------------------------------------------------------- */
    /*private static boolean createCashEntry(String rType)
    {
        Boolean insertSuccess = true;
        cashEntry = new c2g__codaCashEntry__c();
        cashEntry.c2g__Date__c = Date.today();
        cashEntry.c2g__PaymentMethod__c = (rType.equals('SagePay'))?'Debit/Credit Card':'Direct Debit';
        cashEntry.ffcash__DerivePeriod__c = TRUE;
        
        Database.SaveResult bCEList = Database.insert(cashEntry, false);
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        if(!bCEList.isSuccess())
        {
            cashEntry = null; // Reset if insertion has failed
            insertSuccess = false;
            for(Database.Error objErr:bCEList.getErrors())
            {
                errLogList.add(getErrorLog(bCEList.getId(), String.valueof(objErr.getStatusCode()), String.valueof(objErr.getMessage()), 'Cash Entries Generation'));
            }
        }
        
        if(!errLogList.isempty())
        {
            // insert errLogList; Start 22-08-2017 mel.andrei.b.santos  Added try catch method for error handling in inserting error log records
            try
            {
                insert errLogList;
            }catch(Exception ex){}
        }
        return insertSuccess;
    }

    private static ffps_custRem__Custom_Log__c getErrorLog(String failedRecordObjectReference, String errorCode,
                                                    String fullErrorLog, String errorBatchName)
    {
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
        errLog.ffps_custRem__Related_Object_Key__c = failedRecordObjectReference;
        errLog.ffps_custRem__Detail__c = fullErrorLog;
        errLog.ffps_custRem__Message__c = errorBatchName;
        errLog.ffps_custRem__Grouping__c = errorCode;
        
        return errLog;
    }*/
}