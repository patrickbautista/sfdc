@IsTest
public class JVCO_GenerateBlankQuotesBatch_Test {
    
    @testSetup static void setupTestData() 
    {
        JVCO_TestClassObjectBuilder.createBillingConfig();
        
        Product2 prod1 = new Product2();
        prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 83202-C';
        a1.Type = 'Key Account';

        c2g__codaDimension1__c dim = new c2g__codaDimension1__c();
        dim.c2g__ReportingCode__c = 'BC008TEST';
        dim.Name = 'General Purpose';
        insert dim;

        a1.c2g__CODADimension1__c = dim.Id;

        insert a1;
        
        Account a2 = new Account();
        a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.Id);
        insert a2;

        Pricebook2 testPB2 = new Pricebook2();
        testPB2.name = 'Standard Price Book';
        testPB2.IsActive = true;
        insert testPB2;
        
        PriceBookEntry pbe = new PriceBookEntry();
        pbe= JVCO_TestClassObjectBuilder.createPriceBookEntry(prod1.Id);
        insert pbe;

        JVCO_Venue__c nvenue2 = new JVCO_Venue__c();
        nvenue2.Name = 'Sample Venue';
        nvenue2.JVCO_Email__c = 'JVCO_QuoteTriggerHandlerTest@email.com';
        nvenue2.External_Id__c = 'JVCO_QTHTestsampleaccexid';
        nvenue2.JVCO_Field_Visit_Requested__c  = true;
        nvenue2.JVCO_Field_Visit_Requested_Date__c = date.today();
        nvenue2.JVCO_Postcode__c = 'BL2 1AA';
        insert nvenue2;

        JVCO_Affiliation__c  affilRecord2 = new JVCO_Affiliation__c();
        affilRecord2.JVCO_Account__c = a2.Id;
        affilRecord2.JVCO_Venue__c = nvenue2.id;   
        affilRecord2.JVCO_Start_Date__c = system.today().addMonths(3);
        affilRecord2.JVCO_Omit_Quote_Line_Group__c = FALSE;
        insert affilRecord2;
      
    }


    private static testMethod void testMethodGenerateBlankQuotesBatch() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, Licence_Account_Number__c from Account where RecordTypeId =: licenceRT limit 1];
        Date endDate = null;
        Map<Id, Pricebook2> pricebookId = new Map<Id, Pricebook2>([Select Id, Name
                                                                    From Pricebook2
                                                                    Where isStandard = true and isActive = true]);
        Id pbId = pricebookId.values().size() > 0 ? pricebookId.values().get(0).Id:Null;
        Integer totalQuotes = 0;
        Integer totalErrorQuotes = 0;
        String errorMessage = 'Error Message';
        JVCO_Constants__c asCS = JVCO_Constants__c.getOrgDefaults();
        asCS.Key_Account_Blank_Quote_Batch_Size__c = 1;
        upsert asCS JVCO_Constants__c.Id;
        
        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_GenerateBlankQuotesBatch(accountRecord, endDate, pbId, totalQuotes, totalErrorQuotes, errorMessage, false), 1);
        Test.stopTest();
    }
}