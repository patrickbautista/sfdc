@isTest
public class JVCO_QuoteCalculateSchedTest {

    @testSetup
    private static void setupData(){
        Test.startTest();
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        PriceBook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.Type = 'Customer';
        insert acc2;

        //Account acc3 = JVCO_TestClassObjectBuilder.createCustomerAccount();
        //acc3.Name = 'TestCust2';
        //insert acc3;

        Account acc4 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc4.Type = 'Customer';
        insert acc4;

        acc4.JVCO_Customer_Account__c = acc.Id;
        update acc4;

      
        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_Closure_Reason__c = '';
        aff.JVCO_End_Date__c = null;
        insert aff;

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        insert opp;
        Test.stopTest();
    }

    private static testMethod void testAutomaticAdditionOfMinfees2(){

        SBQQ__QuoteProcess__c qProcess = [SELECT ID FROM SBQQ__QuoteProcess__c limit 1];
        Opportunity opp = [SELECT Id, SBQQ__RenewedContract__c FROM Opportunity Limit 1];
        String LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        SBQQ.TriggerControl.disable(); 

        Account customerAccount = new Account();
        customerAccount.Name = 'testminfee';
        customerAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        customerAccount.Type = 'Customer'; 
        insert customerAccount;


        Contact c = new Contact();
        String uniq = String.valueOf(Math.random());
        c.LastName = 'Doeicetest' + uniq;
        c.FirstName = 'Johnicetest0' + uniq;
        c.Email = 'surveyAppUser@hotmailicetest.com';
        c.AccountId = customerAccount.Id;
        insert c;

        c2g__CODADimension3__c d3 = new c2g__CODADimension3__c();
        d3.Name = 'Non Key';
        d3.c2g__ReportingCode__c = 'Non Key';
        insert d3;


        Account acc = [select Id from Account where RecordTypeId = :LicenceRT limit 1];
        acc.c2g__CODADimension3__c = d3.id;
        update acc;

        
        JVCO_Venue__c testVenue2 = new JVCO_Venue__c();
        testVenue2.name = 'Test 232';
        testVenue2.JVCO_Email__c = 'JVCO_QuoteLineTriggerHaicendlerTest2@email.com';
        testVenue2.External_Id__c = 'JVCO_QLTHTest2samplevenexiceidsdsd';
        testVenue2.JVCO_Street__c = 'Test2 Streettysd';
        testVenue2.JVCO_City__c = 'Test2 City';
        testVenue2.JVCO_Country__c = 'Test2 Country';
        testVenue2.JVCO_Postcode__c = 'TN32 5SL';
        insert testVenue2;

        JVCO_Affiliation__c testAffil = JVCO_TestClassObjectBuilder.createAffiliation(acc.id, testVenue2.id);
        testAffil.JVCO_Status__c = 'Trading On Hold';
        testAffil.JVCO_Omit_Quote_Line_Group__c = false;
        testAffil.JVCO_Revenue_Type__c = 'New Business';
        testAffil.JVCO_Venue__c = testVenue2.id;
        insert testAffil;


        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Account__c = acc.Id;
        q.SBQQ__Primary__c = true;
        q.Start_Date__c = date.today();
        q.End_Date__c = date.today().addDays(30);
        q.SBQQ__Type__c = 'Quote';
        q.SBQQ__PriceBook__c = test.getStandardPriceBookId();
        q.SBQQ__QuoteProcessid__c = qProcess.id;
        q.SBQQ__Opportunity2__c = opp.Id;
        q.JVCO_PPL_Gap__c = false;
        q.JVCO_PPLMinFeeAdded__c = false;
        q.JVCO_VPLMinFeeAdded__c = false;
        q.JVCO_PPL_Gap__c = false;
        q.PPL_Minimum_Annual_Royalty__c = 53;
        q.VPL_Minimum_Annual_Royalty__c = 53;
        q.TotalValuePPLandMinFee__c = 1;
        q.TotalValueVPLandMinFee__c = 1;
        q.NoOfTerminatedLines__c = 0;
        q.JVCO_Number_Quote_Lines__c = 2;
        q.SBQQ__Type__c = 'Renewal';
        insert q;       

        SBQQ__QuoteLineGroup__c qlg = new SBQQ__QuoteLineGroup__c();
        qlg.SBQQ__Account__c = acc.id;
        qlg.JVCO_Venue__c = testVenue2.id;
        qlg.JVCO_Affiliated_Venue__c = testAffil.id;
        qlg.SBQQ__Quote__c = q.id;
        insert qlg;


        Product2 p = new Product2();
        p.Name = 'PPLPP024: Dance Supplement - Holiday Centres Tariff';
        p.ProductCode = 'PPLPP024';
        p.IsActive = true;
        p.SBQQ__HidePriceInSearchResults__c = true;
        p.SBQQ__AssetConversion__c = 'One quote per line';
        p.SBQQ__DefaultQuantity__c = 1.0;
        p.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        p.SBQQ__SubscriptionTerm__c = 12;
        p.SBQQ__ChargeType__c = 'Recurring';
        p.SBQQ__BillingFrequency__c = 'Annual';
        p.Family = 'PPL';
        //  p.blng__BillingRule__c = br.id;
        p.SBQQ__BillingType__c = 'Advance';
        insert p;

        Product2 p2 = new Product2();
        p2.Name = 'PPL Minimum';
        p2.ProductCode = 'MRPPPL';
        p2.IsActive = true;
        p2.SBQQ__HidePriceInSearchResults__c = true;
        p2.SBQQ__AssetConversion__c = 'One quote per line';
        p2.SBQQ__DefaultQuantity__c = 1.0;
        p2.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        p2.SBQQ__SubscriptionTerm__c = 12;
        p2.SBQQ__ChargeType__c = 'Recurring';
        p2.SBQQ__BillingFrequency__c = 'Annual';
        p2.Family = 'PPL';
        //  p.blng__BillingRule__c = br.id;
        p2.SBQQ__BillingType__c = 'Advance';
        insert p2;

        Product2 p3 = new Product2();
        p3.Name = 'VPL Minimum';
        p3.ProductCode = 'MRPVPL';
        p3.IsActive = true;
        p3.SBQQ__HidePriceInSearchResults__c = true;
        p3.SBQQ__AssetConversion__c = 'One quote per line';
        p3.SBQQ__DefaultQuantity__c = 1.0;
        p3.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        p3.SBQQ__SubscriptionTerm__c = 12;
        p3.SBQQ__ChargeType__c = 'Recurring';
        p3.SBQQ__BillingFrequency__c = 'Annual';
        p3.Family = 'VPL';
        //  p.blng__BillingRule__c = br.id;
        p3.SBQQ__BillingType__c = 'Advance';
        insert p3;

        Product2 p4 = new Product2();
        p4.Name = 'VPLPP004 Tariff';
        p4.ProductCode = 'VPLPP004';
        p4.IsActive = true;
        p4.SBQQ__HidePriceInSearchResults__c = true;
        p4.SBQQ__AssetConversion__c = 'One quote per line';
        p4.SBQQ__DefaultQuantity__c = 1.0;
        p4.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        p4.SBQQ__SubscriptionTerm__c = 12;
        p4.SBQQ__ChargeType__c = 'Recurring';
        p4.SBQQ__BillingFrequency__c = 'Annual';
        p4.Family = 'VPL';
        //  p.blng__BillingRule__c = br.id;
        p4.SBQQ__BillingType__c = 'Advance';
        insert p4;

        PriceBookEntry pbe = new PriceBookEntry();
        pbe.Pricebook2Id = test.getStandardPriceBookId();
        pbe.product2id = p.id;
        pbe.IsActive = true;
        pbe.UnitPrice = 1.0;
        insert pbe;

         PriceBookEntry pbe4 = new PriceBookEntry();
        pbe4.Pricebook2Id = test.getStandardPriceBookId();
        pbe4.product2id = p4.id;
        pbe4.IsActive = true;
        pbe4.UnitPrice = 1.0;
        insert pbe4;

        MinfeeIds__c settings = new MinfeeIds__c(Name = 'MinFeeId', MRPPPL__c = p2.id, MRPVPL__c = p3.id);
        insert settings;
        system.debug('QUOTE value: '+ q);
        
        Test.startTest();
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c( SBQQ__Product__c = p.id, SBQQ__Quote__c = q.id, SBQQ__Group__c = qlg.id, Start_Date__c = date.today(), End_Date__c = date.today().addDays(30), SBQQ__ListPrice__c = 1,SBQQ__Quantity__c = 1);
        SBQQ__QuoteLine__c ql4 = new SBQQ__QuoteLine__c( SBQQ__Product__c = p4.id, SBQQ__Quote__c = q.id, SBQQ__Group__c = qlg.id, Start_Date__c = date.today(), End_Date__c = date.today().addDays(30), SBQQ__ListPrice__c = 1,SBQQ__Quantity__c = 1);
        insert ql;
        insert ql4;        
        
        
        List<SBQQ__QuoteLine__c> qList2 = [SELECT Id, SBQQ__ProductCode__c from SBQQ__QuoteLine__c where SBQQ__Quote__c =: q.id];
        SBQQ__Quote__c checkQuote = [Select Recalculate__c, JVCO_PPLMinFeeAdded__c, JVCO_PPL_Gap__c, JVCO_Account_Dimension_3_Code__c, PPL_Minimum_Annual_Royalty__c, TotalValuePPLandMinFee__c, NoOfTerminatedLines__c, JVCO_Number_Quote_Lines__c from SBQQ__Quote__c where id =:q.id];

        List<SBQQ__Quote__c> qList = new List<SBQQ__Quote__c>();
        qList.add(checkQuote);
        JVCO_QuoteCalculateSched quoteCalcSched = new JVCO_QuoteCalculateSched(qList);
		String sch = '0 0 23 * * ?';
        system.schedule('Test JVCO_QuoteCalculateSched', sch, quoteCalcSched);
        
        Test.stopTest();
        System.assertEquals(qList2.size(), 4);
    }
}