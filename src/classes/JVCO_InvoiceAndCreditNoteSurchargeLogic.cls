public class JVCO_InvoiceAndCreditNoteSurchargeLogic {
    
    public Map<String, Id> dim2Map;
    public Map<String, Id> prodMap;
    public User surUser;
    public JVCO_Order_Group__c orderGroup;
    public Id originalInvoice;
    public Map<Id, List<OrderItem>> orderToOrderItemMap;
    Map<String, c2g__codaInvoiceLineItem__c> dim2ToInvoiceLineMap = new Map<String, c2g__codaInvoiceLineItem__c>();
    
    public void setupInvoiceAndCreditNoteHeader()
    {
        Map<Id, AggregateResult> ogIdToAggregateResultMap = getOgIdToAggregateResultMap();
        Map<Id, c2g__codaInvoice__c> ogIdToSInvMap = new Map<Id, c2g__codaInvoice__c>();
        c2g__codaInvoice__c invoice = new c2g__codaInvoice__c();
        
        if(ogIdToAggregateResultMap.containsKey(orderGroup.Id))
        {
            AggregateResult ar = ogIdToAggregateResultMap.get(orderGroup.Id);
            //Set Total Amount
            Double totalAmt = (Double)ar.get('totalAmt');
            Date minStartDate = (Date)ar.get('minStartDate');
            Date maxEndDate = (Date)ar.get('maxEndDate');
            Date minDueDate = System.today();
            //Invoice   
            if(totalAmt >= 0)
            {
                Boolean ifReissuedInvoice = false;
                invoice = createSalesInvoice(minStartDate, maxEndDate, minDueDate, ifReissuedInvoice);
            }
        }
        setupInvoiceLineItems(ogIdToAggregateResultMap, invoice);
        linkOrderToInvoice(invoice);
        bulkPostInvoice(invoice);
    }
    
    private void linkOrderToInvoice(c2g__codaInvoice__c invoice){
        Set<Order> surchargeOrderSet = new Set<Order>();
        Set<OrderItem> surchargeOrderItemSet = new Set<OrderItem>();
        system.debug('orderToOrderItemMap '+orderToOrderItemMap);
        for(Id surOrderId : orderToOrderItemMap.keySet()){
                        
            for(OrderItem surOrderItem : orderToOrderItemMap.get(surOrderId)){
                if(dim2ToInvoiceLineMap.containsKey(surOrderItem.JVCO_Dimension_2__c)){
                    surOrderItem.JVCO_Sales_Invoice_Line_Item__c = dim2ToInvoiceLineMap.get(surOrderItem.JVCO_Dimension_2__c).Id;
                    system.debug('surOrderItem.SBQQ__Activated__c '+surOrderItem.SBQQ__Activated__c);
                    surOrderItem.SBQQ__QuoteLine__c = null;
                    surchargeOrderItemSet.add(surOrderItem);
                }
            }
            
            Order surOrder = new Order();
            surOrder.Id = surOrderId;
            surOrder.Status = 'Activated';
            surOrder.JVCO_Sales_Invoice__c = invoice.Id;
            surchargeOrderSet.add(surOrder);
        }
        update new List<OrderItem>(surchargeOrderItemSet);
        update new List<Order>(surchargeOrderSet);
    }
    
    private c2g__codaInvoice__c createSalesInvoice(Date minStartDate, Date maxEndDate, Date minDueDate, Boolean ifReissuedInvoice)
    {
        c2g__codaInvoice__c sInv = new c2g__codaInvoice__c();
        sInv.c2g__Account__c = orderGroup.JVCO_Group_Order_Account__c;
        sInv.c2g__DueDate__c = minDueDate;
        sInv.JVCO_Invoice_Type__c = 'Surcharge';
        sInv.c2g__InvoiceDate__c = date.today();
        sInv.c2g__DeriveCurrency__c = true;
        sInv.c2g__DerivePeriod__c = true;
        sInv.c2g__DeriveDueDate__c = false;
        sInv.JVCO_Licence_Start_Date__c = minStartDate;
        sInv.JVCO_Licence_End_Date__c = maxEndDate;
        String dunningLetterLanguage = orderGroup.JVCO_Group_Order_Account__r.JVCO_Dunning_Letter_Language__c;
        sInv.JVCO_Customer_Type__c = dunningLetterLanguage;
        sInv.JVCO_Original_Invoice__c = originalInvoice;
        sInv.JVCO_Sales_Rep__c = surUser != null ? surUser.Id: null;
        return sInv;
    }
    
    private c2g__codaInvoiceLineItem__c createSalesInvLineItem(Id sInvId, Decimal amt, Decimal taxAmt, String type)
    {
        c2g__codaInvoiceLineItem__c sInvLine = new c2g__codaInvoiceLineItem__c();
        sInvLine.c2g__Invoice__c = sInvId;
        sInvLine.c2g__Dimension1__c = orderGroup.JVCO_Group_Order_Account__r.c2g__CODADimension1__c;
        sInvLine.c2g__Dimension3__c = orderGroup.JVCO_Group_Order_Account__r.c2g__CODADimension3__c;
        sInvLine.c2g__TaxCode1__c = orderGroup.JVCO_Group_Order_Account__r.c2g__CODAOutputVATCode__c;
        sInvLine.c2g__DeriveLineNumber__c = true;
        sInvLine.c2g__DeriveUnitPriceFromProduct__c = false;
        sInvLine.c2g__CalculateTaxValue1FromRate__c = false;
        sInvLine.c2g__TaxValue1__c = taxAmt.setScale(2);
        sInvLine.c2g__Quantity__c = 1;
        sInvLine.c2g__UnitPrice__c = amt.setScale(2);
        sInvLine.c2g__Dimension2__c = dim2Map.get(type);
        sInvLine.c2g__Product__c = prodMap.get(type);
        return sInvLine;
    }
    
    private void setupInvoiceLineItems(Map<Id, AggregateResult> ogIdToAggregateResultMap, c2g__codaInvoice__c invoice)
    {
        //Insert Sales Invoice
        insert invoice;
        
        //Set Sales Invoice Line
        Set<c2g__codaInvoiceLineItem__c> sInvLineItemSet = new Set<c2g__codaInvoiceLineItem__c>();
        
        AggregateResult ar = ogIdToAggregateResultMap.get(orderGroup.Id);
        //PRS
        if((Decimal)ar.get('prsTotalLines') > 0)
        {
            c2g__codaInvoiceLineItem__c installment = createSalesInvLineItem(invoice.Id, (Decimal)ar.get('prsTotalAmt'), (Decimal)ar.get('prsTotalTaxAmt'), 'PRS');            
            dim2ToInvoiceLineMap.put('PRS', installment);
            sInvLineItemSet.add(installment);
        }
        //PPL
        if((Decimal)ar.get('pplTotalLines') > 0)
        {
            c2g__codaInvoiceLineItem__c installment = createSalesInvLineItem(invoice.Id, (Decimal)ar.get('pplTotalAmt'), (Decimal)ar.get('pplTotalTaxAmt'), 'PPL');
            dim2ToInvoiceLineMap.put('PPL', installment);
            sInvLineItemSet.add(installment);
        }
        //VPL
        if((Decimal)ar.get('vplTotalLines') > 0)
        {
            c2g__codaInvoiceLineItem__c installment = createSalesInvLineItem(invoice.Id, (Decimal)ar.get('vplTotalAmt'), (Decimal)ar.get('vplTotalTaxAmt'), 'VPL');
            dim2ToInvoiceLineMap.put('VPL', installment);
            sInvLineItemSet.add(installment);
        }
        //Insert Sales Invoice Line
        insert new List<c2g__codaInvoiceLineItem__c>(sInvLineItemSet);
    }
    
    private Map<Id, AggregateResult> getOgIdToAggregateResultMap()
    {
        Map<Id, AggregateResult> ogIdToAggregateResultMap = new Map<Id, AggregateResult>();
        for(AggregateResult ar : [SELECT JVCO_Order_Group__c Id,
                                  SUM(JVCO_PRS_Total_Lines__c)prsTotalLines,
                                  SUM(JVCO_PPL_Total_Lines__c)pplTotalLines,
                                  SUM(JVCO_VPL_Total_Lines__c)vplTotalLines,
                                  SUM(JVCO_PRS_TotalAmount__c)prsTotalAmt,
                                  SUM(JVCO_PPL_TotalAmount__c)pplTotalAmt,
                                  SUM(JVCO_VPL_TotalAmount__c)vplTotalAmt,
                                  SUM(JVCO_PRS_Tax_Total_Amount__c)prsTotalTaxAmt,
                                  SUM(JVCO_PPL_Tax_Total_Amount__c)pplTotalTaxAmt,
                                  SUM(JVCO_VPL_Tax_Total_Amount__c)vplTotalTaxAmt,
                                  SUM(TotalAmount) totalAmt,
                                  MIN(JVCO_Licence_Start_Date__c)minStartDate,
                                  MAX(JVCO_Licence_End_Date__c)maxEndDate,
                                  COUNT(SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c)migratedContractCounter
                                  FROM Order
                                  WHERE JVCO_Order_Group__c = : orderGroup.Id
                                  AND JVCO_Order_Group__c != null
                                  AND JVCO_Cancelled__c = false
                                  AND JVCO_Number_of_Order_Products__c != 0
                                  GROUP BY JVCO_Order_Group__c])
        {
            ogIdToAggregateResultMap.put((Id)ar.get('Id'), ar); 
        }
        return ogIdToAggregateResultMap;
    }
    
    private void bulkPostInvoice(c2g__codaInvoice__c surchargeInvoice)
    {
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        List<c2g.CODAAPICommon.Reference> refList = new List<c2g.CODAAPICommon.Reference>();
        c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
        reference.Id = surchargeInvoice.Id;
        refList.add(reference);        
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        c2g.CODAAPISalesInvoice_10_0.BulkPostInvoice(context, refList);
    }
    
    public void setDim2AndProdMapping()
    {
        if(dim2Map == null)
        {
            dim2Map = new Map<String, Id>();
            for(c2g__codaDimension2__c dim2 : [SELECT Name, Id FROM c2g__codaDimension2__c
                                               WHERE Name IN ('PRS', 'PPL', 'VPL')])
            {
                dim2Map.put(dim2.Name, dim2.Id);
            }
        }
        
        if(prodMap == null)
        {
            prodMap = new Map<String, Id>();
            for(Product2 prod : [SELECT Id, Name FROM Product2 
                                 WHERE Name IN ('PPL Product', 'PRS Product', 'VPL Product')])
            {
                prodMap.put(prod.Name.substring(0,3), prod.Id);
            }
        }   
    }
}