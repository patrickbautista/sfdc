/* ----------------------------------------------------------------------------------------------
   Name: JVCO_AssignFieldAgentsPostCodeUtil.cls 
   Description: Util class for updating Ownership of Lead or Venue after insert or after update

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  07-Sep-2016   0.1         mark.glenn.b.ayson  Intial creation
  ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_AssignFieldAgentsPostCodeUtil {
  public static boolean firstRun = true;
  public static Id queueID;

  /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: Function that creates a new Task after updating the owner ID of a lead or venue record
    Inputs: List(String), Boolean 
    Returns: Id
    <Date>      <Authors Name>      <Brief Description of Change> 
    07-Sep-2016 mark.glenn.b.ayson  Initial version of function
    09-Mar-2017 ma.carla.r.haldos   Changed the taskRecord.Type to taskRecord.JVCO_Type__c equal to the values of 'Field Visit'
    25-Jan-2018	apple.j.a.manayaga	Added firstrun to make sure 1 task is created

  ----------------------------------------------------------------------------------------------- */
  public static void createNewTask(List<JVCO_AssignFieldAgentWrapper> taskValues, Boolean isLead) 
  {
   if(firstrun){

    List<Task> taskRecordList = new List<Task>();
    Task taskRecord = new Task();

    for(JVCO_AssignFieldAgentWrapper wrapper : taskValues)
    {
        taskRecord = new Task();

        // Lead or Venue Name
        taskRecord.Subject = 'Field Visit - ' + wrapper.venueName; 
        // Owner ID
        taskRecord.OwnerId = wrapper.ownerID;

        // Check if the record is a lead
        if(isLead)
        {
          // Lead ID
          taskRecord.WhoId = wrapper.recordID;
        }
        else
        {
          // Venue ID
          taskRecord.WhatId = wrapper.recordID;
        }

        // Task Priority
        taskRecord.Priority = 'Normal';
        // Task - Custom Type fields
        taskRecord.JVCO_Type__c = 'Field';
        // Task Reminder
        taskRecord.IsReminderSet = false;
        // Task Status
        taskRecord.Status = 'Open';
        // Task Public
        taskRecord.IsVisibleInSelfService = true;

        taskRecordList.add(taskRecord);
    }

    // Insert the list of task
    insert taskRecordList;
    System.debug(taskRecordList);
    firstrun = false;
    }
  }

/* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: Function that gets the ID of the Queue
    Inputs: String(userName)
    Returns: Id
    <Date>      <Authors Name>      <Brief Description of Change> 
    08-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
  public static Id getQueueID()
  {
      if(queueID == null)
      {
          queueID =  [SELECT Id 
                     FROM Group 
                     WHERE DeveloperName = 'JVCO_Unresolved_Addresses'].Id;
      }

    return queueID;
  }

  /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: Get the postcode from the record
    Inputs: String
    Returns: String
    <Date>      <Authors Name>      <Brief Description of Change> 
    29-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
  public static String getPostCode(String recordPostCode)
  {
      String[] postCodeArray = recordPostCode.split(' ');

      return postCodeArray[0].trim();
  }

  /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: Remove the numbers from the postcode
    Inputs: String
    Returns: String
    <Date>      <Authors Name>      <Brief Description of Change> 
    29-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
  public static String removeNumeric(String recordPostCode)
  {
      return recordPostCode.replaceAll('[0-9]', '');
  }

  /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: Create a map that contains the postcode and the Region from the Lead or Venue
    Inputs: Map<Id, String>
    Returns: Map<String, JVCO_AssignFieldAgentWrapper>
    <Date>      <Authors Name>      <Brief Description of Change> 
    22-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
  public static Map<String, JVCO_AssignFieldAgentWrapper> getFieldSalesDetails(Map<Id, String> objectIDAndPostCodeMap)
  {
      Map<String, JVCO_AssignFieldAgentWrapper> postCodeAndRegionMap = new Map<String, JVCO_AssignFieldAgentWrapper>();
      Map<String, Id> postCodeAndFieldSalesPostCodeMap = new Map<String, Id>();
      Map<String, Id> userNameAndUserIDMap = new Map<String, Id>();
      Map<Id, JVCO_Field_Sales_Assignment__mdt> fieldSalesIDAndRecordMap = new Map<Id, JVCO_Field_Sales_Assignment__mdt>();
      Set<String> postCodeSet = new Set<String>();
      Set<String> userNameSet = new Set<String>();
      JVCO_AssignFieldAgentWrapper agentWrapper = new JVCO_AssignFieldAgentWrapper();

      // Get the postcode from the lead/venue records
      for(Id id : objectIDAndPostCodeMap.keySet())
      {
          postCodeSet.add(objectIDAndPostCodeMap.get(id).substring(0, 2).trim());
          postCodeSet.add(objectIDAndPostCodeMap.get(id).substring(0, 1).trim());
      }

      // Get the field sales assignment postcode record and the region
      // Save the region and the field sales assignment postcode record to the map
      for(JVCO_Field_Sales_Assignment_Postcode__mdt fsapmRecord : [SELECT JVCO_Field_Sales_Assignment__c, Label FROM JVCO_Field_Sales_Assignment_Postcode__mdt])
      {
        postCodeAndFieldSalesPostCodeMap.put(fsapmRecord.Label.trim(), fsapmRecord.JVCO_Field_Sales_Assignment__c);
      }

      // Save the field sales record id and the field sales record to a map 
      for(JVCO_Field_Sales_Assignment__mdt fsaRecord : [SELECT Id, Label, JVCO_Username__c FROM JVCO_Field_Sales_Assignment__mdt WHERE Id IN :postCodeAndFieldSalesPostCodeMap.values()])
      {
        fieldSalesIDAndRecordMap.put(fsaRecord.Id, fsaRecord);
      }

      // Get the postcode and the respective username from the map
      for(String postCode : postCodeAndFieldSalesPostCodeMap.keySet())
      {
        // Save the username to the set
        userNameSet.add(fieldSalesIDAndRecordMap.get(postCodeAndFieldSalesPostCodeMap.get(postCode)).JVCO_Username__c);
      }

      // Get the details of the username to the User table
      // Save the username and User ID to a map
      for(User userRecord : [SELECT Id, username FROM User WHERE Username IN :userNameSet])
      {
        userNameAndUserIDMap.put(userRecord.username, userRecord.Id);
      }

      // Get region from the map
      for(String postCode : postCodeAndFieldSalesPostCodeMap.keySet())
      {
        agentWrapper = new JVCO_AssignFieldAgentWrapper();
        agentWrapper.postCodeLabel = fieldSalesIDAndRecordMap.get(postCodeAndFieldSalesPostCodeMap.get(postCode)).Label;
        agentWrapper.userName = userNameAndUserIDMap.get(fieldSalesIDAndRecordMap.get(postCodeAndFieldSalesPostCodeMap.get(postCode)).JVCO_Username__c);

        // Save the postcode and region and user ID
        postCodeAndRegionMap.put(postCode, agentWrapper);
      }

      // Return the map containing the postcode, region and user ID
      return postCodeAndRegionMap;
  }
}