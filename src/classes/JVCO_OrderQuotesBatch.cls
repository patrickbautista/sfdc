/* ----------------------------------------------------------------------------------------------
    Name: JVCO_OrderQuotesBatch
    Description: Batch Class for updating Opportunities and Quotes related to an Account

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    03-Mar-2017     0.1         Marlon Ocillos      Intial creation
    28-Mar-2017     0.2         Jules Pablo         Added code segment that unchecks the JVCO_IsComplete__c
    03-Nov-2017     0.3         mariel.m.buena      Updated query to filter opportunity amount not blank and amount not zero and contracted is true
    11-May-2018     0.4         rhys.j.c.dela.cruz  Added stopper method to prevent Account Trigger Handler from running
    10-Aug-2018     0.5         jules.osberg.a.pablo Placed Logic in JVCO_OrderQuotesHelper
----------------------------------------------------------------------------------------------- */
global class JVCO_OrderQuotesBatch implements Database.Batchable<sObject>, Database.Stateful 
{
    global final Account accountRecord;
    global Boolean isReview;
    global Set<Id> opportunityIds;
    Map<String, String> quoteStringMap = new Map<String, String>();
    Integer orderSize;

    global JVCO_OrderQuotesBatch (Account a, Set<Id> oIds, Boolean flagPassed) 
    {
        accountRecord = a;
        opportunityIds = oIds;
        isReview = flagPassed;
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC) 
    {
        String query = JVCO_OrderQuotesHelper.getQueryString(accountRecord, opportunityIds);
        return Database.getQueryLocator(query);
    }
    
    global void execute (Database.BatchableContext BC, List<sObject> scope) 
    {
        stopperMethod();

        if (scope.size() > 0) {
            quoteStringMap.putAll(JVCO_OrderQuotesHelper.executeOrderQuotes((List<Opportunity>) scope, quoteStringMap));
        }
    }
    
    global void finish (Database.BatchableContext BC) 
    {
        If(isReview){
            JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_OrderReviewQuotesApexJob__c', '','JVCO_OrderReviewQuotesLastSubmitted__c');
        }Else{
            JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_OrderRenewQuotesApexJob__c', '','JVCO_OrderRenewQuotesLastSubmitted__c');
        }
        //orderSize = opportunityIds.size() - quoteStringMap.size();
        orderSize = opportunityIds.size();
        JVCO_OrderQuotesHelper.sendOrderingEmail(accountRecord, orderSize, false, false, quoteStringMap, isReview);
    }

    global static void stopperMethod()
    {
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
    }
}