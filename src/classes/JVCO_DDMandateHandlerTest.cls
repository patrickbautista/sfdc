@isTest
private class JVCO_DDMandateHandlerTest{
    /*@testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();        
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        insert dt;       
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim2('PRS');
        insert JVCO_TestClassHelper.setDim2('VPL');
        Test.startTest();
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        p.Name = 'VPL Product';
        p.JVCO_Society__c = 'VPL';
        p.ProductCode = 'VPL Product';
        insert p;
        
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        //sq.JVCO_CreditReason__c = 'Credit to refund against PRS contract';
        q.JVCO_Quote_Id_to_Scheduler__c = q.id;
        q.NoOfLInesInCurrentYear__c = 10;
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;

        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        List<Order> orderList = new List<Order>();   
        orderList.add(o);
        JVCO_General_Settings__c gs = new JVCO_General_Settings__c();
        gs.JVCO_Invoice_Schedule_Scope__c = 10;
        insert gs;
        
        Test.stopTest();

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        insert bInv;
        Id paybankId = JVCO_TestClassHelper.getPayBank();
        Id ddMandateId = JVCO_TestClassHelper.getDDMandate(bInv.blng__Account__c);
        Id paySchedId = JVCO_TestClassHelper.getPaySched(payBankId);
        
        blng__InvoiceLine__c bInvLine = JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 50, false);
        bInvLine.JVCO_Surcharge_Applicable__c = true;
        bInvLine.Dimension_2s__c = 'VPL';
        JVCO_FFutil.stopBlngInvoiceLineHandlerBeforeInsert = true;
        insert bInvLine;
        Database.executeBatch(new JVCO_OrderMultipleBillNowBatch(orderList));
        
    }
    
    @isTest
    static void testDDMandateUpdate()
    {
        Test.startTest();
        List<JVCO_DD_Mandate__c> mandateList = [SELECT Id FROM JVCO_DD_Mandate__c];
        System.assertEquals(true, mandateList.size() > 0);
        mandateList[0].JVCO_AUDIS_Ref__c = 'test';
        mandateList[0].JVCO_Sort_Code__c = 'test12';
        Database.update(mandateList);
        Test.stopTest();
    }
    
    @isTest
    static void testDDMandateDelete()
    {
        Test.startTest();
        List<JVCO_DD_Mandate__c> mandateList = [SELECT Id FROM JVCO_DD_Mandate__c];
        System.assertEquals(true, mandateList.size() > 0);
        delete mandateList;
        Test.stopTest();
    }
    
    @isTest
    static void testDeactivateDDMandate()
    {
        Test.startTest();
        List<JVCO_DD_Mandate__c> mandateList = [SELECT Id, JVCO_Deactivated__c FROM JVCO_DD_Mandate__c];
        mandateList[0].JVCO_Deactivated__c = TRUE;
        System.assertEquals(true, mandateList.size() > 0);
        Database.update(mandateList);
        Test.stopTest();
    }    */
}