/* ----------------------------------------------------------------------------------------------
   Name: JVCO_SalesInvoiceHandler.cls 
   Description: Handler class for c2g__codaInvoice__c object

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  26-Jan-2017   0.1         ryan.i.r.limlingan               Intial creation
  21-Dec-2017   0.2         filip.bezak@accenture.com        GREEN-27053 - Added methods: afterInsert, beforeInsert, beforeUpdate and updateSurchargeValueOfRelatedInvoice
  01-Oct-2019   0.3         patrick.t.bautista               GREEN-34945 - Throw error if parents are not equal when SIN to SCR only
  02-Dec-2019   0.4         patrick.t.bautista               GREEN-35106 - redesign validation for mismatch parent matching
  15-Jul-2020   0.6         patrick.t.bautista               GREEN-35718 - prevent deletion of instalment lines with pending iddh to collect
  ----------------------------------------------------------------------------------------------- */

public class JVCO_SalesInvoiceHandler
{

    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_SalesInvoiceTrigger__c : false;
    public static Boolean DisableMismatchParent = JVCO_General_Settings__c.getInstance().JVCO_Disable_mismatch_parent__c;
    public static void beforeInsert(List<c2g__codaInvoice__c> sInvoices)
    {
        if(!skipTrigger)
        {
            populateCinemaPPD(sInvoices);
        }
    }
    
    public static void beforeUpdate(List<c2g__codaInvoice__c> sInvoices, Map<Id, c2g__codaInvoice__c> sInvoicesOld)
    {
        if(!skipTrigger)
        {
            if(DisableMismatchParent){
                restrictDifferentTransLineParent(sInvoices,sInvoicesOld);
            }
        }
    }
    
    /* ------------------------------------------------------------------------------------------
        Author: kristoffer.d.martin
        Company: Accenture
        Description: This method handles afterUpdate trigger events for c2g__codaInvoice__c
        Input: List<c2g__codaInvoice__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change>
        26-Dec-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static void afterUpdate(List<c2g__codaInvoice__c> sInvList, Map<Id, c2g__codaInvoice__c> oldSInvMap)
    {
        if(!skipTrigger) 
        {
            //Added Handler Stopper - 15/08/2017 - franz.g.a.dimaapi            
            if(!JVCO_FFUtil.stopCodaInvoiceHandlerAfterUpdate)
            {
                JVCO_SalesInvoiceGeneratePlanLogic.processInvoice(sInvList, oldSInvMap);
                //updateSurchargeValueOfRelatedInvoice(invoices); - Not Needed Anymore
                updateUnlinkedInvoiceGroup(sInvList, oldSInvMap);
            }
        }    
    }
    /* ------------------------------------------------------------------------------------------
        Author: filip.bezak@accenture.com
        Description: This method updates Total_Surchargeable_Amount__c of sales invoices that has the surcharge invoices updated or created
        Input: List<c2g__codaInvoice__c>
        Returns: void
        <Date>          <Authors Name>                  <Brief Description of Change> 
        21-Dec-2017     filip.bezak@accenture.com       Initial version of function for GREEN-27053
    --------------------------------------------------------------------------------------------- */
    /*private static void updateSurchargeValueOfRelatedInvoice(List<c2g__codaInvoice__c> sInvList)
    {
        Set<Id> sInvIds = new Set<Id>();
        for(c2g__codaInvoice__c sInvRec : sInvList)
        {
            if(sInvRec.JVCO_Invoice_Type__c == 'Surcharge' && 
                !String.isBlank(sInvRec.JVCO_Original_Invoice__c) && 
                !String.isBlank(sInvRec.JVCO_Invoice_Legacy_Number__c))
            {
                sInvIds.add(sInvRec.JVCO_Original_Invoice__c);
            }
        }
        
        if(!sInvIds.isEmpty())
        {
            Map<Id, c2g__codaInvoice__c> sInvMap = new Map<Id, c2g__codaInvoice__c>(
                                                    [SELECT 
                                                        (SELECT c2g__InvoiceTotal__c
                                                        FROM Surcharge_Invoices__r)
                                                    FROM c2g__codaInvoice__c
                                                    WHERE Id IN :sInvIds]);
            for(c2g__codaInvoice__c sInvRec : sInvMap.values())
            {
                sInvRec.Total_Surchargeable_Amount__c = 0;
                for(c2g__codaInvoice__c surInvRec : sInvRec.Surcharge_Invoices__r)
                {
                    sInvRec.Total_Surchargeable_Amount__c += surInvRec.c2g__InvoiceTotal__c;
                }
            }
            update sInvMap.values();
        }   
    }*/

    /* ------------------------------------------------------------------------------------------
        Author: jasper.j.figueroa@accenture.com
        Description: This method populates JVCO_CinemaPromptPaymentDiscount__c of sales invoices from the Cinema PPD of the Licence Account
        Input: List<c2g__codaInvoice__c>
        Returns: void
        <Date>          <Authors Name>                  <Brief Description of Change> 
        25-Jun-2018     Jasper Figueroa                 Initial version of function for GREEN-22001
    --------------------------------------------------------------------------------------------- */
    private static void populateCinemaPPD(List<c2g__codaInvoice__c> sInvList)
    {
        Set<Id> accIdSet = new Set<Id>();
        for(c2g__codaInvoice__c sInv : sInvList)
        {
            accIdSet.add(sInv.c2g__Account__c);
        }
        Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, JVCO_LA_PPD_Gross__c FROM Account WHERE Id IN :accIdSet]);
        for(c2g__codaInvoice__c sInv : sInvList)
        {
            if(accountMap.containsKey(sInv.c2g__Account__c))
            {
                sInv.JVCO_CinemaPromptPaymentDiscount__c = accountMap.get(sInv.c2g__Account__c).JVCO_LA_PPD_Gross__c;
            }
            
        }
    }

    /* ------------------------------------------------------------------------------------------
        Author: patrick.t.bautista@accenture.com
        Description: This method update JVCO_Status to Retired when Invoice Group / Payment plan on sales invoice is change
        Input: List<c2g__codaInvoice__c>
        Returns: void
        <Date>          <Authors Name>                  <Brief Description of Change> 
        01-Oct-2019     patrick.t.bautista              Initial version of function for GREEN-33163
    --------------------------------------------------------------------------------------------- */
    private static void updateUnlinkedInvoiceGroup(List<c2g__codaInvoice__c> sInvList, Map<id, c2g__codaInvoice__c> oldsInvMap)
    {
        Map<Id, Set<Id>> invGroupToSINMap = new Map<Id, Set<Id>>();
        Set<JVCO_Invoice_Group__c> invGroupSet = new Set<JVCO_Invoice_Group__c>();
        Set<Id> sInvIdSet = new Set<Id>();
        for(c2g__codaInvoice__c sInv : sInvList)
        {
            c2g__codaInvoice__c oldSInv = oldsInvMap.get(sInv.Id);
            if(sInv.JVCO_Invoice_Group__c != oldSInv.JVCO_Invoice_Group__c 
               && oldSInv.JVCO_Invoice_Group__c != null
               && sInv.JVCO_Invoice_Group__c == null)
            {
                JVCO_Invoice_Group__c invGrp = new JVCO_Invoice_Group__c();
                invGrp.Id = oldSInv.JVCO_Invoice_Group__c;
                invGrp.JVCO_Status__c = 'Retired';
                invGroupSet.add(invGrp);
                sInvIdSet.add(sInv.Id);
                if(!invGroupToSINMap.containsKey(invGrp.Id)){
                    invGroupToSINMap.put(invGrp.Id, new Set<Id>());
                }
                invGroupToSINMap.get(invGrp.Id).add(sInv.Id);
            }
        }

        //Map SmarterPay invoice group to get idd and invoice group
        Map<Id, Id> IDDToInvGrpMap = new Map<Id, Id>();
        for(SMP_DirectDebit_GroupInvoice__c ddInv : [SELECT Id,
                                                     Invoice_Group__c,
                                                     Income_Direct_Debit__c
                                                     FROM SMP_DirectDebit_GroupInvoice__c
                                                     WHERE Invoice_Group__c IN: invGroupToSINMap.keySet()])
        {
            IDDToInvGrpMap.put(ddInv.Income_Direct_Debit__c, ddInv.Invoice_Group__c);
        }
        
        Map<Id, Income_Debit_History__c> invoiceToIDDHDateMap = new Map<Id, Income_Debit_History__c>();
        for(Income_Debit_History__c iddh: [SELECT Id,
                                           Income_Direct_Debit__c,
                                           DD_Collection_Date__c
                                           FROM Income_Debit_History__c
                                           WHERE Income_Direct_Debit__c IN :IDDToInvGrpMap.keySet()
                                           AND Amount__c > 0])
        {
            if(invGroupToSINMap.containsKey(IDDToInvGrpMap.get(iddh.Income_Direct_Debit__c))){
                for(Id sInvId : invGroupToSINMap.get(IDDToInvGrpMap.get(iddh.Income_Direct_Debit__c))){
                    if(iddh.DD_Collection_Date__c != null){
                        invoiceToIDDHDateMap.put(sInvId, iddh);
                    }
                }
            }
        }
        List<Id> incomeDDHistoryWithCLIList = new List<Id>();
        for(c2g__codaCashEntryLineItem__c cli : [SELECT JVCO_Income_Debit_History__c
                                                 FROM c2g__codaCashEntryLineItem__c
                                                 WHERE JVCO_Income_Debit_History__c IN : invoiceToIDDHDateMap.values()])
        {
            for(Id sInvId : invoiceToIDDHDateMap.keySet()){
                if(invoiceToIDDHDateMap.get(sInvId).Id == cli.JVCO_Income_Debit_History__c){
                    invoiceToIDDHDateMap.remove(sInvId);
                }
            }
        }
        
        Set<c2g__codaInvoiceInstallmentLineItem__c> instLineSet = new Set<c2g__codaInvoiceInstallmentLineItem__c>();
        for(c2g__codaInvoiceInstallmentLineItem__c instLine : [SELECT Id,
                                                               c2g__Invoice__c,
                                                               c2g__DueDate__c
                                                               FROM c2g__codaInvoiceInstallmentLineItem__c 
                                                               WHERE JVCO_Payment_Status__c = 'Unpaid' 
                                                               AND c2g__Invoice__c IN : sInvIdSet
                                                               AND JVCO_DD_Failed__c = false])
        {
            if((invoiceToIDDHDateMap.containsKey(instLine.c2g__Invoice__c)
               && invoiceToIDDHDateMap.get(instLine.c2g__Invoice__c).DD_Collection_Date__c != instLine.c2g__DueDate__c)
               || !invoiceToIDDHDateMap.containsKey(instLine.c2g__Invoice__c))
            {
                instLineSet.add(instLine);
            }
        }
        
        for(c2g__codaInvoice__c sInv : [SELECT Id,Name,
                                        JVCO_Invoice_Group__c
                                        FROM c2g__codaInvoice__c 
                                        WHERE JVCO_Invoice_Group__c IN: invGroupToSINMap.keySet()])
        {
            invGroupSet.remove(new JVCO_Invoice_Group__c(Id = sInv.JVCO_Invoice_Group__c,
                                                         JVCO_Status__c = 'Retired'));
        }
        
        update new List<JVCO_Invoice_Group__c>(invGroupSet);
        delete new List<c2g__codaInvoiceInstallmentLineItem__c>(instLineSet);
    }
    
    /* ------------------------------------------------------------------------------------------
        Author: patrick.t.bautista@accenture.com
        Description: This method throws an error when Reference Doc parents is not equal to SIN
        Input: List<c2g__codaInvoice__c>
        Returns: void
        <Date>          <Authors Name>                  <Brief Description of Change> 
        23-Aug-2018     patrick.t.bautista              Initial version of function for GREEN-34945
    --------------------------------------------------------------------------------------------- */
    public static void restrictDifferentTransLineParent(List<c2g__codaInvoice__c> sInvList, Map<Id, c2g__codaInvoice__c> sInvoicesOld)
    {        
        List<String> invoiceOrCredStrList = new List<String>();
        for(c2g__codaInvoice__c invoice: sInvList){
            if(invoice.JVCO_Reference_Document__c != null
               && invoice.JVCO_Reference_Document__c != sInvoicesOld.get(invoice.id).JVCO_Reference_Document__c
               && invoice.JVCO_Reference_Document__c.contains('SCR'))
            {
                invoiceOrCredStrList.add(invoice.JVCO_Reference_Document__c);
                invoiceOrCredStrList.add(invoice.Name);
            }
        }

        if(!invoiceOrCredStrList.isEmpty())
        {
            //Get Parents Mapping
            Map<String,Map<String,Double>> invOrCredToListOfParentMap = getInvoiceOrCredParentMap(invoiceOrCredStrList);
            //Throws error if parents are not equal
            if(!invOrCredToListOfParentMap.isEmpty()){
                for(c2g__codaInvoice__c invoice: sInvList){
                    if(invOrCredToListOfParentMap.containsKey(invoice.JVCO_Reference_Document__c) 
                       && invOrCredToListOfParentMap.containsKey(invoice.Name))
                    {
                        for(String credParentstr : invOrCredToListOfParentMap.get(invoice.JVCO_Reference_Document__c).keySet())
                        {
                            if(!invOrCredToListOfParentMap.get(invoice.Name).containsKey(credParentstr)
                               || invOrCredToListOfParentMap.get(invoice.JVCO_Reference_Document__c).get(credParentstr)
                               > invOrCredToListOfParentMap.get(invoice.Name).get(credParentstr))
                            {
                                invoice.addError(Label.JVCO_RestrictParentNotEqual);
                            }
                        }
                    }
                }
            }
        }
    }
    
    /* ------------------------------------------------------------------------------------------
        Author: patrick.t.bautista@accenture.com
        Description: This method use to store parents of SIN and SCR
        Input: List<c2g__codaInvoice__c>
        Returns: void
        <Date>          <Authors Name>                  <Brief Description of Change> 
        23-Aug-2018     patrick.t.bautista              Initial version of function for GREEN-34945
    --------------------------------------------------------------------------------------------- */
    public static Map<String,Map<String, Double>> getInvoiceOrCredParentMap(List<String> invoiceOrCredStrList)
    {
       Map<String,Map<String, double>> invOrCredToListOfParentMap = new Map<String,Map<String, double>>();
        for(c2g__codaTransactionLineItem__c transLine : [SELECT id, c2g__Dimension2__r.Name,
                                                         c2g__Transaction__r.c2g__SalesCreditNote__c,
                                                         c2g__Transaction__r.c2g__SalesInvoice__c,
                                                         c2g__Transaction__r.c2g__SalesCreditNote__r.Name,
                                                         c2g__Transaction__r.c2g__SalesInvoice__r.Name,
                                                         c2g__DocumentValue__c
                                                         FROM c2g__codaTransactionLineItem__c
                                                         WHERE (c2g__Transaction__r.c2g__SalesCreditNote__r.Name IN : invoiceOrCredStrList 
                                                                OR c2g__Transaction__r.c2g__SalesInvoice__r.Name IN : invoiceOrCredStrList)
                                                         AND (c2g__LineType__c = 'Analysis'
                                                              AND c2g__Dimension2__c != null
                                                              AND c2g__Transaction__r.c2g__TransactionType__c IN('Invoice','Credit Note'))
                                                         AND c2g__DocumentValue__c != 0
                                                         ORDER BY c2g__Dimension2__r.Name])
        {
            if(!invOrCredToListOfParentMap.containsKey(transLine.c2g__Transaction__r.c2g__SalesInvoice__r.Name) 
               && transLine.c2g__Transaction__r.c2g__SalesInvoice__c != null)
            {
                invOrCredToListOfParentMap.put(transLine.c2g__Transaction__r.c2g__SalesInvoice__r.Name,new Map<String, Double>());
            }
            if(!invOrCredToListOfParentMap.containsKey(transLine.c2g__Transaction__r.c2g__SalesCreditNote__r.Name)
               && transLine.c2g__Transaction__r.c2g__SalesCreditNote__c != null)
            {
                invOrCredToListOfParentMap.put(transLine.c2g__Transaction__r.c2g__SalesCreditNote__r.Name,new Map<String, Double>());
            }
            if(invOrCredToListOfParentMap.containsKey(transLine.c2g__Transaction__r.c2g__SalesInvoice__r.Name)
               && !invOrCredToListOfParentMap.get(transLine.c2g__Transaction__r.c2g__SalesInvoice__r.Name).containsKey(transLine.c2g__Dimension2__r.Name)){
                   invOrCredToListOfParentMap.get(transLine.c2g__Transaction__r.c2g__SalesInvoice__r.Name).put(transLine.c2g__Dimension2__r.Name, Math.abs(transline.c2g__DocumentValue__c));
               }
            if(invOrCredToListOfParentMap.containsKey(transLine.c2g__Transaction__r.c2g__SalesCreditNote__r.Name)
               && !invOrCredToListOfParentMap.get(transLine.c2g__Transaction__r.c2g__SalesCreditNote__r.Name).containsKey(transLine.c2g__Dimension2__r.Name)){
                   invOrCredToListOfParentMap.get(transLine.c2g__Transaction__r.c2g__SalesCreditNote__r.Name).put(transLine.c2g__Dimension2__r.Name, Math.abs(transline.c2g__DocumentValue__c));
               }
        }
        return invOrCredToListOfParentMap;
    }
}