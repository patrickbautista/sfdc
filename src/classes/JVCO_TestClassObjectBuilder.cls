@isTest
public with sharing class JVCO_TestClassObjectBuilder {
    
    public static Account createCustomerAccount()
    {
        Account a = new Account();
        a.Name = 'Account 1';
        a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        a.Type = 'Customer'; 
        return a;
    }

    public static Account createLicenceAccount(Id CustomerAccountId)
    {
        list<c2g__codaGeneralLedgerAccount__c> testGALedger = new list<c2g__codaGeneralLedgerAccount__c>();
        testGALedger =  [select id from c2g__codaGeneralLedgerAccount__c limit 1];

        list<c2g__codaTaxCode__c> testTXCode = new list<c2g__codaTaxCode__c> ();
        testTXCode =  [select id from c2g__codaTaxCode__c limit 1];

        //system.assertEquals(null, testGALedger.id);

        c2g__codaGeneralLedgerAccount__c testGeneralLedger = new c2g__codaGeneralLedgerAccount__c(); 
        c2g__codaTaxCode__c taxCode = new c2g__codaTaxCode__c();

        if(testGALedger.size() == 0){
            testGeneralLedger = createGeneralLedger();
            insert testGeneralLedger;
        }else{
            testGeneralLedger = [select id from c2g__codaGeneralLedgerAccount__c limit 1];
        }

        if(testTXCode.size() == 0){
            taxCode = createTaxCode(testGeneralLedger.Id);
            insert taxCode;
        }else{
            taxCode = [select id from c2g__codaTaxCode__c limit 1];
        }
        

        Contact c = JVCO_TestClassObjectBuilder.createContact(CustomerAccountId);
        insert c;

        Account a = new Account();
        a.Name = 'Account 2'; 
        a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
        a.JVCO_Customer_Account__c = CustomerAccountId;
        a.JVCO_Review_Type__c = 'Automatic';
        a.Type = 'Customer';
        a.JVCO_Review_Frequency__c  = 'Annually';
        a.c2g__CODAOutputVATCode__c = taxCode.id;
        a.JVCO_Preferred_Contact_Method__c = 'Email';
        a.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        a.JVCO_Billing_Contact__c = c.id;
        a.JVCO_Licence_Holder__c = c.id;
        a.JVCO_Review_Contact__c = c.id;
        a.JVCO_DD_Payee_Contact__c = c.Id;
        return a;
    }
    
    public static c2g__codaGeneralLedgerAccount__c createGeneralLedger(){
        c2g__codaGeneralLedgerAccount__c testGeneralLedger = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedger.JVCO_Active__c = true;
        testGeneralLedger.name = 'Test Geenral Ledger';
        testGeneralLedger.c2g__Type__c = 'Balance Sheet';
        testGeneralLedger.c2g__ReportingCode__c = '10390';
        testGeneralLedger.c2g__GLAGroup__c = 'Cash';
        testGeneralLedger.c2g__TrialBalance1__c= 'Assets';
        testGeneralLedger.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedger.c2g__TrialBalance3__c = 'Cash and Cash Equivalents';
        testGeneralLedger.c2g__UnitOfWork__c = 182.0;
        testGeneralLedger.c2g__CashFlowCategory__c = 'Operating';
        
        return testGeneralLedger;
    }
    
    public static c2g__codaTaxCode__c createTaxCode(Id gLedger){
        c2g__codaTaxCode__c taxCode = new c2g__codaTaxCode__c();
        taxCode.name = 'GB-O-STD-LIC';
        taxCode.c2g__GeneralLedgerAccount__c = gLedger;
        taxCode.ffvat__NetBox__c = 'Box 6';
        taxCode.ffvat__TaxBox__c = 'Box 1';
        
        return taxCode;
    }

    public static SBQQ__QuoteProcess__c createQuoteProcess() 
    {
        SBQQ__QuoteProcess__c qProcess = new SBQQ__QuoteProcess__c();
        qProcess.SBQQ__Default__c = true;
        return qProcess;
    }

    public static Contact createContact(Id LicenceAccountId)
    {
        Contact c = new Contact();
        String uniq = String.valueOf(Math.random());
        c.LastName = 'Doe' + uniq;
        c.FirstName = 'John' + uniq;
        c.Email = 'surveyAppUser@hotmail.com';
        c.AccountId = LicenceAccountId;
        c.MailingStreet = 'Test Street ' + uniq;
        c.MailingCity = 'Test City ' + uniq;
        c.MailingPostalCode = 'BB10 4RG';
        c.MailingState = 'Lacashire';
        return c;
    }

    public static Contact createContact()
    {
        Contact c = new Contact();
        c.LastName = 'Doe';
        c.FirstName = 'John';
        c.Email = 'surveyAppUser@hotmail.com';
        return c;
    }

    public static Opportunity createOpportunity(Account LicenseAccountObject )
    {
        Opportunity o = new Opportunity();
        o.Name = LicenseAccountObject.Name;
        o.AccountId = LicenseAccountObject.Id;
        o.StageName = 'Quoting';
        o.CloseDate = Date.today();
        return o;
    }
    
    public static Product2 createProduct()
    {
        Product2 p = new Product2();
        p.Name = 'PPLPP003: Pop/Music Quizzes Tariff';
        p.ProductCode = 'PPLPP003';
        p.IsActive = true;
        p.SBQQ__HidePriceInSearchResults__c = true;
        p.SBQQ__AssetConversion__c = 'One quote per line';
        p.SBQQ__DefaultQuantity__c = 1.0;
        p.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        p.SBQQ__SubscriptionTerm__c = 12;
        p.SBQQ__ChargeType__c = 'Recurring';
        p.SBQQ__BillingFrequency__c = 'Annual';
      //  p.blng__BillingRule__c = br.id;
        p.SBQQ__BillingType__c = 'Advance';
        return p;
    }

    public static Product2 createPrsProduct(String Name, String ProductCode, String CustomMinMaxCalcType, String ParentMinCalcProductCode, String ParentMaxCalcProductCode, String CustomCountParentProductCode, String Product_Type, String CustomValueSumParentProductCode, Boolean JVCO_Primary_Tariff_Min_Fee, Boolean Proratable, String JVCO_Society, String SubscriptionPricing, Integer SubscriptionTerm, Boolean JVCO_Auto_Renewable, String ChargeType, String BillingFrequency, Id BillingRule, String BillingType, Boolean HidePriceInSearchResults, String JVCO_Parent_Bundle, String JVCO_Customer_Facing_Name, String ConfigurationType, String ConfigurationEvent)
    {
        Product2 p = new Product2();
        p.Name = Name;
        p.ProductCode = ProductCode;
        p.IsActive = true;
        p.CustomMinMaxCalcType__c = CustomMinMaxCalcType;
        p.ParentMinCalcProductCode__c = ParentMinCalcProductCode;
        p.ParentMaxCalcProductCode__c = ParentMaxCalcProductCode;
        p.CustomCountParentProductCode__c = CustomCountParentProductCode;
        p.Product_Type__c = Product_Type;
        p.CustomValueSumParentProductCode__c = CustomValueSumParentProductCode;
        p.JVCO_Primary_Tariff_Min_Fee__c = JVCO_Primary_Tariff_Min_Fee;
        p.Proratable__c = Proratable;
        p.JVCO_Society__c = JVCO_Society;
        p.SBQQ__SubscriptionPricing__c = SubscriptionPricing;
        p.SBQQ__SubscriptionTerm__c = SubscriptionTerm;
        p.JVCO_Auto_Renewable__c = JVCO_Auto_Renewable;
        p.SBQQ__ChargeType__c = ChargeType;
        p.SBQQ__BillingFrequency__c = BillingFrequency;
        p.blng__BillingRule__c = BillingRule; 
        p.SBQQ__BillingType__c = BillingType;
        p.SBQQ__HidePriceInSearchResults__c = HidePriceInSearchResults;
        p.JVCO_Parent_Bundle__c = JVCO_Parent_Bundle;
        p.JVCO_Customer_Facing_Name__c = JVCO_Customer_Facing_Name;
        p.SBQQ__ConfigurationType__c = ConfigurationType;
        p.SBQQ__ConfigurationEvent__c = ConfigurationEvent;
        return p;
    }
    
    public static Product2 createPplProduct(String Name, String ProductCode, Integer SortOrder, String Family, String CustomMinMaxCalcType, String ParentMinCalcProductCode, String ParentMaxCalcProductCode, String CustomCountParentProductCode, String Product_Type, String CustomValueSumParentProductCode, Boolean JVCO_Primary_Tariff_Min_Fee, Boolean Proratable, String JVCO_Society, String SubscriptionPricing, Integer SubscriptionTerm, Boolean JVCO_Auto_Renewable, String ChargeType, String BillingFrequency, Id BillingRule, String BillingType, Boolean HidePriceInSearchResults, String JVCO_Parent_Bundle, String JVCO_Customer_Facing_Name, String ConfigurationType, String ConfigurationEvent)
    {
        Product2 p = new Product2();
        p.Name = Name;
        p.ProductCode = ProductCode;
        p.IsActive = true;
        p.SBQQ__SortOrder__c = SortOrder;
        p.Family = Family;
        p.CustomMinMaxCalcType__c = CustomMinMaxCalcType;
        p.ParentMinCalcProductCode__c = ParentMinCalcProductCode;
        p.ParentMaxCalcProductCode__c = ParentMaxCalcProductCode;
        p.CustomCountParentProductCode__c = CustomCountParentProductCode;
        p.Product_Type__c = Product_Type;
        p.CustomValueSumParentProductCode__c = CustomValueSumParentProductCode;
        p.JVCO_Primary_Tariff_Min_Fee__c = JVCO_Primary_Tariff_Min_Fee;
        p.Proratable__c = Proratable;
        p.JVCO_Society__c = JVCO_Society;
        p.SBQQ__SubscriptionPricing__c = SubscriptionPricing;
        p.SBQQ__SubscriptionTerm__c = SubscriptionTerm;
        p.JVCO_Auto_Renewable__c = JVCO_Auto_Renewable;
        p.SBQQ__ChargeType__c = ChargeType;
        p.SBQQ__BillingFrequency__c = BillingFrequency;
        p.blng__BillingRule__c = BillingRule; 
        p.SBQQ__BillingType__c = BillingType;
        p.SBQQ__HidePriceInSearchResults__c = HidePriceInSearchResults;
        p.JVCO_Parent_Bundle__c = JVCO_Parent_Bundle;
        p.JVCO_Customer_Facing_Name__c = JVCO_Customer_Facing_Name;
        p.SBQQ__ConfigurationType__c = ConfigurationType;
        p.SBQQ__ConfigurationEvent__c = ConfigurationEvent;
        return p;
    }

    public static JVCO_Venue__c createVenue()
    {
        JVCO_Venue__c v = new JVCO_Venue__c();
        v.Name = 'Sample Venue';
        v.JVCO_Email__c = 'JVCO_QuoteLineTriggerHandlerTest@email.com';
        v.External_Id__c = 'JVCO_QLTHTestsamplevenexid';

        //added by edmundCua
        v.JVCO_Street__c = 'Test Street';
        v.JVCO_City__c = 'Test City';
        v.JVCO_Country__c = 'Test Country';
        v.JVCO_Postcode__c = 'W2 6LG';
       
        return v;
    }

    public static JVCO_Affiliation__c createAffiliation(Id LicenceAccountId, Id VenueId)
    {
        JVCO_Affiliation__c a = new JVCO_Affiliation__c();
        a.JVCO_End_Date__c = date.today().addDays(20);
        a.JVCO_Account__c = LicenceAccountId;
        a.JVCO_Venue__c = VenueId;
        a.JVCO_Closure_Reason__c = 'Dissolved';
        a.JVCO_Start_Date__c = date.today();
        return a;
    }

    public static SBQQ__Quote__c createQuote(Id LicenceAccountId, Id QuoteProcessId, Id OpportunityId, Id BillingContact, String ReviewFrequency)
    {
        Integer endDateModifier = 0;
        if(ReviewFrequency == 'Monthly'){
            endDateModifier = 1;
        }
        else if(ReviewFrequency == 'Quarterly'){
            endDateModifier = 3;
        }
        else if(ReviewFrequency == 'Annually'){
            endDateModifier = 12;
        }
        else{
            endDateModifier = 0;
        }

        SBQQ__Quote__c q = new SBQQ__Quote__c();

        q.SBQQ__Account__c = LicenceAccountId;
        q.SBQQ__Opportunity2__c = OpportunityId;
  
        if (BillingContact != null) 
        {
            q.SBQQ__PrimaryContact__c = BillingContact;
        }
        q.SBQQ__QuoteProcessId__c = '';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Type__c = 'Quote';
        q.SBQQ__Primary__c = true;        
        q.SBQQ__SubscriptionTerm__c = 12;
        q.Accept_TCs__c = 'yes';
        q.SBQQ__PriceBook__c = Test.getStandardPricebookId();
        q.SBQQ__StartDate__c = Date.today();
        q.Start_Date__c = Date.today();
        q.JVCO_CurrentStartDate__c = Date.today();
        if(endDateModifier > 0){
            q.SBQQ__EndDate__c = Date.today().addMonths(endDateModifier).addDays(-1);
            q.End_Date__c = Date.today().addMonths(endDateModifier).addDays(-1);
        }
        q.SBQQ__QuoteProcessId__c = QuoteProcessId;
        q.JVCO_Contact_Type__c = 'Face to Face';
        return q;
    }

    public static SBQQ__QuoteLine__c createQuoteLine(Id QlgAffiliatedVenue, Id QlgQouteId, Id QlgId, Id ProductId)
    {
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.Affiliation__c = QlgAffiliatedVenue;
        ql.SBQQ__Quote__c = QlgQouteId;
        ql.SBQQ__Group__c = QlgId;
        ql.SBQQ__Product__c = ProductId;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 100;
        ql.SBQQ__CustomerPrice__c = 100;
        ql.SBQQ__NetPrice__c = 100;
        ql.SBQQ__SpecialPrice__c = 100;
        ql.SBQQ__RegularPrice__c = 100;
        ql.SBQQ__UnitCost__c = 100;
        ql.SBQQ__ProratedListPrice__c = 100;
        ql.SBQQ__SpecialPriceType__c = 'Custom'; 
        ql.NoOfQuizPerAnnum__c = 100;
        return ql;
    }

    public static blng__BillingRule__c createBillingRule()
    {
        blng__BillingRule__c br = new blng__BillingRule__c();
        br.Name = 'Invoice';
        br.blng__Active__c = true;
        br.blng__GenerateInvoices__c = 'yes';
        br.blng__InitialBillingTrigger__c = 'Order Product Activation Date';
        br.blng__PartialPeriodTreatment__c = 'Combine';
        return br;
    }

    public static SBQQ__PriceRule__c createPriceRule()
    {
        SBQQ__PriceRule__c p = new SBQQ__PriceRule__c();
        p.Name = 'Test Price Rule';
        p.SBQQ__TargetObject__c = 'Calculator';
        p.SBQQ__ConditionsMet__c = 'All';
        p.SBQQ__EvaluationEvent__c = 'On Calculate;After Calculate';
        p.SBQQ__EvaluationOrder__c = 1;
        return p;
    }

    public static SBQQ__PriceCondition__c createPriceCondition(Id PriceRuleId)
    {
        SBQQ__PriceCondition__c pc = new SBQQ__PriceCondition__c();
        pc.SBQQ__Rule__c = PriceRuleId;
        pc.SBQQ__Object__c = 'Quote Line';
        pc.SBQQ__Field__c = 'Product Code';
        pc.SBQQ__Operator__c = 'equals';
        pc.SBQQ__FilterType__c = 'Value';
        pc.SBQQ__Value__c = 'PPLPP003';
        return pc;

    }

    public static List<SBQQ__PriceAction__c> createPriceAction(Id PriceRuleId) 
    {
        List<SBQQ__PriceAction__c> l_pa = new List<SBQQ__PriceAction__c>();

        SBQQ__PriceAction__c pa = new SBQQ__PriceAction__c();
        pa.SBQQ__Rule__c = PriceRuleId;
        pa.SBQQ__TargetObject__c = 'Quote Line';
        pa.SBQQ__Field__c = 'SBQQ__Quantity__c';
        pa.SBQQ__Formula__c = '1';
        l_pa.add(pa);

        pa = new SBQQ__PriceAction__c();
        pa.SBQQ__Rule__c = PriceRuleId;
        pa.SBQQ__TargetObject__c = 'Quote Line';
        pa.SBQQ__Field__c = 'SBQQ__ListPrice__c';
        pa.SBQQ__Formula__c = '1';
        l_pa.add(pa);

        return l_pa;
    }

    public static SBQQ__ConfigurationAttribute__c createConfigurationAttribute(Id ProductId)
    {
        SBQQ__ConfigurationAttribute__c c = new SBQQ__ConfigurationAttribute__c();
        c.Name = 'Number of quizzes per annum';
        c.SBQQ__TargetField__c = 'NoOfQuizPerAnnum__c';
        c.SBQQ__Product__c = ProductId;
        c.SBQQ__Required__c = true;
        c.SBQQ__DisplayOrder__c = 1;
        return c;

    }

     public static Pricebook2 createPriceBook()
    {
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Standard Price Book';
        pb.isActive = true;
        return pb;
    }   

    public static Entitlement createEntitlement(Id LicenceAccountId)
    {
        Entitlement e = new Entitlement();
        e.Name = 'Customer Service Support';
        e.AccountID = LicenceAccountId;
        return e;
    }

    public static Case createCase(Id EntitlementId)
    {
        Case c = new Case();
        c.Subject = 'Test Subject';
        c.Status = 'New';
        c.EntitlementId = EntitlementId;
        c.Origin = 'Email';  
        return c;
    }

    public static Contract createContract(Id LicenceAccountId, Id QuoteId)
    {
        Contract contr = new Contract();
        contr.AccountId = LicenceAccountId;
        contr.Status = 'Draft';
        contr.StartDate = Date.today();
        contr.JVCO_CurrentStartDate__c = Date.today();
        contr.ContractTerm = 12;
        contr.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
        contr.SBQQ__RenewalQuoted__c = FALSE;
        contr.SBQQ__Quote__c = QuoteId;
        return contr;
    }
    
    public static Lead createLead()
    {
        Lead l = new Lead();
        l.LastName = 'Lead Test';
        l.JVCO_Venue_Postcode__c = 'BL2 1AA';
        l.JVCO_Field_Visit_Requested__c = true;
        l.JVCO_Field_Visit_Requested_Date__c = date.today();
        l.JVCO_Venue_Street__c = 'Test Street';
        l.Company = 'Test Company';
        return l;
    }

    public static SBQQ__Subscription__c createSubscription(Id LicenceAccountId, Id ProductId)
    {
        SBQQ__Subscription__c sub = new SBQQ__Subscription__c();
        sub.SBQQ__Quantity__c = 5;
        sub.SBQQ__Account__c = LicenceAccountId;
        sub.SBQQ__Product__c = ProductId;
        return sub;     
    }

    public static JVCO_SLA_Constants__c createSLAConstants()
    {
        JVCO_SLA_Constants__c cs = new JVCO_SLA_Constants__c();
        cs.Name = 'JVCO_SLA_Custom_Settings';
        cs.JVCO_Entitlement_Name__c = 'Customer Service Support';
        cs.JVCO_Case_Origins_with_Entitlement__c = 'Email Webform Live_Chat Request_Callback'
                                                    + ' Inbound_Call White_Mail';
        cs.JVCO_First_Milestone__c = 'First Response';
        cs.JVCO_First_Milestone_Origin__c = 'Email Webform Request_Callback';
        cs.JVCO_First_Milestone_Criteria__c = 'Responded';
        cs.JVCO_Second_Milestone__c = 'Resolution Time';
        cs.JVCO_Second_Milestone_Origin__c = cs.JVCO_Case_Origins_with_Entitlement__c;
        cs.JVCO_Second_Milestone_Criteria__c = 'Closed';
        return cs;
    }

    public static Entitlement createEntitlement(Id LicenceAccountId, Id SLAProcessId)
    {
        //To get SLA Process ID - SlaProcess sl = [SELECT Id FROM SlaProcess WHERE Name='Standard Support' LIMIT 1];
        Entitlement e = new Entitlement();
        e.Name='Customer Service Support';
        e.AccountId=LicenceAccountId;
        e.SlaProcessId=SLAProcessId;
        return e;
    }

    public static void createCustomSettings(){
        List<sObject> budCatList = Test.loadData(JVCO_Budget_Category_Dimension_Mapping__c.sObjectType, 'JVCO_Test_Data_JVCO_Budget_Category_Dimension_Mapping');
    }
    
    public static void createPrimaryTariffMapping(){
        List<sObject> budCatList = Test.loadData(JVCO_Primary_Tariff_Mapping__c.sObjectType, 'JVCO_TestData_PrimaryTariffMapping');
    }
        
    public static void createBillingConfig() {
        List<sObject> bilConList = Test.loadData(blng__BillingConfig__c.sObjectType, 'JVCO_TestData_BillingConfig');
    }

    public static c2g__codaGeneralLedgerAccount__c createGeneralLedger(String Name, String ReportCode, Id TestGroupId)
    {
        c2g__codaGeneralLedgerAccount__c GL = new c2g__codaGeneralLedgerAccount__c();
        GL.c2g__AdjustOperatingActivities__c = false;
        GL.c2g__AllowRevaluation__c = false;
        GL.c2g__CashFlowCategory__c = 'Operating Activities';
        GL.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        GL.c2g__GLAGroup__c = 'Accounts Receivable';
        GL.c2g__ReportingCode__c = '10020';
        GL.c2g__TrialBalance1__c = 'Assets';
        GL.c2g__TrialBalance2__c = 'Current Assets';
        GL.c2g__TrialBalance3__c = 'Accounts Receivables';
        GL.c2g__Type__c = 'Balance Sheet';
        GL.c2g__UnitOfWork__c = 1.0;
        GL.Dimension_1_Required__c = false;
        GL.Dimension_2_Required__c = true;
        GL.Dimension_3_Required__c = false;
        GL.Dimension_4_Required__c = false;
        GL.Name = Name;
        GL.ownerid = TestGroupId;
        GL.c2g__ReportingCode__c = ReportCode;
        return GL;
    }

    public static c2g__codaAccountingCurrency__c createCurrency(Id testCompany, Id testGroupId)
    {
        c2g__codaAccountingCurrency__c accCurrency = new c2g__codaAccountingCurrency__c();
        accCurrency.c2g__OwnerCompany__c = testCompany;
        accCurrency.c2g__DecimalPlaces__c = 2;
        accCurrency.Name = 'GBP';
        accCurrency.c2g__Dual__c = true ;
        accCurrency.ownerid = testGroupId;
        accCurrency.c2g__Home__c = true;
        accCurrency.c2g__UnitOfWork__c = 2.0;
        return accCurrency;
    }

    public static c2g__codaYear__c createYear(Id TestGroupId)
    {
        c2g__codaYear__c yr = new c2g__codaYear__c();
        yr.Name = String.valueOf(Date.today().year());
        yr.OwnerId = TestGroupId;
        yr.c2g__NumberOfPeriods__c = 12;
        yr.c2g__AutomaticPeriodList__c = true;
        yr.c2g__StartDate__c = Date.valueOf(Date.today().year() + '-01-01 00:00:00');
        yr.c2g__EndDate__c = Date.valueOf(Date.today().year() + '-12-31 00:00:00');
        yr.c2g__PeriodCalculationBasis__c = 'Month End';
        return yr;
    }

    public static c2g__codaDimension2__c createDimension2(String Name, string ReportingCode)
    {  
        c2g__codaDimension2__c dim2 = new c2g__codaDimension2__c();
        dim2.Name = Name;
        dim2.c2g__ReportingCode__c = ReportingCode;
        return dim2;
    }

    public static c2g__codaJournal__c createJournal(Id testGroupId)
    {
        c2g__codaJournal__c journal = new c2g__codaJournal__c();
        journal.c2g__JournalDate__c = Date.today();
        journal.OwnerId = testGroupId;
        return journal;
    }

    //added by edmundCua 07-27-2017
    public static PriceBookEntry createPriceBookEntry(id Product2Id){
        PriceBookEntry pbe = new PriceBookEntry();
        pbe.Pricebook2Id = test.getStandardPriceBookId();
        pbe.product2id = product2id;
        pbe.IsActive = true;
        pbe.UnitPrice = 100.0;
        return pbe;
    }

    //added by edmundCua 07-31-2017
    public static c2g__codaTaxRate__c createTaxRate(id taxCodeId){
        c2g__codaTaxRate__c testTaxRate = new c2g__codaTaxRate__c();
        testTaxRate.c2g__Rate__c = 20.0;
        testTaxRate.c2g__UnitOfWork__c = 1.0;
        testTaxRate.c2g__TaxCode__c = taxCodeId;
        return testTaxRate;
    }
    
    //added by jasper.j.figueroa 10-13-2017
    public static c2g__codaCompany__c createCompany(){
        c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
        testCompany.Name = 'Test Company';
        //testCompany.RecordTypeId = 'VAT';
        testCompany.OwnerId = UserInfo.getUserId();
        testCompany.c2g__Street__c = 'Test Street';     
        testCompany.c2g__City__c = 'Test City';
        testCompany.c2g__StateProvince__c   = 'Test State';
        testCompany.c2g__ZipPostCode__c = 'ZC12345';
        testCompany.c2g__CashMatchingCurrencyMode__c = 'Account';
        testCompany.c2g__Country__c = 'GB';
        testCompany.c2g__ECCountryCode__c = 'GB';
        testCompany.c2g__UnitOfWork__c = 1.0;
        return testCompany;
    }
    
    //added by louis.a.del.rosario 10/23/2017 Create Licensing user
    public static User createLicensingUser(String profile){
        User u = new User();
        u.Alias = 'luser';
        u.Email='luser@testorg.com';
        u.EmailEncodingKey='ISO-8859-1';
        u.FirstName = 'Licensing';
        u.LastName='User';
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_GB';
        u.ProfileId = profile;
        u.TimeZoneSidKey='Europe/London';
        u.UserName='luser@dev.prsppl.com';
        return u;
    }

    //added by jason.e.mactal 10/24/2017
    public static User createUser(string ProfileName){
        Profile p = [SELECT Id FROM Profile WHERE Name = :ProfileName];

        User u = new User();
        u.Alias = 'tuser';
        u.Email='luser@testorg.com';
        u.EmailEncodingKey='ISO-8859-1';
        u.FirstName = 'Licensing';
        u.LastName='User';
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_GB';
        u.ProfileId = p.id;
        u.TimeZoneSidKey='Europe/London';
        u.UserName='luser@dev.prsppl.com';
        u.Division= 'Legal';
        u.Department= 'Legal';
        return u;
    }

    //added by jason.e.mactal 10/24/2017
    public static Account createSupplierAccount(){

        Id supplierRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();


        c2g__codaGeneralLedgerAccount__c generalLedgerAcct =  [select id from c2g__codaGeneralLedgerAccount__c limit 1];

        if(generalLedgerAcct==null){
            generalLedgerAcct =  createGeneralLedger();
            insert generalLedgerAcct;
        }

        c2g__codaTaxCode__c taxCode = [select id from c2g__codaTaxCode__c limit 1];

        if(taxCode==null){
            taxCode = createTaxCode(generalLedgerAcct.Id);
            insert taxCode;
        }

        Account acc = new Account();

        acc.RecordTypeId = supplierRT;
        acc.Name = 'Test Account - Supplier';
        acc.SCMC__Default_Payment_Type__c = 'Purchase Order';
        acc.JVCO_Supplier_Status__c = 'Active';
        acc.JVCO_Email__c = 'testEmail@testDomain.com';
        acc.c2g__CODAECCountryCode__c = 'SG';
        acc.c2g__CODAVATRegistrationNumber__c = '00456';
        acc.c2g__CODABankAccountName__c = 'SampleBankAcct';
        acc.c2g__CODABankAccountNumber__c = '02 456 1234';
        acc.c2g__CODABankSortCode__c = 'SSC-234';
        acc.c2g__CODABankAccountReference__c = '002 234 5678';
        acc.c2g__CODAPaymentMethod__c = 'Bank Transfer';
        acc.c2g__CODADescription1__c = 'Sample Desciption';
        acc.c2g__CODABaseDate1__c = 'Start of Next Month';
        acc.JVCO_Supplier_Category__c = 'Travel';
        acc.c2g__CODADaysOffset1__c = 1;
        acc.c2g__CODAFinanceContact__c = createContact().id;
        acc.c2g__CODAInputVATCode__r = taxCode;
        acc.c2g__CODAAccountsPayableControl__c = generalLedgerAcct.id;

        return acc;
    }
}