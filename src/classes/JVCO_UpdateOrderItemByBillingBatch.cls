public class JVCO_UpdateOrderItemByBillingBatch implements Database.Batchable<sObject>
{
    public List<Integer> yearList = new List<Integer>();
    public List<Integer> monthList = new List<Integer>();
    
    public JVCO_UpdateOrderItemByBillingBatch(List<Integer> yearList, List<Integer> monthList){
        this.yearList = yearList;
        this.monthList = monthList;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc)
    {
            return Database.getQueryLocator([SELECT
                                             ID,
                                             blng__OrderProduct__c,
                                             Name,
                                             blng__EndDate__c, 
                                             blng__Product__c, 
                                             blng__Quantity__c,
                                             blng__StartDate__c, 
                                             blng__Subtotal__c,
                                             blng__TaxAmount__c,
                                             blng__TotalAmount__c,
                                             blng__UnitPrice__c,
                                             CreatedById,
                                             CreatedDate,
                                             Date_of_Last_Distribution_Extraction__c,
                                             Date_of_Last_True_Up_Extraction__c,
                                             Dimension_2s__c,
                                             Distri_Jitterbit_Extraction_Rule_Text__c,
                                             Distribution_Jitterbit_Extraction_Rule__c,
                                             Jitterbit_Extraction_Rule__c,
                                             Jitterbit_Extraction_Rule_Text__c,
                                             JVCO_Affiliation__c,
                                             JVCO_Campaign_Code__c,
                                             JVCO_Date_Override__c,
                                             JVCO_Dimension_1_Temp__c,
                                             JVCO_Dimension_2_Copy__c,
                                             JVCO_Dynamic_Paid_Amount__c,
                                             JVCO_Extraction_Complete_BLNG__c,
                                             JVCO_Extraction_Distribution__c,
                                             JVCO_Paid_Amount__c,
                                             JVCO_Paid_Amount_Tracker__c,
                                             JVCO_Paid_Date__c,
                                             JVCO_Percentage_Paid__c,
                                             JVCO_Period_End_Date__c,
                                             JVCO_Period_Start_Date__c,
                                             JVCO_Sales_Credit_Note_Line_Item__c,
                                             JVCO_Sales_Invoice_Line_Item__c,
                                             JVCO_Sent_to_Parent_Companies__c,
                                             JVCO_Surcharge_Applicable__c,
                                             JVCO_Temp_Budget_Category__c,
                                             JVCO_Temp_End_Date__c,
                                             JVCO_Temp_Start_Date__c,
                                             JVCO_Terminated_Line__c,
                                             JVCO_True_Up__c,
                                             JVCO_UsageSummaryOverride__c,
                                             JVCO_Venue__c,
                                             JVCO_Week__c 
                                             FROM blng__invoiceline__c
                                             WHERE blng__OrderProduct__c != null
                                             AND blng__OrderProduct__r.JVCO_Is_Updated__c = false
                                             AND CALENDAR_YEAR(CreatedDate) IN: yearList
                                             AND CALENDAR_MONTH(CreatedDate) IN: monthList
                                             AND blng__Invoice__r.JVCO_Invoice_Type__c NOT IN ('Surcharge','Credit Cancellation')]);
    }
    public void execute(Database.BatchableContext bc, List<blng__InvoiceLine__c> scope)
    {
        Map<Id, OrderItem> orderItemMap = new Map<Id, OrderItem>();
        
        for(blng__InvoiceLine__c bInvLine : scope)
        { 
            if(!orderItemMap.containsKey(bInvLine.blng__OrderProduct__c)){
                OrderItem ordLine = new OrderItem();
                ordLine.Id = bInvLine.blng__OrderProduct__c;
                ordLine.JVCO_TaxAmount__c = bInvLine.blng__TaxAmount__c;
                ordLine.Date_of_Last_Distribution_Extraction__c = bInvLine.Date_of_Last_Distribution_Extraction__c;
                ordLine.Date_of_Last_True_Up_Extraction__c = bInvLine.Date_of_Last_True_Up_Extraction__c;
                ordLine.JVCO_Dimension_2__c = bInvLine.Dimension_2s__c;
                ordLine.Distri_Jitterbit_Extraction_Rule_Text__c = bInvLine.Distri_Jitterbit_Extraction_Rule_Text__c;
                ordLine.Distribution_Jitterbit_Extraction_Rule__c = bInvLine.Distribution_Jitterbit_Extraction_Rule__c;
                ordLine.Jitterbit_Extraction_Rule__c = bInvLine.Jitterbit_Extraction_Rule__c;
                ordLine.Jitterbit_Extraction_Rule_Text__c = bInvLine.Jitterbit_Extraction_Rule_Text__c;
                ordLine.Affiliation__c = bInvLine.JVCO_Affiliation__c;
                ordLine.JVCO_Campaign_Code__c = bInvLine.JVCO_Campaign_Code__c;
                ordLine.JVCO_Date_Override__c = bInvLine.JVCO_Date_Override__c;
                ordLine.JVCO_Dimension_1_Temp__c = bInvLine.JVCO_Dimension_1_Temp__c;
                ordLine.JVCO_Dynamic_Paid_Amount__c = bInvLine.JVCO_Dynamic_Paid_Amount__c;
                ordLine.JVCO_Extraction_Complete_BLNG__c = bInvLine.JVCO_Extraction_Complete_BLNG__c;
                ordLine.JVCO_Extraction_Distribution__c = bInvLine.JVCO_Extraction_Distribution__c;
                ordLine.JVCO_Paid_Amount__c = bInvLine.JVCO_Paid_Amount__c;
                ordLine.JVCO_Paid_Amount_Tracker__c = bInvLine.JVCO_Paid_Amount_Tracker__c;
                ordLine.JVCO_Paid_Date__c = bInvLine.JVCO_Paid_Date__c;
                ordLine.JVCO_Percentage_Paid__c = bInvLine.JVCO_Percentage_Paid__c;
                ordLine.JVCO_Period_End_Date__c = bInvLine.JVCO_Period_End_Date__c;
                ordLine.JVCO_Period_Start_Date__c = bInvLine.JVCO_Period_Start_Date__c;
                ordLine.JVCO_Sales_Credit_Note_Line_Item__c = bInvLine.JVCO_Sales_Credit_Note_Line_Item__c;
                ordLine.JVCO_Sales_Invoice_Line_Item__c = bInvLine.JVCO_Sales_Invoice_Line_Item__c;
                ordLine.JVCO_Sent_to_Parent_Companies__c = bInvLine.JVCO_Sent_to_Parent_Companies__c;
                ordLine.JVCO_Surcharge_Applicable__c = bInvLine.JVCO_Surcharge_Applicable__c;
                ordLine.JVCO_Temp_Budget_Category__c = bInvLine.JVCO_Temp_Budget_Category__c;
                ordLine.JVCO_Temp_End_Date__c = bInvLine.JVCO_Temp_End_Date__c;
                ordLine.JVCO_Temp_Start_Date__c = bInvLine.JVCO_Temp_Start_Date__c;
                ordLine.JVCO_Terminated_Line__c = bInvLine.JVCO_Terminated_Line__c;
                ordLine.JVCO_True_Up__c = bInvLine.JVCO_True_Up__c;
                ordLine.JVCO_UsageSummaryOverride__c = bInvLine.JVCO_UsageSummaryOverride__c;
                ordLine.JVCO_Venue__c = bInvLine.JVCO_Venue__c;
                ordLine.JVCO_Week__c = bInvLine.JVCO_Week__c;
                ordLine.JVCO_Is_Updated__c = true;
                orderItemMap.put(bInvLine.blng__OrderProduct__c, ordLine);
            }
        }
        if(!orderItemMap.isEmpty()){
            try{
                update orderItemMap.values();
            }
            catch(Exception ex){
                insert createErrorLog(orderItemMap.values()[0].Id, ex.getMessage());
            }
        }
    }
    
    public ffps_custRem__Custom_Log__c createErrorLog(Id relatedObjectId, String errMessage){
        
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
        errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(relatedObjectId);
        errLog.ffps_custRem__Detail__c = errMessage;
        errLog.ffps_custRem__Message__c = 'JVCO_UpdateOrderItemByBillingBatch';
        errLog.ffps_custRem__Grouping__c = 'UPOI-001';
        
        return errLog;
    }
    
    public void finish(Database.BatchableContext bc){}
}