@isTest
private class JVCO_CaseHandlerTest
{
	@testSetup static void setupTestData() 
    {
    	JVCO_SLA_Constants__c cs = JVCO_TestClassObjectBuilder.createSLAConstants();
    	insert cs;

		Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
	    insert acc;

	    Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
	    insert acc2;
	  
		Entitlement e = JVCO_TestClassObjectBuilder.createEntitlement(acc2.id);
		insert e;

    	Case c = JVCO_TestClassObjectBuilder.createCase(e.id); 
    	insert c;

    	update c;

    
    }

	@isTest
	static void testWriteOff()
	{
		Case c = New Case();
		c.Subject = 'Test Subject';
    	c.Status = 'New';
    	c.Origin = 'Field';  	
    	c.JVCO_Closure_Code_Area__c = 'Write Off';
        c.JVCO_Closure_Code__c = 'Write Off Complete';
    	c.RecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Write Off').getRecordTypeId();
    	insert c;
		update c;

		List<Case> l_c = [SELECT Type FROM Case];
		System.Assert(!l_c.isEmpty());

	}
    
	@isTest
    static void testUpdateCreditStatusEnforcement()
    {
        JVCO_Constants__c CONSTANT = new JVCO_Constants__c();
        CONSTANT.JVCO_CaseUpdateCredStatusLimit__c = 20;
        insert CONSTANT;
        
        Account acc = [SELECT Id FROM Account WHERE RecordType.Name = 'Customer Account' LIMIT 1];
        Account acc2 = [SELECT Id FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];
        
		Test.startTest();
        acc2.JVCO_Customer_Account__c = acc.Id;
        update acc2;
        
        Case c = new Case();
        c.Subject = 'Test Subject';
        c.Status = 'In Progress';
        c.Origin = 'Internal';   
        c.Type = 'Enforcement';   
        c.AccountId = acc.Id;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Enforcement - Debt Recovery').getRecordTypeId();
        insert c;
        
        Case c5 = new Case();
        c5.Subject = 'Test Subject';
        c5.Description = 'Test Description';
        c5.Priority = 'Not Applicable';
        c5.Status = 'In Progress';
        c5.Origin = 'Internal';
        c5.Type = 'Insolvency';
        c5.AccountId = acc.Id;
        c5.JVCO_Case_Reason_Level_2__c = 'Bankruptcy';
        c5.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c5;
        
        c.JVCO_Closure_Code_Area__c = 'Enforcement';
        c.Status = 'Closed';
        c.JVCO_Closure_Code__c = 'Debt Paid';
        update c;
        Test.stopTest();
    }
    
    @isTest
    static void testUpdateCreditStatusEnforcement2()
    {
        JVCO_Constants__c CONSTANT = new JVCO_Constants__c();
        CONSTANT.JVCO_CaseUpdateCredStatusLimit__c = 1;
        insert CONSTANT;
        
        Account acc = [SELECT Id FROM Account WHERE RecordType.Name = 'Customer Account' LIMIT 1];
        Account acc2 = [SELECT Id FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];
        
		Test.startTest();
        acc2.JVCO_Customer_Account__c = acc.Id;
        update acc2;

        Case c5 = new Case();
        c5.Subject = 'Test Subject';
        c5.Description = 'Test Description';
        c5.Priority = 'Not Applicable';
        c5.Status = 'In Progress';
        c5.Origin = 'Internal';
        c5.Type = 'Insolvency';
        c5.AccountId = acc.Id;
        c5.JVCO_Case_Reason_Level_2__c = 'Insolvency';
        c5.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c5;
        
        Case c6 = new Case();
        c6.Subject = 'Test Subject';
        c6.Description = 'Test Description';
        c6.Priority = 'Not Applicable';
        c6.Status = 'In Progress';
        c6.Origin = 'Internal';
        c6.Type = 'Insolvency';
        c6.AccountId = acc.Id;
        c6.JVCO_Case_Reason_Level_2__c = 'Bankruptcy';
        c6.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c6;
        
        Case c7 = new Case();
        c7.Subject = 'Test Subject';
        c7.Description = 'Test Description';
        c7.Priority = 'Not Applicable';
        c7.Status = 'In Progress';
        c7.Origin = 'Internal';
        c7.Type = 'Insolvency';
        c7.AccountId = acc.Id;
        c7.JVCO_Case_Reason_Level_2__c = 'Liquidation';
        c7.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c7;

        Case c8 = new Case();
        c8.Subject = 'Test Subject';
        c8.Description = 'Test Description';
        c8.Priority = 'Not Applicable';
        c8.Status = 'In Progress';
        c8.Origin = 'Internal';
        c8.Type = 'Insolvency';
        c8.AccountId = acc.Id;
        c8.JVCO_Case_Reason_Level_2__c = 'Dissolved';
        c8.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c8;
        Test.stopTest();
    }
    
    @isTest
    static void testUpdateCreditStatusInfringement()
    {
        JVCO_Constants__c CONSTANT = new JVCO_Constants__c();
        CONSTANT.JVCO_CaseUpdateCredStatusLimit__c = 20;
        insert CONSTANT;
        
        Account acc = [SELECT Id FROM Account WHERE RecordType.Name = 'Customer Account' LIMIT 1];
        Account acc2 = [SELECT Id FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];
        
		Test.startTest();
        acc2.JVCO_Customer_Account__c = acc.Id;
        update acc2;
        
        Case c2 = new Case();
        c2.Subject = 'Test Subject';
        c2.Description = 'Test Description';
        c2.Priority = 'Not Applicable';
        c2.Status = 'Preparing Case';
        c2.Origin = 'Field';
        c2.Type = 'Infringement';
        c2.JVCO_Case_Responsibility__c = 'Enforcement';
        c2.AccountId = acc.Id;
        c2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Enforcement - Infringement').getRecordTypeId();
        insert c2;
        
        Case c4 = new Case();
        c4.Subject = 'Test Subject';
        c4.Description = 'Test Description';
        c4.Priority = 'Not Applicable';
        c4.Status = 'In Progress';
        c4.Origin = 'Internal';
        c4.Type = 'Insolvency';
        c4.AccountId = acc.Id;
        c4.JVCO_Case_Reason_Level_2__c = 'Dissolved';
        c4.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c4;
        
        c2.JVCO_Closure_Code_Area__c = 'Infringement';
        c2.Status = 'Closed';
        c2.JVCO_Closure_Code__c = 'Premises Closed';
        update c2;
        Test.stopTest();
    }
    
	@isTest
    static void testUpdateCreditStatus()
    {
        JVCO_Constants__c CONSTANT = new JVCO_Constants__c();
        CONSTANT.JVCO_CaseUpdateCredStatusLimit__c = 20;
        insert CONSTANT;
        
        Account acc = [SELECT Id FROM Account WHERE RecordType.Name = 'Customer Account' LIMIT 1];
        Account acc2 = [SELECT Id FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];

        acc2.JVCO_Customer_Account__c = acc.Id;
        update acc2;

        Test.startTest();

        Case c = new Case();
        c.Subject = 'Test Subject';
        c.Status = 'In Progress';
        c.Origin = 'Internal';   
        c.Type = 'Enforcement';   
        c.AccountId = acc.Id;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Enforcement - Debt Recovery').getRecordTypeId();
        insert c;

        acc.JVCO_In_Enforcement__c = false;
        update acc;
		
        Case c1 = new Case();
        c1.Subject = 'Test Subject';
        c1.Description = 'Test Description';
        c1.Priority = 'Not Applicable';
        c1.Status = 'In Progress';
        c1.Origin = 'Internal';
        c1.Type = 'Insolvency';
        c1.AccountId = acc.Id;
        c1.JVCO_Case_Reason_Level_2__c = 'Insolvency';
        c1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c1;
        
        Case c3 = new Case();
        c3.Subject = 'Test Subject';
        c3.Description = 'Test Description';
        c3.Priority = 'Not Applicable';
        c3.Status = 'In Progress';
        c3.Origin = 'Internal';
        c3.Type = 'Insolvency';
        c3.AccountId = acc.Id;
        c3.JVCO_Case_Reason_Level_2__c = 'Liquidation';
        c3.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c3;
        Test.stopTest();
    }
	@isTest
    static void testUpdateCreditStatusVouluntary()
    {
        JVCO_Constants__c CONSTANT = new JVCO_Constants__c();
        CONSTANT.JVCO_CaseUpdateCredStatusLimit__c = 20;
        insert CONSTANT;
        
        Account acc = [SELECT Id FROM Account WHERE RecordType.Name = 'Customer Account' LIMIT 1];
        Account acc2 = [SELECT Id FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];

        acc2.JVCO_Customer_Account__c = acc.Id;
        update acc2;
        
        Case c6 = new Case();
        c6.Subject = 'Test Subject';
        c6.Description = 'Test Description';
        c6.Priority = 'Not Applicable';
        c6.Status = 'In Progress';
        c6.Origin = 'Internal';
        c6.Type = 'Insolvency';
        c6.AccountId = acc.Id;
        c6.JVCO_Case_Reason_Level_2__c = 'Voluntary Arrangement';
        c6.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c6;
		
        Case c7 = new Case();
        c7.Subject = 'Test Subject';
        c7.Description = 'Test Description';
        c7.Priority = 'Not Applicable';
        c7.Status = 'In Progress';
        c7.Origin = 'Internal';
        c7.Type = 'Insolvency';
        c7.AccountId = acc.Id;
        c7.JVCO_Case_Reason_Level_2__c = 'Administration';
        c7.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c7;
        
        c6.JVCO_Closure_Code_Area__c = 'Insolvency';
        c6.Status = 'Closed';
        c6.JVCO_Closure_Code__c = 'Write Off';
        update c6;
    }
    
    @isTest
    static void testUpdateCreditStatus2()
    {
        JVCO_Constants__c CONSTANT = new JVCO_Constants__c();
        CONSTANT.JVCO_CaseUpdateCredStatusLimit__c = 20;
        insert CONSTANT;
        
        Account acc = [SELECT Id FROM Account WHERE RecordType.Name = 'Customer Account' LIMIT 1];
        Account acc2 = [SELECT Id FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];

        acc2.JVCO_Customer_Account__c = acc.Id;
        update acc2;

        Test.startTest();
        Case c = new Case();
        c.Subject = 'Test Subject';
        c.Status = 'In Progress';
        c.Origin = 'Internal';   
        c.Type = 'Enforcement';   
        c.AccountId = acc.Id;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Enforcement - Debt Recovery').getRecordTypeId();
        insert c;

        c.JVCO_Closure_Code_Area__c = 'Enforcement';
        c.Status = 'Closed';
        c.JVCO_Closure_Code__c = 'Leave Debt on Account';
        update c;

        Case c8 = new Case();
        c8.Subject = 'Test Subject';
        c8.Description = 'Test Description';
        c8.Priority = 'Not Applicable';
        c8.Status = 'In Progress';
        c8.Origin = 'Internal';
        c8.Type = 'Insolvency';
        c8.AccountId = acc.Id;
        c8.JVCO_Case_Reason_Level_2__c = 'Receivership';
        c8.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c8;
        Test.stopTest();
    }
    
	@isTest
    static void testUpdateCreditStatusProgressedLiq()
    {
        JVCO_Constants__c CONSTANT = new JVCO_Constants__c();
        CONSTANT.JVCO_CaseUpdateCredStatusLimit__c = 20;
        insert CONSTANT;
        
        Account acc = [SELECT Id FROM Account WHERE RecordType.Name = 'Customer Account' LIMIT 1];
        Account acc2 = [SELECT Id FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];

        acc2.JVCO_Customer_Account__c = acc.Id;
        update acc2;

        Test.startTest();
        Case c9 = new Case();
        c9.Subject = 'Test Subject';
        c9.Description = 'Test Description';
        c9.Priority = 'Not Applicable';
        c9.Status = 'In Progress';
        c9.Origin = 'Internal';
        c9.Type = 'Insolvency';
        c9.AccountId = acc.Id;
        c9.JVCO_Case_Reason_Level_2__c = 'Dissolution';
        c9.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c9;
        
        Case c10 = new Case();
        c10.Subject = 'Test Subject';
        c10.Description = 'Test Description';
        c10.Priority = 'Not Applicable';
        c10.Status = 'In Progress';
        c10.Origin = 'Internal';
        c10.Type = 'Insolvency';
        c10.AccountId = acc.Id;
        c10.JVCO_Case_Reason_Level_2__c = 'Progressed to Liquidation';
        c10.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c10;

        c10.JVCO_Closure_Code_Area__c = 'Insolvency';
        c10.Status = 'Closed';
        c10.JVCO_Closure_Code__c = 'Insolvency Action Complete';
        update c10;

        Test.stopTest();
    }
    
    @isTest
    static void testUpdateCreditStatus3()
    {
        JVCO_Constants__c CONSTANT = new JVCO_Constants__c();
        CONSTANT.JVCO_CaseUpdateCredStatusLimit__c = 1;
        insert CONSTANT;
        
        Account acc = [SELECT Id FROM Account WHERE RecordType.Name = 'Customer Account' LIMIT 1];
        Account acc2 = [SELECT Id FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];

        acc2.JVCO_Customer_Account__c = acc.Id;
        update acc2;

        Test.startTest();
        Case c = new Case();
        c.Subject = 'Test Subject';
        c.Status = 'In Progress';
        c.Origin = 'Internal';   
        c.Type = 'Enforcement';   
        c.AccountId = acc.Id;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Enforcement - Debt Recovery').getRecordTypeId();
        insert c;

        c.JVCO_Closure_Code_Area__c = 'Enforcement';
        c.Status = 'Closed';
        c.JVCO_Closure_Code__c = 'Leave Debt on Account';
        update c;


        Case c6 = new Case();
        c6.Subject = 'Test Subject';
        c6.Description = 'Test Description';
        c6.Priority = 'Not Applicable';
        c6.Status = 'In Progress';
        c6.Origin = 'Internal';
        c6.Type = 'Insolvency';
        c6.AccountId = acc.Id;
        c6.JVCO_Case_Reason_Level_2__c = 'Voluntary Arrangement';
        c6.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c6;

        Case c7 = new Case();
        c7.Subject = 'Test Subject';
        c7.Description = 'Test Description';
        c7.Priority = 'Not Applicable';
        c7.Status = 'In Progress';
        c7.Origin = 'Internal';
        c7.Type = 'Insolvency';
        c7.AccountId = acc.Id;
        c7.JVCO_Case_Reason_Level_2__c = 'Administration';
        c7.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c7;

        Case c8 = new Case();
        c8.Subject = 'Test Subject';
        c8.Description = 'Test Description';
        c8.Priority = 'Not Applicable';
        c8.Status = 'In Progress';
        c8.Origin = 'Internal';
        c8.Type = 'Insolvency';
        c8.AccountId = acc.Id;
        c8.JVCO_Case_Reason_Level_2__c = 'Receivership';
        c8.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c8;

        Case c9 = new Case();
        c9.Subject = 'Test Subject';
        c9.Description = 'Test Description';
        c9.Priority = 'Not Applicable';
        c9.Status = 'In Progress';
        c9.Origin = 'Internal';
        c9.Type = 'Insolvency';
        c9.AccountId = acc.Id;
        c9.JVCO_Case_Reason_Level_2__c = 'Dissolution';
        c9.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c9;

        Case c10 = new Case();
        c10.Subject = 'Test Subject';
        c10.Description = 'Test Description';
        c10.Priority = 'Not Applicable';
        c10.Status = 'In Progress';
        c10.Origin = 'Internal';
        c10.Type = 'Insolvency';
        c10.AccountId = acc.Id;
        c10.JVCO_Case_Reason_Level_2__c = 'Progressed to Liquidation';
        c10.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Insolvency').getRecordTypeId();
        insert c10;

        c10.JVCO_Closure_Code_Area__c = 'Insolvency';
        c10.Status = 'Closed';
        c10.JVCO_Closure_Code__c = 'Insolvency Action Complete';
        update c10;

        c6.JVCO_Closure_Code_Area__c = 'Insolvency';
        c6.Status = 'Closed';
        c6.JVCO_Closure_Code__c = 'Write Off';
        update c6;

        Test.stopTest();
    }
}