/* ------------------------------------------------------------------------------------------------
   Name:            JVCO_ContractReviewQuotes_Queueable
   Description:     Queueable implementation for JVCO_ContractReviewQuotesBatch

   Date            Version     Author                       Summary of Changes 
   -----------     -------     -----------------            ---------------------------------------
   09-Jul-2018     0.1         jules.osberg.a.pablo         Initial Version
   09-Oct-2018     0.2         rhys.j.c.dela.cruz           Additional changes to Queueable
   22-May-2019     0.3         rhys.j.c.dela.cruz           GREEN-33618 - Changes to queueable to accomodate new Temp Object
------------------------------------------------------------------------------------------------ */
public class JVCO_ContractReviewQuotes_Queueable implements Queueable {
    private Map<Id, Opportunity> opportunityMap;
    private Account accountRecord;  
    private Integer quoteCount;
    private Integer errorQuoteCount;
    private String queueableErrorMessage;
    private Set<Id> opportunityIdSet;
    private Integer opportunityOriginalSize;
    private Integer qblTempCtr = 0;

    public JVCO_ContractReviewQuotes_Queueable(Account accountRecord) {
        opportunityMap = new Map<Id, Opportunity>((List<Opportunity>)Database.query(JVCO_ContractReviewQuotesHelper.getQueryString(accountRecord)));
        this.accountRecord = accountRecord;
        quoteCount = 0;
        errorQuoteCount = 0;
        queueableErrorMessage = '';
        opportunityIdSet = new Set<Id>();
        opportunityOriginalSize = opportunityMap.size();
        JVCO_ContractReviewQuotesHelper.sendBeginJobEmail(accountRecord, opportunityOriginalSize, true);
    }

    public JVCO_ContractReviewQuotes_Queueable(Map<Id, Opportunity> opportunityMap, Account accountRecord, Integer quoteCount, Integer errorQuoteCount, String queueableErrorMessage, Set<Id> opportunityIdSet, Integer opportunityOriginalSize, Integer qblTempCtr) {
        this.opportunityMap = opportunityMap;
        this.accountRecord = accountRecord;
        this.quoteCount = quoteCount;
        this.errorQuoteCount = errorQuoteCount;
        this.queueableErrorMessage = queueableErrorMessage;
        this.opportunityIdSet = opportunityIdSet;
        this.opportunityOriginalSize = opportunityOriginalSize;
        this.qblTempCtr = qblTempCtr;
    }

    public void execute(QueueableContext context) {

        List<JVCO_KATempHolder__c> kaTempList = [SELECT JVCO_AccountId__c, JVCO_OppId__c, JVCO_Contracted__c, JVCO_Ordered__c, JVCO_ProcessCompleted__c FROM JVCO_KATempHolder__c WHERE JVCO_AccountId__c =: accountRecord.Id AND JVCO_Contracted__c = true AND JVCO_Ordered__c = false ORDER BY ID ASC];

        if(!kaTempList.isEmpty()){

            for(JVCO_KATempHolder__c kaTempRec : kaTempList){

                opportunityIdSet.add(kaTempRec.JVCO_OppId__c);

                if(!opportunityMap.isEmpty() && opportunityMap.containsKey(kaTempRec.JVCO_OppId__c)){

                    opportunityMap.remove(kaTempRec.JVCO_OppId__c);
                }
            }
        }

        //28-June-2019 rhys.j.dela.cruz GREEN-33618 - New field on Account to Abort Running Queueable Job
        List<Account> accForAbort = [SELECT Id, JVCO_AbortRunningQueueable__c, JVCO_ContractReviewQuotesQueueable__c FROM Account WHERE Id =: accountRecord.Id LIMIT 1];
        Boolean abortJob = false;

        if(!accForAbort.isEmpty()){
            abortJob = accForAbort[0].JVCO_AbortRunningQueueable__c;
        }

        //21-May-2019 rhys.j.c.dela.cruz GREEN-33618
        Boolean contLoop = true;
        List<JVCO_KATempHolder__c> kaTempHolderList = new List<JVCO_KATempHolder__c>();

        do{

            if(!opportunityMap.isEmpty()){

                if(opportunityMap.values()[0].SBQQ__Contracted__c && !opportunityMap.values()[0].SBQQ__Ordered__c){
        
                    JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
                    kaTempRec.Name = opportunityMap.values()[0].Id;
                    kaTempRec.JVCO_AccountId__c = opportunityMap.values()[0].AccountId;
                    kaTempRec.JVCO_OppId__c = opportunityMap.values()[0].Id;
                    kaTempRec.JVCO_Contracted__c = true;
                    kaTempHolderList.add(kaTempRec);

                    opportunityIdSet.add(opportunityMap.values()[0].Id);
                    opportunityMap.remove(opportunityMap.values()[0].Id);
                }
                else{
                    contLoop = false;
                }
            }
            else{
                contLoop = false;
            }
        }
        while(contLoop);

        if(!kaTempHolderList.isEmpty()){

            upsert kaTempHolderList Name;
        }

        if(!opportunityMap.isEmpty()) {
            Opportunity opportunityRecord = opportunityMap.values()[0];
            System.debug('@@opportunityRecord: ' + opportunityRecord);
            List<Opportunity> tempOpportunityList = new List<Opportunity>();
            tempOpportunityList.add(opportunityMap.values()[0]);

            String errorMessage = JVCO_ContractReviewQuotesHelper.executeContractAmendmentQuotes(tempOpportunityList);
            if(errorMessage == ''){
                quoteCount++;
            }else{
                errorQuoteCount++;
                queueableErrorMessage += '<li>' + errorMessage + '</li>';
            }

            opportunityIdSet.add(opportunityRecord.Id);
            opportunityMap.remove(opportunityRecord.Id);
        }

        if(abortJob){

            JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
            kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractReview';
            kaTempRec.JVCO_ErrorMessage__c = 'Queueable Job Aborted: ' + System.now();

            upsert kaTempRec Name;

            accForAbort[0].JVCO_AbortRunningQueueable__c = false;
            accForAbort[0].JVCO_ContractReviewQuotesQueueable__c = '';

            update accForAbort[0];
        }
        else if(!opportunityMap.isEmpty() && !abortJob){
            
            //On 4th Queueable, instead of calling again, this will schedule the class instead
            if(qblTempCtr < 3){
                qblTempCtr++;
                if(!Test.isRunningTest()){

                    Id queueableId = System.enqueueJob(new JVCO_ContractReviewQuotes_Queueable(opportunityMap, accountRecord, quoteCount, errorQuoteCount, queueableErrorMessage, opportunityIdSet, opportunityOriginalSize, qblTempCtr));

                    JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
                    kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractReview';
                    kaTempRec.JVCO_JobId__c = queueableId;
                    kaTempRec.JVCO_ErrorMessage__c = '';

                    upsert kaTempRec Name;
                }
            }
            else{

                JVCO_KeyAccountHelperSchedulable kaHelperSched = new JVCO_KeyAccountHelperSchedulable(opportunityMap, accountRecord, quoteCount, errorQuoteCount, queueableErrorMessage, opportunityIdSet, opportunityOriginalSize, 'ContractReview');

                Integer schedulerBuffer = (Integer)JVCO_KABatchSetting__c.getOrgDefaults().JVCO_SchedulerBuffer__c != null ? (Integer)JVCO_KABatchSetting__c.getOrgDefaults().JVCO_SchedulerBuffer__c : 45;

                Datetime dt = DateTime.now().addSeconds(schedulerBuffer);
                String cronExp = '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();

                while(dt <= DateTime.now()){

                    dt = dt.addSeconds(schedulerBuffer);
                }
                
                Id schedId = System.schedule('Key Account Helper Sched: Contract Review ' + accountRecord.Id, cronExp, kaHelperSched);

                JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
                kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractReview';
                kaTempRec.JVCO_JobId__c = schedId;
                kaTempRec.JVCO_ErrorMessage__c = '';

                upsert kaTempRec Name;
            }
        }
        else{
            
            JVCO_ContractReviewQuotesHelper.sendCompletionEmail(accountRecord, UserInfo.getUserId(), quoteCount, errorQuoteCount, queueableErrorMessage, opportunityOriginalSize);
            if(!Test.isRunningTest()) {
                Id queueableId = System.enqueueJob(new JVCO_OrderQuotes_Queueable(accountRecord, opportunityIdSet, true));
                JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord, 'JVCO_ContractReviewQuotesQueueable__c', '', 'JVCO_ContractReviewQuotesLastSubmitted__c');
                JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord, 'JVCO_OrderReviewQuotesQueueable__c', 'Queueable', 'JVCO_OrderReviewQuotesLastSubmitted__c');

                JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
                kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractReview';
                kaTempRec.JVCO_JobId__c = queueableId;
                kaTempRec.JVCO_ErrorMessage__c = '';

                upsert kaTempRec Name;
            }
        }
    }
}