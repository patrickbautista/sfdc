/* ----------------------------------------------------------------------------------------------
Name: JVCO_LicAccCompleteAmendmentBatchTest
Description: Test Class of JVCO_LicAccCompleteAmendmentBatch

Date         Version     Author                          Summary of Changes 
-----------  -------     -----------------               -----------------------------------------
16-Jul-2020   0.1         Accenture-rhys.j.c.dela.cruz   Intial creation
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_LicAccCompleteAmendmentBatchTest
{
    @testSetup static void insertValues()
    {

        List<JVCO_Constants__c> custSetting = new List<JVCO_Constants__c>();
        custSetting.add(new JVCO_Constants__c(JVCO_Quote_Recalculation_Time_Buffer__c = 5));
        insert custSetting;
        
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();  
        insert qProcess;

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Account acc =  JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.JVCO_Renewal_Scenario__c = '';
        insert acc2;

        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_End_Date__c = null;
        aff.JVCO_Closure_Reason__c = null;
        insert aff;

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        opp.JVCO_OpportunityCancelled__c = false;
        insert opp;

        Contract c1 = new Contract();
        c1.SBQQ__RenewalQuoted__c = false;  
        c1.AccountID = acc2.Id;
        c1.StartDate = System.today();
        c1.ContractTerm = 12;
        c1.SBQQ__RenewalQuoted__c = false;
        c1.JVCO_Renewal_Generated__c = false;
        c1.JVCO_RenewableQuantity__c = 1;
        c1.EndDate = System.today().addMonths(12);
        c1.Name = 'Test Contract 18023-L';
        c1.SBQQ__Opportunity__c = opp.Id;    
        c1.JVCO_TempStartDate__c = System.today().addMonths(3);
        c1.JVCO_TempEndDate__c = System.today().addMonths(4);
        c1.JVCO_ProcessedByCreditsBatch__c = true;
        insert c1;

        SBQQ__Subscription__c sub = new SBQQ__Subscription__c();
        sub.SBQQ__Contract__c = c1.Id;
        sub.SBQQ__Account__c = acc2.Id;
        sub.SBQQ__Quantity__c = 300;
        sub.SBQQ__Product__c = prod.Id;
        sub.SBQQ__ListPrice__c = 1;
        sub.SBQQ__NetPrice__c = 300;
        sub.SBQQ__RenewalQuantity__c = 300;
        sub.ChargeYear__c = 'Current Year';
        sub.SBQQ__BillingFrequency__c = 'Annual';
        sub.SBQQ__BillingType__c = 'Advance';
        sub.SBQQ__ChargeType__c = 'Recurring';
        sub.SBQQ__CustomerPrice__c = 1;
        sub.SBQQ__ProrateMultiplier__c = 1;
        sub.SBQQ__RegularPrice__c = 1;
        sub.LicenceFee__c = 300;
        sub.Start_Date__c = System.today();
        sub.SBQQ__SubscriptionStartDate__c = System.today();
        sub.SBQQ__SubscriptionEndDate__c = System.today().addMonths(12);
        sub.End_Date__c = System.today().addMonths(12);

        insert sub;

        Test.startTest();

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, 'Annually');
        q.SBQQ__MasterContract__c = c1.Id;
        q.JVCO_Quote_Auto_Renewed__c = true;
        q.SBQQ__Status__c = 'Draft';
        q.Recalculate__c = false;
        q.SBQQ__Type__c = 'Quote';   
        q.SBQQ__Primary__c = true;
        q.Start_Date__c = System.today() - 5;
        q.End_Date__c = System.today().addMonths(12);
        insert q;

        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];
       
        q.SBQQ__Type__c = 'Amendment';
        q.Recalculate__c = true;
        update q;
        
        //SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, prod.Id);

        ql.CreatedDate = Datetime.Now();
        //ql.JVCO_Salesforce_Last_Recalculated_Time__c = Datetime.Now().addDays(3);
        ql.JVCO_Steelbrick_Last_Recalculated_Time__c = Datetime.Now().addDays(-1);
        ql.SPVAvailability__c = true;
        insert ql;

        JVCO_TempInvoiceProcessingData__c tempRec = new JVCO_TempInvoiceProcessingData__c();
        tempRec.JVCO_Batch_Name__c = 'JVCO_LicAccGenerateAmendmentBatch';
        tempRec.JVCO_Id1__c = c1.Id;
        insert tempRec;

        Test.stopTest();
    }

    @isTest
    static void testLicAccCompleteAmendmentBatch(){

        Test.startTest();
        JVCO_LicAccCompleteAmendmentBatch licAccComplete = new JVCO_LicAccCompleteAmendmentBatch();
        Database.executeBatch(licAccComplete);
        Test.stopTest();
    }
}