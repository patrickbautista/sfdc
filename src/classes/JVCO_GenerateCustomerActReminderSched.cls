/*----------------------------------------------------------------------------------------------
Name: JVCO_GenerateCustomerActReminderSched.cls 
Test class: JVCO_GenerateCustomerActRemindersTest.cls
Description: Sched Batch class that updates CA that has Last Reminder Level
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
18-Oct-2019  0.1         patrick.t.bautista     Initial creation
----------------------------------------------------------------------------------------------- */
global class JVCO_GenerateCustomerActReminderSched implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
        //Retrieve Custom Settings Name "Customer Account Dunning Settings"
    	final JVCO_Customer_Account_Dunning_Settings__c DUNNING_SETTINGS = JVCO_Customer_Account_Dunning_Settings__c.getOrgDefaults();
        final Decimal batchSize = DUNNING_SETTINGS.JVCO_Batch_Size__c == NUll ? 20 : DUNNING_SETTINGS.JVCO_Batch_Size__c;  
		Database.executeBatch(new JVCO_GenerateCustomerAccountReminders(),Integer.valueOf(batchSize));
    }
    
    private static String getSchedulerExpression(DateTime dt) 
    {
        return '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();    
    }
    
    public static void CallExecuteMomentarily()  
    {
        System.schedule('JVCO_GenerateCustomerActReminderSched', getSchedulerExpression(DateTime.Now().addSeconds(5)), new JVCO_GenerateCustomerActReminderSched());
    }
}