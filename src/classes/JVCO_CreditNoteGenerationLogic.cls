/* ----------------------------------------------------------------------------------------------
    Name: JVCO_CreditNoteGenerationLogic.cls 
    Description: Logic class for Sales Credit Note generation

    Date         Version     Author                     Summary of Changes 
    -----------  -------     -----------------          -----------------------------------------
    21-Mar-2017   0.1        ryan.i.r.limlingan         Intial creation
    17-Apr-2017   0.2        ryan.i.r.limlingan         Added setVenueFields()
    16-Sep-2017   1.0        franz.g.a.dimaapi          Refactor Code for GREEN-21231
    24-Sep-2017   1.0        franz.g.a.dimaapi          Improved CreditNote Performance for GREEN-22953
    21-Oct-2017   1.1        franz.g.a.dimaapi          Add Invoice Scheduler functionality
    31-Oct-2017   1.2        franz.g.a.dimaapi          Refactor Vat Registration No. Logic for Company
    20-Nov-2017   1.3        mary.anna.a.ruelan         Updated generateCreditNote to fetch related contracts for GREEN-25814
    07-Dec-2017   1.4        john.patrick.valdez        Added generateCreditNoteFromGroupOrder, createLineItemsFromGroupOrder and
                                                        linkInvoicesFromGroup, llinkContract to Create Sales Credit Note for GREEN-26386
    12-Dec-2017   1.5        franz.g.a.dimaapi          Refactor whole class, transfer logic setup to billingutil, 
                                                        removed uneccessary logics and variables, improved performance issues
    16-Jan-2018   1.6        filip.bezak@accenture.com  GREEN-28418 - Tax Code for Sales Credit Note 
    12-Apr-2018   1.7        franz.g.a.dimaapi          Removed Multiple Order functionality - GREEN-30591
----------------------------------------------------------------------------------------------- */
public class JVCO_CreditNoteGenerationLogic
{
    /*//Map Billing Inv to Credit Note Line Item Map by Dimension 2
    private static Map<Id, Map<String,c2g__codaCreditNoteLineItem__c>> bInvToCNoteLineItemMap;
    private static Map<String, Id> productNameIdMap;
    private static Map<String,Id> dim2Map;
    //Opportunity Sets to Invoiced
    private static Set<Id> oppIdSet = new Set<Id>();
    //User Company
    private static c2g__codaUserCompany__c userCompany;
    // Map Account Name to Tax Registration Number
    private static Map<String, String> taxRegNumMap;
    
    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Creates a Credit Note record from a Billing Invoice
        Inputs: List<blng__Invoice__c>
        Returns: void
        <Date>      <Authors Name>              <Brief Description of Change> 
        20-Mar-2017 ryan.i.r.limlingan          Initial version of function
        16-Jan-2018 franz.g.a.dimaapi           Changed Period Start/End Date Logic
        19-Jan-2018 filip.bezak@accenture.com   GREEN-28418 - Tax Code for Sales Credit Note
    ----------------------------------------------------------------------------------------------- */
    /*public static void generateCreditNote(List<blng__Invoice__c> blngInvoice)
    {
        Map<Id, blng__Invoice__c> blngInvoiceMap = new Map<Id, blng__Invoice__c>(
                                                    [SELECT Id, blng__Account__c, 
                                                    blng__Account__r.c2g__CODADimension1__c,
                                                    blng__Account__r.c2g__CODADimension2__c,
                                                    blng__Account__r.c2g__CODADimension3__c,
                                                    blng__Account__r.c2g__CODAOutputVATCode__c,
                                                    blng__Account__r.c2g__CODAOutputVATCode__r.Name, //filip.bezak@accenture.com GREEN-28418
                                                    blng__BillToContact__c, blng__DueDate__c,
                                                    blng__InvoiceDate__c, blng__InvoiceStatus__c, 
                                                    blng__Order__r.SBQQ__Quote__c, 
                                                    blng__Order__r.SBQQ__Quote__r.SBQQ__Type__c,
                                                    blng__Order__r.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c,
                                                    blng__Order__r.SBQQ__Quote__r.SBQQ__Opportunity2__c,Migrated_Credit_Note__c,
                                                    blng__Order__r.SBQQ__Quote__r.SBQQ__Opportunity2__r.Type,
                                                    blng__Order__r.SBQQ__Quote__r.SBQQ__MasterContract__c,blng__Order__r.JVCO_Order_Group__c,
                                                    blng__Order__r.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Billing_Invoice__c, 
                                                    blng__Order__r.JVCO_Credit_Reason__c, 
                                                    JVCO_Sales_Credit_Note__c,
                                                    CreatedById,
                                                    JVCO_Original_Invoice_Cancelled__c, JVCO_Migrated__c,
                                                    JVCO_Invoice_Legacy_Number__c,JVCO_Temp_Link_Positive_Invoice__c,
                                                    JVCO_PRS_Total_Amount__c, JVCO_PPL_Total_Amount__c, JVCO_VPL_Total_Amount__c, 
                                                    JVCO_PRS_Tax_Total_Amount__c, JVCO_PPL_Tax_Total_Amount__c, JVCO_VPL_Tax_Total_Amount__c,
                                                    (SELECT Id, JVCO_Dimension_2__c
                                                        FROM blng__InvoiceInvoiceLines__r)
                                                    FROM blng__Invoice__c WHERE Id IN: blngInvoice
                                                    AND blng__Order__r.JVCO_Order_Group__c = null]);

        Map<Id, AggregateResult> invIdToAggregateResultMap = new Map<Id, AggregateResult>();
        for(AggregateResult ar : [SELECT blng__Invoice__c bInvId,
                                    MIN(JVCO_Start_Date__c)minStartDate,
                                    MAX(JVCO_End_Date__c)maxEndDate
                                    FROM blng__InvoiceLine__c
                                    WHERE blng__Invoice__c IN : blngInvoiceMap.keySet()
                                    GROUP BY blng__Invoice__c])
        {
            if(ar.get('minStartDate') != null && ar.get('maxEndDate') != null)
            {
                invIdToAggregateResultMap.put((Id)ar.get('bInvId'), ar);
            }
        }

        setCompanyVATRegistrationNum();
        JVCO_FFUtil.constructSupplierRecordTaxRegistrationNumber();
        
        List<c2g__codaCreditNote__c> cNoteList = new List<c2g__codaCreditNote__c>();
        Map<Id,c2g__codaCreditNote__c> bInvCNoteMap = new Map<Id,c2g__codaCreditNote__c>();
        for (Id bInvId : blngInvoiceMap.keySet())
        {
            blng__Invoice__c bInv = blngInvoiceMap.get(bInvId);
            c2g__codaCreditNote__c creditNote = new c2g__codaCreditNote__c();
            AggregateResult ar = invIdToAggregateResultMap.get(bInvId);
            creditNote.c2g__Account__c = bInv.blng__Account__c;
            creditNote.JVCO_Temp_Link_Positive_Invoice__c = bInv.JVCO_Temp_Link_Positive_Invoice__c;
            creditNote.c2g__Invoice__c = null;
            creditNote.JVCO_Credit_Reason__c = bInv.blng__Order__r.JVCO_Credit_Reason__c;
            creditNote.JVCO_Sales_Rep__c = bInv.CreatedById;
            creditNote.c2g__DueDate__c = bInv.blng__DueDate__c;
            creditNote.c2g__CreditNoteDate__c = bInv.blng__InvoiceDate__c;
            creditNote.c2g__DeriveCurrency__c = true;
            creditNote.c2g__DerivePeriod__c = true;
            if(invIdToAggregateResultMap.containsKey(bInvId))
            {
                creditNote.JVCO_Period_Start_Date__c = (Date)invIdToAggregateResultMap.get(bInvId).get('minStartDate');
                creditNote.JVCO_Period_End_Date__c = (Date)invIdToAggregateResultMap.get(bInvId).get('maxEndDate');
            }
            
            if(bInv.blng__Order__r.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c ==null)
            {
                creditNote.JVCO_VAT_Registration_Number__c = userCompany.c2g__Company__r.c2g__VATRegistrationNumber__c;
            }else
            {
                creditNote.Parent__c = bInv.blng__InvoiceInvoiceLines__r.get(0).JVCO_Dimension_2__c; 
                if(bInv.blng__Order__r.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c.contains('PRS'))
                {
                    creditNote.JVCO_VAT_Registration_Number__c = JVCO_FFUtil.getTaxPRSRegNum();
                }else
                {
                    creditNote.JVCO_VAT_Registration_Number__c = JVCO_FFUtil.getTaxRegistrationNumber(bInv.blng__InvoiceInvoiceLines__r);
                }
            }
            
            creditNote.c2g__DeriveDueDate__c = FALSE;
            cNoteList.add(creditNote);
            bInvCNoteMap.put(bInv.Id, creditNote);
            oppIdSet.add(bInv.blng__Order__r.SBQQ__Quote__r.SBQQ__Opportunity2__c);
        }

        if(!cNoteList.isEmpty())
        {
            insert cNoteList;
            dim2Map = constructDimension2Map();
            createCreditLineItems(bInvCNoteMap, blngInvoiceMap);
            linkToOriginal(bInvCNoteMap, blngInvoiceMap);
            linkCredit(bInvCNoteMap, blngInvoiceMap);
            postCreditNote(cNoteList);
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Creates a Sales CreditNote record from migrated data
        Inputs: List<blng__Invoice__c>
        Returns: void
        <Date>      <Authors Name>              <Brief Description of Change> 
        23-Jun-2017 franz.g.a.dimaapi           Initial version of function
        19-Jan-2018 filip.bezak@accenture.com   GREEN-28418 - Tax Code for Sales Credit Note
    ----------------------------------------------------------------------------------------------- */
    /*public static void generateCreditNoteFromDatamig(List<blng__Invoice__c> blngInvoice)
    {
        Map<Id, blng__Invoice__c> blngInvoiceMap = new Map<Id, blng__Invoice__c>(
                                                    [SELECT Id, blng__Account__c, 
                                                    blng__Account__r.JVCO_Dunning_Letter_Language__c,
                                                    blng__Account__r.c2g__CODADimension1__c, 
                                                    blng__Account__r.c2g__CODADimension3__c,
                                                    blng__Account__r.c2g__CODAOutputVATCode__c,
                                                    blng__Account__r.c2g__CODAOutputVATCode__r.Name, //filip.bezak@accenture.com GREEN-28418
                                                    blng__BillToContact__c, blng__DueDate__c,
                                                    blng__InvoiceDate__c, blng__InvoiceStatus__c, 
                                                    CreatedById,
                                                    JVCO_Sales_Credit_Note__c,
                                                    JVCO_Original_Invoice_Cancelled__c, 
                                                    JVCO_Invoice_Legacy_Number__c, 
                                                    JVCO_Temp_Link_Positive_Invoice__c,
                                                    JVCO_PRS_Total_Amount__c, JVCO_PPL_Total_Amount__c, JVCO_VPL_Total_Amount__c, 
                                                    JVCO_PRS_Tax_Total_Amount__c, JVCO_PPL_Tax_Total_Amount__c, JVCO_VPL_Tax_Total_Amount__c,
                                                    JVCO_VAT_Amount__c, JVCO_Migrated__c,
                                                    Migrated_Credit_Note__c,
                                                        (SELECT Id, JVCO_Dimension_2__c
                                                        FROM blng__InvoiceInvoiceLines__r)
                                                    FROM blng__Invoice__c WHERE Id IN: blngInvoice]);

        //Construct Tax Registration Number of SUpplier Account - Franz Dimaapi 10/07/2017
        JVCO_FFUtil.constructSupplierRecordTaxRegistrationNumber();

        List<c2g__codaCreditNote__c> cNoteList = new List<c2g__codaCreditNote__c>();
        Map<Id,c2g__codaCreditNote__c> bInvCNoteMap = new Map<Id,c2g__codaCreditNote__c>();

        for(Id bId : blngInvoiceMap.keySet())
        {
            blng__Invoice__c bInv = blngInvoiceMap.get(bId);
            c2g__codaCreditNote__c creditNote = new c2g__codaCreditNote__c();
            creditNote.c2g__Account__c = bInv.blng__Account__c;
            //field added Lalit GREEN-22398: (putting the value of JVCO_Temp_Link_Positive_Invoice__c from billing invoice to the credit note.)
            creditNote.JVCO_Temp_Link_Positive_Invoice__c = bInv.JVCO_Temp_Link_Positive_Invoice__c;

            // Flag used for Surcharge Sales Invoice to be cancelled
            if(!bInv.JVCO_Original_Invoice_Cancelled__c)
            {
                creditNote.c2g__DueDate__c = bInv.blng__DueDate__c;
                creditNote.c2g__CreditNoteDate__c = bInv.blng__InvoiceDate__c;
                creditNote.c2g__DeriveCurrency__c = true;
                creditNote.c2g__DerivePeriod__c = true;
                creditNote.JVCO_VAT_Registration_Number__c = JVCO_FFUtil.getTaxRegistrationNumber(bInv.blng__InvoiceInvoiceLines__r);
                creditNote.JVCO_Sales_Rep__c = bInv.CreatedById;
            }

            creditNote.c2g__DeriveDueDate__c = false;
            cNoteList.add(creditNote);
            bInvCNoteMap.put(bInv.Id, creditNote);
        }

        if (!cNoteList.isEmpty())
        {
            insert cNoteList;
            dim2Map = constructDimension2Map();
            createCreditLineItems(bInvCNoteMap, blngInvoiceMap);
            linkCredit(bInvCNoteMap, blngInvoiceMap);
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Creates Sales Invoice Line Items
        Inputs: List<c2g__codaCreditNote__c>, Map<Id, c2g__codaCreditNote__c>, Map<Id, blng__Invoice__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Mar-2017 ryan.i.r.limlingan  Initial version of function
        16-Sep-2017 franz.g.a.dimaapi   Refactor code - GREEN-21231
    ----------------------------------------------------------------------------------------------- */
    /*public static void createCreditLineItems(Map<Id,c2g__codaCreditNote__c> bInvCNoteMap, Map<Id, blng__Invoice__c> blngInvMap)
    {   
        productNameIdMap = constructProductNameMap();

        bInvToCNoteLineItemMap = new Map<Id, Map<String,c2g__codaCreditNoteLineItem__c>>();
        List<c2g__codaCreditNoteLineItem__c> cNoteLineList = new List<c2g__codaCreditNoteLineItem__c>();
        for(Id bInvId : bInvCNoteMap.keySet())
        {
            blng__Invoice__c bInv = blngInvMap.get(bInvId);
            c2g__codaCreditNote__c cNote = bInvcNoteMap.get(bInvId);
            Decimal taxValue = bInv.JVCO_Migrated__c ? bInv.JVCO_VAT_Amount__c : 0;
            Map<String,c2g__codaCreditNoteLineItem__c> dim2ToLineItemMap = new Map<String,c2g__codaCreditNoteLineItem__c>();

            //If Parent is PPL
            if(bInv.JVCO_PPL_Total_Amount__c != 0)
            {   
                c2g__codaCreditNoteLineItem__c cNoteLineForPPL = new c2g__codaCreditNoteLineItem__c();
                setCreditNoteLineItem(cNoteLineForPPL, cNote.Id, bInv, 'PPL', bInv.JVCO_PPL_Total_Amount__c, bInv.JVCO_PPL_Tax_Total_Amount__c);
                dim2ToLineItemMap.put('PPL', cNoteLineForPPL);
                cNoteLineList.add(cNoteLineForPPL);
                
            }
            //If Parent is PRS
            if(bInv.JVCO_PRS_Total_Amount__c != 0)
            {
                c2g__codaCreditNoteLineItem__c cNoteLineForPRS = new c2g__codaCreditNoteLineItem__c();
                setCreditNoteLineItem(cNoteLineForPRS, cNote.Id, bInv, 'PRS', bInv.JVCO_PRS_Total_Amount__c, bInv.JVCO_PRS_Tax_Total_Amount__c);
                dim2ToLineItemMap.put('PRS', cNoteLineForPRS);
                cNoteLineList.add(cNoteLineForPRS);
            }
            //If Parent is VPL
            if(bInv.JVCO_VPL_Total_Amount__c != 0)
            {
                c2g__codaCreditNoteLineItem__c cNoteLineForVPL = new c2g__codaCreditNoteLineItem__c();
                setCreditNoteLineItem(cNoteLineForVPL, cNote.Id, bInv, 'VPL', bInv.JVCO_VPL_Total_Amount__c, bInv.JVCO_VPL_Tax_Total_Amount__c);
                dim2ToLineItemMap.put('VPL', cNoteLineForVPL);
                cNoteLineList.add(cNoteLineForVPL);
            }

            if(!dim2ToLineItemMap.isEmpty())
            {   
                bInvToCNoteLineItemMap.put(bInv.Id, dim2ToLineItemMap);
            }
        }
        insert cNoteLineList;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Links the Billing Invoice with the Sales Invoice
        Inputs: Map<Id,c2g__codaCreditNote__c>, Map<Id, blng__Invoice__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Mar-2017 ryan.i.r.limlingan  Initial version of function
        16-Sep-2017 franz.g.a.dimaapi   Refactor code - GREEN-21231
        23-Oct-2017 franz.g.a.dimaapi   Refactor code - GREEN-24706
        10-Nov-2017 franz.g.a.dimaapi   Add InvoiceLinking when Order is 1 - GREEN-25727
    ----------------------------------------------------------------------------------------------- */
    /*private static void linkToOriginal(Map<Id,c2g__codaCreditNote__c> bInvCNoteMap, Map<Id, blng__Invoice__c> blngInvMap)
    {
        //Map blng Invoice Id to Original blng Invoice Id
        Map<Id, Id> bInvToOrigBInvMap = new Map<Id, Id>();
        for(blng__Invoice__c bInv : blngInvMap.values())
        {
            if(bInv.blng__Order__r.SBQQ__Quote__r.SBQQ__MasterContract__r != null &&
                bInv.blng__Order__r.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Billing_Invoice__c != null)
            {
                bInvToOrigBInvMap.put(bInv.Id, bInv.blng__Order__r.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Billing_Invoice__c);
            }        
        }
        
        //Map Original blng Invoice Id to Original blng Invoice
        Map<Id, blng__Invoice__c> origBInvMap = null;
        if(!bInvToOrigBInvMap.isEmpty())
        {
            origBInvMap = new Map<Id, blng__Invoice__c>([
                          SELECT Id, JVCO_Sales_Invoice__c 
                          FROM blng__Invoice__c 
                          WHERE Id IN: bInvToOrigBInvMap.values()
                          AND JVCO_Sales_Invoice__c != NULL
                      ]);
        }
        
        //Link Original Invoice to CreditNote's Invoice Field
        List<c2g__codaCreditNote__c> cNoteToUpdateList = new List<c2g__codaCreditNote__c>();
        if(origBInvMap != null)
        {   
            for(Id bId : bInvCNoteMap.keySet())
            {
                blng__Invoice__c bInv = blngInvMap.get(bId);
                
                // 11.07.2017 - GREEN-19751 - Kristoffer Chuck Martin - START
                if(!bInv.JVCO_Original_Invoice_Cancelled__c &&
                    bInvToOrigBInvMap.containsKey(bId))
                {
                    //Get Original Blng Invoice
                    Id origBInvId = bInvToOrigBInvMap.get(bId);
                    blng__Invoice__c origBInv = origBInvMap.get(origBInvId);
                    //Get CreditNote
                    c2g__codaCreditNote__c cNote = bInvCNoteMap.get(bId);

                    if(origBInv != null)
                    {
                        cNote.c2g__Invoice__c = origBInv.JVCO_Sales_Invoice__c;
                        cNoteToUpdateList.add(cNote);
                    }  
                }
            }
        }
        
        if(!cNoteToUpdateList.isEmpty())
        {
            update cNoteToUpdateList;
        }

    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Links the Billing Invoice with the Credit Note
    Inputs: List<blng__Invoice__c>, Map<Id, blng__Invoice__c>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    13-Mar-2017 ryan.i.r.limlingan  Initial version of function
    16-Sep-2017 franz.g.a.dimaapi   Refactor method
  ----------------------------------------------------------------------------------------------- */
    /*private static void linkCredit(Map<Id,c2g__codaCreditNote__c> bInvCNoteMap, 
                                    Map<Id, blng__Invoice__c> blngInvMap)
    {
        System.debug('LINKING');

        Set<blng__Invoice__c> updatedBlngInvSet = new Set<blng__Invoice__c>();
        Set<blng__InvoiceLine__c> updatedBInvLineSet = new Set<blng__InvoiceLine__c>();
        for (Id bId : bInvCNoteMap.keySet())
        {   
            //Get CreditNote
            c2g__codaCreditNote__c cNote = bInvCNoteMap.get(bId);
            //Get Billing Invoice
            blng__Invoice__c bInv = blngInvMap.get(bId);
            //Get Mapping of Line Item Id by Dimension 2
            Map<String, c2g__codaCreditNoteLineItem__c> lineItemByDim2Map = bInvToCNoteLineItemMap.get(bId);
            if(lineItemByDim2Map != null)
            {
                //Link Sales Invoice Line Item into Billing Line
                for(blng__InvoiceLine__c bInvLine : bInv.blng__InvoiceInvoiceLines__r)
                {
                    String dim2 = bInvLine.JVCO_Dimension_2__c;
                    if(dim2 != null && lineItemByDim2Map.containsKey(dim2))
                    {   
                        blng__InvoiceLine__c updatedBInvLine = new blng__InvoiceLine__c();
                        updatedBInvLine.Id = bInvLine.Id;
                        //Populate Line Item Id by Dimension 2
                        updatedBInvLine.JVCO_Sales_Credit_Note_Line_Item__c = lineItemByDim2Map.get(dim2).Id;
                        updatedBInvLineSet.add(updatedBInvLine);   
                    }
                }
            }
            bInv.blng__InvoiceStatus__c = 'Posted';
            bInv.JVCO_Sales_Credit_Note__c = cNote.Id;
            updatedBlngInvSet.add(bInv);            
        }

        Set<Opportunity> updatedOppSet = new Set<Opportunity>();
        if(!oppIdSet.isEmpty())
        {
            for(Id oppId : oppIdSet)
            {
                //Update Invoiced Field on opportunity
                Opportunity o = new Opportunity();
                o.Id = oppId;
                o.JVCO_Invoiced__c = true;
                updatedOppSet.add(o);
            }    
        }  

        if(!updatedBInvLineSet.isEmpty() && !updatedBlngInvSet.isEmpty())
        {
            update new List<blng__InvoiceLine__c>(updatedBInvLineSet);
            update new List<blng__Invoice__c>(updatedBlngInvSet);
            
        }
        if(!updatedOppSet.isEmpty())
        {
            update new List<Opportunity>(updatedOppSet);
        }
    }   

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Generate Company Vat Registration Number
        Inputs: Set<Id>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        31-Oct-2017 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*private static void setCompanyVATRegistrationNum()
    {
        if(userCompany == null)
        {
            //GREEN-18942 - Kristoffer.d.martin - 08/08/2017
            userCompany = [SELECT c2g__Company__c, c2g__Company__r.c2g__VATRegistrationNumber__c, 
                            c2g__User__c 
                            FROM c2g__codaUserCompany__c 
                            WHERE c2g__User__c =: UserInfo.getUserId()
                            LIMIT 1];
        }    
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Initializes a map to be used in getting the Ids of Dimension 1 records
        Inputs: N/A
        Returns: Map<String,Id>
        <Date>      <Authors Name>      <Brief Description of Change> 
        16-Mar-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*private static Map<String,Id> constructDimension2Map()
    {
        Map<String,Id> nameIdMap = new Map<String,Id>();
        for (c2g__codaDimension2__c dim2 : [SELECT Id, Name FROM c2g__codaDimension2__c])
        {
            nameIdMap.put(dim2.Name, dim2.Id);
        }
        return nameIdMap;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Constructs map for Product records to be used in Sales Invoice creation
        Inputs: N/A
        Returns: Map<String,Id>
        <Date>      <Authors Name>      <Brief Description of Change> 
        14-Mar-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*private static Map<String,Id> constructProductNameMap()
    {
        Map<String,Id> productMap = new Map<String,Id>();
        List<String> prodNames = new List<String>{'PPL Product', 'PRS Product', 'VPL Product'};
        for(Product2 p : [SELECT Id, Name FROM Product2 WHERE Name IN :prodNames])
        {
            productMap.put(p.Name.substring(0,3), p.Id);
        }
        return productMap;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Set Credit Note Line Item
        Inputs: c2g__codaCreditNoteLineItem__c, Id, Account, String, Decimal
        Returns: void
        <Date>      <Authors Name>              <Brief Description of Change> 
        16-Sep-2017 franz.g.a.dimaapi           Initial version of function
        15-Dec-2017 franz.g.a.dimaapi           Fixed GREEN-23579
        16-Jan-2018 filip.bezak@accenture.com   GREEN-28418 - Tax Code for Sales Credit Note
        14-Jan-2019 franz.g.a.dimaapi           GREEN-34200 - Removed GREEN-24818 functionality
    ----------------------------------------------------------------------------------------------- */
    /*private static void setCreditNoteLineItem(c2g__codaCreditNoteLineItem__c cNoteLine, Id cNoteId, 
                                              blng__Invoice__c bInv, String type, Decimal amt, Decimal taxValue)
    {
        cNoteLine.c2g__CreditNote__c = cNoteId;
        cNoteLine.c2g__UnitPrice__c = amt.setScale(2)*-1;
        cNoteLine.c2g__Dimension1__c = bInv.blng__Account__r.c2g__CODADimension1__c;
        cNoteLine.c2g__Dimension2__c = dim2Map.get(type);
        cNoteLine.c2g__Dimension3__c = bInv.blng__Account__r.c2g__CODADimension3__c;
        cNoteLine.c2g__Quantity__c = 1;        
        cNoteLine.c2g__TaxValue1__c = taxValue.setScale(2)*-1;
        cNoteLine.c2g__CalculateTaxValueFromRate__c = false;
        cNoteLine.c2g__DeriveLineNumber__c = TRUE;
        cNoteLine.c2g__DeriveUnitPriceFromProduct__c = FALSE;
        cNoteLine.c2g__TaxCode1__c = bInv.blng__Account__r.c2g__CODAOutputVATCode__c;
        cNoteLine.c2g__Product__c = productNameIdMap.get(type);
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Bulk posts the passed Sales Invoice list
        Inputs: List<c2g__codaInvoice__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        16-Mar-2017 ryan.i.r.limlingan  Initial version of function
        03-Aug-2017 franz.g.a.dimaapi   Removed Matching and moved to BackgroundMatchingLogic
    ----------------------------------------------------------------------------------------------- */
    /*private static void postCreditNote(List<c2g__codaCreditNote__c> cNoteList)
    {
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context(); 
        List<c2g.CODAAPICommon.Reference> refList = new List<c2g.CODAAPICommon.Reference>();
        for (c2g__codaCreditNote__c cNote : cNoteList)
        {
            c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = cNote.Id;
            refList.add(reference);
        }
        c2g.CODAAPISalesCreditNote_10_0.BulkPostCreditNote(context, refList);
    }*/
}