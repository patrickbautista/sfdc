@isTest
public class JVCO_CashEntriesPostingLogicBatchTest {
    @testSetup 
    static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        List<JVCO_BackgroundMatching__c> bgMatchingList = new List<JVCO_BackgroundMatching__c>();
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Trading Currency', JVCO_Default_Value__c = 'GBP'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Receivable Control', JVCO_Default_Value__c = '10040'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Output Vat Code', JVCO_Default_Value__c = 'GB-O-STD-LIC'));
        insert bgMatchingList;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
    
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        insert nonConvGLA;
        Test.startTest();
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2();
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim3();
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        JVCO_FFUtil.stopCodaCreditNoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountDLRSHandler = true;
        
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        Test.stopTest();
        
    }
    
    @isTest
    static void testPostCashEntry()
    {   
        Test.startTest();
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Account acc = [SELECT Id FROM Account WHERE RecordTypeId =:recordTypeId LIMIT 1];
        c2g__codaBankAccount__c bAcc = [SELECT Id FROM c2g__codaBankAccount__c order by CreatedDate DESC limit 1];
        c2g__codaAccountingCurrency__c accCurrency = [SELECT Id FROM c2g__codaAccountingCurrency__c LIMIT 1];
        c2g__codaCashEntry__c cEntry = new c2g__codaCashEntry__c();
        cEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cEntry.c2g__BankAccount__c = bAcc.Id;
        cEntry.c2g__CashEntryCurrency__c = accCurrency.Id;
        insert cEntry;
        
        c2g__codaCashEntryLineItem__c cLine = new c2g__codaCashEntryLineItem__c();
        cLine.c2g__Account__c = acc.Id;
        cLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cLine.c2g__CashEntryValue__c = 12000;
        cLine.c2g__CashEntry__c = cEntry.Id;
        insert cLine;
        
        JVCO_General_Settings__c genSet = new JVCO_General_Settings__c();
        genSet.JVCO_Allow_Partial_Payment__c = true ;
        genSet.JVCO_Bank_Statement_Queue_Rec_For_Jobs__c = 1 ;
        genSet.JVCO_Cash_Post_Queue_Rec_Per_Jobs__c = 1 ;
        genSet.JVCO_Company_Name__c = 'Company Name';
        genSet.JVCO_Currency_Code__c = 'GBP';
        genSet.JVCO_Queuable_Max_TransactionLine_Size__c = 1;
        genSet.JVCO_Background_Matching_Size__c = 1;
        insert genSet;
        
        Set<Id> cEntryIdSet = new Set<Id>();
        cEntryIdSet.add(cEntry.Id);
        Test.stopTest();
        Database.executeBatch(new JVCO_CashEntriesPostingLogicBatch(cEntryIdSet));    
    }

    @isTest
    static void testErrorLog()
    {   
        Test.startTest();
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Account acc = [SELECT Id FROM Account WHERE RecordTypeId =:recordTypeId LIMIT 1];
        c2g__codaBankAccount__c bAcc = [SELECT Id FROM c2g__codaBankAccount__c order by CreatedDate DESC limit 1];
        c2g__codaAccountingCurrency__c accCurrency = [SELECT Id FROM c2g__codaAccountingCurrency__c LIMIT 1];
        c2g__codaCashEntry__c cEntry = new c2g__codaCashEntry__c();
        cEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cEntry.c2g__BankAccount__c = bAcc.Id;
        cEntry.c2g__CashEntryCurrency__c = accCurrency.Id;
        insert cEntry;
        
        JVCO_ErrorUtil errorUtil = new JVCO_ErrorUtil();
        errorUtil.logErrorFromCashEntry(new List<c2g__codaCashEntry__c>{cEntry}, 'Posting of Cash Entry', 'burger', 'tocino');
        errorUtil.logErrorFromOrderGroup(new List<JVCO_Order_Group__c>(), 'Invoice Scheduler', 'burger', 'tocino');
        JVCO_ErrorUtil.logError(cEntry.Id, 'hotdog', 'burger', 'tocino');
        Test.stopTest(); 
    }
    
    @isTest
    static void testBackgroundMatchingScheduler()
    {
        JVCO_General_Settings__c genSet = new JVCO_General_Settings__c();
        genSet.JVCO_Background_Matching_Size__c = 1;
        insert genSet;
        JVCO_PaymentCollectionScheduler.callExecuteMomentarily('hotdog');
    }
    @isTest
    static void testOtherMethod()
    {
        new JVCO_CashEntriesPostingLogicBatch(new Set<Id>(), true);
        new JVCO_CashEntriesPostingLogicBatch(true, new Set<Id>());
    }
}