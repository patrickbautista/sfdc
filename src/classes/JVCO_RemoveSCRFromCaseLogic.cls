/* -----------------------------------------------------------------------------------------------
   Name: JVCO_RemoveSCRFromCaseLogic.cls 
   Description: Business logic class for removing Sales Credits from Case

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   --------------------------------------------------
   12-Feb-2021   0.1         Luke.Walker      	Intial creation
   18-Feb-2021	 o.2		 Luke.Walker		GREEN-36320 - Added update to Case to recalculate amounts
   ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_RemoveSCRFromCaseLogic 
{
  	private ApexPages.StandardSetController stdCtrl;
    private List<c2g__codaCreditNote__c> selectedCredits;
    private id caseID;
    public Integer numOfSelectedCredits {get; set;}
    public final String NO_RECORDS_ERR = System.Label.JVCO_NoSalesCreditNote;
    

    //constructor
    public JVCO_RemoveSCRFromCaseLogic(ApexPages.StandardSetController ssc){
       stdCtrl = ssc;
       selectedCredits = (List<c2g__codaCreditNote__c>)ssc.getSelected();
       numOfSelectedCredits = selectedCredits.size();
       caseID = ApexPages.currentPage().getParameters().get('id');
    }
    
    public PageReference remove()
    {  
        List<c2g__codaCreditNote__c> CreditList = new List<c2g__codaCreditNote__c>();
        Case caseRecord;
        
        if(numOfSelectedCredits > 0){
            CreditList = [SELECT ID, JVCO_Case_Id__c FROM c2g__codaCreditNote__c WHERE ID IN :selectedCredits];
            caseRecord = [SELECT ID FROM Case WHERE ID = :caseID];

            List<c2g__codaCreditNote__c> updateCreditList = new List<c2g__codaCreditNote__c>();
            
            for(c2g__codaCreditNote__c c: CreditList){
                c.JVCO_Case_Id__c = NULL;
                c.JVCO_Write_Off_Status__c = NULL;
                updateCreditList.add(c);
            }
            
            if(updateCreditList.size() > 0){
                update updateCreditList;
                update caseRecord;
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, NO_RECORDS_ERR));
            return null;
        }
        
        String uiThemeDisplayed = UserInfo.getUiThemeDisplayed();
        PageReference pageRef = new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
        
        //Create URL for Lightning or Classic
        if(uiThemeDisplayed == 'Theme4d' || uiThemeDisplayed == 'Theme4u'){
            aura.redirect(pageRef);
      		return pageRef;
        }
        pageRef.setRedirect(true);    
        return pageRef;
    }
    
    public PageReference returnToCase()
    {
        PageReference page = new PageReference(stdCtrl.cancel().getUrl());
        page.setRedirect(TRUE);
        return page;
    }
    
}