/* ----------------------------------------------------------------------------------------------
Author: kristoffer.d.martin
Company: Accenture
Description: Test Class for JVCO_LinkBankAccounts
<Date>      <Authors Name>       <Brief Description of Change> 
12-Apr-2017 kristoffer.d.martin  Initial version
----------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_LinkBankAccounts_Test {
	
	/* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Sets up the needed records for testing
    Inputs: N/
    Returns: voidA
    <Date>      <Authors Name>       <Brief Description of Change> 
    11-Apr-2017 kristoffer.d.martin  Initial version of function
	----------------------------------------------------------------------------------------------- */
 	@testSetup static void createTestData() 
    {
		Group testGroup = new Group(Name='test group', Type='Queue');
		insert testGroup;
		QueuesObject testQueue ; 
		System.runAs(new User(Id=UserInfo.getUserId()))
		{
			List<queuesobject >  listQueue = new List<queuesobject >();
		   	queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
			listQueue.add(q1);
			queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
			listQueue.add(q2);
			queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
			listQueue.add(q3);
			queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
			listQueue.add(q4);
			queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
			listQueue.add(q5);
			queuesobject q6 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankAccount__c'); 
			listQueue.add(q6);
			queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
			listQueue.add(q7);
			queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatement__c'); 
			listQueue.add(q8);			
			queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntry__c'); 
			listQueue.add(q9);	
			queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='PAYBASE2__Payment__c'); 
			listQueue.add(q10);
			insert listQueue;

			GroupMember GroupMemberObj = new GroupMember();
			GroupMemberObj.GroupId = testGroup.id;
			GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
			insert GroupMemberObj;
		}

		c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
		testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
		testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
		testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
		testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
		testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
		testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
		testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
		testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
		testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
		testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
		testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
		testGeneralLedgerAcc.Dimension_1_Required__c = false;
		testGeneralLedgerAcc.Dimension_2_Required__c = false;
		testGeneralLedgerAcc.Dimension_3_Required__c = false;
		testGeneralLedgerAcc.Dimension_4_Required__c = false;
		testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
		testGeneralLedgerAcc.ownerid = testGroup.Id;

        insert testGeneralLedgerAcc;
        
        
        c2g__codaGeneralLedgerAccount__c accRecvblControlGLA = new c2g__codaGeneralLedgerAccount__c();
        accRecvblControlGLA.c2g__AdjustOperatingActivities__c = false;
        accRecvblControlGLA.c2g__AllowRevaluation__c = false;
        accRecvblControlGLA.c2g__CashFlowCategory__c = 'Operating Activities';
        accRecvblControlGLA.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        accRecvblControlGLA.c2g__GLAGroup__c = 'Accounts Receivable';
        accRecvblControlGLA.c2g__ReportingCode__c = '10040';
        accRecvblControlGLA.c2g__TrialBalance1__c = 'Assets';
        accRecvblControlGLA.c2g__TrialBalance2__c = 'Current Assets';
        accRecvblControlGLA.c2g__TrialBalance3__c = 'Accounts Receivables';
        accRecvblControlGLA.c2g__Type__c = 'Balance Sheet';
        accRecvblControlGLA.Name = '10040 - Accounts Receivable';
        accRecvblControlGLA.ownerid = testGroup.Id;
        insert accRecvblControlGLA;

       
		//Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
		
		//Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }
		
        c2g__codaTaxCode__c taxCode = JVCO_TestClassObjectBuilder.createTaxCode(accRecvblControlGLA.id);
        insert taxCode;


		//Create Account
        Account testAccCust = new Account(Name='Test Customer Account');
        testAccCust.AccountNumber = '999999';
        testAccCust.c2g__CODAAccountsReceivableControl__c = accRecvblControlGLA.id;
        testAccCust.Type='Customer';
        testAccCust.RecordTypeId = accountRecordTypes.get('Customer Account');
        insert testAccCust;
        
        Account testAcc = JVCO_TestClassObjectBuilder.createLicenceAccount(testAccCust.id);
        testAcc.AccountNumber = '987654';
        testAcc.c2g__CODAAccountsReceivableControl__c = accRecvblControlGLA.id;
        testAcc.c2g__CODAAllowDeleteInUse__c = false;
        testAcc.c2g__CODACreditLimitEnabled__c = false;
        testAcc.c2g__CODACreditStatus__c = 'Credit Allowed';
        testAcc.c2g__CODADaysOffset1__c = 0.0;
        testAcc.c2g__CODADiscount1__c = 0.0;
        testAcc.c2g__CODAFederallyReportable1099__c = false;
        testAcc.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testAcc.c2g__CODAIntercompanyAccount__c = false;
        testAcc.c2g__CODASalesTaxStatus__c = 'Taxable';
        insert testAcc;

        
        c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
        testCompany.c2g__CashMatchingCurrencyMode__c = 'Account';
        testCompany.c2g__Country__c = 'GB';
        testCompany.c2g__ECCountryCode__c = 'GB';
        testCompany.c2g__UnitOfWork__c = 1.0;
        testCompany.Name = 'Test Company';
        testCompany.ownerid = testGroup.Id;

        insert testCompany;

		c2g__codaAccountingCurrency__c accCurrency = new c2g__codaAccountingCurrency__c();
		accCurrency.c2g__OwnerCompany__c = testCompany.id;
		accCurrency.c2g__DecimalPlaces__c = 2;
		accCurrency.Name = 'GBP';
		accCurrency.c2g__Dual__c = true ;
		accCurrency.ownerid = testGroup.Id;
		accCurrency.c2g__Home__c = true;
		accCurrency.c2g__UnitOfWork__c = 2.0;
		insert accCurrency;

        c2g__codaBankAccount__c testBankAccount = new c2g__codaBankAccount__c();
        testBankAccount.c2g__AccountName__c = 'Test Bank Account';
        testBankAccount.c2g__AccountNumber__c = '987654';
        testBankAccount.c2g__BankName__c = 'Barclays Bank - Current';
        testBankAccount.c2g__GeneralLedgerAccount__c = testGeneralLedgerAcc.id;
        testBankAccount.Name = 'Test Bank Account 1';
        testBankAccount.c2g__OwnerCompany__c = testCompany.Id;
        testBankAccount.c2g__ReportingCode__c = 'BARC-CURR-JV';
        testBankAccount.c2g__BankAccountCurrency__c = accCurrency.id;
        insert testBankAccount;

        testCompany.c2g__BankAccount__c = testBankAccount.id;
        update testCompany;

		c2g__codaYear__c yr= new c2g__codaYear__c();
		yr.Name ='2016';
		yr.c2g__AutomaticPeriodList__c =  true;
		yr.c2g__OwnerCompany__c = testCompany.id;
		yr.c2g__ExternalId__c = 'yzsd1234';
		yr.c2g__NumberOfPeriods__c =11;
		yr.c2g__StartDate__c =  system.today() - 10;
		yr.c2g__Status__c = 'Open';
		yr.c2g__PeriodCalculationBasis__c = '445';
		yr.c2g__YearEndMode__c = 'Full Accounting Code' ; 
		yr.c2g__UnitOfWork__c = 12;
		yr.ownerid = testGroup.Id;
		insert yr;

        c2g__codaPeriod__c testPeriod = new c2g__codaPeriod__c();
        testPeriod.c2g__OwnerCompany__c = testCompany.id;
        testPeriod.c2g__StartDate__c = System.today()-10;
        testPeriod.c2g__EndDate__c = System.today()+10;
        testPeriod.c2g__PeriodNumber__c ='123';
		testPeriod.c2g__Description__c ='test Desc';
		testPeriod.c2g__PeriodGroup__c = 'Q1';
		testPeriod.c2g__PeriodNumber__c = '1';
		testPeriod.c2g__YearName__c = yr.id;
        insert testPeriod;

		c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c();
		userCompany.c2g__Company__c = testCompany.id;
		userCompany.c2g__User__c = userInfo.getUserId();
		userCompany.c2g__ExternalId__c = 'ABCDE1234567876';
		userCompany.c2g__UnitOfWork__c = 111 ;
		insert userCompany;

        //Create Sales Invoice
        c2g__codaInvoice__c testInvoice = new c2g__codaInvoice__c();
        testInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testInvoice.c2g__InvoiceDate__c = date.today().addDays(-7);
        testInvoice.c2g__DueDate__c = date.today().addDays(-7);
        testInvoice.c2g__Account__c = testAcc.Id;
        testInvoice.JVCO_Customer_Type__c = 'New Business';
        testInvoice.c2g__OwnerCompany__c = testCompany.id;
        testInvoice.ownerid = testGroup.Id;
        testInvoice.JVCO_Invoice_Legacy_Number__c = 'SIN00001';
        insert testInvoice;

        //Create Cash Entry
        c2g__codaCashEntry__c testCashEntry = new c2g__codaCashEntry__c();
        testCashEntry.c2g__PaymentMethod__c = 'Electronic';
        testCashEntry.c2g__OwnerCompany__c = testCompany.id;
        testCashEntry.ownerid = testGroup.Id;
        testCashEntry.c2g__Status__c = 'In Progress';
        testCashEntry.c2g__Date__c = Date.today();
        testCashEntry.c2g__Value__c = 0.00;
        insert testCashEntry;

        List<c2g__codaInvoice__c> salesInvoiceList = [SELECT Id, Name, c2g__Transaction__c from c2g__codaInvoice__c];

        //Create Cash Entry Line Item
        c2g__codaCashEntryLineItem__c cashEntryLineItem1 = new c2g__codaCashEntryLineItem__c();
        cashEntryLineItem1.c2g__CashEntry__c = testCashEntry.id;
        cashEntryLineItem1.c2g__Account__c = testAccCust.id;
        cashEntryLineItem1.c2g__CashEntryValue__c = 15000;
        cashEntryLineItem1.c2g__LineNumber__c = 1;
        cashEntryLineItem1.c2g__AccountReference__c = salesInvoiceList[0].Name;
        cashEntryLineItem1.c2g__AccountPaymentMethod__c = 'Electronic';
        cashEntryLineItem1.c2g__OwnerCompany__c = testCompany.id;
        cashEntryLineItem1.JVCO_Temp_AccountReference__c = 'SIN00001';
        insert cashEntryLineItem1;

        PAYACCVAL1__Bank_Account__c payBankAccount = new PAYACCVAL1__Bank_Account__c();
        payBankAccount.PAYACCVAL1__Bank_Name__c = 'test pay bank';
        payBankAccount.PAYFISH3__Originator__c = false;
        payBankAccount.PAYACCVAL1__Account_Number__c = '12345678';
        payBankAccount.PAYACCVAL1__Sort_Code__c = '123456';
        payBankAccount.PAYFISH3__Account_Name__c = 'test account name';
        insert payBankAccount;

        PAYREC2__Payment_Schedule__c paySchedule = new PAYREC2__Payment_Schedule__c();
        paySchedule.PAYFISH3__Bank_Account__c = payBankAccount.id;
        paySchedule.PAYREC2__Day__c = '1';
        paySchedule.PAYFISH3__First_Collection_Interval__c = 1;
        paySchedule.PAYREC2__Frequency__c = 'month';
        paySchedule.PAYFISH3__FTA_Sub_Type__c = 'Default';
        paySchedule.PAYFISH3__Fund_Transfer_Agent__c = 'Unknown';
        paySchedule.PAYREC2__Interval__c = 1;
        paySchedule.PAYREC2__Max__c = 10;
        paySchedule.PAYREC2__Type__c = 'Fixed';
        insert paySchedule;

        PAYREC2__Payment_Agreement__c paymentAgreement = new PAYREC2__Payment_Agreement__c();
        paymentAgreement.PAYREC2__Account__c = testAcc.Id;
        paymentAgreement.PAYFISH3__Current_Bank_Account__c = payBankAccount.id;
        paymentAgreement.PAYREC2__Ongoing_Collection_Amount__c = 100;
        paymentAgreement.PAYFISH3__Payee_Agreed__c = true;
        paymentAgreement.PAYREC2__Payment_Schedule__c = paySchedule.Id;
        insert paymentAgreement;

		//Get record type of Paymnet
        List<RecordType> rtypes1 = [SELECT Name, Id FROM RecordType WHERE sObjectType='PAYBASE2__Payment__c' AND isActive=true];
		
		//Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> paymentRecordTypes = new Map<String,String>{};
        for(RecordType rt1: rtypes1)
        {
          paymentRecordTypes.put(rt1.Name,rt1.Id);
        }

        PAYBASE2__Payment__c paybasePayment = new PAYBASE2__Payment__c();
        paybasePayment.PAYREC2__Payment_Agreement__c = paymentAgreement.id;
        paybasePayment.PAYFISH3__Type__c = 'Collection';
        paybasePayment.RecordTypeId = paymentRecordTypes.get('Direct Debit Collection');
        paybasePayment.Name = 'Test Payment 01';
        paybasePayment.Ownerid = testGroup.Id;
        paybasePayment.PAYBASE2__Is_Paid__c = true;
        //paybasePayment.JVCO_Process__c = false;
        //paybasePayment.PAYBASE2__Balance__c = 100;
        paybasePayment.PAYBASE2__Amount_Refunded__c = 100;
        paybasePayment.PAYCP2__Is_Test__c = true;
        paybasePayment.PAYREC2__Is_Actual_Collection__c = true;
        paybasePayment.PAYSPCP1__Redirect_Parent__c= true;
        paybasePayment.PAYSPCP1__Gift_Aid__c= true;
	    paybasePayment.PAYBASE2__Amount__c = 1250.00;
	    paybasePayment.PAYCP2__Payment_Description__c = 'GIN-000044';
	    paybasePayment.PAYBASE2__Status__c = 'Pending';
        insert paybasePayment;

        //michael.alamag 11/08/2017
        c2g__codaCashMatchingSettings__c CMSettings = new c2g__codaCashMatchingSettings__c();
        CMSettings.JVCO_Write_off_Limit__c = 1.0;
        insert CMSettings;

    }

	/* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Call the JVCO_LinkBankAccounts Batch
    Inputs: N/
    Returns: voidA
    <Date>      <Authors Name>       <Brief Description of Change> 
    11-Apr-2017 kristoffer.d.martin  Initial version of function
	----------------------------------------------------------------------------------------------- */
	@isTest static void JVCO_LinkBankAccounts_test()
    {   

		Test.startTest();

		JVCO_LinkBankAccounts linkBankAccounts = new JVCO_LinkBankAccounts();
		Database.executeBatch(linkBankAccounts);

		Test.stopTest();

        PAYBASE2__Payment__c paymentRec = [SELECT Id, Name, PAYFISH3__Payee_Bank_Account__c
        												   FROM PAYBASE2__Payment__c
        												   order by CreatedDate DESC limit 1];

        PAYREC2__Payment_Agreement__c paymentAgreementAccount = [SELECT Id, Name, PAYFISH3__Current_Bank_Account__c
						        							     FROM PAYREC2__Payment_Agreement__c 
						        							     order by CreatedDate DESC limit 1];

        System.assertEquals(paymentRec.PAYFISH3__Payee_Bank_Account__c, paymentAgreementAccount.PAYFISH3__Current_Bank_Account__c);
    }
}