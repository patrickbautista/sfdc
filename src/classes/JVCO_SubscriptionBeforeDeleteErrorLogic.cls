/* ----------------------------------------------------------------------------------------------
   Name: JVCO_ValidateCreditNoteLogic.cls 
   Description: Added an error message VF page when deleting a Subscription

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  16-Oct-2017   0.1         jasper.j.figueroa  Intial creation - fix for GREEN-22873
  ----------------------------------------------------------------------------------------------- */
public class JVCO_SubscriptionBeforeDeleteErrorLogic
{
    private SBQQ__Subscription__c stdCtrl {get; set;}

    // Constants used for page messages
    public String INVALID_INVOICE_ERR1 = 'You cannot delete a Subscription';
	
    public JVCO_SubscriptionBeforeDeleteErrorLogic(ApexPages.StandardController ssc)
    {
        stdCtrl = (SBQQ__Subscription__c)ssc.getRecord();
    }

    public PageReference init()
    {
    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, INVALID_INVOICE_ERR1));
    	return null;
	}

    //Returns to the Sales Invoice after clicking the 'Back' button
    public PageReference returnToPrevious()
    {
        PageReference page = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        page.setRedirect(TRUE);
        return page;
    }
}