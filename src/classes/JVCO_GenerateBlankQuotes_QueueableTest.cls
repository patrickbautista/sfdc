/* ----------------------------------------------------------------------------------------------
    Name: JVCO_GenerateBlankQuotes_QueueableTest
    Description: Test Class for JVCO_GenerateBlankQuotes_Queueable

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    08-Aug-2018     0.1         Jules Pablo         Intial creation
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_GenerateBlankQuotes_QueueableTest
{
	@testSetup static void setupTestData() 
    {
        JVCO_TestClassObjectBuilder.createBillingConfig();
        
        Product2 prod1 = new Product2();
        prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 83202-C';
        a1.Type = 'Key Account';

        c2g__codaDimension1__c dim = new c2g__codaDimension1__c();
        dim.c2g__ReportingCode__c = 'BC008TEST';
        dim.Name = 'General Purpose';
        insert dim;

        a1.c2g__CODADimension1__c = dim.Id;

        insert a1;
        
        Account a2 = new Account();
        a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.Id);
        insert a2;

        Pricebook2 testPB2 = new Pricebook2();
        testPB2.name = 'Standard Price Book';
        testPB2.IsActive = true;
        insert testPB2;
        
        PriceBookEntry pbe = new PriceBookEntry();
        pbe= JVCO_TestClassObjectBuilder.createPriceBookEntry(prod1.Id);
        insert pbe;

        JVCO_Venue__c nvenue2 = new JVCO_Venue__c();
        nvenue2.Name = 'Sample Venue';
        nvenue2.JVCO_Email__c = 'JVCO_QuoteTriggerHandlerTest@email.com';
        nvenue2.External_Id__c = 'JVCO_QTHTestsampleaccexid';
        nvenue2.JVCO_Field_Visit_Requested__c  = true;
        nvenue2.JVCO_Field_Visit_Requested_Date__c = date.today();
        nvenue2.JVCO_Postcode__c = 'BL2 1AA';
        insert nvenue2;

        JVCO_Affiliation__c  affilRecord2 = new JVCO_Affiliation__c();
        affilRecord2.JVCO_Account__c = a2.Id;
        affilRecord2.JVCO_Venue__c = nvenue2.id;   
        affilRecord2.JVCO_Start_Date__c = system.today().addMonths(3);
        affilRecord2.JVCO_Omit_Quote_Line_Group__c = FALSE;
        insert affilRecord2;
      
    }

    private static testMethod void testMethodGenerateBlankQuotesExecute() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, Licence_Account_Number__c from Account where RecordTypeId =: licenceRT limit 1];

        Test.startTest();
        System.enqueueJob(new JVCO_GenerateBlankQuotes_Queueable(null, accountRecord, '', 0, 0));
        Test.stopTest();
    }

    private static testMethod void testMethodGenerateBlankQuotesExecuteEndDated() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, Licence_Account_Number__c from Account where RecordTypeId =: licenceRT limit 1];

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 83202';
        o.AccountId = accountRecord.Id;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;

        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Quote';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = accountRecord.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);   
        insert q;

        Contract contr = new Contract();
        contr.AccountId = accountRecord.Id;
        contr.Status = 'Draft';
        contr.StartDate = Date.today();
        contr.ContractTerm = 12;
        contr.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
        contr.SBQQ__RenewalQuoted__c = FALSE;
        contr.SBQQ__Quote__c = q.id;
        contr.SBQQ__Opportunity__c = o.id;
        contr.SBQQ__RenewalQuoted__c = false;
        contr.JVCO_Renewal_Generated__c = false;
        contr.JVCO_RenewableQuantity__c = 1;
        //contr.JVCO_Current_Period__c = true;
        contr.JVCO_Value__c = 1000;
        insert contr;

        q.SBQQ__MasterContract__c = contr.Id;
        update q;

        Set<Id> contractId = new Set<Id>();
        contractId.add(contr.Id);

        Test.startTest();
        System.enqueueJob(new JVCO_GenerateBlankQuotes_Queueable(contractId, accountRecord, '', 0, 0));
        Test.stopTest();
    }

    private static testMethod void testMethodGenerateBlankQuotesReExecute() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, Licence_Account_Number__c from Account where RecordTypeId =: licenceRT limit 1];
        Map<Id, JVCO_Affiliation__c> affilationsForCreation = new Map<Id, JVCO_Affiliation__c>([select Id, Name, JVCO_Venue__r.Name, JVCO_Account__c, JVCO_Venue__c, JVCO_Start_Date__c, JVCO_End_Date__c, JVCO_Omit_Quote_Line_Group__c from JVCO_Affiliation__c where JVCO_Omit_Quote_Line_Group__c = FALSE and JVCO_Account__c = :accountRecord.Id and JVCO_End_Date__c = null AND Id NOT in (Select Affiliation__c from SBQQ__Subscription__c where SBQQ__Contract__r.JVCO_Renewal_Generated__c = false and SBQQ__Contract__r.AccountId = :accountRecord.Id) AND Id NOT in (Select JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c where SBQQ__Quote__r.SBQQ__Status__c = 'Draft' and SBQQ__Quote__r.SBQQ__Account__c = :accountRecord.Id)]);

        Test.startTest();
        System.enqueueJob(new JVCO_GenerateBlankQuotes_Queueable(affilationsForCreation, System.today(), accountRecord, '', 1, 0, 1));
        Test.stopTest();
    }
}