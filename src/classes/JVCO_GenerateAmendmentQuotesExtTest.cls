@IsTest
public class JVCO_GenerateAmendmentQuotesExtTest {
    
@testSetup static void setupTestData() 
    {
        Product2 prod1 = new Product2();
        prod1.Name = 'Test Product 18023-1';
        prod1.ProductCode = 'TP 18023-1';
        prod1.IsActive = true;
        prod1.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        prod1.SBQQ__SubscriptionTerm__c = 12;
        insert prod1;

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        JVCO_Venue__c v1 = JVCO_TestClassObjectBuilder.createVenue();
        v1.Name = 'Some Venue for Testing 1';
        insert v1;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 18023-C';
        a1.Type = 'Key Account';
        insert a1;
        
        Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
        a2.RecordTypeId = licenceRT;
        a2.Name = 'Test Account 18023-L';
        a2.JVCO_Customer_Account__c = a1.Id;
        
        insert a2;
        Test.startTest();
        JVCO_Affiliation__c aff1 = new JVCO_Affiliation__c();
        aff1.Name = 'Some Aff 1';
        aff1.JVCO_Account__c = a2.Id;
        aff1.JVCO_Venue__c = v1.Id;
        aff1.JVCO_Start_Date__c = System.today() - 10;
        aff1.JVCO_Status__c = 'Trading';
        aff1.JVCO_Venue_Name__c = 'Some Venue for Testing 1';
        insert aff1;

        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 18023';
        o.AccountId = a2.Id;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Quote';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__SubscriptionTerm__c = 12;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.Start_Date__c = System.today() - 5;
        q.End_Date__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        q.SBQQ__SubscriptionTerm__c = 12;
        insert q;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        insert ql;
        
        /*Test.startTest();
        q.JVCO_QuoteComplete__c = true;
        q.SBQQ__Status__c = 'Accepted';
        update q;
        
        o.StageName = 'Closed Won';
        o.SBQQ__Contracted__c = true;
        o.Probability = 100;
        update o;
        Test.stopTest();*/
        
        Contract c1 = new Contract();
        c1.SBQQ__RenewalQuoted__c = false;  
        c1.AccountID = a2.Id;
        c1.StartDate = System.today() - 5;
        c1.ContractTerm = 12;
        //c1.EndDate = System.today().addMonths(12);
        c1.Name = 'Test Contract 18023-L';
        c1.SBQQ__Quote__c = q.Id;
        c1.SBQQ__Opportunity__c = o.Id;
        c1.AccountId = a2.Id;
        insert c1;
        Test.stopTest();
    }
    
    static testMethod void testForGenerateAmendmentQuotes ()
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        JVCO_GenerateAmendmentQuotesExtension ctrl = new JVCO_GenerateAmendmentQuotesExtension(sc);
        
        Test.startTest();
        ctrl.queueGenerateAmendmentQuotes();
        ctrl.jobCheck();
        ctrl.backToRecord();
        Test.stopTest();
    }
    
    static testMethod void testForGenerateAmendmentQuotes2 ()
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        asCS.JVCO_Contract_Batch_Size__c = 2;
        upsert asCS JVCO_Array_Size__c.Id;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        JVCO_GenerateAmendmentQuotesExtension ctrl = new JVCO_GenerateAmendmentQuotesExtension(sc);
        
        Test.startTest();
        ctrl.queueGenerateAmendmentQuotes();
        ctrl.backToRecord();
        Test.stopTest();
    }
}