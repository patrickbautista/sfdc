/* ----------------------------------------------------------------------------------------------
    Name: JVCO_ContractBeforeDeleteErrorLogicTest.cls 
    Description: Test the JVCO_ContractBeforeDeleteErrorLogic class

    Date         Version     Author              	Summary of Changes 
    -----------  -------     -----------------   	-----------------------------------------
	26-Feb-2021	  0.1		  luke.walker		 	GREEN-36357 - initial creation
  ----------------------------------------------------------------------------------------------- */

@isTest
public class JVCO_ContractBeforeDeleteErrorLogicTest {
    
	@testSetup
    private static void testSetup(){
		Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        Account cusAcct = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert cusAcct;
        
        Account Licacct = JVCO_TestClassObjectBuilder.createLicenceAccount(cusAcct.id);
        insert licAcct;
        
        SBQQ__Quote__c quote = JVCO_TestClassObjectBuilder.createQuote(licAcct.id, qProcess.id, null, null,null);
        insert quote;

        Contract contract = JVCO_TestClassObjectBuilder.createContract(licAcct.id, quote.id);
        insert contract;
       
    }
    
    @isTest
    private static void deleteContract(){
       Contract con = [SELECT ID FROM Contract LIMIT 1];
        Profile adminProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u = new User(Alias = 'admin', 
                          Email='admin@pplprs.co.uk',
        				  EmailEncodingKey='UTF-8',
                          LastName='admin', 
                          LanguageLocaleKey='en_US',
        				  LocaleSidKey='en_US', 
                          ProfileId = adminProfile.Id,
                          Department = 'Technology',
                          Division = 'Technology',
        				  TimeZoneSidKey='Europe/London',
         				  UserName='adminuser' + DateTime.now().getTime() + '@pplprs.co.uk.test');
       insert u;
       system.assertNotEquals(null,con);
       
       test.startTest();        
        	System.runAs(u) {
                Apexpages.currentpage().getparameters().put('id', con.Id); 
                ApexPages.standardController sc = new ApexPages.standardController(con);
                JVCO_ContractBeforeDeleteErrorLogic errorLogic = new JVCO_ContractBeforeDeleteErrorLogic(sc);
                errorLogic.init();
            }
       test.stopTest();
        
       List<Contract> conResult = [SELECT ID FROM Contract WHERE id = :con.id];
       system.assertEquals(0, conResult.size());
    }
    
    @isTest
    private static void deleteContractLEX(){
        Contract con = [SELECT ID FROM Contract LIMIT 1];
        Profile adminProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u = new User(Alias = 'admin', 
                          Email='admin@pplprs.co.uk',
        				  EmailEncodingKey='UTF-8',
                          LastName='admin', 
                          LanguageLocaleKey='en_US',
        				  LocaleSidKey='en_US', 
                          ProfileId = adminProfile.Id,
                          Department = 'Technology',
                          Division = 'Technology',
        				  TimeZoneSidKey='Europe/London',
         				  UserName='adminuser' + DateTime.now().getTime() + '@pplprs.co.uk.test');
       insert u;
       system.assertNotEquals(null,con);
       
       test.startTest();        
        	System.runAs(u) {
                Apexpages.currentpage().getparameters().put('id', con.Id); 
                ApexPages.standardController sc = new ApexPages.standardController(con);
                JVCO_ContractBeforeDeleteErrorLogic errorLogic = new JVCO_ContractBeforeDeleteErrorLogic(sc);
                errorLogic.lightningUI = true;
                errorLogic.init();
            }
       test.stopTest();
        
       List<Contract> conResult = [SELECT ID FROM Contract WHERE id = :con.id];
       system.assertEquals(0, conResult.size());
    }
    
    @isTest
    private static void cannotDeleteContract(){
		Contract con = [SELECT ID FROM Contract LIMIT 1];
    	Profile licensingProfile = [SELECT Id FROM Profile WHERE Name='Licensing User'];
    	User u = new User(Alias = 'test', 
                          		 Email='test@pplprs.co.uk',
        				  		 EmailEncodingKey='UTF-8',
                          		 LastName='Test', 
                        		 LanguageLocaleKey='en_US',
        						 LocaleSidKey='en_US', 
                    		     ProfileId = licensingProfile.Id,
                     		     Department = 'Technology',
                   		         Division = 'Technology',
        						 TimeZoneSidKey='Europe/London',
         						 UserName='licenseuser' + DateTime.now().getTime() + '@pplprs.co.uk.test');
      insert u;
        
      system.assertNotEquals(null,con); 
        
      test.startTest();        
        	System.runAs(u) {
                Apexpages.currentpage().getparameters().put('id', con.Id); 
                ApexPages.standardController sc = new ApexPages.standardController(con);
                JVCO_ContractBeforeDeleteErrorLogic errorLogic = new JVCO_ContractBeforeDeleteErrorLogic(sc);
                errorLogic.init();
            }
      test.stopTest();
        
       List<Contract> conResult = [SELECT ID FROM Contract WHERE id = :con.id];
       system.assertNotEquals(0, conResult.size());
    }
    
    @isTest
    private static void returnToContract(){
       Contract con = [SELECT ID FROM Contract LIMIT 1];
       system.assertNotEquals(null,con);
       
    	test.startTest();        
        	Apexpages.currentpage().getparameters().put('id', con.Id); 
            ApexPages.standardController sc = new ApexPages.standardController(con);
            JVCO_ContractBeforeDeleteErrorLogic errorLogic = new JVCO_ContractBeforeDeleteErrorLogic(sc);
            errorLogic.returnToContract();
        test.stopTest();
        
        List<Contract> conResult = [SELECT ID FROM Contract WHERE id = :con.id];
        system.assertNotEquals(0, conResult.size()); 
    }
    
    @isTest
    private static void returnToContractLEX(){
    Contract con = [SELECT ID FROM Contract LIMIT 1];
    system.assertNotEquals(null,con);
       
    test.startTest();        
    	Apexpages.currentpage().getparameters().put('id', con.Id); 
    	ApexPages.standardController sc = new ApexPages.standardController(con);
    	JVCO_ContractBeforeDeleteErrorLogic errorLogic = new JVCO_ContractBeforeDeleteErrorLogic(sc);
        errorLogic.lightningUI = true;
    	errorLogic.returnToContract();
    test.stopTest();
        
    List<Contract> conResult = [SELECT ID FROM Contract WHERE id = :con.id];
    system.assertNotEquals(0, conResult.size());
        
    }
}