/* ----------------------------------------------------------------------------------------------
    Name: JVCO_ContractReviewQuotesBatchTest
    Description: Test Class for JVCO_ContractReviewQuotesBatch

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    29-Mar-2017     0.1         Jules Pablo         Intial creation
----------------------------------------------------------------------------------------------- */
@IsTest
private class JVCO_ContractReviewQuotesBatchTest 
{
    @testSetup static void setupTestData() 
    {
        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Product2 prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        Account a1 = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert a1;

        Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
        insert a2;
        
        Opportunity o = JVCO_TestClassObjectBuilder.createOpportunity(a2);
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;
        
        SBQQ__QuoteProcess__c qpID = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qpId;

        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Amendment';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        q.SBQQ__QuoteProcessId__c = qpId.Id;
        insert q;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        qL.SPVAvailability__c = true;
        insert ql;
        
        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();           
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = JVCO_TestClassObjectBuilder.createAffiliation(a2.id, venueRecord.id);    
        Insert  affilRecord;   

        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;      

        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'ErrorNoSPV', Value__c = 'You can not contract the Quote since there are Quote Line(s) w/o SPV'));
        insert settings;

    }
    
    private static testMethod void testMethodContactReviewQuotesBatch() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_ContractReviewQuotesBatch(a, 1), 1);
        Test.stopTest();
    }

    private static testMethod void testMethodErrorSPV() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        SBQQ__Quote__c q = [select Id, SBQQ__Account__c from SBQQ__Quote__c where SBQQ__Account__c =: a.ID];
        SBQQ__QuoteLine__c qL = [select Id,SPVAvailability__c,ParentProduct__c from SBQQ__QuoteLine__c where SBQQ__Quote__c =: q.ID];
        qL.SPVAvailability__c = false;
        qL.ParentProduct__c = false;

        update qL;
        
        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_ContractReviewQuotesBatch(a, 1), 1);
        Test.stopTest();
    }
}