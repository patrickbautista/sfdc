public class JVCO_QuoteLineModifyQuoteRedirect 
{
    public PageReference redirect()
    {
        String id = ApexPages.currentPage().getParameters().get('Id');
        Pagereference pr = Page.SBQQ__sb; 
                              pr.getParameters().put('id', id);
        pr.getParameters().put('qId', id); 

        //PageReference redirectPage= new PageReference('https://sbqq' + ApexPages.currentPage().getHeaders().get('Host').substring(1) + '/apex/sb?id=' + id + '&scontrolCaching=1#quote/le?qId=' + id); 
        //redirectPage.setRedirect(true);
        return pr;
    }

}