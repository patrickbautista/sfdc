/* ----------------------------------------------------------------------------------------------
    Name: JVCO_ContractReviewQuotesExtensionTest
    Description: Test Class for JVCO_ContractReviewQuotesExtension

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    29-Mar-2017     0.1         Jules Pablo         Intial creation
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_ContractReviewQuotesExtensionTest
{
    @testSetup static void setupTestData() 
    {
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPricebook();
        insert pb;

        Product2 prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        Account a1 = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert a1;

        Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
        insert a2;
        
        Opportunity o = JVCO_TestClassObjectBuilder.createOpportunity(a2);
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Amendment';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        q.JVCO_Number_Quote_Lines__c = 1;
        insert q;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        qL.SPVAvailability__c = true;
        insert ql;
        
        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();           
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = JVCO_TestClassObjectBuilder.createAffiliation(a2.id, venueRecord.id);

        Insert  affilRecord;     
        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;   

        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_Opportunity', Value__c = 'Quote did not successfully complete because the \'Opportunity\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_ContactType', Value__c = 'Quote did not successfully complete because the \'Contact Type\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_Primary', Value__c = 'Quote did not successfully complete because the \'Primary\' checkbox was not ticked'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_StartDate', Value__c = 'Quote did not successfully complete because the \'Start Date\' was not provided'));
        //settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_EndDate', Value__c = 'Quote did not successfully complete because the \'End Date\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_SubscriptionTerm', Value__c = 'Quote did not successfully complete because the \'Subscription Term\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'ThresholdAndStatusErrorMsg', Value__c = 'You cannot complete the quote if its credit is requiring an approval'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'UpdateOppAndQuoteFail', Value__c = 'The update did not push through because an error occurred. Details are as follows:'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'UpdateOppAndQuoteSuccess', Value__c = 'Quote Successfully Completed'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_AcceptT&Cs', Value__c = 'Quote did not successfully complete because the \'T&C\' was not checked'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'CancelledQuoteErrorMsg', Value__c = 'You cannot complete and contract a Cancelled quote'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_EndDate', Value__c = 'Quote did not successfully complete because the \'End Date\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'ErrorNoSPV', Value__c = 'You can not contract the Quote since there are Quote Line(s) w/o SPV'));
        
        insert settings;

        JVCO_KABatchSetting__c kaBS = JVCO_KABatchSetting__c.getOrgDefaults();
        kaBS.JVCO_ContractRenewQuotesQueueable__c = false;
        kaBS.JVCO_ContractReviewQuotesQueueable__c = false;
        kaBS.JVCO_GenerateAmendmentQuotesQueueable__c = false;
        kaBS.JVCO_GenerateRenewalQuotesQueueable__c = false;
        upsert kaBS JVCO_KABatchSetting__c.Id;
    }
    
    private static testMethod void testMethodContactReviewExtentionCorrectBatchSize() 
    {  
        Test.startTest();
        JVCO_Array_Size__c arraySize = new JVCO_Array_Size__c();
        arraySize.JVCO_Contract_Batch_Size__c = 3;
        Insert arraySize;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        List<Account> accountList = new List<Account>([SELECT Id, JVCO_Contract_Batch_Size__c, JVCO_Customer_Account__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1]);

        List<Account> customerAccountList = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
        customerAccountList[0].TCs_Accepted__c = 'Yes';
        update customerAccountList;
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(accountList[0]);
        JVCO_ContractReviewQuotesExtension controller = new JVCO_ContractReviewQuotesExtension(standardController);

        controller.queueContractReviewQuotes();
        controller.jobCheck();


        //System.assertEquals(accountList[0], controller.accountRecord);    
        System.assertEquals(3, controller.contractBatchSize);   
        
        Test.stopTest();
        
    }

    private static testMethod void testMethodContactReviewExtentionInCorrectBatchSize() 
    {  
        Test.startTest();

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        List<Account> accountList = new List<Account>([SELECT Id, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1]);
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(accountList[0]);
        JVCO_ContractReviewQuotesExtension controller = new JVCO_ContractReviewQuotesExtension(standardController);

        controller.queueContractReviewQuotes();


        //System.assertEquals(accountList[0], controller.accountRecord);    
        System.assertEquals(1, controller.contractBatchSize);   
        
        controller.queueContractReviewQuotes();
        Test.stopTest();
    }

    private static testMethod void testMethodConfirmTCError() 
    {  
        Test.startTest();

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        List<Account> accountList = new List<Account>([SELECT Id, JVCO_Customer_Account__c, JVCO_Contract_Batch_Size__c FROM Account WHERE RecordTypeId =: licenceRT order by CreatedDate DESC LIMIT 1]);
        List<Account> customerAccountList = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
        customerAccountList[0].TCs_Accepted__c = null;
        update customerAccountList;

        ApexPages.StandardController standardController = new ApexPages.StandardController(accountList[0]);
        JVCO_ContractReviewQuotesExtension controller = new JVCO_ContractReviewQuotesExtension(standardController);

        controller.selectedTC = NULL;
        controller.queueContractReviewQuotes();

        System.assert(wasMessageAdded(new ApexPages.Message(ApexPages.severity.ERROR, Label.TermsAndConditionErrorText)));
        Test.stopTest();
    }

    //private static testMethod void testMethodConfirmTCNo() 
    //{  
    //    Test.startTest();

    //    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
    //    List<Account> accountList = new List<Account>([SELECT Id, JVCO_Customer_Account__c, JVCO_Contract_Batch_Size__c FROM Account WHERE RecordTypeId =: licenceRT order by CreatedDate DESC LIMIT 1]);
    //    List<Account> customerAccountList = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
    //    customerAccountList[0].TCs_Accepted__c = null;
    //    update customerAccountList;

    //    ApexPages.StandardController standardController = new ApexPages.StandardController(accountList[0]);
    //    JVCO_ContractReviewQuotesExtension controller = new JVCO_ContractReviewQuotesExtension(standardController);

    //    controller.selectedTC = 'No';
    //    controller.queueContractReviewQuotes();

    //    List<Account> customerAccountList2 = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
    //    System.assertEquals('No', customerAccountList2[0].TCs_Accepted__c);
    //    Test.stopTest();
    //}

    //private static testMethod void testMethodConfirmTCYes() 
    //{  
    //    Test.startTest();

    //    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
    //    List<Account> accountList = new List<Account>([SELECT Id, JVCO_Customer_Account__c, JVCO_Contract_Batch_Size__c FROM Account WHERE RecordTypeId =: licenceRT order by CreatedDate DESC LIMIT 1]);
    //    List<Account> customerAccountList = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
    //    customerAccountList[0].TCs_Accepted__c = null;
    //    update customerAccountList;

    //    ApexPages.StandardController standardController = new ApexPages.StandardController(accountList[0]);
    //    JVCO_ContractReviewQuotesExtension controller = new JVCO_ContractReviewQuotesExtension(standardController);

    //    controller.selectedTC = 'Yes';
    //    controller.queueContractReviewQuotes();
    //    List<Selectoption> tcFields = controller.getselectedTCfields();

    //    List<Account> customerAccountList2 = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
    //    System.assertEquals('Yes', customerAccountList2[0].TCs_Accepted__c);

    //    Test.stopTest();
    //}

    private static testMethod void testMethodConfirmTCYesQueueable() 
    {  
        Test.startTest();

        //JVCO_ContractReviewQuotes_Queueable.doChainJob = false;

        JVCO_KABatchSetting__c kaBS = JVCO_KABatchSetting__c.getOrgDefaults();
        kaBS.JVCO_ContractRenewQuotesQueueable__c = true;
        kaBS.JVCO_ContractReviewQuotesQueueable__c = true;
        kaBS.JVCO_GenerateAmendmentQuotesQueueable__c = true;
        kaBS.JVCO_GenerateRenewalQuotesQueueable__c = true;
        upsert kaBS JVCO_KABatchSetting__c.Id;

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        List<Account> accountList = new List<Account>([SELECT Id, JVCO_Customer_Account__c, JVCO_Contract_Batch_Size__c FROM Account WHERE RecordTypeId =: licenceRT order by CreatedDate DESC LIMIT 1]);
        List<Account> customerAccountList = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
        customerAccountList[0].TCs_Accepted__c = null;
        update customerAccountList;

        ApexPages.StandardController standardController = new ApexPages.StandardController(accountList[0]);
        JVCO_ContractReviewQuotesExtension controller = new JVCO_ContractReviewQuotesExtension(standardController);

        controller.selectedTC = 'Yes';
        controller.queueContractReviewQuotes();
        List<Selectoption> tcFields = controller.getselectedTCfields();

        List<Account> customerAccountList2 = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
        System.assertEquals('Yes', customerAccountList2[0].TCs_Accepted__c);
        Test.stopTest();
    }

    private static Boolean wasMessageAdded(ApexPages.Message message) {
         //System.assertNotEquals(TRUE, pageMessages.hasMessages());

        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertNotEquals(0, pageMessages.size());

        Boolean messageFound = false;

        for(ApexPages.Message msg : pageMessages) {
            if(msg.getSummary() == message.getSummary()
                && msg.getDetail() == message.getDetail()
                && msg.getSeverity() == message.getSeverity()) {
                messageFound = true;        
            }
        }

        return messageFound;
    }

    private static testMethod void testMethodQueueableProcessing() 
    {  
        Test.startTest();

        //JVCO_ContractReviewQuotes_Queueable.doChainJob = false;

        JVCO_KABatchSetting__c kaBS = JVCO_KABatchSetting__c.getOrgDefaults();
        kaBS.JVCO_ContractRenewQuotesQueueable__c = true;
        kaBS.JVCO_ContractReviewQuotesQueueable__c = false;
        kaBS.JVCO_GenerateAmendmentQuotesQueueable__c = true;
        kaBS.JVCO_GenerateRenewalQuotesQueueable__c = true;
        upsert kaBS JVCO_KABatchSetting__c.Id;

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        List<Account> accountList = new List<Account>([SELECT Id, JVCO_Customer_Account__c, JVCO_Contract_Batch_Size__c FROM Account WHERE RecordTypeId =: licenceRT order by CreatedDate DESC LIMIT 1]);
        List<Account> customerAccountList = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
        customerAccountList[0].TCs_Accepted__c = null;
        update customerAccountList;


        ApexPages.StandardController standardController = new ApexPages.StandardController(accountList[0]);
        JVCO_ContractReviewQuotesExtension controller = new JVCO_ContractReviewQuotesExtension(standardController);

        controller.selectedTC = 'Yes';
        controller.jobCheck();
        controller.queueContractReviewQuotes();
        controller.backToRecord();

        Test.stopTest();
    }
}