/* ----------------------------------------------------------------------------------------------
   Name: JVCO_AssignFieldAgentWrapper.cls 
   Description: Wrapper class that will hold the label and username for a postcode

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  27-Sep-2016   0.1         mark.glenn.b.ayson  Intial creation
  ----------------------------------------------------------------------------------------------- */
public with sharing class JVCO_AssignFieldAgentWrapper {

	public String postCodeLabel {get;set;}
	public String userName {get;set;}
  public String venueName {get;set;}
  public Id ownerID {get;set;}
  public Id  recordID {get;set;}

	public JVCO_AssignFieldAgentWrapper() {
		
	}
}