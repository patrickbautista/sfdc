/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CampaignUpdateMembersLogic.cls 
   Description: Business logic class for updating Ownership of CampaignMembers in Campaign

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  01-Sep-2016   0.1         ryan.i.r.limlingan  Intial creation
  07-Sep-2016   0.2         ryan.i.r.limlingan  Updated to call functions from a shared class
  19-Sep-2016   0.3         ryan.i.r.limlingan  Updated to reflect changes in a function
  21-Sep-2016   0.4         ryan.i.r.limlingan  Reverted to v0.2
  ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_CampaignUpdateMembersLogic
{

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Function that checks if appropriate Campaign changes were made
    Inputs: List<Campaign>, Map<Id, Campaign>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    01-Sep-2016 ryan.i.r.limlingan  Initial version of function
    07-Sep-2016	ryan.i.r.limlingan	Call to JVCO_CampaignSharedLogic.checkMembersFromCampaign
  ----------------------------------------------------------------------------------------------- */
    public static void updateMembers(List<Campaign> campaigns, Map<Id, Campaign> oldCampaignsMap)
    {

        Set<Campaign> campaignSet = new Set<Campaign>();
        for (Campaign c : campaigns)
        {
            if (didCampaignChannelChange(c, oldCampaignsMap)
                || didTelesalesPartnerChange(c, oldCampaignsMap))
            {
                campaignSet.add(c);
            }
        }
        // Call function to check if there are CampaignMembers to be updated
        JVCO_CampaignSharedLogic.checkMembersFromCampaign(campaignSet);
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Checks if Campaign Channel was modified or not
    Inputs: Campaign, Map<Id, Campaign>
    Returns: Boolean
    <Date>      <Authors Name>      <Brief Description of Change> 
    01-Sep-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public static Boolean didCampaignChannelChange(Campaign c, Map<Id, Campaign> oldCampaignsMap)
    {
        if ((c.JVCO_Campaign_Channel__c != oldCampaignsMap.get(c.Id).JVCO_Campaign_Channel__c)
            && c.JVCO_Campaign_Channel__c == 'Telesales')
        {
            return TRUE;
        }
        return FALSE;
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Checks if Telesales Partner was modified or not
    Inputs: Campaign, Map<Id, Campaign>
    Returns: Boolean
    <Date>      <Authors Name>      <Brief Description of Change> 
    01-Sep-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public static Boolean didTelesalesPartnerChange(Campaign c, Map<Id, Campaign> oldCampaignsMap)
    {
        if ((c.JVCO_Telesales_Partner__c != oldCampaignsMap.get(c.Id).JVCO_Telesales_Partner__c)
            && c.JVCO_Campaign_Channel__c == 'Telesales')
        {
            return TRUE;
        }
        return FALSE;
    }
}