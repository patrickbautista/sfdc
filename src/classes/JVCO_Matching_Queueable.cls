public class JVCO_Matching_Queueable Implements System.Queueable {

    public Integer batchSize;
    set<Id> lineIds;
    
    //how many queueables to launch in parallel (system limit 50)
    public static Integer defaultQueueableLimit = 50;
    public static Integer defaultBatchSize = 5;
    public static String defaultQuery = 'SELECT Id, Name, c2g__LineType__c, c2g__MatchingStatus__c, c2g__AccountOutstandingValue__c FROM c2g__codaTransactionLineItem__c WHERE c2g__MatchingStatus__c = \'Available\' AND c2g__LineType__c = \'Account\' AND c2g__AccountOutstandingValue__c!=0 AND c2g__Transaction__r.c2g__TransactionType__c IN (\'Cash\') LIMIT 50000';

    // method to launch a large number of queueable jobs to process posting of all cash entries
    public static void start() {
        start(defaultQuery,defaultBatchSize,defaultQueueableLimit);
    }

    public static void start(String queryString) {
        start(queryString,defaultBatchSize,defaultQueueableLimit);
    }
    
    public static void start(String queryString, Integer overrideBatchSize) {
        start(queryString,overrideBatchSize,defaultQueueableLimit);
    }
    
    public static void start(Integer overrideBatchSize) {
        start(defaultQuery,overrideBatchSize,defaultQueueableLimit);
    }
    
    public static void start(Integer overrideBatchSize, Integer overrideQueueableLimit) {
        start(defaultQuery,overrideBatchSize,overrideQueueableLimit);
    }
    
    public static void start(String queryString, Integer batchSize, Integer queueableLimit) {

        //select all line lines
        list<c2g__codaTransactionLineItem__c> lines = Database.query(queryString);

        //number of records to push into each queue. round up to ensure all records are processed even if some batches are of un
        Integer recordSplit = (Integer)((Decimal)lines.size()/(Decimal)queueableLimit).round(System.RoundingMode.UP);

        //loop to create the correct number of queueable jobs whilst we have records
        for(Integer i =0; i < queueableLimit && !lines.isEmpty(); i++) {

            JVCO_Matching_Queueable piq = new JVCO_Matching_Queueable();
            piq.LineIds = new set<Id>();
            piq.batchSize = batchSize;

            //put an appropriate number of records into the job
            for(Integer j =0; j < recordSplit && !lines.isEmpty(); j++) {
                piq.LineIds.add(lines.get(0).Id);
                lines.remove(0);
            }

            System.enqueueJob(piq);
        }
    }

    //execute posting
    public void execute(QueueableContext ctx) {

        
        Integer linesAdded = 0;
        list<c2g__codaTransactionLineItem__c > linesToPost = [SELECT Id, Name, c2g__LineType__c, c2g__MatchingStatus__c, c2g__AccountOutstandingValue__c FROM c2g__codaTransactionLineItem__c WHERE id in : lineids];
        list<c2g__codaTransactionLineItem__c > linesToPostInThisTransaction = new list<c2g__codaTransactionLineItem__c >();

        while(linesAdded < batchSize && !linesToPost.isEmpty()) {
            linesAdded += 1;
            linesToPostInThisTransaction.add(linesToPost.remove(0));
        }

        JVCO_BackgroundMatchingLogic.matchingLogic(new Map<Id, c2g__codaTransactionLineItem__c>(linesToPostInThisTransaction), false);

        //launch next queueable
        if(!linesToPost.isEmpty()) {
            JVCO_Matching_Queueable piq = new JVCO_Matching_Queueable();
            piq.batchSize = batchSize;
            piq.lineIds = new set<Id>();
            
            for(c2g__codaTransactionLineItem__c l : linesToPost) {
                piq.lineIds.add(l.Id);
            }
            if(!Test.isRunningTest()) {
              System.enqueueJob(piq);
            }
        }
    }
}