/* ----------------------------------------------------------------------------------------------
    Name: JVCO_GenerateRenewalQuotesExtension
    Description: Extension Class for JVCO_GenerateRenewalQuotes page

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    03-Mar-2017     0.1         Marlon Ocillos      Intial creation
    02-Apr-2018     0.6         August Del rosario  GREEN-30531 Fixed the amount of contract that is being renewed.
    12-Oct-2018     0.7         Rhys Dela Cruz      GREEN-33673 - Batch Job Helper
----------------------------------------------------------------------------------------------- */
public class JVCO_GenerateRenewalQuotesExtension 
{
    public Account fetchedAccountRecord;
    public Account accountRecord;
    public Integer renewBatchSize;
    public Integer numberOfRecords {get; set;}
    public List<Contract> contractsToProcess {get; set;}
    public Boolean isContinued {get; set;}
    public Boolean isProcessing {get; set;}
    public String apexJobId {get; set;} 
    public Boolean canContinue {get; set;}
    public Boolean queueableRunning {get;set;}
    
    public JVCO_GenerateRenewalQuotesExtension (ApexPages.StandardController stdController) 
    {
        numberOfRecords = 0;
        contractsToProcess = new List<Contract>();
        isContinued = false;
        fetchedAccountRecord = (Account)stdController.getRecord();
        accountRecord = [select Id, Name, Licence_Account_Number__c, JVCO_Renew_Batch_Size__c,JVCO_ProcessingGenerateRenewQuotes__c,JVCO_GenerateRenewQuotesApexJob__c, JVCO_GenerateRenewQuotesQueueable__c from Account where Id =: fetchedAccountRecord.Id];

        if(accountRecord.JVCO_ProcessingGenerateRenewQuotes__c || accountRecord.JVCO_GenerateRenewQuotesQueueable__c == 'Queueable'){

            isProcessing = true;
        }

        queueableRunning = false;
        if(!accountRecord.JVCO_ProcessingGenerateRenewQuotes__c && accountRecord.JVCO_GenerateRenewQuotesQueueable__c == 'Queueable'){

            //apexJobId = null;
            queueableRunning = true;
        }
        else{

            apexJobId = accountRecord.JVCO_GenerateRenewQuotesApexJob__c;
        }
        
        if (accountRecord.JVCO_Renew_Batch_Size__c != null && accountRecord.JVCO_Renew_Batch_Size__c != 0) 
        {
            renewBatchSize = (Integer)accountRecord.JVCO_Renew_Batch_Size__c;   
        } else {
            renewBatchSize = 1;
        }
        
        contractsToProcess = [select Id, EndDate, ForceDeferred__c from Contract where SBQQ__RenewalQuoted__c = false and JVCO_Renewal_Generated__c = false and JVCO_RenewableQuantity__c > 0 and AccountId =: accountRecord.Id];
        //numberOfRecords = contractsToProcess.size();

        //START 03-Apr-2018 GREEN-30531 louis.a.del.rosario
        Set<Id> setIdContract = new Set<Id>();
        Set<Id> setIdProcessContract = new Set<Id>();

        for(Contract c:contractsToProcess){
            setIdContract.add(c.Id);
        }

        System.debug('## ADR Ola contract Id -- ' +  setIdContract);

        Map<Id,SBQQ__Subscription__c> mapIdSubscription = new Map<Id,SBQQ__Subscription__c>([SELECT Id,SBQQ__RenewalQuantity__c,SBQQ__Contract__c,SBQQ__Contract__r.JVCO_Contract_EndDate__c,Affiliation__c,Affiliation__r.JVCO_End_Date__c FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c =: setIdContract]);

        System.debug('## ADR Ola mapIdSubscription  -- ' +  mapIdSubscription);

        for(SBQQ__Subscription__c s:mapIdSubscription.values()){
            if(s.Affiliation__r.JVCO_End_Date__c == null){
                setIdProcessContract.add(s.SBQQ__Contract__c);
            }else if(s.SBQQ__Contract__r.JVCO_Contract_EndDate__c > s.Affiliation__r.JVCO_End_Date__c){
                setIdProcessContract.add(s.SBQQ__Contract__c);
            }
        }

        numberOfRecords = setIdProcessContract.size();
        //END 03-Apr-2018 GREEN-30531 louis.a.del.rosario

        canContinue = true;
        if(!JVCO_BatchJobHelper.checkLargeJobs(numberOfRecords)){
            canContinue = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.JVCO_BatchJobHelperCannotProcess));
        }

    }
    
    public void queueGenerateRenewalQuotes() 
    {
        if(JVCO_KABatchSetting__c.getInstance(userinfo.getUserId()).JVCO_GenerateRenewalQuotesQueueable__c) {

            Id queueableId = System.enqueueJob(new JVCO_GenerateRenewalQuotes_Queueable(accountRecord));

            //KA Queueable Update Processing Status
            JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_GenerateRenewQuotesQueueable__c', 'Queueable','JVCO_GenerateRenewQuotesLastSubmitted__c');

            JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
            kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':GenerateRenewals';
            kaTempRec.JVCO_AccountId__c = accountRecord.Id;
            kaTempRec.JVCO_JobId__c = queueableId;

            upsert kaTempRec Name;

        } 
        else {

            Id batchId = database.executeBatch(new JVCO_GenerateRenewalQuotesBatch(accountRecord), renewBatchSize);           
            JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_GenerateRenewQuotesApexJob__c', batchId,'JVCO_GenerateRenewQuotesLastSubmitted__c');
        }

        isContinued = true;
    }
    
    public PageReference backToRecord() 
    {
        PageReference pRef = new PageReference('/' + accountRecord.Id);
        return pRef;
    }

    //GREEN-33789
    public PageReference jobCheck(){
        
        Id batchId = apexJobId;
        Id accountId = fetchedAccountRecord.Id;

        PageReference tempRef;

        JVCO_ApexJobProgressBarController progBar = new JVCO_ApexJobProgressBarController();

        //PageReference tempRef = progBar.updateAcc(batchId, accountId);

        if(!queueableRunning){

            tempRef = progBar.updateAcc(batchId, accountId);
        }
        else{

            tempRef = progBar.updateAcc(accountId, 'GenerateRenewals');   
        }

        return tempRef;
    }
}