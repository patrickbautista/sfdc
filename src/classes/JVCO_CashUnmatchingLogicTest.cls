@isTest
public class JVCO_CashUnmatchingLogicTest
{
	@testSetup
	public static void createTestData()
	{
		Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        c2g__codaDimension2__c dim2 = JVCO_TestClassHelper.setDim2('PPL');
        insert dim2;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

		JVCO_CashUnmatchingLogic.openPeriodId = UserInfo.getUserId();
		//Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_TestClassHelper.setGeneralSettingsCS();
	}

	@isTest
	public static void testUndoCashMatching()
	{
		c2g__codaCashMatchingHistory__c cashMatchingHist = new c2g__codaCashMatchingHistory__c();
		cashMatchingHist.c2g__Account__c = UserInfo.getUserId();
		cashMatchingHist.c2g__MatchingReference__c = UserInfo.getUserId();
		try
		{
			JVCO_CashUnmatchingLogic.undoCashMatching(cashMatchingHist);
			System.assert(false);
		}catch(Exception e)
		{
			System.assert(true);
		}
	}

	@isTest
	public static void testRematchToRefund()
	{
		c2g__codaCashEntry__c rematchToRefund = new c2g__codaCashEntry__c();
        rematchToRefund.c2g__PaymentMethod__c = 'Direct Debit';
        rematchToRefund.c2g__Type__c = 'Receipt';
        rematchToRefund.ffcash__DeriveCurrency__c = true;
        insert rematchToRefund;

        c2g__codaCashEntryLineItem__c cashEntryLine = new c2g__codaCashEntryLineItem__c();
        cashEntryLine.c2g__Account__c = [SELECT Id FROM Account WHERE RecordTypeId = :JVCO_GlobalConstants.ACCOUNT_LICENCE_RECORDTYPE_ID].Id;
        cashEntryLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cashEntryLine.c2g__CashEntryValue__c = 1199;
        cashEntryLine.c2g__CashEntry__c = rematchToRefund.Id;
        insert cashEntryLine;

        c2g.CODAAPICommon_7_0.Context context = new c2g.CODAAPICommon_7_0.Context();
        c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
        ref.Id = rematchToRefund.Id;
        Test.startTest();
        c2g.CODAAPICashEntry_7_0.PostCashEntry(context, ref);
        Test.stopTest();

		List<c2g__codaTransactionLineItem__c> receiptCashTransLineList = [SELECT Id FROM c2g__codaTransactionLineItem__c WHERE c2g__LineType__c = 'Account'];
		try
		{
			JVCO_CashUnmatchingLogic.rematchToRefund(rematchToRefund, receiptCashTransLineList);
			System.assert(false);
		}catch(Exception e)
		{
			System.assert(true);
		}
	}
}