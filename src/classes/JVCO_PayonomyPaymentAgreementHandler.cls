/* ----------------------------------------------------------------------------------------------
    Name: PayonomyPaymentAgreementHandler.cls 
    Description: Handler class for PAYREC2__Payment_Agreement__c trigger

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    29-Dec-2016   0.1        ryan.i.r.limlingan  Intial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_PayonomyPaymentAgreementHandler
{
    public static Boolean stopAgreementDeletion = true;
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_PayonomyPaymentAgreementTrigger__c : false;
    private static Map<String, JVCO_Payonomy_Error_Codes__c> PAYMENT_ERROR_CODE;

    /* ------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: This method handles beforeInsert trigger events for PAYREC2__Payment_Agreement__c
        Input: List<PAYREC2__Payment_Agreement__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        10-Jan-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static void beforeInsert(List<PAYREC2__Payment_Agreement__c> agreements)
    {
        if(!skipTrigger)
        { 
            JVCO_PayonomyPaymentAgreementLogic.linkToInvoiceGroup(agreements);
        }
    }

    /* ------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: This method handles afterInsert trigger events for PAYREC2__Payment_Agreement__c
        Input: List<PAYREC2__Payment_Agreement__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        29-Dec-2016 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static void afterInsert(List<PAYREC2__Payment_Agreement__c> agreements)
    {
        if(!skipTrigger) 
        {
            JVCO_PayonomyPaymentAgreementLogic.linkInvoiceGrouptoPaymentAgreement(agreements);
            JVCO_PayonomyPaymentAgreementLogic.deleteDuplicateAgreements(agreements);
            JVCO_PayonomyPaymentAgreementLogic.createInstalmentPlan(agreements);
        }
    }

    /* ------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: This method handles afterInsert trigger events for PAYREC2__Payment_Agreement__c
        Input: List<PAYREC2__Payment_Agreement__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        12-Nov-2018 franz.g.a.dimaapi    Initial version of function - GREEN-33455
    ----------------------------------------------------------------------------------------------- */
    public static void beforeUpdate(List<PAYREC2__Payment_Agreement__c> newAgreementList, Map<Id, PAYREC2__Payment_Agreement__c> oldAgreementMap)
    {
        if(!skipTrigger)
        {
            revertStatusToPriorStatus(newAgreementList, oldAgreementMap);
        }
    }

    public static void afterUpdate(List<PAYREC2__Payment_Agreement__c> newAgreementList, Map<Id, PAYREC2__Payment_Agreement__c> oldAgreementMap)
    {
        if(!skipTrigger)
        {   
            Map<Id, Id> accIdToOldBankAccountIdMap = new Map<Id, Id>();
            Map<Id, Id> accIdToNewBankAccountIdMap = new Map<Id, Id>();
            Map<Id, PAYREC2__Payment_Agreement__c> erroredAgreementMap = new Map<Id, PAYREC2__Payment_Agreement__c>();
            for(PAYREC2__Payment_Agreement__c agreement : newAgreementList)
            {
                PAYREC2__Payment_Agreement__c oldAgreement = oldAgreementMap.get(agreement.Id);
                if(agreement.PAYFISH3__Current_Bank_Account__c != oldAgreement.PAYFISH3__Current_Bank_Account__c)
                {
                    accIdToOldBankAccountIdMap.put(oldAgreement.PAYREC2__Account__c, oldAgreement.PAYFISH3__Current_Bank_Account__c);
                    accIdToNewBankAccountIdMap.put(agreement.PAYREC2__Account__c, agreement.PAYFISH3__Current_Bank_Account__c);
                }
                if(agreement.JVCO_Status_Message__c != oldAgreement.JVCO_Status_Message__c &&
                    agreement.PAYREC2__Status__c.equalsIgnoreCase('Errored'))
                {
                    erroredAgreementMap.put(agreement.Id, agreement);
                }
            }
            
            if(!accIdToOldBankAccountIdMap.isEmpty() && !accIdToNewBankAccountIdMap.isEmpty())
            {
                reflectBankDetailChangesToDDMandate(accIdToOldBankAccountIdMap, accIdToNewBankAccountIdMap);
            }
            if(!erroredAgreementMap.isEmpty())
            {
                deactivateDDMandateForDeliberate(erroredAgreementMap);
            }  
        }
    }

    /* ------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: This method handles beforeDelete trigger events for PAYREC2__Payment_Agreement__c
        Input: List<PAYREC2__Payment_Agreement__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Nov-2018 franz.g.a.dimaapi   Initial version of function - GREEN-33773
        27-Feb-2018 patrick.t.bautista  Added logic when it status not equals new instruction GREEN-34162
    ----------------------------------------------------------------------------------------------- */
    public static void beforeDelete(List<PAYREC2__Payment_Agreement__c> agreementList)
    {
        if(!skipTrigger)
        {
            preventAgreementDeletion(agreementList);
        }
    }

    private static void revertStatusToPriorStatus(List<PAYREC2__Payment_Agreement__c> newAgreementList, Map<Id, PAYREC2__Payment_Agreement__c> oldAgreementMap)
    {
        for(PAYREC2__Payment_Agreement__c agreement : newAgreementList)
        {
            PAYREC2__Payment_Agreement__c oldAgreement = oldAgreementMap.get(agreement.Id);
            //If Expired revert to prior status
            if(oldAgreement.PAYREC2__Status__c.equalsIgnoreCase('expired') &&
              agreement.PAYFISH3__Current_Bank_Account__c != oldAgreement.PAYFISH3__Current_Bank_Account__c)
            {
                agreement.PAYREC2__Status__c = oldAgreement.PAYREC2__Status__c;
            //If status is active save to prior status
            }else if(agreement.PAYREC2__Status__c.equalsIgnoreCase('new instruction') &&
                     !oldAgreement.PAYREC2__Status__c.equalsIgnoreCase('new instruction') &&
                     agreement.PAYFISH3__Current_Bank_Account__c != oldAgreement.PAYFISH3__Current_Bank_Account__c)
            {
                agreement.JVCO_Prior_Status__c = oldAgreement.PAYREC2__Status__c;
            //if status is active and old status != new instruction, cancel, errored
            }else if(!agreement.PAYREC2__Status__c.equalsIgnoreCase('new instruction') &&
                     !agreement.PAYREC2__Status__c.equalsIgnoreCase('cancel') &&
                     !agreement.PAYREC2__Status__c.equalsIgnoreCase('Errored') &&
                     agreement.PAYFISH3__Current_Bank_Account__c != oldAgreement.PAYFISH3__Current_Bank_Account__c)
            {
                agreement.JVCO_Prior_Status__c = agreement.PAYREC2__Status__c;
                agreement.PAYREC2__Status__c = 'New instruction';
            //If Prior Status is populated revert the status
            }else if(agreement.PAYREC2__Status__c.equalsIgnoreCase('first collection') && agreement.JVCO_Prior_Status__c != null)
            {
                agreement.PAYREC2__Status__c = agreement.JVCO_Prior_Status__c;
                agreement.JVCO_Prior_Status__c = null;
            //If Errored And old status is First Represent revert to prior status
            }else if(agreement.PAYREC2__Status__c.equalsIgnoreCase('Errored') &&
                oldAgreement.PAYREC2__Status__c.equalsIgnoreCase('First re-present'))
            {
                agreement.PAYREC2__Status__c = 'First re-present';
            }
        }
    }

    /* ------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: This method handles beforeDelete trigger events for PAYREC2__Payment_Agreement__c
        Input: List<PAYREC2__Payment_Agreement__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Mar-2019 franz.g.a.dimaapi   Initial version of function - GREEN-34128
        16-Apr-2019 franz.g.a.dimaapi   GREEN-34421 - Added Retiring of Invoice Group
    ----------------------------------------------------------------------------------------------- */
    private static void deactivateDDMandateForDeliberate(Map<Id, PAYREC2__Payment_Agreement__c> erroredAgreementMap)
    {
        //Map all Paymonomy Error Codes
        if(PAYMENT_ERROR_CODE == null)
        {
            PAYMENT_ERROR_CODE = JVCO_Payonomy_Error_Codes__c.getAll();
        }
        if(!PAYMENT_ERROR_CODE.isEmpty())
        {
            Set<Id> invoiceGroupIdSet = new Set<Id>();
            Map<Id, String> accIdToPayonomyErrorTypeMap = new Map<Id, String>();
            for(Id agreementId : erroredAgreementMap.keySet())
            {
                PAYREC2__Payment_Agreement__c agreement = erroredAgreementMap.get(agreementId);
                String errorCode = agreement.JVCO_Status_Message__c.substringBefore(':');
                String errorCodeType = agreement.JVCO_Status_Message__c.substringAfter(':').substringBefore(':').trim();
                String fullErrorCode = errorCode+' '+errorCodeType;
                if(PAYMENT_ERROR_CODE.containsKey(fullErrorCode) && PAYMENT_ERROR_CODE.get(fullErrorCode).JVCO_Deliberate__c.equalsIgnoreCase('Deliberate'))
                {
                    accIdToPayonomyErrorTypeMap.put(agreement.PAYREC2__Account__c, fullErrorCode.trim());
                    invoiceGroupIdSet.add(agreement.JVCO_Invoice_Group__c);
                }
            }
            
            if(!accIdToPayonomyErrorTypeMap.isEmpty())
            {
                List<JVCO_DD_Mandate__c> ddMandateList = [SELECT Id
                                                            FROM JVCO_DD_Mandate__c
                                                            WHERE JVCO_Deactivated__c = FALSE
                                                            AND JVCO_Licence_Account__c IN : accIdToPayonomyErrorTypeMap.keySet()];
                for(JVCO_DD_Mandate__c ddMandate : ddMandateList)
                {
                    ddMandate.JVCO_Deactivated__c = true;
                }
                update ddMandateList;

                List<c2g__codaInvoice__c> sInvList = [SELECT Id FROM c2g__codaInvoice__c WHERE JVCO_Invoice_Group__c IN : invoiceGroupIdSet];
                for(c2g__codaInvoice__c sInv : sInvList)
                {
                    sInv.JVCO_Invoice_Group__c = null;
                }
                update sInvList;
            }
        }
    }

    /* ------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: This method handles afterInsert trigger events for PAYREC2__Payment_Agreement__c
        Input: List<PAYREC2__Payment_Agreement__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        15-Jul-2019 franz.g.a.dimaapi   GREEN-34422 - Removed JVCO_Bank_Name__c in the criteria
    ----------------------------------------------------------------------------------------------- */
    private static void reflectBankDetailChangesToDDMandate(Map<Id, Id> accIdToOldBankAccountIdMap, Map<Id, Id> accIdToNewBankAccountIdMap)
    {
        Map<Id, PAYACCVAL1__Bank_Account__c> bankAccountMap = new Map<Id, PAYACCVAL1__Bank_Account__c>(
                                                                [SELECT Id, PAYACCVAL1__Account_Number__c,
                                                                PAYACCVAL1__Bank_Name__c,
                                                                PAYACCVAL1__Sort_Code__c
                                                                FROM PAYACCVAL1__Bank_Account__c
                                                                WHERE Id IN :accIdToOldBankAccountIdMap.values()
                                                                OR Id IN :accIdToNewBankAccountIdMap.values()]);
        
        List<JVCO_DD_Mandate__c> ddMandateList = new List<JVCO_DD_Mandate__c>();
        for(JVCO_DD_Mandate__c ddMandate : [SELECT Id, JVCO_Licence_Account__c,
                                            JVCO_Account_Number__c,
                                            JVCO_Bank_Name__c,
                                            JVCO_Sort_Code__c
                                            FROM JVCO_DD_Mandate__c
                                            WHERE JVCO_Licence_Account__c IN : accIdToOldBankAccountIdMap.keySet()
                                            AND JVCO_Deactivated__c = FALSE])
        {
            Id oldBankAccountId = accIdToOldBankAccountIdMap.get(ddMandate.JVCO_Licence_Account__c);
            Id newBankAccountId = accIdToNewBankAccountIdMap.get(ddMandate.JVCO_Licence_Account__c);
            PAYACCVAL1__Bank_Account__c oldBankAccount = bankAccountMap.get(oldBankAccountId);
            if(oldBankAccount != null && 
                ddMandate.JVCO_Account_Number__c == oldBankAccount.PAYACCVAL1__Account_Number__c &&
                ddMandate.JVCO_Sort_Code__c == oldBankAccount.PAYACCVAL1__Sort_Code__c &&
                bankAccountMap.containsKey(newBankAccountId))
            {
                PAYACCVAL1__Bank_Account__c newBankAccount = bankAccountMap.get(newBankAccountId);
                ddMandate.JVCO_Account_Number__c = newBankAccount.PAYACCVAL1__Account_Number__c;
                ddMandate.JVCO_Bank_Name__c = newBankAccount.PAYACCVAL1__Bank_Name__c;
                ddMandate.JVCO_Sort_Code__c = newBankAccount.PAYACCVAL1__Sort_Code__c;
                ddMandateList.add(ddMandate);
            }
        }
        update ddMandateList;
    }

    /* ------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: This method prevents the deletion of the Agreement records.
        Input: List<PAYREC2__Payment_Agreement__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Nov-2018 franz.g.a.dimaapi   Initial version of function - GREEN-33773
    ----------------------------------------------------------------------------------------------- */
    private static void preventAgreementDeletion(List<PAYREC2__Payment_Agreement__c> agreementList)
    {
        if(stopAgreementDeletion)
        {
            List<Profile> profileList = [SELECT Id FROM Profile 
                                        WHERE Id =: UserInfo.getProfileId()
                                        AND Name IN ('System Administrator','Finance')];
            if(profileList.isEmpty())
            {
                for(PAYREC2__Payment_Agreement__c agreement : agreementList)
                {
                    agreement.addError('You Cannot Delete An Agreement');
                }
            }
        }
    }
}