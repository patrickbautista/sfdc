@isTest
private class SMP_PaymentWizardControllerTest {
    @testSetup static void createTestData(){
        
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        //JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        
        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;
        
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        
        Test.startTest();
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        Set<Id> orderGroupIdSet = new Set<Id>();  
        orderGroupIdSet.add(orderGroup.Id);

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_TestClassHelper.setGeneralSettingsCS();
        insert new GLA_for_Cash_Transfer__c(name = '10100 - Bank Cash'); 
        //JVCO_BackgroundMatchingLogic.stopMatching = true;
        JVCO_FFUtil.stopCashTransferLogic = true;
        
        Database.executeBatch(new JVCO_OrderMultipleBillNowLogicBatch(orderGroupIdSet));
        Test.stopTest();
        
        Income_Direct_Debit__c idd = new Income_Direct_Debit__c();
        idd.Account__c = licAcc.id;
        idd.DD_Bank_Sort_Code__c = '938600';
        idd.DD_Bank_Account_Number__c = '42368003';
        idd.DD_Collection_Day__c = '3';
        idd.DD_Collection_Period__c = 'Monthly';
        idd.DD_Status__c = 'New Instruction';
        idd.DD_Bank_Account_Name__c = 'Test';
        idd.DD_Account_Email__c = c.Email;
        idd.Contact__c = c.id;
        insert idd;
    }
    @isTest
    static void testNext() {
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.next();
    }
    @isTest
    static void testNext1() {
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.directDebit.DD_Account_Number_OK__c = true;
        controller.directDebit.DD_Sort_Code_OK__c = true;
        controller.stepNo = 3;
        controller.next();
    }
    @isTest
    static void testNext2() {
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.directDebit.DD_Account_Number_OK__c = false;
        controller.directDebit.DD_Sort_Code_OK__c = false;
        controller.stepNo = 3;
        controller.firstCollectionDate = date.today().addDays(12);
        controller.amount = 100;
        controller.cardPaymentAmount = 10;
        controller.newDirectDebit = true;
        controller.selectedSalesInvoiceList = [SELECT Id, c2g__Account__c, JVCO_Invoice_Group__c, c2g__OutstandingValue__c 
                                               FROM c2g__codaInvoice__c];
        String responseBody = '{"ProcessListNewDDFirstCollectionDateResult":[{"Error":"","FirstCollectionDate":"11-05-2017"},{"Error":"","FirstCollectionDate":"11-05-2017"}]}';
        HttpRestCallOutMock fakeResponse = new HttpRestCallOutMock(200, responseBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Test.startTest();
        controller.next();
        Test.stopTest();
    }
    @isTest
    static void testBack() {
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.back();
    }
    @isTest
    static void testCancel() {
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.cancel();
    }
    @isTest
    static void testrecalculateTotals() {
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.recalculateTotals();
        //System.AssertEquals(false,controller.selectedSalesInvoiceList.isEmpty());
    }
    @isTest
    static void testController() {
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        //System.AssertEquals(false,controller.salesInvoiceWrapperList.isEmpty());
    }
    
    @isTest
    static void testValidatePaymentDates(){
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.firstCollectionDate = date.today().addDays(12);
        controller.amount = 100;
        controller.cardPaymentAmount = 10;
        controller.newDirectDebit = true;
        String responseBody = '{"ProcessListNewDDFirstCollectionDateResult":[{"Error":"","FirstCollectionDate":"11-05-2017"},{"Error":"","FirstCollectionDate":"11-05-2017"}]}';
        HttpRestCallOutMock fakeResponse = new HttpRestCallOutMock(200, responseBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Test.startTest();
        controller.validatePaymentDates();
        controller.validateBankDetails();
        controller.finalisePaymentDetails();
        Test.stopTest();
    }
    @isTest
    static void testValidatePaymentDates1(){
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.firstCollectionDate = date.today().addDays(12);
        controller.amount = 100;
        controller.cardPaymentAmount = 0;
        controller.newDirectDebit = true;
        String responseBody = '{"ProcessListNewDDFirstCollectionDateResult":[{"Error":"","FirstCollectionDate":"11-05-2017"},{"Error":"","FirstCollectionDate":"11-05-2017"}]}';
        HttpRestCallOutMock fakeResponse = new HttpRestCallOutMock(200, responseBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Test.startTest();
        controller.validatePaymentDates();
        controller.validateBankDetails();
        controller.finalisePaymentDetails();
        Test.stopTest();
    }
    @isTest
    static void testValidatePaymentDates2(){
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.firstCollectionDate = date.today().addDays(12);
        controller.amount = 100;
        controller.cardPaymentAmount = 10;
        controller.newDirectDebit = false;
        String responseBody = '{"ProcessListNewDDFirstCollectionDateResult":[{"Error":"","FirstCollectionDate":"11-05-2017"},{"Error":"","FirstCollectionDate":"11-05-2017"}]}';
        HttpRestCallOutMock fakeResponse = new HttpRestCallOutMock(200, responseBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Test.startTest();
        controller.validatePaymentDates();
        controller.validateBankDetails();
        controller.finalisePaymentDetails();
        Test.stopTest();
    }
    @isTest
    static void testValidatePaymentDates3(){
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.firstCollectionDate = date.today().addDays(12);
        controller.amount = 100;
        controller.cardPaymentAmount = 0;
        controller.newDirectDebit = false;
        controller.selectedRange = '3';
        String responseBody = '{"ProcessListNewDDFirstCollectionDateResult":[{"Error":"","FirstCollectionDate":"11-05-2017"},{"Error":"","FirstCollectionDate":"11-05-2017"}]}';
        HttpRestCallOutMock fakeResponse = new HttpRestCallOutMock(200, responseBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Test.startTest();
        controller.validatePaymentDates();
        controller.validateBankDetails();
        controller.finalisePaymentDetails();
        //controller.addToCurrentDirectDebit();
        controller.getExistingDirectDebit();
        Test.stopTest();
    }
    @isTest
    static void testValidatePaymentDatesOldDD(){
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.firstCollectionDate = date.today().addDays(12);
        controller.amount = 100;
        controller.cardPaymentAmount = 10;
        controller.newDirectDebit = false;
        String responseBody = '{"ProcessListNewDDFirstCollectionDateResult":[{"Error":"","FirstCollectionDate":"11-05-2017"},{"Error":"","FirstCollectionDate":"11-05-2017"}]}';
        HttpRestCallOutMock fakeResponse = new HttpRestCallOutMock(200, responseBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Test.startTest();
        controller.validatePaymentDates();
        Test.stopTest();
    }
    @isTest
    static void testValidatePaymentDatesIncorrectDates(){
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.firstCollectionDate = date.today();
        controller.amount = 100;
        controller.cardPaymentAmount = 10;
        controller.newDirectDebit = true;
        controller.validatePaymentDates();
    }
    
    @isTest
    static void testValidatePaymentDatesIncorrectDates1(){
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.firstCollectionDate = date.today().addYears(2);
        controller.amount = 100;
        controller.cardPaymentAmount = 10;
        controller.newDirectDebit = true;
        controller.validatePaymentDates();
    }
    
    @isTest
    static void testValidatePaymentDatesIncorrectDates2(){
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.firstCollectionDate = date.today().addDays(11);
        controller.amount = 100;
        controller.cardPaymentAmount = 100;
        controller.newDirectDebit = true;
        List<SelectOption> optionList = controller.getInstallmentRange();
        optionList = controller.getInstallmentDays();
        List<string> stringList = controller.getDatePickListValuesIntoList();
        controller.showSpinner();
        
        controller.validatePaymentDates();
    }
    @isTest
    static void testValidatePaymentDatesIncorrectDates3(){
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.firstCollectionDate = Date.newInstance(date.today().year(), date.today().month(), 30);
        controller.amount = 100;
        controller.cardPaymentAmount = 10;
        controller.newDirectDebit = true;
        SMP_PaymentWizardController.ddPaymentWrapper pWrapper = new SMP_PaymentWizardController.ddPaymentWrapper();
        pWrapper.ddinvoiceGroup = null;
        pWrapper.directDebit = null;
        pWrapper.invoiceGroup = null;
        pWrapper.paymentWrapperList = null;
        pWrapper.salesInvoiceList = null;
        SMP_PaymentWizardController.ddWrapper wrapper = new SMP_PaymentWizardController.ddWrapper();
        wrapper.directDebit = null;
        wrapper.selected = true;
        
        controller.validatePaymentDates();
    }
    @isTest
    static void testValidateExistingGin(){
        c2g__codaInvoice__c invoice = [SELECT Id, Name, c2g__Account__c, JVCO_Invoice_Group__c, c2g__OutstandingValue__c 
                                       FROM c2g__codaInvoice__c limit 1];
        
        Income_Direct_Debit__c incomeDD = [SELECT Id, Name, DD_Next_Collection_Date__c, DD_Ongoing_Collection_Amount__c,
                                           DD_Account_Number_OK__c, DD_Sort_Code_OK__c, DD_Bank_Name__c, DD_Branch_Name__c,
                                           DD_Bank_Account_Name__c, DD_Bank_Account_Number__c, DD_Bank_Sort_Code__c, DD_First_Collection_Date__c 
                                           FROM Income_Direct_Debit__c limit 1];
        
        JVCO_Invoice_Group__c invoiceGroup = new JVCO_Invoice_Group__c();
        invoiceGroup.JVCO_Total_Amount__c = 12;
        invoiceGroup.CC_SINs__c = invoice.Name + ',';
        insert invoiceGroup;
            
        SMP_DirectDebit_GroupInvoice__c IDDInvGroup = new SMP_DirectDebit_GroupInvoice__c();
        IDDInvGroup.Income_Direct_Debit__c = incomeDD.Id;
        IDDInvGroup.Invoice_Group__c = invoiceGroup.Id;
        insert IDDInvGroup;
        
        invoice.JVCO_Invoice_Group__c = invoiceGroup.Id;
        update invoice;
        
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.firstCollectionDate = date.today().addDays(12);
        controller.amount = 100;
        controller.cardPaymentAmount = 0;
        controller.newDirectDebit = false;
        controller.selectedRange = '3';
        String responseBody = '{"ProcessListNewDDFirstCollectionDateResult":[{"Error":"","FirstCollectionDate":"11-05-2017"},{"Error":"","FirstCollectionDate":"11-05-2017"}]}';
        HttpRestCallOutMock fakeResponse = new HttpRestCallOutMock(200, responseBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Test.startTest();
        controller.validatePaymentDates();
        controller.validateBankDetails();
        controller.finalisePaymentDetails();
        Test.stopTest();
    }
    @isTest
    static void testValidatePaymentDates4(){
        Account account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c,
                           JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c 
                           from Account 
                           where RecordType.Name = 'Licence Account' limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        
        SMP_PaymentWizardController controller = new SMP_PaymentWizardController(sc);
        controller.firstCollectionDate = date.today().addDays(12);
        controller.amount = 100;
        controller.cardPaymentAmount = 0;
        controller.newDirectDebit = false;
        controller.selectedRange = '3';
        String responseBody = '{"ProcessListNewDDFirstCollectionDateResult":[{"Error":"","FirstCollectionDate":"11-05-2017"},{"Error":"","FirstCollectionDate":"11-05-2017"}]}';
        HttpRestCallOutMock fakeResponse = new HttpRestCallOutMock(200, responseBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Test.startTest();
        controller.validatePaymentDates();
        controller.validateBankDetails();
        controller.finalisePaymentDetails();
        //controller.addToCurrentDirectDebit();
        controller.createInvoiceGroup('Direct Debit', false, controller.selectedSalesInvoiceList);
        Test.stopTest();
        controller.newDirectDebit();
    }
}