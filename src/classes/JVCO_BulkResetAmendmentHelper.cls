/* ----------------------------------------------------------------------------------------------
   Name: JVCO_BulkResetAmendmentHelper
   Description: To allow for KA Amendment Quotes to have their Net Price zero�d

   Date         Version     Author               Summary of Changes 
   -----------  -------     -----------------    -----------------------------------------
   09-Nov-2018   0.1        rhys.j.c.dela.cruz   Intial creation
   20-May-2019   0.2        rhys.j.c.dela.cruz   GREEN-34604 - Update to code to have QLG match Quote's NetTotal 
----------------------------------------------------------------------------------------------- */
public class JVCO_BulkResetAmendmentHelper{

    public Account accountRecord {get; set;}
    public Integer numToProcess {get; set;}
    public Boolean canProcess {get;set;}

    @TestVisible private static Boolean testError = false;
    @TestVisible private static Boolean testError2 = false;

    public JVCO_BulkResetAmendmentHelper(ApexPages.StandardController stdController){

        this.accountRecord = (Account)stdController.getRecord();

        accountRecord = [SELECT id, Name, JVCO_GenerateReviewQuotesLastSubmitted__c FROM Account WHERE Id =: accountRecord.Id];

        numToProcess  = [select count() from sbqq__quoteline__c where sbqq__quote__r.sbqq__account__c =: accountRecord.Id AND SBQQ__Quote__r.SBQQ__Type__c = 'Amendment' AND SBQQ__Quote__r.SBQQ__Status__c = 'Draft' AND Affiliation__r.JVCO_End_Date__c = null AND SBQQ__Quote__r.JVCO_AmendmentReset__c = false AND Terminate__c = false AND SBQQ__Quote__r.JVCO_Recalculated__c = true AND SBQQ__NetTotal__c != 0];

        User currentUser = [SELECT Id, Profile.Name FROM User WHERE Id =: UserInfo.getUserId()];

        //5 Min Buffer Validation
        Integer tempMinutes = (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_KABulkResetBufferMins__c != null ? (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_KABulkResetBufferMins__c : 5;

        if(accountRecord.JVCO_GenerateReviewQuotesLastSubmitted__c != null && currentUser.Profile.Name == 'Key Accounts Manager'){
            DateTime tempDt = accountRecord.JVCO_GenerateReviewQuotesLastSubmitted__c;
            tempDt.addMinutes(tempMinutes);
            if(System.now() < tempDt){
                canProcess = true;
            }
            else{
                canProcess = false;
            }
        }
        else if(currentUser.Profile.Name == 'System Administrator' && numToProcess > 0){
            canProcess = true;
        }
        else{
            canProcess = false;
        }

    }
    

    public PageReference updateQuoteLines(){

        Id accountRecordId = accountRecord.Id;
        
        runReset(accountRecordId);

        PageReference returnToAcc = new PageReference('/' + accountRecordId);

        return returnToAcc;
    }

    public PageReference backToAcc(){

        PageReference returnToAcc = new PageReference('/' + accountRecord.Id);

        return returnToAcc;
    }

    @future
    public static void runReset(Id accountRecordId){

        Map<Id, SBQQ__QuoteLine__c> quoteLineMap = new Map<Id, SBQQ__QuoteLine__c>();
        List<SBQQ__QuoteLine__c> qlForUpdate = new List<SBQQ__QuoteLine__c>();
        String query = 'SELECT Id, SBQQ__Quantity__c, CustomMinMaxCalcType__c, SBQQ__PriorQuantity__c, SBQQ__RequiredBy__r.LimitDeltaTotal__c, JVCO_PriorTotal__c, SBQQ__Quote__c, SBQQ__Quote__r.JVCO_AmendmentReset__c, SBQQ__Quote__r.Recalculate__c, SBQQ__Group__c FROM SBQQ__QuoteLine__c where SBQQ__Quote__r.SBQQ__Account__c = \''+ accountRecordId + '\' AND SBQQ__Quote__r.SBQQ__Type__c = \'Amendment\' AND SBQQ__Quote__r.SBQQ__Status__c = \'Draft\' AND Affiliation__r.JVCO_End_Date__c = null AND SBQQ__Quote__r.JVCO_AmendmentReset__c = false AND Terminate__c = false AND SBQQ__Quote__r.JVCO_Recalculated__c = true AND SBQQ__NetTotal__c != 0';

        //If null, AdHoc Test
        //if(contList != null){
          //  query += 'AND SBQQ__Quote__r.SBQQ__MasterContract__c IN: \'' + contList + '\'';
        //}
        
        quoteLineMap = new Map<Id, SBQQ__QuoteLine__c>((List<SBQQ__QuoteLine__c>)Database.query(query));

        List<SBQQ__Quote__c> qList = new List<SBQQ__Quote__c>();
        List<sObject> sObjectList = new List<sObject>();
        Set<Id> qlGroupIdSet = new Set<Id>();
        
        if(!quoteLineMap.isEmpty()){

            Savepoint sp = Database.setSavepoint();

            for(SBQQ__QuoteLine__c ql : quoteLineMap.values()){
                
                ql.SBQQ__PriorQuantity__c = ql.SBQQ__Quantity__c;

                if(ql.CustomMinMaxCalcType__c != null || ql.CustomMinMaxCalcType__c != ''){
                    //ql.SBQQ__Quote__r.JVCO_AmendmentReset__c = true;
                    ql.JVCO_PriorTotal__c = ql.SBQQ__RequiredBy__r.LimitDeltaTotal__c;
                }

                qlForUpdate.add(ql);

                SBQQ__Quote__c qRec = new SBQQ__Quote__c();
                qRec.Id = ql.SBQQ__Quote__c;
                qRec.JVCO_AmendmentReset__c = true;
                //qRec.Recalculate__c = !qRec.Recalculate__c;

                qlGroupIdSet.add(ql.SBQQ__Group__c);

                if(!qList.contains(qRec)){
                    qList.add(qRec);
                }
            }

            System.debug('BulkAmendmentReset qlForUpdate: ' + qlForUpdate);
            System.debug(!qlForUpdate.isEmpty());

            List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
            
            if(!qlForUpdate.isEmpty()){
                sObjectList.addAll(qlForUpdate);
            }
            if(!qList.isEmpty()){
                System.debug('qList Size: ' + qList.size());
                sObjectList.addAll(qList);
            }

            if(Test.isRunningTest() && testError){
                sObjectList.add(new Opportunity());
            }

            if(!sObjectList.isEmpty()){
                
                System.debug('Start Update');
                System.debug('BulkAmendmentReset sObjectList: ' + sObjectList);
                //Start - Error Log Handler 
                
                //Use of CPQ Disable Trigger to prevent queueable error
                SBQQ.TriggerControl.disable();

                List<Database.SaveResult> res = new List<Database.SaveResult>();
                res = Database.update(sObjectList, false);
                
                for(Integer i = 0; i < sObjectList.size(); i++){

                    Database.SaveResult sr = res[i];
                    sObject obj = sObjectList[i];
                    if(!sr.isSuccess()){
                        Database.rollback(sp);
                        System.debug('Update QL fail: ');
                        for(Database.Error objErr: sr.getErrors()){

                            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                            /*
                            errLog.Name = String.valueOf(obj.Id);
                            errLog.JVCO_FailedRecordObjectReference__c = String.valueOf(obj.Id);
                            errLog.blng__FullErrorLog__c = string.valueof(objErr.getMessage());
                            errLog.blng__ErrorCode__c = string.valueof(objErr.getStatusCode());
                            errLog.JVCO_ErrorBatchName__c = 'JVCO_BulkResetAmendment';
                            */

                            errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(obj.Id);
                            errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                            errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                            errLog.ffps_custRem__Message__c = 'JVCO_BulkResetAmendment';

                            errLogList.add(errLog);
                        }
                    }
                }
            }

            if(!qlGroupIdSet.isEmpty()){

                List<SBQQ__QuoteLineGroup__c> qlGListForUpdate = new List<SBQQ__QuoteLineGroup__c>();

                List<SBQQ__QuoteLineGroup__c> qlGroupList = [SELECT Id, SBQQ__ListTotal__c, SBQQ__CustomerTotal__c, SBQQ__NetTotal__c, SBQQ__Quote__r.SBQQ__NetAmount__c FROM SBQQ__QuoteLineGroup__c WHERE SBQQ__Quote__r.JVCO_AmendmentReset__c = true and SBQQ__Quote__r.SBQQ__Status__c = 'Draft' AND SBQQ__Quote__r.SBQQ__Type__c = 'Amendment' AND Id IN: qlGroupIdSet];

                if(Test.isRunningTest() && testError2){
                    qlGListForUpdate.add(new SBQQ__QuoteLineGroup__c());
                }

                if(!qlGroupList.isEmpty()){

                    for(SBQQ__QuoteLineGroup__c qlgRec : qlGroupList){

                        if(qlgRec.SBQQ__ListTotal__c != qlgRec.SBQQ__Quote__r.SBQQ__NetAmount__c || 
                            qlgRec.SBQQ__CustomerTotal__c != qlgRec.SBQQ__Quote__r.SBQQ__NetAmount__c ||
                            qlgRec.SBQQ__NetTotal__c != qlgRec.SBQQ__Quote__r.SBQQ__NetAmount__c)
                        {

                            qlgRec.SBQQ__ListTotal__c = qlgRec.SBQQ__Quote__r.SBQQ__NetAmount__c;
                            qlgRec.SBQQ__CustomerTotal__c = qlgRec.SBQQ__Quote__r.SBQQ__NetAmount__c;
                            qlgRec.SBQQ__NetTotal__c = qlgRec.SBQQ__Quote__r.SBQQ__NetAmount__c;

                            qlGListForUpdate.add(qlgRec);
                        }
                    }
                }

                if(!qlGListForUpdate.isEmpty()){

                    List<Database.SaveResult> res2 = new List<Database.SaveResult>();
                    res2 = Database.update(qlGListForUpdate, false);

                    for(Integer i = 0; i < qlGListForUpdate.size(); i++){

                    Database.SaveResult sr2 = res2[i];
                    sObject obj2 = qlGListForUpdate[i];
                    if(!sr2.isSuccess()){
                            Database.rollback(sp);
                            System.debug('Update QL fail: ');
                            for(Database.Error objErr: sr2.getErrors()){

                                ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();

                                /*
                                errLog.Name = String.valueOf(obj2.Id);
                                errLog.JVCO_FailedRecordObjectReference__c = String.valueOf(obj2.Id);
                                errLog.blng__FullErrorLog__c = string.valueof(objErr.getMessage());
                                errLog.blng__ErrorCode__c = string.valueof(objErr.getStatusCode());
                                errLog.JVCO_ErrorBatchName__c = 'JVCO_BulkResetAmendment : QLG Update';
                                */

                                errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(obj2.Id);
                                errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                                errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                                errLog.ffps_custRem__Message__c = 'JVCO_BulkResetAmendment : QLG Update';

                                errLogList.add(errLog);
                            }
                        }
                    }
                }
            }

            if(!errLogList.isEmpty()){
                try{
                    insert errLogList;
                }
                catch(Exception e){
                    System.debug('BulkResetAmendment Insert Error Log Error: ' + e);
                }
            }
        }
    }
}