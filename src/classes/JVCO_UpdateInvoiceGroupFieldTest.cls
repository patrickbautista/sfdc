@isTest
private class JVCO_UpdateInvoiceGroupFieldTest
{
    @testSetup static void createTestData() 
    {
        List<RecordType> rtypesCENLIH = [SELECT Name, Id FROM RecordType WHERE sObjectType='JVCO_CashEntryLineItemHistory__c' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> RecordTypesCENLIH  = new Map<String,String>{};
        for(RecordType rt: rtypesCENLIH)
        {
          RecordTypesCENLIH.put(rt.Name,rt.Id);
        }
        List<CashEntryLineItemHistory_CS__c> settingsCENLIH = new List<CashEntryLineItemHistory_CS__c>();
        settingsCENLIH.add(new CashEntryLineItemHistory_CS__c(Name = 'Matched', Value__c = RecordTypesCENLIH.get('Matched Cash')));
        settingsCENLIH.add(new CashEntryLineItemHistory_CS__c(Name = 'Reversed', Value__c = RecordTypesCENLIH.get('Reversed Cash')));
        insert settingsCENLIH;
        
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        //Settings
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_codaCreditNoteTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        insert dt;
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        c2g__codaDimension2__c dim2 = JVCO_TestClassHelper.setDim2('PPL');
        insert dim2;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        JVCO_SLA_Constants__c cs = JVCO_TestClassObjectBuilder.createSLAConstants();
        insert cs;

        Entitlement e = JVCO_TestClassObjectBuilder.createEntitlement(licAcc.id);
        insert e;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Test.startTest();
        List<c2g__codaInvoice__c> sInvList = new List<c2g__codaInvoice__c>();
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        sInvList.add(sInv);
        c2g__codaInvoice__c sInv2 = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        sInvList.add(sInv2);
        insert sInvList;
        
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv2.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);

        //Create Cash Entry
        c2g__codaCashEntry__c testCashEntry = new c2g__codaCashEntry__c();
        testCashEntry.c2g__PaymentMethod__c = 'Electronic';
        testCashEntry.c2g__OwnerCompany__c = comp.id;
        testCashEntry.ownerid = testGroup.Id;
        testCashEntry.c2g__Status__c = 'In Progress';
        testCashEntry.c2g__Date__c = Date.today();
        testCashEntry.c2g__Value__c = 100.00;
        insert testCashEntry;
        List<c2g__codaInvoice__c> invoiceList = [select id, Name from c2g__codaInvoice__c limit 2];
        //Create Cash Entry Line Item
        List<c2g__codaCashEntryLineItem__c> cashEntryLIList = new List<c2g__codaCashEntryLineItem__c>();
        c2g__codaCashEntryLineItem__c cashEntryLineItem1 = new c2g__codaCashEntryLineItem__c();
        cashEntryLineItem1.c2g__CashEntry__c = testCashEntry.id;
        cashEntryLineItem1.c2g__Account__c = licAcc.id;
        cashEntryLineItem1.c2g__CashEntryValue__c = 100;
        cashEntryLineItem1.c2g__LineNumber__c = 1;
        cashEntryLineItem1.c2g__AccountReference__c = invoiceList[0].Name;
        cashEntryLineItem1.c2g__AccountPaymentMethod__c = 'Cheque';
        cashEntryLineItem1.c2g__OwnerCompany__c = comp.id;
        cashEntryLineItem1.JVCO_Temp_AccountReference__c = 'SIN00001';
        cashEntryLineItem1.JVCO_CaseRecordType_Insolvency__c = '';
        cashEntryLIList.add(cashEntryLineItem1);

        c2g__codaCashEntryLineItem__c cashEntryLineItem2 = new c2g__codaCashEntryLineItem__c();
        cashEntryLineItem2.c2g__CashEntry__c = testCashEntry.id;
        cashEntryLineItem2.c2g__Account__c = licAcc.id;
        cashEntryLineItem2.c2g__CashEntryValue__c = 100;
        cashEntryLineItem2.c2g__LineNumber__c = 2;
        cashEntryLineItem2.c2g__AccountReference__c = invoiceList[1].Name;
        cashEntryLineItem2.c2g__AccountPaymentMethod__c = 'Cheque';
        cashEntryLineItem2.c2g__OwnerCompany__c = comp.id;
        cashEntryLineItem2.JVCO_Temp_AccountReference__c = 'SIN00002';
        cashEntryLineItem2.JVCO_CaseRecordType_Insolvency__c = '';
        cashEntryLIList.add(cashEntryLineItem2);
        insert cashEntryLIList;
        Test.stopTest();
        
        //Turn off Background Matching
        JVCO_BackgroundMatchingLogic.stopMatching = true;

        PAYACCVAL1__Bank_Account__c payBankAccount = new PAYACCVAL1__Bank_Account__c();
        payBankAccount.PAYACCVAL1__Bank_Name__c = 'test pay bank';
        payBankAccount.PAYFISH3__Originator__c = false;
        payBankAccount.PAYACCVAL1__Account_Number__c = '12345678';
        payBankAccount.PAYACCVAL1__Sort_Code__c = '123456';
        payBankAccount.PAYFISH3__Account_Name__c = 'test account name';
        insert payBankAccount;

        PAYREC2__Payment_Schedule__c paySchedule = new PAYREC2__Payment_Schedule__c();
        paySchedule.PAYFISH3__Bank_Account__c = payBankAccount.id;
        paySchedule.PAYREC2__Day__c = '1';
        paySchedule.PAYFISH3__First_Collection_Interval__c = 1;
        paySchedule.PAYREC2__Frequency__c = 'month';
        paySchedule.PAYFISH3__FTA_Sub_Type__c = 'Default';
        paySchedule.PAYFISH3__Fund_Transfer_Agent__c = 'Unknown';
        paySchedule.PAYREC2__Interval__c = 1;
        paySchedule.PAYREC2__Max__c = 10;
        paySchedule.PAYREC2__Type__c = 'Fixed';
        insert paySchedule;

        PAYREC2__Payment_Agreement__c paymentAgreement = new PAYREC2__Payment_Agreement__c();
        paymentAgreement.PAYREC2__Account__c = licAcc.Id;
        paymentAgreement.PAYFISH3__Current_Bank_Account__c = payBankAccount.id;
        paymentAgreement.PAYREC2__Ongoing_Collection_Amount__c = 100;
        paymentAgreement.PAYFISH3__Payee_Agreed__c = true;
        paymentAgreement.PAYREC2__Payment_Schedule__c = paySchedule.Id;
        paymentAgreement.JVCO_Temp_External_Id__c = 'PAY001';
        insert paymentAgreement;
        
        PAYREC2__Payment_Agreement__c paymentAgreement2 = new PAYREC2__Payment_Agreement__c();
        paymentAgreement2.PAYREC2__Account__c = licAcc.Id;
        paymentAgreement2.PAYFISH3__Current_Bank_Account__c = payBankAccount.id;
        paymentAgreement2.PAYREC2__Ongoing_Collection_Amount__c = 100;
        paymentAgreement2.PAYFISH3__Payee_Agreed__c = true;
        paymentAgreement2.PAYREC2__Payment_Schedule__c = paySchedule.Id;
        paymentAgreement2.JVCO_Temp_External_Id__c = 'PAY002';
        insert paymentAgreement2;

        //Get record type of Paymnet
        List<RecordType> rtypes1 = [SELECT Name, Id FROM RecordType WHERE sObjectType='PAYBASE2__Payment__c' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> paymentRecordTypes = new Map<String,String>{};
        for(RecordType rt1: rtypes1)
        {
          paymentRecordTypes.put(rt1.Name,rt1.Id);
        }

        PAYBASE2__Payment__c paybasePayment = new PAYBASE2__Payment__c();
        paybasePayment.PAYREC2__Payment_Agreement__c = paymentAgreement.id;
        paybasePayment.PAYFISH3__Type__c = 'Collection';
        paybasePayment.RecordTypeId = paymentRecordTypes.get('Direct Debit Collection');
        paybasePayment.Name = 'Test Payment 01';
        paybasePayment.Ownerid = testGroup.Id;
        paybasePayment.PAYBASE2__Is_Paid__c = true;
        paybasePayment.PAYBASE2__Amount_Refunded__c = 100;
        paybasePayment.PAYCP2__Is_Test__c = true;
        paybasePayment.PAYREC2__Is_Actual_Collection__c = true;
        paybasePayment.PAYSPCP1__Redirect_Parent__c= true;
        paybasePayment.PAYSPCP1__Gift_Aid__c= true;
        paybasePayment.PAYBASE2__Amount__c = 1250.00;
        paybasePayment.PAYCP2__Payment_Description__c = 'GIN-000044';
        paybasePayment.PAYBASE2__Status__c = 'Pending';
        insert paybasePayment;
        
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        List<c2g.CODAAPICommon.Reference> refList = new List<c2g.CODAAPICommon.Reference>();
        Double totalAmount = 0.0;
        c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
        reference.Id = sInv.Id;
        refList.add(reference);
        c2g.CODAAPISalesInvoice_10_0.BulkPostInvoice(context, refList);

    }

    @isTest
    static void itShould()
    {
        JVCO_Invoice_Group__c testInvoiceGroup = new JVCO_Invoice_Group__c();
        testInvoiceGroup.JVCO_Migrated_Invoice__c = 'SIN00001;SIN00001';
        testInvoiceGroup.JVCO_Payonomy_Payment_Agreement_Ext_Id__c = 'PAY001';
        testInvoiceGroup.JVCO_Total_Amount__c = 1000.00;
        insert testInvoiceGroup;
        
        JVCO_UpdateInvoiceGroupField obj = new JVCO_UpdateInvoiceGroupField();
        Database.executeBatch(obj);
    }
}