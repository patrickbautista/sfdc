/* --------------------------------------------------------------------------------------
Name: JVCO_LateDDIReminder
Description: Batch Class for sending out late DDI Reminders

Date            Version     Author              Summary of Changes 
-----------     -------     -----------------   -----------------------------------------
10-Sep-2020     0.1         Luke.Walker      Intial creation
29-Sep-2020     0.2         rhys.j.c.dela.cruz Adjustment of Query  
----------------------------------------------------------------------------------------- */

public class JVCO_LateDDIReminder implements Database.Batchable<sObject>{
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
		
        date d1 = system.today().addDays(-10);
        date d2 = d1.addDays(1);

        return Database.getQueryLocator([SELECT Id, 
                                         JVCO_Related_Account__c, 
                                         JVCO_Related_Account__r.JVCO_Billing_Contact__c, 
                                         JVCO_Related_Account__r.ffps_custRem__Preferred_Communication_Channel__c,
                                         JVCO_Related_Account__r.JVCO_Customer_Account__c 
                                         FROM JVCO_Document_Queue__c 
                                         WHERE JVCO_Scenario__c = 'Direct Debit Mandate' 
                                         AND JVCO_Generation_Status__c = 'Generated'
                                         AND CreatedDate >= :d1
                                         AND CreatedDate < :d2
                                         AND JVCO_Related_Account__r.JVCO_In_Enforcement__c = false 
                                         AND JVCO_Related_Account__r.JVCO_In_Infringement__c = false 
                                         AND JVCO_Related_Account__r.JVCO_Late_Direct_Debit_Last_Sent__c != TODAY
										 AND JVCO_Related_Account__c NOT IN (SELECT Account__c 
                                    										 FROM Income_Direct_Debit__c 
                                    										 WHERE CreatedDate = LAST_N_DAYS:10)]);
    }
    public void execute(Database.BatchableContext bc, List<JVCO_Document_Queue__c> scope){

        List<Account> AccountstoUpdate = new List<Account>();
        List<JVCO_Document_Queue__c> DocQueues = new List<JVCO_Document_Queue__c>();
        Map<Id, Account> accMap = new Map<Id, Account>();

        //For loop to dedupe in scenario of multiple accounts
        for(JVCO_Document_Queue__c docRec : scope){

            Account acc = new Account();
            acc.Id = docRec.JVCO_Related_Account__c;
            acc.JVCO_Customer_Account__c = docRec.JVCO_Related_Account__r.JVCO_Customer_Account__c;
            acc.JVCO_Billing_Contact__c = docRec.JVCO_Related_Account__r.JVCO_Billing_Contact__c;
            acc.ffps_custRem__Preferred_Communication_Channel__c = docRec.JVCO_Related_Account__r.ffps_custRem__Preferred_Communication_Channel__c;

            if(!accMap.containsKey(acc.Id)){

                accMap.put(acc.Id, acc);
            }
        }

        if(!accMap.isEmpty()){
			//loop through each account and add a new document queue
            for (Account account : accMap.values()) {
            
                JVCO_Document_Queue__c DocQueue = new JVCO_Document_Queue__c();
                DocQueue.JVCO_Format__c = 'PDF';
                DocQueue.JVCO_Generation_Status__c = 'To be Generated';
                DocQueue.JVCO_Recipient__c = account.JVCO_Billing_Contact__c;
                DocQueue.JVCO_Related_Account__c = account.id;
                DocQueue.JVCO_Related_Customer_Account__c = account.JVCO_Customer_Account__c;
                DocQueue.JVCO_Scenario__c = 'Late Direct Debit Instruction Return';
                DocQueue.Scenario_Category__c = 'Other';
                
                //Add letter specific fields to the new record if preference is print
                if(account.ffps_custRem__Preferred_Communication_Channel__c == 'Print'){
                    DocQueue.JVCO_Transmission__c = 'Letter';    
                    DocQueue.JVCO_Inclusions__c = 'BRE Envelope 1';
                    DocQueue.JVCO_Print_Status__c = 'For Printing';
                } else {
                    DocQueue.JVCO_Transmission__c = 'Email'; 
                }
                
                account.JVCO_Late_Direct_Debit_Last_Sent__c = system.today();
                
                DocQueues.Add(DocQueue);
                AccountstoUpdate.Add(Account);
            }
        }
        
        if(!DocQueues.isEmpty())
        {
            try{
                insert new List<JVCO_Document_Queue__c>(DocQueues);
                update AccountstoUpdate;
            }
            catch(Exception e){
                insert createErrorLog('Late DD', e.getMessage(), 'JVCO_LateDDIReminder');
            }
        }
        
    }

    @TestVisible
    private ffps_custRem__Custom_Log__c createErrorLog(String errCode, String errMessage, String batchName)
    {
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Grouping__c = errCode;
        errLog.ffps_custRem__Detail__c = errMessage;
        errLog.ffps_custRem__Message__c = batchName;
        return errLog;
    }    
    public void finish(Database.BatchableContext bc){}    
}