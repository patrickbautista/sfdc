/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_UpdateQuoteMaxQuoteLinesBatch.cls 
   Description:     Class that updates the field JVCO_Maximum_Number_of_Quote_Lines__c as per the custom setting
   Test class:      JVCO_UpdateQuoteMaxQuoteLinesBatchTest.cls 

   Date                 Version       Author                             Summary of Changes 
   -----------          -------     -----------------                  -------------------------------------------
  11-Sep-2017           0.1         Accenture-Rolando Valencia         Intial draft
------------------------------------------------------------------------------------------------ */

global class JVCO_UpdateQuoteMaxQuoteLinesBatch implements Database.Batchable<sObject>
{

    global Database.QueryLocator start (Database.BatchableContext BC)
    {
 
        String query = 'SELECT id, JVCO_Maximum_Number_of_Quote_Lines__c FROM SBQQ__Quote__c WHERE JVCO_Maximum_Number_of_Quote_Lines__c = NULL';
        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext bc, List<SBQQ__Quote__c> quoteList)
    {
        
        For(SBQQ__Quote__c qList: quoteList)
        {
            qList.JVCO_Maximum_Number_of_Quote_Lines__c = 200;
        }

        if(!quoteList.isempty())
        {   
            update quoteList;
        }

    }

    global void finish(Database.BatchableContext BC)
    {

    }

}