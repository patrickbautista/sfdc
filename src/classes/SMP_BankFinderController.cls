public with sharing class SMP_BankFinderController 
{   
    public SMP_BankFinderController (ApexPages.StandardController sc)
    {
        ddRecord = (Income_Direct_Debit__c) sc.getRecord();
    }

    public Income_Direct_Debit__c DDRecord{get;set;}

    public PageReference start()
    {
        System.Debug('get bankname first');

        String BankName = DDRecord.DD_Bank_Name__c;
        System.Debug('BankNamee =' + BankName);

        DateTime lastmod =  DDRecord.LastModifiedDate;

        System.Debug('lastmod =' + lastmod);

        if (BankName  == null || BankName == '' || lastmod > DateTime.Now().addDays(-1))
        {
            String newbankname = 'This is my Bank';
            DDRecord.DD_Bank_Name__c = newbankname ;


            System.Debug('about to get back details');

            getbankdetailsandupdate();
        }

        return null;
    }

    public void getbankdetailsandupdate()
    {
        System.debug('#### getBankDetailsAndUpdate');

        String sortcode = ddRecord.DD_Bank_Sort_Code__c;
        String accountnumber = ddRecord.DD_Bank_Account_Number__c;

        System.debug('#### sortcode: ' + sortcode);
        System.debug('#### accountnumber: ' + accountnumber);

        try
        {                       
            ddRecord.DD_Bank_Name__c = String.isBlank(ddRecord.DD_Bank_Name__c) ? 'Error' : ddRecord.DD_Bank_Name__c;

            SMP_DDServiceBankAccountModel bankAccount = SMP_DDServiceHandler.getBankDetails(ddRecord.DD_Bank_Sort_Code__c, ddRecord.DD_Bank_Account_Number__c);

            if(bankAccount != null)
            {
                bankAccount.copyFieldsToDirectDebitRecord(ddRecord);
            }

            update ddRecord;                       
        }        
        catch (Exception e)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.FATAL, 'There was an error trying to check bank details.'));
        }
    }
}