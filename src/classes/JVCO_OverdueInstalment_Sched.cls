/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_OverdueInstalment_Sched.cls 
   Description:     Schedulable Class for Batch JVCO_OverdueInstalment_Batch

   Date            Version     Author                             Summary of Changes 
   -----------     -------     -----------------                  -----------------------------------
   03-Feb-2017     0.1         kristoffer.d.martin    			  Intial draft       
  ------------------------------------------------------------------------------------------------ */
global class JVCO_OverdueInstalment_Sched implements Schedulable {
	global void execute(SchedulableContext sc) {
		Database.executeBatch(new JVCO_OverdueInstalment_Batch());
	}
}