/* ----------------------------------------------------------------------------------------------
    Name: JVCO_BillingLineUtil.cls 
    Description: Handler Class for Billing Invoice Line

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    18-Jul-2017   0.1         franz.g.a.dimaapi   Initial creation
    10-Dec-2017   0.2         franz.g.a.dimaapi   Removed Old Methods, Added New Methods
    23-Feb-2018   0.3         franz.g.a.dimaapi   Clean Class, Ignore Invoice From Scheduler - GREEN-30429
  ----------------------------------------------------------------------------------------------- */
public class JVCO_BillingUtil
{   
    /*private static List<blng__Invoice__c> bInvFromDatamigList = new List<blng__Invoice__c>();
    private static List<blng__Invoice__c> cInvFromDatamigList = new List<blng__Invoice__c>();
    private static List<blng__Invoice__c> singleOrderBlngInvList = new List<blng__Invoice__c>();
    private static List<blng__Invoice__c> singleOrderNegBlngInvList = new List<blng__Invoice__c>();
    private static List<blng__Invoice__c> manualBInvList = new List<blng__Invoice__c>();
    private static Map<Id, blng__Invoice__c> multipleSurchargeOrderBlngInvMap = new Map<Id, blng__Invoice__c>();
    private static Boolean flag = true;

    public static void setupBlngInvoice(List<blng__Invoice__c> bInvList)
    {
        if(flag)
        {
            for(blng__Invoice__c bInv : bInvList)
            {   
                //Manual Invoice
                if(bInv.JVCO_Manual_Invoice__c && bInv.JVCO_Sales_Invoice__c == null && 
                    bInv.JVCO_Sales_Credit_Note__c == null && bInv.blng__InvoiceStatus__c == 'Posted' && 
                    bInv.blng__NumberOfInvoiceLines__c > 0)
                {
                    manualBInvList.add(bInv);
                //Positive Invoice
                }else if(bInv.JVCO_Total_Amount__c >= 0 && bInv.JVCO_Sales_Invoice__c == null)
                {   
                    setInvoiceGenerationType(bInv, false);
                //NegativeInvoice
                }else if(bInv.JVCO_Total_Amount__c < 0 && bInv.JVCO_Sales_Credit_Note__c == null)
                {
                    setInvoiceGenerationType(bInv, true);
                }
            }
            callInvAndCNoteGenerationFunction();
        }    
    }

    private static void setInvoiceGenerationType(blng__Invoice__c bInv, Boolean isNegative)
    {
        //Single Order
        if(bInv.blng__Order__c != null)
        {
            fromSingleOrder(bInv, isNegative);
        //Migrated
        }else if(bInv.blng__Order__c == null && bInv.blng__InvoiceStatus__c == 'Posted' &&
            (bInv.JVCO_Migrated__c || bInv.JVCO_Migrated_Original_Invoice__c))
        {
            fromMigrated(bInv, isNegative);
        }
    }

    private static void fromSingleOrder(blng__Invoice__c bInv, Boolean isNegative)
    {
        //Single Order
        if(!bInv.JVCO_Has_Order_Group__c && (bInv.blng__InvoiceStatus__c == 'Draft' || 
            (bInv.JVCO_Invoice_Type__c == 'Surcharge' && bInv.blng__InvoiceStatus__c == 'Posted')))
        {
            if(!isNegative && bInv.JVCO_Processed_By_BillNow__c && bInv.blng__InvoiceRunCreatedBy__c == null)
            {
                singleOrderBlngInvList.add(bInv);
            }else if(isNegative && bInv.JVCO_Processed_By_BillNow__c && bInv.blng__InvoiceRunCreatedBy__c == null)
            {
                singleOrderNegBlngInvList.add(bInv);
            }
        //Multiple Order Surcharge
        }else
        {
            if(bInv.JVCO_Invoice_Type__c == 'Surcharge' && bInv.blng__InvoiceStatus__c == 'Posted')
            {
                multipleSurchargeOrderBlngInvMap.put(bInv.id, bInv);
            }
        }
    }

    private static void fromMigrated(blng__Invoice__c bInv, Boolean isNegative)
    { 
        if(!isNegative)
        {
            bInvFromDatamigList.add(bInv);
        }else
        {
            cInvFromDatamigList.add(bInv);
        }
    }

    private static void callInvAndCNoteGenerationFunction()
    {   
        //Migrated Invoice
        if(!bInvFromDatamigList.isEmpty())
        {
            flag = FALSE;
            System.debug('Retrieved Billing Invoice from Datamig...');
            JVCO_InvoiceGenerationLogic.generateInvoiceFromDatamig(bInvFromDatamigList);
        }
        //Migrated CreditNote
        if(!cInvFromDatamigList.isEmpty())
        {
            flag = FALSE;
            System.debug('Retrieved Credit Invoice from Datamig...');
            JVCO_CreditNoteGenerationLogic.generateCreditnoteFromDatamig(cInvFromDatamigList);
        }
        //Single Order
        if(!singleOrderBlngInvList.isEmpty())
        {
            flag = FALSE;
            System.debug('Generating Invoice from single order...');
            JVCO_InvoiceGenerationLogic.generateInvoiceFromSingleOrder(singleOrderBlngInvList);
        }
        //Single Order CreditNote
        if(!singleOrderNegBlngInvList.isEmpty())
        {
            flag = FALSE;
            System.debug('Generating CreditNote from single order...');
            JVCO_CreditNoteGenerationLogic.generateCreditNote(singleOrderNegBlngInvList);
        }
        //Multiple Order Surcharge
        if(!multipleSurchargeOrderBlngInvMap.isEmpty())
        {
            flag = FALSE;
            System.debug('Generating Surcharge Invoice from order group...');
            JVCO_InvoiceGenerationLogic.generateSurchargeInvoiceFromOrderGroup(multipleSurchargeOrderBlngInvMap);
        }
        //Manual Invoice
        if(!manualBInvList.isEmpty())
        {
            flag = FALSE;
            //JVCO_InvoiceAndCreditNoteManualLogic.setupInvoiceAndCreditNoteHeader(manualBInvList);
        }
    }*/

}