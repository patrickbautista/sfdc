/* ----------------------------------------------------------------------------------------------
   Name: JVCO_ContractTriggerHandlerTest
   Description: Test Class of Contract Trigger Handler

   Date         Version     Author                          Summary of Changes 
   -----------  -------     -----------------               -----------------------------------------
  05-Jul-2017   0.1         Accenture-erica.jane.w.matias   Intial creation
  25-Oct-2017   0.2         Accenture-r.p.valencia.iii      added test method testcopyQuotePriceBook
  
  ----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_ContractTriggerHandlerTest
{
    @testSetup
    private static void testSetup(){
            Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, null, null, null);
        insert q;

        SBQQ__Quote__c q2 = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, null, null, null);
        q2.SBQQ__PriceBook__c = NULL;
        insert q2;
        Test.startTest();
        Contract c = JVCO_TestClassObjectBuilder.createContract(acc2.id, q.id);
        insert c;

        Contract c2 = JVCO_TestClassObjectBuilder.createContract(acc2.id, q.id);
        insert c2;

        Contract c3 = JVCO_TestClassObjectBuilder.createContract(acc2.id, q2.id);
        insert c3;

        Contract c4 = JVCO_TestClassObjectBuilder.createContract(acc2.id, q2.id);
        insert c4;
        Test.stopTest();

        JVCO_HistoricCurrentValuesSetting__c settings = new JVCO_HistoricCurrentValuesSetting__c();
        settings.JVCO_DaysCovered__c = 7;
        settings.JVCO_EnableInitialDataUpdate__c = false;
        settings.JVCO_AccountRecordTypeName__c = 'Licence Account';
        insert settings;
    }

    @isTest
    static void itShould()
    {
        List<Contract> c =  [SELECT Id
                             FROM Contract
                             order by CreatedDate DESC limit 1];
        System.debug('Contract Result -' + c);
        System.assert(!c.isEmpty());
    }

    @isTest
    static void testcopyQuotePriceBook()
    {
        List<Contract> c3 =  [SELECT Id, Pricebook2Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__PriceBook__c
                              FROM Contract
                              WHERE SBQQ__Quote__r.SBQQ__PriceBook__c != NULL
                              order by CreatedDate DESC limit 1];
        JVCO_ContractTriggerHandler.copyQuotePriceBook(c3);
        System.assertEquals(c3[0].SBQQ__Quote__r.SBQQ__PriceBook__c, c3[0].Pricebook2Id);
        System.debug('@@@ c3' + c3);
    }

  @isTest
  static void itShouldAfterUpdate()
  {

    List<Contract> c =  [SELECT Id, AccountId, SBQQ__Quote__c, JVCO_Contract_Temp_External_ID__c 
                         FROM Contract
                         order by CreatedDate DESC limit 1];
    JVCO_ContractTriggerHandler.onAfterInsert(c);
    //System.debug('Contract Result -' + c);
    //System.assert(!c.isEmpty());
  }

   @isTest
  static void itShouldBeforeDelete()
  {

    /*
    List<Contract> c =  [select Id, SBQQ__Quote__c from Contract order by CreatedDate DESC limit 1];
    Delete c;
    System.debug('itShouldBeforeDelete Contract Result -' + c);
    */
    
    map<id, Contract> c = new map<id, Contract> ([SELECT Id, SBQQ__Quote__c 
                                                  FROM Contract
                                                  order by CreatedDate DESC limit 1]);

    JVCO_ContractTriggerHandler.onBeforeDelete(c);
    
    //System.debug('Contract Result -' + c);
    //System.assert(!c.isEmpty());
  }
  
    @isTest
    static void itShouldBeforeDeleteTrigger(){
        
        List<Contract> c =  [SELECT Id, SBQQ__Quote__c
                             FROM Contract 
                             order by CreatedDate DESC limit 1];
        try{
            Delete c;
        }catch(Exception e){
            System.debug(e);
        }
        System.debug('itShouldBeforeDeleteTrigger Contract Result -' + c);

        //System.debug('Contract Result -' + c);
        //System.assert(!c.isEmpty());
    }
    
    @isTest
    static void testEmptyQuoteLookup()
    {
        Contract updateContractTest = [SELECT Id FROM Contract order by CreatedDate DESC limit 1];
        updateContractTest.Name = 'UpdateTest';
        update updateContractTest;

        Account licAcc = [SELECT Id FROM Account WHERE RecordType.Name = 'Licence Account' order by CreatedDate DESC limit 1];
        
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c order by CreatedDate DESC limit 1];

        List<Contract> contractWithoutQuoteLookupList = new List<Contract>();
        for(Integer i = 0; i < 2; i++)
        {
            contractWithoutQuoteLookupList.add(JVCO_TestClassObjectBuilder.createContract(licAcc.id, null));
        }

        try
        {
            insert contractWithoutQuoteLookupList;
        }
        catch(Exception e)
        {
            System.debug(e);
        }
    }

    @isTest
    static void testStampCurrentValues()
    {
        String LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Account accLicence = [Select Id From Account where RecordTypeId = :LicenceRT limit 1];
        accLicence.JVCO_Review_Frequency__c = 'Bi-annually';
        update accLicence;
        
        List<Contract> contList = [SELECT Id, AccountId, StartDate, EndDate FROM Contract WHERE AccountId =:accLicence.Id order by CreatedDate DESC limit 3];

        Integer startDate = -182;
        Integer endDate = 1;
        for(Contract cont : contList)
        {
            date sd = date.today().addDays(startDate);
            date ed = date.today().addDays(endDate);
            cont.StartDate = sd;
            cont.EndDate = ed;
            cont.AccountId = accLicence.Id;
            cont.JVCO_RenewableQuantity__c = 60;
            startDate--;
            endDate++;
        }

        update contList;

        Product2 proRecPRS = JVCO_TestClassObjectBuilder.createProduct();
        proRecPRS.JVCO_Society__c = 'PRS';
        insert proRecPRS; 

        Product2 proRecPPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecPPL.JVCO_Society__c = 'PPL';
        insert proRecPPL;

        Product2 proRecVPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecVPL.JVCO_Society__c = 'VPL';
        insert proRecVPL;

        List<SBQQ__Subscription__c> subList = new List<SBQQ__Subscription__c>();

        for(Contract cont : contList)
        {
            SBQQ__Subscription__c subsPRS = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecPRS.Id);
            subsPRS.SBQQ__Contract__c = cont.Id;
            subsPRS.SBQQ__ListPrice__c = 10;
            subsPRS.SBQQ__RenewalQuantity__c = 10;
            subsPRS.SBQQ__Quantity__c = 10;
            subsPRS.SBQQ__NetPrice__c = 10;
            subsPRS.SBQQ__TerminatedDate__c = null;
            subsPRS.Start_Date__c = cont.StartDate;
            subsPRS.End_Date__c = cont.EndDate;
            subsPRS.SBQQ__SubscriptionStartDate__c = cont.StartDate;
            subsPRS.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsPRS.ChargeYear__c = 'Current Year';
            subList.add(subsPRS);

            SBQQ__Subscription__c subsPPL = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecPPL.Id);
            subsPPL.SBQQ__Contract__c = cont.Id;
            subsPPL.SBQQ__ListPrice__c = 10;
            subsPPL.SBQQ__RenewalQuantity__c = 20;
            subsPPL.SBQQ__Quantity__c = 20;
            subsPPL.SBQQ__NetPrice__c = 10;
            subsPPL.SBQQ__TerminatedDate__c = null;
            subsPPL.Start_Date__c = cont.StartDate;
            subsPPL.End_Date__c = cont.EndDate;
            subsPPL.SBQQ__SubscriptionStartDate__c =cont.StartDate;
            subsPPL.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsPPL.ChargeYear__c = 'Current Year';
            subList.add(subsPPL);

             SBQQ__Subscription__c subsVPL = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecVPL.Id);
            subsVPL.SBQQ__Contract__c = cont.Id;
            subsVPL.SBQQ__ListPrice__c = 10;
            subsVPL.SBQQ__RenewalQuantity__c = 30;
            subsVPL.SBQQ__Quantity__c = 30;
            subsVPL.SBQQ__NetPrice__c = 10;
            subsVPL.SBQQ__TerminatedDate__c = null;
            subsVPL.Start_Date__c = cont.StartDate;
            subsVPL.End_Date__c = cont.EndDate;
            subsVPL.SBQQ__SubscriptionStartDate__c =cont.StartDate;
            subsVPL.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsVPL.ChargeYear__c = 'Current Year';
            subList.add(subsVPL);
        }

        Test.startTest();
        
        insert subList;

        Test.stopTest();


        Account accLicence1 = [Select Id, JVCO_PRS_Current_Value_Total__c, JVCO_PPL_Current_Value_Total__c, JVCO_VPL_Current_Value_Total__c From Account where RecordTypeId = :LicenceRT limit 1];

        system.assertEquals(300,accLicence1.JVCO_PRS_Current_Value_Total__c);
        system.assertEquals(600,accLicence1.JVCO_PPL_Current_Value_Total__c);
        system.assertEquals(900,accLicence1.JVCO_VPL_Current_Value_Total__c);

    }

    @isTest
    static void countRenewal()
    {
        Test.startTest();
        Account licAcc = [SELECT Id FROM Account WHERE RecordType.Name = 'Licence Account' order by CreatedDate DESC limit 1];   
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__Type__c FROM SBQQ__Quote__c order by CreatedDate DESC limit 1];
        quote.SBQQ__Type__c = 'Renewal';
        update quote;
        
        Contract contRec = new Contract(Accountid = licAcc.id, SBQQ__Quote__c = quote.id, Status = 'Draft', StartDate = date.today(), EndDate = date.today().addDays(60));
        insert contRec;
        Test.stopTest();

        Account checkLicAcc = [SELECT Id, No_of_Renewal_Contracts__c FROM Account WHERE id =:licAcc.id];
        system.assertNotEquals(checkLicAcc.No_of_Renewal_Contracts__c,0);
        
    }

    @isTest
    static void setTransitionRenewalQuoteTest()
    {
        String LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
        Account acc = [select Id, Name, JVCO_Customer_Account__r.Type from Account where RecordTypeId = :LicenceRT and Type = 'Customer' limit 1];

        acc.JVCO_Renewal_Scenario__c = 'PRS First';
        acc.JVCO_Review_Type__c = 'Manual';
        acc.JVCO_Transition_Status__c = 'Awaiting PRS Renewal';
        acc.JVCO_Non_Auto_Renewable_Product_Count__c = 0;
        acc.JVCO_Live__c = false;
        update acc;

        Contract cont1Rec = new Contract();
        cont1Rec.AccountId = acc.Id;
        cont1Rec.Status = 'Draft';
        cont1Rec.StartDate = Date.today();
        cont1Rec.ContractTerm = 12;
        cont1Rec.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
        cont1Rec.SBQQ__RenewalQuoted__c = true;
        cont1Rec.JVCO_Contract_Temp_External_ID__c = 'PRS';
        insert cont1Rec;

        SBQQ__Quote__c quoteRec = [Select Id, JVCO_Quote_Auto_Renewed__c, SBQQ__Type__c from SBQQ__Quote__c where SBQQ__PriceBook__c != null Limit 1];
        quoteRec.JVCO_Quote_Auto_Renewed__c = false;
        quoteRec.SBQQ__Type__c = 'Renewal';
        update quoteRec;

        Test.startTest();

        Opportunity oppRec = JVCO_TestClassObjectBuilder.createOpportunity(acc);
        oppRec.SBQQ__PrimaryQuote__c = quoteRec.Id;
        oppRec.SBQQ__RenewedContract__c = cont1Rec.Id;
        oppRec.StageName = 'Closed Won';
        oppRec.Probability = 100;
        oppRec.SBQQ__Contracted__c = true;
        insert oppRec;

        quoteRec.SBQQ__Opportunity2__c = oppRec.Id;
        update quoteRec;

        cont1Rec.SBQQ__RenewalOpportunity__c = oppRec.Id;
        update cont1Rec;

        Contract cont2Rec = new Contract();
        cont2Rec.AccountId = acc.Id;
        cont2Rec.Status = 'Draft';
        cont2Rec.StartDate = Date.today();
        cont2Rec.ContractTerm = 12;
        cont2Rec.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
        cont2Rec.SBQQ__RenewalQuoted__c = false;
        cont2Rec.SBQQ__Opportunity__c = oppRec.Id;
        cont2Rec.SBQQ__Quote__c = quoteRec.id;
        insert cont2Rec;

        Test.stopTest();

        Account acntCheck = [Select Id, JVCO_Review_Type__c, JVCO_Transition_Status__c From Account where Id =: acc.Id];
        system.assertEquals('Automatic', acntCheck.JVCO_Review_Type__c, 'Error: JVCO_Review_Type__c not set to Automatic');
        system.assertEquals('PRS Renewal Completed', acntCheck.JVCO_Transition_Status__c, 'Error: JVCO_Transition_Status__c not set to PRS Renewal Completed');

    }
}
