@isTest
private class JVCO_CalculatePriceTest
{
     @testSetup
    private static void testSetup()
    {
        JVCO_TestClassHelper.createBillingConfig();

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 

        System.runAs(new User(Id=UserInfo.getUserId())){
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        insert dt;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;

        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        OrderItem oi1 = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi1; 

    }

    @isTest
    static void orderPriceCalcFailedTest(){
        
        List<Order> testOrder = [SELECT Id, OrderNumber, SBQQ__PriceCalcStatus__c FROM Order order by CreatedDate DESC limit 1];

        for(Order o : testOrder)
        {
            o.SBQQ__PriceCalcStatus__c = 'Failed';          
        }

        //update testOrder; 

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(testOrder);
        sc.setSelected(testOrder);
        JVCO_CalculatePrice ctrl = new JVCO_CalculatePrice(sc);
        
        Test.startTest();
        ctrl.init();
        ctrl.returnToAccount();
        Test.stopTest(); 

       // PageReference pageRef = Page.YourVFPageName;
       // Test.setCurrentPage(pageRef);)

    }

    @isTest
    static void orderPriceCalcNoSelectTest(){
        
        List<Order> testOrder = [SELECT Id, OrderNumber, SBQQ__PriceCalcStatus__c FROM Order order by CreatedDate DESC limit 1];

        for(Order o : testOrder)
        {
            o.SBQQ__PriceCalcStatus__c = 'Failed';          
        }

        //update testOrder; 

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(testOrder);
        JVCO_CalculatePrice ctrl = new JVCO_CalculatePrice(sc);
        
        Test.startTest();
        ctrl.init();
        ctrl.returnToAccount();
        Test.stopTest(); 

       // PageReference pageRef = Page.YourVFPageName;
       // Test.setCurrentPage(pageRef);)

    }
}