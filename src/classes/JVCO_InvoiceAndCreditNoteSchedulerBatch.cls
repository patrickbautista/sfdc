/* ----------------------------------------------------------------------------------------------
    Name: JVCO_InvoiceAndCreditNoteSchedulerBatch
    TestClass: JVCO_InvoiceAndCreditNoteSchedBatchTest
    Description: Batch for SalesInvoice And CreditNote Order Group generation

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    16-Apr-2021  0.1         patrick.t.bautista    Intial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_InvoiceAndCreditNoteSchedulerBatch implements Database.Batchable<sObject>, Database.Stateful
{
    private Set<Id> orderGroupIdSet = new Set<Id>();
    private Boolean isManualProcess = false;
    private Set<Id> orderIdSet = new Set<Id>();
    private Set<Id> accountIdSet = new Set<Id>();
    private List<orderWrapper> orderWrapperStatefulList = new List<orderWrapper>();
    
    public JVCO_InvoiceAndCreditNoteSchedulerBatch(){}
    
    public JVCO_InvoiceAndCreditNoteSchedulerBatch(Set<Id> orderIdSet){
        this.isManualProcess = true;
        this.orderIdSet = orderIdSet;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        if(isManualProcess){
            return Database.getQueryLocator([SELECT Id, AccountId,
                                             JVCO_Order_Group__c,
                                             SBQQ__Quote__r.JVCO_Payment_Terms__c,
                                             SBQQ__Quote__r.JVCO_Quote_Id_to_Scheduler__c,
                                             JVCO_Processed_By_Scheduler__c
                                             FROM Order
                                             WHERE Id IN : orderIdSet
                                             AND JVCO_Processed_By_BillNow__c = false
                                             AND Status = 'Activated'
                                             AND SBQQ__Quote__c != null
                                             AND JVCO_Cancelled__c = false
                                             AND JVCO_Sales_Invoice__c = null
                                             AND JVCO_Sales_Credit_Note__c = null
                                             AND JVCO_Processed_By_Scheduler__c = false
                                             AND JVCO_Number_of_Order_Products__c != 0
                                            ]);
        }else{
            return Database.getQueryLocator([SELECT Id, AccountId,
                                             JVCO_Order_Group__c,
                                             SBQQ__Quote__r.JVCO_Payment_Terms__c,
                                             SBQQ__Quote__r.JVCO_Quote_Id_to_Scheduler__c,
                                             JVCO_Processed_By_Scheduler__c
                                             FROM Order
                                             WHERE AccountId != null
                                             AND JVCO_Processed_By_BillNow__c = false
                                             AND Status = 'Activated'
                                             AND SBQQ__Quote__c != null
                                             AND JVCO_Cancelled__c = false
                                             AND JVCO_Sales_Invoice__c = null
                                             AND JVCO_Sales_Credit_Note__c = null
                                             AND JVCO_Processed_By_Scheduler__c = false
                                             AND JVCO_Number_of_Order_Products__c != 0
                                            ]);
        }
    }
    
    
    public void execute(Database.BatchableContext bc, List<Order> scope)
    {
        Set<Id> tempAccountIdSet = new Set<Id>();
        Map<Id, List<Order>> tempAccountIdWithOderGrpMap = new Map<Id, List<Order>>();
        List<orderWrapper> orderWrapperList = new List<orderWrapper>();
        
        for(Order ord : scope){
            if(!accountIdSet.contains(ord.AccountId)){
                accountIdSet.add(ord.AccountId);
                tempAccountIdSet.add(ord.AccountId);
            }
            else if(accountIdSet.contains(ord.AccountId)){
                if(!tempAccountIdWithOderGrpMap.containsKey(ord.AccountId))
                    tempAccountIdWithOderGrpMap.put(ord.AccountId, new List<Order>());
                if(tempAccountIdWithOderGrpMap.containsKey(ord.AccountId))
                    tempAccountIdWithOderGrpMap.get(ord.AccountId).add(ord);
            }
        }
        
        if(!tempAccountIdSet.isEmpty())
            orderWrapperList = aggregateOrderAndCreateOrderGroup(tempAccountIdSet);
        
        if(!orderWrapperList.isEmpty())
            associateOrderGroupToOrder(scope, orderWrapperList);      
        
        if(!tempAccountIdWithOderGrpMap.isEmpty()){
            associateExistingOrderGroupToOrder(tempAccountIdWithOderGrpMap);
        }
    }
        
    public List<orderWrapper> aggregateOrderAndCreateOrderGroup(Set<Id> tempAccountIdSet){
        
        List<JVCO_Order_Group__c> orderGroupList = new List<JVCO_Order_Group__c>();
        List<orderWrapper> orderWrapperList = new List<orderWrapper>();
        
        for(AggregateResult ar : [SELECT AccountId accId,
                                  SBQQ__Quote__r.JVCO_Payment_Terms__c paymentTerm,
                                  SBQQ__Quote__r.JVCO_Quote_Id_to_Scheduler__c creditReason
                                  FROM Order
                                  WHERE Status = 'Activated'
                                  AND JVCO_Processed_By_BillNow__c = false
                                  AND JVCO_Cancelled__c = false
                                  AND AccountId IN : tempAccountIdSet
                                  AND SBQQ__Quote__c != null
                                  AND JVCO_Processed_By_Scheduler__c = false
                                  AND JVCO_Sales_Invoice__c = null
                                  AND JVCO_Sales_Credit_Note__c = null
                                  AND JVCO_Number_of_Order_Products__c != 0
                                  GROUP BY AccountId, SBQQ__Quote__r.JVCO_Payment_Terms__c,
                                  SBQQ__Quote__r.JVCO_Quote_Id_to_Scheduler__c])
        {            
            JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
            orderGroup.JVCO_Group_Order_Account__c = (Id)ar.get('accId');
            orderGroupList.add(orderGroup);
            
            orderWrapper ordWrap = new orderWrapper();
            ordWrap.orderGroup = orderGroup;
            ordWrap.accountId = (Id)ar.get('accId');
            ordWrap.paymentTerm = (String)ar.get('paymentTerm');
            ordWrap.creditReason = (String)ar.get('creditReason');
            orderWrapperList.add(ordWrap);
            orderWrapperStatefulList.add(ordWrap);
        }
        
        insert orderGroupList;
        
        return orderWrapperList;
    }
    
    public void associateOrderGroupToOrder(List<Order> orderList, List<orderWrapper> orderWrapperList){
        
        List<Order> orderToUpdateList = new List<Order>();
        
        for(Order ord : orderList)
        {
            for(orderWrapper ordWrap : orderWrapperList){
                if(ord.accountId == ordWrap.AccountId 
                   && ord.SBQQ__Quote__r.JVCO_Payment_Terms__c == ordWrap.paymentTerm
                   && ord.SBQQ__Quote__r.JVCO_Quote_Id_to_Scheduler__c == ordWrap.creditReason)
                {
                    ord.JVCO_Order_Group__c = ordWrap.orderGroup.Id;
                    orderGroupIdSet.add(ord.JVCO_Order_Group__c);
                    ord.JVCO_Processed_By_Scheduler__c = isManualProcess ? false : true;
                    orderToUpdateList.add(ord);
                }
            }
        }
        if(!orderToUpdateList.isEmpty())
            update orderList;
    }
    
    public void associateExistingOrderGroupToOrder(Map<Id, List<Order>> tempAccountIdWithOderGrpMap){
        
        List<Order> orderToUpdateList = new List<Order>();
        
        for(Id acctId : tempAccountIdWithOderGrpMap.keySet())
        {
            for(Order ord : tempAccountIdWithOderGrpMap.get(acctId))
            {
                for(orderWrapper ordWrap : orderWrapperStatefulList){
                    if(ord.accountId == ordWrap.AccountId 
                       && ord.SBQQ__Quote__r.JVCO_Payment_Terms__c == ordWrap.paymentTerm
                       && ord.SBQQ__Quote__r.JVCO_Quote_Id_to_Scheduler__c == ordWrap.creditReason)
                    {
                        ord.JVCO_Order_Group__c = ordWrap.orderGroup.Id;
                        orderGroupIdSet.add(ord.JVCO_Order_Group__c);
                        ord.JVCO_Processed_By_Scheduler__c = isManualProcess ? false : true;
                        orderToUpdateList.add(ord);
                    }
                }
            }
        }
        
        if(!orderToUpdateList.isEmpty())
            update orderToUpdateList;
    }
    
    public class orderWrapper{
        Id accountId;
        String paymentTerm;
        String creditReason;
        JVCO_Order_Group__c orderGroup;
    }
    
    public void finish(Database.BatchableContext bc)
    {
        if(!orderGroupIdSet.isEmpty()){
            final JVCO_General_Settings__c GeneralSettings = JVCO_General_Settings__c.getInstance();
            final Decimal INVOICESCHEDSCOPE = GeneralSettings != null ? GeneralSettings.JVCO_Invoice_Schedule_Scope__c : 10;
            Id jobId = Database.executeBatch(new JVCO_OrderMultipleBillNowLogicBatch(orderGroupIdSet),Integer.valueOf(INVOICESCHEDSCOPE));
        }
    }
}