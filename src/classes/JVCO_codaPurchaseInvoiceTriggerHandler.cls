/* ----------------------------------------------------------------------------------------------------------
    Name:            JVCO_codaPurchaseInvoiceTriggerHandler.cls 
    Description:     Trigger handler for JVCO_codaPurchaseInvoiceTrigger.trigger
    Test class:      

    Date             Version     Author                            Summary of Changes 
    -----------      -------     -----------------                 -------------------------------------------
    24-Oct-2017      0.1         Accenture-jizzle.a.a.camagong     Initial version of the code. GREEN-24714
    26-Oct_2017      0.2         Accenture-r.p.valencia.iii        Refactored the trigger, introduced the method setHoldStatus
    19-Nov-2017      0.3         Accenture - michael.alamag        Remove afterInsert and repace afterUpdate - GREEN-25795
    23-Feb-2018      0.4         franz.g.a.dimaapi                 Refactor Class and fixed - GREEN-30321
---------------------------------------------------------------------------------------------------------- */
public class JVCO_codaPurchaseInvoiceTriggerHandler {
  
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_codaPurchaseInvoiceTrigger__c : false;

    public static void afterUpdate (Map<Id, c2g__codaPurchaseInvoice__c> newMap, Map<Id, c2g__codaPurchaseInvoice__c> oldMap)
    {
        if(!skipTrigger) 
        {
            setHoldStatus(newMap, oldMap);
        }
    }

    private static void setHoldStatus(Map<Id, c2g__codaPurchaseInvoice__c> pInvNewMap, Map<Id, c2g__codaPurchaseInvoice__c> pInvOldMap)
    {
        Map<Id, Id> pInvToAccIdMap = new Map<Id, Id>();

        for(Id pInvId: pInvNewMap.keySet())
        {
            if(pInvNewMap.get(pInvId).c2g__InvoiceStatus__c == 'Complete' && pInvOldMap.get(pInvId).c2g__InvoiceStatus__c == 'In Progress')
            {
                pInvToAccIdMap.put(pInvId, pInvNewMap.get(pInvId).c2g__Account__c); 
            }    
        }

        if(!pInvToAccIdMap.isEmpty())
        {   
            Map<Id, Account> accMap = new Map<Id, Account>([SELECT Id, c2g__CODACreditStatus__c
                                                            FROM Account 
                                                            WHERE Id IN :pInvToAccIdMap.values() 
                                                            AND RecordType.Name = 'Supplier']);
            if(!accMap.isEmpty())
            {
                List<c2g.CODAAPICommon.Reference> refList = new List<c2g.CODAAPICommon.Reference>();
                for(Id pInvId: pInvToAccIdMap.keySet())
                {
                    Id accId = pInvToAccIdMap.get(pInvId);
                    if(accMap.get(accId).c2g__CODACreditStatus__c == 'On Hold')
                    {
                        c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
                        ref.Id = pInvId;
                        refList.add(ref);
                    }
                }
                if(!refList.isEmpty())
                {
                    c2g.CODAAPICommon_9_0.Context context = new c2g.CODAAPICommon_9_0.Context();
                    c2g.CODAAPIPurchaseInvoice_9_0.BulkPlaceOnHold(context, refList);
                }
            }    
        }
    }
}