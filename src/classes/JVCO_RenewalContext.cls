public class JVCO_RenewalContext implements Queueable  {
  
  RenewalContracts rc = null;
  public static boolean validateQuoteLineLimit = true;
  
  public JVCO_RenewalContext(Id masterContract, Contract[] contractsList)
  {
    List<Contract> conCheck = [Select id, Account.Type, SBQQ__AmendmentRenewalBehavior__c  from Contract where id = :masterContract];
    List<Contract> conUpdate = new List<Contract>();
    system.debug('Check Contract ' + conCheck);

    for(Contract c : conCheck)
    {
      if(c.Account.type == 'Key Account')
      {
        system.debug('Test if Key Accounts  ' + c.Account.Type);
        c.SBQQ__AmendmentRenewalBehavior__c = 'Earliest End Date';
        conUpdate.add(c);
      } 
    }

    if(!conUpdate.isEmpty())
    {
      update conUpdate;
    }



    rc = new RenewalContracts();
    rc.masterContractId = masterContract;
    rc.renewedContracts = contractsList;
  }

	public void execute(QueueableContext context) {
		validateQuoteLineLimit = false;
		SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractRenewer', rc.masterContractId, JSON.serialize(rc)); 
    }


	private class RenewalContracts
	{
		public Id masterContractId; 
		public Contract[] renewedContracts; 
	}

}