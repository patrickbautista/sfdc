@isTest
public class JVCO_CompleteRenewalQuoteSchedulerTest{
    @testSetup
    static void testData(){
        
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();  
        insert qProcess;

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Account acc =  JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        //acc2.JVCO_Renewal_Scenario__c = 'PPL First: Greater Than 60 Days';
        insert acc2;

        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_Closure_Reason__c = '';
        aff.JVCO_End_Date__c = null;
        insert aff;

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        insert opp;
		
        
        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
        insert q;
        
        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c order by CreatedDate DESC limit 1];
		
        SBQQ.TriggerControl.disable();
        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, prod.Id);
        insert ql;
        
        q.SBQQ__Type__c = 'Renewal';
        q.JVCO_Quote_Auto_Renewed__c = false;
        q.SBQQ__Status__c = 'Draft';
        update q;
		SBQQ.TriggerControl.enable();
        
        List<JVCO_Renewal_Settings__c> settings = new List<JVCO_Renewal_Settings__c>();
        settings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Completion', Days__c = 0.0));  
        settings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Generation', Days__c = 29));
        insert settings;
    }
    
    static testMethod void testJVCO_CompleteRenewalQuoteScheduler(){
       
        Test.startTest();
             
        JVCO_CompleteRenewalQuoteBatch completeRQBatch = new JVCO_CompleteRenewalQuoteBatch();
        Database.executeBatch(completeRQBatch);
        Test.stopTest();

    }

    static testMethod void testJVCO_CompleteRenewalQuoteScheduler2(){

        Test.startTest();
        JVCO_CompleteRenewalQuoteScheduler.callExecuteMomentarily();
        Test.stopTest();
    }

    static testMethod void testJVCO_CompleteRenewalQuoteScheduler3(){

        SBQQ__Quote__c q = [SELECT id FROM SBQQ__Quote__c order by CreatedDate DESC limit 1];

        //adhoc testing 
        Test.startTest();
        database.executeBatch(new JVCO_CompleteRenewalQuoteBatch(q.id));
        Test.stopTest();
    }

    static testMethod void testJVCO_CompleteRenewalQuoteScheduler4(){

        Test.startTest();
        List<SBQQ__Quote__c> l_q = [SELECT id FROM SBQQ__Quote__c order by CreatedDate DESC limit 1];
        Set<id> set_q = new Set<id>();

        for (SBQQ__Quote__c qo : l_q)
        {
            set_q.add(qo.id); 
        } 
        
        database.executeBatch(new JVCO_CompleteRenewalQuoteBatch(set_q));

        Test.stopTest();    
    }
}