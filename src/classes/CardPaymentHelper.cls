/**
* @description A helper class to easily create payments and refunds of different types
* such as Card Payment, Cash Payment or Cheque Payment. This simply handles the creation
* of the payment records and payment history records (and the messages to SagePay in the
* case of Card Payments) - business rules surrounding payments should be handled elsewhere.
*/
global class CardPaymentHelper {
    // private static String partnerURL = (String.valueof(url.getSalesforceBaseUrl().toExternalForm()).replace('-api.salesforce.com','') + '/services/Soap/u/37.0/' + userinfo.getOrganizationId());
    private static String partnerURL = [select API_URL__c from User where id=:userinfo.getUserId()].API_URL__c;
    public static SMP_Payment_Config__c paymentConfig = SMP_Payment_Config__c.getInstance();

    /**
    * @description Prepares the SagePay payment URL using passed parameters.
    */
    public static PageReference generateMOTOCardPaymentURL(MOTOPayment motoIn)
    {  
        System.debug('#### Generating MOTO Card Payment URL');
        System.debug('#### motoIn:'+motoIn);

        PageReference paymentURL = null;
        if(!Test.isRunningTest()){ 
            paymentURL = new PageReference(paymentConfig.MOTO_Payment_URL__c);
            System.debug('#### customersetting:'+paymentConfig.MOTO_Payment_URL__c);
        }
        else 
        {
            paymentURL = new PageReference('https://test2.income-systemsltd.com/income-sagepay/default.aspx');

        }
        

        paymentURL.getParameters().put('sagepayvendor', motoIn.sagepayvendor);
        paymentURL.getParameters().put('incomeusername', motoIn.incomeusername);
        paymentURL.getParameters().put('salesforceconnectionid', fetchUserSessionId());
        //paymentURL.getParameters().put('salesforceconnectionid', UserInfo.getSessionId());
        paymentURL.getParameters().put('salesforceconnectionurl', partnerURL);
        paymentURL.getParameters().put('softwareversion', motoIn.softwareVersion);
        paymentURL.getParameters().put('salesforcecardpaymentrecordtype', motoIn.cardPaymentRecordType);

        paymentURL.getParameters().put('firstsfreferenceobject', motoIn.firstReferenceObject);
        paymentURL.getParameters().put('firstsfreferenceobjectname', motoIn.firstReferenceObjectName);
        paymentURL.getParameters().put('firstsfreferenceid', motoIn.firstReferenceId);
        paymentURL.getParameters().put('firstsfreferencename', motoIn.firstReferenceName);

        paymentURL.getParameters().put('secondsfreferenceobject', motoIn.secondReferenceObject);
        paymentURL.getParameters().put('secondsfreferenceobjectname', motoIn.secondReferenceObjectName);
        paymentURL.getParameters().put('secondsfreferenceid', motoIn.secondReferenceId);

        paymentURL.getParameters().put('thirdsfreferenceobject', motoIn.thirdReferenceObject);
        paymentURL.getParameters().put('thirdsfreferenceobjectname', motoIn.thirdReferenceObjectName);
        paymentURL.getParameters().put('thirdsfreferenceid', motoIn.thirdReferenceId);

        paymentURL.getParameters().put('billingaddress', String.isNotBlank(motoIn.billingAddress) && motoIn.billingAddress.length() > 100 ? motoIn.billingAddress.substring(0, 99) : motoIn.billingAddress);
        paymentURL.getParameters().put('billingpostcode', String.isNotBlank(motoIn.billingPostcode) && motoIn.billingPostcode.length() > 10 ? motoIn.billingPostcode.substring(0, 9) : motoIn.billingPostcode);
        paymentURL.getParameters().put('billingcity', String.isNotBlank(motoIn.billingCity) && motoIn.billingCity.length() > 40 ? motoIn.billingCity.substring(0, 39) : motoIn.billingCity);
        paymentURL.getParameters().put('billingcountrycode', motoIn.billingCountryCode);
        paymentURL.getParameters().put('billingemail', motoIn.billingEmail);
        paymentURL.getParameters().put('billingfirstname', String.isNotBlank(motoIn.billingFirstName) && motoIn.billingFirstName.length() > 20 ? motoIn.billingFirstName.substring(0, 19) : motoIn.billingFirstName);
        paymentURL.getParameters().put('billingsurname', String.isNotBlank(motoIn.billingSurname) && motoIn.billingSurname.length() > 20 ? motoIn.billingSurname.substring(0, 19) : motoIn.billingSurname);        

        paymentURL.getParameters().put('deliveryaddress', String.isNotBlank(motoIn.deliveryAddress) && motoIn.deliveryAddress.length() > 100 ? motoIn.deliveryAddress.substring(0, 99) : motoIn.deliveryAddress);
        paymentURL.getParameters().put('deliverypostcode', String.isNotBlank(motoIn.deliveryPostCode) && motoIn.deliveryPostCode.length() > 10 ? motoIn.deliveryPostCode.substring(0, 9) : motoIn.deliveryPostCode);
        paymentURL.getParameters().put('deliverycity', String.isNotBlank(motoIn.deliveryCity) && motoIn.deliveryCity.length() > 40 ? motoIn.deliveryCity.substring(0, 39) : motoIn.deliveryCity);
        paymentURL.getParameters().put('deliverycountrycode', motoIn.deliveryCountryCode);
        paymentURL.getParameters().put('deliveryemail', motoIn.deliveryEmail);
        paymentURL.getParameters().put('deliveryfirstname', String.isNotBlank(motoIn.deliveryFirstName) && motoIn.deliveryFirstName.length() > 20 ? motoIn.deliveryFirstName.substring(0, 19) : motoIn.deliveryFirstName);
        paymentURL.getParameters().put('deliverysurname', String.isNotBlank(motoIn.deliverySurname) && motoIn.deliverySurname.length() > 20 ? motoIn.deliverySurname.substring(0, 19) : motoIn.deliverySurname);
        
        paymentURL.getParameters().put('saleamount', motoIn.saleamount);
        paymentURL.getParameters().put('multicurrencymode', motoIn.isMultiCurrency == true ? 'true' : 'false');
        paymentURL.getParameters().put('salecurrencycode', motoIn.salesCurrencyCode);

        paymentURL.getParameters().put('paymentreason', motoIn.paymentReason);
        paymentURL.getParameters().put('paymentdescription', motoIn.paymentDescription);
        paymentURL.getParameters().put('usebasket', motoIn.useBasket == true ? 'true' : 'false');
        paymentURL.getParameters().put('basketcontent', motoIn.basketContent);
        
        paymentURL.getParameters().put('rpfrequency', motoIn.rpFrequency );
        paymentURL.getParameters().put('rpnextpaymentdate', motoIn.rpNextPaymentDate);
        paymentURL.getParameters().put('rpenddate', motoIn.rpLastPaymentDate);
        paymentURL.getParameters().put('rpstartdate', motoIn.rpStartDate);
        paymentURL.getParameters().put('rpamount', motoIn.rpAmount );
        paymentURL.getParameters().put('rpstretch', motoIn.rpStretch);
        paymentURL.getParameters().put('rpdescription', motoIn.rpDescription);
        paymentURL.getParameters().put('rpenabled', motoIn.rpEnabled);
        
        System.debug('#### Generated URL:' + paymentURL.getURL());

        return paymentURL;
    }
    public static PageReference generateECommerceRefundPaymentURL(Refund refundIn)
    {
        System.debug('#### Generating Ecommerce Refund Payment URL');
        System.debug('#### ecomIn:'+refundIn);
        System.debug('#### paymentConfig:'+paymentConfig);

        PageReference paymentURL = null;
        if(!Test.isRunningTest()){ 
            paymentURL = new PageReference(paymentConfig.Ecommerce_Refund_URL__c);
            System.debug('#### customersetting:'+paymentConfig.Ecommerce_Refund_URL__c);
        }
        else 
        {
            paymentURL = new PageReference('https://www.income-systemsltd.com/income-sagepay/refundvoid.aspx    ');

        }
        //PageReference paymentURL = new PageReference(paymentConfig.Ecommerce_Refund_URL__c);

        paymentURL.getParameters().put('salesforceconnectionid', UserInfo.getSessionId());
        paymentURL.getParameters().put('salesforceconnectionurl',partnerURL);
        paymentURL.getParameters().put('salesforcepaymentrecordid', refundIn.cardPaymentRecordId);
        paymentURL.getParameters().put('salesforcepaymentrecordname', refundIn.cardPaymentRecordName);
        paymentURL.getParameters().put('cardnumber', refundIn.cardNumber);
        paymentURL.getParameters().put('paymentdescription', refundIn.paymentDescription);
        paymentURL.getParameters().put('originalamount', refundIn.originalAmount);
        paymentURL.getParameters().put('currency', 'GBP');
        paymentURL.getParameters().put('multicurrencymode', 'false');
        paymentURL.getParameters().put('refundreasonrequired', 'true');
        paymentURL.getParameters().put('RelatedVPSTxId', refundIn.relatedVPSTxId);
        paymentURL.getParameters().put('RelatedVendorTxCode', refundIn.relatedVendorTXCode);
        paymentURL.getParameters().put('RelatedSecurityKey', refundIn.relatedSecurityKey);
        paymentURL.getParameters().put('RelatedTxAuthNo', refundIn.relatedTxAuthNo);
        paymentURL.getParameters().put('SagepayVendor', 'pplprs');
        paymentURL.getParameters().put('RefundOnly', 'false');
        paymentURL.getParameters().put('RefundFullAmountOnly', 'false');
        paymentURL.getParameters().put('AuthorisationDate', refundIn.authorisationDate);
        
        System.debug('#### Generated URL:' + paymentURL.getURL());

        return paymentURL;            
    }
    public static PageReference generateECommerceCardPaymentURL(EcommercePayment ecomIn)
    {
        System.debug('#### Generating EcommerceO Card Payment URL');
        System.debug('#### ecomIn:'+ecomIn);

        PageReference paymentURL = new PageReference(paymentConfig.Ecommerce_Payment_URL__c);

        paymentURL.getParameters().put('sagepayvendor', ecomIn.sagepayvendor);
        paymentURL.getParameters().put('incomeusername', ecomIn.incomeusername);

        paymentURL.getParameters().put('salesforceconnectionid', UserInfo.getSessionId());
        paymentURL.getParameters().put('salesforceconnectionurl', partnerURL);
        paymentURL.getParameters().put('softwareversion', ecomIn.softwareVersion);
        paymentURL.getParameters().put('salesforcecardpaymentrecordtype', ecomIn.cardPaymentRecordType);

        paymentURL.getParameters().put('firstsfreferenceobject', ecomIn.firstReferenceObject);
        paymentURL.getParameters().put('firstsfreferenceobjectname', ecomIn.firstReferenceObjectName);
        paymentURL.getParameters().put('firstsfreferenceid', ecomIn.firstReferenceId);
        paymentURL.getParameters().put('firstsfreferencename', ecomIn.firstReferenceName);

        paymentURL.getParameters().put('secondsfreferenceobject', ecomIn.secondReferenceObject);
        paymentURL.getParameters().put('secondsfreferenceobjectname', ecomIn.secondReferenceObjectName);
        paymentURL.getParameters().put('secondsfreferenceid', ecomIn.secondReferenceId);

        paymentURL.getParameters().put('thirdsfreferenceobject', ecomIn.thirdReferenceObject);
        paymentURL.getParameters().put('thirdsfreferenceobjectname', ecomIn.thirdReferenceObjectName);
        paymentURL.getParameters().put('thirdsfreferenceid', ecomIn.thirdReferenceId);

        paymentURL.getParameters().put('billingaddress', String.isNotBlank(ecomIn.billingAddress) && ecomIn.billingAddress.length() > 100 ? ecomIn.billingAddress.substring(0, 99) : ecomIn.billingAddress);
        paymentURL.getParameters().put('billingpostcode', String.isNotBlank(ecomIn.billingPostcode) && ecomIn.billingPostcode.length() > 10 ? ecomIn.billingPostcode.substring(0, 9) : ecomIn.billingPostcode);
        paymentURL.getParameters().put('billingcity', String.isNotBlank(ecomIn.billingCity) && ecomIn.billingCity.length() > 40 ? ecomIn.billingCity.substring(0, 39) : ecomIn.billingCity);
        paymentURL.getParameters().put('billingcountrycode', ecomIn.billingCountryCode);
        paymentURL.getParameters().put('billingemail', ecomIn.billingEmail);
        paymentURL.getParameters().put('billingfirstname', String.isNotBlank(ecomIn.billingFirstName) && ecomIn.billingFirstName.length() > 20 ? ecomIn.billingFirstName.substring(0, 19) : ecomIn.billingFirstName);
        paymentURL.getParameters().put('billingsurname', String.isNotBlank(ecomIn.billingSurname) && ecomIn.billingSurname.length() > 20 ? ecomIn.billingSurname.substring(0, 19) : ecomIn.billingSurname);        

        paymentURL.getParameters().put('deliveryaddress', String.isNotBlank(ecomIn.deliveryAddress) && ecomIn.deliveryAddress.length() > 100 ? ecomIn.deliveryAddress.substring(0, 99) : ecomIn.deliveryAddress);
        paymentURL.getParameters().put('deliverypostcode', String.isNotBlank(ecomIn.deliveryPostCode) && ecomIn.deliveryPostCode.length() > 10 ? ecomIn.deliveryPostCode.substring(0, 9) : ecomIn.deliveryPostCode);
        paymentURL.getParameters().put('deliverycity', String.isNotBlank(ecomIn.deliveryCity) && ecomIn.deliveryCity.length() > 40 ? ecomIn.deliveryCity.substring(0, 39) : ecomIn.deliveryCity);
        paymentURL.getParameters().put('deliverycountrycode', ecomIn.deliveryCountryCode);
        paymentURL.getParameters().put('deliveryemail', ecomIn.deliveryEmail);
        paymentURL.getParameters().put('deliveryfirstname', String.isNotBlank(ecomIn.deliveryFirstName) && ecomIn.deliveryFirstName.length() > 20 ? ecomIn.deliveryFirstName.substring(0, 19) : ecomIn.deliveryFirstName);
        paymentURL.getParameters().put('deliverysurname', String.isNotBlank(ecomIn.deliverySurname) && ecomIn.deliverySurname.length() > 20 ? ecomIn.deliverySurname.substring(0, 19) : ecomIn.deliverySurname);
        
        paymentURL.getParameters().put('saleamount', ecomIn.saleamount);
        paymentURL.getParameters().put('multicurrencymode', ecomIn.isMultiCurrency == true ? 'true' : 'false');
        paymentURL.getParameters().put('salecurrencycode', ecomIn.salesCurrencyCode);
        
        paymentURL.getParameters().put('paymentreason', ecomIn.paymentReason);
        paymentURL.getParameters().put('paymentdescription', ecomIn.paymentDescription);
        paymentURL.getParameters().put('usebasket', ecomIn.useBasket == true ? 'true' : 'false');
        paymentURL.getParameters().put('basketcontent', ecomIn.basketContent);
        
        System.debug('#### Generated URL:' + paymentURL.getURL());

        return paymentURL;
    }

    public static PageReference generateAuthenticateCardURL(AuthenticateCard authCardIn)
    {
        System.debug('#### Generating Authenticate Card Payment URL');
        System.debug('#### authCardIn:'+authCardIn);

        PageReference paymentURL = new PageReference(paymentConfig.Authenticate_Internal_URL__c);

        paymentURL.getParameters().put('sagepayvendor', authCardIn.sagepayvendor);
        paymentURL.getParameters().put('incomeusername', authCardIn.incomeusername);

        paymentURL.getParameters().put('salesforceconnectionid', UserInfo.getSessionId());
        paymentURL.getParameters().put('salesforceconnectionurl', partnerURL);
        paymentURL.getParameters().put('softwareversion', authCardIn.softwareVersion);
        paymentURL.getParameters().put('salesforcecardpaymentrecordtype', authCardIn.cardPaymentRecordType);

        paymentURL.getParameters().put('firstsfreferenceobject', authCardIn.firstReferenceObject);
        paymentURL.getParameters().put('firstsfreferenceobjectname', authCardIn.firstReferenceObjectName);
        paymentURL.getParameters().put('firstsfreferenceid', authCardIn.firstReferenceId);
        paymentURL.getParameters().put('firstsfreferencename', authCardIn.firstReferenceName);

        paymentURL.getParameters().put('secondsfreferenceobject', authCardIn.secondReferenceObject);
        paymentURL.getParameters().put('secondsfreferenceobjectname', authCardIn.secondReferenceObjectName);
        paymentURL.getParameters().put('secondsfreferenceid', authCardIn.secondReferenceId);

        paymentURL.getParameters().put('billingaddress', String.isNotBlank(authCardIn.billingAddress) && authCardIn.billingAddress.length() > 100 ? authCardIn.billingAddress.substring(0, 99) : authCardIn.billingAddress);
        paymentURL.getParameters().put('billingpostcode', String.isNotBlank(authCardIn.billingPostcode) && authCardIn.billingPostcode.length() > 10 ? authCardIn.billingPostcode.substring(0, 9) : authCardIn.billingPostcode);
        paymentURL.getParameters().put('billingcity', String.isNotBlank(authCardIn.billingCity) && authCardIn.billingCity.length() > 40 ? authCardIn.billingCity.substring(0, 39) : authCardIn.billingCity);
        paymentURL.getParameters().put('billingcountrycode', authCardIn.billingCountryCode);
        paymentURL.getParameters().put('billingemail', authCardIn.billingEmail);
        paymentURL.getParameters().put('billingfirstname', String.isNotBlank(authCardIn.billingFirstName) && authCardIn.billingFirstName.length() > 20 ? authCardIn.billingFirstName.substring(0, 19) : authCardIn.billingFirstName);
        paymentURL.getParameters().put('billingsurname', String.isNotBlank(authCardIn.billingSurname) && authCardIn.billingSurname.length() > 20 ? authCardIn.billingSurname.substring(0, 19) : authCardIn.billingSurname);            

        paymentURL.getParameters().put('deliveryaddress', String.isNotBlank(authCardIn.deliveryAddress) && authCardIn.deliveryAddress.length() > 100 ? authCardIn.deliveryAddress.substring(0, 99) : authCardIn.deliveryAddress);
        paymentURL.getParameters().put('deliverypostcode', String.isNotBlank(authCardIn.deliveryPostCode) && authCardIn.deliveryPostCode.length() > 10 ? authCardIn.deliveryPostCode.substring(0, 9) : authCardIn.deliveryPostCode);
        paymentURL.getParameters().put('deliverycity', String.isNotBlank(authCardIn.deliveryCity) && authCardIn.deliveryCity.length() > 40 ? authCardIn.deliveryCity.substring(0, 39) : authCardIn.deliveryCity);
        paymentURL.getParameters().put('deliverycountrycode', authCardIn.deliveryCountryCode);
        paymentURL.getParameters().put('deliveryemail', authCardIn.deliveryEmail);
        paymentURL.getParameters().put('deliveryfirstname', String.isNotBlank(authCardIn.deliveryFirstName) && authCardIn.deliveryFirstName.length() > 20 ? authCardIn.deliveryFirstName.substring(0, 19) : authCardIn.deliveryFirstName);
        paymentURL.getParameters().put('deliverysurname', String.isNotBlank(authCardIn.deliverySurname) && authCardIn.deliverySurname.length() > 20 ? authCardIn.deliverySurname.substring(0, 19) : authCardIn.deliverySurname);

        paymentURL.getParameters().put('saleamount', authCardIn.saleamount);
        paymentURL.getParameters().put('multicurrencymode', authCardIn.isMultiCurrency == true ? 'true' : 'false');
        paymentURL.getParameters().put('salecurrencycode', authCardIn.salesCurrencyCode);
        
        paymentURL.getParameters().put('paymentreason', authCardIn.paymentReason);
        paymentURL.getParameters().put('paymentdescription', authCardIn.paymentDescription);
        paymentURL.getParameters().put('usebasket', authCardIn.useBasket == true ? 'true' : 'false');
        paymentURL.getParameters().put('basketcontent', authCardIn.basketContent);

        System.debug('#### Generated URL:' + paymentURL.getURL());

        return paymentURL;
    }

    public static PageReference generateECommerceAuthenticateCardURL(AuthenticateCard authCardIn)
    {
        System.debug('#### Generating Authenticate Card Payment URL');
        System.debug('#### authCardIn:'+authCardIn);

        PageReference paymentURL = new PageReference(paymentConfig.Authenticate_Ecommerce_URL__c);

        paymentURL.getParameters().put('sagepayvendor', authCardIn.sagepayvendor);
        paymentURL.getParameters().put('incomeusername', authCardIn.incomeusername);

        paymentURL.getParameters().put('salesforceconnectionid', UserInfo.getSessionId());
        paymentURL.getParameters().put('salesforceconnectionurl', partnerURL);
        paymentURL.getParameters().put('softwareversion', authCardIn.softwareVersion);
        paymentURL.getParameters().put('salesforcecardpaymentrecordtype', authCardIn.cardPaymentRecordType);

        paymentURL.getParameters().put('firstsfreferenceobject', authCardIn.firstReferenceObject);
        paymentURL.getParameters().put('firstsfreferenceobjectname', authCardIn.firstReferenceObjectName);
        paymentURL.getParameters().put('firstsfreferenceid', authCardIn.firstReferenceId);
        paymentURL.getParameters().put('firstsfreferencename', authCardIn.firstReferenceName);

        paymentURL.getParameters().put('secondsfreferenceobject', authCardIn.secondReferenceObject);
        paymentURL.getParameters().put('secondsfreferenceobjectname', authCardIn.secondReferenceObjectName);
        paymentURL.getParameters().put('secondsfreferenceid', authCardIn.secondReferenceId);

        paymentURL.getParameters().put('billingaddress', String.isNotBlank(authCardIn.billingAddress) && authCardIn.billingAddress.length() > 100 ? authCardIn.billingAddress.substring(0, 99) : authCardIn.billingAddress);
        paymentURL.getParameters().put('billingpostcode', String.isNotBlank(authCardIn.billingPostcode) && authCardIn.billingPostcode.length() > 10 ? authCardIn.billingPostcode.substring(0, 9) : authCardIn.billingPostcode);
        paymentURL.getParameters().put('billingcity', String.isNotBlank(authCardIn.billingCity) && authCardIn.billingCity.length() > 40 ? authCardIn.billingCity.substring(0, 39) : authCardIn.billingCity);
        paymentURL.getParameters().put('billingcountrycode', authCardIn.billingCountryCode);
        paymentURL.getParameters().put('billingemail', authCardIn.billingEmail);
        paymentURL.getParameters().put('billingfirstname', String.isNotBlank(authCardIn.billingFirstName) && authCardIn.billingFirstName.length() > 20 ? authCardIn.billingFirstName.substring(0, 19) : authCardIn.billingFirstName);
        paymentURL.getParameters().put('billingsurname', String.isNotBlank(authCardIn.billingSurname) && authCardIn.billingSurname.length() > 20 ? authCardIn.billingSurname.substring(0, 19) : authCardIn.billingSurname);            

        paymentURL.getParameters().put('deliveryaddress', String.isNotBlank(authCardIn.deliveryAddress) && authCardIn.deliveryAddress.length() > 100 ? authCardIn.deliveryAddress.substring(0, 99) : authCardIn.deliveryAddress);
        paymentURL.getParameters().put('deliverypostcode', String.isNotBlank(authCardIn.deliveryPostCode) && authCardIn.deliveryPostCode.length() > 10 ? authCardIn.deliveryPostCode.substring(0, 9) : authCardIn.deliveryPostCode);
        paymentURL.getParameters().put('deliverycity', String.isNotBlank(authCardIn.deliveryCity) && authCardIn.deliveryCity.length() > 40 ? authCardIn.deliveryCity.substring(0, 39) : authCardIn.deliveryCity);
        paymentURL.getParameters().put('deliverycountrycode', authCardIn.deliveryCountryCode);
        paymentURL.getParameters().put('deliveryemail', authCardIn.deliveryEmail);
        paymentURL.getParameters().put('deliveryfirstname', String.isNotBlank(authCardIn.deliveryFirstName) && authCardIn.deliveryFirstName.length() > 20 ? authCardIn.deliveryFirstName.substring(0, 19) : authCardIn.deliveryFirstName);
        paymentURL.getParameters().put('deliverysurname', String.isNotBlank(authCardIn.deliverySurname) && authCardIn.deliverySurname.length() > 20 ? authCardIn.deliverySurname.substring(0, 19) : authCardIn.deliverySurname);

        paymentURL.getParameters().put('saleamount', authCardIn.saleamount);
        paymentURL.getParameters().put('multicurrencymode', authCardIn.isMultiCurrency == true ? 'true' : 'false');
        paymentURL.getParameters().put('salecurrencycode', authCardIn.salesCurrencyCode);
        
        paymentURL.getParameters().put('paymentreason', authCardIn.paymentReason);
        paymentURL.getParameters().put('paymentdescription', authCardIn.paymentDescription);
        paymentURL.getParameters().put('usebasket', authCardIn.useBasket == true ? 'true' : 'false');
        paymentURL.getParameters().put('basketcontent', authCardIn.basketContent);
        
        System.debug('#### Generated URL:' + paymentURL.getURL());

        return paymentURL;
    }
   public static String fetchUserSessionId() {
       if (Test.isRunningTest()) {
            return '';
        }
        
        String sessionId = '';
        // Refer to the Page
        PageReference reportPage = Page.currentUserInfoCtrl;
        // Get the content of the VF page
        String vfContent = reportPage.getContent().toString();
        System.debug('vfContent '+vfContent);
        // Find the position of Start_Of_Session_Id and End_Of_Session_Id
        Integer startP = vfContent.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
        endP = vfContent.indexOf('End_Of_Session_Id');
        // Get the Session Id
        sessionId = vfContent.substring(startP, endP);
        System.debug('sessionId '+sessionId);
        // Return Session Id
        return sessionId;
}


    global with sharing class MOTOPayment
    {       
        public String sagepayvendor {get; set;}
        public String incomeusername {get; set;}

        public String softwareVersion {get; set;}
        public String cardPaymentRecordType {get; set;}
        public String firstReferenceId {get; set;}
        public String firstReferenceName {get; set;}
        public String firstReferenceObject {get; set;}
        public String firstReferenceObjectName {get; set;}
        public String secondReferenceId {get; set;}
        public String secondReferenceObject {get; set;}
        public String secondReferenceObjectName {get; set;}
        public String thirdReferenceId {get; set;}
        public String thirdReferenceObject {get; set;}
        public String thirdReferenceObjectName {get; set;}

        public String billingAddress {get; set;}
        public String billingPostcode {get; set;}
        public String billingCity {get; set;}        
        public String billingCountryCode {get; set;}
        public String billingEmail {get; set;}
        public String billingFirstName {get; set;}
        public String billingSurname {get; set;}
         
        public String deliveryAddress {get; set;}
        public String deliveryPostCode {get; set;}
        public String deliveryCity {get; set;}
        public String deliveryCountryCode {get; set;}
        public String deliveryEmail {get; set;}
        public String deliveryFirstName {get; set;}
        public String deliverySurname {get; set;}

        public String salesCurrencyCode {get; set;}
        public String saleamount {get; set;}
        public Boolean isMultiCurrency {get; set;}

        public String paymentReason {get; set;}
        public String paymentDescription {get; set;}
        public Boolean useBasket {get; set;}
        public String basketContent {get; set;}
        
        public String rpFrequency{get; set;}
        public String rpNextPaymentDate {get; set;}
        public String rpLastPaymentDate {get; set;}
        public String rpStartDate {get; set;}
        public String rpStretch {get; set;}
        public String rpAmount {get; set;}
        public String rpDescription {get; set;}
        public String rpEnabled{get; set;}

         
    }

    global with sharing class EcommercePayment
    {       
        public String sagepayvendor {get; set;}
        public String incomeusername {get; set;}

        public String softwareVersion {get; set;}
        public String cardPaymentRecordType {get; set;}
        public String firstReferenceId {get; set;}
        public String firstReferenceName {get; set;}
        public String firstReferenceObject {get; set;}
        public String firstReferenceObjectName {get; set;}
        public String secondReferenceId {get; set;}
        public String secondReferenceObject {get; set;}
        public String secondReferenceObjectName {get; set;}
        public String thirdReferenceId {get; set;}
        public String thirdReferenceObject {get; set;}
        public String thirdReferenceObjectName {get; set;}

        public String billingAddress {get; set;}
        public String billingPostcode {get; set;}
        public String billingCity {get; set;}        
        public String billingCountryCode {get; set;}
        public String billingEmail {get; set;}
        public String billingFirstName {get; set;}
        public String billingSurname {get; set;}
         
        public String deliveryAddress {get; set;}
        public String deliveryPostCode {get; set;}
        public String deliveryCity {get; set;}
        public String deliveryCountryCode {get; set;}
        public String deliveryEmail {get; set;}
        public String deliveryFirstName {get; set;}
        public String deliverySurname {get; set;}

        public String salesCurrencyCode {get; set;}
        public String saleamount {get; set;}
        public Boolean isMultiCurrency {get; set;}

        public String paymentReason {get; set;}
        public String paymentDescription {get; set;}
        public Boolean useBasket {get; set;}
        public String basketContent {get; set;}
    }

    global with sharing class AuthenticateCard
    {       
        public String sagepayvendor {get; set;}
        public String incomeusername {get; set;}

        public String softwareVersion {get; set;}
        public String cardPaymentRecordType {get; set;}
        public String firstReferenceId {get; set;}
        public String firstReferenceName {get; set;}
        public String firstReferenceObject {get; set;}
        public String firstReferenceObjectName {get; set;}
        public String secondReferenceId {get; set;}
        public String secondReferenceObject {get; set;}
        public String secondReferenceObjectName {get; set;}

        public String billingAddress {get; set;}
        public String billingPostcode {get; set;}
        public String billingCity {get; set;}        
        public String billingCountryCode {get; set;}
        public String billingEmail {get; set;}
        public String billingFirstName {get; set;}
        public String billingSurname {get; set;}
         
        public String deliveryAddress {get; set;}
        public String deliveryPostCode {get; set;}
        public String deliveryCity {get; set;}
        public String deliveryCountryCode {get; set;}
        public String deliveryEmail {get; set;}
        public String deliveryFirstName {get; set;}
        public String deliverySurname {get; set;}

        public String salesCurrencyCode {get; set;}
        public String saleamount {get; set;}
        public Boolean isMultiCurrency {get; set;}

        public String paymentReason {get; set;}
        public String paymentDescription {get; set;}
        public Boolean useBasket {get; set;}
        public String basketContent {get; set;}
    }

    global with sharing class AuthoriseCard
    {
        public String sagepayvendor {get; set;}

        public String relatedVendorTXCode {get; set;}
        public String authoriseAmount {get; set;}
        public String currencyCode {get; set;}

        public String relatedVPSTxId {get; set;}
        public String relatedSecurityKey {get; set;}

        public String description {get; set;}

        public String cardPaymentRecordId {get; set;}

        public Boolean isMultiCurrency {get; set;}
    }

    global with sharing class RepeatPayment
    {
        public String sagepayvendor {get; set;}

        public String relatedVendorTXCode {get; set;}
        public String newAmount {get; set;}
        public String currencyCode {get; set;}
        public String relatedVPSTxId {get; set;}
        public String relatedSecurityKey {get; set;}
        public String relatedTxAuthNo {get; set;}
        public String description {get; set;}
        public String cvv {get; set;}
        public String cardPaymentRecordId {get; set;}
    }

    global with sharing class Refund
    {
        public String sagepayvendor {get; set;}

        public String relatedVendorTXCode {get; set;}
        public String newAmount {get; set;}
        public String originalAmount {get; set;}
        public String currencyCode {get; set;}

        public String relatedVPSTxId {get; set;}
        public String relatedSecurityKey {get; set;}
        public String relatedTxAuthNo {get; set;}

        public String refundReason {get; set;}
        public String cardPaymentRecordId {get; set;}
        public String cardPaymentRecordName {get; set;}
        public String cardNumber {get; set;}
        public String paymentDescription {get; set;}

        public String authorisationDate {get; set;}
    }
}