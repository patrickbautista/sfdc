/* -------------------------------------------------------------------------------------
    Name: JVCO_AccountRestructureExtension
    Description: Extension class for JVCO_AccountRestructure page

    Date            Version     Author                Summary of Changes 
    -----------     -------     -----------------     ----------------------------------
    10-May-2017     0.1         jules.osberg.a.pablo  Intial creation
------------------------------------------------------------------------------------- */
public class JVCO_AccountRestructureExtension {
    public Account fetchedAccountRecord;
    public Account accountRecord;
    public Integer numberOfRecords {get; set;}
    public Boolean isContinued {get; set;}
    public Boolean isProcessing {get; set;}
    public Boolean isValidType {get; set;}
    public Boolean isValidContractInvoice {get; set;}
    private JVCO_AccountRestructureHelper accountRestructureHelper { get; set; }
    public JVCO_AccountRestructureExtension (ApexPages.StandardController stdController){
        numberOfRecords = 0;
        isContinued = false;
        isValidType = false;
        isValidContractInvoice = true;
        fetchedAccountRecord = (Account)stdController.getRecord();
        accountRestructureHelper = new JVCO_AccountRestructureHelper();

        accountRecord = [SELECT Id, Name, Type FROM Account WHERE Id =: fetchedAccountRecord.Id];

        
        isValidType = isValidProfile(accountRecord.Type);
        
        if(invalidQuotesCount() > 0){
            isValidContractInvoice = false;
        }

        List<JVCO_Affiliation__c> affiliationToProcess = Database.query(accountRestructureHelper.getQueryString(accountRecord.Id));
        if(!affiliationToProcess.isEmpty()){
            numberOfRecords = affiliationToProcess.size();
        }
    }
    


    public void queueAccountRestructure(){
        Integer batchSize = 1;
        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        if(asCS.JVCO_AccountRestuctureBatchSize__c != null & Integer.valueOf(asCS.JVCO_AccountRestuctureBatchSize__c) > 0){
            batchSize = Integer.valueOf(asCS.JVCO_AccountRestuctureBatchSize__c);
        }

        Id batchId = database.executeBatch(new JVCO_AccountRestructureBatch(accountRecord), batchSize);
        isContinued = true;
    }
    
    private Boolean isValidProfile(String accountType){
        Id profileId=userinfo.getProfileId();
        Profile profile = [SELECT Id, Name FROM Profile WHERE Id=:profileId];

        JVCO_PermissionsForAccountRestructure__c permission = JVCO_PermissionsForAccountRestructure__c.getInstance(profile.Id);

        if((accountType == 'Key Account' || accountType == 'Agency') && JVCO_PermissionsForAccountRestructure__c.getInstance(profile.Id).JVCO_GrantRestructurePermission__c){
            return true;
        }else{
            return false;
        }
    }

    private Integer invalidQuotesCount(){
        Set<String> invalidQuotesSet = new Set<String>();
        for(JVCO_OpenQuoteStatuses__c qStatus : JVCO_OpenQuoteStatuses__c.getall().values()){
            invalidQuotesSet.add(qStatus.JVCO_StatusName__c);
        }   
        AggregateResult[] invalidQuotesCount = [SELECT Count(Id) rec FROM SBQQ__Quote__c WHERE JVCO_Cust_Account__c =: accountRecord.Id AND SBQQ__NetAmount__c != 0 AND JVCO_Invoiced__c = false AND SBQQ__Status__c not in :invalidQuotesSet];
        return (Integer)invalidQuotesCount[0].get('rec');
    }

    public PageReference backToRecord(){
        PageReference pRef = new PageReference('/' + accountRecord.Id);
        return pRef;
    }
}