/* ----------------------------------------------------------------------------------------------
Name: JVCO_LinkTempAccountReference.cls 
Description: Batch class that handles Temp Account Reference Linking to the sales invoice then update
             the account reference             
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
11-Apr-2017  0.1         kristoffer.d.martin   Intial creation
15-Aug-2017  0.2         desiree.m.quijada     Added filter criteria on c2g__codaInvoice__c and c2g__codaCashEntryLineItem__c
01-Sept-2017 0.4         desiree.m.quijada     Catch dml exception error and create a record on the error log object
----------------------------------------------------------------------------------------------- */
global class JVCO_LinkTempAccountReference implements Database.Batchable<sObject>, Database.Stateful{
        
    private static Map<String, ID> subscriptionIdMap;
    private static List<c2g__codaCashEntryLineItem__c> cashEntryLineItemForUpdate;
    private static List<c2g__codaCashEntryLineItem__c> cashEntryLineItemList;
    private static Set<String> accountReferencesSet;

    private static void init(){
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();

        List<c2g__codaInvoice__c> salesInvoiceList = [SELECT Id, Name, JVCO_Invoice_Legacy_Number__c FROM c2g__codaInvoice__c 
                                                      WHERE JVCO_Invoice_Legacy_Number__c != null 
                                                      AND CreatedBy.Name = 'Data Migration'
                                                      AND JVCO_Invoice_Legacy_Number__c IN :accountReferencesSet];
        
        cashEntryLineItemForUpdate = new List<c2g__codaCashEntryLineItem__c>();

        for(c2g__codaCashEntryLineItem__c cashEntryLineItem : cashEntryLineItemList)
        {
            for (c2g__codaInvoice__c salesInvoice : salesInvoiceList)
            {
                if (salesInvoice.JVCO_Invoice_Legacy_Number__c == cashEntryLineItem.JVCO_Temp_AccountReference__c)
                {
                    cashEntryLineItem.c2g__AccountReference__c = salesInvoice.Name;
                    cashEntryLineItemForUpdate.add(cashEntryLineItem);
                }
            }
        }
        /*
        if(cashEntryLineItemForUpdate != null){
            update cashEntryLineItemForUpdate;
        }*/
        if(cashEntryLineItemForUpdate != null){
      
          //update cashEntryLineItemForUpdate - desiree.m.quijada
          List<Database.SaveResult> saveResList = Database.update(cashEntryLineItemForUpdate,false);

          for(Integer i=0; i < cashEntryLineItemForUpdate.size(); i++){
            Database.SaveResult indRes = saveResList[i];
            c2g__codaCashEntryLineItem__c cashEnt = cashEntryLineItemForUpdate[i];

            //every failed update/inser will be logged to the error log object blng__ErrorLog__c
            if(!indRes.isSuccess()){
              for(Database.Error err : indRes.getErrors()) {
                  ffps_custRem__Custom_Log__c newErr =  new ffps_custRem__Custom_Log__c();
                  if(cashEnt.ID != null || !String.isBlank(String.valueOf(cashEnt.ID))){
                    if(err.getMessage() != null || !String.isBlank(String.valueOf(err.getMessage()))){
                        if(err.getStatusCode() != null || !String.isBlank(String.valueOf(err.getStatusCode()))){
                            
                            newErr.ffps_custRem__Running_User__c = UserInfo.getUserId();
                            newErr.ffps_custRem__Related_Object_Key__c= String.valueOf(cashEnt.ID);
                            newErr.ffps_custRem__Detail__c = String.valueof(err.getMessage());
                            newErr.ffps_custRem__Message__c = 'Link Temporary Account Reference';
                            newErr.ffps_custRem__Grouping__c = String.valueof(err.getStatusCode());
                        } 
                    }
                    errLogList.add(newErr);
                  }
              }
            }
            else{
                System.debug('Update successfully');
            }
          }

          if(!errLogList.isEmpty()){
            try{
              insert errLogList;
            } 
            catch (Exception e){
              System.debug('Error: ' +e.getMessage() +e.getCause() +e.getTypeName());
            }

          }  
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id, Name, JVCO_Temp_AccountReference__c, c2g__AccountReference__c, c2g__CashEntry__c,
                                                c2g__CashEntry__r.c2g__Status__c
                                         FROM c2g__codaCashEntryLineItem__c
                                         WHERE c2g__CashEntry__r.c2g__Status__c = 'In Progress'
                                            AND JVCO_Temp_AccountReference__c!= null 
                                            AND CreatedBy.Name = 'Data Migration']);

    }

    global void execute(Database.BatchableContext bc, List<c2g__codaCashEntryLineItem__c> scope)
    {
        cashEntryLineItemList = scope;
        accountReferencesSet = new Set<String>();
        for(c2g__codaCashEntryLineItem__c ac : cashEntryLineItemList){
            accountReferencesSet.add(ac.JVCO_Temp_AccountReference__c);
        }
        init();
                
    }

    global void finish(Database.BatchableContext BC) 
    {
        
        System.debug('DONE');
    
    }
}