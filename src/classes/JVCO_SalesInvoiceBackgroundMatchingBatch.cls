/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_SalesInvoiceBackgroundMatchingBatch.cls 
   Description:     Batch Class for JVCO_SalesInvoiceBackgroundMatchingBatch.cls
   Test class:      

   Date            Version     Author              Summary of Changes 
   -----------     -------     -----------------   -----------------------------------
   13-Mar-2019     0.1         franz.g.a.dimaapi   Intial creation
  ------------------------------------------------------------------------------------------------ */
public class JVCO_SalesInvoiceBackgroundMatchingBatch implements Database.Batchable<sObject>, Database.Stateful
{
    private static final JVCO_General_Settings__c generalSettings = JVCO_General_Settings__c.getInstance();
    public Map<Id, Id> licAndCustAccIdMap = new Map<Id, Id>();
    
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {   
        return Database.getQueryLocator([SELECT Id,
                                         c2g__Account__c,
                                         c2g__Account__r.JVCO_Customer_Account__c,
                                         c2g__AccountOutstandingValue__c,
                                         c2g__OwnerCompany__r.c2g__CustomerWriteOff__c,
                                         c2g__Transaction__r.c2g__Period__r.c2g__Closed__c,
                                         c2g__Transaction__r.c2g__TransactionType__c,
                                         c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Reference_Document__c,
                                         c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Reference_Document_Type__c
                                         FROM c2g__codaTransactionLineItem__c
                                         WHERE c2g__AccountOutstandingValue__c != 0
                                         AND c2g__LineType__c IN ('Account')
                                         AND c2g__LineDescription__c != 'Not for Matching'
                                         AND c2g__Account__r.JVCO_In_Infringement__c = FALSE
                                         AND c2g__Account__r.JVCO_In_Enforcement__c = FALSE
                                         AND c2g__Account__r.JVCO_Exclude_From_Background_Matching__c = FALSE
                                         AND c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Reference_Document_Type__c IN ('Invoice', 'Credit', 'Journal', 'Cash')
                                        ]);
    }
    
    public void execute(Database.BatchableContext bc, List<c2g__codaTransactionLineItem__c> scope)
    {
        if(!generalSettings.JVCO_Disable_Invoice_Background_Matching__c)
        {
            setMatchingReferenceMapping(scope);
            removeReferenceDocument(scope);
            getAccountToRunQueable(scope);
        }
    }
    
    private void setMatchingReferenceMapping(List<c2g__codaTransactionLineItem__c> transLineList)
    {
        Map<Id, String> sInvTransLineIdToSInvNameMap = new Map<Id, String>();
        Map<Id, String> sInvTransLineIdToCNoteNameMap = new Map<Id, String>();
        Map<Id, String> sInvTransLineIdToJournalNameMap = new Map<Id, String>();
        Map<Id, String> sInvTransLineIdToReceiptLineNameMap = new Map<Id, String>();
        Map<Id, String> sInvTransLineIdToRefundLineNameMap = new Map<Id, String>();
        Map<Id, c2g__codaTransactionLineItem__c> transLineMap = new Map<Id, c2g__codaTransactionLineItem__c>();
        
        for(c2g__codaTransactionLineItem__c tli : transLineList)
        {
            String sInvReferenceDocument = tli.c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Reference_Document__c;
            if(tli.c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Invoice'))
            {
                sInvTransLineIdToSInvNameMap.put(tli.Id, sInvReferenceDocument);
            }else if(tli.c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Credit'))
            {
                sInvTransLineIdToCNoteNameMap.put(tli.Id, sInvReferenceDocument);
            }else if(tli.c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Journal'))
            {
                sInvTransLineIdToJournalNameMap.put(tli.Id, sInvReferenceDocument);
            }else if(tli.c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Cash') &&
                tli.c2g__AccountOutstandingValue__c > 0)
            {
                sInvTransLineIdToReceiptLineNameMap.put(tli.Id, sInvReferenceDocument);
            }else
            {
                sInvTransLineIdToRefundLineNameMap.put(tli.Id, sInvReferenceDocument);
            }
            transLineMap.put(tli.Id, tli);
        }
        executeMatching(sInvTransLineIdToSInvNameMap, sInvTransLineIdToCNoteNameMap, sInvTransLineIdToJournalNameMap,
                        sInvTransLineIdToReceiptLineNameMap, sInvTransLineIdToRefundLineNameMap, transLineMap);
    }
    
    private void executeMatching(Map<Id, String> sInvTransLineIdToSInvNameMap, Map<Id, String> sInvTransLineIdToCNoteNameMap,
                                 Map<Id, String> sInvTransLineIdToJournalNameMap, Map<Id, String> sInvTransLineIdToReceiptLineNameMap,
                                 Map<Id, String> sInvTransLineIdToRefundLineNameMap, Map<Id, c2g__codaTransactionLineItem__c> transLineMap)
    {
        JVCO_MatchingReferenceDocumentInterface mrdi = null;
        if(!sInvTransLineIdToSInvNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToInvoiceLogic(sInvTransLineIdToSInvNameMap, transLineMap);
        }else if(!sInvTransLineIdToCNoteNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToCreditNoteLogic(sInvTransLineIdToCNoteNameMap, transLineMap);
        }else if(!sInvTransLineIdToJournalNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToJournalLogic(sInvTransLineIdToJournalNameMap, transLineMap);
        }else if(!sInvTransLineIdToReceiptLineNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToCashReceiptLogic(sInvTransLineIdToReceiptLineNameMap, transLineMap);
        }else
        {
            mrdi = new JVCO_MatchToCashRefundLogic(sInvTransLineIdToRefundLineNameMap, transLineMap);
        }
        mrdi.execute();
    }
    
    private void getAccountToRunQueable(List<c2g__codaTransactionLineItem__c> scope){
        for(c2g__codaTransactionLineItem__c tli : scope)
        {
            if(!licAndCustAccIdMap.containsKey(tli.c2g__Account__c) && tli.c2g__Account__c != null && tli.c2g__Account__r.JVCO_Customer_Account__c != null){
                licAndCustAccIdMap.put(tli.c2g__Account__c, tli.c2g__Account__r.JVCO_Customer_Account__c);
            }
        }
    }
    
    private void removeReferenceDocument(List<c2g__codaTransactionLineItem__c> transLineList)
    {
        Set<c2g__codaInvoice__c> sInvSet = new Set<c2g__codaInvoice__c>();
        for(c2g__codaTransactionLineItem__c transLine : transLineList)
        {
            c2g__codaInvoice__c sInv = new c2g__codaInvoice__c();
            sInv.Id = transLine.c2g__Transaction__r.c2g__SalesInvoice__c;
            sInv.JVCO_Reference_Document__c = null;
            sInvSet.add(sInv);
        }
        update new List<c2g__codaInvoice__c>(sInvSet);
    }

    public void finish(Database.BatchableContext bc)
    {
        JVCO_JournalBackgroundMatchingBatch jbmb = new JVCO_JournalBackgroundMatchingBatch();
        jbmb.licAndCustAccIdMap = licAndCustAccIdMap;
        Database.executeBatch(jbmb, 1);
    }
}