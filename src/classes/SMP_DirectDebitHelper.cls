public with sharing class SMP_DirectDebitHelper{
    public static Map<Id, Income_Direct_Debit__c> getParentDirectDebitsFromHistories(List<Income_Debit_History__c> historiesMap){
        System.debug('#### getParentDirectDebitsFromHistories');

        Set<Id> directDebitIds = new Set<Id>();

        for(Income_Debit_History__c history : historiesMap){
            directDebitIds.add(history.Income_Direct_Debit__c);
        }

        Map<Id, Income_Direct_Debit__c> directDebits = new Map<Id, Income_Direct_Debit__c>([SELECT Id, DD_Account_Email__c 
                                                                                            FROM Income_Direct_Debit__c
                                                                                            WHERE Id IN :directDebitIds]);

        return directDebits;
    }

    public static List<Income_Debit_History__c> getNewInstructions(List<Income_Debit_History__c> histories){
        System.debug('#### getNewInstructions');
        System.debug('#### histories: '+histories);

        List<Income_Debit_History__c> newInstructionsList = new List<Income_Debit_History__c>();

        for(Income_Debit_History__c history : histories){
            if(history.DD_Status__c == 'New Instruction'){
                newInstructionsList.add(history);
            }
        }

        return newInstructionsList;
    }

    public static List<Income_Debit_History__c> getNewInstructionsByStage(List<Income_Debit_History__c> newDDHistories, Map<Id, Income_Debit_History__c> oldDDHistories, String stage){
        System.debug('#### getNewInstructionsByStage');
        System.debug('#### newDDHistories: ' + newDDHistories);
        System.debug('#### oldDDHistories: ' + oldDDHistories);
        System.debug('#### stage: ' + stage);

        List<Income_Debit_History__c> newInstructionsList = new List<Income_Debit_History__c>();

        for(Income_Debit_History__c history : newDDHistories){
            if(history.DD_Status__c == 'New Instruction'){
                if(history.DD_Stage__c == stage && (oldDDHistories == null || oldDDHistories.get(history.Id) == null || oldDDHistories.get(history.Id).DD_Stage__c != stage)){
                    newInstructionsList.add(history);
                }               
            }
        }

        return newInstructionsList;
    }

    public static List<Income_Debit_History__c> getCollections(List<Income_Debit_History__c> histories){
        System.debug('#### getCollections');
        System.debug('#### histories: '+histories);

        List<Income_Debit_History__c> collectionsList = new List<Income_Debit_History__c>();

        for(Income_Debit_History__c history : histories){
            if(history.DD_Status__c == 'First Collection' || history.DD_Status__c == 'Ongoing Collection' || history.DD_Status__c == 'Final Collection'
                || history.DD_Status__c == 'First Represent' || history.DD_Status__c == 'Second Represent' || history.DD_Status__c == 'Third Represent'){
                collectionsList.add(history);
            }
        }

        return collectionsList;
    }

    public static List<Income_Debit_History__c> getCollectionsByStage(List<Income_Debit_History__c> newDDHistories, Map<Id, Income_Debit_History__c> oldDDHistories, String stage){
        System.debug('#### getCollectionsByStage');
        System.debug('#### newDDHistories: ' + newDDHistories);
        System.debug('#### oldDDHistories: ' + oldDDHistories);
        System.debug('#### stage: ' + stage);
        
        List<Income_Debit_History__c> collections = new List<Income_Debit_History__c>();

        for(Income_Debit_History__c history : newDDHistories){
            if(history.DD_Status__c == 'First Collection' || history.DD_Status__c == 'Ongoing Collection' || history.DD_Status__c == 'Final Collection'
                || history.DD_Status__c == 'First Represent' || history.DD_Status__c == 'Second Represent' || history.DD_Status__c == 'Third Represent'){
                if(history.DD_Stage__c == stage && (oldDDHistories == null || oldDDHistories.get(history.Id) == null || oldDDHistories.get(history.Id).DD_Stage__c != stage)){
                    collections.add(history);
                }               
            }
        }

        return collections;
    }

    public static List<Income_Debit_History__c> getHistoriesByReasonCode(List<Income_Debit_History__c> histories, Map<Id, Income_Debit_History__c> oldDDHistories){
        System.debug('#### getHistoriesByReasonCode');
        System.debug('#### histories: ' + histories);
        System.debug('#### oldDDHistories: ' + oldDDHistories);
        //Get Error codes for AUDDIS and ADDACS
        Map<String, JVCO_Payonomy_Error_Codes__c> errorCodesMap = new Map<String, JVCO_Payonomy_Error_Codes__c>();
        for(JVCO_Payonomy_Error_Codes__c errorCode : JVCO_Payonomy_Error_Codes__c.getall().values()){
            if(!errorCodesMap.containsKey(errorCode.JVCO_REASON_CODE__c) 
               && !errorCode.JVCO_Cash_Refund__c
               && errorCode.JVCO_Deliberate__c == 'Deliberate'){
                errorCodesMap.put(errorCode.JVCO_REASON_CODE__c.toLowerCase(), errorCode);
            }
        }
        
        List<Income_Debit_History__c> collectionsList = new List<Income_Debit_History__c>();

        for(Income_Debit_History__c history : histories){
            if(String.isNotBlank(history.DD_Code__c) && errorCodesMap.containsKey(history.DD_Code__c.toLowerCase()) && (oldDDHistories == null || oldDDHistories.get(history.Id) == null || !errorCodesMap.containsKey(history.DD_Code__c.toLowerCase()))){
                collectionsList.add(history);
            }
        }

        return collectionsList;
    }
}