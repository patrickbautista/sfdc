@isTest
private class JVCO_TransOfAccountTypeBatchTest{
    @testSetup 
    static void setup() {
            
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AffiliationTrigger__c = true;
        dt.JVCO_AccountTrigger__c = true;
        insert dt;
        
            Account cusAcc =  JVCO_TestClassObjectBuilder.createCustomerAccount();
            cusAcc.JVCO_Target_Account_Type__c = 'Customer to Key';
            insert cusAcc;

            Account licAcc = JVCO_TestClassObjectBuilder.createLicenceAccount(cusAcc.id);
            licAcc.JVCO_Renewal_Scenario__c = '';
            insert licAcc;

            //List<sObject> lProd = Test.loadData(Product2.SObjectType, 'JVCO_Products');
            
            Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
            prod.proratable__c = false;
            insert prod;

            Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
            insert pb;

            PriceBookEntry pEntry = JVCO_TestClassObjectBuilder.createPriceBookEntry(prod.id);
            insert pEntry;

            Contract cont = JVCO_TestClassObjectBuilder.createContract(licAcc.id, null);
            cont.EndDate = Date.today().addDays(28);
            insert cont;

            JVCO_Venue__c venueRec = JVCO_TestClassObjectBuilder.createVenue();
            insert venueRec;

            JVCO_Venue__c venueRec2 = JVCO_TestClassObjectBuilder.createVenue();
            venueRec2.Name = 'Sample Venue2'; 
            venueRec2.External_Id__c = 'JVCO_QLTHTestsamplevenexidtest2';
            venueRec2.JVCO_Street__c = 'testStreet1234';
            insert venueRec2;

            //create affiliation
            JVCO_Affiliation__c affiliationRec = JVCO_TestClassObjectBuilder.createAffiliation(licAcc.id, venueRec.id);
            affiliationRec.JVCO_Closure_Reason__c = '';
            affiliationRec.JVCO_End_Date__c = null;
            insert affiliationRec;

            JVCO_Affiliation__c affiliationRec2 = JVCO_TestClassObjectBuilder.createAffiliation(licAcc.id, venueRec2.id);
            affiliationRec2.JVCO_Closure_Reason__c = '';
            affiliationRec2.JVCO_End_Date__c = null;
            insert affiliationRec2;

            List<SBQQ__Subscription__c> lSubs = new List<SBQQ__Subscription__c>();

            for(integer i= 0; i < 4; i++){
                
                if(i<2){
                    lSubs.add(new SBQQ__Subscription__c( SBQQ__Product__c = prod.id, SBQQ__ListPrice__c = 1,SBQQ__NetPrice__c = 1, SBQQ__ProrateMultiplier__c = 1, SBQQ__Quantity__c = 3, SBQQ__PartnerDiscount__c = 10, SBQQ__Contract__c = cont.id, Affiliation__c = affiliationRec.id, SBQQ__Account__c = licAcc.id, JVCO_Venue__c = venueRec.id));  
                }

                if(i>=2){
                    lSubs.add(new SBQQ__Subscription__c( SBQQ__Product__c = prod.id, SBQQ__ListPrice__c = 1,SBQQ__NetPrice__c = 1, SBQQ__ProrateMultiplier__c = 1, SBQQ__Quantity__c = 3, SBQQ__PartnerDiscount__c = 10, SBQQ__Contract__c = cont.id, Affiliation__c = affiliationRec2.id, SBQQ__Account__c = licAcc.id, JVCO_Venue__c = venueRec2.id));    
                }
                


            }
            insert lSubs;
    }

    @isTest
    static void testBatch() {        
        Integer toProcess = [SELECT id FROM Account WHERE RecordType.Name = 'Customer Account' and JVCO_Target_Account_Type__c!=null].size();
        JVCO_TransformationOfAccountTypeBatch bc = new JVCO_TransformationOfAccountTypeBatch(null, toProcess);
        Database.executeBatch(bc, 1);

        //List<SBQQ__Subscription__c> subsLIst = [SELECT id from SBQQ__Subscription__c];
        Contract cont = [SELECT id, JVCO_Current_Period__c,  JVCO_Renewal_Generated__c from Contract];

        system.assertEquals(cont.JVCO_Current_Period__c,true);
    }

    @isTest
    static void testBatchInfringement() {

        Account acc = [SELECT id, JVCO_In_Infringement__c from Account where RecordType.Name = 'Licence Account'];
        acc.JVCO_In_Infringement__c = true;
        update acc;

        Integer toProcess = [SELECT id FROM Account WHERE RecordType.Name = 'Customer Account' and JVCO_Target_Account_Type__c!=null].size();
        JVCO_TransformationOfAccountTypeBatch bc = new JVCO_TransformationOfAccountTypeBatch(null, toProcess);
        Database.executeBatch(bc, 1);

        //List<SBQQ__Subscription__c> subsLIst = [SELECT id from SBQQ__Subscription__c];
        Contract cont = [SELECT id, JVCO_Current_Period__c,  JVCO_Renewal_Generated__c from Contract];

        system.assertEquals(cont.JVCO_Current_Period__c,true);
    }

    @isTest
    static void testBatchAgency() {

        Account acc = [SELECT id, JVCO_In_Infringement__c, Type from Account where RecordType.Name = 'Customer Account'];
        acc.Type = 'Agency';
        update acc;
           
        Integer toProcess = [SELECT id FROM Account WHERE RecordType.Name = 'Customer Account' and JVCO_Target_Account_Type__c!=null].size();
        JVCO_TransformationOfAccountTypeBatch bc = new JVCO_TransformationOfAccountTypeBatch(null, toProcess);
        Database.executeBatch(bc, 1);

        //List<SBQQ__Subscription__c> subsLIst = [SELECT id from SBQQ__Subscription__c];
        Contract cont = [SELECT id, JVCO_Current_Period__c,  JVCO_Renewal_Generated__c from Contract];

        system.assertEquals(cont.JVCO_Current_Period__c,true);
    }


    @isTest
    static void testBatchWithDraftQuote() {        
        Account acc=[SELECT id, Name from Account where RecordType.Name = 'Licence Account'] ;
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

         Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc);
        insert opp;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc.id, qProcess.id, opp.id, null, null);
        insert q;

        Integer toProcess = [SELECT id FROM Account WHERE RecordType.Name = 'Customer Account' and JVCO_Target_Account_Type__c!=null].size();
        JVCO_TransformationOfAccountTypeBatch bc = new JVCO_TransformationOfAccountTypeBatch(null, toProcess);
        Database.executeBatch(bc, 1);

        //List<SBQQ__Subscription__c> subsLIst = [SELECT id from SBQQ__Subscription__c];
        Contract cont = [SELECT id, JVCO_Current_Period__c,  JVCO_Renewal_Generated__c from Contract];

        system.assertEquals(cont.JVCO_Current_Period__c,true);
    }


    @isTest
    static void testBatchKeyToCus() {        
        Account cusAcc =  JVCO_TestClassObjectBuilder.createCustomerAccount();
        cusAcc.JVCO_Target_Account_Type__c = 'Key to Customer';
        cusAcc.Type = 'Key Account';
        cusAcc.Name = 'test Ice';
        insert cusAcc;

        Contact contRec = [SELECT id, AccountId from Contact limit 1];
        contRec.AccountId = cusAcc.id;
        update contRec;

        Account licAcc = [SELECT id, Name, JVCO_Customer_Account__c from Account where RecordType.Name = 'Licence Account' limit 1];
        licAcc.JVCO_Renewal_Scenario__c = '';
        licAcc.Type = 'Key Account';
        licAcc.Name = 'test Ice2';
        licAcc.JVCO_Customer_Account__c = cusAcc.id;
        update licAcc;

        //List<sObject> lProd = Test.loadData(Product2.SObjectType, 'JVCO_Products');
        
        Product2 prod = [SELECT id from Product2 limit 1];

        Pricebook2 pb = [SELECT id from Pricebook2 limit 1];
        
        Contract cont = JVCO_TestClassObjectBuilder.createContract(licAcc.id, null);
        cont.EndDate = Date.today().addDays(28);
        
        insert cont;

        JVCO_Venue__c venueRec = JVCO_TestClassObjectBuilder.createVenue();
        venueRec.Name = 'Sample Venue689'; 
        venueRec.External_Id__c = 'JVCO_QLTHTestsamplevenexidtest678';
        venueRec.JVCO_Street__c = 'test89Street123434';
        insert venueRec;

        JVCO_Venue__c venueRec2 = JVCO_TestClassObjectBuilder.createVenue();
        venueRec2.Name = 'Sample Venue5'; 
        venueRec2.External_Id__c = 'JVCO_QLTHTestsamplevenexidtest5';
        venueRec2.JVCO_Street__c = 'testStreet';
        insert venueRec2;

        //create affiliation
        JVCO_Affiliation__c affiliationRec = JVCO_TestClassObjectBuilder.createAffiliation(licAcc.id, venueRec.id);
        affiliationRec.JVCO_Closure_Reason__c = '';
        affiliationRec.JVCO_End_Date__c = null;
        insert affiliationRec;

        JVCO_Affiliation__c affiliationRec2 = JVCO_TestClassObjectBuilder.createAffiliation(licAcc.id, venueRec2.id);
        affiliationRec2.JVCO_Closure_Reason__c = '';
        affiliationRec2.JVCO_End_Date__c = null;
        insert affiliationRec2;

        List<SBQQ__Subscription__c> lSubs = new List<SBQQ__Subscription__c>();

        for(integer i= 0; i < 4; i++){
            
            if(i<2){
                lSubs.add(new SBQQ__Subscription__c( SBQQ__Product__c = prod.id, SBQQ__ListPrice__c = 1,SBQQ__NetPrice__c = 1, SBQQ__ProrateMultiplier__c = 1, SBQQ__Quantity__c = 3, SBQQ__PartnerDiscount__c = 10, SBQQ__Contract__c = cont.id, Affiliation__c = affiliationRec.id, SBQQ__Account__c = licAcc.id, JVCO_Venue__c = venueRec.id));  
            }

            if(i>=2){
                lSubs.add(new SBQQ__Subscription__c( SBQQ__Product__c = prod.id, SBQQ__ListPrice__c = 1,SBQQ__NetPrice__c = 1, SBQQ__ProrateMultiplier__c = 1, SBQQ__Quantity__c = 3, SBQQ__PartnerDiscount__c = 10, SBQQ__Contract__c = cont.id, Affiliation__c = affiliationRec2.id, SBQQ__Account__c = licAcc.id, JVCO_Venue__c = venueRec2.id));    
            }
            


        }
        insert lSubs;
        
        Integer toProcess = [SELECT id FROM Account WHERE RecordType.Name = 'Customer Account' and JVCO_Target_Account_Type__c!=null].size();
        JVCO_TransformationOfAccountTypeBatch bc = new JVCO_TransformationOfAccountTypeBatch(null, toProcess);
        Database.executeBatch(bc, 1);

       
        Contract contCheck = [SELECT id, JVCO_Current_Period__c,  JVCO_Renewal_Generated__c from Contract where id =: cont.id];

        system.assertEquals(contCheck.JVCO_Current_Period__c,true);
    }

    @isTest
    static void adHocTesting() {

        Set<id> adhocTest = new Set<id>();
        Account acc = [SELECT id from Account where RecordType.Name = 'Customer Account'];
        adhocTest.add(acc.Id);

        Integer toProcess = [SELECT id FROM Account WHERE RecordType.Name = 'Customer Account' and JVCO_Target_Account_Type__c!=null].size();
        JVCO_TransformationOfAccountTypeBatch bc = new JVCO_TransformationOfAccountTypeBatch(adhocTest, toProcess);
        Database.executeBatch(bc, 1);


    }


    @isTest
    static void helperWithFail(){

       List<Account> accLists =  [SELECT id, Name, Transformation__c, JVCO_Customer_Account__r.JVCO_Target_Account_Type__c, JVCO_Customer_Account__c, JVCO_Customer_Account__r.JVCO_Account_Type_Change__c, JVCO_In_Enforcement__c, JVCO_In_Infringement__c, Type, JVCO_Customer_Account__r.Type, JVCO_Review_Type__c FROM Account WHERE JVCO_Customer_Account__c != null AND (JVCO_Customer_Account__r.JVCO_Target_Account_Type__c = 'Customer to Key' OR JVCO_Customer_Account__r.JVCO_Target_Account_Type__c = 'Key to Customer') AND JVCO_Live__c != true AND JVCO_Closure_Reason__c = null AND JVCO_Closure_Reason__c = null];


        Map<String, Set<String>> tmp = new Map<String, Set<String>>();
        

        if(accLists.size() > 0){
            system.debug('Account List' + accLists);
            tmp = JVCO_TransOfAccountTypeHelper.doTransformation(accLists, true);
        }
    }

    @isTest
    static void testScheduler(){

        String CRON_EXP = '0 0 0 15 3 ? *';
        
        Test.startTest();

            String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new JVCO_TransOfAccTypeScheduler());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);

        Test.stopTest();
    }


    @isTest
    static void testPageController(){
        
        Test.startTest();

            JVCO_SchedTransBatchExtension schedPageController = new JVCO_SchedTransBatchExtension();
            schedPageController.redirectToSched();

        Test.stopTest();
    }


}