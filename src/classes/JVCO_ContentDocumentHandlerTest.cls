@isTest()
private class JVCO_ContentDocumentHandlerTest{
    
    private static Account licAcc;
     
    @testSetup
    public static void setupData(){
        Profile p = [select id from Profile where Name = 'Licensing User' limit 1];
        User runningUser = new User();
        UserRole uRole = new UserRole();

        uRole.DeveloperName = 'JVCO_Generic_Test_User'; 
        uRole.Name = 'Generic Test User';
        insert uRole;

        runningUser = new User(
            Alias = 'athr',
            Email='JVCO_AccountTriggerHandlerTest1@gmail.com', 
            EmailEncodingKey='UTF-8',
            FirstName = 'Generic', 
            LastName='User',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            UserRoleId = uRole.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='guuserstand3541987*@testorg.com',
            Division='Legal',
            Department='Legal'
        );
        
        insert runningUser;
        
        PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors' limit 1];
        PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = ps.id, AssigneeId = runningUser.id);
        insert psa;
    }
    
    static void prepareTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        Test.startTest();
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        insert glaList;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc.JVCO_In_Enforcement__c = false;
        licAcc.JVCO_Credit_Status__c = 'Enforcement - Debt Recovery';
        insert licAcc;
        
        Test.stopTest();
    }
    
    private static testMethod void testInsertContentDocumentLinkWithRestriction(){
        prepareTestData();
        
        User runUser = [SELECT Id FROM User WHERE Email = 'JVCO_AccountTriggerHandlerTest1@gmail.com'];
        System.assertNotEquals(runUser, null);
        Boolean errorExists = false;
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
    
        insert contentVersion_1;
    
        ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        
        System.runAs(runUser){
            
            ContentDocumentLink link = new ContentDocumentLink();
            link.LinkedEntityId = licAcc.Id;
            link.ContentDocumentId = contentVersion_2.ContentDocumentId;
            
            try{
                insert link;
            }
            catch(Exception e){
                errorExists = true;
            }
        }
        
        System.assertEquals(true, errorExists);
    }
    
    private static testMethod void testEditContentDocumentWithRestriction(){
        prepareTestData();
        
        User runUser = [SELECT Id FROM User WHERE Email = 'JVCO_AccountTriggerHandlerTest1@gmail.com'];
        System.assertNotEquals(runUser, null);
        Boolean errorExists = false;
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
    
        insert contentVersion_1;
    
        ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        
        System.runAs(runUser){
            try{
                ContentDocument contentDoc = new ContentDocument();
                contentDoc.Id = contentVersion_2.ContentDocumentId;
                contentDoc.Description = 'This is an updated test description';
                update contentDoc;
            }
            catch(Exception e){
                errorExists = true;
            }
        }
        
        System.assertEquals(true, errorExists);
    }
    
    private static testMethod void testDeleteContentDocumentWithRestriction(){
        prepareTestData();
        
        User runUser = [SELECT Id FROM User WHERE Email = 'JVCO_AccountTriggerHandlerTest1@gmail.com'];
        System.assertNotEquals(runUser, null);
        Boolean errorExists = false;
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
    
        insert contentVersion_1;
    
        ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        ContentDocument contentDocument = [SELECT Id FROM ContentDocument WHERE Id = :contentVersion_2.ContentDocumentId];
        
        System.runAs(runUser){    
            try{
                delete contentDocument;
            }
            catch(Exception e){
                errorExists = true;
            }
        }
        
        System.assertEquals(true, errorExists);
        delete contentDocument;
    }
    
    private static testMethod void testcheckUserIfRestricted(){
       //prepareTestData();
        
        User runUser = [SELECT Id FROM User WHERE Email = 'JVCO_AccountTriggerHandlerTest1@gmail.com'];
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        //System.runAs(runUser)
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        Test.startTest();
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        insert glaList;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        
        JVCO_ContentDocumentLinkTriggerHandler.checkUserIfRestricted();
        
        Test.stopTest();
        
        //PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors'];
        //insert new PermissionSetAssignment(AssigneeId = runUser.id, PermissionSetId = ps.Id);
  
        System.runAs(runUser){ 
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc.JVCO_In_Enforcement__c = false;
        licAcc.JVCO_Credit_Status__c = 'Enforcement - Debt Recovery';
        insert licAcc;   
        //System.assertNotEquals(runUser, null);
        Boolean errorExists = false;
        //Account acct = [SELECT Id FROM Account WHERE name='Test Account 83202-Lic'];
        licAcc.JVCO_In_Enforcement__c = true;
        update licAcc;
            ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true);
        
    
        insert contentVersion_1;
        ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        ContentDocument contentDocument = [SELECT Id FROM ContentDocument WHERE Id = :contentVersion_2.ContentDocumentId];
        
        }
    }
}