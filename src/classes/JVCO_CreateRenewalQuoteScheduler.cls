/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_CreateRenewalQuoteScheduler.cls 
   Description:     Class that handles CreateRenewalQuote Schedulable APEX class as per JIRA ticket GREEN-9202
   Test class:      JVCO_CreateRenewalQuoteSchedulerTest.cls 

   Date                 Version     Author                          Summary of Changes 
   -----------          -------     -----------------               -------------------------------------------
  20-Dec-2016           0.1         Accenture-Rolando Valencia      Intial draft
  21-Dec-2016           0.2         Accenture-Rolando Valencia      Added comments and method signatures.
  31-Jan-2017           0.3         Accenture-Rolando Valencia      Added CRON schedule.              
  ------------------------------------------------------------------------------------------------ */
global class JVCO_CreateRenewalQuoteScheduler implements Schedulable
{
    global void execute (SchedulableContext sc)
    {
        JVCO_CreateRenewalQuoteBatch createRQBatch = new JVCO_CreateRenewalQuoteBatch();
        //JVCO_CreateRenewalQuoteScheduler createRQSch = new  JVCO_CreateRenewalQuoteScheduler();
        //String cronStr = '0 0 23 * * ?';
        //String jobID = System.schedule('Process CompleteRenewalQuoteScheduler', cronStr, createRQSch);
        Database.executeBatch(createRQBatch, 1);
    }
    
    private static String getSchedulerExpression(DateTime dt) 
    {
         return '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();    
    }

    public static void CallExecuteMomentarily()  
    {
        System.schedule('JVCO_CreateRenewalQuoteScheduler', getSchedulerExpression(DateTime.Now().addSeconds(5)), new JVCO_CreateRenewalQuoteScheduler());
    }
}