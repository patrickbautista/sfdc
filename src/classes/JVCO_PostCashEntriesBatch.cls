/* ----------------------------------------------------------------------------------------------
    Name: JVCO_PostCashEntriesBatch.cls 
    Description: Batch class that Bulk Post Cash entries         
    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    11-Apr-2017  0.1         kristoffer.d.martin   Intial creation
    21-Jul-2017  0.2         franz.g.a.dimaapi     Refactor
    06-Sept-2017  0.3        desiree.m.quijada     Capture error and insert it to the error log object
----------------------------------------------------------------------------------------------- */
global class JVCO_PostCashEntriesBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    private static List<c2g__codaCashEntryLineItem__c> cashEntryLineList;

    private static void init() {

        // Loop Cash Entry Line and Add CashEntry Id to Set
        Set<Id> cashEntryIdSet = new Set<Id>();
        for(c2g__codaCashEntryLineItem__c cashEntryLineItem : cashEntryLineList)
        {
            cashEntryIdSet.add(cashEntryLineItem.c2g__CashEntry__c);
        }
        
        // Post Cash Entry
        if(cashEntryIdSet.size() > 0)
        {
            postCashEntry(cashEntryIdSet);
        }
        
    }

    // Change Query Logic - franz.g.a.dimaapi - 07/21/2017
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id, c2g__AccountReference__c, c2g__CashEntry__c
                                         FROM c2g__codaCashEntryLineItem__c
                                         WHERE c2g__Account__c != null
                                         AND c2g__CashEntry__r.c2g__Status__c = 'In Progress']);

    }

    // Change from CashEntry to CashEntryLine - franz.g.a.dimaapi - 07/21/2017
    global void execute(Database.BatchableContext bc, List<c2g__codaCashEntryLineItem__c> scope)
    {
        cashEntryLineList = scope;
        init();            
    }

    global void finish(Database.BatchableContext BC) 
    {   
        System.debug('@@JVCO_PostCashEntriesBatch Done');
    }

    /* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Posts cash entry record
    Inputs: Id
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    05-May-2016 kristoffer.d.martin Initial version of function
    21-Jul-2017 franz.g.a.dimaapi   Refactor Code
    ----------------------------------------------------------------------------------------------- */
    private static void postCashEntry(Set<Id> cashEntryIdSet)
    {
        System.debug('Posting...');
        c2g.CODAAPICommon_7_0.Context context = new c2g.CODAAPICommon_7_0.Context();
        
        List<c2g.CODAAPICommon.Reference> referenceList = new List<c2g.CODAAPICommon.Reference>();
        
        for (Id cashEntryId : cashEntryIdSet)
        {
            // Moved the reference from outside the loop to inside the loop - franz.g.a.dimaapi
            c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = cashEntryId;
            referenceList.add(reference);
        }

        try {
            //Call Bulk Posting for CashEntry API
            c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(context, referenceList);
            System.debug('@@Bulk Post CashEntry Done');
        } catch (Exception e) {
            System.debug(e.getMessage() + e.getTypeName());
            
            ffps_custRem__Custom_Log__c newErr = new ffps_custRem__Custom_Log__c();            
            newErr.ffps_custRem__Running_User__c = UserInfo.getUserId();
            newErr.ffps_custRem__Detail__c = string.valueof(e.getMessage());
            newErr.ffps_custRem__Message__c = 'Posting of Cash Entry';
            newErr.ffps_custRem__Grouping__c = 'PCB-001';
            
            if(newErr!=null){
                insert newErr;
            }
        }
        
    }
    
}