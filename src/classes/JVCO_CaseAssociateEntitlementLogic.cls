/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CaseAssociateEntitlementLogic.cls 
   Description: Business logic class for associating Entitlement to a Case record

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  12-Oct-2016   0.1         ryan.i.r.limlingan  Intial creation
  13-Oct-2016   0.2         ryan.i.r.limlingan  Corrected picklist values being checked
                                                in linkEntitlementToCase
  14-Oct-2016   0.3         ryan.i.r.limlingan  Added @future annotation in setSLAViolationTime()
  18-Oct-2016   0.4         ryan.i.r.limlingan  Modified setSLAViolationTime() to include setting
                                                of custom fields for SLA Violation Time
  24-Oct-2016   0.5         ryan.i.r.limlingan  Added references to utility class
  ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_CaseAssociateEntitlementLogic
{

    // Flag to check if there are Case records to update after insert
    public static Boolean shouldSetSLAViolationTime = FALSE;

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Function associates an Entitlement to a Case record
    Inputs: List<Case>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    12-Oct-2016 ryan.i.r.limlingan  Initial version of function
    13-Oct-2016 ryan.i.r.limlingan  Added setSLAViolationTime function
    24-Oct-2016 ryan.i.r.limlingan  Added calls to utility class
  ----------------------------------------------------------------------------------------------- */
    public static void linkEntitlementToCase(List<Case> cases)
    {
        // Call function to ensure Custom Setting variables have been loaded
        JVCO_CaseUtils.loadCustomSettings();

        String entitlementName = JVCO_CaseUtils.slaConstants.JVCO_Entitlement_Name__c;
        String originsWithEnt = JVCO_CaseUtils.slaConstants.JVCO_Case_Origins_with_Entitlement__c;

        Entitlement e = [SELECT Id FROM Entitlement WHERE Name=:entitlementName LIMIT 1];
        for (Case c : cases)
        {
            // Check if the Case Origin is in the defined Custom Setting
            if (originsWithEnt.contains(c.Origin))
            {
                c.EntitlementId = e.Id;
                shouldSetSLAViolationTime = TRUE;
            }
        }
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Sets JVCO_SLA_Violation_Time__c of a Case record
    Inputs: List<Id>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    13-Oct-2016 ryan.i.r.limlingan  Initial version of function
    14-Oct-2016 ryan.i.r.limlingan  Added @future annotation
    18-Oct-2016 ryan.i.r.limlingan  Added handling of two new custom fields for SLA Violation Times
    24-Oct-2016 ryan.i.r.limlingan  Added calls to utility class; changed logic to handle bulk
                                    update of Case records
  ----------------------------------------------------------------------------------------------- */
    @future
    public static void setSLAViolationTime(List<Id> caseIdList)
    {
        // Call function to ensure Custom Setting variables have been loaded
        JVCO_CaseUtils.loadCustomSettings();

        List<Case> casesToUpdate = [SELECT Id, Origin, JVCO_First_Response_SLA_Violation_Time__c,
                                    JVCO_Resolution_Time_SLA_Violation_Time__c FROM Case
                                    WHERE Id IN :caseIdList];

        List<Id> caseIdsWithFRSla = new List<Id>();
        List<Id> caseIdsWithRTSla = new List<Id>();

        String firstMilestoneOrigin = JVCO_CaseUtils.slaConstants.JVCO_First_Milestone_Origin__c;
        String secMilestoneOrigin = JVCO_CaseUtils.slaConstants.JVCO_Second_Milestone_Origin__c;
        for (Case c : casesToUpdate)
        {
            // Check which Case records have the appropriate Milestone
            // Ex. Case record with Origin White Mail has only Resolution Time SLA
            if (firstMilestoneOrigin.contains(c.Origin))
            {
                caseIdsWithFRSla.add(c.Id);
            }

            if (secMilestoneOrigin.contains(c.Origin))
            {
                caseIdsWithRTSla.add(c.Id);
            }
        }

        // Retrieve milestone name from Custom Setting
        String milestoneName = JVCO_CaseUtils.slaConstants.JVCO_First_Milestone__c + '%';
        Map<Id, Datetime> caseIdFRTargetTimeMap = new Map<Id, Datetime>();
        // Call utility function to construct map
        JVCO_CaseUtils.constructCaseIdTargetDateMap(caseIdsWithFRSla, milestoneName,
                                                    caseIdFRTargetTimeMap);

        milestoneName = JVCO_CaseUtils.slaConstants.JVCO_Second_Milestone__c + '%';
        Map<Id, Datetime> caseIdRTTargetTimeMap = new Map<Id, Datetime>();
        JVCO_CaseUtils.constructCaseIdTargetDateMap(caseIdsWithRTSla, milestoneName,
                                                    caseIdRTTargetTimeMap);

        for (Case c : casesToUpdate)
        {
            c.JVCO_First_Response_SLA_Violation_Time__c = caseIdFRTargetTimeMap.get(c.Id);
            c.JVCO_Resolution_Time_SLA_Violation_Time__c = caseIdRTTargetTimeMap.get(c.Id);
        }

        if (!casesToUpdate.isEmpty())
        {
            update casesToUpdate;
        }
    }
}