/*----------------------------------------------------------------------------------------------
    Name: JVCO_InvoiceAndCreditNoteScheduler.cls
    Test:JVCO_InvoiceAndCreditNoteSchedBatchTest
    Description: Scheduler to execute Invoice and Credit Note Batch - JVCO_InvoiceAndCreditNoteSchedulerBatch
    
    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    19-Apr-2021  0.1         patrick.t.bautista     Initial creation
----------------------------------------------------------------------------------------------- */
global class JVCO_InvoiceAndCreditNoteScheduler implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
        final JVCO_General_Settings__c GeneralSettings = JVCO_General_Settings__c.getInstance();
        final Decimal INVOICESCHEDSCOPE = GeneralSettings.JVCO_Invoice_Scheduler_per_run__c != null ? GeneralSettings.JVCO_Invoice_Scheduler_per_run__c : 200;
        
        Database.executeBatch(new JVCO_InvoiceAndCreditNoteSchedulerBatch(), Integer.valueOf(INVOICESCHEDSCOPE));
    }
    
    private static String getSchedulerExpression(DateTime dt) 
    {
        return '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();    
    }
    
    public static void CallExecuteMomentarily()  
    {
        System.schedule('JVCO_InvoiceAndCreditNoteScheduler', getSchedulerExpression(DateTime.Now().addSeconds(5)), new JVCO_InvoiceAndCreditNoteScheduler());
    }
}