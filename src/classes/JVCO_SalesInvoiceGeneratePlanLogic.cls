/* ----------------------------------------------------------------------------------------------
    Name: JVCO_PayonomyPaymentDirectDebitLogic.cls 
    Description: Business logic class for generating instalment line items for Invoice records

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    26-Jan-2017   0.1         ryan.i.r.limlingan  Intial creation
    04-May-2017   0.2         ryan.i.r.limlingan  Changed logic for createInstalmentPlanPerInvoice()
    ----------------------------------------------------------------------------------------------- */
public class JVCO_SalesInvoiceGeneratePlanLogic
{
    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Filters Sales Invoice records based on criteria and generates instalment plans
        Inputs: List<c2g__codaInvoice__c>, Map<Id,c2g__codaInvoice__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        26-Jan-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static void processInvoice(List<c2g__codaInvoice__c> invoiceList, Map<Id,c2g__codaInvoice__c> oldInvoicesMap)
    {
        List<c2g__codaInvoice__c> invoiceToGeneratePlanList = new List<c2g__codaInvoice__c>();
        for(c2g__codaInvoice__c invoice : invoiceList)
        {
            c2g__codaInvoice__c oldInvoice = oldInvoicesMap.get(invoice.Id);
            if(invoice.JVCO_Generate_Payment_Schedule__c != oldInvoice.JVCO_Generate_Payment_Schedule__c && invoice.JVCO_Generate_Payment_Schedule__c)
            {
                invoiceToGeneratePlanList.add(invoice);
            }else if (invoice.JVCO_Generate_Payment_Schedule__c && (invoice.JVCO_Number_of_Payments__c != oldInvoice.JVCO_Number_of_Payments__c ||
                invoice.JVCO_First_Due_Date__c != oldInvoice.JVCO_First_Due_Date__c))
            {
                invoiceToGeneratePlanList.add(invoice);
            }
        }

        if(!invoiceToGeneratePlanList.isEmpty())
        {
            delete [SELECT Id FROM c2g__codaInvoiceInstallmentLineItem__c WHERE c2g__Invoice__c IN :invoiceToGeneratePlanList];
            generateInstalmentPlans(invoiceToGeneratePlanList);
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Creates Sales Invoice Line Item records for the passed Sales Invoice records
        Inputs: List<c2g__codaInvoice__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        26-Jan-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static void generateInstalmentPlans(List<c2g__codaInvoice__c> invoiceList)
    {
        List<c2g__codaInvoiceInstallmentLineItem__c> instalmentLineItemList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
        for(c2g__codaInvoice__c invoice : invoiceList)
        {
            instalmentLineItemList.addAll(createInstalmentPlanPerInvoice(invoice));
        }
        if(!instalmentLineItemList.isEmpty())
        {
            insert instalmentLineItemList;
            JVCO_FFUtil.stopCodaInvoiceHandlerAfterUpdate = true;
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Creates Sales Invoice Line Item records for the passed Sales Invoice records
        Inputs: List<c2g__codaInvoice__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        26-Jan-2017 ryan.i.r.limlingan  Initial version of function
        04-May-2017 ryan.i.r.limlingan  Changed logic to consider decimal values in the computation
    ----------------------------------------------------------------------------------------------- */
    private static List<c2g__codaInvoiceInstallmentLineItem__c> createInstalmentPlanPerInvoice(c2g__codaInvoice__c invoice)
    {
        Integer numOfPayments = Integer.valueOf(invoice.JVCO_Number_Of_Payments__c);
        Integer ongoingCollectionAmt = Integer.valueOf(invoice.c2g__OutstandingValue__c) / numOfPayments;
        Double finalCollectionAmt = invoice.c2g__OutstandingValue__c - (ongoingCollectionAmt * (numOfPayments - 1));
        
        List<c2g__codaInvoiceInstallmentLineItem__c> itemList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
        for(Integer i = 1; i <= numOfPayments; i++)
        {
            c2g__codaInvoiceInstallmentLineItem__c item = new c2g__codaInvoiceInstallmentLineItem__c();
            item.c2g__Invoice__c = invoice.Id;
            item.c2g__Amount__c = (i == numOfPayments) ? finalCollectionAmt
                                                       : ongoingCollectionAmt;
            item.c2g__DueDate__c = Date.newInstance(invoice.JVCO_First_Due_Date__c.year(),
                                                    invoice.JVCO_First_Due_Date__c.month() + i - 1,
                                                    invoice.JVCO_First_Due_Date__c.day());
            itemList.add(item);
        }
        return itemList;
    }

}