/* ----------------------------------------------------------------------------------------------
Name: JVCO_LinkSubscriptionToProdFeat.cls 
Description: Batch class that handles Subscription Linking to the Product Feature and Other Subscription

Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
07-Feb-2017  0.1         franz.g.a.dimaapi   Intial creation
----------------------------------------------------------------------------------------------- */
global class JVCO_LinkSubscriptionToProdFeat implements Database.Batchable<sObject>, Database.Stateful{
        
    private static Map<String, ID> subscriptionIdMap;
    private static Map<String, ID> productFeatIdMap;
    private static List<SBQQ__Subscription__c> subscriptionList;

    private static void init(){

        Set<String> productFeatIdSet = new Set<String>();
        Set<String> subscriptionIdSet = new Set<String>();
        productFeatIdMap = new Map<String, ID>();
        subscriptionIdMap = new Map<String, ID>();


        // Add Temp Required by Id to Product Feature set
        // Add Temp Dynamic Option by ID to subscription set
        for(SBQQ__Subscription__c subscription : subscriptionList){

            if(subscription.JVCO_Temp_RequiredById__c != null && subscription.JVCO_Temp_RequiredById__c != ''){
                subscriptionIdSet.add(subscription.JVCO_Temp_RequiredById__c);
            }
            if(subscription.JVCO_Temp_DynamicOptionId__c != null && subscription.JVCO_Temp_DynamicOptionId__c != ''){
                productFeatIdSet.add(subscription.JVCO_Temp_DynamicOptionId__c);
            }
        }

        system.debug('@@@*** subscriptionIdSet debug: ' + subscriptionIdSet);
        system.debug('@@@*** productFeatIdSet debug: ' + productFeatIdSet);

        List<SBQQ__ProductFeature__c> prodFeatureList = [SELECT Id, JVCO_Temp_PF_Ext_ID__c FROM SBQQ__ProductFeature__c WHERE JVCO_Temp_PF_Ext_ID__c in :productFeatIdSet];
        List<SBQQ__Subscription__c> subscrList = [SELECT Id, JVCO_Subscription_Temp_External_ID__c FROM SBQQ__Subscription__c WHERE JVCO_Subscription_Temp_External_ID__c = :subscriptionIdSet];

        system.debug('@@@*** prodFeatureList debug: ' + prodFeatureList);
        system.debug('@@@*** subscrList debug: ' + subscrList);

        for(SBQQ__ProductFeature__c prodFeature: prodFeatureList){

            productFeatIdMap.put(prodFeature.JVCO_Temp_PF_Ext_ID__c, prodFeature.Id);            

        }

        for(SBQQ__Subscription__c subscription: subscrList){

            subscriptionIdMap.put(subscription.JVCO_Subscription_Temp_External_ID__c, subscription.Id);            

        }
    }
    
    private static void updateSubscription(){
     
        List<SBQQ__Subscription__c> subscriptionToUpdateList = new List<SBQQ__Subscription__c>();
        for(SBQQ__Subscription__c subscription : subscriptionList)
        {   
            if((subscription.JVCO_Temp_RequiredById__c != null && subscription.JVCO_Temp_RequiredById__c != '')
              || (subscription.JVCO_Temp_DynamicOptionId__c != null && subscription.JVCO_Temp_DynamicOptionId__c != ''))
            {
                
                //if(subscriptionIdMap.containsKey(subscription.JVCO_Temp_DynamicOptionId__c))
                if(productFeatIdMap.containsKey(subscription.JVCO_Temp_DynamicOptionId__c))
                {
                    //subscription.SBQQ__DynamicOptionId__c = subscriptionIdMap.get(subscription.JVCO_Temp_DynamicOptionId__c);
                    subscription.SBQQ__DynamicOptionId__c = productFeatIdMap.get(subscription.JVCO_Temp_DynamicOptionId__c);
                }
                //if(productFeatIdMap.containsKey(subscription.JVCO_Temp_RequiredById__c))
                if(subscriptionIdMap.containsKey(subscription.JVCO_Temp_RequiredById__c))
                {
                    //subscription.SBQQ__RequiredById__c = productFeatIdMap.get(subscription.JVCO_Temp_RequiredById__c);
                    subscription.SBQQ__RequiredById__c = subscriptionIdMap.get(subscription.JVCO_Temp_RequiredById__c);
                }

                subscriptionToUpdateList.add(subscription);
            }
        }

        update subscriptionToUpdateList;
    }

    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id, Name, JVCO_Temp_DynamicOptionId__c, JVCO_Temp_RequiredById__c, 
                                            SBQQ__RequiredById__c, SBQQ__DynamicOptionId__c FROM SBQQ__Subscription__c 
                                            WHERE JVCO_Temp_RequiredById__c NOT IN ('',NULL) OR JVCO_Temp_DynamicOptionId__c NOT IN ('',NULL)]);
    }

    global void execute(Database.BatchableContext bc, List<SBQQ__Subscription__c> scope)
    {
        subscriptionList = scope;
        init();
        updateSubscription();
                
    }

    global void finish(Database.BatchableContext BC) 
    {
        
        System.debug('DONE');
    
    }
}