public class JVCO_PostAndMatchController
{
    private ApexPages.StandardSetController ssc;
    private Id cNoteId;

    public JVCO_PostAndMatchController(ApexPages.StandardSetController ssc)
    {
        this.ssc = ssc;
        cNoteId = ApexPages.currentPage().getParameters().get('id');
    }

    public PageReference init()
    {   
        Boolean isPosted = false;
        Boolean isValid = false;
        String sInvName = '';
        //Always One Record
        for(c2g__codaCreditNote__c cNote : [SELECT Id, c2g__CreditNoteTotal__c, 
                                            JVCO_From_Post_And_Match__c,
                                            JVCO_VAT_Registration_Number__c,
                                            c2g__Invoice__r.c2g__InvoiceTotal__c,
                                            c2g__Invoice__r.Name
                                            FROM c2g__codaCreditNote__c
                                            WHERE c2g__Invoice__r.JVCO_From_Convert_to_Credit__c = TRUE
                                            AND Id = :cNoteId])
        {
            isPosted = cNote.JVCO_From_Post_And_Match__c;
            isValid = (cNote.c2g__CreditNoteTotal__c == cNote.c2g__Invoice__r.c2g__InvoiceTotal__c) && 
                        cNote.JVCO_VAT_Registration_Number__c == NULL;    
            sInvName = cNote.c2g__Invoice__r.Name;
        }
        
        if(isPosted)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.JVCO_StopPostAndMatch));
            return null;
        //If Valid for cancellation
        }else if(isValid)
        {
            //Batch Job Helper - if false, will not continue
            if(!JVCO_BatchJobHelper.checkLargeJobs(1)){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.JVCO_BatchJobHelperCannotProcess));
                return null;
            }
            //End Batch Job Helper
            else{
                bulkPostCreditNote();
                c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
                cNote.Id = cNoteId;
                cNote.JVCO_From_Post_And_Match__c = true;
                cNote.JVCO_Reference_Document__c = sInvName;
                update cNote;
                return new PageReference('/' + cNoteId);        
            }
        }else
        {
            return new PageReference('/apex/c2g__codapostandmatchcreditnote?id=' + cNoteId);
        }
    }

    private void bulkPostCreditNote()
    {
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        List<c2g.CODAAPICommon.Reference> refList = new List<c2g.CODAAPICommon.Reference>();
        c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
        reference.Id = cNoteId;
        refList.add(reference);
            
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        c2g.CODAAPISalesCreditNote_10_0.BulkPostCreditNote(context, refList);
    }

    //Returns to the Sales Credit Note after clicking the 'Back' button
    public PageReference returnToCreditNote()
    {
        PageReference page = new PageReference(ssc.cancel().getUrl());
        page.setRedirect(true);
        return page;
    }
}