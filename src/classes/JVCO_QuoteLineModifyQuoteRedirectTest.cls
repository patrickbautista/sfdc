/*------------------------------------------------------------
Name: JVCO_LinkQuoteLineToProdFeatTest 
Description: test class for JVCO_DeleteInvoiceCommitBatch
Date		Version		Author				Summary of Changes
---------	-------		----------------	--------------------------	
08-23-2017	0.1								Initial draft
08-24-2017	0.2			mary.ann.a.ruelan	

*/

@isTest
private class JVCO_QuoteLineModifyQuoteRedirectTest{    
    
    
    static testMethod void redirectTest() {  
        
        //creating test product and product feature
        Product2 testProduct = JVCO_TestClassObjectBuilder.createProduct();       
        insert testProduct;
        
      
        SBQQ__ProductFeature__c testProdFeat = new SBQQ__ProductFeature__c(Name='NewFeature', SBQQ__Number__c=1, SBQQ__ConfiguredSKU__c=testProduct.Id, SBQQ__MinOptionCount__c=1, JVCO_Temp_PF_Ext_ID__c='Test_000');             
        testProdFeat.JVCO_Temp_PF_Ext_ID__c = 'Test_000';
        insert testProdFeat;
       
        //creating test quote and quote line
        Account customerAcc =  JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert customerAcc;
        
        Contact con = JVCO_TestClassObjectBuilder.createContact(customerAcc.Id);
        con.Firstname = 'Test';
        con.LastName = 'TestContact';
        insert con;
        
        Account licenceAcc = JVCO_TestClassObjectBuilder.createLicenceAccount(customerAcc.Id);
        insert licenceAcc;
        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(licenceAcc);
        insert opp;        
        SBQQ__QuoteProcess__c quoteProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert quoteProcess;        
        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;       
       JVCO_Venue__c venue = JVCO_TestClassObjectBuilder.createVenue();
        insert venue;        
        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(licenceAcc.Id, venue.Id);
        insert aff;       
        SBQQ__Quote__c testQuote = JVCO_TestClassObjectBuilder.createQuote(licenceAcc.Id, quoteProcess.Id, opp.Id, con.Id, 'Annually');
       insert testQuote;
        
        
        //testing           
        test.startTest();
        
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c order by CreatedDate DESC limit 1];
       
     	pageReference pageR = new pageReference('https://cs86.salesforce.com/'+quote.Id);
      System.debug('@@@@pageR: '+pageR);       
        Test.setCurrentPage(pageR);
    
        JVCO_QuoteLineModifyQuoteRedirect redirectPage = new JVCO_QuoteLineModifyQuoteRedirect();
        pageReference newPage = redirectPage.redirect();         
        test.stopTest();
        System.debug(newPage);
        //assertion
        
    }                                           
}