/**************************************************************
 * @Author: Carmela Santos
 * @Company: Accenture
 * @Description: Test class for JVCO_GenerateReviewForm
 * @Created Date:
 * @Revisions:
 *      <Name>              <Date>          <Description>
 *      Jason Mactal        10.16.2017      Deleted Unnecessary Data Setup to avoid exceeding CPU Time Limit
 *                                          Added Assertions
 *      Rhys Dela Cruz      12.04.2019      Added fix for test setup error and code coverage
 *************************************************************/
@isTest
private class JVCO_GenerateReviewForm_test {

    @testSetup
    public static void setupTestData() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        List<JVCO_BackgroundMatching__c> bgMatchingList = new List<JVCO_BackgroundMatching__c>();
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Trading Currency', JVCO_Default_Value__c = 'GBP'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Receivable Control', JVCO_Default_Value__c = '10390'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Output Vat Code', JVCO_Default_Value__c = 'GB-O-STD-LIC'));
        
        insert bgMatchingList;

        c2g__codaGeneralLedgerAccount__c testGeneralLedger =  JVCO_TestClassObjectBuilder.createGeneralLedger();
        insert testGeneralLedger;
        c2g__codaTaxCode__c taxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedger.Id);
        insert taxCode;

        c2g__codaDimension1__c dim1 = new c2g__codaDimension1__c();
        dim1.Name = 'Live';
        dim1.c2g__ReportingCode__c = 'BC012';
        dim1.JVCO_Function__c = 'Budget';
        insert dim1;

        list<Account> customerAcctList = new list<Account>();
        for(integer i=0;i<1;i++){
            Account a1 = new Account();
            a1.RecordTypeId = customerRT;
            a1.Name = 'Test Account 18023-C-'+i;
            a1.Type = 'Customer';
            customerAcctList.add(a1);
        }
        insert customerAcctList;

        list<Contact> contactList = new list<Contact>();
        for(Account custAcc: customerAcctList){
            Contact con = JVCO_TestClassObjectBuilder.createContact(custAcc.id);
            con.FirstName = custAcc.Name+' con';
            contactList.add(con);
        }
        insert contactList;

        list<Account> licenceAcctList = new list<Account> ();
        for(Contact contact : contactList){
            Account a2 = new Account();
            a2.RecordTypeId = licenceRT;
            a2.Name = 'Test Account 18023-L';
            a2.JVCO_Customer_Account__c = contact.AccountID;
            a2.c2g__CODAOutputVATCode__c = taxCode.id;
            a2.JVCO_Review_Form__c  = 'Holiday Centre';
            a2.Type = 'Customer';
            a2.JVCO_Review_Type__c = 'Manual';
            a2.JVCO_Review_Frequency__c  = 'Annually';
            a2.JVCO_Preferred_Contact_Method__c = 'Email';
            a2.ffps_custRem__Preferred_Communication_Channel__c= 'Email';
            a2.JVCO_Billing_Contact__c = contact.id;
            a2.JVCO_Licence_Holder__c = contact.id;
            a2.JVCO_Review_Contact__c = contact.id;
            licenceAcctList.add(a2);
        }
        insert licenceAcctList;

        Contract_Default_Review_Days__c rDate = new Contract_Default_Review_Days__c();
        rDate.Community_Building__c = 42;
        rDate.Discotheques_Dance_Halls__c = 7;
        rDate.Holiday_Centre__c = 42;

        insert rDate;
        
        list<Contract> contList = new list<Contract> ();
        for(Account licAcct: licenceAcctList){
            Contract c1 = new Contract();
            c1.SBQQ__RenewalQuoted__c = false;  
            c1.AccountID = licAcct.Id;
            c1.StartDate = System.today();
            c1.EndDate = System.today() + 42;
            c1.ContractTerm = 1;
            c1.Name = 'Test Contract 18023-L';
            c1.JVCO_Licence_Period_End_Date__c = System.today() + 42;
            c1.JVCO_Requested_Review_Date__c = null;
            c1.JVCO_Review_Type__c = 'Manual';
            c1.SBQQ__RenewalOpportunity__c = null;

            contList.add(c1);
        }
        insert contList;
        system.debug('**** ContactList:' + contList);


        List<JVCO_GenerateReviewFormSettings__c> settings = new List<JVCO_GenerateReviewFormSettings__c>();
        settings.add(new JVCO_GenerateReviewFormSettings__c(Name = 'Limit', Limit__c = '100000'));
        insert settings;
    }

    private static testMethod void generateReviewTest1(){

        Test.startTest();        
        JVCO_GenerateReviewForm batch = new JVCO_GenerateReviewForm ();
        DataBase.executeBatch(batch);
        Test.stopTest();
    }

    private static testMethod void generateReviewTest2(){

        Test.startTest();        
        //JVCO_GenerateReviewForm.isError = true;

        JVCO_GenerateReviewForm batch = new JVCO_GenerateReviewForm ();
        DataBase.executeBatch(batch);
        Test.stopTest();
    }

    private static testMethod void generateReviewTest3(){

        Test.startTest();        

        JVCO_GenerateReviewForm batch = new JVCO_GenerateReviewForm ();
        String sch = '0 0 23 * * ?';
        System.schedule('Review Form Test', sch, batch);
        Test.stopTest();
    }
}