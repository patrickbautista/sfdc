@isTest
public class JVCO_BillNowLogicTest {
    
    @testSetup static void createTestData() 
    {
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        //JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        Test.startTest();
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        insert JVCO_TestClassHelper.setQuoteProcess();
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        JVCO_Venue__c ven = JVCO_TestClassHelper.createVenue();
        insert ven;
        
        Test.stopTest();
        
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        o.Status = 'Activated';
        List<Order> oList = new List<Order>();
        oList.add(o);
        
        Order o1 = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o1;
        OrderItem oi1 = JVCO_TestClassHelper.setOrderItem(o1.Id, pbe.Id, ql.Id);
        insert oi1;
        oList.add(o1);
        update oList;

        JVCO_TestClassHelper.setGeneralSettingsCS();
		JVCO_TestClassHelper.setCashMatchingCS();
    }
    
    @isTest
    static void testCreateTickBillnow()
    {
        SBQQ__Quote__c q = [select id, JVCO_Quote_Id_to_Scheduler__c FROM SBQQ__Quote__c limit 1];
        q.JVCO_Quote_Id_to_Scheduler__c = 'TEST CREDIT REASON';
        update q;
        
        Test.startTest();
        list<order> listOrder = [select id from order limit 1];
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_BillNowLogic billNow = new JVCO_BillNowLogic(sc);
        billNow.init();
        Test.stopTest();
    }

    @isTest
    static void testCreateNeedToBeActivated()
    {
        list<order> listOrder = [select id from order limit 1];
        Test.startTest();
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_BillNowLogic billNow = new JVCO_BillNowLogic(sc);
        billNow.init();
        Test.stopTest();
    }

    @isTest
    static void testCreateOneOrderisBilled()
    {
        Opportunity op = [select id, JVCO_Invoiced__c from opportunity limit 1];
        op.JVCO_Invoiced__c = true;
        update op;
        list<order> listOrder = [select id from order limit 1];
        update listOrder;
        Test.startTest();
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_BillNowLogic billNow = new JVCO_BillNowLogic(sc);
        billNow.init();
        Test.stopTest();
    }

    @isTest
    static void testReturnToAccount()
    {
        list<order> listOrder = [select id from order limit 1];
        Test.startTest();
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_BillNowLogic billNow = new JVCO_BillNowLogic(sc);
        billNow.init();
        billNow.returnToAccount();
        Test.stopTest();
    }

    @isTest
    static void testDMLException()
    {
        list<order> listOrder = [select id, name from order limit 1];
        listOrder[0].Name = '===================================================================================================Some string which is longer than 255 characters================================================================================================================';
        Test.startTest();
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_BillNowLogic billNow = new JVCO_BillNowLogic(sc);
        billNow.init();
        billNow.returnToAccount();
        Test.stopTest();
    }

    @isTest
    static void testVenueSizeError()
    {
        Account testAcc = [select id,JVCO_DD_Mandate_Checker__c from Account limit 1];
        SBQQ__Quote__c testQuote = [select id from SBQQ__Quote__c limit 1];
        SBQQ__QuoteLine__c testQuoteLine = [select id, JVCO_Venue__c from SBQQ__QuoteLine__c limit 1];
        testQuoteLine.JVCO_Venue__c = JVCO_TestClassHelper.createVenue().Id;
        update testQuoteLine;
        Order testOrder = new Order();
        testorder.Accountid = testAcc.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAcc.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        testOrder.JVCO_Credit_Reason__c  = null;
        insert testOrder;  

        PriceBookEntry testPB = [select id from PriceBookEntry limit 1];

        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = testQuoteLine.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = -1.00;
        testOrderItem.EndDate = date.today().addMonths(5);
        testOrderItem.JVCO_Venue__c = testQuoteLine.JVCO_Venue__c;
        insert testOrderItem;
        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
        List<Order> listOrder = new list<Order>();
        listOrder.add(testOrder);
        
        Test.startTest();
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_BillNowLogic billNow = new JVCO_BillNowLogic(sc);
        billNow.init();
        Test.stopTest();
    }

    @isTest
    static void testProductSizeError()
    {
        list<order> listOrder = [select id from order limit 1];
        ApexPages.StandardSetController scAct = new ApexPages.StandardSetController(listOrder);
        scAct.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(scAct);
        actOrder.activate();
        update listOrder;
        
        Test.startTest();
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_BillNowLogic billNow = new JVCO_BillNowLogic(sc);
        billNow.init();
        Test.stopTest();
    }
}