/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_QuoteLineTriggerHandler.cls 
   Description:     Trigger handler for JVCO_QuoteLineTrigger.trigger
   Test class:      JVCO_QuoteLineTriggerHandlerTest.cls 

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
   03-Apr-2017       0.1        samuel.a.d.oberes                 Intial version
   15-Feb-2018       0.2        jules.osberg.a.pablo              Added updateChildrenQuoteLineAccountVenue Method for populating The Venue and Account Fields
   09-Aug-2018       0.3        rhys.j.c.dela.cruz                GREEN-33006 - Added code for prevent delete of Quote Line Group except for Sys Ad
   ---------------------------------------------------------------------------------------------------------- */
public class JVCO_QuoteLineGroupTriggerHandler {

    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_QuoteLineGroupTrigger__c : false;
    //GREEN-25795
    public static Map<Id, SBQQ__QuoteLineGroup__c> qlgMap = new Map<Id, SBQQ__QuoteLineGroup__c>();
    public static Profile currentUserProf = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId()];




    public static Boolean qlgAlreadyUpdatedQliPrimaryTariff { 
        get {
            if (qlgAlreadyUpdatedQliPrimaryTariff == null) {
                qlgAlreadyUpdatedQliPrimaryTariff = false;
            }
            return qlgAlreadyUpdatedQliPrimaryTariff;
        } 
        set; 
    }

    public JVCO_QuoteLineGroupTriggerHandler(){
        // constructor
    }

    public static void onBeforeInsert(List<SBQQ__QuoteLineGroup__c> quoteLineGroupsList) {
        if(!skipTrigger) 
        {
            //trigger code
            updateChildrenQuoteLineAccountVenue(quoteLineGroupsList);
        }
    }
    
    public static void onAfterInsert(List<SBQQ__QuoteLineGroup__c> quoteLineGroupsList) {
        if(!skipTrigger) 
        {
            updateChildrenQuoteLinePrimaryTariff(quoteLineGroupsList, null);
        }
    }
    
    public static void onBeforeUpdate(List<SBQQ__QuoteLineGroup__c> quoteLineGroupsList, Map<Id, SBQQ__QuoteLineGroup__c> oldQuoteLineGroupsMap) {
        if(!skipTrigger) 
        {
            //trigger code
            qlgQueries(quoteLineGroupsList);
            preventUpdateOfGroupName(quoteLineGroupsList, oldQuoteLineGroupsMap);
        }
    }
    
    public static void onAfterUpdate(List<SBQQ__QuoteLineGroup__c> quoteLineGroupsList, Map<Id, SBQQ__QuoteLineGroup__c> oldQuoteLineGroupsMap) {
        if(!skipTrigger) 
        {
            updateChildrenQuoteLinePrimaryTariff(quoteLineGroupsList, oldQuoteLineGroupsMap);
        }
    }

    public static void onBeforeDelete(Map<Id, SBQQ__QuoteLineGroup__c> oldQuoteLineGroupsMap) {
        if(!skipTrigger)
        {
            qlgQueries(oldQuoteLineGroupsMap.values());
            preventQuoteLineGroupDeletion(oldQuoteLineGroupsMap);
        }
    }

    //Query Method
    public static void qlgQueries(List<SBQQ__QuoteLineGroup__c> quoteLineGroupsList) {
        
        Set<Id> qlgIds = new Set<Id>(new Map<Id, SBQQ__QuoteLineGroup__c>(quoteLineGroupsList).keySet());

        List<SBQQ__QuoteLineGroup__c> qlgList = new List<SBQQ__QuoteLineGroup__c>();

        qlgList = [SELECT Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Status__c, SBQQ__Quote__r.JVCO_Cancelled__c FROM SBQQ__QuoteLineGroup__c WHERE Id IN: qlgIds];

        if(!qlgList.isEmpty())
        {
            qlgMap.putAll(qlgList);
        }
    }

    /* ----------------------------------------------------------------------------------------------------------
    Author:         samuel.a.d.oberes
    Company:        Accenture
    Description:    Whenever there are changes in the primary tariff formula field of the QLG record, those changes gets trickled down to its children QLIs.
    Inputs:         List of Quote Line Groups
    Returns:        n/a
    <Date>          <Authors Name>                      <Brief Description of Change> 
    03-Apr-2017     samuel.a.d.oberes                   Initial version
    ----------------------------------------------------------------------------------------------------------*/    
    public static void updateChildrenQuoteLinePrimaryTariff(List<SBQQ__QuoteLineGroup__c> quoteLineGroupsList, Map<Id, SBQQ__QuoteLineGroup__c> oldQuoteLineGroupsMap) {

        Map<Id, String> qlgIdsToPrimaryTariffMap = new Map<Id, String>();
        for (SBQQ__QuoteLineGroup__c qlg : quoteLineGroupsList) {
            if (oldQuoteLineGroupsMap != null) {
                
                // UPDATE SCENARIO
                Boolean primaryTariffChangedAndNotNull = (qlg.JVCO_Primary_Tariff__c != null && qlg.JVCO_Primary_Tariff__c != oldQuoteLineGroupsMap.get(qlg.Id).JVCO_Primary_Tariff__c);
                if (primaryTariffChangedAndNotNull) {
                    qlgIdsToPrimaryTariffMap.put(qlg.Id, qlg.JVCO_Primary_Tariff__c);
                }

            } else {
                
                // INSERT SCENARIO
                if (qlg.JVCO_Primary_Tariff__c != null) {
                    qlgIdsToPrimaryTariffMap.put(qlg.Id, qlg.JVCO_Primary_Tariff__c);
                }

            }
        }

        if (!qlgIdsToPrimaryTariffMap.isEmpty())
        {
            List<SBQQ__QuoteLine__c> quoteLinesToUpdate = [SELECT Id, JVCO_Primary_Tariff__c, SBQQ__Group__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Group__c IN :qlgIdsToPrimaryTariffMap.keySet()];
            for (SBQQ__QuoteLine__c ql : quoteLinesToUpdate) {
                ql.JVCO_Primary_Tariff__c = qlgIdsToPrimaryTariffMap.get(ql.SBQQ__Group__c);
            }

            try {
                update quoteLinesToUpdate;
            } catch (DmlException de) {
                quoteLineGroupsList.get(0).addError('Primary tariff in the quote lines failed to update because of the ff. error(s): ' + de.getMessage());
            }

            JVCO_QuoteLineGroupTriggerHandler.qlgAlreadyUpdatedQliPrimaryTariff = true;
        }
    }

    public static void updateChildrenQuoteLineAccountVenue(List<SBQQ__QuoteLineGroup__c> quoteLineGroupsList) {
        Set<Id> quotesToQuery = new Set<Id>();
        Set<Id> affiliationsToQuery = new Set<Id>();
        Map<Id, SBQQ__Quote__c> quoteMap = new Map<Id, SBQQ__Quote__c>();
        Map<Id, JVCO_Affiliation__c> affiliationMap = new Map<Id, JVCO_Affiliation__c>();

        for (SBQQ__QuoteLineGroup__c qlg :quoteLineGroupsList){
            if (string.isBlank(qlg.SBQQ__Account__c)){
                quotesToQuery.add(qlg.SBQQ__Quote__c); 
            }

            //Added checking if qlg.JVCO_Affiliated_Venue__c is not empty GREEN-30818 -robert.j.b.lacatan
            if (string.isBlank(qlg.JVCO_Venue__c) && !string.isBlank(qlg.JVCO_Affiliated_Venue__c)){
                affiliationsToQuery.add(qlg.JVCO_Affiliated_Venue__c);
            }
        }

        if (quotesToQuery.size() > 0){
            quoteMap = new Map<Id, SBQQ__Quote__c>([SELECT Id, SBQQ__Account__c FROM SBQQ__Quote__c WHERE id in :quotesToQuery]);
        }

        if (affiliationsToQuery.size() > 0){
            affiliationMap = new Map<Id, JVCO_Affiliation__c>([SELECT Id, JVCO_Venue__c FROM JVCO_Affiliation__c WHERE id in :affiliationsToQuery]);
        }
   
        if (affiliationMap.size() > 0 || quoteMap.size() > 0){
            for (SBQQ__QuoteLineGroup__c qlg :quoteLineGroupsList){                  
                if (string.isBlank(qlg.SBQQ__Account__c)){
                    qlg.SBQQ__Account__c = quoteMap.get(qlg.SBQQ__Quote__c).SBQQ__Account__c;
                }
                //Added checking if qlg.JVCO_Affiliated_Venue__c is not empty GREEN-30818 -robert.j.b.lacatan
                if (string.isBlank(qlg.JVCO_Venue__c) && !string.isBlank(qlg.JVCO_Affiliated_Venue__c)){
                    qlg.JVCO_Venue__c = affiliationMap.get(qlg.JVCO_Affiliated_Venue__c).JVCO_Venue__c;
                }                   
            }
        }     
    }

    /* ----------------------------------------------------------------------------------------------------------
    Author:         rhys.j.c.dela.cruz
    Company:        Accenture
    Description:    New method to prevent user deleting QLG if not draft
    Inputs:         Old Map of Quote Line Groups
    Returns:        n/a
    <Date>          <Authors Name>                      <Brief Description of Change> 
    09-Aug-2018     rhys.j.c.dela.cruz                   Initial version
    ----------------------------------------------------------------------------------------------------------*/
    public static void preventQuoteLineGroupDeletion(Map<Id, SBQQ__QuoteLineGroup__c> oldQuoteLineGroupsMap) {

        if(!oldQuoteLineGroupsMap.isEmpty())
        {
            for(SBQQ__QuoteLineGroup__c qlgRec : oldQuoteLineGroupsMap.values())
            {
                if((qlgMap.get(qlgRec.Id).SBQQ__Quote__r.SBQQ__Status__c != 'Draft' || qlgMap.get(qlgRec.Id).SBQQ__Quote__r.JVCO_Cancelled__c)  && currentUserProf.Name != 'System Administrator')
                {
                    //Users cannot delete a Quote Line Group once a Contract and Order has been generated (Quote not in Draft Status)
                    oldQuoteLineGroupsMap.get(qlgRec.Id).addError(Label.JVCO_ErrMsg_RejectQLGDeletion_1);
                }
            }
        }
    }

    /* ----------------------------------------------------------------------------------------------------------
    Author:         rhys.j.c.dela.cruz
    Company:        Accenture
    Description:    Method to prevent change name of QLG if status != Draft
    Inputs:         List and Old Map
    Returns:        n/a
    <Date>          <Authors Name>                      <Brief Description of Change> 
    09-Aug-2018     rhys.j.c.dela.cruz                   Initial version
    ----------------------------------------------------------------------------------------------------------*/
    public static void preventUpdateOfGroupName(List<SBQQ__QuoteLineGroup__c> qlgList, Map<Id, SBQQ__QuoteLineGroup__c> oldQuoteLineGroupsMap) {

        if(qlgList != null && oldQuoteLineGroupsMap != null)
        {
            for(SBQQ__QuoteLineGroup__c qlgRec : qlgList)
            {   
                if((qlgRec.Name != oldQuoteLineGroupsMap.get(qlgRec.Id).Name || qlgRec.SBQQ__Description__c != oldQuoteLineGroupsMap.get(qlgRec.Id).SBQQ__Description__c) && (qlgMap.get(qlgRec.Id).SBQQ__Quote__r.SBQQ__Status__c != 'Draft' || qlgMap.get(qlgRec.Id).SBQQ__Quote__r.JVCO_Cancelled__c) && currentUserProf.Name != 'System Administrator')
                {
                    //User cannot change Name of Group once a Contract and Order has been generated (Quote not in Draft Status)
                    qlgRec.addError(Label.JVCO_ErrMsg_RejectQLGNameEdit_1);
                }
            }
        }
    }
}