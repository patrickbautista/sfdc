public class AggregateResultIterator Implements Iterator<AggregateResult>
{ 
    AggregateResult[] results {get;set;}
	Integer index {get; set;} 
	
	public AggregateResultIterator(String query, Date JVCO_LAST_N_DAYS, Set<Id> sInvIdSet, Set<Id> orderIdSet) 
	{
		index = 0;
		results = Database.query(query); 
	}

	public boolean hasNext()
	{ 
		return results != null && !results.isEmpty() && index < results.size(); 
	} 

	public AggregateResult next()
	{ 
		return results[index++];
	}  
}