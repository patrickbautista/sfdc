/* -------------------------------------------------------------------------------------
    Name: JVCO_AccountRestructureHelper_Test
    Description: Test Class for JVCO_AccountRestructureHelper

    Date            Version     Author                Summary of Changes 
    -----------     -------     -----------------     ----------------------------------
    23-May-2017     0.1         jules.osberg.a.pablo  Intial creation
------------------------------------------------------------------------------------- */
@isTest
private class JVCO_AccountRestructureHelper_Test {
	@testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        //JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        Test.startTest();
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        //custAcc.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        c.MailingPostalCode = 'ne6 5ls';
        c.MailingCity = 'Test City';
        c.MailingCountry = 'Test Country';
        c.MailingState = 'Test State';
        c.MailingStreet = 'Test Street';
        c.FirstName = 'Test Name';
        c.LastName = 'Test Last Name';
        insert c;
        
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc.AccountNumber = '832020';
        licAcc.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
        insert licAcc;  

        Account licAcc2 = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc2.AccountNumber = '832021';
        insert licAcc2; 

        Account licAcc3 = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc3.AccountNumber = '832022';
        LicAcc3.JVCO_Live__c = true;
        insert licAcc3; 

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        JVCO_Venue__c ven = JVCO_TestClassHelper.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassHelper.createAffiliation(licAcc.Id, ven.Id);
        insert aff;


        
        JVCO_Event__c event = new JVCO_Event__c();
        event.JVCO_Venue__c = ven.id;
        event.JVCO_Tariff_Code__c = 'LC';
        event.JVCO_Event_Type__c = 'Concert - Qualifies for Old Rate';
        event.JVCO_Event_Start_Date__c = date.today();
        event.JVCO_Event_End_Date__c = date.today() + 7;
        event.License_Account__c=licAcc3.id;
        event.Headline_Type__c = 'No Headliner';
        event.JVCO_Number_of_Set_Lists_Expected__c = 2;
        insert event;
        
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        q.JVCO_Cust_Account__c = custAcc.Id;
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        ql.Affiliation__c = aff.id;
        ql.JVCO_Event__c = event.id;
        ql.JVCO_Venue__c = ven.id;
        insert ql;
        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = aff.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        quoteLineGroupRecord.SBQQ__Account__c = licAcc.Id;
        Insert  quoteLineGroupRecord;

        ql.SBQQ__Group__c = quoteLineGroupRecord.Id;
        update ql;

        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;

        SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(licAcc.id, p.id);
    	s.SBQQ__Contract__c = contr.Id;
    	s.Affiliation__c = aff.Id;
    	s.SBQQ__QuoteLine__c = ql.Id;
    	insert s;

        q.SBQQ__MasterContract__c = contr.Id;
        update q;
        
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopOrderBeforeUpdateHandler = true;
        JVCO_FFUtil.stopOrderAfterUpdateHandler = true;
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        OrderItem oi1 = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi1;
        
        insert new GLA_for_Cash_Transfer__c(name = '10040 - Accounts receivables control');
        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        insert bInv;
        /*
        list<PAYLIC1__Licence__c> lcList = new list<PAYLIC1__Licence__c>();
        PAYLIC1__Licence__c lc = new  PAYLIC1__Licence__c();
        lc.name = 'aBL8E0000004CcL';
        lc.PAYLIC1__Version__c = '1.1';
        lc.PAYLIC1__Expires__c = date.today().adddays(30);
        lc.PAYLIC1__Product__c = 'Payonomy Validator';
        lc.PAYLIC1__Signature__c = '+ysmjpGFRPJC88CshG/ByQxhUs8=';
        insert lc; 
        */
        Test.stopTest();
        
        Set<Id> orderGroupIdSet = new Set<Id>();  
        orderGroupIdSet.add(orderGroup.Id);

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_TestClassHelper.setGeneralSettingsCS();
        insert new GLA_for_Cash_Transfer__c(name = '10100 - Bank Cash'); 
        //JVCO_BackgroundMatchingLogic.stopMatching = true;
        JVCO_FFUtil.stopCashTransferLogic = true;
        
        Database.executeBatch(new JVCO_OrderMultipleBillNowLogicBatch(orderGroupIdSet));
        
	}

	private static testMethod void getQueryString() {
		Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
    	Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    	Id oldLicAccId;
        
    	Account customerAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: customerRT LIMIT 1]; 
        
        JVCO_Affiliation__c restructureAffiliation = [SELECT Id, JVCO_Account__c, JVCO_NewAccountId__c FROM JVCO_Affiliation__c Limit 1];
        oldLicAccId = restructureAffiliation.JVCO_Account__c;

        Account oldLicenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT AND Id = :oldLicAccId LIMIT 1];

        Account newLicenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT AND Id != :oldLicAccId AND JVCO_Live__c=false LIMIT 2];
        Test.startTest();
        restructureAffiliation.JVCO_NewAccountId__c = newLicenceAccount.Id; 
        update restructureAffiliation;

        List<JVCO_OpenQuoteStatuses__c> settings = new List<JVCO_OpenQuoteStatuses__c>();
    	settings.add(new JVCO_OpenQuoteStatuses__c(Name = 'Draft', JVCO_StatusName__c = 'Draft'));
    	insert settings;

        JVCO_PermissionsForAccountRestructure__c arCS = JVCO_PermissionsForAccountRestructure__c.getOrgDefaults();
        arCS.JVCO_GrantRestructurePermission__c = true;
        upsert arCS JVCO_PermissionsForAccountRestructure__c.Id;

        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        asCS.JVCO_AccountRestuctureBatchSize__c = 1;
        upsert asCS JVCO_Array_Size__c.Id;

        JVCO_AccountRestructureHelper accountRestructureHelper = new JVCO_AccountRestructureHelper();
		List<JVCO_Affiliation__c> affList = Database.query(accountRestructureHelper.getQueryString(customerAccount.Id));
		System.assertEquals(affList.size(), 1);
		Test.stopTest();
	}

	private static testMethod void runValidationsOnAffiliations() {
		Set<Id> affiliationIdSet = new Set<Id>();
		Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
    	Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    	Id oldLicAccId;
        
    	Account customerAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: customerRT LIMIT 1]; 
        
        JVCO_Affiliation__c restructureAffiliation = [SELECT Id, JVCO_Account__c, JVCO_NewAccountId__c FROM JVCO_Affiliation__c Limit 1];
        oldLicAccId = restructureAffiliation.JVCO_Account__c;

        Account oldLicenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT AND JVCO_Customer_Account__c = :customerAccount.Id AND Id = :oldLicAccId LIMIT 1];
        Account newLicenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT AND JVCO_Customer_Account__c = :customerAccount.Id AND Id != :oldLicAccId LIMIT 1];
     	Test.startTest();
        restructureAffiliation.JVCO_NewAccountId__c = newLicenceAccount.Id; 
        update restructureAffiliation;

        List<JVCO_OpenQuoteStatuses__c> settings = new List<JVCO_OpenQuoteStatuses__c>();
    	settings.add(new JVCO_OpenQuoteStatuses__c(Name = 'Draft', JVCO_StatusName__c = 'Draft'));
    	insert settings;

        JVCO_PermissionsForAccountRestructure__c arCS = JVCO_PermissionsForAccountRestructure__c.getOrgDefaults();
        arCS.JVCO_GrantRestructurePermission__c = true;
        upsert arCS JVCO_PermissionsForAccountRestructure__c.Id;

        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        asCS.JVCO_AccountRestuctureBatchSize__c = 1;
        upsert asCS JVCO_Array_Size__c.Id;

        JVCO_AccountRestructureHelper accountRestructureHelper = new JVCO_AccountRestructureHelper();
		List<JVCO_Affiliation__c> affList = Database.query(accountRestructureHelper.getQueryString(customerAccount.Id));
		System.assertEquals(affList.size(), 1);
		//System.assertEquals(affList, null);
		List<JVCO_Affiliation__c> validAffiliations = accountRestructureHelper.runValidationsOnAffiliations(affList, customerAccount.Id);
		System.assertEquals(validAffiliations.size(), 1);

		for(JVCO_Affiliation__c aff :validAffiliations){
			affiliationIdSet.add(Id.valueOf(aff.Id));
		}
		List<SBQQ__QuoteLine__c> quoteLineList = [SELECT Id, SBQQ__Group__c, SBQQ__Quote__c FROM SBQQ__QuoteLine__c WHERE Affiliation__c IN :affiliationIdSet AND (SBQQ__Quote__r.SBQQ__Status__c = 'Draft' OR SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Renewal_Generated__c = false) AND SBQQ__Quote__r.JVCO_Cust_Account__c = :customerAccount.Id]; 
		System.assertEquals(quoteLineList.size(), 1);
		List<SBQQ__QuoteLineGroup__c> quoteLineGroupList = [SELECT Id, SBQQ__Account__c FROM SBQQ__QuoteLineGroup__c WHERE SBQQ__Account__c = :oldLicenceAccount.Id]; 	
		System.assertEquals(quoteLineGroupList.size(), 1);
		List<SBQQ__Quote__c> quoteList = [SELECT Id, SBQQ__Opportunity2__c, SBQQ__Account__c FROM SBQQ__Quote__c WHERE SBQQ__Account__c = :oldLicenceAccount.Id];
		System.assertEquals(quoteList.size(), 1);
		List<SBQQ__Subscription__c> subscriptionList = [SELECT Id, SBQQ__Contract__c, SBQQ__Account__c FROM SBQQ__Subscription__c WHERE SBQQ__Account__c = :oldLicenceAccount.Id]; 
		System.assertEquals(subscriptionList.size(), 1);
		List<Contract> contractList = [SELECT Id, AccountId FROM Contract WHERE AccountId = :oldLicenceAccount.Id];
		System.assertEquals(contractList.size(), 1);

		
		String errorMessage = accountRestructureHelper.processValidAffiliations(validAffiliations, customerAccount.Id);
		
		quoteLineList = [SELECT Id, SBQQ__Group__c, SBQQ__Quote__c FROM SBQQ__QuoteLine__c WHERE Affiliation__c IN :affiliationIdSet AND (SBQQ__Quote__r.SBQQ__Status__c = 'Draft' OR SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Renewal_Generated__c = false) AND SBQQ__Quote__r.JVCO_Cust_Account__c = :customerAccount.Id]; 
		System.assertEquals(quoteLineList.size(), 1);
		quoteLineGroupList = [SELECT Id, SBQQ__Account__c FROM SBQQ__QuoteLineGroup__c WHERE SBQQ__Account__c = :newLicenceAccount.Id];	
		System.assertEquals(quoteLineGroupList.size(), 1);
		quoteList = [SELECT Id, SBQQ__Opportunity2__c, SBQQ__Account__c FROM SBQQ__Quote__c WHERE SBQQ__Account__c = :newLicenceAccount.Id];
		System.assertEquals(quoteList.size(), 1);		
		subscriptionList = [SELECT Id, SBQQ__Contract__c, SBQQ__Account__c FROM SBQQ__Subscription__c WHERE SBQQ__Account__c = :newLicenceAccount.Id]; 
		System.assertEquals(subscriptionList.size(), 1);
		contractList = [SELECT Id, AccountId FROM Contract WHERE AccountId = :newLicenceAccount.Id];
		System.assertEquals(contractList.size(), 1);
		Test.stopTest();
	}

	private static testMethod void runValidationsOnAffiliationsInvalid() {
		Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
    	Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    	Id oldLicAccId;
        
    	Account customerAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: customerRT LIMIT 1]; 
        
        JVCO_Affiliation__c restructureAffiliation = [SELECT Id, JVCO_Account__c, JVCO_NewAccountId__c FROM JVCO_Affiliation__c Limit 1];
        oldLicAccId = restructureAffiliation.JVCO_Account__c;

        Account oldLicenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT AND Id = :oldLicAccId LIMIT 1];

        Account newLicenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT AND Id != :oldLicAccId AND JVCO_Live__c=false LIMIT 2];
        Test.startTest();
        newLicenceAccount.JVCO_Live__c = true;
        update newLicenceAccount;

        restructureAffiliation.JVCO_NewAccountId__c = newLicenceAccount.Id; 
        update restructureAffiliation;

        List<JVCO_OpenQuoteStatuses__c> settings = new List<JVCO_OpenQuoteStatuses__c>();
    	settings.add(new JVCO_OpenQuoteStatuses__c(Name = 'Draft', JVCO_StatusName__c = 'Draft'));
    	insert settings;

        JVCO_PermissionsForAccountRestructure__c arCS = JVCO_PermissionsForAccountRestructure__c.getOrgDefaults();
        arCS.JVCO_GrantRestructurePermission__c = true;
        upsert arCS JVCO_PermissionsForAccountRestructure__c.Id;

        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        asCS.JVCO_AccountRestuctureBatchSize__c = 1;
        upsert asCS JVCO_Array_Size__c.Id;

        JVCO_AccountRestructureHelper accountRestructureHelper = new JVCO_AccountRestructureHelper();
		List<JVCO_Affiliation__c> affList = Database.query(accountRestructureHelper.getQueryString(customerAccount.Id));
		System.assertEquals(affList.size(), 1);
		List<JVCO_Affiliation__c> validAffiliations = accountRestructureHelper.runValidationsOnAffiliations(affList, customerAccount.Id);
		System.assertEquals(validAffiliations.size(), 0);
		Test.stopTest();
	}


}