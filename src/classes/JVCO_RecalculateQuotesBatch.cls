/* ----------------------------------------------------------------------------------------------
    Name: JVCO_RecalculateQuotesBatch
    Description: Batch Class for updating Contracts related to an Account from Disposals

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    02-May-2017     0.1         Marlon Ocillos      Intial creation
    08-Apr-2019     0.2         Rhys Dela Cruz      GREEN-34460 - include for recalc Opps with blank Primary Quote field
----------------------------------------------------------------------------------------------- */
global class JVCO_RecalculateQuotesBatch implements Database.Batchable<sObject>, Database.Stateful
{
    global Integer venuesDisposed;
    global Set<Id> quoteIdsToProcess;
    global Boolean isDisposals;
    global Integer quoteCount;
    global Account accountRecord;
    global String errorMessage;
    
    global JVCO_RecalculateQuotesBatch (Set<Id> quoteIds, Integer disposeCount, Boolean fromDisposals, Account a)
    {
        quoteIdsToProcess = new Set<Id>();
        venuesDisposed = disposeCount;
        quoteCount = 0;
        if(!quoteIds.isEmpty()) {
            quoteIdsToProcess.addAll(quoteIds);
            quoteCount = quoteIds.size();
        }
        isDisposals = fromDisposals;
        
        if(!isDisposals) {
        	if(a != null) {
                accountRecord = a;
            }   
        }
        errorMessage = '';
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        String query = 'select Id, Recalculate__c, JVCO_Recalculated__c, SBQQ__Opportunity2__c, SBQQ__Opportunity2__r.SBQQ__PrimaryQuote__c from SBQQ__Quote__c where Id in: quoteIdsToProcess';
        return Database.getQueryLocator(query);
    }
    
    global void execute (Database.BatchableContext BC, List<sObject> scope)
    {
        List<SBQQ__Quote__c> quoteListToRecalculate = new List<SBQQ__Quote__c>();
        List<sObject> sobjListForUpdate = new List<sObject>();

        if (scope.size() > 0) 
        {
            for (SBQQ__Quote__c quoteRecord : (List<SBQQ__Quote__c>)scope) 
            {

                if(!quoteRecord.JVCO_Recalculated__c){

                    if(quoteRecord.Recalculate__c) {
                        quoteRecord.Recalculate__c = false;
                    } else {
                        quoteRecord.Recalculate__c = true;
                    }
                }

                //08-Apr-19 rhys.j.c.dela.cruz Check if Primary Quote of Opp is null
                if(quoteRecord.SBQQ__Opportunity2__c != null){

                    if(quoteRecord.SBQQ__Opportunity2__r.SBQQ__PrimaryQuote__c == null){

                        Opportunity oppRec = new Opportunity();
                        oppRec.Id = quoteRecord.SBQQ__Opportunity2__c;
                        oppRec.SBQQ__PrimaryQuote__c = quoteRecord.Id;
                        System.debug(oppRec);
                        sobjListForUpdate.add(oppRec);
                        System.debug(sobjListForUpdate);
                    }
                }

                //quoteListToRecalculate.add(quoteRecord);
                sobjListForUpdate.add(quoteRecord);
            }
            
            try 
            {
                if(!sobjListForUpdate.isEmpty()){
                    update sobjListForUpdate;
                }
                
            } catch (Exception ex) {
                System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                errorMessage = ex.getMessage() + ex.getCause() + ex.getStackTraceString();
            }
            
        }
    }
    
    global void finish (Database.BatchableContext BC)
    {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id = :BC.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {a.CreatedBy.Email});
        mail.setReplyTo('no-reply@salesforce.com');
        mail.setSenderDisplayName('Batch Processing');
        if(isDisposals) {
            mail.setSubject('Disposals Batch Process Completed');
            mail.setPlainTextBody('This is to notify you that the process for Disposals has now completed. Number of venues disposed: ' + venuesDisposed);
        } else {
            mail.setSubject('Recalculation Batch Process Completed');
            String emailMsg = 'This is to notify you ' + a.JobItemsProcessed + ' Quotes have been sent for repricing and this is currently in progress. You can view progress of this by clicking on the Contracting Buttons on the Licence Account' + accountRecord.Name + 'where you can see how many quotes are still recalculating';
            //String emailMsg = 'The recalculation of ' + a.JobItemsProcessed + ' Quotes for the Licence Account ' + accountRecord.Name + ' has now completed';
            //mail.setHTMLBody('The recalculation of ' + a.JobItemsProcessed + ' Quotes for the Licence Account ' + accountRecord.Name + ' has now completed with ' + a.NumberOfErrors + ' number of errors.');
            if(errorMessage != '') {
                emailMsg = emailMsg + '</br>' + a.NumberOfErrors + ' number of quotes encountered an error while recalculating.</br> Error: ' + errorMessage;
            }
            mail.setHTMLBody(emailMsg);
        }
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}