/* ----------------------------------------------------------------------------------------------
    Name: JVCO_GenerateAmendmentQuotesBatch
    Description: Batch Class for updating Contracts related to an Account

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    26-Apr-2017     0.1         Marlon Ocillos      Intial creation
    26-Sep-2017     0.2         Jules pablo         Added GREEN-23369 and GREEN-23331 Fix
    26-Sep-2017     0.3         Jasper Figueroa     Added GREEN-22316
    26-Sep-2017     0.4         Jules pablo         Added GREEN-23743, GREEN-23651, GREEN-23493 and GREEN-23492 Fix
    13-Oct-2017     0.5         Jules pablo         Added GREEN-24704 Fix, updated email body text 
    19-Oct-2017     0.6         Jules pablo         Added GREEN-24997 Fix, updated affilationsForCreation query
    02-Nov-2017     0.7         Jules pablo         Added GREEN-25156 Functionality. 
                                                    If a new venue has been added with start date which falls under the time frame of the true-up (amendment quotes), a blank quote of quote type should be generated 
                                                    If a new venue has been added with start date which falls under the renewal year, then a blank quote of quote type should not be created but a blank quote should be created for it.
    03-Nov-2017     0.8         Mary Ruelan         Added GREEN-24916 Fix, quote end date is affiliation start date+1 year for contract end date<affilitian start date
    06-Nov-2017     0.9         Robert John Lacatan Added fix GREEN-24916, if there are no contracts end date of quote is defaulted to 1 year from start date
    20-Nov-2017     1.0         Jules pablo         Chained JVCO_KeyAccountQuoteCreationBatch in the finish method to create blank quotes
    06-Feb-2018     1.1         JUles pablo         GREEN-28126 Added JVCO_Omit_Quote_Line_Group__c = FALSE Filter in the Affiliation query
----------------------------------------------------------------------------------------------- */
global class JVCO_GenerateAmendmentQuotesBatch implements Database.Batchable<sObject>, Database.Stateful
{
    global final Account accountRecord;
    global Integer contractBatchSize;
    global Set<Id> quoteIds;
    global Set<Id> contractIds;
    global String errorMessage;

    global JVCO_GenerateAmendmentQuotesBatch (Account a)
    {
        accountRecord = a;
        contractBatchSize = 1;
        quoteIds = new Set<Id>();
        contractIds = new Set<Id>();
        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        if(asCS.JVCO_Contract_Batch_Size__c != null && asCS.JVCO_Contract_Batch_Size__c > 0) {
            contractBatchSize = Integer.valueOf(asCS.JVCO_Contract_Batch_Size__c);
        }
        errorMessage = '';
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        String query = 'select Id, EndDate, ForceDeferred__c from Contract where JVCO_Renewal_Generated__c = false and AccountId = ' + '\'' + accountRecord.Id + '\'' + ' and Id not in (Select SBQQ__MasterContract__c from SBQQ__Quote__c where SBQQ__Account__c = ' + '\'' + accountRecord.Id + '\'' + ' AND SBQQ__Type__c = \'Amendment\' AND (SBQQ__Status__c = \'Draft\' OR (SBQQ__Status__c = \'Accepted\' AND SBQQ__MasterContract__r.JVCO_RenewableQuantity__c = 0 )))';
        //End GREEN-22316

        return Database.getQueryLocator(query);
    }
    
    global void execute (Database.BatchableContext BC, List<sObject> scope)
    {
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>(); //Update GREEN-23743, GREEN-23651, GREEN-23493 and GREEN-23492 jules.osberg.a.pablo
        List<Contract> contractListToUpdate = new List<Contract>();
        List<SBQQ__Quote__c> updateQuotesEndDate = new List<SBQQ__Quote__c>();
        
        if (scope.size() > 0) 
        {
            for (Contract contractRecord : (List<Contract>)scope) 
            {
                contractRecord.ForceDeferred__c = 1;
                contractListToUpdate.add(contractRecord);
            }
            
            try 
            {
                Database.update(contractListToUpdate);
            } catch (Exception ex) {
                System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
            }
            
            Schema.DescribeSObjectResult r = SBQQ__Quote__c.sObjectType.getDescribe();
            String quotePrefix = r.getKeyPrefix();
            String qId;
            Set<Id> quoteIdSet = new Set<Id>();
            List<SBQQ__QuoteLineGroup__c> qlgUndelList = new List<SBQQ__QuoteLineGroup__c>();

            for (Contract contractRecord : (List<Contract>)scope) 
            {
                contractIds.add(contractRecord.Id);

                
                try { //Update GREEN-23743, GREEN-23651, GREEN-23493 and GREEN-23492 jules.osberg.a.pablo
                    String qmodelJson = SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractAmender', contractRecord.Id, null);          
                    system.debug('@@@*** qmodelJson debug: ' + qmodelJson);
                } 
                catch(Exception e)
                {   
                    String ErrorLineNumber = e.getLineNumber() != Null ? String.ValueOf(e.getLineNumber()):'None';
                    String ErrorMessage = 'Cause: ' + e.getCause() + ' ErrMsg: ' + e.getMessage();
                    errLogList.add(JVCO_ErrorUtil.logError(contractRecord.Id, ErrorMessage , ErrorLineNumber , 'JVCO_ContractAmendmentQuotesBatch:SBQQ.ContractManipulationAPI'));
                }

                if(!errLogList.isempty())
                {
                    try {
                        insert errLogList;
                    }
                    catch(Exception ex)
                    {
                        System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                         
                    }
                } 
            }
        }
    }
    
    global void finish (Database.BatchableContext BC)
    {
        List<JVCO_Affiliation__c> affilationsForCreation = new List<JVCO_Affiliation__c>();
        List<SBQQ__Quote__c> generatedAmendmentQuotes = new List<SBQQ__Quote__c>();
        Date endDate = NULL;
        
        generatedAmendmentQuotes = [select Id, Name, End_Date__c from SBQQ__Quote__c where SBQQ__MasterContract__c in: contractIds];
            
        if(!generatedAmendmentQuotes.isEmpty()) {
            Integer loopCtr = 0;
            for(SBQQ__Quote__c qRec : generatedAmendmentQuotes) {
                if(loopCtr == 0) {
                    endDate = qRec.End_Date__c;
                } else {
                    if(qRec.End_Date__c > endDate) {
                        endDate = qRec.End_Date__c;
                    }
                }
                loopCtr++;
            }
        }

        //GREEN-28126 jules.osberg.a.pablo Added JVCO_Omit_Quote_Line_Group__c = FALSE Filter
        if (endDate == NULL) {
            affilationsForCreation = [select Id, Name, JVCO_Venue__r.Name, JVCO_Account__c, JVCO_Venue__c, JVCO_Start_Date__c, JVCO_End_Date__c, JVCO_Omit_Quote_Line_Group__c from JVCO_Affiliation__c where JVCO_Omit_Quote_Line_Group__c = FALSE and JVCO_Account__c = :accountRecord.Id and JVCO_End_Date__c = null AND Id NOT in (Select Affiliation__c from SBQQ__Subscription__c where SBQQ__Contract__r.JVCO_Renewal_Generated__c = false and SBQQ__Contract__r.AccountId = :accountRecord.Id) AND Id NOT in (Select JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c where SBQQ__Quote__r.SBQQ__Status__c = 'Draft' and SBQQ__Quote__r.SBQQ__Account__c = :accountRecord.Id)]; //Add GREEN-24997 jules.osberg.a.pablo
        } else {
            affilationsForCreation = [select Id, Name, JVCO_Venue__r.Name, JVCO_Account__c, JVCO_Venue__c, JVCO_Start_Date__c, JVCO_End_Date__c, JVCO_Omit_Quote_Line_Group__c from JVCO_Affiliation__c where JVCO_Omit_Quote_Line_Group__c = FALSE and JVCO_Account__c = :accountRecord.Id and JVCO_Start_Date__c <= :endDate and JVCO_End_Date__c = null AND Id NOT in (Select Affiliation__c from SBQQ__Subscription__c where SBQQ__Contract__r.JVCO_Renewal_Generated__c = false and SBQQ__Contract__r.AccountId = :accountRecord.Id) AND Id NOT in (Select JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c where SBQQ__Quote__r.SBQQ__Status__c = 'Draft' and SBQQ__Quote__r.SBQQ__Account__c = :accountRecord.Id)]; //02-Nov-2017 GREEN-25156 jules.osberg.a.pablo
        }

        //start 06-11-2017 mariel.m.buena GREEN-24769 Querying pricebook to map it to opportunity
        Map<Id, Pricebook2> pricebookId = new Map<Id, Pricebook2>([Select Id, Name
                                                                    From Pricebook2
                                                                    Where isStandard = true and isActive = true]);
        //end

        //start GREEN-25866 20-11-2017 jules.osberg.a.pablo
        Id pbId = pricebookId.values().size() > 0 ? pricebookId.values().get(0).Id:Null;
        AsyncApexJob aJob = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id = :BC.getJobId()];
        Integer totalQuotes = (contractBatchSize * aJob.JobItemsProcessed);
        Integer totalErrorQuotes = (contractBatchSize * aJob.NumberOfErrors);
        String errorMessage = String.valueOf(aJob.ExtendedStatus);

        Integer quoteBatchSize = 50;
        JVCO_Constants__c asCS = JVCO_Constants__c.getOrgDefaults();
        if(asCS.Key_Account_Blank_Quote_Batch_Size__c != null && asCS.Key_Account_Blank_Quote_Batch_Size__c > 0) {
            quoteBatchSize = Integer.valueOf(asCS.Key_Account_Blank_Quote_Batch_Size__c);
        }

        if(!affilationsForCreation.isEmpty()) {
            if(affilationsForCreation.size() > quoteBatchSize) { 
                Id batchId = database.executeBatch(new JVCO_GenerateBlankQuotesBatch(accountRecord, endDate, pbId, totalQuotes, totalErrorQuotes, errorMessage, true), quoteBatchSize);
            } else {
                JVCO_KeyAccountQuote.createQuote(affilationsForCreation, pbId,  endDate, accountRecord); 
                        JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_GenerateReviewQuotesApexJob__c', '','JVCO_GenerateReviewQuotesLastSubmitted__c');
                JVCO_KeyAccountQuote.sendEmailNotification(accountRecord, affilationsForCreation.size(), 0, errorMessage, totalQuotes, totalErrorQuotes, aJob.Createdby.Email, true,null);
            }
        } else {
            JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_GenerateReviewQuotesApexJob__c', '','JVCO_GenerateReviewQuotesLastSubmitted__c');
            JVCO_KeyAccountQuote.sendEmailNotification(accountRecord, 0, 0, errorMessage, totalQuotes, totalErrorQuotes, aJob.Createdby.Email, true,null);
            
        }
        //end
    }
}