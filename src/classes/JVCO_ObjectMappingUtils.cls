/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_ObjectMappingUtils.cls 
   Description:     
   Test class:  JVCO_ObjectMappingUtilsTest.cls 

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
  16-AUG-2016       0.1         Accenture-joseph.g.barrameda      Initial draft           
  ---------------------------------------------------------------------------------------------------------- */
 

public class JVCO_ObjectMappingUtils{

    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    
    Inputs:         String objectName 
    Returns:        String 
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-AUG-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public static String getAllFieldNames(String objName){
        
        sObjectType objType= Schema.getGlobalDescribe().get(objName);
        Map<String,Schema.SObjectField> mfields = objType.getDescribe().fields.getMap();
        
        String commaSeparatedFields = '';
        for(String fieldName : mFields.keyset()){
            if(commaSeparatedFields == null || commaSeparatedFields == ''){
                commaSeparatedFields = fieldName;
            }else{
                commaSeparatedFields = commaSeparatedFields + ', ' + fieldName;
            }
        }
        
        return commaSeparatedFields;
    }

    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-AUG-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public static sObject mapLeadToTargetObject(Lead leadSource, String targetObj, Id recordId){

        //Retrieve all the Lead Fields
        String leadQuery='SELECT ' + getAllFieldNames('Lead') + ' FROM Lead WHERE Id=\'' + leadSource.id + '\'';
        sObject sObjSource = Database.query(leadQuery);
        
        //Instantiate sObject
        sObject sObj = Schema.getGlobalDescribe().get(targetObj).newSObject() ;
        
        //Query the Target record
        if (recordId != null){
            String targetQuery='SELECT ' + getAllFieldNames(targetObj) + ' FROM ' + targetObj + ' WHERE Id=\'' + recordId + '\'';
            sObj= Database.query(targetQuery);
        }
        
        List<JVCO_Lead_Field_Mapping__mdt> leadFieldMappingList = [SELECT JVCO_RecordType__c, JVCO_Source_Field__c, JVCO_Target_Field__c, JVCO_Target_Object__C 
                                                                FROM JVCO_Lead_Field_Mapping__mdt 
                                                                WHERE JVCO_Target_Object__c =: targetObj AND JVCO_RecordType__c=null];
        
        for(JVCO_Lead_Field_Mapping__mdt lfmap:leadFieldMappingList){
            if (recordId != null){
                if(sObj.get(lfmap.JVCO_Target_Field__c) == null){
                    //Populate field if the Source Field is null
                    sObj.put(lfmap.JVCO_Target_Field__c, sobjSource.get(lfmap.JVCO_Source_Field__c));    
                }
            }
            else{
               
                    sObj.put(lfmap.JVCO_Target_Field__c, sobjSource.get(lfmap.JVCO_Source_Field__c));               
            }
        }
        
        return sObj;
    }
    
    
}