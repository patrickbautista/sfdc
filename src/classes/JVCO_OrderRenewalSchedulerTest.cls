@isTest
private class JVCO_OrderRenewalSchedulerTest
{
    @testSetup
    static void testData(){
    	List<JVCO_Renewal_Settings__c> settings = new List<JVCO_Renewal_Settings__c>();
    	settings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Generation', Days__c = 29));
    	insert settings;
    }
    
	@isTest
	static void itShould()
	{
		Test.startTest();

		JVCO_OrderRenewalScheduler orderSched = new JVCO_OrderRenewalScheduler();
		String sch = '0 0 23 * * ?';
		system.schedule('Test Order Renewal Scheduler', sch, orderSched);
		//Database.executeBatch(orderSched);
		//orderSched.CallExecuteMomentarily();


		Test.stopTest();

	}

	@isTest
	static void itShould2()
	{
		Test.startTest();

		JVCO_OrderRenewalScheduler orderSched = new JVCO_OrderRenewalScheduler();
		String sch = '0 0 23 * * ?';
		//system.schedule('Test Order Renewal Scheduler', sch, orderSched);
		//Database.executeBatch(orderSched);
		JVCO_OrderRenewalScheduler.CallExecuteMomentarily();


		Test.stopTest();

	}
}