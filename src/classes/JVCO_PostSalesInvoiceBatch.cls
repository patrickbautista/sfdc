/* ----------------------------------------------------------------------------------------------
    Name: JVCO_PostSalesInvoiceBatch
    Description: Batch for Posting Sales Invoice, Email Invalid DD Mandate, Creation of Direct Debit

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    19-Jan-2018  0.1         franz.g.a.dimaapi     Intial creation
    26-Sep-2018  0.2         john.patrick.valdez   GREEN-33032 - Direct Debits created automatically do not validate for DD Payee Contact
    05-Oct-2018  0.3         john.patrick.valdez   GREEN-33185 - CC Sin field not always populating in Invoice Group
    07-Feb-2018  0.4         franz.g.a.dimaapi     GREEN-34314 - Changed Bank Details Mapping
	13-May-2020  0.5         patrick.t.bautista    GREEN-35525 - change payonomy to smarterpay
----------------------------------------------------------------------------------------------- */
public class JVCO_PostSalesInvoiceBatch implements Database.Batchable<sObject>, Database.Stateful
{
    private Set<Id> sInvIdStatefulSet = new Set<Id>();
    private Set<Id> cNoteIdStatefulSet = new Set<Id>();
    private Set<Id> sInvIdSet;
    private List<c2g__codaInvoice__c> invoiceWithoutDDPayeeList;
    private Map<Id, Boolean> checkIddIfContactIsPopulatedMap;
    private Map<Id, Id> licAndCustAccIdMap;
    public Boolean stopMatching;

    public JVCO_PostSalesInvoiceBatch(){}
    public JVCO_PostSalesInvoiceBatch(Set<Id> sInvIdStatefulSet, Set<Id> cNoteIdStatefulSet)
    {
        this.sInvIdStatefulSet = sInvIdStatefulSet;
        this.cNoteIdStatefulSet = cNoteIdStatefulSet;
        invoiceWithoutDDPayeeList = new List<c2g__codaInvoice__c>();
        licAndCustAccIdMap = new Map<Id, Id>();
        checkIddIfContactIsPopulatedMap = new Map<Id, Boolean>();
        sInvIdSet = new Set<Id>();
    }

    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT Id, Name, 
                                        c2g__Account__c,
                                        c2g__Account__r.JVCO_DD_Mandate_Checker__c,
                                        c2g__Account__r.CreatedBy.Email,
                                        c2g__OutstandingValue__c,
                                        c2g__Account__r.JVCO_DD_Payee_Contact__c,
                                        JVCO_Outstanding_Amount_to_Process__c,
                                        JVCO_Sales_Rep__r.Email,JVCO_Sales_Rep__r.Name,
                                        c2g__Account__r.Name, CreatedDate, JVCO_Sales_Rep__r.Manager.Email
                                        FROM c2g__codaInvoice__c
                                        WHERE Id IN : sInvIdStatefulSet]);
    }

    public void execute(Database.BatchableContext bc, List<c2g__codaInvoice__c> scope)
    {   
        JVCO_FFUtil.stopCodaInvoiceHandlerAfterUpdate = true;
        bulkPostInvoice(scope);
        createDirectDebit(scope);
    }

    private void bulkPostInvoice(List<c2g__codaInvoice__c> sInvList)
    {
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        List<c2g.CODAAPICommon.Reference> refList = new List<c2g.CODAAPICommon.Reference>();
        for (c2g__codaInvoice__c sInv : sInvList)
        {
            c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = sInv.Id;
            refList.add(reference);
        }
        
        JVCO_BackgroundMatchingLogic.stopMatching = stopMatching;
        c2g.CODAAPISalesInvoice_10_0.BulkPostInvoice(context, refList);
    }
	
    private void createDirectDebit(List<c2g__codaInvoice__c> sInvoiceList)
    {
        List<c2g__codaInvoice__c> postedSInvList = [SELECT Id, Name, 
                                                    c2g__Account__c,
                                                    c2g__Account__r.Name,
                                                    c2g__OutstandingValue__c, CreatedDate,
                                                    JVCO_Outstanding_Amount_to_Process__c,
                                                    JVCO_Sales_Rep__r.Email, JVCO_Sales_Rep__r.Name,
                                                    JVCO_Sales_Rep__r.Manager.Email,
                                                    c2g__Account__r.JVCO_DD_Payee_Contact__c,
                                                    c2g__Account__r.JVCO_Customer_Account__c
                                                    FROM c2g__codaInvoice__c
                                                    WHERE Id IN :sInvoiceList
                                                    AND c2g__OutstandingValue__c > 0];

        Map<Id, List<c2g__codaInvoice__c>> accIdToSInvListMap = new Map<Id, List<c2g__codaInvoice__c>>();
        Map<Id, Double> accIdToSInvTotAmt = new Map<Id, Double>();
        for(c2g__codaInvoice__c sInv : postedSInvList)
        {   
            //Account to Sales Invoice List Map
            Id accId = sInv.c2g__Account__c;
            if(!licAndCustAccIdMap.containsKey(accId) && sInv.c2g__Account__r.JVCO_Customer_Account__c != null && accId != null){
                licAndCustAccIdMap.put(accId, sInv.c2g__Account__r.JVCO_Customer_Account__c);
            }
            if(!accIdToSInvListMap.containsKey(accId))
            {
                accIdToSInvListMap.put(accId, new List<c2g__codaInvoice__c>{});
            }
            accIdToSInvListMap.get(accId).add(sInv);
            //Account to Sales Invoice Total Amount Map
            if(!accIdToSInvTotAmt.containsKey(sInv.c2g__Account__c))
            {
                accIdToSInvTotAmt.put(accId, 0);
            }
            accIdToSInvTotAmt.put(accId, accIdToSInvTotAmt.get(accId) + sInv.c2g__OutstandingValue__c);
        }
        
        //Check if there's an active income direct debit
        Map<id, List<c2g__codaInvoice__c>> accIdToInvoiceMap = checkActiveIDDonAccount(accIdToSInvListMap);
        
        //AccountId to Payment Plan Map                                
        Map<Id, List<c2g__codaInvoice__c>> accIdToSInvToProcessMap = new Map<Id, List<c2g__codaInvoice__c>>();
        //Check if account has dd payee if not send an email, if yes proceed updating IDD	
        Set<String> accNumberSet = new Set<String>();
        //Set this to null then add invoices with active IDD
        for(Id accId : accIdToInvoiceMap.keySet()){
            Account account = new Account();
            account.Id = accId;
            account.JVCO_DD_Payee_Contact__c = accIdToInvoiceMap.get(accId)[0].c2g__Account__r.JVCO_DD_Payee_Contact__c;
            system.debug('account.JVCO_DD_Payee_Contact__c '+account.JVCO_DD_Payee_Contact__c);
            system.debug('dd payee '+accIdToInvoiceMap.get(accId)[0].c2g__Account__r.JVCO_DD_Payee_Contact__c);
            for(c2g__codaInvoice__c sInv :  accIdToInvoiceMap.get(accId))
            {
                // newly created
                // null 
                if(account.JVCO_DD_Payee_Contact__c == null && 
                   (!checkIddIfContactIsPopulatedMap.containsKey(accId) 
                    || (checkIddIfContactIsPopulatedMap.containsKey(accId) && !checkIddIfContactIsPopulatedMap.get(accId))))
                {
                    invoiceWithoutDDPayeeList.add(sInv);
                }else{
                    //Store Account and invoices and proceed creating invoice group and updating IDD next batch
                    if(!accIdToSInvToProcessMap.containsKey(accId)){
                        accIdToSInvToProcessMap.put(accId, new List<c2g__codaInvoice__c>());
                    }
                    accIdToSInvToProcessMap.get(accId).add(sInv);
                    sInvIdSet.add(sInv.Id);
                }
            }
        }
        
        //Create invoice group
        Map<Id, JVCO_Invoice_Group__c> accIdToIGMap = new Map<Id, JVCO_Invoice_Group__c>();
        for(Id accId : accIdToSInvToProcessMap.keySet())
        {   
            JVCO_Invoice_Group__c ig = new JVCO_Invoice_Group__c();
            ig.JVCO_Total_Amount__c = accIdToSInvTotAmt.get(accId);
            String sInvNameStr = '';
            for(c2g__codaInvoice__c sInv : accIdToSInvListMap.get(accId))
            {
                sInvNameStr += sInv.Name + ',';
            }
            ig.CC_SINs__c = sInvNameStr;
            accIdToIGMap.put(accId, ig);
        } 
        //Insert invoice group and process update on invoices and cc mapping
        if(!accIdToIGMap.isEmpty())
        {
            //Insert Invoice Group
            insert accIdToIGMap.values();
            List<c2g__codaInvoice__c> updatedSInvList = new List<c2g__codaInvoice__c>();
            List<JVCO_CC_Outstanding_Mapping__c> ccOutsToUpdate = new List<JVCO_CC_Outstanding_Mapping__c>();
            for(Id accId : accIdToIGMap.keySet())
            {
                JVCO_Invoice_Group__c ig = accIdToIGMap.get(accId);
                for(c2g__codaInvoice__c sInv : accIdToSInvListMap.get(accId))
                {   
                    // Fields to update
                    sInv.JVCO_Invoice_Group__c = ig.Id;
                    sInv.JVCO_Payment_Method__c = 'Direct Debit'; //GREEN-31106 02-Apr-2018 john.patrick.valdez
                    sInv.JVCO_Outstanding_Amount_to_Process__c = sInv.c2g__OutstandingValue__c;
                    JVCO_CC_Outstanding_Mapping__c ccOutsAmt = 
                        new JVCO_CC_Outstanding_Mapping__c(JVCO_CC_Outstanding_Value__c = sInv.c2g__OutstandingValue__c,
                                                           JVCO_Sales_Invoice_Name__c = sInv.Name,
                                                           JVCO_CC_Invoice_Group__c = ig.Id);
                    ccOutsToUpdate.add(ccOutsAmt);
                    updatedSInvList.add(sInv);
                }    
            }
            //Insert cc mapping and update sales invoice
            if(!updatedSInvList.isEmpty())
            {
                update updatedSInvList;
                insert ccOutsToUpdate;
            }
        }
    }
    private Map<Id, List<c2g__codaInvoice__c>> checkActiveIDDonAccount(Map<Id, List<c2g__codaInvoice__c>> accIdToInvoiceMap){
        Map<id, List<c2g__codaInvoice__c>> accIdToInvoicesMap = new Map<Id, List<c2g__codaInvoice__c>>();
        for(Income_Direct_Debit__c idd : [SELECT id,
                                          Account__c,
                                          Contact__c
                                          FROM Income_Direct_Debit__c
                                          WHERE Account__c IN: accIdToInvoiceMap.keySet()
                                          AND DD_Status__c NOT IN ('Cancelled', 'Cancelled by Originator', 'Cancelled by Payer', 'Expired')])
        {
            if(!accIdToInvoicesMap.containsKey(idd.Account__c)){
                accIdToInvoicesMap.put(idd.Account__c, accIdToInvoiceMap.get(idd.Account__c));
            }
            if(!checkIddIfContactIsPopulatedMap.containsKey(idd.Account__c)){
                checkIddIfContactIsPopulatedMap.put(idd.Account__c, idd.Contact__c != null ? true : false);
            }
        }
        return accIdToInvoicesMap;
    }

    public void finish(Database.BatchableContext bc)
    {
        if(!sInvIdSet.isEmpty() || !cNoteIdStatefulSet.isEmpty()){
            SMP_DirectDebitBatch batch = new SMP_DirectDebitBatch(sInvIdSet, cNoteIdStatefulSet);
            batch.licAndCustAccIdMap = licAndCustAccIdMap;
            batch.stopMatching = true;
            Database.executeBatch(batch, 1);
        }
        else if(!licAndCustAccIdMap.isEmpty() && cNoteIdStatefulSet.isEmpty() && sInvIdSet.isEmpty() && !sInvIdStatefulSet.isEmpty()){
            System.enqueueJob(new JVCO_UpdateSalesInvoiceQueueable(licAndCustAccIdMap));
        }
        
        if(!invoiceWithoutDDPayeeList.isEmpty()){    
            List<Messaging.SingleEmailMessage> emailforSendingList = new List<Messaging.SingleEmailMessage>();
            List<String> mailToAddresses = new List<String>();
            
            for(User u : [SELECT Email FROM User WHERE Id IN (
                SELECT UserOrGroupId
                FROM GroupMember
                WHERE Group.DeveloperName = 'JVCO_Customer_Service_Managers')])
            {
                mailToAddresses.add(u.email);
            }
                
            String emailMsgDDPayee = 'Income Direct Debit has not been generated due to Licence Account without DD Payee Contact.';
            String orgLink = URL.getSalesforceBaseUrl().toExternalForm();
            String accDetails = '<br><br>Licence Account: ';
            String sInvDetails = '<br><br>Sales Invoice: ';
            String salesRepDetails = '<br>Sales Rep: ';
            String sInvCreatedDetails = '<br>Sales Invoice Created Date: ';
            String instructions = '<br><br>To fix this issue:<br>Add DD Payee Contact and create Income Direct Debit<br>Or Deactivate Income Direct Debit';
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            mail.setReplyTo('noreply@pplprs.co.uk');
            mail.setSenderDisplayName('Licence Account DD Payee Contact Checker');
            mail.setSubject('The Licence Account does not have a DD Payee Contact');
            
            for(c2g__codaInvoice__c sInv : invoiceWithoutDDPayeeList){
                
                //GREEN-33507 No DD Payee Contact Email
                mail.setHTMLBody(emailMsgDDPayee + accDetails + '<a href="' + orgLink + '/' + sInv.c2g__Account__c + '">'+ sInv.c2g__Account__r.Name +'</a>' +
                                 sInvDetails + '<a href="' + orgLink + '/'+ sInv.Id + '">' + sInv.Name +'</a>' +
                                 salesRepDetails + sInv.JVCO_Sales_Rep__r.Name +
                                 sInvCreatedDetails + sInv.CreatedDate + instructions                          
                                );
                if(sInv.JVCO_Sales_Rep__r.Name == 'Automation User')
                {
                    mail.setToAddresses(mailToAddresses);
                }else
                {   
                    //GREEN-33032 Email Content to Send if there is no DD Payee Contact
                    List<String> emailList = new List<String>();
                    if(sInv.JVCO_Sales_Rep__r.Email != null){
                        emailList.add(sInv.JVCO_Sales_Rep__r.Email);
                        mail.setToAddresses(emailList);
                    }
                    if(sInv.JVCO_Sales_Rep__r.Manager.Email != null){
                        emailList.add(sInv.JVCO_Sales_Rep__r.Manager.Email);
                        mail.setToAddresses(emailList);
                    }
                }
                if(sInv.JVCO_Sales_Rep__r.Name == 'Automation User' || sInv.JVCO_Sales_Rep__r.Email != null || sInv.JVCO_Sales_Rep__r.Manager.Email != null){
                    emailforSendingList.add(mail);
                }
            }
            if(!emailforSendingList.isEmpty())
            {
                Messaging.sendEmail(emailforSendingList);
            }
        }
    }  
}