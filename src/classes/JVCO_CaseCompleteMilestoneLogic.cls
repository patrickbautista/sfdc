/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CaseCompleteMilestoneLogic.cls 
   Description: Business logic class for completing milestones of a Case record

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  13-Oct-2016   0.1         ryan.i.r.limlingan  Intial creation
  24-Oct-2016   0.2         ryan.i.r.limlingan  Added references to utility class
  25-Oct-2016   0.3         ryan.i.r.limlingan  Added call to loadCustomSettings() in class' entry
                                                point
  08-Nov-2016   0.4         ryan.i.r.limlingan  Modified completeMilestone() to handle the feature
                                                described in GREEN-7322
  ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_CaseCompleteMilestoneLogic
{

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Function that completes a milestone if conditions are met
    Inputs: List<Case>, Map<Id,Case>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    13-Oct-2016 ryan.i.r.limlingan  Initial version of function
    24-Oct-2016 ryan.i.r.limlingan  Added calls to utility class
    25-Oct-2016 ryan.i.r.limlingan  Added calls to loadCustomSettings()
    08-Nov-2016 ryan.i.r.limlingan  Also populated firstResponseSLACasesList when status is changed
                                    to Closed
  ----------------------------------------------------------------------------------------------- */
    public static void completeMilestone(List<Case> cases, Map<Id, Case> oldCasesMap)
    {
        // Call function to ensure Custom Setting variables have been loaded
        JVCO_CaseUtils.loadCustomSettings();

        List<Case> firstResponseSLACasesList = new List<Case>();
        List<Case> resolutionSLACasesList = new List<Case>();

        String firMilestoneCriteria = JVCO_CaseUtils.slaConstants.JVCO_First_Milestone_Criteria__c;
        String secMilestoneCriteria = JVCO_CaseUtils.slaConstants.JVCO_Second_Milestone_Criteria__c;
        for (Case c : cases)
        {
            if (didCaseStatusChange(c, oldCasesMap, firMilestoneCriteria)) // Responded
            {
                firstResponseSLACasesList.add(c);
            } else if (didCaseStatusChange(c, oldCasesMap, secMilestoneCriteria)) { // Closed
                firstResponseSLACasesList.add(c); // To ensure completion of First Milestone as well
                resolutionSLACasesList.add(c);
            }
        }

        String firstMilestoneName = JVCO_CaseUtils.slaConstants.JVCO_First_Milestone__c;
        if (!firstResponseSLACasesList.isEmpty())
        {
            processMilestone(firstResponseSLACasesList, firstMilestoneName);
        }

        String secondMilestoneName = JVCO_CaseUtils.slaConstants.JVCO_Second_Milestone__c;
        if (!resolutionSLACasesList.isEmpty())
        {
            processMilestone(resolutionSLACasesList, secondMilestoneName);
        }
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Retrieves CaseMilestones to be marked for completion
    Inputs: Case, Map<Id, Case>, String
    Returns: Boolean
    <Date>      <Authors Name>      <Brief Description of Change> 
    13-Oct-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public static void processMilestone(List<Case> casesList, String milestone)
    {
        String milestoneName = milestone + '%';

        // Retrieve Id of Case records to be used to filter out CaseMilestones
        List<Id> caseIdList = new List<Id>();
        for (Case c : casesList)
        {
            caseIdList.add(c.Id);
        }

        List<CaseMilestone> milestonesToUpdateList = [SELECT Id, CaseId, CompletionDate
                                                      FROM CaseMilestone
                                                      WHERE CaseId IN :caseIdList
                                                      AND MilestoneType.Name LIKE :milestoneName
                                                      AND CompletionDate = NULL];
        if (!milestonesToUpdateList.isEmpty())
        {
            for (CaseMilestone cm : milestonesToUpdateList)
            {
                cm.CompletionDate = System.now();
            }
            update milestonesToUpdateList;
        }
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Checks if Case Status was updated to passed Status parameter
    Inputs: Case, Map<Id, Case>, String
    Returns: Boolean
    <Date>      <Authors Name>      <Brief Description of Change> 
    13-Oct-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public static Boolean didCaseStatusChange(Case c, Map<Id, Case> oldCasesMap, String status)
    {
        if ((c.Status != oldCasesMap.get(c.Id).Status) && c.Status == status)
        {
            return TRUE;
        }
        return FALSE;
    }
}