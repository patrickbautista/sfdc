/*
* @author update alykka.m.t.duites
* @date 08/23/2017
*/

@isTest
private class JVCO_InvoiceWeekLogic_Test {
    public static blng__Invoice__c invoice = null;
    public static blng__Invoice__c invoice2 = null;

    static void setUpData()
    {
        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;
        
        invoice = new blng__Invoice__c();   
        invoice.blng__Account__c = acc.Id;
        invoice.blng__InvoiceDate__c = Date.newInstance(2017, 7, 30);
        insert invoice;
        
        invoice2 = new blng__Invoice__c();   
        invoice2.blng__Account__c = acc.Id;
        invoice2.blng__InvoiceDate__c = Date.newInstance(2017, 9, 7);
        insert invoice2;
    }
    
    @isTest
    public static void testIfWeekend(){
        Test.startTest();
            setUpData();
            Date result = JVCO_InvoiceWeekLogic.setWeekWork(invoice);
        Test.stopTest();
        //System.assertEquals(invoice.blng__InvoiceDate__c.addDays(1), result);
    }
    
    @isTest
    public static void testIfNotWeekend(){
        Test.startTest();
            setUpData();
            Date result = JVCO_InvoiceWeekLogic.setWeekWork(invoice2);
        Test.stopTest();

        //System.assertEquals(invoice2.blng__InvoiceDate__c.addDays(1), result);
    }
}