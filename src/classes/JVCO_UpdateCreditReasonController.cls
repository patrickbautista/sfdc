/* ----------------------------------------------------------------------------------------------
    Name: JVCO_UpdateCreditReasonController
    Description: Controller for Mandatory Input of credit reason field for Key Accounts

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
	18-Jul-2018  0.1		 patrick.t.bautista    Controller for Mandatory Input of credit reason 
												   field - GREEN-32099
----------------------------------------------------------------------------------------------- */
public class JVCO_UpdateCreditReasonController {
    private ApexPages.StandardController stdCtrl;
    public id fetchedAccountRecord;
    public String AccountName {get; set;}
    public Boolean isError {get; set;}
    public Boolean isKAUser {get; set;}
    public Boolean isProceed {get; set;}
    public Boolean isPopulateCredReason {get; set;}
    public Boolean isChecking {get; set;}
    public Boolean isPaymentTermNotUpdated {get; set;}
    public Boolean showPaymentTermSuccessMsg {get; set;}
    public Boolean returnToAcc {get; set;}
    public Account acctToUpdate;
    public list<SBQQ__Quote__c> retrieveNegativeQuotes;
    public List<SBQQ__Quote__c> quoteList;
    public List<selectOption> tempOptionLst;
    public List<selectOption> paymentTermList;
    public String selectedname {get;set;}
    public String selectedPaymentTerm {get;set;}
    public Boolean isDefaultPaymentTerm;
    public Boolean hasNoPaymentTerm {get; set;}
    
    public JVCO_UpdateCreditReasonController(ApexPages.StandardController ssc){
        stdCtrl = ssc;
        fetchedAccountRecord = ApexPages.currentPage().getParameters().get('id');
        
        isDefaultPaymentTerm = FALSE;
        isPaymentTermNotUpdated = TRUE;
        showPaymentTermSuccessMsg = FALSE;
        hasNoPaymentTerm = FALSE;
        quoteList = [SELECT Id, Name, SBQQ__Type__c, SBQQ__NetAmount__c, JVCO_Salesforce_Last_Recalculated_Time__c, Start_Date__c, End_Date__c, JVCO_Payment_Terms__c
                     FROM SBQQ__Quote__c 
                     WHERE SBQQ__Opportunity2__r.SBQQ__Contracted__c = FALSE 
                     AND SBQQ__Account__c =: fetchedAccountRecord 
                     AND (SBQQ__Type__c = 'Amendment' OR SBQQ__Type__c = 'Quote')
                     AND SBQQ__NetAmount__c > 0];
        
        acctToUpdate = [SELECT id, Name, JVCO_Active__c, JVCO_Billing_Contact__c, JVCO_Billing_Contact__r.MailingStreet, JVCO_Billing_Contact__r.MailingPostalCode, JVCO_Review_Contact__c, JVCO_Review_Contact__r.MailingStreet, JVCO_Review_Contact__r.MailingPostalCode, JVCO_Licence_Holder__c, JVCO_Licence_Holder__r.MailingStreet, JVCO_Licence_Holder__r.MailingPostalCode,ffps_custRem__Preferred_Communication_Channel__c from Account where id =: fetchedAccountRecord];
        retrieveNegativeQuotes = [select Id, Name, SBQQ__Type__c, SBQQ__NetAmount__c, JVCO_Salesforce_Last_Recalculated_Time__c, Start_Date__c, End_Date__c, JVCO_CreditReason__c from SBQQ__Quote__c WHERE /*JVCO_Recalculated__c = false AND*/ SBQQ__Opportunity2__r.SBQQ__Contracted__c = false AND SBQQ__Account__c =: fetchedAccountRecord AND SBQQ__Type__c = 'Amendment' AND SBQQ__NetAmount__c < 0];
        AccountName = acctToUpdate.Name;
    }
    public void updateOppAndQuoteKeyAccounts() 
    {
        checkProfileAndPermissionSet();
        if(retrieveNegativeQuotes.size() > 0)
        {
            isPopulateCredReason = true;
        }
        else
        {
            isProceed = true;
        }

        if(!acctToUpdate.JVCO_Active__c){
            ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, 'Account is not active.'));
            isProceed = false;
            returnToAcc = true;
        }

        if(acctToUpdate.JVCO_Billing_Contact__c != null && acctToUpdate.ffps_custRem__Preferred_Communication_Channel__c == 'Print'){
            if(acctToUpdate.JVCO_Billing_Contact__r.MailingStreet == null || acctToUpdate.JVCO_Billing_Contact__r.MailingPostalCode == null){
                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, 'The contact does not have an address. Either change the Preferred Comms Method to Email or add an address to the contact'));
                isProceed = false;
                returnToAcc = true;
            }
        }

        if(acctToUpdate.JVCO_Licence_Holder__c != null && acctToUpdate.ffps_custRem__Preferred_Communication_Channel__c == 'Print'){
            if(acctToUpdate.JVCO_Licence_Holder__r.MailingStreet == null|| acctToUpdate.JVCO_Licence_Holder__r.MailingPostalCode == null){
                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, 'The contact does not have an address. Either change the Preferred Comms Method to Email or add an address to the contact'));
                isProceed = false;
                returnToAcc = true;
            }
        }

        if(acctToUpdate.JVCO_Billing_Contact__c != null && acctToUpdate.ffps_custRem__Preferred_Communication_Channel__c == 'Print'){
            if(acctToUpdate.JVCO_Billing_Contact__r.MailingStreet == null || acctToUpdate.JVCO_Billing_Contact__r.MailingPostalCode == null){
                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, 'The contact does not have an address. Either change the Preferred Comms Method to Email or add an address to the contact'));
                isProceed = false;
                returnToAcc = true;
            }
        }
    }
    public void checkIfError()
    {
        showPaymentTermSuccessMsg = FALSE;
        
        system.debug('Payment Term Selected on Error Check: '+selectedPaymentTerm);
        isChecking = true;
        isPopulateCredReason = false;
        String negativeAmendmentQuoteError = 'Credit Reason must be populated if you have a Negative Amendment quote.';
        ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, negativeAmendmentQuoteError));
    }
    public void selectCreditReason()
    {
        isChecking = false;
        isError = True;
        
        showPaymentTermSuccessMsg = FALSE;
    }
    public  void updateQuoteCreditReason()
    {
        showPaymentTermSuccessMsg = FALSE;
        
        List<SBQQ__Quote__c> qouteUpdList = new List<SBQQ__Quote__c>();
        for(SBQQ__Quote__c quoteToUpt : retrieveNegativeQuotes)
        {
            quoteToUpt.JVCO_CreditReason__c = selectedname;
            qouteUpdList.add(quoteToUpt);
        }
        if(!qouteUpdList.isEmpty())
        {
            update qouteUpdList;
        }
    }
    public PageReference proceedToNext() 
    {    
        showPaymentTermSuccessMsg = FALSE;
        
        system.debug('Payment Term Selected on Error Check: '+selectedPaymentTerm);
        PageReference orderPage = new PageReference('/' + 'apex' + '/' + 'JVCO_ContractReviewQuotes' 
                                                    + '?scontrolCaching=1&id=' + fetchedAccountRecord);
        return orderPage;
    }
    
    public PageReference returnToAccount() 
    {
        PageReference orderPage = new PageReference('/' + fetchedAccountRecord);
        return orderPage;
    }
    
    public List<SelectOption> getOptionList()
    {
        tempOptionLst = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = SBQQ__Quote__c.JVCO_CreditReason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry s:ple){
            tempOptionLst.add(new SelectOption(s.getLabel(),s.getValue()));
        }
        return tempOptionLst;
    }
    
    public void checkProfileAndPermissionSet()
    {
        if(![SELECT AssigneeId, PermissionSet.Name FROM PermissionSetAssignment WHERE AssigneeId =: Userinfo.getUserId() AND PermissionSet.Name = 'Set_Payment_Terms_On_Generate_Quote'].isEmpty() 
           && [SELECT Name FROM Profile WHERE Id =: userinfo.getProfileId()][0].Name.contains('Key Accounts')){
            isKAUser = TRUE;
        }
        else{
            isKAUser= FALSE;
        }
                 
    }
    
    public List<SelectOption> getPaymentTermOptionList(){
        paymentTermList = new List<SelectOption>();
        List<Schema.PicklistEntry> ple = SBQQ__Quote__c.JVCO_Payment_Terms__c.getDescribe().getPicklistValues();
        for(Schema.PicklistEntry s:ple){
            paymentTermList.add(new SelectOption(s.getLabel(),s.getValue()));
            if(s.isDefaultValue()&&!isDefaultPaymentTerm){
                if(paymentTermList.size()!=0){
                    paymentTermList.remove(paymentTermList.size()-1);
                }
                paymentTermList.add(0, new SelectOption(s.getLabel(),s.getValue()));
                isDefaultPaymentTerm = TRUE;
            }
        }
        return paymentTermList;
    }
    
    public void updatePaymentTerm(){
        selectedPaymentTerm = selectedPaymentTerm;
        List<SBQQ__Quote__c> qouteUpdList = new List<SBQQ__Quote__c>();
        for(SBQQ__Quote__c quoteToUpt : quoteList){
            quoteToUpt.JVCO_Payment_Terms__c = selectedPaymentTerm;
            qouteUpdList.add(quoteToUpt);
        }
        if(!qouteUpdList.isEmpty()){
            update qouteUpdList;
            isPaymentTermNotUpdated = FALSE;
        	showPaymentTermSuccessMsg = TRUE;
        }
        else{
            hasNoPaymentTerm = TRUE;
        }
        
    }
}