/*----------------------------------------------------------------------------------------------
Name: JVCO_DunningResetAccountStatusSched.cls 
Description: Batch class that updates LA that has Last Reminder and Severity Level Populated
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
04-July-2018  0.1         patrick.t.bautista     Initial creation
----------------------------------------------------------------------------------------------- */
global class JVCO_DunningResetAccountStatusSched implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
        //Retrieve Custom Settings Name "Reset Account Status Settings"
    	final JVCO_ResetAccountStatusSettings__c DUNNING_SETTINGS = JVCO_ResetAccountStatusSettings__c.getOrgDefaults();
        final Decimal batchSize = DUNNING_SETTINGS.JVCO_BatchSizeInteger__c == NUll ? 20 : DUNNING_SETTINGS.JVCO_BatchSizeInteger__c;  
		id batchjobid = Database.executeBatch(new JVCO_DunningResetAccountStatusBatch(),Integer.valueOf(batchSize));
    }
    
    private static String getSchedulerExpression(DateTime dt) 
    {
        return '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();    
    }
    
    public static void CallExecuteMomentarily()  
    {
        System.schedule('JVCO_DunningResetAccountStatusSched', getSchedulerExpression(DateTime.Now().addSeconds(5)), new JVCO_DunningResetAccountStatusSched());
    }
}