public class JVCO_UpdatePaymentTerms {

    private ApexPages.StandardController stndrdCtrl;
    public Id accountId{get; set;}
    public Boolean isKAUser {get; set;}
    public List<selectOption> paymentTermList;
    public String selectedPaymentTerm {get;set;}
    public Boolean isDefaultPaymentTerm;
    public Boolean isPaymentTermNotUpdated {get; set;}
    public Boolean hasNoPaymentTerm {get; set;}
    public List<SBQQ__Quote__c> quoteList;
    
    
    
    public JVCO_UpdatePaymentTerms(ApexPages.StandardController sc){
        
        stndrdCtrl = sc;
        accountId = ApexPages.currentPage().getParameters().get('id');
        isPaymentTermNotUpdated = TRUE;
        isDefaultPaymentTerm = FALSE;
        
        quoteList = [SELECT Id, Name, SBQQ__Type__c, SBQQ__NetAmount__c, JVCO_Salesforce_Last_Recalculated_Time__c, Start_Date__c, End_Date__c, JVCO_Payment_Terms__c
                     FROM SBQQ__Quote__c 
                     WHERE SBQQ__Opportunity2__r.SBQQ__Contracted__c = FALSE 
                     AND SBQQ__Account__c =: accountId 
                     AND SBQQ__Type__c = 'Renewal' 
                     AND SBQQ__NetAmount__c > 0];
    }
    
    public List<SelectOption> getPaymentTermOptionList(){
        paymentTermList = new List<SelectOption>();
        List<Schema.PicklistEntry> ple = SBQQ__Quote__c.JVCO_Payment_Terms__c.getDescribe().getPicklistValues();
        for(Schema.PicklistEntry s:ple){
            paymentTermList.add(new SelectOption(s.getLabel(),s.getValue()));
            if(s.isDefaultValue()&&!isDefaultPaymentTerm){
                if(paymentTermList.size()!=0){
                    paymentTermList.remove(paymentTermList.size()-1);
                }
                paymentTermList.add(0, new SelectOption(s.getLabel(),s.getValue()));
                isDefaultPaymentTerm = TRUE;
            }
        }
        return paymentTermList;
    }
    
    public void checkProfileAndPermissionSet(){
        if(![SELECT AssigneeId, PermissionSet.Name FROM PermissionSetAssignment WHERE AssigneeId =: Userinfo.getUserId() AND PermissionSet.Name = 'Set_Payment_Terms_On_Generate_Quote'].isEmpty() 
           && [SELECT Name FROM Profile WHERE Id =: userinfo.getProfileId()][0].Name.contains('Key Accounts')){
            isKAUser = TRUE;
        }
        else{
            isKAUser= FALSE;
        }
                 
    }
    
    public void updatePaymentTerm(){
        system.debug('PAYMENT TERM: '+selectedPaymentTerm);
        List<SBQQ__Quote__c> qouteUpdList = new List<SBQQ__Quote__c>();
        for(SBQQ__Quote__c quoteToUpt : quoteList){
            quoteToUpt.JVCO_Payment_Terms__c = selectedPaymentTerm;
            qouteUpdList.add(quoteToUpt);
        }
        if(!qouteUpdList.isEmpty()){
            update qouteUpdList;
            isPaymentTermNotUpdated = FALSE;
        }
        else{
            hasNoPaymentTerm = TRUE;
        }
        
    }
    
    public PageReference runContractRenewalQuote(){
        PageReference orderPage = new PageReference('/' + 'apex' + '/' + 'JVCO_ContractRenewQuotes' 
                                                    + '?scontrolCaching=1&id=' + accountId);
        return orderPage;
    }
    
    public PageReference returnToAccount(){
        PageReference orderPage = new PageReference('/' + accountId);
        return orderPage;
    }
}