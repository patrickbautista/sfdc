@isTest
public class JVCO_CreditNoteBackgroundMatchingTest 
{
    @testSetup static void createTestData() 
    {
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        //Settings
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_codaCreditNoteTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        c2g__codaDimension2__c dim2 = JVCO_TestClassHelper.setDim2('PPL');
        insert dim2;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;

        //Credit Note
        c2g__codaCreditNote__c posCNote = JVCO_TestClassHelper.getCreditNote(licAcc.Id);
        c2g__codaCreditNote__c negCNote = JVCO_TestClassHelper.getCreditNote(licAcc.Id);
        List<c2g__codaCreditNote__c> cNoteList = new List<c2g__codaCreditNote__c>();
        cNoteList.add(posCNote);
        cNoteList.add(negCNote);
        insert cNoteList;
        c2g__codaCreditNoteLineItem__c posCNoteLine = JVCO_TestClassHelper.getCreditNoteLine(posCNote.Id, dim1.Id, dim3.Id, taxCode.Id, dim2.Id, p.Id, 500);
        c2g__codaCreditNoteLineItem__c negCNoteLine = JVCO_TestClassHelper.getCreditNoteLine(negCNote.Id, dim1.Id, dim3.Id, taxCode.Id, dim2.Id, p.Id, -500);
        List<c2g__codaCreditNoteLineItem__c> cNoteLineList = new List<c2g__codaCreditNoteLineItem__c>();
        cNoteLineList.add(posCNoteLine);
        cNoteLineList.add(negCNoteLine);
        insert cNoteLineList;
        List<c2g.CODAAPICommon.Reference> cNoteRefList = new List<c2g.CODAAPICommon.Reference>();
        for(c2g__codaCreditNote__c cNote : cNoteList)
        {
            c2g.CODAAPICommon.Reference cNoteRef = new c2g.CODAAPICommon.Reference();
            cNoteRef.Id = cNote.Id;
            cNoteRefList.add(cNoteRef);
        }
       
        //Sales Invoice
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        insert sInv;
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference sInvRef = new c2g.CODAAPICommon.Reference();
        sInvRef.Id = sInv.Id;

        Test.startTest();
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(context, sInvRef);
        c2g.CODAAPISalesCreditNote_10_0.BulkPostCreditNote(context, cNoteRefList);
        Test.stopTest();
    }

    @isTest
    static void matchToInvoiceLogic()
    {

        c2g__codaCreditNote__c cNote = [SELECT Id
                                        FROM c2g__codaCreditNote__c
                                        WHERE c2g__CreditNoteTotal__c > 0
                                        LIMIT 1];
        c2g__codaInvoice__c sInv = [SELECT Id, Name
                                    FROM c2g__codaInvoice__c
                                    LIMIT 1];
        cNote.JVCO_Reference_Document__c = sInv.Name;
        update cNote;
        Map<Id, c2g__codaCreditNote__c> sInvIdToCNoteMap = new Map<Id, c2g__codaCreditNote__c>();
        sInvIdToCNoteMap.put(sInv.Id, cNote);
        Test.startTest();
        Database.executeBatch(new JVCO_CreditNoteBackgroundMatchingBatch(sInvIdToCNoteMap), 1);
        Test.stopTest();
    }

    @isTest
    static void matchToCreditNoteLogic()
    {

        List<c2g__codaCreditNote__c> cNoteList = [SELECT Id, Name
                                                    FROM c2g__codaCreditNote__c];
        if(cNoteList.size() > 1)
        {
            cNoteList[0].JVCO_Reference_Document__c = cNoteList[1].Name;
            update cNoteList[0];
            Test.startTest();
            Database.executeBatch(new JVCO_CreditNoteBackgroundMatchingBatch(), 1);
            Test.stopTest();
        }
    }

    @isTest
    static void testMatchToJournalLogic()
    {   
        c2g__codaCreditNoteLineItem__c cNoteLineItem = [SELECT Id,
                                                        c2g__Dimension1__c, c2g__Dimension3__c, 
                                                        c2g__TaxCode1__r.c2g__GeneralLedgerAccount__c,
                                                        c2g__CreditNote__r.c2g__Account__r.c2g__CODAAccountsReceivableControl__c
                                                        FROM c2g__codaCreditNoteLineItem__c
                                                        WHERE c2g__UnitPrice__c > 0
                                                        LIMIT 1];

        c2g__codaJournal__c journal = new c2g__codaJournal__c();
        journal.c2g__JournalDate__c = System.today();
        journal.JVCO_Journal_Type__c = 'Standard';
        journal.c2g__Type__c = 'Manual Journal';
        journal.c2g__JournalDescription__c = 'Test';
        journal.c2g__DeriveCurrency__c = true;
        journal.c2g__DerivePeriod__c = true;
        insert journal;

        List<c2g__codaJournalLineItem__c> journalLineList = new List<c2g__codaJournalLineItem__c>();
        c2g__codaJournalLineItem__c journalLine1 = new c2g__codaJournalLineItem__c();
        journalLine1.c2g__Value__c = 500;
        journalLine1.c2g__LineType__c = 'Account - Customer';
        journalLine1.c2g__Dimension3__c = cNoteLineItem.c2g__Dimension3__c;
        journalLine1.c2g__GeneralLedgerAccount__c = cNoteLineItem.c2g__CreditNote__r.c2g__Account__r.c2g__CODAAccountsReceivableControl__c;
        journalLine1.c2g__Account__c = cNoteLineItem.c2g__CreditNote__r.c2g__Account__c;
        journalLine1.c2g__Dimension1__c = cNoteLineItem.c2g__dimension1__c;
        journalLine1.c2g__Journal__c = journal.Id;
        
        c2g__codaJournalLineItem__c journalLine2 = new c2g__codaJournalLineItem__c();
        journalLine2.c2g__Value__c = -500;
        journalLine2.c2g__LineType__c = 'General Ledger Account';
        journalLine2.c2g__Dimension3__c = cNoteLineItem.c2g__Dimension3__c;
        journalLine2.c2g__GeneralLedgerAccount__c = cNoteLineItem.c2g__TaxCode1__r.c2g__GeneralLedgerAccount__c;
        journalLine2.c2g__Dimension1__c = cNoteLineItem.c2g__dimension1__c;
        journalLine2.c2g__Journal__c = journal.Id;
        journalLineList.add(journalLine1);
        journalLineList.add(journalLine2);
        insert journalLineList;

        c2g__codaJournal__c journalRef = [SELECT Id, Name FROM c2g__codaJournal__c LIMIT 1];
        c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
        cNote.Id = cNoteLineItem.c2g__CreditNote__c;
        cNote.JVCO_Reference_Document__c = journalRef.Name;
        update cNote;

        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
        ref.Id = journal.Id;
        Test.startTest();
        c2g.CODAAPIJournal_12_0.PostJournal(context, ref);
        Database.executeBatch(new JVCO_CreditNoteBackgroundMatchingBatch(), 1);
        Test.stopTest();
    }

    @isTest
    static void testMatchToCashReceiptLogic()
    {
        Set<Id> cashEntryIdSet = new Set<Id>();
        c2g__codaCashEntry__c cashEntry = new c2g__codaCashEntry__c();
        cashEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cashEntry.c2g__Type__c = 'Receipt';
        insert cashEntry;
        cashEntryIdSet.add(cashEntry.Id);
        c2g__codaCreditNote__c cNote = [SELECT Id, c2g__Account__c
                                        FROM c2g__codaCreditNote__c
                                        WHERE c2g__CreditNoteTotal__c < 0
                                        LIMIT 1];
        c2g__codaCashEntryLineItem__c cashEntryLine = new c2g__codaCashEntryLineItem__c();
        cashEntryLine.c2g__Account__c = cNote.c2g__Account__c;
        cashEntryLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cashEntryLine.c2g__CashEntryValue__c = 12000;
        cashEntryLine.c2g__CashEntry__c = cashEntry.Id;
        insert cashEntryLine;

        c2g__codaCashEntryLineItem__c cashEntryLineRef = [SELECT Name FROM c2g__codaCashEntryLineItem__c LIMIT 1];
        cNote.JVCO_Reference_Document__c = cashEntryLineRef.Name;
        update cNote;
        c2g.CODAAPICommon_7_0.Context context = new c2g.CODAAPICommon_7_0.Context();
        c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
        ref.Id = cashEntry.Id;
        Test.startTest();
        c2g.CODAAPICashEntry_7_0.PostCashEntry(context, ref);
        Database.executeBatch(new JVCO_CreditNoteBackgroundMatchingBatch(), 1);
        Test.stopTest();
    }

    @isTest
    static void testMatchToCashRefundLogic()
    {
        Set<Id> cashEntryIdSet = new Set<Id>();
        c2g__codaCashEntry__c cashEntry = new c2g__codaCashEntry__c();
        cashEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cashEntry.c2g__Type__c = 'Refund';
        insert cashEntry;
        cashEntryIdSet.add(cashEntry.Id);
        c2g__codaCreditNote__c cNote = [SELECT Id, c2g__Account__c
                                        FROM c2g__codaCreditNote__c
                                        WHERE c2g__CreditNoteTotal__c > 0
                                        LIMIT 1];
        c2g__codaCashEntryLineItem__c cashEntryLine = new c2g__codaCashEntryLineItem__c();
        cashEntryLine.c2g__Account__c = cNote.c2g__Account__c;
        cashEntryLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cashEntryLine.c2g__CashEntryValue__c = 12000;
        cashEntryLine.c2g__CashEntry__c = cashEntry.Id;
        insert cashEntryLine;

        c2g__codaCashEntryLineItem__c cashEntryLineRef = [SELECT Name FROM c2g__codaCashEntryLineItem__c LIMIT 1];
        cNote.JVCO_Reference_Document__c = cashEntryLineRef.Name;
        update cNote;
        c2g.CODAAPICommon_7_0.Context context = new c2g.CODAAPICommon_7_0.Context();
        c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
        ref.Id = cashEntry.Id;
        Test.startTest();
        c2g.CODAAPICashEntry_7_0.PostCashEntry(context, ref);
        JVCO_BackgroundMatchingScheduler bms = new JVCO_BackgroundMatchingScheduler();
        String sch = '5 0 0 * * ?'; 
        System.schedule('JVCO_BackgroundMatchingScheduler' + System.Today(), sch, bms);
        Test.stopTest();
    }
}