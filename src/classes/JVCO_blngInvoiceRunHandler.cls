/* ----------------------------------------------------------------------------------------------
    Name: JVCO_blngInvoiceRunHandler
    Description: Handler for blng__InvoiceRun__c trigger

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    27-Dec-2017  0.1         franz.g.a.dimaapi     Intial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_blngInvoiceRunHandler {

    public static void afterUpdate(Map<Id, blng__InvoiceRun__c> newInvRunMap, Map<Id, blng__InvoiceRun__c> oldInvRunMap)
    {   
        Date invoiceDate = newInvRunMap.values()[0].blng__InvoiceDate__c;
        Set<Id> invRunIdSet = new Set<Id>();
        for(blng__InvoiceRun__c invRun : newInvRunMap.values())
        {
            if(invRun.blng__Status__c != oldInvRunMap.get(invRun.Id).blng__Status__c &&
                invRun.blng__Status__c == 'Completed')
            {
                invRunIdSet.add(invRun.Id);
            }
        }
        if(!invRunIdSet.isEmpty())
        {
            final JVCO_General_Settings__c GeneralSettings = JVCO_General_Settings__c.getInstance();
            final Decimal INVOICESCHEDSCOPE = GeneralSettings != null ? GeneralSettings.JVCO_Invoice_Schedule_Scope__c : 10;
            Id jobId = Database.executeBatch(new JVCO_InvoiceAndCreditNoteGenerationBatch(invRunIdSet, invoiceDate),Integer.valueOf(INVOICESCHEDSCOPE));
        }    
    }
}