/*
     * Author: eu.rey.t.cadag@accenture.com
     * Date created: 01-10-2017
     * Description: Extension Class for VFpage JVCO_AddEvents
     *
     <Date>          <Authors Name>                      <Brief Description of Change> 
     04-Jul-2017     mel.andrei.b.santos                  added a boolean field as a trigger for rendering the inputcheckbox in vpage based on GREEN-18737
     11-Aug-2017     mel.andrei.b.santos                  changed the replacement of urlVal to prevent error in redirecting due to changes of domain
     29-Aug-2017    mel.andrei.b.santos                     added condition for tariff code
     06-Sep-2017    mel.andrei.b.santos                    updated code so that  JVCO_Percentage_Controlled__c will populate PercOfContWorks__c for V_PRS as per GREEN-22161
     07-Sep-2017    mel.andrei.b.santos                     added logic to populate needed value for the attribute of DP_PRS
     27-Sep-2017    mel.andrei.b.santos                    removed 'insufficient information' from condition for rendering input checkbox based on GREEN-23277
     27-Sep-2017    mel.andrei.b.santos                    added condition for JVCO_Percentage_Controlled__c for V_PRS as part of GREEN-22161
     05-Oct-2017     reymark.j.l.arlos                    added line to set PercOfContWorksPick__c to null/blank if product is not V_PRS GREEN-23689
     25-Oct-2017    reymark.j.l.arlos                    added functionality to copy percentage controlled from event to quote line regardless of product code
     06-Dec-2017    reymark.j.l.arlos                    Added logic to include minimum fee products
     12-Dec-2017    rhys.j.c.dela.cruz                   Added logic for implementation of GREEN-25052
     15-May-2018    reymark.j.l.arlos                     to prevent the user from importing events that is requested classification and tariff code blank GREEN-31944
     24-May-2018     reymark.j.l.arlos                    Created setUpProductCodeName and updated childProductPicklist GREEN-31184 GREEN-32021
     27-Jul-2018    mel.andrei.b.santos                    GREEN-32430 - updated childProductPicklist for LC_PRS
     29-Jul-2019    rhys.j.c.dela.cruz                     GREEN-21709 - Adjusted redirect method to retain selected group when returning to QLE
     02-Apr-2020    rhys.j.c.dela.cruz                     GREEN-35496 - Create PR for changes by Luke in regards to Start Date Filter for Events
*/
    public class JVCO_ImportEventsExtension_Enhancement
    {
        Public List<wrapEvent> eventLst {get;set;} // List variable to display records in VFPage
        Private SBQQ__Quote__c recQuote = null; // Quote Variable 
        Map<String, JVCO_Live_Tariff_Mapping__c> tariffPicklistValues = JVCO_Live_Tariff_Mapping__c.getAll();
        Public Boolean checkLive {get; set;} //16-Jan-2018 reymark.j.l.arlos render checker for non-Live LA GREEN-27875
        Map<String, List<String>> tariffCodesMapping = new Map<String, List<String>>();
        Map<String, String> tariffCodesNames = new Map<String, String>();
                
        /*
         * Author: eu.rey.t.cadag@accenture.com
         * Description: Wrapper class to organize data to display in VFpage
         * Date created: 24 JAN 2017
         *
         <Date>          <Authors Name>                      <Brief Description of Change> 
         04-Jul-2017     mel.andrei.b.santos                  added a boolean field as a trigger for rendering the inputcheckbox in vpage based on GREEN-18737
        */
        public class wrapEvent 
        {
            public JVCO_Event__c eventrec {get; set;}
            public Boolean selected {get; set;}
            //04-Jul-2017 mel.andrei.b.santos added a boolean field as a trigger for rendering the inputcheckbox in vpage based on GREEN-18737
            public Boolean checkRender {get; set;}
            public String selectedOption {get; set;}
            public List<SelectOption> childOptions {get; set;}
     
            public wrapEvent(JVCO_Event__c evt) 
            {
                eventrec = evt;
                selected = false;
                // 04-Jul-2017 mel.andrei.b.santos added a boolean field as a trigger for rendering the inputcheckbox in vpage based on GREEN-18737
                checkRender = true;
                selectedOption = null;
                childOptions = new List<SelectOption>();
            }
        }
        
        /*
         * Author: eu.rey.t.cadag@accenture.com
         * Description: Constructor method to populate important variables
         * 
         <Date>          <Authors Name>                      <Brief Description of Change> 
         04-Jul-2017     mel.andrei.b.santos                  added a condition for rendering the inputcheckbox in vpage
         27-Sep-2017    mel.andrei.b.santos                    removed 'insufficient information' from condition for rendering input checkbox based on GREEN-23277
         15-May-2018     reymark.j.l.arlos                    Updated to prevent the user to select events with requested classification status and tariff clode blank GREEN-31944
        */
        public JVCO_ImportEventsExtension_Enhancement(ApexPages.StandardController stdController)
        {
            Id issueId = stdController.getId();
            eventLst = new List<wrapEvent>();
            List<SBQQ__QuoteLine__c> existingQLI = new List<SBQQ__QuoteLine__c>(); //Quote Line Variable - Bart Valencia 25APR2017

            if(issueId != null)
            {
                recQuote = [SELECT Id, JVCO_CurrentStartDate__c, Start_Date__c, End_Date__c, SBQQ__Account__c, SBQQ__Type__c, EditLinesFieldSetName__c, SBQQ__Account__r.Type, SBQQ__LineItemCount__c, SBQQ__Account__r.JVCO_Live__c, SBQQ__LineItemsGrouped__c FROM SBQQ__Quote__c WHERE Id =: issueId]; //16-Jan-2018 reymark.j.l.arlos added SBQQ__Account__r.JVCO_Live__c to check LA GREEN-27875

                //List of existing QLI for disabling double entry of event - Bart Valencia 27APR2017
                existingQLI = [SELECT Id, JVCO_Event__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =: recQuote.Id];
                
                //start 16-Jan-2018 reymark.j.l.arlos Added condition to show error message for non-live LA GREEN-27875
                if(recQuote != Null)
                {
                    if(recQuote.SBQQ__Account__r.JVCO_Live__c)
                    {   
                        checkLive = true;
                        List<SelectOption> blankDefaultTariffPicklist = new List<SelectOption>();
                        blankDefaultTariffPicklist.add(new SelectOption('', '--Not Applicable--'));
                        setUpProductCodeName(tariffPicklistValues, tariffCodesMapping, tariffCodesNames);
                        
                        for(JVCO_Event__c evtrec: JVCO_ImportEventsLogic_enhancement.loadEvents(recQuote))
                        {
                            if(!evtrec.JVCO_Invoice_Paid__c)
                            {
                                wrapEvent wrpRec = new wrapEvent(evtrec);
        
                                if(evtrec.JVCO_Event_Quoted__c)
                                    wrpRec.selected = false;
                                else
                                    // 04-Jul-2017 mel.andrei.b.santos added a condition for rendering the inputcheckbox in vpage
                                    // 29-Aug-2017 mel.andrei.b.santos added condition for tariff code
                                    // 27-Sep-2017 mel.andrei.b.santos removed 'insufficient information' from condition for rendering input checkbox based on GREEN-23277
                                    if( evtrec.JVCO_Event_Classification_Status__c == 'Requested' || evtrec.JVCO_Tariff_Code__c == null) 
                                    {
                                        wrpRec.checkRender = false;
                                        wrpRec.selected = false; //27-Jul-2017 mel.andrei.b.santos added field update to false to prevent it from being selected
                                        wrpRec.selectedOption = '--Not Applicable--';
                                        wrpRec.childOptions.addAll(blankDefaultTariffPicklist);
                                    }
                                    //end
                                    else
                                    {
                                       wrpRec.selected = true;
                                       //start 15-May-2018 reymark.j.l.arlos to prevent the user from importing events that is pending classificaiton GREEN-31944
                                       wrpRec.childOptions.addAll(childProductPicklist(evtrec.JVCO_Tariff_Code__c, evtrec.JVCO_Event_Type__c,evtrec.JVCO_Percentage_Controlled_Required__c, tariffCodesMapping, tariffCodesNames, evtrec.JVCO_Price_of_Admission__c));
                                       wrpRec.selectedOption = defaultPicklistProduct(evtrec.JVCO_Tariff_Code__c, tariffPicklistValues, evtrec.JVCO_Percentage_Controlled_Required__c);
                                       // end
                                       //27-Jul-2018 mel.andrei.b.santos  GREEN-32430 - childProductPicklist for LC_PRS
                                    }

                                    /*System.debug('@@@ Event Name: ' + evtrec.JVCO_Event_Name__c);
                                    System.debug('@@@Child Options: ' + wrpRec.childOptions);
                                    System.debug('@@@Child Options: ' + wrpRec.selectedOption);*/                                    
                                eventLst.add(wrpRec);
                                
        
                                //Filter if the tariff code is already existing on the quote line group - Bart Valencia 27APR2017
                                /*
                                if(evtrec.JVCO_Event_Quoted__c)
                                    wrpRec.selected = false;
                                else
                                    wrpRec.selected = true;
                                for(SBQQ__QuoteLine__c exQLI: existingQLI)
                                {
                                    if(exQLI.JVCO_Event__c != evtrec.Id)
                                    {
                                        eventLst.add(wrpRec);
                                    }
                                } 
                                */
                            }
                        }
                    }
                    //start 16-Jan-2018 reymark.j.l.arlos Added condition to show error message for non-live LA GREEN-27875
                    else
                    {
                        checkLive = false;
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.JVCO_ErrorEventNonLiveLA));
                    }
                    //end reymark.j.l.arlos
                }

            }
        }
        
        
    ///*
    // * Author: Eu Rey Cadag
    // * Description: When Quote's Group Line Item is False, this process gets all events having same Tariff code and put it under one bundle, unlike on 
    // *              pervious approach, when Quote's Group Line Item is false, each event has its own parent bundle that has raised this issue: GREEN-27915. . 
    // * Date Created: 23 Jan 2018
    // *
    // *
    //*/
    //public static Map<String, Map<String, List<JVCO_Event__c>>> regroupEventTariffLine(Map<String, Map<String, List<JVCO_Event__c>>> MapOfEventTariffLine)
    //{
    //    Map<String, Map<String, List<JVCO_Event__c>>> MapOfEventTariffLineRec = new Map<String, Map<String, List<JVCO_Event__c>>>();
    //    Map<String, List<JVCO_Event__c>> MapTariff = new Map<String, List<JVCO_Event__c>>();
    //    String VenRecId = null;
    //    for(String VenId: MapOfEventTariffLine.KeySet())
    //    {
    //        if(String.isEmpty(VenRecId)){
    //            VenRecId = VenId; 
    //            MapOfEventTariffLineRec.put(VenId, new Map<String, List<JVCO_Event__c>>());
    //        }
                
    //        for(String TariffCode: MapOfEventTariffLine.get(VenId).keySet())
    //        {
    //            if(!MapTariff.containsKey(TariffCode))
    //                MapTariff.put(TariffCode, new List<JVCO_Event__c>());
    //            for(JVCO_Event__c evt:MapOfEventTariffLine.get(VenId).get(TariffCode))
    //            {
    //                MapTariff.get(TariffCode).add(evt);
    //            }
    //        }
    //    }
    //    if(VenRecId != null)
    //    {
    //        MapOfEventTariffLineRec.put(VenRecId, MapTariff);
    //        return MapOfEventTariffLineRec;
    //    }
        
    //    return MapOfEventTariffLine;
    //}
    
    public PageReference generate()
    {
        //Map Collection: Id as Key and Venue Id as Value, String as Key of Map and value as Tariff/Product Code, List<JVCO_Event__c> as List of Events
        //Map<Id, Map<String, List<wrapEvent>>> MapOfEventTariffLine = new Map<Id, Map<String, List<wrapEvent>>>();
        Map<Id, Map<String, List<wrapEvent>>> eventTariffMap = new Map<Id, Map<String, List<wrapEvent>>>();

        Map<String, List<wrapEvent>> tariffEventMap = new Map<String, List<wrapEvent>>();
        Boolean isPromoter = false;

        //Set of Product/Tariff Code of Parent Products
        String[] SetOfTariffToCreate = new String[]{};
        
        
        //Set of Event Id
        /*Set<Id> setEventId = new Set<Id>();
        String tempSelectedProduct = null;
        List<String> selectedProduct = new List<String>();
        Integer ctrTwo = 0, ctr = 0;*/

        if(recQuote.SBQQ__Account__r.Type == 'Promoter')
        {
            isPromoter = true;
        }

        /*
         * Description:
         *  For loop to organize List of Events where Venue Id and Product/Tariff Code as Key
        */
        //System.debug('@@eventLst :' + eventLst);
        for(wrapEvent evt:eventLst)
        {   
            if(evt.selected)
            {
                String customTariffCode = null;
                if(String.valueOf(evt.eventrec.JVCO_Tariff_Code__c).contains('-'))
                {
                    String [] FormattedName =    String.valueOf(evt.eventrec.JVCO_Tariff_Code__c).split('-');
                    customTariffCode = FormattedName[0] + '_PRS%';
                }
                else
                {
                    customTariffCode = evt.eventrec.JVCO_Tariff_Code__c + '_PRS%';
                }

                SetOfTariffToCreate.add(customTariffCode);
                //setEventId.add(evt.eventrec.JVCO_Venue__c);

                if(!isPromoter)
                {
                    if(!eventTariffMap.containsKey(evt.eventrec.JVCO_Venue__c))
                    {
                        eventTariffMap.put(evt.eventrec.JVCO_Venue__c, new Map<String, List<wrapEvent>>());
                    }
                    
                    if(!eventTariffMap.get(evt.eventrec.JVCO_Venue__c).containsKey(customTariffCode))
                    {
                        eventTariffMap.get(evt.eventrec.JVCO_Venue__c).put(customTariffCode, new List<wrapEvent>());
                    }
                    if(eventTariffMap.get(evt.eventrec.JVCO_Venue__c).containsKey(customTariffCode))
                    {
                        eventTariffMap.get(evt.eventrec.JVCO_Venue__c).get(customTariffCode).add(evt);
                    }
                }
                else if (isPromoter)
                {
                    if(!tariffEventMap.containsKey(customTariffCode))
                    {
                        tariffEventMap.put(customTariffCode, new List<wrapEvent>());
                    }

                    if(tariffEventMap.containsKey(customTariffCode))
                    {
                        tariffEventMap.get(customTariffCode).add(evt);
                    }
                }
            }
        }
        
        /*system.debug('@@@selectedProduct: ' + selectedProduct);
        system.debug('@@@SetOfTariffToCreate: ' + SetOfTariffToCreate);*/

        /*
         * Description:
         *  Retrieve all Bundle Product based on selected Tariff Code
        */
        JVCO_ImportEventsLogic_enhancement.retrieveProductOption(SetOfTariffToCreate, recQuote);
        /*
         * Description:
         *  Retrieve existing Quote Lines
        
        JVCO_ImportEventsLogic.retrieveQuoteLine(recQuote);
        System.debug('@@@ Bundle Tariff Quote Line: ' +JVCO_ImportEventsLogic.MapBundleQuoteLine);
        */
        /*
         * Description:
         *  Method to create QuoteLine
        */
        JVCO_ImportEventsLogic_enhancement.generateQuoteLine(recQuote, eventTariffMap, tariffEventMap, isPromoter);
        return redirect();
        //return null;
    }

        /* ----------------------------------------------------------------------------------------------------------
        Author:         reymark.j.l.arlos
        Company:        Accenture
        Description:    Creates 2 map that contains the values of custom setting JVCO_Live_Tariff_Mapping__c to easly manipulate the data
        Inputs:         Map of JVCO_Live_Tariff_Mapping__c
        Returns:        Boolean
        <Date>          <Authors Name>                      <Brief Description of Change> 
        24-May-2018     reymark.j.l.arlos                    Initial version of the code (GREEN-31184)
     ----------------------------------------------------------------------------------------------------------*/
        private static void setUpProductCodeName(Map<String, JVCO_Live_Tariff_Mapping__c> liveTariffMapping, Map<String, List<String>> tariffCodesMapping, Map<String, String> tariffCodesNames)
        {
            List<String> tariffCodeOptions = new List<String>();
            List<String> tariffNameOptions = new List<String>();
            Integer ctr = 0;
            for(JVCO_Live_Tariff_Mapping__c tariffMapping : liveTariffMapping.values())
            {
                if(tariffMapping.Name.endsWith('Options'))
                {
                    tariffCodeOptions.clear();
                    tariffNameOptions.clear();
                    if(!tariffCodesMapping.containsKey(tariffMapping.Name))
                    {
                        tariffCodesMapping.put(tariffMapping.Name, new List<String>());
                    }

                    if(!String.isBlank(tariffMapping.JVCO_Child_Picklist_Product_Id__c))
                    {
                        tariffCodeOptions.addAll(tariffMapping.JVCO_Child_Picklist_Product_Id__c.split(';'));
                        tariffCodesMapping.get(tariffMapping.Name).addAll(tariffCodeOptions);
                    }

                    if(!String.isBlank(tariffMapping.JVCO_Child_Picklist_Product_Name__c))
                    {
                        tariffNameOptions.addAll(tariffMapping.JVCO_Child_Picklist_Product_Name__c.split(';'));

                        if(!String.isBlank(tariffMapping.JVCO_Child_Picklist_Product_Name_Cont__c))
                        {
                            tariffNameOptions.addAll(tariffMapping.JVCO_Child_Picklist_Product_Name_Cont__c.split(';'));
                        }
                    }

                    if(!tariffCodeOptions.isEmpty())
                    {
                        ctr = 0;
                        for(String tariffCode : tariffCodeOptions)
                        {
                            if(!tariffCodesNames.containsKey(tariffCode))
                            {
                                if(!tariffNameOptions.isEmpty())
                                {
                                    if(!String.isBlank(tariffNameOptions[ctr]))
                                    {
                                        tariffCodesNames.put(tariffCode, tariffNameOptions[ctr]);
                                    }
                                }
                            }
                            ctr++;
                        }
                    }
                }
            }
        }

         /* ----------------------------------------------------------------------------------------------------------
        Author:         rhys.j.c.dela.cruz
        Company:        Accenture
        Description:    Collects the options for the user to select child products to import
        Inputs:         TariffCode, Event Type, Custom Map of Live Tariff, Custom Map of Live Tariff Name
        Returns:        Boolean
        <Date>          <Authors Name>                      <Brief Description of Change> 
        13-May-2018     rhys.j.c.dela.cruz                   Initial version of the code
        24-May-2018     reymark.j.l.arlos                    Updated to simplify the logic since setUpProductCodeName is created (GREEN-31184) and added a condition for LP tariff (GREEN-32021)
        27-Jul-2018    mel.andrei.b.santos                    GREEN-32430 - updated childProductPicklist for LC_PRS
    ----------------------------------------------------------------------------------------------------------*/
        private static List<SelectOption> childProductPicklist(String tariffCode, String evntType, Boolean evntPercCtrlReq, Map<String, List<String>> tariffCodesMapping, Map<String, String> tariffCodesNames, Decimal AdmissionPrice)
        {
            List<SelectOption> tariffCodeNameOptions = new List<SelectOption>();
            if(tariffCode == 'LP')
            {
                if(evntType == 'Concert - Qualifies for Old Rate' || evntType == 'Festival - Qualifies for Old Rate')
                {
                    tariffCode = tariffCode + '_PRS Old Options';
                }
                else
                {
                    tariffCode = tariffCode + '_PRS New Options';
                }
            }
            //START 27-Jul-2018    mel.andrei.b.santos  GREEN-32430 - updated childProductPicklist for LC_PRS
            else if( tariffCode == 'LC')
            {
                if(!evntPercCtrlReq)
                {
                     if(AdmissionPrice == null){
                         tariffCode = tariffCode + '_PRS Options';
                     } else if(AdmissionPrice >= 5.00) {
                         tariffCode = tariffCode + '_PRS PerCtrNotReqOver5 Options';
                     } else {
                         tariffCode = tariffCode + '_PRS PerCtrNotReqUnder5 Options';
                     }
                } else {
                        tariffCode = tariffCode + '_PRS PerCtrReq Options';
                }
            }
            //END
            else
            {
                tariffCode = tariffCode + '_PRS Options';
            }
            
            system.debug('tariffCode: ' + tariffCode);
            if(tariffCodesMapping.containsKey(tariffCode))
            {
                for(String tariff : tariffCodesMapping.get(tariffCode))
                {
                    if(tariffCodesNames.containsKey(tariff))
                    {
                        tariffCodeNameOptions.add(new SelectOption(tariff, tariffCodesNames.get(tariff)));
                    }
                }
            }

            return tariffCodeNameOptions;
        }
        
        private static String defaultPicklistProduct(String tariffCode, Map<String, JVCO_Live_Tariff_Mapping__c> tariffPicklistValues, Boolean fixVariablePicklist){
            String defaultTariff = null;
            Integer ctr = 0;
            String tariffCustomName = null;
            String fixVariableProduct = fixVariablePicklist?'Variable': 'Fixed';

            if(String.valueOf(tariffCode).contains('-'))
            {
              String [] FormattedName =    String.valueOf(tariffCode).split('-');
              tariffCustomName = FormattedName[0] + '_PRS Level ' + String.valueOf(ctr);
              fixVariableProduct = FormattedName[1];
            }
            else{
                tariffCustomName = tariffCode + '_PRS Level ' + String.valueOf(ctr);
            }

            if(tariffPicklistValues.containsKey(tariffCustomName) && tariffCustomName.endsWith('Level 0')){
                if(!String.isBlank(tariffPicklistValues.get(tariffCustomName).JVCO_Child_Product_Id__c)){
                    defaultTariff = tariffPicklistValues.get(tariffCustomName).JVCO_Child_Product_Id__c;
                }
            }
            else if(tariffPicklistValues.containsKey(tariffCustomName + ' ' + fixVariableProduct)){
                if(!String.isBlank(tariffPicklistValues.get(tariffCustomName + ' ' + fixVariableProduct).JVCO_Child_Product_Id__c)){
                    defaultTariff = tariffPicklistValues.get(tariffCustomName + ' ' + fixVariableProduct).JVCO_Child_Product_Id__c;
                }
            }

            return defaultTariff;
        }
        
        public PageReference redirect()
        {
            //2019-07-26 rhys.j.c.dela.cruz - GREEN-21709 - adjusted string that creates URL back to QLE to current QLG open
            String tempGroupKey = Apexpages.currentPage().getParameters().get('groupKey');
            String groupKey = '';
            if(tempGroupKey != null){
                groupKey = '&groupKey=' + tempGroupKey.remove('_');
            }

            //String urlVal = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/sb?scontrolCaching=1&id=' + recQuote.Id + groupKey;
            String urlVal = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/sb?id=' + recQuote.Id + '#quote/le?qId=' + recQuote.Id + groupKey;
               //urlVal = urlVal.replace('https://c.', 'https://sbqq.'); START mel.andrei.b.santos 11-08-2017
               
            System.debug('urlVal = ' + urlVal);
            /* Start GREEN-34416  
            List<String> urLValParts = urlVal.split('\\.');
            List<String> urlValPartsReplace = urlVal.split('c');
            System.debug('urLValParts = ' + urLValParts + ' ' + urLValParts.size());
            System.debug('urlValPartsReplace = ' + urlValPartsReplace + ' ' + urlValPartsReplace.size());
            
            if(urlValPartsReplace.size() > 1 && urLValParts.size() > 0)
            {
                urlVal = urlVal.replace(urLValParts[0] , urlValPartsReplace[0] + 'sbqq' );
            }
            else
            {
                urlVal = urlVal.replace('https://c.', 'https://sbqq.');
            }*/
            
            String urlVal2 = urlVal.replace('c.', 'sbqq.');
            //End GREEN-34416
            // END
               PageReference pg = new PageReference(urlVal2);
               pg.setredirect(true);
           
           return pg;
        }
        
        public PageReference cancel()
        {
            if(recQuote != null)
             {
                 return redirect();
             }
            return null;
        }
    }