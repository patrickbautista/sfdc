@isTest
public class JVCO_CreateChurnLeadTest {
    
@testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        insert dt;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        
        c2g__codaTaxRate__c taxRate = new c2g__codaTaxRate__c(
            c2g__Rate__c = 20.0,
            c2g__TaxCode__c = taxCode.Id,
            c2g__StartDate__c = system.today()-10
            );
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim2('PRS');
        insert JVCO_TestClassHelper.setDim3();
        
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Test.startTest();
        SBQQ.TriggerControl.disable();
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        insert JVCO_TestClassHelper.setQuoteProcess();
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        
        JVCO_Venue__c venue = JVCO_TestClassHelper.createVenue();
        insert venue;
        JVCO_Affiliation__c affil = JVCO_TestClassHelper.createAffiliation(licAcc.id, venue.id);
        insert affil;
        
        SBQQ.TriggerControl.enable();

        Test.stopTest();
    }

    @isTest static void testExecute(){
        
        JVCO_Constants__c batchSettings = new JVCO_Constants__c();
        batchsettings.JVCO_CreateChurnLead_Batch_Size__c = 20;
        insert batchSettings;
            
        List<Account> acctList = new List<Account>();       
        Account acct = [Select id,
                        JVCO_Customer_Account__c
                        FROM Account 
                        WHERE RecordType.Name = 'Licence Account' 
                        limit 1];
    
        acctList.add(acct);
        Test.startTest();
        JVCO_CreateChurnLeadSchedule jc = new JVCO_CreateChurnLeadSchedule();
        jc.execute(null);
        Test.stopTest(); 
        System.assertEquals(true, acctList.size() != 0);
    }

    
    //Closed affiliation should create a Churn Lead
    @isTest static void testClosedAffiliation(){
        JVCO_Affiliation__c affil = [SELECT ID FROM JVCO_Affiliation__c LIMIT 1];
        affil.JVCO_Closure_Reason__c = 'Ceased Trading';
        affil.JVCO_Start_Date__c = system.today()-2;
        affil.JVCO_End_Date__c = system.today()-1;
        update affil;
        
        Contact c = [SELECT ID, Phone FROM Contact LIMIT 1];
        c.Phone = '01234567890';
        update c;
        
        Test.startTest();
        Database.executeBatch(new JVCO_CreateChurnLead());
        Test.stopTest();
        
        Lead churnLead = [SELECT ID, LeadSource, Phone FROM Lead WHERE LeadSource = 'Churn' LIMIT 1];
        System.assertEquals('Churn', churnLead.LeadSource);
		System.assertEquals('01234567890',churnLead.Phone);
        }
    
    //Open affiliation shouldn't create Churn lead
    @isTest static void testOpenAffiliation(){
        JVCO_Affiliation__c affil = [SELECT ID FROM JVCO_Affiliation__c LIMIT 1];

        Test.startTest();
        Database.executeBatch(new JVCO_CreateChurnLead());
        Test.stopTest();
        
        List<Lead> churnLeads = [SELECT ID FROM Lead WHERE LeadSource = 'Churn' LIMIT 1];
        System.assertEquals(0, churnLeads.size());
    }
    
    //Lapser closure reason should create churn lead as lapser
    @isTest static void testLapserAffiliation(){
        JVCO_Affiliation__c affil = [SELECT ID FROM JVCO_Affiliation__c LIMIT 1];
        affil.JVCO_Closure_Reason__c = 'Stopped playing music';
        affil.JVCO_Start_Date__c = system.today()-2;
        affil.JVCO_End_Date__c = system.today()-1;
        update affil;
        
        Test.startTest();
        Database.executeBatch(new JVCO_CreateChurnLead());
        Test.stopTest();
        
        Lead churnLead = [SELECT ID, FirstName, LastName, Phone FROM Lead WHERE LeadSource = 'Churn' LIMIT 1];
        System.assertEquals('Test',churnLead.FirstName);
        System.assertEquals('Contact',churnLead.LastName);
        }
    
    //If live category is completed then the previous account type should be Live
    @isTest static void testLiveAffiliation(){
        JVCO_Affiliation__c affil = [SELECT ID FROM JVCO_Affiliation__c LIMIT 1];
        affil.JVCO_Closure_Reason__c = 'Ceased Trading';
        affil.JVCO_Start_Date__c = system.today()-2;
        affil.JVCO_End_Date__c = system.today()-1;
        update affil;
        
        Account acct = [SELECT ID FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];
        acct.JVCO_LiveCategory__c = 'Universities';
        update acct;
            
        Test.startTest();
        Database.executeBatch(new JVCO_CreateChurnLead());
        Test.stopTest();
        
        Lead churnLead = [SELECT ID, Previous_Account_Type__c FROM Lead WHERE LeadSource = 'Churn' AND Previous_Account_Type__c = 'Live' LIMIT 1];
        System.assertEquals('Live',churnLead.Previous_Account_Type__c);
    }
    
    //Check calculation of potential amount
    @isTest static void testPotentialAmount(){
        
        JVCO_Affiliation__c affil = [SELECT ID FROM JVCO_Affiliation__c LIMIT 1];
        affil.JVCO_Closure_Reason__c = 'Ceased Trading';
        affil.JVCO_Start_Date__c = system.today()-2;
        affil.JVCO_End_Date__c = system.today()-1;
        update affil;
        
        List<SBQQ__Subscription__c> subs = new List<SBQQ__Subscription__c>();
        SBQQ__Subscription__c sub1 = new SBQQ__Subscription__c();
        sub1.SBQQ__Quantity__c = 1;
        sub1.Affiliation__c = affil.id;
        sub1.SBQQ__NetPrice__c = 100;
        sub1.Start_Date__c = system.today().addDays(-100);
        sub1.End_Date__c = system.today().addDays(100);
        subs.add(sub1);

        SBQQ__Subscription__c sub2 = new SBQQ__Subscription__c();
        sub2.SBQQ__Quantity__c = 1;
        sub2.Affiliation__c = affil.id;
        sub2.SBQQ__NetPrice__c = 200;
        sub2.Start_Date__c = system.today().addDays(-100);
        sub2.End_Date__c = system.today().addDays(100);
        subs.add(sub2);

        SBQQ__Subscription__c sub3 = new SBQQ__Subscription__c();
        sub3.SBQQ__Quantity__c = 1;
        sub3.Affiliation__c = affil.id;
        sub3.SBQQ__NetPrice__c = 100;
        sub3.Start_Date__c = system.today().addDays(-400);
        sub3.End_Date__c = system.today().addDays(300);
        subs.add(sub3);  

        insert subs;

        Test.startTest();
        Database.executeBatch(new JVCO_CreateChurnLead());
        Test.stopTest();
        
        Lead churnLead = [SELECT ID, JVCO_Potential_Amount__c FROM Lead WHERE LeadSource = 'Churn' LIMIT 1];
        System.assertEquals(300, churnLead.JVCO_Potential_Amount__c);
    }
    
    //If there is a second open affiliation on the venue then the affiliation should be marked as not required
    @isTest static void test2ndOpenAffiliation(){
        
        //Close original affiliation with end date in past
        JVCO_Affiliation__c affil = [SELECT ID FROM JVCO_Affiliation__c LIMIT 1];
        affil.JVCO_Closure_Reason__c = 'Ceased Trading';
        affil.JVCO_Start_Date__c = system.today()-2;
        affil.JVCO_End_Date__c = system.today()-1;
        update affil;
        
        //Insert a 2nd Open affiliation on the venue
        Account custAcc = [SELECT ID FROM Account WHERE RecordType.Name = 'Customer Account' LIMIT 1];
        c2g__codaTaxCode__c taxCode = [SELECT ID FROM c2g__codaTaxCode__c LIMIT 1];
        Contact c = [SELECT ID FROM Contact LIMIT 1];
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = [SELECT ID FROM c2g__codaGeneralLedgerAccount__c LIMIT 1];
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        //Find original Venue
        JVCO_Venue__c venue = [SELECT ID FROM JVCO_Venue__c LIMIT 1];
        
        //Create second affiliation which isn't closed
        JVCO_Affiliation__c Affil2 = new JVCO_Affiliation__c(); 
        Affil2.JVCO_Account__c = licAcc.id;
        Affil2.JVCO_Venue__c = venue.id;
        Affil2.JVCO_Start_Date__c = system.today().addDays(-20);
        insert Affil2;

        Test.startTest();
        Database.executeBatch(new JVCO_CreateChurnLead());
        Test.stopTest();
        
        JVCO_Affiliation__c affiliation = [SELECT ID, JVCO_Churn_Not_Required__c FROM JVCO_Affiliation__c WHERE JVCO_Closure_Reason__c = 'Ceased Trading'];
        System.assertEquals(TRUE,affiliation.JVCO_Churn_Not_Required__c);

    }
    
    @isTest static void testErrorLog(){
        JVCO_CreateChurnLead CreateChurnLead = new JVCO_CreateChurnLead();
        
        Test.startTest();
        insert CreateChurnLead.createErrorLog('Create Churn Leads','Error Message','JVCO_CreateChurnLead');
        Test.stopTest();
        
        List<ffps_custRem__Custom_Log__c> errorLog = [SELECT ID
                                                       FROM ffps_custRem__Custom_Log__c
                                                       WHERE ffps_custRem__Grouping__c = 'Create Churn Leads'
                                                       AND ffps_custRem__Message__c = 'JVCO_CreateChurnLead'
                                                       LIMIT 1];
         System.assertEquals(1,errorLog.size());
    }
        
}