@isTest
public class JVCO_RecalculateOldTLI_Batch_Test
{
    /*@testSetup
    static void createData()
    {
        JVCO_TestClassHelper.createBillingConfig();
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
    
        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        glaList.add(JVCO_TestClassHelper.setGLA('CONV', testGroup.Id));
        glaList.add(JVCO_TestClassHelper.setGLA('PPLCLEAR', testGroup.Id));
        glaList.add(JVCO_TestClassHelper.setGLA('PRSCLEAR', testGroup.Id));
        glaList.add(JVCO_TestClassHelper.setGLA('VPLCLEAR', testGroup.Id));
        insert glaList;
        
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_AccountTrigger__c = true;
        insert dt;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2();
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim3();
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        c2g__codaPeriod__c pr = JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert pr;
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        JVCO_FFUtil.stopCodaCreditNoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountDLRSHandler = true;

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        insert JVCO_TestClassHelper.setQuoteProcess();
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;


        Test.startTest();
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopOrderBeforeUpdateHandler = true;
        JVCO_FFUtil.stopOrderAfterUpdateHandler = true;

        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        insert new GLA_for_Cash_Transfer__c(name = '10040 - Accounts receivables control');
        
        list<blng__Invoice__c> bInvList = [select id from blng__Invoice__c limit 1];
        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        insert bInv;
        insert JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);
        bInvList.add(bInv);
        JVCO_InvoiceGenerationLogic.generateInvoiceFromSingleOrder(bInvList);
        Test.stopTest();
    }
    @isTest
    static void startBatch()
    {
        c2g__codaInvoice__c sInv = [SELECT Id, c2g__Account__c, c2g__OutstandingValue__c, Name FROM c2g__codaInvoice__c LIMIT 1];
        c2g__codaBankAccount__c bAcc = [SELECT Id FROM c2g__codaBankAccount__c LIMIT 1];
        c2g__codaCashEntry__c cEntry = new c2g__codaCashEntry__c();
        cEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cEntry.c2g__BankAccount__c = bAcc.Id;
        insert cEntry;
        Test.startTest();
        c2g__codaCashEntryLineItem__c cLine = new c2g__codaCashEntryLineItem__c();
        cLine.c2g__Account__c = sInv.c2g__Account__c;
        cLine.c2g__AccountReference__c = sInv.Name;
        cLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cLine.c2g__CashEntryValue__c = 15000;
        cLine.c2g__CashEntry__c = cEntry.Id;
        insert cLine;
        
        c2g.CODAAPICommon_7_0.Context contextObj = new c2g.CODAAPICommon_7_0.Context();
        List<c2g.CODAAPICommon.Reference> referenceList = new List<c2g.CODAAPICommon.Reference>();
        c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
        ref.Id = cEntry.id;
        referenceList.add(ref);
        c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(contextObj, referenceList);
        Id batchJobId = Database.executeBatch(new JVCO_RecalculateOldTLI_Batch(), 200);
        Test.stopTest();
    }
    @isTest
    static void startOverridenBatch()
    {
        c2g__codaInvoice__c sInv = [SELECT Id, c2g__Account__c, c2g__OutstandingValue__c, Name FROM c2g__codaInvoice__c LIMIT 1];
        c2g__codaBankAccount__c bAcc = [SELECT Id FROM c2g__codaBankAccount__c LIMIT 1];
        c2g__codaCashEntry__c cEntry = new c2g__codaCashEntry__c();
        cEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cEntry.c2g__BankAccount__c = bAcc.Id;
        insert cEntry;
        Test.startTest();
        c2g__codaCashEntryLineItem__c cLine = new c2g__codaCashEntryLineItem__c();
        cLine.c2g__Account__c = sInv.c2g__Account__c;
        cLine.c2g__AccountReference__c = sInv.Name;
        cLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cLine.c2g__CashEntryValue__c = 15000;
        cLine.c2g__CashEntry__c = cEntry.Id;
        insert cLine;
        
        c2g.CODAAPICommon_7_0.Context contextObj = new c2g.CODAAPICommon_7_0.Context();
        List<c2g.CODAAPICommon.Reference> referenceList = new List<c2g.CODAAPICommon.Reference>();
        c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
        ref.Id = cEntry.id;
        referenceList.add(ref);
        c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(contextObj, referenceList);
        Id batchJobId = Database.executeBatch(new JVCO_RecalculateOldTLI_Batch('SELECT Id FROM c2g__codaTransactionLineItem__c'), 200);
        Test.stopTest();
    }*/
}