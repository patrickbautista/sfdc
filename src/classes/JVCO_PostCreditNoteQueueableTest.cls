@isTest
private class JVCO_PostCreditNoteQueueableTest
{
    @testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();        
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        //added
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_OpportunityTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_QuoteLineGroupTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        insert dt;
        
        JVCO_FFUtil.stopAccountDLRSHandler = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopCodaInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopContractHandlerAfterUpdate = true;
        JVCO_FFUtil.stopContractHandlerBeforeDelete = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_FFUtil.stopQuoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopQuoteHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopOrderAfterUpdateHandler = true;
        JVCO_FFUtil.stopOrderBeforeUpdateHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        
        c2g__codaCashMatchingSettings__c settings = new c2g__codaCashMatchingSettings__c();
        settings.JVCO_Write_off_Limit__c = 1.00;
        insert settings;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');

        //original //Test.startTest();
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
 
        //create creditnote 1
        c2g__codaCreditNote__c testCreditNote1 = new c2g__codaCreditNote__c();
        testCreditNote1.c2g__Account__c = licAcc.Id;
        testCreditNote1.c2g__CreditNoteStatus__c = 'In Progress';
        insert testCreditNote1;
        
        //create creditnote 2
        c2g__codaCreditNote__c testCreditNote2 = new c2g__codaCreditNote__c();
        testCreditNote2.c2g__Account__c = licAcc.Id;
        testCreditNote2.c2g__CreditNoteStatus__c = 'In Progress';
        insert testCreditNote2;
        
        //create creditnote line item 1
        c2g__codaCreditNoteLineItem__c testCreditNoteLineItem1 = new c2g__codaCreditNoteLineItem__c();
        testCreditNoteLineItem1.c2g__CreditNote__c = testCreditNote1.Id;
        testCreditNoteLineItem1.c2g__quantity__c = 1;
        testCreditNoteLineItem1.c2g__unitprice__c = 10;
        testCreditNoteLineItem1.c2g__Product__c = p.Id;
        insert testCreditNoteLineItem1;
        
        //create creditnote line item 2
        c2g__codaCreditNoteLineItem__c testCreditNoteLineItem2 = new c2g__codaCreditNoteLineItem__c();
        testCreditNoteLineItem2.c2g__CreditNote__c = testCreditNote2.Id;
        testCreditNoteLineItem2.c2g__quantity__c = 2;
        testCreditNoteLineItem2.c2g__unitprice__c = 20;
        testCreditNoteLineItem2.c2g__Product__c = p.Id;
        insert testCreditNoteLineItem2;
    }
    
    @isTest
    static void testPostCreditNoteDefault()
    {       
        Test.startTest();
        JVCO_PostCreditNoteQueueable.start();
        Test.stopTest();
        System.assertEquals(2, [SELECT Id FROM c2g__codaCreditNote__c WHERE c2g__CreditNoteStatus__c = 'Complete'].size());        
    }
    @isTest
    static void testPostCreditNoteOverrideQueryString()
    {
        String queryString = 'SELECT Id FROM c2g__codaCreditNote__c WHERE c2g__CreditNoteStatus__c = \'In Progress\'';        
        Test.startTest();
        JVCO_PostCreditNoteQueueable.start(queryString);
        Test.stopTest();
        System.assertEquals(2, [SELECT Id FROM c2g__codaCreditNote__c WHERE c2g__CreditNoteStatus__c = 'Complete'].size());        
    }
    @isTest
    static void testPostCreditNoteOverrideQueryStringAndBatchSize()
    {
        String queryString = 'SELECT Id FROM c2g__codaCreditNote__c WHERE c2g__CreditNoteStatus__c = \'In Progress\'';        
        Test.startTest();
        JVCO_PostCreditNoteQueueable.start(queryString, 201);
        Test.stopTest();
        System.assertEquals(2, [SELECT Id FROM c2g__codaCreditNote__c WHERE c2g__CreditNoteStatus__c = 'Complete'].size());        
    }
    @isTest
    static void testPostCreditNoteOverrideBatchSize()
    {  
        Test.startTest();
        JVCO_PostCreditNoteQueueable.start(201);
        Test.stopTest();
        System.assertEquals(2, [SELECT Id FROM c2g__codaCreditNote__c WHERE c2g__CreditNoteStatus__c = 'Complete'].size());        
    }
    @isTest
    static void testPostCreditNoteOverrideBatchSizeAndQueueableLimit()
    {  
        Test.startTest();
        JVCO_PostCreditNoteQueueable.start(201,51);
        Test.stopTest();
        System.assertEquals(2, [SELECT Id FROM c2g__codaCreditNote__c WHERE c2g__CreditNoteStatus__c = 'Complete'].size());        
    }
    @isTest
    static void testPostCreditNoteOverrideQueryStringBatchSizeAndQueueableLimit()
    {
        String queryString = 'SELECT Id FROM c2g__codaCreditNote__c WHERE c2g__CreditNoteStatus__c = \'In Progress\'';        
        Test.startTest();
        JVCO_PostCreditNoteQueueable.start(queryString, 201, 51);
        Test.stopTest();
        System.assertEquals(2, [SELECT Id FROM c2g__codaCreditNote__c WHERE c2g__CreditNoteStatus__c = 'Complete'].size());        
    }
    @isTest
    static void testPostCreditNoteListAsParameter()
    {
        List<c2g__codaCreditNote__c> testCreditNotes = [SELECT Id FROM c2g__codaCreditNote__c WHERE c2g__CreditNoteStatus__c = 'In Progress'];
        Test.startTest();
        JVCO_PostCreditNoteQueueable.start(testCreditNotes, 1, 1, true);
        Test.stopTest();
        System.assertEquals(1, [SELECT Id FROM c2g__codaCreditNote__c WHERE c2g__CreditNoteStatus__c = 'Complete'].size());        
    }
}