/* ----------------------------------------------------------------------------------------------
Name: JVCO_DCAQueryCasesBatchProcess
Description: Batch Class for Closing Case Who Meets Certain Criteria

Date            Version     Author              Summary of Changes 
-----------     -------     -----------------   -----------------------------------------
21-May-2018     0.1         ralph.l.a.recio      Intial creation
17-Jul-2018     0.2			jasper.j.figueroa	 Refactored the first condition to also include on hold scenario. Added a new condition for cases with blank status and reason code. Updated the queru
10-Oct-2018		0.3			patrick.t.bautista	 GREEN-31578 removed condition of DCA Status = null and Status Reason Code = null
----------------------------------------------------------------------------------------------- */
global class JVCO_DCAQueryCasesBatchProcess  implements Database.Batchable<sObject>{
    
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([SELECT id, Status, Description,
                                         JVCO_DCA_Status__c, Type,
                                         JVCO_Status_Reason_Code__c,
                                         AccountId,  Account.JVCO_Incoming_Reason_Code__c,
                                         Account.JVCO_Incoming_Status_to_DCA__c
                                         FROM Case 
                                         WHERE Status ='Open' 
                                         AND Type= 'Query'
                                         AND Account.JVCO_Incoming_Reason_Code__c != null 
                                         AND Account.JVCO_Incoming_Status_to_DCA__c != null]);
    }
    
    global void execute(Database.BatchableContext BC, List<Case> scope)
    {
        List<Case> CaseToClose = new List<Case>();
        For(case caseVar: scope)
        {
            if((caseVar.Account.JVCO_Incoming_Status_to_DCA__c == 'Withdrawal' &&
                (caseVar.Account.JVCO_Incoming_Reason_Code__c == '100 - PAID IN FULL' ||  
                 caseVar.Account.JVCO_Incoming_Reason_Code__c == '220 - RECALLED TO JVCO' ||
                 caseVar.Account.JVCO_Incoming_Reason_Code__c == '230 - ACCOUNT CLOSED BY JVCO'))
               || (caseVar.Account.JVCO_Incoming_Status_to_DCA__c == 'On Hold' &&
                   (caseVar.Account.JVCO_Incoming_Reason_Code__c == '370 - REQUEST TO PAY' ||
                    caseVar.Account.JVCO_Incoming_Reason_Code__c == '380 - PROMISE TO PAY')))
            {
                caseVar.Status = 'Closed';
                caseVar.JVCO_DCA_Status__c = caseVar.Account.JVCO_Incoming_Status_to_DCA__c;
                caseVar.JVCO_Status_Reason_Code__c = caseVar.Account.JVCO_Incoming_Reason_Code__c;
                CaseToClose.add(caseVar);
            }
        }
        if(!CaseToClose.isEmpty())
        {
            update CaseToClose;
        }
    }
    global void finish(Database.BatchableContext BC){}
}