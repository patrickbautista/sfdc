@isTest 
private class JVCO_AccountTriggerHandlerTest{

    private static Account licAcc;
    private static Account customerAcc;
    private static Account supplierAcc;
    
    @testSetup
    public static void setupData(){
        List<User> uList = new List<User>(); 
        Profile p = [select id from Profile where Name = 'Licensing User' limit 1];
        User runningUser = new User();
    
        UserRole uRole = new UserRole();
        uRole.DeveloperName = 'JVCO_Generic_Test_User'; 
        uRole.Name = 'Generic Test User';
        insert uRole;
    
            runningUser = new User(
            Alias = 'athr',
            Email='JVCO_AccountTriggerHandlerTest1@gmail.com', 
            EmailEncodingKey='UTF-8',
            FirstName = 'Generic', 
            LastName='User',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            UserRoleId = uRole.id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='guuserstand3541987*@testorg.com',
            Division='Legal',
            Department='Legal'
        );
        uList.add(runningUser);

        runningUser = new User(
            Alias = 'athr',
            Email='JVCO_AccountTriggerHandlerTest2@gmail.com', 
            EmailEncodingKey='UTF-8',
            FirstName = 'Another', 
            LastName='Someone',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles',
            UserName='asuserstand3541987*@testorg.com',
            Division='Legal',
            Department='Legal'
        );
        uList.add(runningUser);

        insert uList;
    }
    
    private static void prepareTestData(Boolean isLive, String lastAssignedDCA){
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);

        c2g__codaGeneralLedgerAccount__c testGeneralLedger = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedger.JVCO_Active__c = true;
        testGeneralLedger.name = 'Test Geenral Ledger';
        testGeneralLedger.c2g__Type__c = 'Balance Sheet';
        testGeneralLedger.c2g__ReportingCode__c = '10390';
        testGeneralLedger.c2g__GLAGroup__c = 'Cash';
        testGeneralLedger.c2g__TrialBalance1__c= 'Assets';
        testGeneralLedger.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedger.c2g__TrialBalance3__c = 'Cash and Cash Equivalents';
        testGeneralLedger.c2g__UnitOfWork__c = 182.0;
        testGeneralLedger.c2g__CashFlowCategory__c = 'Operating';
        glaList.add(testGeneralLedger);
        insert glaList;

        c2g__codaTaxCode__c testTaxCode = new c2g__codaTaxCode__c();
        testTaxCode.name = 'GB-O-STD-LIC';
        testTaxCode.c2g__GeneralLedgerAccount__c = testGeneralLedger.id;
        testTaxCode.c2g__TaxGroup__c = 'Tax Group 1';
        testTaxCode.c2g__IsParent__c = false;
        testTaxCode.c2g__UnitOfWork__c = 1.0;
        testTaxCode.ffvat__NetBox__c = 'Box 6';
        testTaxCode.ffvat__TaxBox__c = 'Box 1';
        testTaxCode.JVCO_Rate__c = 20.0;
        insert testTaxCode;

        c2g__codaTaxRate__c testTaxRate = new c2g__codaTaxRate__c();
        testTaxRate.c2g__Rate__c = 20.0;
        testTaxRate.c2g__UnitOfWork__c = 1.0;
        testTaxRate.c2g__TaxCode__c = testTaxCode.id;
        insert testTaxRate;
            
        List<JVCO_BackgroundMatching__c> bgMatchingList = new List<JVCO_BackgroundMatching__c>();
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Trading Currency', JVCO_Default_Value__c = 'GBP'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Receivable Control', JVCO_Default_Value__c = '10040'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Output Vat Code', JVCO_Default_Value__c = 'GB-O-STD-LIC'));
        
        insert bgMatchingList;
        
        List<c2g__codaDimension3__c> dimensions = new List<c2g__codaDimension3__c>();
        dimensions.add(new c2g__codaDimension3__c(Name = 'Key', c2g__ReportingCode__c = 'Key'));
        dimensions.add(new c2g__codaDimension3__c(Name = 'Non Key', c2g__ReportingCode__c = 'Non Key'));
        insert dimensions;
        
        c2g__codaDimension1__c dim1 = new c2g__codaDimension1__c(Name = 'Live', c2g__ReportingCode__c = 'BC002', JVCO_Function__c = 'Budget');
        insert dim1;
        
        c2g__codaDimension1__c dim2 = new c2g__codaDimension1__c(Name = 'General Purpose', c2g__ReportingCode__c = 'BC004', JVCO_Function__c = 'Budget');
        insert dim2;
        //JVCO_DCA_Stage_Settings__c DCA_SETTINGS = JVCO_DCA_Stage_Settings__c.getOrgDefaults();
        JVCO_DCA_Stage_Settings__c dcaAss = new JVCO_DCA_Stage_Settings__c(JVCO_Maximum_Account_Balance_to_DCA__c = 3500,JVCO_Last_Assigned_DCA__c = lastAssignedDCA);
        insert dcaAss;
        
        User runUser = [SELECT Id FROM User WHERE Email = 'JVCO_AccountTriggerHandlerTest2@gmail.com'];
        System.assertNotEquals(runUser, null);
        
        System.runAs(runUser){
            
            customerAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
            customerAcc.TCs_Accepted__c = 'No';
            customerAcc.Type = 'Agency';
            customerAcc.c2g__CODADimension1__c = null;
            insert customerAcc;
            
            supplierAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
            supplierAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            supplierAcc.JVCO_Supplier_Category__c = 'Hire and Leasing';
            supplierAcc.c2g__CODACreditStatus__c = 'Credit Allowed';
            insert supplierAcc;
            
            licAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
            licAcc.Name = 'JVCO_AccountTriggerHandlerTest';
            licAcc.JVCO_Live__c = isLive;
            licAcc.Type = 'Customer';
            licAcc.JVCO_Customer_Account__c = customerAcc.Id;
            licAcc.OwnerId = UserInfo.getUserId();
            licAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
            licAcc.c2g__CODAOutputVATCode__c = testTaxCode.Id;
            licAcc.c2g__CODADimension1__c = isLive ? dim1.Id : dim2.Id;
            licAcc.JVCO_DCA_Type__c = lastAssignedDCA;
            insert licAcc;
        }
    }

    private static testMethod void checkUserRole(){
        prepareTestData(false, 'Oriel');
        Account acc = [select Id, OwnerId from Account WHERE Id = :licAcc.Id];
        User u = [select Id, UserRoleId from User where Id = :acc.OwnerId];
        User userGen = [Select Id from User where UserRole.DeveloperName = 'JVCO_Generic_User' limit 1];
        User userOther = [Select Id from User where UserRole.DeveloperName != 'JVCO_Generic_User' limit 1];

        Test.startTest();
        
        acc.JVCO_In_Infringement__c = true;
        acc.OwnerId = userOther.Id;
        update acc;

        Test.stopTest();

        acc = new Account();
        acc.Type = 'Agency';
        acc = [select Id, OwnerId from Account limit 1];

        //System.assertEquals(acc.OwnerId, userGen.Id);
    }
    
    private static testMethod void testLiveLicenceAccount(){
        prepareTestData(true, 'Oriel');
        new JVCO_AccountTriggerHandler();
        Account acc = [select Id, RecordTypeId, c2g__CODADimension1__c from Account WHERE Id = :licAcc.Id];
        Account accCust = [select Id, RecordTypeId from Account WHERE Id = :customerAcc.Id];
        c2g__codaDimension1__c dimensions1 = [SELECT Id, Name FROM c2g__codaDimension1__c WHERE Name = 'General Purpose' LIMIT 1];

        Test.startTest();
        //accCust.c2g__CODADimension1__c = dimensions1.id;
        //update accCust;

        //acc.JVCO_Live__c = true;
        acc.JVCO_Credit_Status__c = 'Debt - 1st stage';
        //acc.ffps_custRem__Last_Reminder_Severity_Level__c = 3;
        acc.ffps_accbal__Account_Balance__c = 2000;
        /*acc.JVCO_AccountValue__c = 2000;*/
        acc.JVCO_Manual_pass_to_DCA__c = true;
        acc.c2g__CODADimension1__c = dimensions1.id;
        update acc;
        
        //acc = [select Id, c2g__CODADimension1__c from Account WHERE Id = :licAcc.Id];
        //List<c2g__codaDimension1__c> dimensions = [SELECT Id, Name FROM c2g__codaDimension1__c WHERE Name = 'Live' LIMIT 1];
        //System.assertEquals(1, dimensions.size());
        //System.assertEquals(dimensions[0].Id, acc.c2g__CODADimension1__c);

        acc = [select Id, JVCO_DCA_Type__c from Account WHERE Id = :licAcc.Id];
        System.assertEquals('Oriel', acc.JVCO_DCA_Type__c);

        Test.stopTest();
    }
    
    private static testMethod void testCSLDCAAssignment(){
        prepareTestData(true, 'CSL');
        new JVCO_AccountTriggerHandler();
        Account acc = [select Id, RecordTypeId from Account WHERE Id = :licAcc.Id];

        Test.startTest();

        //acc.JVCO_Live__c = true;
        acc.JVCO_Credit_Status__c = 'Passed to DCA - 1st Stage';
        acc.ffps_custRem__Last_Reminder_Severity_Level__c = 3;
        acc.ffps_accbal__Account_Balance__c = 2000;
        update acc;
        
        acc = [select Id, c2g__CODADimension1__c from Account WHERE Id = :licAcc.Id];
        List<c2g__codaDimension1__c> dimensions = [SELECT Id, Name FROM c2g__codaDimension1__c WHERE Name = 'Live' LIMIT 1];
        System.assertEquals(1, dimensions.size());
        System.assertEquals(dimensions[0].Id, acc.c2g__CODADimension1__c);

        //delete acc;
        
        Test.stopTest();
    }
    
    private static testMethod void testSupplierAccount(){
        prepareTestData(false, 'CSL');


        Test.startTest();
        supplierAcc.c2g__CODACreditStatus__c = 'On Hold';
        update supplierAcc;
        

        Test.stopTest();
    }

    private static testMethod void testSupplierAccount_CashWithOrder(){
        prepareTestData(false, 'CSL');


        Test.startTest();
        supplierAcc.c2g__CODACreditStatus__c = 'Cash with Order';
        update supplierAcc;
        

        Test.stopTest();
    }

    private static testMethod void testDeleteAccount(){
        prepareTestData(false, 'CSL');

        Test.startTest();
        delete supplierAcc;      
        Test.stopTest();
    }
    
    private static testMethod void testUpdateCustomerAccount(){
        prepareTestData(false, 'CSL');
        List<c2g__codaDimension1__c> dimensions = [SELECT Id, Name FROM c2g__codaDimension1__c WHERE Name = 'General Purpose' LIMIT 1];
        System.assertEquals(1, dimensions.size());
        
        Map<Id, Account> oldMap = new Map<Id, Account>();
        oldMap.put(customerAcc.Id, customerAcc);
        
        Test.startTest();
        Account testAcc = new Account();
        testAcc.Type = 'Customer';
        testAcc.c2g__CODADimension1__c = dimensions[0].Id;
        testAcc.TCs_Accepted__c = 'Yes';
        testAcc.Id = customerAcc.Id;
        testAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        testAcc.JVCO_HistoricCurrentValuesBatchProcessed__c = true;
        update testAcc;
        Test.stopTest();
    }

    @isTest
    static void testUpdatePhoneAndBillingAddress()
    {
        prepareTestData(false, 'Oriel');

        Contact c = new Contact();
        c.LastName = 'Doee';
        c.FirstName = 'Jane';
        c.Email = 'accountTest@testClass.com';
        c.AccountId = customerAcc.Id;
        c.Phone = '090909090909';
        c.MobilePhone = '1111111111';
        String uniq = String.valueOf(Math.random());
        c.MailingStreet = 'Test Street ' + uniq;
        c.MailingCity = 'Test City ' + uniq;
        c.MailingPostalCode = 'BB10 4RG';
        c.MailingState = 'Lacashire';
        c.OwnerId = UserInfo.getUserId();
        insert c;

        Account a = new Account();
        a.Id = customerAcc.Id;
        //a.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        //a.JVCO_Billing_Contact__c = c.Id;

        Test.startTest();
        update a;
        Test.stopTest();
    }

    // 03-01-2018 reymark.j.l.arlos
    private static testMethod void testClosedLicenceAccount()
    {
        prepareTestData(false, 'Oriel');
        Account custAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert custAcc;
        
        Account licenceAcc1 = JVCO_TestClassObjectBuilder.createLicenceAccount(custAcc.Id);
        insert licenceAcc1;

        Account licenceAcc = [Select Id, Name From Account where Id =: licenceAcc1.Id Limit 1];

        List<Opportunity> lstOpp = new List<Opportunity>();
        for(Integer i1 = 0; i1 < 2; i1++)
        {
            Opportunity oppRec = JVCO_TestClassObjectBuilder.createOpportunity(licenceAcc);
            lstOpp.add(oppRec);
        }
        insert lstOpp;

        List<JVCO_Venue__c> lstVenue = new List<JVCO_Venue__c>();
        for(Integer i3 = 0; i3 < 3; i3++)
        {
            JVCO_Venue__c venRec = new JVCO_Venue__c();
            venRec =  JVCO_TestClassObjectBuilder.createVenue();
            venRec.Name += i3;
            venRec.External_Id__c += i3;
            venRec.JVCO_Street__c += i3;
            lstVenue.add(venRec);
        }
        insert lstVenue;

        List<JVCO_Affiliation__c> lstAff = new List<JVCO_Affiliation__c>();
        for(JVCO_Venue__c venRec1 : lstVenue )
        {
            JVCO_Affiliation__c affRec = new JVCO_Affiliation__c();
            affRec = JVCO_TestClassObjectBuilder.createAffiliation(licenceAcc.Id, venRec1.Id);
            affRec.JVCO_End_Date__c = Null;
            affRec.JVCO_Closure_Reason__c = Null;
            lstAff.add(affRec);
        }
        insert lstAff;

        Product2 proRec = JVCO_TestClassObjectBuilder.createProduct();
        insert proRec;

        PriceBookEntry pbEntry = JVCO_TestClassObjectBuilder.createPriceBookEntry(proRec.Id);
        insert pbEntry;

        List<SBQQ__Subscription__c> lstSubs = new List<SBQQ__Subscription__c>();
        for(JVCO_Affiliation__c affRec1 : lstAff)
        {
            SBQQ__Subscription__c subRec = new SBQQ__Subscription__c();
            subRec = JVCO_TestClassObjectBuilder.createSubscription(licenceAcc.Id, proRec.Id);
            subRec.SBQQ__RenewalQuantity__c = 100;
            subRec.Affiliation__c = affRec1.Id;
            lstSubs.add(subRec);
        }

        insert lstSubs;

        Test.startTest();

        Id custAccId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();

        Account custAccToUpate = [Select Id, TCs_Accepted__c, JVCO_Date_TCs_Accepted__c
                                    From Account
                                    Where RecordTypeId =: custAccId
                                    LIMIT 1];

        custAccToUpate.TCs_Accepted__c = Null;

        update custAccToUpate;

        custAccToUpate.TCs_Accepted__c = 'Yes';
        custAccToUpate.JVCO_Date_TCs_Accepted__c = Date.today();

        update custAccToUpate;

        Account linAccToUpdate = [Select Id, JVCO_Closure_Reason__c, JVCO_Closure_Date__c
                                    From Account Where Id =: licenceAcc1.Id];

        linAccToUpdate.JVCO_Closure_Reason__c = 'Ceased Trading';
        linAccToUpdate.JVCO_Closure_Date__c = Date.today().addDays(10);

        Contact cnt = new Contact();
        cnt.LastName = 'Doee';
        cnt.FirstName = 'Jane';
        cnt.Email = 'accountTest@testClass.com';
        cnt.AccountId = custAcc.Id;
        cnt.Phone = Null;
        cnt.MobilePhone = '1111111111';
        String uniq = String.valueOf(Math.random());
        cnt.MailingStreet = 'Test Street ' + uniq;
        cnt.MailingCity = 'Test City ' + uniq;
        cnt.MailingPostalCode = 'BB10 4RG';
        cnt.MailingState = 'Lacashire';
        cnt.OwnerId = UserInfo.getUserId();
        insert cnt;
        
        linAccToUpdate.JVCO_Billing_Contact__c = cnt.Id;

        update linAccToUpdate;
        Test.stopTest();

        //Account acc = new Account();
        //System.assertEquals(acc, custAccToUpate);

        List<Opportunity> oppCheck = [Select Id, StageName 
                                        From Opportunity
                                        Where AccountId =: licenceAcc1.Id ];

        if(!oppCheck.isEmpty())
        {
            Integer oppCheckCtr = 0;
            for(Opportunity oppCheckRec : oppCheck)
            {
                if(oppCheckRec.StageName == 'Closed Lost')
                {
                    oppCheckCtr++;
                }
            }

            System.assertEquals(oppCheck.size(), oppCheckCtr);
        }

        List<JVCO_Affiliation__c> affCheck = [Select Id, JVCO_Closure_Reason__c, JVCO_End_Date__c
                                                From JVCO_Affiliation__c
                                                Where JVCO_Account__c =: licenceAcc1.Id];

        if(!affCheck.isEmpty())
        {
            Integer affCheckCtr = 0;
            for(JVCO_Affiliation__c affCheckRec : affCheck)
            {
                if(affCheckRec.JVCO_Closure_Reason__c == 'Ceased Trading' &&
                    affCheckRec.JVCO_End_Date__c == Date.today().addDays(10) )
                {
                    affCheckCtr++;
                }
            }

            System.assertEquals(affCheck.size(), affCheckCtr);
        }

        List<SBQQ__Subscription__c> subsCheck = [Select Id, SBQQ__RenewalQuantity__c
                                                    From SBQQ__Subscription__c
                                                    where SBQQ__Account__c =: licenceAcc.Id];

        if(!subsCheck.isEmpty())
        {
            Integer subsCheckCtr = 0;
            for(SBQQ__Subscription__c subsCheckRec : subsCheck)
            {
                if(subsCheckRec.SBQQ__RenewalQuantity__c == 0)
                {
                    subsCheckCtr++;
                }
            }

            System.assertEquals(subsCheck.size(), subsCheckCtr);
        }
        
    }

    private static testMethod void testContactUpdate()
    {
        prepareTestData(false, 'CSL');
        
        Account custAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert custAcc;
        
        Account licenceAcc1 = JVCO_TestClassObjectBuilder.createLicenceAccount(custAcc.Id);
        insert licenceAcc1;
        
        Contact cnt = [SELECT id, AccountId, Phone, MobilePhone, MailingState, MailingPostalCode,
                       MailingCity, MailingStreet 
                       FROM Contact Limit 1];
        
        
        Test.startTest();
        cnt.AccountId = custAcc.Id;
        cnt.Phone = Null;
        cnt.MobilePhone = '1111111111';
        cnt.MailingState = 'Test State';
        cnt.MailingPostalCode = '123456';
        cnt.MailingCity = 'Test City';
        cnt.MailingStreet = 'Test Street';
        cnt.Phone = '111113456';
        cnt.MailingState = 'Test Change State';
        update cnt;
        Test.stopTest();
        
    }
    
    private static testMethod void testAffiliationNameUpdate()
    {
        prepareTestData(false, 'Oriel');
        Account acc = [select Id, OwnerId from Account WHERE Id = :licAcc.Id];

        Test.startTest();

        JVCO_Venue__c venRec = new JVCO_Venue__c();
        venRec =  JVCO_TestClassObjectBuilder.createVenue();
        venRec.Name = 'VenueAff'; 
        insert venRec;

        JVCO_Affiliation__c affRec = new JVCO_Affiliation__c();
        affRec = JVCO_TestClassObjectBuilder.createAffiliation(acc.Id, venRec.Id);
        affRec.JVCO_End_Date__c = Null;
        affRec.JVCO_Closure_Reason__c = Null;
        insert affRec;

        JVCO_Affiliation__c aff = [SELECT Id, Name, JVCO_Venue_Name__c, JVCO_Account__c, JVCO_Account__r.Name FROM JVCO_Affiliation__c WHERE JVCO_Account__c = :acc.Id];
        String nameTest = aff.JVCO_Account__r.Name + ':' + aff.JVCO_Venue_Name__c;
        System.assertEquals(nameTest, aff.Name);

        acc.JVCO_Account_Description__c = 'Test';
        update acc;

        aff = [SELECT Id, Name, JVCO_Venue_Name__c, JVCO_Account__c, JVCO_Account__r.Name FROM JVCO_Affiliation__c WHERE JVCO_Account__c = :acc.Id];
        nameTest = aff.JVCO_Account__r.Name + ':' + aff.JVCO_Venue_Name__c;
        System.assertEquals(nameTest, aff.Name);

        Test.stopTest();

        
    }

    @isTest
    static void testStampCurrentValues()
    {
        SBQQ.TriggerControl.disable();
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        insert dt;
        
        prepareTestData(false, 'Oriel');
        Account accLicence = [select Id, OwnerId, JVCO_HistoricCurrentValuesBatchProcessed__c from Account WHERE Id = :licAcc.Id];
        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(accLicence.id, qProcess.id, null, null, null);
        insert q;

        SBQQ__Quote__c q2 = JVCO_TestClassObjectBuilder.createQuote(accLicence.id, qProcess.id, null, null, null);
        q2.SBQQ__PriceBook__c = NULL;
        insert q2;

        Contract c = JVCO_TestClassObjectBuilder.createContract(accLicence.id, q.id);
        insert c;

        Contract c2 = JVCO_TestClassObjectBuilder.createContract(accLicence.id, q.id);
        insert c2;

        Contract c3 = JVCO_TestClassObjectBuilder.createContract(accLicence.id, q2.id);
        insert c3;
        
        List<Contract> contList = [SELECT Id, AccountId, StartDate, EndDate FROM Contract WHERE AccountId =:accLicence.Id order by CreatedDate DESC limit 3];

        Integer startDate = -364;
        Integer endDate = 1;
        for(Contract cont : contList)
        {
            date sd = date.today().addDays(startDate);
            date ed = date.today().addDays(endDate);
            cont.StartDate = sd;
            cont.EndDate = ed;
            cont.AccountId = accLicence.Id;
            cont.JVCO_RenewableQuantity__c = 60;
            startDate--;
            endDate++;
        }

        Test.startTest();
        update contList;

        Product2 proRecPRS = JVCO_TestClassObjectBuilder.createProduct();
        proRecPRS.JVCO_Society__c = 'PRS';
        insert proRecPRS; 

        Product2 proRecPPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecPPL.JVCO_Society__c = 'PPL';
        insert proRecPPL;

        Product2 proRecVPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecVPL.JVCO_Society__c = 'VPL';
        insert proRecVPL;
        Test.stopTest();

        List<SBQQ__Subscription__c> subList = new List<SBQQ__Subscription__c>();

        for(Contract cont : contList)
        {
            SBQQ__Subscription__c subsPRS = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecPRS.Id);
            subsPRS.SBQQ__Contract__c = cont.Id;
            subsPRS.SBQQ__ListPrice__c = 10;
            subsPRS.SBQQ__RenewalQuantity__c = 10;
            subsPRS.SBQQ__Quantity__c = 10;
            subsPRS.SBQQ__NetPrice__c = 10;
            subsPRS.SBQQ__TerminatedDate__c = null;
            subsPRS.Start_Date__c = cont.StartDate;
            subsPRS.End_Date__c = cont.EndDate;
            subsPRS.SBQQ__SubscriptionStartDate__c = cont.StartDate;
            subsPRS.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsPRS.ChargeYear__c = 'Current Year';
            subList.add(subsPRS);

            SBQQ__Subscription__c subsPPL = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecPPL.Id);
            subsPPL.SBQQ__Contract__c = cont.Id;
            subsPPL.SBQQ__ListPrice__c = 10;
            subsPRS.SBQQ__RenewalQuantity__c = 10;
            subsPPL.SBQQ__Quantity__c = 20;
            subsPPL.SBQQ__NetPrice__c = 10;
            subsPPL.SBQQ__TerminatedDate__c = null;
            subsPPL.Start_Date__c = cont.StartDate;
            subsPPL.End_Date__c = cont.EndDate;
            subsPPL.SBQQ__SubscriptionStartDate__c =cont.StartDate;
            subsPPL.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsPPL.ChargeYear__c = 'Current Year';
            subList.add(subsPPL);

             SBQQ__Subscription__c subsVPL = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecVPL.Id);
            subsVPL.SBQQ__Contract__c = cont.Id;
            subsVPL.SBQQ__ListPrice__c = 10;
            subsVPL.SBQQ__RenewalQuantity__c = 30;
            subsVPL.SBQQ__Quantity__c = 30;
            subsVPL.SBQQ__NetPrice__c = 10;
            subsVPL.SBQQ__TerminatedDate__c = null;
            subsVPL.Start_Date__c = cont.StartDate;
            subsVPL.End_Date__c = cont.EndDate;
            subsVPL.SBQQ__SubscriptionStartDate__c =cont.StartDate;
            subsVPL.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsVPL.ChargeYear__c = 'Current Year';
            subList.add(subsVPL);
        }

        //Test.startTest();
        
        insert subList;

        JVCO_HistoricCurrentValuesSetting__c customSet = JVCO_HistoricCurrentValuesSetting__c.getOrgDefaults();

        customSet.JVCO_EnableInitialDataUpdate__c = true;

        JVCO_CalcHistoricCurrentValuesBatch batchRunTest = new JVCO_CalcHistoricCurrentValuesBatch(accLicence.Id);

        //Test.stopTest();


        Account accLicence1 = [Select Id, JVCO_HistoricCurrentValuesBatchProcessed__c From Account where Id =: accLicence.Id limit 1];

        system.assertEquals(false, accLicence1.JVCO_HistoricCurrentValuesBatchProcessed__c);

    }

    @isTest
    static void testAccountExpiryDate()
    {
        prepareTestData(false, 'Oriel');
        Account custAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert custAcc;
        
        Account accLicence = JVCO_TestClassObjectBuilder.createLicenceAccount(custAcc.Id);
        accLicence.JVCO_Active__c = true;
        insert accLicence;

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(accLicence.id, qProcess.id, null, null, null);
        insert q;

        SBQQ__Quote__c q2 = JVCO_TestClassObjectBuilder.createQuote(accLicence.id, qProcess.id, null, null, null);
        q2.SBQQ__PriceBook__c = NULL;
        insert q2;
        //Test.startTest();
        Contract c = JVCO_TestClassObjectBuilder.createContract(accLicence.id, q.id);
        c.EndDate = Date.today().addDays(365);
        c.JVCO_RenewableQuantity__c = 150;
        insert c;

        Test.startTest();
        accLicence.JVCO_Live__c = true;
        update accLicence;

        accLicence.JVCO_Review_Frequency__c = 'Quarterly';
        update accLicence;

        accLicence.JVCO_Live__c = false;
        update accLicence;
        Test.stopTest();
        
        List<Contract> contList = [SELECT Id, AccountId, StartDate, EndDate FROM Contract WHERE AccountId =:accLicence.Id order by CreatedDate DESC limit 1];
    }

    @isTest
    static void testInfringementInjunctionEnforcementMethod()
    {   
        prepareTestData(false, 'Oriel');

        Account custAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        custAcc.JVCO_Injunction__c = false;
        custAcc.JVCO_In_Infringement__c = false;
        insert custAcc;

        Account accLicence = JVCO_TestClassObjectBuilder.createLicenceAccount(custAcc.Id);
        accLicence.JVCO_Active__c = true;
        accLicence.JVCO_Closure_Date__c = null;
        accLicence.JVCO_Closure_Reason__c = null;
        accLicence.ffps_accbal__Account_Balance__c = 1;
        insert accLicence;

        Test.startTest();

        custAcc.JVCO_In_Infringement__c = true;
        update custAcc;
        
        Test.stopTest();
    }

    @isTest
    static void testAgencyDiscAmount()
    {
        prepareTestData(false, 'Oriel');
        Account custAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        custAcc.Type = 'Agency';
        insert custAcc;

        Account accLicence = JVCO_TestClassObjectBuilder.createLicenceAccount(custAcc.Id);
        accLicence.Type = 'Customer';
        accLicence.JVCO_Agency__c = custAcc.Id;
        insert accLicence;

        Test.startTest();
        custAcc.JVCO_Account_Discount_Value__c = '10%';
        update custAcc;
        Test.stopTest();
    }

    @isTest
    static void testCreateDCA()
    {
        prepareTestData(false, 'Oriel');
        Account custAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert custAcc;

        Account accLicence = JVCO_TestClassObjectBuilder.createLicenceAccount(custAcc.Id);
        insert accLicence;

        Test.startTest();
        accLicence.JVCO_Outgoing_Status_to_DCA_2nd_Stage__c = 'WITHDRAWAL';
        accLicence.JVCO_Outgoing_Reason_Code_DCA_2nd_Stage__c = '100 - PAID IN FULL';
        update accLicence;
        Test.stopTest();
    }

    @isTest
    static void testUpdateCAIfLAIsPermitHolder()
    {
        prepareTestData(false, 'Oriel');
        Account custAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert custAcc;

        Account accLicence = JVCO_TestClassObjectBuilder.createLicenceAccount(custAcc.Id);
        accLicence.JVCO_Permit_Holder__c = true;
        insert accLicence;

        Test.startTest();
        accLicence.JVCO_Permit_Holder__c = false;
        accLicence.JVCO_Non_Auto_Renewable_Product_Count__c = 1;
        accLicence.JVCO_Review_Type__c = 'Automatic';
        update accLicence;

        accLicence.JVCO_Permit_Holder__c = true;
        update accLicence;
        Test.stopTest();
    }
    /*
    @isTest
    static void testResubmitForApproval()
    {
        prepareTestData(false, 'Oriel');
        Id suppAccRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account custAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        custAcc.JVCO_Supplier_Category__c = 'Hire and Leasing';
        custAcc.c2g__CODACreditStatus__c = 'Credit Allowed';
        custAcc.BillingCity = 'Test';
        custAcc.c2g__CODABankAccountName__c = 'A';
        custAcc.Type = 'Supplier';
        custAcc.RecordTypeId = suppAccRecTypeId;
        insert custAcc;

        Test.startTest();
        custAcc.Type = 'Supplier';
        custAcc.BillingCity = 'TestCity';
        custAcc.c2g__CODABankAccountName__c = 'B';
        custAcc.BillingCountry = 'TestCountry';
        update custAcc;

        Test.stopTest();
    }*/

    @isTest
    static void testUpdateCreditStatus()
    {
        prepareTestData(false, 'Oriel');

        Account custAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert custAcc;

        Account accLicence = JVCO_TestClassObjectBuilder.createLicenceAccount(custAcc.Id);
        accLicence.JVCO_DCA_Type__c = 'CSL';
        accLicence.JVCO_DCA_Type_2nd_Stage__c = 'Oriel';
        insert accLicence;

        //Test.startTest();
        accLicence.ffps_accbal__Account_Balance__c = 0;
        accLicence.JVCO_Credit_Status__c = 'Not in Debt';
        update accLicence;

        accLicence.ffps_custRem__Last_Reminder_Severity_Level__c = 2;
        update accLicence;

        Test.startTest();
        accLicence.ffps_custRem__Last_Reminder_Severity_Level__c = 4;
        update accLicence;

        accLicence.ffps_custRem__Last_Reminder_Severity_Level__c = 5;
        update accLicence;

        accLicence.ffps_custRem__Exclude_From_Reminder_Process__c = true;
        accLicence.ffps_custRem__Last_Reminder_Severity_Level__c = 1;
        accLicence.ffps_accbal__Account_Balance__c = 1;
        update accLicence;

        accLicence.ffps_custRem__Last_Reminder_Severity_Level__c = 3;
        accLicence.ffps_accbal__Account_Balance__c = 4000;
        accLicence.JVCO_Credit_Status__c = 'Passed to DCA - 1st Stage';
        update accLicence;

        accLicence.ffps_custRem__Last_Reminder_Severity_Level__c = 3;
        accLicence.ffps_accbal__Account_Balance__c = 5;
        accLicence.JVCO_Credit_Status__c = 'Not in Debt';
        update accLicence;

        accLicence.JVCO_Manual_Passed_to_DCA_2nd_Stage__c = true;
        update accLicence;

        accLicence.JVCO_DCA_Send_to_Letter__c = true;
        update accLicence;

        accLicence.ffps_custRem__Last_Reminder_Severity_Level__c = 3;
        accLicence.ffps_accbal__Account_Balance__c = 400;
        accLicence.JVCO_Incoming_Status_from_DCA_2nd_Stage__c = 'WITHDRAWAL';
        accLicence.JVCO_Credit_Status__c = 'Passed to DCA – 2nd Stage';
        update accLicence;

        /* accLicence.JVCO_Manual_pass_to_DCA__c = true;
        accLicence.JVCO_Credit_Status__c = 'Passed to DCA - 1st Stage';
        update accLicence; */

        Test.stopTest();
    }

    @isTest
    static void testUpdateCreditStatus2()
    {
        prepareTestData(false, 'Oriel');
        Account accLicence = [select Id, OwnerId,JVCO_DCA_Type__c,
                              JVCO_DCA_Type_2nd_Stage__c,
                              ffps_accbal__Account_Balance__c,
                              JVCO_Credit_Status__c
                              from Account WHERE Id = :licAcc.Id];
        accLicence.JVCO_DCA_Type__c = 'CSL';
        accLicence.JVCO_DCA_Type_2nd_Stage__c = 'Oriel';
        accLicence.ffps_accbal__Account_Balance__c = -10;
        accLicence.JVCO_Credit_Status__c = 'Passed to DCA – 2nd Stage';
        update accLicence;
    }

    @isTest
    static void testIsAsyncMethod()
    {
        JVCO_AccountTriggerHandler.isAsync();
    }
}