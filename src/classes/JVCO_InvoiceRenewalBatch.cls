/* ----------------------------------------------------------------------------------------------
   Name: JVCO_InvoiceRenewalBatch.cls 
   Description: Batch class that handles Invoice generation of renewed Orders

   Date         Version     Author                     Summary of Changes 
   -----------  -------     -----------------          -----------------------------------------
  26-Mar-2017   0.1         ryan.i.r.limlingan         Intial creation
  24-Aug-2017   0.2         ryan.i.r.limlingan         GREEN-22238: Changed query to resolve issue
  03-May-2018   0.3         filip.bezak@accenture.com  GREEN-31676 - TCP - Sales credit notes containing PPL product have PRS populated in the Parent field
  ----------------------------------------------------------------------------------------------- */
global class JVCO_InvoiceRenewalBatch implements Database.Batchable<sObject>
{
    global JVCO_InvoiceRenewalBatch() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id,
                                         AccountId,
                                         SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c,
                                         TotalAmount
                                         FROM Order
                                         WHERE JVCO_Invoiced__c = false
                                         AND SBQQ__Quote__r.JVCO_Quote_Auto_Renewed__c = true]);
    }

    global void execute(Database.BatchableContext BC, List<Order> scope)
    {
        System.debug('scope: ' + scope);
        //Database.executeBatch(new JVCO_OrderMultipleBillNowBatch(scope), 1);
        //JVCO_BillNowLogic.logInvalidOrders(JVCO_BillNowLogic.removeInvalidOrders(scope));
        //JVCO_BillNowLogic.tickBillNow(scope);
    }
    
    global void finish(Database.BatchableContext BC)
    {
        // Query all Price Schedule records and delete them
        List<SBQQ__PriceSchedule__c> pSchedToDeleteList = [SELECT Id FROM SBQQ__PriceSchedule__c];
        if (!pSchedToDeleteList.isEmpty())
        {
            System.debug('deleted size: ' + pSchedToDeleteList.size());
            delete pSchedToDeleteList;
        }
    }
}