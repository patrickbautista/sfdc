public class JVCO_PayonomyMatchRefundQueueable implements Queueable{
    
    private c2g__codaCashEntry__c refundCashEntry;
    private List<c2g__codaTransactionLineItem__c> receiptCashTransLineList;
    
    public JVCO_PayonomyMatchRefundQueueable(c2g__codaCashEntry__c refundCashEntry, List<c2g__codaTransactionLineItem__c> receiptCashTransLineList)
    {
        this.refundCashEntry = refundCashEntry;
        this.receiptCashTransLineList = receiptCashTransLineList;
    }
    public void execute(QueueableContext context)
    {
        if(!Test.isRunningTest()){
            JVCO_CashUnmatchingLogic.rematchToRefund(refundCashEntry, receiptCashTransLineList);
        }
        Map<Id, Id> licAndCustAccIdMap = new Map<Id, Id>();
        
        c2g__codaCashEntry__c cEntry = [SELECT Id,
                                        c2g__Account__c,
                                        c2g__Account__r.JVCO_Customer_Account__c
                                        FROM c2g__codaCashEntry__c 
                                        WHERE Id = : refundCashEntry.Id];
        
        if(!licAndCustAccIdMap.containsKey(cEntry.c2g__Account__c) && cEntry.c2g__Account__c != null && cEntry.c2g__Account__r.JVCO_Customer_Account__c != null){
            licAndCustAccIdMap.put(cEntry.c2g__Account__c, cEntry.c2g__Account__r.JVCO_Customer_Account__c);
        }
        
        if(!licAndCustAccIdMap.isEmpty() && !Test.isRunningTest()){
            System.enqueueJob(new JVCO_UpdateSalesInvoiceQueueable(licAndCustAccIdMap));
        }
    }
    
}