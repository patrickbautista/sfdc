/* ----------------------------------------------------------------------------------------------
    Name: JVCO_BankStatementLogicQueueable.cls 
    Description: Handler class for c2g__codaBankStatement__c object

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    25-Oct-2017  0.1         franz.g.a.dimaapi     Intial creation
    04-Oct-2018  0.2         franz.g.a.dimaapi     GREEN-33641 - Locking Row Issue
    26-Jun-2019  0.3         franz.g.a.dimaapi     GREEN-34731 - Refactor, Added Error Logs
	18-Nov-2020	 0.5		 patrick.t.bautista	   GREEN-36082 - Refactor to new queueable for invoice update
----------------------------------------------------------------------------------------------- */
public class JVCO_BankStatementLogicQueueable implements Queueable
{
    private static final JVCO_General_Settings__c GeneralSettings = JVCO_General_Settings__c.getInstance();
    private static final Decimal BANKSTATEMENT_RECORDS_PER_JOBS = GeneralSettings != null ? GeneralSettings.JVCO_Bank_Statement_Queue_Rec_For_Jobs__c : 85;
    private Map<Id, String> bslineToAcctRefMap;
    private Map<Id, Id> cashEntryToBSLine = new Map<Id, Id>();
    private List<c2g__codaCashEntry__c> updatedCashEntryList;
    private List<c2g__codaCashEntryLineItem__c> cashEntryLineList;
    private Id bankStatementId;

    public JVCO_BankStatementLogicQueueable(List<c2g__codaCashEntry__c> updatedCashEntryList, Map<Id, String> bslineToAcctRefMap, Id bankStatementId)
    {
        this.cashEntryLineList = new List<c2g__codaCashEntryLineItem__c>();
        this.updatedCashEntryList = updatedCashEntryList;
        this.bslineToAcctRefMap = bslineToAcctRefMap;
        this.bankStatementId = bankStatementId;
    }
    
    public JVCO_BankStatementLogicQueueable(List<c2g__codaCashEntry__c> updatedCashEntryList, Map<Id, String> bslineToAcctRefMap, Id bankStatementId, List<c2g__codaCashEntryLineItem__c> cashEntryLineList)
    {
        this.cashEntryLineList = cashEntryLineList;
        this.updatedCashEntryList = updatedCashEntryList;
        this.bslineToAcctRefMap = bslineToAcctRefMap;
        this.bankStatementId = bankStatementId;
    }
    
    public void execute(QueueableContext context) 
    {
        Integer counter = 0;
        //Store cash entry to update now
        List<c2g__codaCashEntry__c> cashEntryToInsertList = new List<c2g__codaCashEntry__c>();
        //Store cash entry to proceed next queueable run
        List<c2g__codaCashEntry__c> cashEntryToQueueList = new List<c2g__codaCashEntry__c>();
        
        for(c2g__codaCashEntry__c cashEntry : updatedCashEntryList){
            if(counter != BANKSTATEMENT_RECORDS_PER_JOBS){
                cashEntryToInsertList.add(cashEntry);
                counter++;
            }else{
                cashEntryToQueueList.add(cashEntry);
            }
        }
        insertCashEntry(cashEntryToInsertList);
        if(updatedCashEntryList.size() > BANKSTATEMENT_RECORDS_PER_JOBS)
        {
            System.enqueueJob(new JVCO_BankStatementLogicQueueable(cashEntryToQueueList, bslineToAcctRefMap, bankStatementId, cashEntryLineList));
        }else
        {
            c2g__codaBankStatement__c bankStatement = new c2g__codaBankStatement__c();
            bankStatement.id = bankStatementId;
            bankStatement.JVCO_Bank_Statement_Queueable_Completed__c = true;
            update bankStatement;
        }
    }
    
    private void insertCashEntry(List<c2g__codaCashEntry__c> cashEntryList)
    {        
        try
        {
            insert cashEntryList;
            createCashEntryLineFromBankStatementLine(cashEntryList);
        }catch(Exception e)
        {
            insert createErrorLog('BSQ-001', e.getMessage(), 'Cash Entries Generation');
        }
    }

    private void createCashEntryLineFromBankStatementLine(List<c2g__codaCashEntry__c> cashEntryList)
    {
        List<c2g__codaCashEntryLineItem__c> cashEntryLineInsertList = new List<c2g__codaCashEntryLineItem__c>();
        for (c2g__codaCashEntry__c cashEntry : cashEntryList)
        {
            //Create Cash Entry Line
            c2g__codaCashEntryLineItem__c cashEntryLine = new c2g__codaCashEntryLineItem__c();
            cashEntryLine.c2g__Account__c = cashEntry.c2g__Account__c;
            cashEntryLine.c2g__AccountReference__c = bslineToAcctRefMap.get(cashEntry.JVCO_Bank_Statement_Line_Item__c);
            cashEntryLine.c2g__CashEntryValue__c = cashEntry.c2g__Value__c;
            cashEntryLine.c2g__LineNumber__c = 1;
            cashEntryLine.c2g__AccountPaymentMethod__c = 'Electronic';
            cashEntryLine.c2g__CashEntry__c = cashEntry.Id;
            cashEntryLine.c2g__LineDescription__c = '{Bank Transfer}';
            cashEntryLineInsertList.add(cashEntryLine);   
            cashEntryToBSLine.put(cashEntry.Id, cashEntry.JVCO_Bank_Statement_Line_Item__c);
            cashEntryLineList.add(cashEntryLine);
        }

        insert cashEntryLineInsertList;
        updateBankStatementLineItem(cashEntryLineInsertList);
    }
    
    private void updateBankStatementLineItem(List<c2g__codaCashEntryLineItem__c> cashEntryLineInsertList)
    {
        List<c2g__codaBankStatementLineItem__c> updatedBSLineItemList = new List<c2g__codaBankStatementLineItem__c>();
        for(c2g__codaCashEntryLineItem__c cli : cashEntryLineInsertList)
        {
            //Add CashEntry in BankStatementLineItem
            c2g__codaBankStatementLineItem__c bsLine = new c2g__codaBankStatementLineItem__c();
            bsLine.Id = cashEntryToBSLine.get(cli.c2g__CashEntry__c);
            bsLine.JVCO_Cash_Entry__c = cli.c2g__CashEntry__c;
            bsLine.JVCO_Cash_Entry_Line_Item__c = cli.Id;
            bsLine.JVCO_Cash_Entry_Processed__c = true;
            updatedBSLineItemList.add(bsLine);
        }

        update updatedBSLineItemList;
    }

    @TestVisible
    private ffps_custRem__Custom_Log__c createErrorLog(String errCode, String errMessage, String batchName)
    {
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
        errLog.ffps_custRem__Detail__c = errMessage;
        errLog.ffps_custRem__Message__c = batchName;
        errLog.ffps_custRem__Grouping__c = errCode;
        
        return errLog;
    }
}