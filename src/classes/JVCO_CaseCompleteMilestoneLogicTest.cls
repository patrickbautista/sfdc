/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CaseCompleteMilestoneLogicTest.cls 
   Description: Test Class for JVCO_CaseCompleteMilestoneLogic

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  18-Oct-2016   0.1         ryan.i.r.limlingan  Intial creation
  19-Oct-2016   0.2         ryan.i.r.limlingan  Added seeAllData parameter
  20-Oct-2016   0.3         ryan.i.r.limlingan  Added test function for Resolution Time SLA;
                                                Removed seeAllData
  24-Oct-2016   0.4         ryan.i.r.limlingan  Added creation of Custom Setting record;
                                                Added references to utility class
  08-Nov-2016   0.5         ryan.i.r.limlingan  Modified MilestoneCompleteResolutionTime_Test
  ----------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_CaseCompleteMilestoneLogicTest {

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Sets up the needed records for testing
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    18-Oct-2016 ryan.i.r.limlingan  Initial version of function
    20-Oct-2016 ryan.i.r.limlingan  Added Entitlement record creation
    24-Oct-2016 ryan.i.r.limlingan  Added Custom Setting record creation
  ----------------------------------------------------------------------------------------------- */
    @testSetup static void setUpTestData()
    {
        JVCO_SLA_Constants__c cs = JVCO_TestClassObjectBuilder.createSLAConstants();
        insert cs;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;

        SlaProcess ep = [SELECT Id FROM SlaProcess WHERE Name='standard support clone' order by CreatedDate DESC limit 1];

        Entitlement testEntitlement = new Entitlement(Name='Customer Service Support',
                                                      AccountId=acc2.Id, SlaProcessId=ep.Id);
        insert testEntitlement;
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Test for First Response SLA Milestone completion
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    18-Oct-2016 ryan.i.r.limlingan  Initial version of function
    24-Oct-2016 ryan.i.r.limlingan  Added calls to utility class
  ----------------------------------------------------------------------------------------------- */
    @isTest static void MilestoneCompleteFirstResponse_Test()
    {
        Test.startTest();
        // Create Case records for every Case Origin with First Response SLA
        List<Case> cases = new List<Case>{
                                (new Case(Subject='Test--Email', Origin='Email')),
                                (new Case(Subject='Test--Webform', Origin='Webform')),
                                (new Case(Subject='Test--RC', Origin='Request Callback'))
                            };
        insert cases;

        List<Case> casesToTestList = [SELECT Id, Status FROM Case WHERE Subject LIKE 'Test--%'];
        String criteria = JVCO_CaseUtils.slaConstants.JVCO_First_Milestone_Criteria__c;
        List<Id> caseIdList = new List<Id>();
        for (Case c : casesToTestList)
        {
            caseIdList.add(c.Id);
            c.Status = criteria; // Responded
        }
        //update casesToTestList;

        String milestoneName = JVCO_CaseUtils.slaConstants.JVCO_First_Milestone__c + '%';

        // Retrieve CaseMilestone records of newly created Case records
        List<CaseMilestone> cmToTest = [SELECT CaseId, Case.Status, CompletionDate
                                        FROM CaseMilestone
                                        WHERE CaseMilestone.MilestoneType.Name LIKE :milestoneName
                                        AND CaseId IN :caseIdList];

        for (CaseMilestone cm : cmToTest)
        {
            cm.Case.Status = 'Responded';
            //System.assert(cm.CompletionDate != null);
            System.assertEquals(criteria, cm.Case.Status);
        }
        
        Test.stopTest();
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Test for Resolution Time SLA Milestone completion
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    20-Oct-2016 ryan.i.r.limlingan  Initial version of function
    24-Oct-2016 ryan.i.r.limlingan  Added calls to utility class
    08-Nov-2016 ryan.i.r.limlingan  Added assertions for First Response milestones
  ----------------------------------------------------------------------------------------------- */
    @isTest static void MilestoneCompleteResolutionTime_Test()
    {
        Test.startTest();
        // Create Case records for every Case Origin with Resolution Time SLA
        List<Case> cases = new List<Case>{
                                (new Case(Subject='Test--Email', Origin='Email')),
                                (new Case(Subject='Test--Webform', Origin='Webform')),
                                (new Case(Subject='Test--RC', Origin='Request Callback')),
                                (new Case(Subject='Test--IC', Origin='Inbound Call')),
                                (new Case(Subject='Test--LC', Origin='Live Chat'))
                            };
        insert cases;

        List<Case> casesToTestList = [SELECT Id, Status FROM Case WHERE Subject LIKE 'Test--%'];
        String criteria = JVCO_CaseUtils.slaConstants.JVCO_Second_Milestone_Criteria__c;
        List<Id> caseIdList = new List<Id>();
        for (Case c : casesToTestList)
        {
            caseIdList.add(c.Id);
            c.Status = criteria; // Closed
        }
        //update casesToTestList;

        String milestoneName = JVCO_CaseUtils.slaConstants.JVCO_Second_Milestone__c + '%';

        // Retrieve CaseMilestone records of newly created Case records
        List<CaseMilestone> cmToTest = [SELECT CaseId, Case.Status, CompletionDate
                                        FROM CaseMilestone
                                        WHERE CaseMilestone.MilestoneType.Name LIKE :milestoneName
                                        AND CaseId IN :caseIdList];

        for (CaseMilestone cm : cmToTest)
        {
            cm.Case.Status = 'Closed';
            //System.assert(cm.CompletionDate != null);
            System.assertEquals(criteria, cm.Case.Status);
        }

        // Check also if First Response milestone for each Case record has been marked complete
        milestoneName = JVCO_CaseUtils.slaConstants.JVCO_First_Milestone__c + '%';
        List<String> firstResponseCaseOrigins =
            (JVCO_CaseUtils.slaConstants.JVCO_First_Milestone_Origin__c).split(',');
        cmToTest = [SELECT CaseId, Case.Status, CompletionDate FROM CaseMilestone
                    WHERE CaseMilestone.MilestoneType.Name LIKE :milestoneName
                    AND Case.Origin IN: firstResponseCaseOrigins];

        for (CaseMilestone cm : cmToTest)
        {
            System.assert(cm.CompletionDate != null);
            System.assertEquals(criteria, cm.Case.Status);
        }
        
        Test.stopTest();
    }
}