/* ----------------------------------------------------------------------------------------------------------
Name:            JVCO_WebsitePaymentscheduler.cls
Description:     websit payment schedular class
Test class:      JVCO_WebsitePaymentschedulerTest.cls

Date             Version     Author                            Summary of Changes
-----------      -------     -----------------                 -------------------------------------------
03-Nov-2017      0.1         Accenture-saket.mohan.jha         Initial version of the code
11-Jan-2018      0.2         patrick.t.bautista                Refactor class
02-Feb-2018      0.3         jasper.j.figueroa                 Added JVCO_PayonomyPaymentProcessingBatchLimit__c Batch Size
03-July-2018     0.4         jasper.j.figueroa                 Refactored the code and made it a batch class
10-July-2018     0.5         jasper.j.figueroa                 Refactored the code and reverted it to a schedulable class. 
                                                               Placed the previous code to the JVCO_WebsitePaymentBatch class
---------------------------------------------------------------------------------------------------------- */
public class JVCO_WebsitePaymentscheduler /*implements Schedulable*/
{
    /*public void execute(SchedulableContext sc)
    {
        //String sched = '0 0 12 1/1 * ? *';

        //String jobId = System.schedule('...Running Batch Job', sched, backgroundMatchingBatch);
        //System.debug('.....Running Batch Job ID : '+jobId);
        
        Database.executeBatch(new JVCO_WebsitePaymentBatch(),1);
    }*/
    
}