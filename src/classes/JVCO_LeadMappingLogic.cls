/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_LeadMappingLogic.cls 
   Description:     
   Test class:  JVCO_LeadMappingLogicTest.cls 

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
  23-AUG-2016       0.1         Accenture-joseph.g.barrameda      Initial draft
  28-OCT-2020       0.2         Accenture-joseph.g.barrameda      GREEN-35828 - Appended "Salutation" field with "AccountName" when JVCO_Sole_Trader__c is TRUE
  ---------------------------------------------------------------------------------------------------------- */
 

public class JVCO_LeadMappingLogic{
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used in mapping 
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    23-AUG-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/ 
    public static Account createAccount(Lead sourceLead, Id accountId){
        
        //Instantiate Sobject
        sObject acctObj= Schema.getGlobalDescribe().get('Account').newSObject() ;        
        //Map the fields         
        acctObj = JVCO_ObjectMappingUtils.mapLeadToTargetObject(sourceLead, 'Account',accountId); 
        //Set the default recordtypeId to Customer Account
        acctObj.put('RecordTypeId' ,Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId());
        
         //GREEN-35828
        if (sourceLead.JVCO_Sole_Trader__c == true){
             acctObj.put('Name', sourceLead.Salutation + ' ' + acctObj.get('Name'));
        }
        else {
            acctObj.put('Name', acctObj.get('Name'));
        }
        
        return (Account)acctObj;
         
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used in mapping 
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    23-AUG-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/ 
    public static Contact createContact(Lead sourceLead, Id accountId){
        
        //Instantiate Sobject
        sObject contactObj= Schema.getGlobalDescribe().get('Contact').newSObject() ;
        //Map the fields 
        contactObj = JVCO_ObjectMappingUtils.mapLeadToTargetObject(sourceLead, 'Contact', null); 
        
        Contact contactRecord = new Contact();     
        contactRecord = (Contact)contactObj;             
        return contactRecord; 
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used in mapping 
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    1-SEP-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/    
    public static JVCO_Venue__c createVenue(Lead sourceLead, Id venueId){
        
        //Instantiate Sobject
        sObject venueObj= Schema.getGlobalDescribe().get('JVCO_Venue__c').newSObject() ;
        venueObj = JVCO_ObjectMappingUtils.mapLeadToTargetObject(sourceLead,'JVCO_Venue__c', venueId);
        
        JVCO_Venue__c venueRecord = new JVCO_Venue__c();
        venueRecord = (JVCO_Venue__c)venueObj;
        venueRecord.JVCO_Status__c ='Active';
        
        return venueRecord;
    }

    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method maps the fields from a Lead object to Affiliation object 
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    1-SEP-2016     Accenture-joseph.g.barrameda         Initial version of the code
    24-AUG-2016    Accenture-joseph.g.barrameda         Set default value of JVCO_Start_Date__c to Today GREEN-22105
	20-SEP-2017    Accenture-recuerdo.b.bregente        Added bypass validtion field for existing active affiliations
	23-FEB-2018    Acenture-gypt.d.minierva             Set value for Start Date
    ----------------------------------------------------------------------------------------------------------*/
    public static JVCO_Affiliation__c createAffiliation (Lead sourceLead, Id accountId, Id VenueId){
        
        //Check if there is already an Affiliation record
        List<JVCO_Affiliation__c> aff = [SELECT ID from JVCO_Affiliation__c WHERE JVCO_Account__c=: accountId AND JVCO_Venue__c=: venueId];
       
        if (aff.size() == 0)
        {
            //Create Affiliation
            JVCO_Affiliation__c newAff = new JVCO_Affiliation__c();
            newAff.JVCO_Account__c = accountId;
            newAff.JVCO_Venue__c = venueId; 
            newAff.JVCO_Status__c = 'Trading';
            newAff.JVCO_Revenue_Type__c = sourceLead.leadSource == 'Churn' ? 'Churn':'New Business';
            // START GREEN-23190
            // Update affiliation start date to align with the lead - gypt.d.minierva
            if(sourceLead.JVCO_Start_Date__c != null){
                    
                newAff.JVCO_Start_Date__c = sourceLead.JVCO_Start_Date__c;
                    
            } else {
                    
				newAff.JVCO_Start_Date__c = date.today();
                    
            }
            // END GREEN-23190
			
			JVCO_AffiliationTriggerHandler.runAffiliationExistValidation = false;      // GREEN-21488 - recuerdo.b.bregente
            
            System.debug('**** Affiliation Record:' + newAff);
            insert newAff;            
            return newAff;
        }
        return null; 
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method maps the fields from Lead to Account which has a "Licence Account" of recordtype. 
    Inputs:         Account Name , Contact Id
    Returns:        Account
    <Date>          <Authors Name>                      <Brief Description of Change> 
    1-SEP-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public static Account createLicenceAccount(String accountName , Id ContactId, Id leadId){
                
        //Retrieve all the Lead Fields
        String leadQuery='SELECT ' + JVCO_ObjectMappingUtils.getAllFieldNames('Lead') + ' FROM Lead WHERE Id=\'' + leadId + '\'';
        sObject sObjSource = Database.query(leadQuery);        
        //Instantiate sObject
        sObject sObj = Schema.getGlobalDescribe().get('Account').newSObject() ;        
        //Query Custom Metadata
        List<JVCO_Lead_Field_Mapping__mdt> leadFieldMappingList = [SELECT JVCO_Source_Field__c, JVCO_Target_Field__c, JVCO_Target_Object__C FROM JVCO_Lead_Field_Mapping__mdt 
                                                                    WHERE JVCO_Target_Object__c ='Account' AND JVCO_RecordType__c ='Licence Account'];
                
        for(JVCO_Lead_Field_Mapping__mdt lfmap:leadFieldMappingList){
            if(lfmap.JVCO_Source_Field__c == 'Contact'){
                sObj.put(lfmap.JVCO_Target_Field__c, contactId);
            }
            else{
                sObj.put(lfMap.JVCO_Target_Field__c , sobjSource.get(lfmap.JVCO_Source_Field__c));
            }        
        } 
                
        sObj.put('Name', accountName);
        sObj.put('RecordTypeId', Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId());
        return (Account)sobj;
    }    
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         raus.k.b.ablaza
    Company:        Accenture
    Description:    This method creates quote 
    Inputs:         Account Name , Contact Id, Opportunity Id
    Returns:        Quote
    <Date>          <Authors Name>                      <Brief Description of Change> 
    12-DEC-2016     Accenture-raus.k.b.ablaza         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public static SBQQ__Quote__c createQuote(Id accountId , Id contactId, Id opportunityId){
        SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
        
        quoteRec.SBQQ__Account__c = accountId;
        quoteRec.SBQQ__Opportunity2__c = opportunityId;
        quoteRec.SBQQ__PrimaryContact__c = contactId;
        quoteRec.SBQQ__QuoteProcessId__c = '';
        quoteRec.SBQQ__Status__c = 'Draft';
        quoteRec.SBQQ__Type__c = 'Quote';
        quoteRec.SBQQ__Primary__c = true;
        quoteRec.SBQQ__LineItemsGrouped__c = true;
        quoteRec.SBQQ__LineItemsPrinted__c = true;
        
        return quoteRec;
    }    
   
}