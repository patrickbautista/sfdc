@isTest
private class JVCO_PayonomyPaymentProcessingBatchTest
{
    /*@testSetup static void createTestData() 
    {
        JVCO_TestClassHelper.createBillingConfig();
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_TestClassHelper.setGeneralSettingsCS();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
		SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        Test.startTest();
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;

        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Processed_By_BillNow__c = true;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        insert bInv;
        
        Id paybankId = JVCO_TestClassHelper.getPayBank();
        Id ddMandateId = JVCO_TestClassHelper.getDDMandate(bInv.blng__Account__c);
        Id paySchedId = JVCO_TestClassHelper.getPaySched(payBankId);
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        Test.stopTest();
        insert JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);  
        
        List<c2g__codaInvoice__c> selectedInvoices = [SELECT Id, Name,JVCO_Payment_Method__c, 
                                                      JVCO_Outstanding_Amount_to_Process__c, 
                                                      c2g__OutstandingValue__c 
                                                      FROM c2g__codaInvoice__c];
        
        JVCO_PaymentSharedLogic paymentLogic = new JVCO_PaymentSharedLogic(selectedInvoices, JVCO_PaymentSharedLogic.PaymentMode.Debit);
        JVCO_Invoice_Group__c invGroup = paymentLogic.createInvoiceGroup('Direct Debit', TRUE);
        
        PAYREC2__Payment_Agreement__c agreement = new PAYREC2__Payment_Agreement__c();
        agreement.PAYREC2__Account__c = bInv.blng__Account__c;
        agreement.PAYREC2__Ongoing_Collection_Amount__c = invGroup.JVCO_Total_Amount__c/4;
        agreement.PAYREC2__Description__c = invGroup.Name;
        agreement.PAYFISH3__Current_Bank_Account__c = payBankId;
        agreement.PAYFISH3__Payee_Agreed__c = true;
        agreement.PAYREC2__Payment_Schedule__c = paySchedId;
        agreement.PAYREC2__Status__c = 'New Instruction';
        agreement.RecordTypeId = Schema.SObjectType.PAYREC2__Payment_Agreement__c.getRecordTypeInfosByName().get('Direct Debit').getRecordTypeId();
        insert agreement;
    }

    @isTest
    static void testDirectDebit()
    {
        PAYBASE2__Payment__c payment = JVCO_TestClassHelper.getPayonomyPayment('DirectDebit');
        payment.PAYREC2__Payment_Agreement__c = [SELECT Id FROM PAYREC2__Payment_Agreement__c LIMIT 1].Id;
        payment.PAYCP2__Payment_Description__c = [SELECT Name FROM JVCO_Invoice_Group__c LIMIT 1].Name;
        payment.PAYBASE2__Status__c = 'Submitted';
        payment.JVCO_All_Sales_Invoice__c = 'SIN123456';
        insert payment;
        List<PAYBASE2__Payment__c> paymentList = new List<PAYBASE2__Payment__c>();
        paymentList.add(payment);
        
        Test.startTest();
        JVCO_PayonomyPaymentProcessingBatch paymentBatch = new JVCO_PayonomyPaymentProcessingBatch('Direct Debit Collection');
        Database.QueryLocator ql = paymentBatch.start(null);
        paymentBatch.execute(null, paymentList);
        paymentBatch.finish(null);
        Test.stopTest();
    }

    @isTest
    static void testSagePay()
    {
        PAYBASE2__Payment__c payment = JVCO_TestClassHelper.getPayonomyPayment('SagePay');
        payment.PAYREC2__Payment_Agreement__c = [SELECT Id FROM PAYREC2__Payment_Agreement__c LIMIT 1].Id;
        JVCO_Invoice_Group__c invGroup = [SELECT Id, Name, CC_SINs__c FROM JVCO_Invoice_Group__c LIMIT 1];
        invGroup.CC_SINs__c = [SELECT Name FROM c2g__codaInvoice__c LIMIT 1].Name + ',';
        update invGroup;
        JVCO_CC_Outstanding_Mapping__c ccOuts = new JVCO_CC_Outstanding_Mapping__c();
        ccOuts.JVCO_CC_Invoice_Group__c =  invGroup.id;
        ccOuts.JVCO_CC_Outstanding_Value__c = [SELECT c2g__OutstandingValue__c FROM c2g__codaInvoice__c LIMIT 1].c2g__OutstandingValue__c;
        ccOuts.JVCO_Sales_Invoice_Name__c =[SELECT Name FROM c2g__codaInvoice__c LIMIT 1].Name;
        insert ccOuts;
        payment.PAYCP2__Payment_Description__c = invGroup.Name;
        insert payment;
        List<PAYBASE2__Payment__c> paymentList = new List<PAYBASE2__Payment__c>();
        paymentList.add(payment);

        Test.startTest();
        JVCO_PayonomyPaymentProcessingBatch paymentBatch = new JVCO_PayonomyPaymentProcessingBatch('SagePay');
        Database.QueryLocator ql = paymentBatch.start(null);
        paymentBatch.execute(null, paymentList);
        paymentBatch.finish(null);
        Test.stopTest();
    }

    @isTest
    static void testFailCreateCashEntry()
    {
        PAYBASE2__Payment__c payment = JVCO_TestClassHelper.getPayonomyPayment('DirectDebit');
        payment.PAYREC2__Payment_Agreement__c = [SELECT Id FROM PAYREC2__Payment_Agreement__c LIMIT 1].Id;
        payment.PAYCP2__Payment_Description__c = [SELECT Name FROM JVCO_Invoice_Group__c LIMIT 1].Name;
        payment.JVCO_All_Sales_Invoice__c = 'SIN123456';
        insert payment;
        List<PAYBASE2__Payment__c> paymentList = new List<PAYBASE2__Payment__c>();
        paymentList.add(payment);
        c2g__codaCompany__c company = [SELECT Id, c2g__BankAccount__c FROM c2g__codaCompany__c LIMIT 1];
        company.c2g__BankAccount__c = null;
        update company;

        Test.startTest();
        JVCO_PayonomyPaymentProcessingBatch paymentBatch = new JVCO_PayonomyPaymentProcessingBatch('Direct Debit Collection');
        Database.QueryLocator ql = paymentBatch.start(null);
        paymentBatch.execute(null, paymentList);
        paymentBatch.finish(null);
        Test.stopTest();
    }

    @isTest
    static void testEmptyConstructor()
    {
        JVCO_PayonomyPaymentProcessingBatch paymentBatch = new JVCO_PayonomyPaymentProcessingBatch();
    }

    @isTest
    static void testDifferentRecordType()
    {
        JVCO_PayonomyPaymentProcessingBatch paymentBatch = new JVCO_PayonomyPaymentProcessingBatch('Hotdog');
        Database.QueryLocator ql = paymentBatch.start(null);
    }
    
    @isTest
    static void testPayonomyPaymentSchedDDMomentarily()
    {
        Test.startTest();
        JVCO_PayonomyPaymentDirectDebitScheduler.callExecuteMomentarily();
        Test.stopTest();
    }
    
    @isTest
    static void testPayonomyPaymentSchedCCMomentarily()
    {
        Test.startTest();
        JVCO_PayonomyPaymentCreditCardScheduler.callExecuteMomentarily();
        Test.stopTest();
    }
    
    @isTest
    static void testPayonomyPaymentSchedDD()
    {
        Test.startTest();
        
        JVCO_PayonomyPaymentDirectDebitScheduler ddScheduler = new JVCO_PayonomyPaymentDirectDebitScheduler();
        String sch = '5 0 0 * * ?'; 
        system.schedule('Test DD Scheduler', sch, ddScheduler);
        Test.stopTest();
    }
    
    @isTest
    static void testPayonomyPaymentSchedCC()
    {
        Test.startTest();
        JVCO_PayonomyPaymentCreditCardScheduler ccScheduler = new JVCO_PayonomyPaymentCreditCardScheduler();
        String sch = '5 0 0 * * ?'; 
        system.schedule('Test CC Scheduler', sch, ccScheduler);
        Test.stopTest();
    }

    @isTest
    static void testPayonomyFlagBatch()
    {
        PAYBASE2__Payment__c payment = JVCO_TestClassHelper.getPayonomyPayment('DirectDebit');
        PAYREC2__Payment_Agreement__c paymentagreement = [SELECT Id, PAYREC2__Account__c FROM PAYREC2__Payment_Agreement__c LIMIT 1];
        payment.PAYREC2__Payment_Agreement__c = paymentagreement.Id;
        payment.PAYCP2__Payment_Description__c = [SELECT Name FROM JVCO_Invoice_Group__c LIMIT 1].Name;
        payment.PAYBASE2__Status__c = 'Submitted';
        payment.PAYBASE2__Status_Text__c = 'Cancelled';
        payment.PAYBASE2__Created_From_Account__c = paymentagreement.PAYREC2__Account__c;
        payment.JVCO_All_Sales_Invoice__c = 'SIN123456';
        insert payment;

        Account a = new Account();
        a.Id = paymentagreement.PAYREC2__Account__c;
        a.JVCO_Send_Direct_Debit_Reminder__c = true;
        update a;
        
        List<PAYREC2__Payment_Agreement__c> paymentAgreementList = [SELECT Id FROM PAYREC2__Payment_Agreement__c];
        
        Test.startTest();
        JVCO_Payment_Flag_Batch paymentFlagBatch = new JVCO_Payment_Flag_Batch();
        Database.QueryLocator ql = paymentFlagBatch.start(null);
        paymentFlagBatch.execute(null, paymentAgreementList);
        paymentFlagBatch.finish(null);
        Test.stopTest();
    }

    @isTest
    static void testPayonomyFlagSched()
    {
        Test.startTest();
        JVCO_Payment_Flag_Sched pls = new JVCO_Payment_Flag_Sched();
        String s = '5 0 0 * * ?'; 
        system.schedule('Test CC Scheduler', s, pls);
        Test.stopTest();
    }

    @isTest
    static void testPayonomyFlagSchedMomentarily()
    {
        Test.startTest();
        JVCO_Payment_Flag_Sched.callExecuteMomentarily();
        Test.stopTest();
    }
    
    @isTest
    static void testBackgroundMatchingScheduler()
    {
        JVCO_PaymentCollectionScheduler.callExecuteMomentarily('hotdog');
    }

    @isTest
    static void testDeleteAgreement()
    {
        delete [SELECT Id FROM PAYREC2__Payment_Agreement__c];
    }*/
    
}