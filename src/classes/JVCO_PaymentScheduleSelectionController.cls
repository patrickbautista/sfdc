/* ----------------------------------------------------------------------------------------------
   Name: JVCO_PaymentScheduleSelectionController.cls 
   Description: Business logic class selecting invoices to be paid using Debit Card

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  19-Dec-2016   0.1         ryan.i.r.limlingan  Intial creation
  09-Jan-2017   0.2         ryan.i.r.limlingan  Finalized calls to JVCO_PaymentSharedLogic
  07-Mar-2017   0.3         ryan.i.r.limlingan  Added Payment Method when creating an Invoice Group
  17-Apr-2017   0.4         ryan.i.r.limlingan  Changed data type of variables used in computing for
                                                collection amounts
  29-Nov-2018   0.5         patrick.t.bautista  GREEN-34038 - Refactor class and add the Error Message
  ----------------------------------------------------------------------------------------------- */
public with sharing class JVCO_PaymentScheduleSelectionController
{
    /*private ApexPages.StandardSetController stdCtrl;
    private List<c2g__codaInvoice__c> selectedInvoices;
    public List<c2g__codaInvoice__c> retrievedInvoices {get; set;}
    public Decimal totalAmount {get; set;}
    public Integer numOfSelectedInv {get; set;} // Used to control what is rendered
    public String selectedPaymentSched {get; set;}
    private Map<String,Integer> scheduleInstalmentsMap;
    public Boolean isPaperDebit {get; set;}
    public Decimal firstCollectionAmt {get; set;}
    public Decimal ongoingCollectionAmt {get; set;}
    public Decimal finalCollectionAmt {get;set;}
    public Account licenceAccount;
    List<SelectOption> paymentSchedOptions;
    private Id accId;
    private JVCO_PaymentSharedLogic paymentLogic;
    public Integer numberOfInstalments {get; set;}

    // Constants used for page messages
    public final String NO_RECORDS_ERR = System.Label.JVCO_PaymentNoRecords;
    public final String INVALID_RECORDS_ERR = System.Label.JVCO_PaymentInvalidRecords;
    public final String INVALID_PROJECTED_AMOUNT = System.Label.JVCO_PaymentInvalidProjectedAmount;
    public final String INVALID_DD_ERROR = System.Label.JVCO_DirectDebitInfringementErrorMess;
    /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Class constructor
    Inputs: StandardSetController
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    19-Dec-2016 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public JVCO_PaymentScheduleSelectionController(ApexPages.StandardSetController ssc)
    {
        stdCtrl = ssc;
        isPaperDebit = false;
        // Retrieve selected Sales Invoice records from related list
        selectedInvoices = (List<c2g__codaInvoice__c>)ssc.getSelected();
        numOfSelectedInv = selectedInvoices.size();
        
        // Payment Schedule
        paymentSchedOptions = new List<SelectOption>();
        paymentSchedOptions.add(new SelectOption('Default', '--Select Instalment Plan--'));
        
        // Initialize Payment Logic
        paymentLogic = new JVCO_PaymentSharedLogic(selectedInvoices,
            JVCO_PaymentSharedLogic.PaymentMode.Debit);
    }

    /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Returns Payment Schedule records as options to an apex:selectList
    Inputs: N/A
    Returns: List<SelectOption>
    <Date>      <Authors Name>      <Brief Description of Change> 
    27-Dec-2016 ryan.i.r.limlingan  Initial version of function
    05-Apr-2018 patrick.t.bautista  Error Message for GREEN-31226 Direct Debit
    ----------------------------------------------------------------------------------------------- */
    /*public PageReference init()
    {
        //Getting Permission set of the user and see if it's has Enforcement Advisors
        String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
        Map<Id,PermissionSet> enAdPermSet = new Map<Id,PermissionSet>([SELECT Id FROM PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors']);
        List<PermissionSetAssignment> permissionSetList = [SELECT Id FROM PermissionSetAssignment WHERE 
                                                           AssigneeId = :UserInfo.getUserId() AND PermissionSetId IN :enAdPermSet.keySet()];
        if (numOfSelectedInv > 0)
        {
            totalAmount = 0.0;
            retrievedInvoices = [SELECT Id, Name, c2g__InvoiceStatus__c, c2g__OutstandingValue__c,
                                 c2g__Account__r.JVCO_Credit_Status__c, c2g__PaymentStatus__c, 
                                 JVCO_Invoice_Group__c, c2g__Account__c, JVCO_Payment_Method__c,
                                 c2g__Account__r.JVCO_In_Infringement__c, c2g__Account__r.JVCO_In_Enforcement__c,
                                 JVCO_Invoice_Group__r.JVCO_Payment_Agreement__c, c2g__DueDate__c ,
                                 c2g__Account__r.JVCO_DD_Payee_Contact__c,
                                 c2g__Account__r.JVCO_Number_of_Active_DD_Mandate__c
                                 FROM c2g__codaInvoice__c WHERE Id IN :selectedInvoices
                                 ORDER BY Id ASC];
            //Stores Licence Account to be used in payment shared logic
            licenceAccount = [SELECT id, Name FROM Account WHERE id =: retrievedInvoices[0].c2g__Account__c];
            
            for (c2g__codaInvoice__c invoice : retrievedInvoices)
            {
                // Compute totalAmount of selected invoice records
                totalAmount += invoice.c2g__OutstandingValue__c.setScale(2);
                // Check if there's a DD Payee Contact
                if(invoice.c2g__Account__r.JVCO_DD_Payee_Contact__c == null)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The Licence Account does not have a DD Payee Contact'));
                    numOfSelectedInv = 0;
                    return null;
                }
                //Check if Sales Invoice is already has an existing PPA
                else if(invoice.JVCO_Payment_Method__c == 'Direct Debit' && invoice.JVCO_Invoice_Group__c != NULL
                   && invoice.JVCO_Invoice_Group__r.JVCO_Payment_Agreement__c != NULL)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, INVALID_RECORDS_ERR));
                    numOfSelectedInv = 0;
                    return null;
                }
                //check if the Licence Account in infringement or Enforcement
                else if((invoice.c2g__Account__r.JVCO_In_Infringement__c == TRUE || invoice.c2g__Account__r.JVCO_In_Enforcement__c == TRUE) 
                        && (invoice.c2g__Account__r.JVCO_Credit_Status__c == 'Enforcement - Debt Recovery' ||
                            invoice.c2g__Account__r.JVCO_Credit_Status__c == 'Enforcement - Infringement') && 
                        profileName <> 'System Administrator' && permissionSetList.isEmpty())
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, INVALID_DD_ERROR));
                    numOfSelectedInv = 0;
                    return null;
                }
                // Check if Sales Invoice is Not Paid and must be Complete
                else if(!invoice.c2g__InvoiceStatus__c.equals('Complete') || invoice.c2g__PaymentStatus__c.equals('Paid'))
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, INVALID_RECORDS_ERR));
                    numOfSelectedInv = 0;
                    return null;
                }
                // Check the Licence Account if there is a valid DD Mandate
                else if(invoice.c2g__Account__r.JVCO_Number_of_Active_DD_Mandate__c > 0)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please deactivate existing DD Mandate if you\'re planning to create a New Installment'));
                    numOfSelectedInv = 0;
                    return null;
                }
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, NO_RECORDS_ERR));
        }
        return null;
    }
    /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Returns Payment Schedule records as options to an apex:selectList
    Inputs: N/A
    Returns: List<SelectOption>
    <Date>      <Authors Name>      <Brief Description of Change> 
    27-Dec-2016 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public List<SelectOption> getPaymentSchedOptions()
    {
        // Only Default value is added
        if (paymentSchedOptions.size() == 1)
        {
            // Retrieves all Payment Schedule Records
            List<PAYREC2__Payment_Schedule__c> paymentScheds = [SELECT Id, Name, PAYREC2__Max__c
                                                                FROM PAYREC2__Payment_Schedule__c
                                                                ORDER BY PAYREC2__Max__c, PAYREC2__Day__c];
            scheduleInstalmentsMap = new Map<String,Integer>();
            for (PAYREC2__Payment_Schedule__c p : paymentScheds)
            {
                paymentSchedOptions.add(new SelectOption(p.Name, p.Name));
                scheduleInstalmentsMap.put(p.Name, Integer.valueOf(p.PAYREC2__Max__c));
            }
            System.debug(scheduleInstalmentsMap);
        }
        return paymentSchedOptions;
    }

    /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Resets variables to initial state
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    31-Jan-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public void resetVariables()
    {
        firstCollectionAmt = null;
        ongoingCollectionAmt = finalCollectionAmt = null;
        ApexPages.getMessages().clear();
        numberOfInstalments = scheduleInstalmentsMap.get(selectedPaymentSched);
    }

    /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Populates Collection Amount section depending on selected Payment Schedule
    Inputs: N/A
    Returns: PageReference
    <Date>      <Authors Name>      <Brief Description of Change> 
    27-Dec-2016 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public void reflectInstalmentPlan()
    {
        // Reset variables every time Instalment Plan is changed
        resetVariables();
        // Removes Default value upon selection of an Instalment Plan
        if (paymentSchedOptions.get(0).getValue().equals('Default'))
        {
            paymentSchedOptions.remove(0);
        }
        // Compute for collection amounts
       if (numberOfInstalments == 1)
        {
            ongoingCollectionAmt = totalAmount;   
        } else {
            ongoingCollectionAmt = (totalAmount / numberOfInstalments);
            decimal RoundOngoingColAmt = ongoingCollectionAmt.setScale(2);
            ongoingCollectionAmt = RoundOngoingColAmt;
            if (numberOfInstalments > 2) firstCollectionAmt = ongoingCollectionAmt.setScale(2);
            finalCollectionAmt = totalAmount - (ongoingCollectionAmt * (numberOfInstalments - 1));
            decimal RoundFinalColAmt = finalCollectionAmt.setScale(2);
            finalCollectionAmt = RoundFinalColAmt;
        }
    }

    /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Redirects to the Payonomy page but validates passed parameters first
    Inputs: N/A
    Returns: PageReference
    <Date>      <Authors Name>      <Brief Description of Change> 
    19-Dec-2016 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public PageReference proceedWithValidation()
    {
        if (!isProjectedCollectedAmtValid())
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                INVALID_PROJECTED_AMOUNT));
        } else {
            return proceed();
        }
        return null;
    }

    /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Redirects to the Payonomy page with passed parameters
    Inputs: N/A
    Returns: PageReference
    <Date>      <Authors Name>      <Brief Description of Change> 
    10-Jan-2017 ryan.i.r.limlingan  Initial version of function
    07-Mar-2017 ryan.i.r.limlingan  Added Direct Debit string to the passed parameter for
                                    createInvoiceGroup
    ----------------------------------------------------------------------------------------------- */
    /*public PageReference proceed()
    {
        JVCO_Invoice_Group__c ig = paymentLogic.createInvoiceGroup('Direct Debit', isPaperDebit);
        // Set variables needed for Account update
        paymentLogic.paymentSched = selectedPaymentSched;
        paymentLogic.firstColAmt = firstCollectionAmt;
        paymentLogic.ongoingColAmt = ongoingCollectionAmt; 
        paymentLogic.finalColAmt = finalCollectionAmt;
        paymentLogic.updateLicenceAccount(ig, licenceAccount);
        return paymentLogic.redirectToPayonomy(licenceAccount);
    }

    /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Computes for the projected amount to be collected, and determines if valid or not
    Inputs: N/A
    Returns: Boolean
    <Date>      <Authors Name>      <Brief Description of Change> 
    28-Dec-2016 ryan.i.r.limlingan  Initial version of function
    06-Jan-2017 ryan.i.r.limlingan  Optimized computation of projected collected amount
    ----------------------------------------------------------------------------------------------- */
    /*public Boolean isProjectedCollectedAmtValid() {
        Double projectedCollectedAmt = (numberOfInstalments > 2) ? firstCollectionAmt
            + finalCollectionAmt + ((numberOfInstalments - 2) * ongoingCollectionAmt)
            : ongoingCollectionAmt + finalCollectionAmt;
        return (projectedCollectedAmt != totalAmount) ? FALSE : TRUE;
    }
    
    /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Returns to Account detail page
    Inputs: N/A
    Returns: PageReference
    <Date>      <Authors Name>      <Brief Description of Change> 
    09-Jan-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public PageReference returnToAccount()
    {
        PageReference page = new PageReference(stdCtrl.cancel().getUrl());
        page.setRedirect(TRUE);
        return page;
    }*/
}