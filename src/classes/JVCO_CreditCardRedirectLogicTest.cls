@isTest
private class JVCO_CreditCardRedirectLogicTest
{
    /*@testSetup static void setupTestData(){
        
        JVCO_TestClassHelper.createBillingConfig();

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
    
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        insert nonConvGLA;
        insert JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        PAYACCVAL1__Bank_Account__c testBankDebit = new PAYACCVAL1__Bank_Account__c();
        testBankDebit.PAYACCVAL1__Account_Number__c = '12345678';
        testBankDebit.PAYACCVAL1__Sort_Code__c = '000001';
        testBankDebit.PAYFISH3__Account_Name__c = 'Test Debit';
        insert testBankDebit;

        id testPaySched = JVCO_TestClassHelper.getPaySched(testBankDebit.id);

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim2('PRS');
        insert JVCO_TestClassHelper.setDim2('VPL');
        insert JVCO_TestClassHelper.setDim3();
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        Test.startTest();
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o;
        
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        
        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        insert bInv;
        
        insert JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);
        List<blng__Invoice__c> bInvList = [SELECT id FROM blng__Invoice__c WHERE id =: bInv.id];
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_InvoiceGenerationLogic.generateInvoiceFromSingleOrder(bInvList);
        Test.stopTest();
    }
    @isTest
    static void creditCardRedirectTest()
    {
        test.startTest();
        Account licAcc = [select id, Name,JVCO_CC_Amount__c, JVCO_Invoice_Group__c,JVCO_Invoice_Group_Id__c from Account where RecordType.Name = 'Licence Account' limit 1];
        PageReference testPageRef = ApexPages.currentPage();
        list<c2g__codaInvoice__c> selectedInvoices = [select id, name from c2g__codaInvoice__c];
        Test.setCurrentPage(testPageRef);
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(selectedInvoices);
        sc.setSelected(selectedInvoices);
        JVCO_CreditCardRedirectLogic ccLogic = new JVCO_CreditCardRedirectLogic(sc);
        try{
            ccLogic.init();
        }catch(exception e){
        }
        test.stopTest();
    }
    @isTest
    static void notsysaddTest()
    {
        test.startTest();
        Account licAcc = [select id, Name,JVCO_CC_Amount__c, JVCO_Invoice_Group__c,JVCO_Invoice_Group_Id__c from Account where RecordType.Name = 'Licence Account' limit 1];
        licAcc.JVCO_In_Enforcement__c = true;
        licAcc.JVCO_Credit_Status__c = 'Enforcement - Infringement';
        update licAcc;
        PageReference testPageRef = ApexPages.currentPage();
        list<c2g__codaInvoice__c> selectedInvoices = [select id, name from c2g__codaInvoice__c];
        Test.setCurrentPage(testPageRef);
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Finance'];
        User u = new User(Alias = 'standt1',Country='United Kingdom',Email='sample@gmail.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='test@gmail.sample.com', Division='External', Department='Accenture');
        insert u;
        
        System.runAs(u)
        {
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(selectedInvoices);
            sc.setSelected(selectedInvoices);
            JVCO_CreditCardRedirectLogic ccLogic = new JVCO_CreditCardRedirectLogic(sc);
            try{
                ccLogic.init();
            }catch(exception e){
            }
        }
        test.stopTest();
    }
    @isTest
    static void withValidPPAandDDMandateTest()
    {
        Account licAcc = [select id, Name,JVCO_CC_Amount__c, JVCO_Invoice_Group__c,JVCO_Invoice_Group_Id__c from Account where RecordType.Name = 'Licence Account' limit 1];
        c2g__codaInvoice__c invoice = [select id, JVCO_Payment_Method__c, JVCO_Invoice_Group__c, name from c2g__codaInvoice__c LIMIT 1];
        List<c2g__codaInvoice__c> selectedInvoices = new List<c2g__codaInvoice__c>();
        test.startTest();
        
        JVCO_Invoice_Group__c invGroup = new JVCO_Invoice_Group__c();
        invGroup.CC_SINs__c = invoice.Name;
        invGroup.JVCO_Total_Amount__c = 100;
        insert invGroup;
        
        invoice.JVCO_Payment_Method__c = 'Direct Debit';
        invoice.JVCO_Invoice_Group__c = invGroup.id;
        update invoice;
        selectedInvoices.add(invoice);
        
        PAYREC2__Payment_Agreement__c paymentAgreement = new PAYREC2__Payment_Agreement__c();
        paymentAgreement.PAYREC2__Account__c = licAcc.Id;
        paymentAgreement.PAYFISH3__Current_Bank_Account__c = [SELECT Id FROM PAYACCVAL1__Bank_Account__c LIMIT 1].id;
        paymentAgreement.PAYREC2__Ongoing_Collection_Amount__c = 100;
        paymentAgreement.PAYFISH3__Payee_Agreed__c = true;
        paymentAgreement.PAYREC2__Payment_Schedule__c = [SELECT Id, Name FROM PAYREC2__Payment_Schedule__c LIMIT 1].Id;
        paymentAgreement.JVCO_Invoice_Group__c = invGroup.id;
        paymentAgreement.PAYREC2__Status__c = 'New Instruction';
        insert paymentAgreement;
        
        invGroup.JVCO_Payment_Agreement__c = paymentAgreement.id;
        update invGroup;
        
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(selectedInvoices);
        sc.setSelected(selectedInvoices);
        JVCO_CreditCardRedirectLogic ccLogic = new JVCO_CreditCardRedirectLogic(sc);
        try{
            ccLogic.init();
        }catch(exception e){
        }
        test.stopTest();
    }
    @isTest
    static void returnToAccountTest()
    {
        test.startTest();
        Account licAcc = [select id, Name,JVCO_CC_Amount__c, JVCO_Invoice_Group__c,JVCO_Invoice_Group_Id__c from Account where RecordType.Name = 'Licence Account' limit 1];
        PageReference testPageRef = ApexPages.currentPage();
        list<c2g__codaInvoice__c> selectedInvoices = [select id, name from c2g__codaInvoice__c];
        Test.setCurrentPage(testPageRef);
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(selectedInvoices);
        sc.setSelected(selectedInvoices);
        JVCO_CreditCardRedirectLogic ccLogic = new JVCO_CreditCardRedirectLogic(sc);
        try{
            ccLogic.returnToAccount();
        }catch(exception e){
        }
        test.stopTest();
    }*/
}