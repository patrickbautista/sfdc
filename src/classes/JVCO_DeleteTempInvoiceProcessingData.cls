/* ----------------------------------------------------------------------------------------------
    Name: JVCO_DeleteTempInvoiceProcessingData
    Description: Batch for Linking Blng Line to Sales Line

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    07-Sep-2018  0.1         franz.g.a.dimaapi     Intial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_DeleteTempInvoiceProcessingData implements Database.Batchable<sObject>
{
	private Set<String> jobIdentifierSet = new Set<String>();

	public JVCO_DeleteTempInvoiceProcessingData(String jobIdentifier)
	{
		jobIdentifierSet.add(jobIdentifier);
	}

	public JVCO_DeleteTempInvoiceProcessingData(Set<String> jobIdentifierSet)
	{
		this.jobIdentifierSet = jobIdentifierSet;
	}

	public Database.QueryLocator start(Database.BatchableContext bc)
    {
    	if(jobIdentifierSet != null)
    	{
    		return Database.getQueryLocator([SELECT Id 
	    									FROM JVCO_TempInvoiceProcessingData__c 
											WHERE JVCO_Job_Identifier__c IN :jobIdentifierSet]);
    	}else
    	{
    		return null;
    	}
        
    }

    public void execute(Database.BatchableContext bc, List<JVCO_TempInvoiceProcessingData__c> scope)
    {   
        delete scope;
    }

    public void finish(Database.BatchableContext bc)
    {

    }
}