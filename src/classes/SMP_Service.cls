public with sharing class SMP_Service {
    public static List<c2g__codaInvoiceInstallmentLineItem__c> insertLineItems(List<c2g__codaInvoiceInstallmentLineItem__c> installmentLineItemList) {
        HttpRequest req = getHttpRequest(installmentLineItemList);
        Http http = new Http();
        
            HTTPResponse res = http.send(req);

            return formatResponse(res);
        
    }

    public static List<c2g__codaInvoiceInstallmentLineItem__c> formatResponse(HTTPResponse res){
        string responseBody = res.getBody().replace('{"InsertInstallmentLineItemListResult":', '[');
        responseBody = responseBody.right(responseBody.length() - 1);
        responseBody = responseBody.replace('"Id', '"attributes":{"type":"c2g__codaInvoiceInstallmentLineItem__c"},"Id');
        system.debug('response: ' + res);
        system.debug('responseBody: ' + responseBody);
        return (List<c2g__codaInvoiceInstallmentLineItem__c>)JSON.deserialize(responseBody, List<c2g__codaInvoiceInstallmentLineItem__c>.class);
    }

    public static HttpRequest getHttpRequest(List<c2g__codaInvoiceInstallmentLineItem__c> installmentLineItemList){
        SmarterPay_Direct_Debit_Settings__c ddSettings =  SmarterPay_Direct_Debit_Settings__c.getInstance();
        HttpRequest req = new HttpRequest();
        String authorizationHeader = 'Bearer ' + ddSettings.ServiceToken__c;
        system.debug('authorizationHeader: ' + authorizationHeader);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(ddSettings.ServiceEndpoint__c);
        req.setMethod('POST');
        req.setTimeout(120000);
        string jsonToSend = '{"InstallmentLineItemList": ' + JSON.serialize(installmentLineItemList) + '}';
        system.debug('json: ' + jsonToSend);
        req.setBody(jsonToSend);
        system.debug('request: ' + req);

        return req;
    }    
}