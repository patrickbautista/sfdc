/* ----------------------------------------------------------------------------------------------
    Name: JVCO_ContractBeforeDeleteErrorLogic.cls 
    Description: Added an error message VF page when deleting a Contract

    Date         Version     Author              	Summary of Changes 
    -----------  -------     -----------------   	-----------------------------------------
    16-Oct-2017   0.1         jasper.j.figueroa  	Intial creation - fix for GREEN-22873
    20-Oct-2017   0.2         jules.osberg.a.pablo  Added fix for GREEN-25066, allow system admin to delete contracts
	24-Feb-2021	  0.3		  luke.walker		 	GREEN-36345 - Page was failing in lightning so updated to remove error
  ----------------------------------------------------------------------------------------------- */
public class JVCO_ContractBeforeDeleteErrorLogic
{
    private ApexPages.StandardController stdCtrl {get; set;}

    // Constants used for page messages
    public String INVALID_INVOICE_ERR1 = 'You cannot delete a Contract';
    private String uiThemeDisplayed;
    @TestVisible private boolean lightningUI = false;
    private String returnURL;
    private final String contractURL = '/800';

  
    public JVCO_ContractBeforeDeleteErrorLogic(ApexPages.StandardController ssc)
    {
        system.debug('Cancel URL'+ApexPages.currentPage().getHeaders().get('cancelURL'));
        
        stdCtrl = ssc;
        uiThemeDisplayed = UserInfo.getUiThemeDisplayed();
        returnURL = stdCtrl.cancel().getUrl();
        if(uiThemeDisplayed == 'Theme4d' || uiThemeDisplayed == 'Theme4u'){
            lightningUI=true;
        }
              
    }

    public PageReference init()
    {
        Contract cont = (Contract) stdCtrl.getRecord();
        Profile p = [select id, name from profile where name = 'System Administrator' limit 1];
        if(p.id == UserInfo.getProfileId()) {
            delete cont;
            
			PageReference page = new PageReference(contractURL);
            
            //Checks for Lightning or Classic
            if(lightningUI){
                aura.redirect(page);
            } else {
                page.setRedirect(true);    
            }
            
            return page;
        }
        else {   
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, INVALID_INVOICE_ERR1));
            return null;
        }
    }

    //Returns to the Contract after clicking the 'Back' button
    public PageReference returnToContract()
    {
		PageReference page = new PageReference(returnURL);
        system.debug('retURL'+returnURL);   
        //Checks for Lightning or Classic
        if(lightningUI){
             aura.redirect(page);
        } else {
             page.setRedirect(true);    
        }
            
        return page;
    }
}