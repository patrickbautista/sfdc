/* ----------------------------------------------------------------------------------------------
   Name: JVCO_FixProRateValuesTest
   Description: Unit test JVCO_FixProRateValues

   Date         Version     Author               Summary of Changes 
   -----------  -------     -----------------    -----------------------------------------
  09-Fe-2018   0.1      Robert John B. Lacatan   Intial creation
  ----------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_FixProRateValuesTest {

    @testSetup 
    static void setup() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        User u = new User(Alias = 'utest', Email='unit.test@unit.test.com',
                          EmailEncodingKey='UTF-8', LastName='Unit Test', 
                          LanguageLocaleKey='en_US', LocaleSIdKey='en_GB', ProfileId = p.Id,
                          TimezoneSIdKey='Europe/London', Username='test.test@unit.test.com',
                          Division='Legal', Department='Legal');

        insert u;

        system.runAs(u){
            Account acc =  JVCO_TestClassObjectBuilder.createCustomerAccount();
            insert acc;

            Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
            acc2.JVCO_Renewal_Scenario__c = '';
            insert acc2;

            Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
            prod.proratable__c = false;
            insert prod;

            Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
            insert pb;

            PriceBookEntry pEntry = JVCO_TestClassObjectBuilder.createPriceBookEntry(prod.id);
            insert pEntry;

            Contract cont = JVCO_TestClassObjectBuilder.createContract(acc2.id, null);
            insert cont;
            
            SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(acc2.id, prod.id);
            s.SBQQ__ListPrice__c = 1;
            s.SBQQ__NetPrice__c = 0.33;
            s.SBQQ__ProrateMultiplier__c = 0.33;
            s.ChargeYear__c = 'Current Year';

            insert s;

        }

        JVCO_FixProRateValuesCustomSetting__c setting = new JVCO_FixProRateValuesCustomSetting__c(Name = 'FixProrate', Min__c = 'Min', MAx__c = 'Max',MRPPPL__c = 'MRPPPL', MRPVPL__c = 'MRPVPL', DataMigId__c= u.Id);
        insert setting;
        


    }

    @isTest 
    static void testUpdateSubs() {        
        Test.startTest();
        JVCO_FixProRateValues fixProrate = new JVCO_FixProRateValues ();
        Id batchId = Database.executeBatch(fixProrate,1);
        Test.stopTest();

        SBQQ__Subscription__c checkSubs = [SELECT id, JVCO_NetTotal__c,SBQQ__NetPrice__c,SBQQ__ListPrice__c,SBQQ__ProrateMultiplier__c,SBQQ__Quantity__c,JVCO_NOT_Renewable__c,ChargeYear__c ,SBQQ__RenewalQuantity__c FROM SBQQ__Subscription__c limit 1];


        // after the testing stops, assert records were updated properly
        System.assertEquals(checkSubs.SBQQ__ListPrice__c,checkSubs.SBQQ__NetPrice__c);
        System.assertEquals(checkSubs.SBQQ__ProrateMultiplier__c,1);
    }

}