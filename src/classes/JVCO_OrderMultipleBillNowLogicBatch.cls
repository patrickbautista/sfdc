/* ----------------------------------------------------------------------------------------------
    Name: JVCO_OrderMultipleBillNowLogicBatch
    Description: Batch for SalesInvoice And CreditNote Generation Logic for Multiple Orders

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    11-Apr-2018  0.1         franz.g.a.dimaapi     Intial creation
    25-Nov-2020  1.0         patrick.t.bautista    GREEN-36039 - added criteria Re-issued
----------------------------------------------------------------------------------------------- */
public class JVCO_OrderMultipleBillNowLogicBatch implements Database.Batchable<sObject>, Database.Stateful
{
    private Set<Id> orderGroupIdSet;
    //Invoice Scheduler Invoice Date
    private Date invoiceDate;

    //Map of Blng Invoice Id to SalesInvoice/CreditNote Id
    private Map<Id, Id> bInvIdToDocHeaderIdMap = new Map<Id, Id>();

    //Dim2,Prod,OrderGroup, userCompany Mapping, VATRegistration Mapping
    private Map<String, Id> dim2Map;
    private Map<String, Id> prodMap;
    private Map<Id, JVCO_Order_Group__c> orderGroupMap;
    private c2g__codaUserCompany__c userCompany;
    // Map Account Name to Tax Registration Number
    private Map<String, String> taxRegNumMap;
    private Map<Id, Id> salesRepMap;

    public JVCO_OrderMultipleBillNowLogicBatch(){}
    public JVCO_OrderMultipleBillNowLogicBatch(Set<Id> orderGroupIdSet)
    {
        this.orderGroupIdSet = orderGroupIdSet;
        invoiceDate = System.today();
        setSalesRep();
        setDim2AndProdMapping();
        setCompanyVATRegistrationNum();
        constructSupplierRecordTaxRegistrationNumber();
    }

    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        if(orderGroupIdSet == null)
        {
            return null;
        }else 
        {
            return Database.getQueryLocator([SELECT Id,
                                            JVCO_Group_Order_Account__r.JVCO_Dunning_Letter_Language__c,
                                            JVCO_Group_Order_Account__r.c2g__CODADimension1__c,
                                            JVCO_Group_Order_Account__r.c2g__CODADimension3__c,
                                            JVCO_Group_Order_Account__r.c2g__CODAOutputVATCode__r.JVCO_Rate__c
                                            FROM JVCO_Order_Group__c
                                            WHERE Id IN : orderGroupIdSet
                                            ]);
        }
        
    }

    public void execute(Database.BatchableContext bc, List<JVCO_Order_Group__c> scope)
    {
        orderGroupMap = new Map<Id, JVCO_Order_Group__c>(scope);
        try
        {
            setupInvoiceAndCreditNoteHeader(scope);
        }catch(Exception e)
        {
            JVCO_ErrorUtil errorUtil = new JVCO_ErrorUtil();
            errorUtil.logErrorFromOrderGroup(orderGroupMap.values(), 'Invoice Scheduler', 'BN-001', e.getMessage());
        }
    }

    private void setupInvoiceAndCreditNoteHeader(List<JVCO_Order_Group__c> orderGroupList)
    {
        Map<Id, AggregateResult> ogIdToAggregateResultMap = getOgIdToAggregateResultMap(orderGroupList);
        Map<Id, c2g__codaInvoice__c> ogIdToSInvMap = new Map<Id, c2g__codaInvoice__c>();
        Map<Id, c2g__codaCreditNote__c> ogIdToCNoteMap = new Map<Id, c2g__codaCreditNote__c>();
        for(JVCO_Order_Group__c orderGroup : orderGroupList)
        {   
            if(ogIdToAggregateResultMap.containsKey(orderGroup.Id))
            {
                AggregateResult ar = ogIdToAggregateResultMap.get(orderGroup.Id);
                //Set Total Amount
                Double totalAmt = (Double)ar.get('totalAmt');
                Date minStartDate = (Date)ar.get('minStartDate');
                Date maxEndDate = (Date)ar.get('maxEndDate');
                Date minDueDate = invoiceDate.addDays(Integer.valueOf(ar.get('minPaymentTerm')));
                //Invoice   
                if(totalAmt >= 0)
                {
                    Decimal totalSurAmount = (Decimal)ar.get('totalSurAmount');
                    Decimal totalSurTariffs = (Decimal)ar.get('totalSurTariffs');
                    Boolean ifReissuedInvoice = (Decimal)ar.get('ifReissuedInvoice') == 1 ? true : false;
                    ogIdToSInvMap.put(orderGroup.Id, createSalesInvoice(orderGroup.Id, totalSurAmount, 
                                                                    totalSurTariffs, minStartDate, maxEndDate, minDueDate, ifReissuedInvoice));
                //CreditNote
                }else if(totalAmt < 0)
                {
                    Boolean migratedContract = (Decimal)ar.get('migratedContractCounter') > 0;
                    String parent = 'COMPANY';
                    if(migratedContract)
                    {
                        if((Decimal)ar.get('prsTotalLines') > 0)
                        {
                            parent = 'PRS';
                        }else
                        {
                            parent = (Decimal)ar.get('pplTotalLines') > 0 ? 'PPL' : 'VPL';
                        }
                    }
                    Id sInvId = (Decimal)ar.get('origSInvCounter') == 1 ? (Id)ar.get('origSInvId') : null;
                    String sInvName = (String)ar.get('origSInvName') != '' ? (String)ar.get('origSInvName') : '';
                    ogIdToCNoteMap.put(orderGroup.Id, createCreditNote(orderGroup.Id, minStartDate, maxEndDate, 
                                                                parent, minDueDate, sInvId, sInvName));
                }   
            }   
        }
        setupInvoiceLineItems(ogIdToAggregateResultMap, ogIdToSInvMap);
        setupCreditNoteLineItems(ogIdToAggregateResultMap, ogIdToCNoteMap);
        setupBInvIdToDocumentHeaderIdMap(ogIdToSInvMap, ogIdToCNoteMap);   
    }

    private void setupInvoiceLineItems(Map<Id, AggregateResult> ogIdToAggregateResultMap, Map<Id, c2g__codaInvoice__c> ogIdToSInvMap)
    {
        //Sales Invoice Line
        if(!ogIdToSInvMap.isEmpty())
        {
            //Insert Sales Invoice
            insert ogIdToSInvMap.values();

            //Set Sales Invoice Line
            Set<c2g__codaInvoiceLineItem__c> sInvLineItemSet = new Set<c2g__codaInvoiceLineItem__c>();
            for(Id ogId : ogIdToSInvMap.keySet())
            {
                c2g__codaInvoice__c sInv = ogIdToSInvMap.get(ogId);
                AggregateResult ar = ogIdToAggregateResultMap.get(ogId);
                //PRS
                if((Decimal)ar.get('prsTotalLines') > 0)
                {
                    sInvLineItemSet.add(createSalesInvLineItem(sInv.Id, ogId, (Decimal)ar.get('prsTotalAmt'), (Decimal)ar.get('prsTotalTaxAmt'), 'PRS'));
                }
                //PPL
                if((Decimal)ar.get('pplTotalLines') > 0)
                {
                    sInvLineItemSet.add(createSalesInvLineItem(sInv.Id, ogId, (Decimal)ar.get('pplTotalAmt'), (Decimal)ar.get('pplTotalTaxAmt'), 'PPL'));
                }
                //VPL
                if((Decimal)ar.get('vplTotalLines') > 0)
                {
                    sInvLineItemSet.add(createSalesInvLineItem(sInv.Id, ogId, (Decimal)ar.get('vplTotalAmt'), (Decimal)ar.get('vplTotalTaxAmt'), 'VPL'));
                }
            }
            //Insert Sales Invoice Line
            insert new List<c2g__codaInvoiceLineItem__c>(sInvLineItemSet);
        }
    }

    private void setupCreditNoteLineItems(Map<Id, AggregateResult> ogIdToAggregateResultMap, Map<Id, c2g__codaCreditNote__c> ogIdToCNoteMap)
    {
        //Credit Note   Line
        if(!ogIdToCNoteMap.isEmpty())
        {   
            //Insert Sales Credit Note
            insert ogIdToCNoteMap.values();

            //Setup Sales Credit Note Line
            Set<c2g__codaCreditNoteLineItem__c> cNoteLineItemSet = new Set<c2g__codaCreditNoteLineItem__c >();
            for(Id ogId : ogIdToCNoteMap.keySet())
            {
                AggregateResult ar = ogIdToAggregateResultMap.get(ogId);
                c2g__codaCreditNote__c cNote = ogIdToCNoteMap.get(ogId);
                //PRS
                if((Decimal)ar.get('prsTotalLines') > 0)
                {
                    cNoteLineItemSet.add(createCredNoteLineItem(cNote.Id, ogId, (Decimal)ar.get('prsTotalAmt'), (Decimal)ar.get('prsTotalTaxAmt'), 'PRS'));
                }
                //PPL
                if((Decimal)ar.get('pplTotalLines') > 0)
                {
                    cNoteLineItemSet.add(createCredNoteLineItem(cNote.Id, ogId, (Decimal)ar.get('pplTotalAmt'), (Decimal)ar.get('pplTotalTaxAmt'), 'PPL'));
                }
                //VPL
                if((Decimal)ar.get('vplTotalLines') > 0)
                {
                    cNoteLineItemSet.add(createCredNoteLineItem(cNote.Id, ogId, (Decimal)ar.get('vplTotalAmt'), (Decimal)ar.get('vplTotalTaxAmt'), 'VPL'));
                }
            }
            //Insert Sales Credit Note Line
            insert new List<c2g__codaCreditNoteLineItem__c>(cNoteLineItemSet);
        }
    }

    private void setupBInvIdToDocumentHeaderIdMap(Map<Id, c2g__codaInvoice__c> ogIdToSInvMap, 
                                                    Map<Id, c2g__codaCreditNote__c> ogIdToCNoteMap)
    {
        for(Order order : [SELECT Id, JVCO_Order_Group__c 
                          FROM Order
                          WHERE JVCO_Order_Group__c IN : orderGroupIdSet
                          AND JVCO_Number_of_Order_Products__c != 0])
        {   
            //Invoice
            if(ogIdToSInvMap.containsKey(order.JVCO_Order_Group__c))
            {
                bInvIdToDocHeaderIdMap.put(order.Id, ogIdToSInvMap.get(order.JVCO_Order_Group__c).Id);
            //CreditNote
            }else if(ogIdToCNoteMap.containsKey(order.JVCO_Order_Group__c))
            {
                bInvIdToDocHeaderIdMap.put(order.Id, ogIdToCNoteMap.get(order.JVCO_Order_Group__c).Id);
            }
        }
    }

    private c2g__codaInvoice__c createSalesInvoice(Id ogId, Decimal totalSurAmount, Decimal totalSurTariffs, Date minStartDate, Date maxEndDate, Date minDueDate, Boolean ifReissuedInvoice)
    {
        c2g__codaInvoice__c sInv = new c2g__codaInvoice__c();
        sInv.c2g__Account__c = orderGroupMap.get(ogId).JVCO_Group_Order_Account__c;
        sInv.c2g__DueDate__c = minDueDate;
        sInv.c2g__InvoiceDate__c = invoiceDate;
        sInv.c2g__DeriveCurrency__c = true;
        sInv.c2g__DerivePeriod__c = true;
        sInv.c2g__DeriveDueDate__c = false;
        sInv.JVCO_Licence_Start_Date__c = minStartDate;
        sInv.JVCO_Licence_End_Date__c = maxEndDate;
        String dunningLetterLanguage = orderGroupMap.get(ogId).JVCO_Group_Order_Account__r.JVCO_Dunning_Letter_Language__c;
        sInv.JVCO_Customer_Type__c = dunningLetterLanguage;
        sInv.JVCO_Surcharge_Tariffs__c = totalSurTariffs;
        sInv.JVCO_Surcharge_Generation_Date__c = JVCO_DocumentGenerationWrapper.getSurchargeDate(dunningLetterLanguage, totalSurAmount, totalSurTariffs, sInv.c2g__DueDate__c);
        sInv.Total_Surchargeable_Amount__c = totalSurAmount;
        //GREEN-31875 08-May-2018 john.patrick valdez
        sInv.JVCO_Sales_Rep__c = salesRepMap.get(ogId);
        sINv.Re_issued__c = ifReissuedInvoice;
        return sInv;
    }

    private c2g__codaCreditNote__c createCreditNote(Id ogId, Date minStartDate, Date maxEndDate, String parent, Date minDueDate, Id sInvId, String sInvName)
    {
        c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
        cNote.c2g__Account__c = orderGroupMap.get(ogId).JVCO_Group_Order_Account__c;
        //cNote.c2g__DueDate__c = minDueDate;
        cNote.c2g__CreditNoteDate__c = invoiceDate;
        cNote.c2g__DeriveCurrency__c = true;
        cNote.c2g__DerivePeriod__c = true;
        cNote.c2g__DeriveDueDate__c = true;
        cNote.JVCO_Credit_Reason__c = 'Invoice amendment';
        cNote.JVCO_Period_Start_Date__c = minStartDate;
        cNote.JVCO_Period_End_Date__c = maxEndDate;
        cNote.c2g__Invoice__c = sInvId;
        cNote.JVCO_Reference_Document__c = sInvName;
        //GREEN-31875 08-May-2018 john.patrick valdez
        cNote.JVCO_Sales_Rep__c = salesRepMap.get(ogId);
        if(parent == 'COMPANY')
        {
            cNote.JVCO_VAT_Registration_Number__c = userCompany.c2g__Company__r.c2g__VATRegistrationNumber__c;
        }else
        {
            cNote.JVCO_VAT_Registration_Number__c = taxRegNumMap.get(parent);
            cNote.Parent__c = parent;
        }
        
        return cNote;
    }
    
    private c2g__codaInvoiceLineItem__c createSalesInvLineItem(Id sInvId, Id ogId, Decimal amt, Decimal taxAmt, String type)
    {
        c2g__codaInvoiceLineItem__c sInvLine = new c2g__codaInvoiceLineItem__c();
        sInvLine.c2g__Invoice__c = sInvId;
        sInvLine.c2g__Dimension1__c = orderGroupMap.get(ogId).JVCO_Group_Order_Account__r.c2g__CODADimension1__c;
        sInvLine.c2g__Dimension3__c = orderGroupMap.get(ogId).JVCO_Group_Order_Account__r.c2g__CODADimension3__c;
        sInvLine.c2g__TaxCode1__c = orderGroupMap.get(ogId).JVCO_Group_Order_Account__r.c2g__CODAOutputVATCode__c;
        sInvLine.c2g__DeriveLineNumber__c = true;
        sInvLine.c2g__DeriveUnitPriceFromProduct__c = false;
        sInvLine.c2g__CalculateTaxValue1FromRate__c = false;
        sInvLine.c2g__TaxValue1__c = taxAmt.setScale(2);
        sInvLine.c2g__Quantity__c = 1;
        sInvLine.c2g__UnitPrice__c = amt.setScale(2);
        sInvLine.c2g__Dimension2__c = dim2Map.get(type);
        sInvLine.c2g__Product__c = prodMap.get(type);
        return sInvLine;
    }

    private c2g__codaCreditNoteLineItem__c createCredNoteLineItem(Id cNoteId, Id ogId, Decimal amt, Decimal taxAmt, String type)
    {
        c2g__codaCreditNoteLineItem__c cNoteLine = new c2g__codaCreditNoteLineItem__c();
        cNoteLine.c2g__CreditNote__c = cNoteId;
        cNoteLine.c2g__Dimension1__c = orderGroupMap.get(ogId).JVCO_Group_Order_Account__r.c2g__CODADimension1__c;
        cNoteLine.c2g__Dimension3__c = orderGroupMap.get(ogId).JVCO_Group_Order_Account__r.c2g__CODADimension3__c;
        cNoteLine.c2g__TaxCode1__c = orderGroupMap.get(ogId).JVCO_Group_Order_Account__r.c2g__CODAOutputVATCode__c;
        cNoteLine.c2g__DeriveLineNumber__c = true;
        cNoteLine.c2g__DeriveUnitPriceFromProduct__c = false;
        cNoteLine.c2g__CalculateTaxValueFromRate__c = false;
        cNoteLine.c2g__TaxValue1__c = (taxAmt * -1).setScale(2);
        cNoteLine.c2g__Quantity__c = 1;
        cNoteLine.c2g__UnitPrice__c = (amt * -1).setScale(2);
        cNoteLine.c2g__Dimension2__c = dim2Map.get(type);
        cNoteLine.c2g__Product__c = prodMap.get(type);
        return cNoteLine;
    }

    private Map<Id, AggregateResult> getOgIdToAggregateResultMap(List<JVCO_Order_Group__c> orderGroupList)
    {
        Map<Id, AggregateResult> ogIdToAggregateResultMap = new Map<Id, AggregateResult>();
        for(AggregateResult ar : [SELECT JVCO_Order_Group__c Id,
                                  SUM(JVCO_PRS_Total_Lines__c)prsTotalLines,
                                  SUM(JVCO_PPL_Total_Lines__c)pplTotalLines,
                                  SUM(JVCO_VPL_Total_Lines__c)vplTotalLines,
                                  SUM(JVCO_PRS_TotalAmount__c)prsTotalAmt,
                                  SUM(JVCO_PPL_TotalAmount__c)pplTotalAmt,
                                  SUM(JVCO_VPL_TotalAmount__c)vplTotalAmt,
                                  SUM(JVCO_PRS_Tax_Total_Amount__c)prsTotalTaxAmt,
                                  SUM(JVCO_PPL_Tax_Total_Amount__c)pplTotalTaxAmt,
                                  SUM(JVCO_VPL_Tax_Total_Amount__c)vplTotalTaxAmt,
                                  SUM(TotalAmount) totalAmt,
                                  MIN(JVCO_Licence_Start_Date__c)minStartDate,
                                  MAX(JVCO_Licence_End_Date__c)maxEndDate,
                                  MIN(SBQQ__Quote__r.JVCO_Payment_Terms__c)minPaymentTerm,
                                  SUM(JVCO_Total_Surcharge_Amount__c)totalSurAmount,
                                  SUM(JVCO_Surcharge_Tariffs__c)totalSurTariffs,
                                  MAX(SBQQ__Quote__r.JVCO_Re_issued__c)ifReissuedInvoice,
                                  COUNT(SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c)migratedContractCounter,
                                  COUNT_DISTINCT(SBQQ__Quote__r.SBQQ__MasterContract__r.SBQQ__Quote__r.JVCO_Sales_Invoice__c)origSInvCounter,
                                  MAX(SBQQ__Quote__r.SBQQ__MasterContract__r.SBQQ__Quote__r.JVCO_Sales_Invoice__c)origSInvId,
                                  MAX(SBQQ__Quote__r.SBQQ__MasterContract__r.SBQQ__Quote__r.JVCO_Sales_Invoice__r.Name)origSInvName
                                  FROM Order
                                  WHERE JVCO_Order_Group__c != NULL
                                  AND JVCO_Order_Group__c IN : orderGroupList
                                  AND JVCO_Cancelled__c = false
                                  AND JVCO_Number_of_Order_Products__c != 0
                                  GROUP BY JVCO_Order_Group__c])
        {
            ogIdToAggregateResultMap.put((Id)ar.get('Id'), ar); 
        }
        return ogIdToAggregateResultMap;
    }
    
    private void setDim2AndProdMapping()
    {
        if(dim2Map == null)
        {
            dim2Map = new Map<String, Id>();
            for(c2g__codaDimension2__c dim2 : [SELECT Name, Id FROM c2g__codaDimension2__c
                                                WHERE Name IN ('PRS', 'PPL', 'VPL')])
            {
                dim2Map.put(dim2.Name, dim2.Id);
            }
        }
        
        if(prodMap == null)
        {
            prodMap = new Map<String, Id>();
            for(Product2 prod : [SELECT Id, Name FROM Product2 
                                    WHERE Name IN ('PPL Product', 'PRS Product', 'VPL Product')])
            {
                prodMap.put(prod.Name.substring(0,3), prod.Id);
            }
        }   
    }

    private void setCompanyVATRegistrationNum()
    {
        if(userCompany == null)
        {
            userCompany = [SELECT c2g__Company__c, c2g__Company__r.c2g__VATRegistrationNumber__c, 
                            c2g__User__c 
                            FROM c2g__codaUserCompany__c 
                            WHERE c2g__User__c =: UserInfo.getUserId()
                            LIMIT 1];
        }    
    }

    private void constructSupplierRecordTaxRegistrationNumber()
    {
        if(taxRegNumMap == null)
        {
            taxRegNumMap = new Map<String, String>();                        
            for(Account a : [SELECT Id, Name, c2g__CODAVATRegistrationNumber__c
                            FROM Account 
                            WHERE Name IN ('PPL','PRS','VPL')])
            {
                taxRegNumMap.put(a.Name, a.c2g__CODAVATRegistrationNumber__c);
            }
        }   
    }

    private void setSalesRep()
    {
        if(salesRepMap == null)
        {
            salesRepMap = new Map<Id,Id>();
            for(Order o : [SELECT Id, SBQQ__Quote__r.SBQQ__SalesRep__c, JVCO_Order_Group__c
                            FROM Order
                            WHERE JVCO_Order_Group__c IN : orderGroupIdSet])
            {
                salesRepMap.put(o.JVCO_Order_Group__c, o.SBQQ__Quote__r.SBQQ__SalesRep__c);
            }
        }
    }
    
    public void finish(Database.BatchableContext bc)
    {
        Decimal BILLINGINVOICESCOPE = JVCO_DocumentGenerationWrapper.generalSettings.JVCO_Link_Billing_Line_Scope__c != null ? 
                                        JVCO_DocumentGenerationWrapper.generalSettings.JVCO_Link_Billing_Line_Scope__c : 1;
        JVCO_LinkBlngLineToSInvAndCnoteLineBatch batch = new JVCO_LinkBlngLineToSInvAndCnoteLineBatch(bInvIdToDocHeaderIdMap, 'BILLNOW' + userinfo.getuserid());
        batch.stopMatching = true;
        Id jobId = Database.executeBatch(batch, Integer.valueOf(BILLINGINVOICESCOPE));
    }
}