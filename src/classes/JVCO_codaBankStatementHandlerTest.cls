@isTest
private class JVCO_codaBankStatementHandlerTest
{
    @testSetup 
    static void setupTestData()
    {
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId(), Email = 'test@gmail.com'))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        insert glaList;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        List<c2g__codaDimension2__c> dim2List = new List<c2g__codaDimension2__c>();
        c2g__codaDimension2__c pplDim2 = JVCO_TestClassHelper.setDim2('PPL');
        dim2List.add(pplDim2);
        c2g__codaDimension2__c prsDim2 = JVCO_TestClassHelper.setDim2('PRS');
        dim2List.add(prsDim2);
        c2g__codaDimension2__c vplDim2 = JVCO_TestClassHelper.setDim2('VPL');
        dim2List.add(vplDim2);
        insert dim2List;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();
        insert dim3;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
    custAcc.Name=Label.JVCO_Account_Unidentified;
    update custAcc;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Test.startTest();
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        insert JVCO_TestClassHelper.setQuoteProcess();
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        Test.stopTest();

        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        JVCO_TestClassHelper.setGeneralSettingsCS();
        JVCO_TestClassHelper.setCashMatchingCS();
        
        
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        insert sInv;
        List<c2g__codaInvoiceLineItem__c> sInvLineList = new List<c2g__codaInvoiceLineItem__c>();
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, pplDim2.Id, p.Id));
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, prsDim2.Id, p.Id));
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, vplDim2.Id, p.Id));
        insert sInvLineList;
        Set<Id> sInvIdSet = new Set<Id>();
        sInvIdSet.add(sInv.Id);

        //Run Post Batch
        JVCO_PostSalesInvoiceBatch pSIB = new JVCO_PostSalesInvoiceBatch(sInvIdSet, new Set<Id>());
        psIB.stopMatching = true;
        Database.executeBatch(pSIB);
        
        c2g__codaBankAccount__c testBankAccount = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.id, accReceivableGLA.id);
        insert testBankAccount;
    }
    
    @isTest 
    static void codaBankStatementHandlerTest()
    {
        c2g__codaInvoice__c testInvoice = [SELECT Id, Name, c2g__Account__c, c2g__OwnerCompany__c
                                           FROM c2g__codaInvoice__c
                                           LIMIT 1];

        c2g__codaBankAccount__c testBankAccount = [SELECT Id, Name, c2g__AccountName__c
                                                   FROM c2g__codaBankAccount__c
                                                   LIMIT 1];

        c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem bankStatementLine = new c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem();
        bankStatementLine.DateValue = System.today();
        bankStatementLine.Amount = 1000;
        bankStatementLine.Reference = 'test0001';
        bankStatementLine.OwnerCompany = c2g.CODAAPICommon.Reference.getRef(testInvoice.c2g__OwnerCompany__c, null);
        
        List<c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem> bankStatementLineItemList = new List<c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem>();
        bankStatementLineItemList.add(bankStatementLine);
        
        c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItems bankStatementLineItems = new c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItems();
        bankStatementLineItems.LineItemList = bankStatementLineItemList;
        
        c2g.CODAAPIBankStatementTypes_6_0.BankStatement bankStatement = new c2g.CODAAPIBankStatementTypes_6_0.BankStatement();
        bankStatement.BankAccount = c2g.CODAAPICommon.Reference.getRef(testBankAccount.Id, null);
        bankStatement.OwnerCompany = c2g.CODAAPICommon.Reference.getRef(testInvoice.c2g__OwnerCompany__c, null);
        bankStatement.Reference = 'test0001';
        bankStatement.OpeningBalance = 10100;
        bankStatement.StatementDate = System.today();
        bankStatement.LineItems = bankStatementLineItems;
        c2g.CODAAPIBankStatement_6_0.CreateBankStatement(new c2g.CODAAPICommon_6_0.Context(), bankStatement);

        c2g__codaBankStatementLineItem__c updatedBankStatementLine = [SELECT Id
                                                                        FROM c2g__codaBankStatementLineItem__c 
                                                                        LIMIT 1];
        updatedBankStatementLine.JVCO_Account__c = testInvoice.c2g__Account__c;
        updatedBankStatementLine.JVCO_Bank_Account__c = testBankAccount.Id;
        updatedBankStatementLine.JVCO_SalesInvoice__c = testInvoice.Id;
        update updatedBankStatementLine;

        c2g__codaBankStatement__c updatedBankStatement = [SELECT Id, Name, c2g__Reference__c
                                                           FROM c2g__codaBankStatement__c 
                                                           WHERE c2g__Reference__c = 'test0001' 
                                                           LIMIT 1];
        Test.startTest();
        updatedBankStatement.JVCO_Bank_Transfer__c = true;
        update updatedBankStatement;
        try{
            update updatedBankStatement;
            System.assert(false);
        }catch(Exception e)
        {
            System.assert(true);
        }
        
        Test.stopTest();
        
        JVCO_BankStatementLogicQueueable blq = new JVCO_BankStatementLogicQueueable(null, null, null, null);
        blq.createErrorLog(null, null, null);
    }
}