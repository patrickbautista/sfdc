@isTest
public class JVCO_CreditCancellationLogicTest {
    @testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        SBQQ.TriggerControl.disable();
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        
        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim3();
        
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        
        Test.startTest();
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        
        JVCO_TestClassHelper.setGeneralSettingsCS();
        
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        JVCO_FFUtil.stopCodaTransasctionHandlerAfterUpdate = true;
        insert new GLA_for_Cash_Transfer__c(name = '10100 - Bank Cash');

        c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
        cNote.c2g__Account__c = licAcc.Id;
        cNote.c2g__DueDate__c = System.today() + 30;
        cNote.c2g__CreditNoteDate__c = System.today();
        cNote.c2g__DeriveCurrency__c = true;
        cNote.c2g__DerivePeriod__c = true;
        cNote.c2g__DeriveDueDate__c = true;
        insert cNote;
        
        c2g__codaCreditNoteLineItem__c cNoteLine = new c2g__codaCreditNoteLineItem__c();
        cNoteLine.c2g__CreditNote__c = cNote.Id;
        cNoteLine.c2g__Dimension1__c = dim1.Id;
        cNoteLine.c2g__Dimension3__c = [SELECT Id FROM c2g__codaDimension3__c LIMIT 1].Id;
        cNoteLine.c2g__TaxCode1__c = taxCode.Id;
        cNoteLine.c2g__DeriveLineNumber__c = true;
        cNoteLine.c2g__DeriveUnitPriceFromProduct__c = false;
        cNoteLine.c2g__Quantity__c = 1;
        cNoteLine.c2g__UnitPrice__c = 500;
        cNoteLine.c2g__Dimension2__c = [SELECT Id FROM c2g__codaDimension2__c LIMIT 1].Id;
        cNoteLine.c2g__Product__c =  p.Id;
        insert cNoteLine;
        
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        o.JVCO_Sales_Credit_Note__c = cNote.Id;
        insert o;
        
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        List<Order> orderList = new List<Order>();
        orderList.add(o);
        
        Test.stopTest();
    }
    
    @isTest
    static void testRunProcess()
    {
        
        c2g__codaCreditNote__c cNote = [SELECT Id, Name, c2g__Account__c,
                                        JVCO_Credit_Reason__c, 
                                        c2g__CreditNoteStatus__c, 
                                        c2g__PaymentStatus__c,
                                        JVCO_Period_Start_Date__c,      
                                        JVCO_Period_End_Date__c,
                                        JVCO_Sales_Rep__c
                                        FROM c2g__codaCreditNote__c LIMIT 1];
        
        Test.startTest();
        PageReference pageRef = Page.JVCO_CancelCreditNotePage;
        Test.setCurrentPage(pageRef);
        apexPages.Currentpage().getParameters().put('Id', cNote.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(cNote);
        JVCO_RunCreditNoteBatch runCreditCancellationPage = new JVCO_RunCreditNoteBatch(sc);
        runCreditCancellationPage.runConvert();
        runCreditCancellationPage.returnTocNote();
        Test.stopTest();
    }
    @isTest
    static void testRunBatch(){
        
        c2g__codaCreditNote__c credNote = [SELECT Id, Name, c2g__Account__c,
                                           JVCO_Credit_Reason__c, 
                                           c2g__CreditNoteStatus__c, 
                                           c2g__PaymentStatus__c,
                                           JVCO_Period_Start_Date__c,      
                                           JVCO_Period_End_Date__c,
                                           JVCO_Sales_Rep__c
                                           FROM c2g__codaCreditNote__c LIMIT 1];
        Order ord = [SELECT Id,JVCO_Sales_Credit_Note__c FROM Order LIMIT 1];
        ord.JVCO_Sales_Credit_Note__c = credNote.Id;
        Test.startTest();
        update ord;
        Database.executeBatch(new JVCO_CreditCancellationLogic(credNote),1);
        Test.stopTest();
    }
}