public with sharing class JVCO_InvokeDupeElimMergeProcessVenueExt {

    public class DupeElimRedirectException extends Exception {}

    private List<JVCO_Venue__c> venueList { get; set; }

    public JVCO_InvokeDupeElimMergeProcessVenueExt(ApexPages.StandardSetController stdSetController){
        venueList = (List<JVCO_Venue__c>)stdSetController.getSelected();
    }

    public PageReference redirectVenueToDupeElimPage() {
        Profile profile = [Select Name from Profile Where Id = :UserInfo.getProfileId()];
        if(!'Licensing User'.equals(profile.Name) && !'System Administrator'.equals(profile.Name)){
            return new PageReference('/apex/InsufficientPrivileges');
        }
        
        List<String> venueNamesList = new List<String>();
        if (!venueList.isEmpty()) {
            for (JVCO_Venue__c a : queryVenues(venueList))
            {
                EncodingUtil.urlEncode(a.Name, 'UTF-8');
                venueNamesList.add(a.Name);
            }
        }
        return new PageReference(getDestinationUrl(venueNamesList, true));
    }

    public PageReference redirectVenueToDupeElimPage2() {
        Profile profile = [Select Name from Profile Where Id = :UserInfo.getProfileId()];
        if(!'Licensing User'.equals(profile.Name) && !'System Administrator'.equals(profile.Name)){
            return new PageReference('/apex/InsufficientPrivileges');
        }
        
        List<String> venueNamesList = new List<String>();
        if (!venueList.isEmpty()) {
            for (JVCO_Venue__c a : queryVenues(venueList))
            {
                venueNamesList.add(a.Name);
            }
        }
        Set<String> venueSet = new Set<String>();
        venueSet.addAll(venueNamesList);
        venueNamesList = new List<String>();
        venueNamesList.addAll(venueSet);
        return new PageReference(getDestinationUrl(venueNamesList, false));
    }

    private List<JVCO_Venue__c> queryVenues(List<JVCO_Venue__c> venueList) {
        return [SELECT Id, Name FROM JVCO_Venue__c WHERE Id IN :venueList LIMIT 3];
    }

    private String getDestinationUrl(List<String> venueNamesList, Boolean isHideFilter) {

        String names = '';
        String destination = '';
        if (isHideFilter) {
            destination = '/apex/JVCO_AccountMerge?find=true&object={0}&field1={1}&limit={2}&hideFilter=1';
        } else {
            destination = '/apex/JVCO_SearchMerge?find=true&object={0}&field1={1}&limit={2}&field2={5}&op2={6}&field3={7}&op3={8}';
        }
        if (!venueNamesList.isEmpty()) {
            names = String.join(venueNamesList, ', ');
            destination = destination + '&op1={3}&value1={4}';
        }

        String[] arguments;
        if (!venueNamesList.isEmpty() && venueNamesList.size() > 1)
        {
        	arguments = new String[]{ 'JVCO_Venue__c', 'Name', '3', 'contains', names, 'JVCO_PPL_Id__c', 'equals', 'JVCO_PRS_Id__c', 'equals'};
        }
        else
        {
        	arguments = new String[]{ 'JVCO_Venue__c', 'Name', '3', 'equals', names, 'JVCO_PPL_Id__c', 'equals', 'JVCO_PRS_Id__c', 'equals'};
        }
        return String.format(destination, arguments);
    }

}