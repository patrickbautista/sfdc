/* ----------------------------------------------------------------------------------------------
Name: JVCO_PopulateDimension2Batch.cls 
Description: Batch class that populates Dimension_2s__c of old invoice l
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
22-Nov-2017  0.1         mary.ann.a.ruelan     Intial creation
----------------------------------------------------------------------------------------------- */
global class JVCO_PopulateDimension2Batch implements Database.Batchable<sObject>, Database.Stateful{ 
    
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        return Database.getQueryLocator([SELECT Id, blng__Invoice__c, blng__Product__c, blng__OrderProduct__c, blng__Subtotal__c,
                                         JVCO_Dimension_2_Copy__c, Dimension_2s__c
                                         FROM blng__InvoiceLIne__c
                                         WHERE JVCO_Dimension_2__c
                                         IN ('', NULL) AND blng__Subtotal__c!=NULL AND blng__Product__c NOT IN ('', NULL)]);         
        
    }
    
    global void execute(Database.BatchableContext bc, List<blng__InvoiceLine__c> scope)
    {
        if (scope.size()>0)
        {
            JVCO_BillingLineUtil.updateDimension2(scope);            
            List<Database.SaveResult> SaveResultList = Database.update(scope, false);  
            
        }  
        
        
    }    
    
    global void finish(Database.BatchableContext BC) 
    {                   
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
        System.debug('The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures.');
        System.debug('DONE');    
    }
}