public class JVCO_approvalQuotesCreationCompleteBatch implements Database.Batchable<sObject>, Database.Stateful 
{

    public Set<Id> qIdSet = new Set<Id>();
    public Set<Id> oppIdSetForOrdring = new Set<Id>();


    public JVCO_approvalQuotesCreationCompleteBatch(Set<Id> idSet){
        qIdSet = idSet;
    } 
    
    public Database.QueryLocator start(Database.BatchableContext BC){

        string query = 'SELECT ID, SBQQ__Type__c, JVCO_Quote_Auto_Renewed__c, SBQQ__Status__c, SBQQ__Opportunity2__c, SBQQ__Opportunity2__r.SBQQ__Contracted__c, SBQQ__NetAmount__c FROM SBQQ__Quote__c WHERE SBQQ__LineItemCount__c > 0 AND JVCO_Recalculated__c = TRUE AND SBQQ__Primary__c = TRUE AND NoOfQuoteLinesWithoutSPV__c = 0 AND SBQQ__Opportunity2__r.SBQQ__Contracted__c = false AND (SBQQ__NetAmount__c != 0 AND SBQQ__NetAmount__c != null)'; 
        //AND SBQQ__Opportunity2__r.JVCO_OpportunityCancelled__c = False '; 02-Mar-2021  GREEN-36362-removed the condition

        
        String strIds ='';
        for(Id i : qIdSet){
            strIds += '\'' + i + '\',';
        }

        strIds = strIds.substring(0, strIds.length() - 1);

        query += 'AND Id IN (';
        query += strIds;
        query += ')';
        system.debug('approvalprocess'+query);
    

                

        return Database.getQueryLocator(query);
        
    }
    
    public void execute(Database.BatchableContext BC, List<SBQQ__Quote__c> quoteList)
    {
        
        
        List<sObject> lstObjectRec = new List<sObject>();
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>(); 
        Set<Id> oppIdSet = new Set<Id>();
        Set<Id> quoteIdSet = new Set<Id>();
        Savepoint sp = Database.setSavepoint();

        for(SBQQ__Quote__c quote : quoteList)
        {
            if(quote.SBQQ__Type__c == 'Amendment'){
                quoteIdSet.add(quote.Id);
            }

            if(quote.SBQQ__Opportunity2__c != Null)
            {
              if(!quote.SBQQ__Opportunity2__r.SBQQ__Contracted__c)
              {
                oppIdSet.add(quote.SBQQ__Opportunity2__c);
                Opportunity OppRec = quote.SBQQ__Opportunity2__r;
                OppRec.StageName = 'Closed Won';

                OppRec.Amount = quote.SBQQ__NetAmount__c;
                //end

                lstObjectRec.add((sObject) OppRec);

                quote.SBQQ__Status__c = 'Accepted';
                quote.JVCO_QuoteComplete__c = true; 
                lstObjectRec.add((sObject) quote );
              }
            }
        }

        try{
                            
            if(!quoteIdSet.isEmpty()){
                List<SBQQ__QuoteLine__c> qlRecList = new List<SBQQ__QuoteLine__c>();
                for(SBQQ__QuoteLine__c ql : [SELECT id, SBQQ__Quantity__c, SBQQ__PriorQuantity__c from SBQQ__QuoteLine__c where SBQQ__Quote__c IN: quoteIdSet AND SBQQ__EffectiveQuantity__c = 0]){
                    if(ql.SBQQ__PriorQuantity__c != ql.SBQQ__Quantity__c){
                            ql.SBQQ__PriorQuantity__c = ql.SBQQ__Quantity__c;
                            qlRecList.add(ql);
                    } 
                }

                SBQQ.TriggerControl.disable();
                if(!qlRecList.isEmpty()){
                    update qlRecList;
                }
                SBQQ.TriggerControl.enable();
            }
        }
        catch(Exception e){
            Database.rollback(sp);
            ffps_custRem__Custom_Log__c qErrLog = new ffps_custRem__Custom_Log__c();
            qErrLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
            qErrLog.ffps_custRem__Detail__c = 'Error: ' + string.valueof(e.getMessage());
            qErrLog.ffps_custRem__Message__c = 'Approval Process: Update of Prior Quantity on QL';
            qErrLog.ffps_custRem__Grouping__c = 'ERROR';
            insert qErrLog;
        }
        
        if(lstObjectRec.size() > 0)
        {
            if(!lstObjectRec.isEmpty())
            {
               
               if(Test.isRunningTest()){
                     lstObjectRec.add(new Opportunity());
                }
              List<Database.SaveResult> res = Database.update(lstObjectRec,false); 
                for(Integer i = 0; i < lstObjectRec.size(); i++)
                {
                    Database.SaveResult srOppList = res[i];
                    sObject origrecord = lstObjectRec[i];
                  if(!srOppList.isSuccess())
                    {
                        Database.rollback(sp);
                        System.debug('Update sObject fail: ');
                        for(Database.Error objErr:srOppList.getErrors())
                        {
                            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                            errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                            errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(origrecord.ID);
                            errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                            errLog.ffps_custRem__Message__c = 'Background Auto Matching';
                            errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());

                            if(origrecord.getSObjectType() == Opportunity.sObjectType)
                            {
                                errLog.ffps_custRem__Message__c = 'ApprovalProcessAutomaticContracting: Update Opportunity Record';
                            }else if(origrecord.getSObjectType() == SBQQ__Quote__c.sObjectType)
                            {
                                errLog.ffps_custRem__Message__c = 'ApprovalProcessAutomaticContracting: Update Quote Record';
                                //qIdSet.remove(origrecord.ID);
                            }else if(origrecord.getSObjectType() == SBQQ__QuoteLineGroup__c.sObjectType)
                            {
                                errLog.ffps_custRem__Message__c = 'ApprovalProcessAutomaticContracting: Update SBQQ__QuoteLineGroup__c Record';
                            }else if(origrecord.getSObjectType() == JVCO_Affiliation__c.sObjectType)
                            {
                                errLog.ffps_custRem__Message__c = 'ApprovalProcessAutomaticContracting: Update JVCO_Affiliation__c Record';
                            }

                            errLogList.add(errLog);                         
                       }
                    }
                }
            }
        }
        
        List<Opportunity> setOpp = new List<Opportunity>();
        setOpp = [Select ID,SBQQ__PrimaryQuote__c from Opportunity where id in :oppIdSet];
        JVCO_OpportunityTriggerHandler.setPrimaryQuote(setOpp);

        for(Id oppId: oppIdSet)
        {
          try
          { oppIdSetForOrdring.add(oppId);
            system.debug('oppIdSetForOrdring2: '+ oppIdSetForOrdring);
            SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractGenerator',oppId , null); 
          }catch(exception e)
          {
            String ErrorLineNumber = e.getLineNumber() != Null ? String.ValueOf(e.getLineNumber()):'None';
            String ErrorMessage = 'Cause: ' + e.getCause() + ' ErrMsg: ' + e.getMessage();
            errLogList.add(JVCO_ErrorUtil.logError(oppId, ErrorMessage , ErrorLineNumber , 'ApprovalProcessAutomaticContracting'));
            oppIdSetForOrdring.remove(oppId);
            Database.rollback(sp);
          }
        }

         if(!errLogList.isempty())
         {
             try
             {
                 insert errLogList;
             }
             catch(Exception ex)
             {
                 System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                 
             }
             
         } 
    }
    
    public void finish(Database.BatchableContext BC)
    {
            system.debug('oppIdSetForOrdring: '+oppIdSetForOrdring);
            JVCO_approvalQuotesCreationOrderBatch orderRQBatch = new JVCO_approvalQuotesCreationOrderBatch(oppIdSetForOrdring);
            Id batchProcessId = Database.executeBatch(orderRQBatch,1);
    }

}