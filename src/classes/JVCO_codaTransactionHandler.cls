public class JVCO_codaTransactionHandler
{
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_codaTransactionTrigger__c : false;
    
    
    public static void beforeInsert(List<c2g__codaTransaction__c> transactionList)
    {
        if(!skipTrigger) 
        {
            system.debug(logginglevel.ERROR,'### JVCO_codaTransactionHandler beforeInsert START CPU Time: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime());//jason.e.mactal added to monitor CPU Time   
            JVCO_WeekendLogic.setWeekendWork(transactionList);
            system.debug(logginglevel.ERROR,'### JVCO_codaTransactionHandler beforeInsert END CPU Time: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime());//jason.e.mactal added to monitor CPU Time
        }
    }
    
    public static void afterInsert(Map<Id, c2g__codaTransaction__c> transactionMap)
    {
        if(!skipTrigger) 
        {
        }
    }
    
    public static void afterUpdate(Map<Id, c2g__codaTransaction__c> transactionMap, Map<Id, c2g__codaTransaction__c> transactionOldMap)
    {
        if(!skipTrigger && !JVCO_FFutil.stopCodaTransactionHandlerSMP) 
        {
            updateIncomeDirectDebit(transactionMap, transactionOldMap);
        }
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: sean.g.perez
        Company: Accenture
        Description: GREEN-35598 GmarterPay - Credit or Cash (Other Payment Method) is matched against Sales Invoice with Active DD
        Param: List<c2g__codaInvoice__c
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        11-May-2020 sean.g.perez         Initial version of the function
    ----------------------------------------------------------------------------------------------- */
    private static void updateIncomeDirectDebit(Map<Id, c2g__codaTransaction__c> newTransactionMap, Map<Id,c2g__codaTransaction__c> oldTransactionMap)
    {
        Set<Id> sInvIdSet = new Set<Id>();
        for(c2g__codaTransaction__c trans : newTransactionMap.values())
        {
            if(trans.c2g__TransactionType__c == 'Invoice' && 
                oldTransactionMap.get(trans.Id).c2g__AccountOutstandingTotal__c != 0 &&
                trans.c2g__AccountOutstandingTotal__c == 0 &&
                trans.c2g__SalesInvoice__c != null)
            {
                sInvIdSet.add(trans.c2g__SalesInvoice__c); 
            }
        }

        if(!sInvIdSet.isEmpty())
        {
            Set<Id> invGrpIdSet = new Set<Id>();
            Set<Id> incDDIdSet = new Set<Id>();
            List<Income_Direct_Debit__c> incomeDirectDebitList = new List<Income_Direct_Debit__c>();
            
            for(c2g__codaInvoice__c salesInv : [SELECT JVCO_Invoice_Group__c 
                                                FROM c2g__codaInvoice__c 
                                                WHERE Id IN: sInvIdSet
                                                AND JVCO_Invoice_Group__c != NULL])
            {
                invGrpIdSet.add(salesInv.JVCO_Invoice_Group__c);
            }
            
            for(SMP_DirectDebit_GroupInvoice__c ddGrpInv : [SELECT Income_Direct_Debit__c 
                                                            FROM SMP_DirectDebit_GroupInvoice__c 
                                                            WHERE Invoice_Group__c IN: invGrpIdSet])
            {
                incDDIdSet.add(ddGrpInv.Income_Direct_Debit__c);
            }
            
            for(Income_Direct_Debit__c incDD : [SELECT DD_Status__c 
                                                FROM Income_Direct_Debit__c 
                                                WHERE DD_Status__c NOT IN ('On Hold', 'Cancelled', 'Expired')
                                                AND Id IN: incDDIdSet])
            {
                incDD.DD_Status__c = 'On Hold';
                incomeDirectDebitList.add(incDD);
            }
            
            if(!incomeDirectDebitList.isEmpty())
            {
                update incomeDirectDebitList;
            }
        }    
    }
}