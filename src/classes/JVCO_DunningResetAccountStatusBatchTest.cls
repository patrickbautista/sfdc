@isTest
public class JVCO_DunningResetAccountStatusBatchTest {
@testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim2('PRS');
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc.JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c = '180 - WORK EXHAUSTED';
        licAcc.JVCO_Incoming_Status_from_DCA_2nd_Stage__c = 'Withdrawal';
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Test.startTest();
        SBQQ.TriggerControl.disable();
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        insert JVCO_TestClassHelper.setQuoteProcess();
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;

        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        SBQQ.TriggerControl.enable();
        
        JVCO_ResetAccountStatusSettings__c DunningSettings = new JVCO_ResetAccountStatusSettings__c();
        DunningSettings.JVCO_BatchSizeInteger__c = 1;
        DunningSettings.JVCO_EmailAddress__c = 'test@gmail.com';
        DunningSettings.JVCO_Limit__c = 0;
        insert DunningSettings;
        
        Test.stopTest();
        JVCO_DCA_2nd_Stage_Automation_Controller__c dcaStatusReason = new JVCO_DCA_2nd_Stage_Automation_Controller__c();
        dcaStatusReason.Name = 'Test';
        dcaStatusReason.JVCO_Reset_Dunning_Incoming_Reason__c = '180 - WORK EXHAUSTED';
        dcaStatusReason.JVCO_Reset_Dunning_Incoming_Status__c = 'Withdrawal';
        insert dcaStatusReason;
        
    }
    @isTest
    static void testExecute()
    {
        JVCO_ResetAccountStatusSettings__c DunningSettings = [SELECT JVCO_Limit__c FROM JVCO_ResetAccountStatusSettings__c LIMIT 1];
        DunningSettings.JVCO_Limit__c = 1;
        update DunningSettings;
        
        Account acct = [Select id, ffps_custRem__Last_Reminder_Severity_Level__c,
                        ffps_custRem__Reminders_Blocked_Until_Date__c,
                        JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c,
                        JVCO_Incoming_Status_from_DCA_2nd_Stage__c,
                        ffps_accbal__Account_Balance__c,
                        JVCO_In_Enforcement__c,
                        JVCO_Exclude_Reason__c,
                        JVCO_In_Infringement__c,
                        JVCO_Credit_Status__c,
                        RecordType.Name,
                        JVCO_DCA_Type__c
                        FROM Account 
                        WHERE RecordType.Name = 'Licence Account' 
                        limit 1
                       ];
        acct.JVCO_DCA_Type__c = 'CSL';
        acct.ffps_accbal__Account_Balance__c = 0;
        acct.JVCO_Credit_Status__c = 'Debt - 1st stage';
        acct.JVCO_Exclude_Reason__c = 'Sent to DCA';
        acct.JVCO_In_Enforcement__c = FALSE;
        acct.JVCO_In_Infringement__c = FALSE;
        acct.ffps_custRem__Last_Reminder_Severity_Level__c = 3;
        acct.ffps_custRem__Reminders_Blocked_Until_Date__c = Date.today();
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        
		List<Account> acctList = new List<Account>();
		acctList.add(acct);
		Test.startTest();
		update acctList;
		JVCO_DunningResetAccountStatusSched.callExecuteMomentarily();
		Test.stopTest(); 
		System.assertEquals(true, acctList.size() != 0);

    }
    
    @isTest
    static void testSuccessAccountBalanceToReviewToFalse()
    {
        List<Account> accList = new List<Account>();
        Account acct = [Select id, ffps_custRem__Last_Reminder_Severity_Level__c,
                        ffps_custRem__Reminders_Blocked_Until_Date__c,
                        JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c,
                        JVCO_Incoming_Status_from_DCA_2nd_Stage__c,
                        ffps_accbal__Account_Balance__c,
                        JVCO_In_Enforcement__c,
                        JVCO_In_Infringement__c,
                        JVCO_Credit_Status__c,
                        RecordType.Name,
                        JVCO_DCA_Type__c,
                        JVCO_Exclude_Reason__c
                        FROM Account 
                        WHERE RecordType.Name = 'Licence Account' 
                        limit 1
                       ];
        acct.JVCO_DCA_Type__c = 'CSL';
        acct.ffps_accbal__Account_Balance__c = 31;
        acct.JVCO_Credit_Status__c = 'Pre DCA 1st Stage';
        acct.JVCO_In_Enforcement__c = FALSE;
        acct.JVCO_In_Infringement__c = FALSE;
        acct.ffps_custRem__Last_Reminder_Severity_Level__c = 3;
        acct.ffps_custRem__Reminders_Blocked_Until_Date__c = Date.today();
        acct.ffps_custRem__Balance_Needs_Review__c = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        accList.add(acct);
        Test.startTest();
        update accList;
        JVCO_DunningResetAccountStatusBatch dunning = new JVCO_DunningResetAccountStatusBatch();
        dunning.start(null);
        dunning.execute(null, accList);
        Test.stopTest();
        
        System.assertEquals(true, !acct.ffps_custRem__Balance_Needs_Review__c);
    }
    
    //Test Account Balance > 0 < 30 not in DCA - Should Reset
    @isTest
    static void testNonDCAwithBalance0to30()
    {
        List<Account> accList = new List<Account>();
        Account acct = [Select id, ffps_custRem__Last_Reminder_Severity_Level__c,
                        ffps_custRem__Reminders_Blocked_Until_Date__c,
                        JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c,
                        JVCO_Incoming_Status_from_DCA_2nd_Stage__c,
                        ffps_accbal__Account_Balance__c,
                        JVCO_In_Enforcement__c,
                        JVCO_In_Infringement__c,
                        JVCO_Credit_Status__c,
                        RecordType.Name,
                        JVCO_DCA_Type__c,
                        JVCO_Exclude_Reason__c
                        FROM Account 
                        WHERE RecordType.Name = 'Licence Account' 
                        limit 1
                       ];
        acct.ffps_accbal__Account_Balance__c = 15;
        acct.JVCO_Credit_Status__c = 'Debt - 1st stage';
        acct.JVCO_In_Enforcement__c = FALSE;
        acct.JVCO_In_Infringement__c = FALSE;
        acct.ffps_custRem__Last_Reminder_Severity_Level__c = 2;
        acct.ffps_custRem__Reminders_Blocked_Until_Date__c = Date.today();
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        accList.add(acct);
        Test.startTest();
        update accList;
		Database.executeBatch(new JVCO_DunningResetAccountStatusBatch(), 1);
        Test.stopTest();
        
        Account acc = [select Id, ffps_custRem__Last_Reminder_Severity_Level__c from Account 
                       WHERE Id = :acct.Id];
        System.assertEquals(0, acc.ffps_custRem__Last_Reminder_Severity_Level__c);
    }
    
    //Test Account balance <= 0 not in DCA - Should reset
    @isTest
    static void testNonDCAwithBalanceLessThan0()
    {
        List<Account> accList = new List<Account>();
        Account acct = [Select id, ffps_custRem__Last_Reminder_Severity_Level__c,
                        ffps_custRem__Reminders_Blocked_Until_Date__c,
                        JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c,
                        JVCO_Incoming_Status_from_DCA_2nd_Stage__c,
                        ffps_accbal__Account_Balance__c,
                        JVCO_In_Enforcement__c,
                        JVCO_In_Infringement__c,
                        JVCO_Credit_Status__c,
                        RecordType.Name,
                        JVCO_DCA_Type__c,
                        JVCO_Exclude_Reason__c
                        FROM Account 
                        WHERE RecordType.Name = 'Licence Account' 
                        limit 1
                       ];
        acct.ffps_accbal__Account_Balance__c = -1;
        acct.JVCO_Credit_Status__c = 'Debt - 1st stage';
        acct.JVCO_In_Enforcement__c = FALSE;
        acct.JVCO_In_Infringement__c = FALSE;
        acct.ffps_custRem__Last_Reminder_Severity_Level__c = 2;
        acct.ffps_custRem__Reminders_Blocked_Until_Date__c = Date.today();
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        accList.add(acct);
        Test.startTest();
        update accList;
		Database.executeBatch(new JVCO_DunningResetAccountStatusBatch(), 1);
        Test.stopTest();
        
        Account acc = [select Id, ffps_custRem__Last_Reminder_Severity_Level__c from Account 
                       WHERE Id = :acct.Id];
        System.assertEquals(0, acc.ffps_custRem__Last_Reminder_Severity_Level__c);
    }
    
    //Test Account Balance > 30 not in DCA - Should not be picked up
    @isTest
    static void testNonDCAwithBalanceOver30()
    {
        List<Account> accList = new List<Account>();
        Account acct = [Select id, ffps_custRem__Last_Reminder_Severity_Level__c,
                        ffps_custRem__Reminders_Blocked_Until_Date__c,
                        JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c,
                        JVCO_Incoming_Status_from_DCA_2nd_Stage__c,
                        ffps_accbal__Account_Balance__c,
                        JVCO_In_Enforcement__c,
                        JVCO_In_Infringement__c,
                        JVCO_Credit_Status__c,
                        RecordType.Name,
                        JVCO_DCA_Type__c,
                        JVCO_Exclude_Reason__c
                        FROM Account 
                        WHERE RecordType.Name = 'Licence Account' 
                        limit 1
                       ];
        acct.ffps_accbal__Account_Balance__c = 50;
        acct.JVCO_Credit_Status__c = 'Debt - 1st stage';
        acct.JVCO_In_Enforcement__c = FALSE;
        acct.JVCO_In_Infringement__c = FALSE;
        acct.ffps_custRem__Last_Reminder_Severity_Level__c = 2;
        acct.ffps_custRem__Reminders_Blocked_Until_Date__c = Date.today();
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        accList.add(acct);
        Test.startTest();
        update accList;
		Database.executeBatch(new JVCO_DunningResetAccountStatusBatch(), 1);
        Test.stopTest();
        
        Account acc = [select Id, ffps_custRem__Last_Reminder_Severity_Level__c from Account 
                       WHERE Id = :acct.Id];
        System.assertEquals(2, acc.ffps_custRem__Last_Reminder_Severity_Level__c);
    }
    
    //Test Account Balance > 30 in DCA - Should not be picked up
    @isTest
    static void testDCAwithBalanceOver30()
    {
        List<Account> accList = new List<Account>();
        Account acct = [Select id, ffps_custRem__Last_Reminder_Severity_Level__c,
                        ffps_custRem__Reminders_Blocked_Until_Date__c,
                        JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c,
                        JVCO_Incoming_Status_from_DCA_2nd_Stage__c,
                        ffps_accbal__Account_Balance__c,
                        JVCO_In_Enforcement__c,
                        JVCO_In_Infringement__c,
                        JVCO_Credit_Status__c,
                        RecordType.Name,
                        JVCO_DCA_Type__c,
                        JVCO_Exclude_Reason__c
                        FROM Account 
                        WHERE RecordType.Name = 'Licence Account' 
                        limit 1
                       ];
        acct.ffps_accbal__Account_Balance__c = 50;
        acct.JVCO_Credit_Status__c = 'Passed to DCA - 1st Stage';
        acct.JVCO_In_Enforcement__c = FALSE;
        acct.JVCO_In_Infringement__c = FALSE;
        acct.ffps_custRem__Last_Reminder_Severity_Level__c = 7;
        acct.ffps_custRem__Reminders_Blocked_Until_Date__c = Date.today();
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        accList.add(acct);
        Test.startTest();
        update accList;
        Database.executeBatch(new JVCO_DunningResetAccountStatusBatch(), 1);
        Test.stopTest();
        
        Account acc = [select Id, ffps_custRem__Last_Reminder_Severity_Level__c from Account 
                       WHERE Id = :acct.Id];
        System.assertEquals(7, acc.ffps_custRem__Last_Reminder_Severity_Level__c);
    }
    
    //Test Account Balance < 30 in DCA - Should not be picked up
    @isTest
    static void testDCAwithBalanceLessthan30()
    {
        List<Account> accList = new List<Account>();
        Account acct = [Select id, ffps_custRem__Last_Reminder_Severity_Level__c,
                        ffps_custRem__Reminders_Blocked_Until_Date__c,
                        JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c,
                        JVCO_Incoming_Status_from_DCA_2nd_Stage__c,
                        ffps_accbal__Account_Balance__c,
                        JVCO_In_Enforcement__c,
                        JVCO_In_Infringement__c,
                        JVCO_Credit_Status__c,
                        RecordType.Name,
                        JVCO_DCA_Type__c,
                        JVCO_Exclude_Reason__c
                        FROM Account 
                        WHERE RecordType.Name = 'Licence Account' 
                        limit 1
                       ];
        acct.ffps_accbal__Account_Balance__c = 20;
        acct.JVCO_Credit_Status__c = 'Passed to DCA - 1st Stage';
        acct.JVCO_In_Enforcement__c = FALSE;
        acct.JVCO_In_Infringement__c = FALSE;
        acct.ffps_custRem__Last_Reminder_Severity_Level__c = 7;
        acct.ffps_custRem__Reminders_Blocked_Until_Date__c = Date.today();
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        accList.add(acct);
        Test.startTest();
        update accList;
		Database.executeBatch(new JVCO_DunningResetAccountStatusBatch(), 1);
        Test.stopTest();
        
        Account acc = [select Id, ffps_custRem__Last_Reminder_Severity_Level__c from Account 
                       WHERE Id = :acct.Id];
        System.assertEquals(7, acc.ffps_custRem__Last_Reminder_Severity_Level__c);
    }
    
    //Test Account Balance <= 0 in DCA - Should be reset
    @isTest
    static void testDCAwithBalanceLessThan0()
    {
        List<Account> accList = new List<Account>();
        Account acct = [Select id, ffps_custRem__Last_Reminder_Severity_Level__c,
                        ffps_custRem__Reminders_Blocked_Until_Date__c,
                        JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c,
                        JVCO_Incoming_Status_from_DCA_2nd_Stage__c,
                        ffps_accbal__Account_Balance__c,
                        JVCO_In_Enforcement__c,
                        JVCO_In_Infringement__c,
                        JVCO_Credit_Status__c,
                        RecordType.Name,
                        JVCO_DCA_Type__c,
                        JVCO_Exclude_Reason__c
                        FROM Account 
                        WHERE RecordType.Name = 'Licence Account' 
                        limit 1
                       ];
        acct.ffps_accbal__Account_Balance__c = -1;
        acct.JVCO_Credit_Status__c = 'Passed to DCA - 1st Stage';
        acct.JVCO_In_Enforcement__c = FALSE;
        acct.JVCO_In_Infringement__c = FALSE;
        acct.ffps_custRem__Last_Reminder_Severity_Level__c = 7;
        acct.ffps_custRem__Reminders_Blocked_Until_Date__c = Date.today();
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        accList.add(acct);
        Test.startTest();
        update accList;
		Database.executeBatch(new JVCO_DunningResetAccountStatusBatch(), 1);
        Test.stopTest();
        
        Account acc = [select Id, ffps_custRem__Last_Reminder_Severity_Level__c from Account 
                       WHERE Id = :acct.Id];
        System.assertEquals(0, acc.ffps_custRem__Last_Reminder_Severity_Level__c);
    }
    
}