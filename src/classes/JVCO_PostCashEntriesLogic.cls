/* ----------------------------------------------------------------------------------------------
    Name: JVCO_PostCashEntriesLogic.cls 
    Description: Business logic class for checking the Cash Entry with Cash Entry Lines and Posting it.

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    30-Nov-2017   0.1        patrick.t.bautista  GREEN-25780 - Intial creation
    21-Dec-2017   0.2        franz.g.a.dimaapi   GREEN-27576 - Added ErrorHandler for Duplicate Posting
    08-Feb-2018   0.3        franz.g.a.dimaapi   GREEN-29949 - Added Bulk Posting
    15-Oct-2018   0.4        franz.g.a.dimaapi   GREEN-33408 - Bulk Posting is now Queueable
----------------------------------------------------------------------------------------------- */
public class JVCO_PostCashEntriesLogic
{
    private ApexPages.StandardSetController standardController;
    public c2g__codaCashEntry__c selectedCashentries { get; set; }

    public JVCO_PostCashEntriesLogic(ApexPages.StandardSetController standardController) 
    {
        this.standardController = standardController;   
    }

    public PageReference postCashEntries()
    {   
        //Checking of Cash Entry
        selectedCashentries = [SELECT Id, name, c2g__Status__c, JVCO_Posted__c 
                                FROM c2g__codaCashEntry__c 
                                WHERE Id = :ApexPages.currentPage().getParameters().get('id')];

        //Checking of Cash Entry Line item
        List<c2g__codaCashEntryLineItem__c> cshEntryLineList = [SELECT Id
                                                                FROM c2g__codaCashEntryLineItem__c 
                                                                WHERE c2g__CashEntry__c = :selectedCashentries.id]; 
        //Error Message if Cash Entry is Paid
        if(selectedCashentries.c2g__Status__c == 'Complete')
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Cash Entry Already Posted \''+selectedCashentries.Name+'\''));
            return null;
        //Error Message if Cash Entry has no Line item
        }else if(cshEntryLineList.isEmpty())
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You cannot post a Cash Entry without lines \''+selectedCashentries.Name+'\''));
            return null;
        //Batch to run
        }else if(!JVCO_BatchJobHelper.checkLargeJobs(1)){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.JVCO_BatchJobHelperCannotProcess));
            return null;
        }else
        {
            //If Posted first time
            if(!selectedCashEntries.JVCO_Posted__c)
            {
                //Stored Cash Entry for the Batch Class   
                Set<Id> cashEntryIdSet = new Set<Id>();
                cashEntryIdSet.add(selectedCashentries.id);
                Database.executeBatch(new JVCO_CashEntriesPostingLogicBatch(cashEntryIdSet),1);
                updateCashEntryPostedChkBox(selectedCashentries);
                return standardController.cancel();
            //If Already Posted
            }else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Posting Already Inprogress \''+selectedCashentries.Name+'\''));
                return null;
            }
        }    
    }

    public PageReference bulkPostCashEntries()
    {   
        final JVCO_General_Settings__c GENERAL_SETTINGS = JVCO_General_Settings__c.getInstance();
        final Decimal POST_CASH_ENTRIES_QUEUEABLE_LIMIT = GENERAL_SETTINGS != null ? GENERAL_SETTINGS.JVCO_PostCashEntriesQueueableLimit__c : 20;

        List<c2g__codaCashEntry__c> cashEntryList = (List<c2g__codaCashEntry__c>) standardController.getSelected();
        List<c2g__codaCashEntry__c> updatedCashEntryList = new List<c2g__codaCashEntry__c>();

        Integer batchNumber = 1;
        Map<Integer, List<c2g__codaCashEntry__c>> batchNumberToCashEntryListMap = new Map<Integer, List<c2g__codaCashEntry__c>>();
        for(c2g__codaCashEntry__c cashEntry : [SELECT Id FROM c2g__codaCashEntry__c
                                                WHERE c2g__Status__c = 'In Progress'
                                                AND c2g__Value__c > 0
                                                AND c2g__BankAccountValue__c > 0
                                                AND c2g__NetValue__c > 0
                                                AND JVCO_Posted__c = FALSE
                                                AND Id IN : cashEntryList])
        {
            cashEntry.JVCO_Posted__c = true;
            if(!batchNumberToCashEntryListMap.isEmpty() && batchNumberToCashEntryListMap.get(batchNumber).size() == POST_CASH_ENTRIES_QUEUEABLE_LIMIT)
            {
                batchNumber++;
            }
            if(!batchNumberToCashEntryListMap.containsKey(batchNumber))
            {
                batchNumberToCashEntryListMap.put(batchNumber, new List<c2g__codaCashEntry__c>());
            }
            batchNumberToCashEntryListMap.get(batchNumber).add(cashEntry);
            updatedCashEntryList.add(cashEntry);
        }

        if(!batchNumberToCashEntryListMap.isEmpty())
        {
            update updatedCashEntryList;
            System.enqueueJob(new JVCO_PostCashEntriesQueueable(batchNumberToCashEntryListMap, 1));   
        }
        return standardController.cancel();    
    }

    //Returns to Cash Entry by clicking Cancel button
    public PageReference returntoCashEntry()
    {
        PageReference page = new PageReference(standardController.cancel().getUrl());
        page.setRedirect(TRUE);
        return page;
    }

    private void updateCashEntryPostedChkBox(c2g__codaCashEntry__c cEntry)
    {
        cEntry.JVCO_Posted__c = true;
        update cEntry;
    }
}