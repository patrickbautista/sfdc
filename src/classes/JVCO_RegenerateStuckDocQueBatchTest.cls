@isTest
public class JVCO_RegenerateStuckDocQueBatchTest {
    @testSetup 
    static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
    
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        insert nonConvGLA;
        Test.startTest();
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2();
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim3();
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        JVCO_FFUtil.stopCodaCreditNoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountDLRSHandler = true;
        
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        
        Test.stopTest();
        
    }
    
    @isTest
    static void testStucktoRequestSubmittedToCongaNonKA()
    {   
        Test.startTest();
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        Account testLicAccount = [select id from Account where RecordTypeId = :licenceRT limit 1];
        testLicAccount .ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        Account testCusAccount = [select id from Account where RecordTypeId = :customerRT limit 1];
        Contact testContact = [SELECT ID FROM Contact limit 1];
        c2g__codaBankAccount__c bAcc = [SELECT Id FROM c2g__codaBankAccount__c order by CreatedDate DESC limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];

        
        c2g__codaAccountingCurrency__c accCurrency = [SELECT Id FROM c2g__codaAccountingCurrency__c LIMIT 1];
        c2g__codaCashEntry__c cEntry = new c2g__codaCashEntry__c();
        cEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cEntry.c2g__BankAccount__c = bAcc.Id;
        cEntry.c2g__CashEntryCurrency__c = accCurrency.Id;
        insert cEntry;
        
        c2g__codaCashEntryLineItem__c cLine = new c2g__codaCashEntryLineItem__c();
        cLine.c2g__Account__c = testCusAccount.Id;
        cLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cLine.c2g__CashEntryValue__c = 12000;
        cLine.c2g__CashEntry__c = cEntry.Id;
        insert cLine;
        
        JVCO_General_Settings__c genSet = new JVCO_General_Settings__c();
        genSet.JVCO_Allow_Partial_Payment__c = true ;
        genSet.JVCO_Bank_Statement_Queue_Rec_For_Jobs__c = 1 ;
        genSet.JVCO_Cash_Post_Queue_Rec_Per_Jobs__c = 1 ;
        genSet.JVCO_Company_Name__c = 'Company Name';
        genSet.JVCO_Currency_Code__c = 'GBP';
        genSet.JVCO_Queuable_Max_TransactionLine_Size__c = 1;
        genSet.JVCO_Background_Matching_Size__c = 1;
        insert genSet;
        
        JVCO_Document_Queue__c testDocument = new JVCO_Document_Queue__c();
        testDocument.JVCO_Recipient__c = testContact.id;
        testDocument.JVCO_Related_Account__c = testLicAccount.id;
        testDocument.JVCO_Related_Customer_Account__c = testCusAccount.id;
        testDocument.JVCO_Scenario__c = 'Manual Review Notification Transition';
        testDocument.JVCO_Transmission__c = 'Email';
        testDocument.JVCO_Generation_Status__c = 'Request Submitted To Conga';
        testDocument.CreatedDate = System.today() - 1;
        insert TestDocument;
        
        Test.stopTest();
        Database.executeBatch(new JVCO_RegenerateStuckDocQueBatch());     
    }
    
    @isTest
    static void testStucktoRequestSubmittedToCongaKAs()
    {   
        Test.startTest();
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        Account testLicAccount = [select id from Account where RecordTypeId = :licenceRT limit 1];
        testLicAccount .ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        Account testCusAccount = [select id from Account where RecordTypeId = :customerRT limit 1];
        Contact testContact = [SELECT ID FROM Contact limit 1];
        c2g__codaBankAccount__c bAcc = [SELECT Id FROM c2g__codaBankAccount__c order by CreatedDate DESC limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];

        
        c2g__codaAccountingCurrency__c accCurrency = [SELECT Id FROM c2g__codaAccountingCurrency__c LIMIT 1];
        c2g__codaCashEntry__c cEntry = new c2g__codaCashEntry__c();
        cEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cEntry.c2g__BankAccount__c = bAcc.Id;
        cEntry.c2g__CashEntryCurrency__c = accCurrency.Id;
        insert cEntry;
        
        c2g__codaCashEntryLineItem__c cLine = new c2g__codaCashEntryLineItem__c();
        cLine.c2g__Account__c = testCusAccount.Id;
        cLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cLine.c2g__CashEntryValue__c = 12000;
        cLine.c2g__CashEntry__c = cEntry.Id;
        insert cLine;
        
        JVCO_General_Settings__c genSet = new JVCO_General_Settings__c();
        genSet.JVCO_Allow_Partial_Payment__c = true ;
        genSet.JVCO_Bank_Statement_Queue_Rec_For_Jobs__c = 1 ;
        genSet.JVCO_Cash_Post_Queue_Rec_Per_Jobs__c = 1 ;
        genSet.JVCO_Company_Name__c = 'Company Name';
        genSet.JVCO_Currency_Code__c = 'GBP';
        genSet.JVCO_Queuable_Max_TransactionLine_Size__c = 1;
        genSet.JVCO_Background_Matching_Size__c = 1;
        insert genSet;
        
        JVCO_Document_Queue__c testDocument = new JVCO_Document_Queue__c();
        testDocument.JVCO_Recipient__c = testContact.id;
        testDocument.JVCO_Related_Account__c = testLicAccount.id;
        testDocument.JVCO_Related_Customer_Account__c = testCusAccount.id;
        testDocument.JVCO_Scenario__c = 'Key Account Quote Usage Summary PDF';
        testDocument.JVCO_Transmission__c = 'Email';
        testDocument.JVCO_Generation_Status__c = 'Request Submitted To Conga';
        testDocument.CreatedDate = System.today() - 1;
        insert TestDocument;
        
        Test.stopTest();
        Database.executeBatch(new JVCO_RegenerateStuckDocQueBatch());     
    }
    
    @isTest
    static void testStucktoRequestSubmittedToCongaBespoke()
    {   
        Test.startTest();
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        Account testLicAccount = [select id from Account where RecordTypeId = :licenceRT limit 1];
        testLicAccount .ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        Account testCusAccount = [select id from Account where RecordTypeId = :customerRT limit 1];
        Contact testContact = [SELECT ID FROM Contact limit 1];
        c2g__codaBankAccount__c bAcc = [SELECT Id FROM c2g__codaBankAccount__c order by CreatedDate DESC limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];

        
        c2g__codaAccountingCurrency__c accCurrency = [SELECT Id FROM c2g__codaAccountingCurrency__c LIMIT 1];
        c2g__codaCashEntry__c cEntry = new c2g__codaCashEntry__c();
        cEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cEntry.c2g__BankAccount__c = bAcc.Id;
        cEntry.c2g__CashEntryCurrency__c = accCurrency.Id;
        insert cEntry;
        
        c2g__codaCashEntryLineItem__c cLine = new c2g__codaCashEntryLineItem__c();
        cLine.c2g__Account__c = testCusAccount.Id;
        cLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cLine.c2g__CashEntryValue__c = 12000;
        cLine.c2g__CashEntry__c = cEntry.Id;
        insert cLine;
        
        JVCO_General_Settings__c genSet = new JVCO_General_Settings__c();
        genSet.JVCO_Allow_Partial_Payment__c = true ;
        genSet.JVCO_Bank_Statement_Queue_Rec_For_Jobs__c = 1 ;
        genSet.JVCO_Cash_Post_Queue_Rec_Per_Jobs__c = 1 ;
        genSet.JVCO_Company_Name__c = 'Company Name';
        genSet.JVCO_Currency_Code__c = 'GBP';
        genSet.JVCO_Queuable_Max_TransactionLine_Size__c = 1;
        genSet.JVCO_Background_Matching_Size__c = 1;
        insert genSet;
        
        JVCO_Document_Queue__c testDocument = new JVCO_Document_Queue__c();
        testDocument.JVCO_Recipient__c = testContact.id;
        testDocument.JVCO_Related_Account__c = testLicAccount.id;
        testDocument.JVCO_Related_Customer_Account__c = testCusAccount.id;
        testDocument.JVCO_Scenario__c = 'Generic Template';
        testDocument.JVCO_Transmission__c = 'Email';
        testDocument.JVCO_Generation_Status__c = 'Request Submitted To Conga';
        testDocument.JVCO_Message_Body__c = 'test';
        testDocument.JVCO_Signature__c = 'test';
        testDocument.JVCO_Email_Subject__c = 'test';
        testDocument.JVCO_Salutation__c = 'test';
        testDocument.CreatedDate = System.today() - 1;
        insert TestDocument;
        
        Test.stopTest();
        Database.executeBatch(new JVCO_RegenerateStuckDocQueBatch());     
    } 
    
    @isTest
    static void testUsageSummaryExcelSubScenario()
    {   
        Test.startTest();
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        Account testLicAccount = [select id from Account where RecordTypeId = :licenceRT limit 1];
        testLicAccount .ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        Account testCusAccount = [select id from Account where RecordTypeId = :customerRT limit 1];
        Contact testContact = [SELECT ID FROM Contact limit 1];
        c2g__codaBankAccount__c bAcc = [SELECT Id FROM c2g__codaBankAccount__c order by CreatedDate DESC limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];

        
        c2g__codaAccountingCurrency__c accCurrency = [SELECT Id FROM c2g__codaAccountingCurrency__c LIMIT 1];
        c2g__codaCashEntry__c cEntry = new c2g__codaCashEntry__c();
        cEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cEntry.c2g__BankAccount__c = bAcc.Id;
        cEntry.c2g__CashEntryCurrency__c = accCurrency.Id;
        insert cEntry;
        
        c2g__codaCashEntryLineItem__c cLine = new c2g__codaCashEntryLineItem__c();
        cLine.c2g__Account__c = testCusAccount.Id;
        cLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cLine.c2g__CashEntryValue__c = 12000;
        cLine.c2g__CashEntry__c = cEntry.Id;
        insert cLine;
        
        JVCO_General_Settings__c genSet = new JVCO_General_Settings__c();
        genSet.JVCO_Allow_Partial_Payment__c = true ;
        genSet.JVCO_Bank_Statement_Queue_Rec_For_Jobs__c = 1 ;
        genSet.JVCO_Cash_Post_Queue_Rec_Per_Jobs__c = 1 ;
        genSet.JVCO_Company_Name__c = 'Company Name';
        genSet.JVCO_Currency_Code__c = 'GBP';
        genSet.JVCO_Queuable_Max_TransactionLine_Size__c = 1;
        genSet.JVCO_Background_Matching_Size__c = 1;
        insert genSet;
        
        JVCO_Document_Queue__c testDocument = new JVCO_Document_Queue__c();
        testDocument.JVCO_Recipient__c = testContact.id;
        testDocument.JVCO_Related_Account__c = testLicAccount.id;
        testDocument.JVCO_Related_Customer_Account__c = testCusAccount.id;
        testDocument.JVCO_Scenario__c = 'Generic Template';
        testDocument.JVCO_Transmission__c = 'Email';
        testDocument.JVCO_Sub_Scenario__c = 'Surcharge Invoice;Usage Summary (Excel);Pre-Contract Invoice';
        testDocument.JVCO_Generation_Status__c = 'Request Submitted To Conga';
        testDocument.JVCO_Message_Body__c = 'test';
        testDocument.JVCO_Signature__c = 'test';
        testDocument.JVCO_Email_Subject__c = 'test';
        testDocument.JVCO_Salutation__c = 'test';
        testDocument.CreatedDate = System.today() - 1;
        insert TestDocument;
        
        Test.stopTest();
        Database.executeBatch(new JVCO_RegenerateStuckDocQueBatch());     
    }
}