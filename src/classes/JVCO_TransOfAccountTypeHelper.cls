public class JVCO_TransOfAccountTypeHelper{
	public static List<Database.SaveResult> res = new List<Database.SaveResult>();
	public static List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
	public static List<Database.DeleteResult> delRes = new List<Database.DeleteResult>();
	public static set<id> contractsToDeleteSet = new Set<id>();
	public static Map<id,String> processedRecAndAccountNAme = new Map<id,String>();
	public static Map<String, Set<id>> excludeAccount = new Map<String, Set<id>>();
	public static Map<String, Set<String>> sucessAndFail = new Map<String, Set<String>>();
	public static Set<Account> updateAccSet = new Set<Account>();
	public static Id keyDimension = (id)JVCO_Constants__c.getOrgDefaults().KeyDimension3__c;
	public static Id nonKeyDimension = (id)JVCO_Constants__c.getOrgDefaults().NonKeyDimension3__c;
	@TestVisible static Boolean failTest = false;

	public static Map<String, Set<String>> doTransformation(List<Account> accLists, Boolean testCheck){
		failTest = testCheck;

		Map<id,Account> accMap = new Map<id,Account>(accLists);
		Set<id> accToTransformSet = new Set<id>(accMap.keyset());
		
		Set<id> accCustomerToKeyTransform = new Set<id>();
		Set<id> accKeyToCustomerTransform = new Set<id>();
		Map<id, integer> contNumberOfVenueMap = new Map<id, integer>();
		Map<id, date> contStartDateMap = new Map<id, date>();
		Map<id, date> contEndDateMap = new Map<id, date>();
		Map<id,id> accContMap = new Map<id, id>();
		decimal maximumSubscriptions = (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_SubsMaxLimit__c != null ? (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_SubsMaxLimit__c : 80;
		
		


		Savepoint sp = Database.setSavepoint();

		List<SBQQ__Quote__c> quoteList = [SELECT id, SBQQ__Account__c 
										   FROM SBQQ__Quote__c 
										   WHERE SBQQ__Account__c IN: accToTransformSet AND SBQQ__Status__c = 'Draft'];

		if(!quoteList.isEmpty()){
			for(SBQQ__Quote__c q: quoteList){
				if(excludeAccount.containsKey('Quote')){
					Set<id> tmp = new Set<id>(excludeAccount.get('Quote'));
					tmp.add(q.SBQQ__Account__c);
					excludeAccount.put('Quote', tmp);
				}else{
					excludeAccount.put('Quote',new Set<id>{q.SBQQ__Account__c});
				}
			}

			for(Id recId: excludeAccount.get('Quote')){
				ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                
                errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(recId);
                errLog.ffps_custRem__Detail__c = 'Account was not included because it contains draft quotes';
                errLog.ffps_custRem__Message__c = 'JVCO_TransformationOFAccountTypeBatch';
                errLog.ffps_custRem__Grouping__c = 'Account with draft quotes';
                
				errLogList.add(errLog); 	
			}
			 
		}

		List<c2g__codaInvoice__c> salesInvoiceList = [SELECT id, c2g__Account__c 
													  FROM c2g__codaInvoice__c 
													  WHERE c2g__Account__c IN: accToTransformSet AND (c2g__PaymentStatus__c = 'Unpaid' OR c2g__PaymentStatus__c = 'Part Paid' )];

		if(!salesInvoiceList.isEmpty()){
			
			for(c2g__codaInvoice__c sI: salesInvoiceList){
				if(excludeAccount.containsKey('Invoice')){
					Set<id> tmp = new Set<id>(excludeAccount.get('Invoice'));
					tmp.add(sI.c2g__Account__c);
					excludeAccount.put('Invoice', tmp);
				}else{
					excludeAccount.put('Invoice',new Set<id>{sI.c2g__Account__c});
				}
			}

			for(Id recId: excludeAccount.get('Invoice')){
				ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                
                errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(recId);
                errLog.ffps_custRem__Detail__c = 'Account was not included because it contains Invoices that are partly paid or unpaid';
                errLog.ffps_custRem__Message__c = 'JVCO_TransformationOFAccountTypeBatch';
                errLog.ffps_custRem__Grouping__c = 'Account with unpaid or partly paid Invoice';
                
				errLogList.add(errLog); 	
			}
		}

		for(Account acc: accLists){

			if(acc.JVCO_In_Enforcement__c || acc.JVCO_In_Infringement__c){
				//excludeAccount.add(acc.id);
				if(excludeAccount.containsKey('Enforcement')){
					Set<id> tmp = new Set<id>(excludeAccount.get('Enforcement'));
					tmp.add(acc.id);
					excludeAccount.put('Enforcement', tmp);
				}else{
					excludeAccount.put('Enforcement',new Set<id>{acc.id});
				}
			}

			if(acc.JVCO_Customer_Account__r.Type == 'Agency'){
				//excludeAccount.add(acc.id);
				if(excludeAccount.containsKey('Agency')){
					Set<id> tmp = new Set<id>(excludeAccount.get('Agency'));
					tmp.add(acc.id);
					excludeAccount.put('Agency', tmp);
				}else{
					excludeAccount.put('Agency',new Set<id>{acc.id});
				}

			}

			if(acc.Transformation__c !=null){
				if(acc.Transformation__c == acc.JVCO_Customer_Account__r.JVCO_Target_Account_Type__c){

					if(excludeAccount.containsKey('Transformed')){
						Set<id> tmp = new Set<id>(excludeAccount.get('Transformed'));
						tmp.add(acc.id);
						excludeAccount.put('Transformed', tmp);
					}else{
						excludeAccount.put('Transformed',new Set<id>{acc.id});
					}

				}

			}

		}

		if(excludeAccount.containsKey('Agency')){
			accToTransformSet.removeAll(excludeAccount.get('Agency'));
			for(Id recId: excludeAccount.get('Agency')){
				ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                
                errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(recId);
                errLog.ffps_custRem__Detail__c = 'Account was not included because Customer Account is type Agency';
                errLog.ffps_custRem__Message__c = 'JVCO_TransformationOFAccountTypeBatch';
                errLog.ffps_custRem__Grouping__c = 'Agency Account';
                
				errLogList.add(errLog); 	

			}
		}
		if(excludeAccount.containsKey('Quote')){
			accToTransformSet.removeAll(excludeAccount.get('Quote'));
		}
		if(excludeAccount.containsKey('Invoice')){
			accToTransformSet.removeAll(excludeAccount.get('Invoice'));
		}
		if(excludeAccount.containsKey('Transformed')){
			accToTransformSet.removeAll(excludeAccount.get('Transformed'));
		}
		if(excludeAccount.containsKey('Enforcement')){
			accToTransformSet.removeAll(excludeAccount.get('Enforcement'));
			for(Id recId: excludeAccount.get('Enforcement')){
				ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();

                errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(recId);
                errLog.ffps_custRem__Detail__c = 'Account was not included because it is in Enforcement or Infringement';
                errLog.ffps_custRem__Message__c = 'JVCO_TransformationOFAccountTypeBatch';
                errLog.ffps_custRem__Grouping__c = 'Enforcement and Infringement Account';
                
				errLogList.add(errLog); 	

			}
		}



		for(Id accId: accToTransformSet){

			if(accMap.get(accId).JVCO_Customer_Account__r.JVCO_Target_Account_Type__c == 'Customer to Key'){
				accCustomerToKeyTransform.add(accId);
			}

			if(accMap.get(accId).JVCO_Customer_Account__r.JVCO_Target_Account_Type__c == 'Key to Customer'){
				accKeyToCustomerTransform.add(accId);
			}
			
		}

		
		if(!accCustomerToKeyTransform.isEmpty()){

			Map<id,set<id>> contNoOfVenueTmp = new Map<id,set<id>>();
            Map<id,set<id>> venueAndContractCount = new Map<id,set<id>>(); 
            List<SBQQ__Subscription__c> subsList = [SELECT id, SBQQ__Contract__c, SBQQ__Contract__r.Pricebook2Id, SBQQ__Account__c, JVCO_Venue__c, SBQQ__Account__r.JVCO_Customer_Account__c, SBQQ__Account__r.Name, SBQQ__Account__r.JVCO_Customer_Account__r.JVCO_Account_Type_Change__c FROM SBQQ__Subscription__c where SBQQ__Account__c IN: accCustomerToKeyTransform AND SBQQ__Contract__r.JVCO_Current_Period__c = TRUE AND SBQQ__Contract__r.JVCO_Renewal_Generated__c = FALSE ORDER BY Start_Date__c desc];


            for(SBQQ__Subscription__c subsRec: subsList){
                if(contNoOfVenueTmp.containsKey(subsRec.SBQQ__Contract__c)){
                    Set<id> tmpList = contNoOfVenueTmp.get(subsRec.SBQQ__Contract__c);
                    tmpList.add(subsRec.JVCO_Venue__c);
                    contNoOfVenueTmp.put(subsRec.SBQQ__Contract__c,tmpList);
                }else{
                    contNoOfVenueTmp.put(subsRec.SBQQ__Contract__c, new Set<id>{subsRec.JVCO_Venue__c});
                }

                if(venueAndContractCount.containsKey(subsRec.JVCO_Venue__c)){
                    Set<id> tmpList = venueAndContractCount.get(subsRec.JVCO_Venue__c);
                    tmpList.add(subsRec.SBQQ__Contract__c);
                    venueAndContractCount.put(subsRec.JVCO_Venue__c,tmpList);
                }else{
                    venueAndContractCount.put(subsRec.JVCO_Venue__c, new Set<id>{subsRec.SBQQ__Contract__c});
                }


            }

            if(!contNoOfVenueTmp.isEmpty()){
                for(Id idTmp: contNoOfVenueTmp.keySet()){
                    contNumberOfVenueMap.put(idTmp,contNoOfVenueTmp.get(idTmp).size());
                }
            }

			List<AggregateResult> subsContVenue = [SELECT SBQQ__Account__c accId, SBQQ__Contract__c contId, MIN(SBQQ__Contract__r.StartDate) startDate, COUNT(JVCO_Venue__c) numOfVenue, Max(SBQQ__Contract__r.Enddate) endDate FROM SBQQ__Subscription__c where SBQQ__Account__c IN: accCustomerToKeyTransform AND SBQQ__Contract__r.JVCO_Current_Period__c = TRUE AND SBQQ__Contract__r.JVCO_Renewal_Generated__c = FALSE GROUP BY SBQQ__Contract__c, SBQQ__Account__c];
			
			for(AggregateResult ar: subsContVenue){
				//contNumberOfVenueMap.put((Id)ar.get('contId'), (Integer)ar.get('numOfVenue'));
				contStartDateMap.put((Id)ar.get('contId'), (Date)ar.get('startDate'));
				contEndDateMap.put((Id)ar.get('contId'), (Date)ar.get('endDate'));
				accContMap.put((Id)ar.get('accId'),(Id)ar.get('contId'));
			}

			
			customerToKeyTransform(accCustomerToKeyTransform,subsList,accMap, contNumberOfVenueMap, contStartDateMap, contEndDateMap, accContMap);


		}


		if(!accKeyToCustomerTransform.isEmpty()){
			Map<id, List<id>> accAndCont = new Map<id,List<id>>();
			Map<id,Set<Date>> accAndStartDate = new Map<id,Set<Date>>();
			Map<id,Set<Date>> accAndEndDate = new Map<id,Set<Date>>();
			Map<id,integer> contNoOfSubsMap = new Map<id,Integer>();
			Map<id,integer> accountNoOfSubsMap = new Map<id, integer>();
			Map<id,Contract> accountAndNewContractMap = new Map<id, Contract>();
			boolean typeoftransfrom = false;
			Set<id> accountsWithUnalignedDates = new Set<id>();

			for(Contract cont: [SELECT id, StartDate, EndDate, AccountId FROM Contract where AccountId IN: accKeyToCustomerTransform AND JVCO_Current_Period__c = TRUE AND JVCO_Renewal_Generated__c = FALSE]){

				if(accAndCont.containsKey(cont.AccountId)){
					List<id> tmp = accAndCont.get(cont.AccountId);
					tmp.add(cont.id);
				}else{
					accAndCont.put(cont.AccountId,new List<id>{cont.Id});
				}

				if(accAndStartDate.containsKey(cont.AccountId)){
					set<Date> tmp = accAndStartDate.get(cont.AccountId);
					tmp.add(cont.StartDate);
				}else{
					accAndStartDate.put(cont.AccountId, new Set<Date>{cont.startDate});
				}

				if(accAndEndDate.containsKey(cont.AccountId)){
					set<Date> tmp = accAndEndDate.get(cont.AccountId);
					tmp.add(cont.EndDate);
				}else{
					accAndEndDate.put(cont.AccountId, new Set<Date>{cont.EndDate});
				}

				if(accAndStartDate.get(cont.AccountId).size()>1 || accAndEndDate.get(cont.AccountId).size()>1){
					accountsWithUnalignedDates.add(cont.AccountId);
				}

				//insert login to remove accounts whose contracts are not aligned
			}

			

			

			List<SBQQ__Subscription__c> subsList = [SELECT id, SBQQ__Contract__c, SBQQ__Contract__r.Pricebook2Id, SBQQ__Account__c, JVCO_Venue__c, SBQQ__Account__r.JVCO_Customer_Account__c, SBQQ__Account__r.Name FROM SBQQ__Subscription__c where SBQQ__Account__c IN: accKeyToCustomerTransform AND SBQQ__Contract__r.JVCO_Current_Period__c = TRUE AND SBQQ__Contract__r.JVCO_Renewal_Generated__c = FALSE ORDER BY Start_Date__c desc];

			

			for(AggregateResult ar: [SELECT count(id) numOfSubs, JVCO_Venue__c venueId,  SBQQ__Account__c accId FROM SBQQ__Subscription__c where SBQQ__Account__c IN: accKeyToCustomerTransform AND SBQQ__Contract__r.JVCO_Current_Period__c = TRUE AND SBQQ__Contract__r.JVCO_Renewal_Generated__c = FALSE GROUP BY JVCO_Venue__c, SBQQ__Account__c]){
				contNoOfSubsMap.put((Id)ar.get('venueId'), (Integer)ar.get('numOfSubs'));
			}

			for(AggregateResult ar:  [SELECT count(Id) numOfSubs, SBQQ__Account__c accId FROM SBQQ__Subscription__c where SBQQ__Account__c IN: accKeyToCustomerTransform AND SBQQ__Contract__r.JVCO_Current_Period__c = TRUE AND SBQQ__Contract__r.JVCO_Renewal_Generated__c = FALSE GROUP BY SBQQ__Account__c ]){
				accountNoOfSubsMap.put((id)ar.get('accId'),(integer)ar.get('numOfSubs'));

			}

			//logic as to how many contracts are created per LA
			
			for(Id accId: accKeyToCustomerTransform){

				if(accountNoOfSubsMap.get(accId)<=maximumSubscriptions && accAndCont.get(accId).size()>1 && !accountsWithUnalignedDates.contains(accId)){
					Contract cont = new Contract();
					cont = createContract(accId, subsList, accMap, null, null, null, typeoftransfrom);
					List<Date> startDateList = new List<Date>(accAndStartDate.get(accId));
					startDateList.sort();
					cont.StartDate = startDateList.get(0);
					List<Date> endDateList = new List<Date>(accAndEndDate.get(accId));
					endDateList.sort();
					cont.EndDate = endDateList.get(0);

					accountAndNewContractMap.put(accId, cont);
				}


				if(accountNoOfSubsMap.get(accId)<=maximumSubscriptions && accAndCont.get(accId).size() ==1 && !accountsWithUnalignedDates.contains(accId)){
					Account acc = new Account();
					acc.id = accMap.get(accId).JVCO_Customer_Account__c;
					acc.JVCO_Account_Type_Change__c = system.today();
					acc.Type = 'Customer';
					updateAccSet.add(acc);

					accMap.get(accId).Type = 'Customer';
                    accMap.get(accId).c2g__CODADimension3__c = nonKeyDimension;
                    accMap.get(accId).Transformation__c = 'Key to Customer';
                    updateAccSet.add(accMap.get(accId));
					
				}

				//accMap.get(accId).Type = 'Customer';
			    //accMap.get(accId).c2g__CODADimension3__c = nonKeyDimension;
			    //accMap.get(accId).Transformation__c = 'Key to Customer';
			    //updateAccSet.add(accMap.get(accId));

				//if(accountNoOfSubsMap.get(accId)>maximumSubscriptions && accAndCont.get(accId).size()>1){
				//	decimal numberOfContractsToCreate = Math.ceil(accountNoOfSubsMap.get(accId)/maximumSubscriptions);
				//}
			}

			if(!accountAndNewContractMap.isEmpty()){
				res.addall(database.insert(accountAndNewContractMap.values(),false));

			}

			for(SBQQ__Subscription__c subs: subsList){
				if(accountAndNewContractMap.containsKey(subs.SBQQ__Account__c) && !accountsWithUnalignedDates.contains(subs.SBQQ__Account__c)){
					contractsToDeleteSet.add(subs.SBQQ__Contract__c);
					subs.SBQQ__Contract__c = accountAndNewContractMap.get(subs.SBQQ__Account__c).id;
					processedRecAndAccountNAme.put(subs.id, subs.SBQQ__Account__r.Name );
					Account acc = new Account();
					acc.id = subs.SBQQ__Account__r.JVCO_Customer_Account__c;
					acc.JVCO_Account_Type_Change__c = system.today();
					acc.Type = 'Customer';

					updateAccSet.add(acc);
					accMap.get(subs.SBQQ__Account__c).Type = 'Customer';
					accMap.get(subs.SBQQ__Account__c).c2g__CODADimension3__c = nonKeyDimension;
					accMap.get(subs.SBQQ__Account__c).Transformation__c = 'Key to Customer';
					updateAccSet.add(accMap.get(subs.SBQQ__Account__c));
				}
			}

			if(!subsList.isEmpty()){
				res.addAll(database.update(subsList, false));
			}

						
		}

		if(!contractsToDeleteSet.isEmpty()){
			List<Contract> contractsToDeleteList = [SELECT id from Contract where id IN: contractsToDeleteSet];
			delRes.addAll(database.delete(contractsToDeleteList, false));
		}


		if(!updateAccSet.isEmpty()){

			res.addAll(database.update(new list<Account>(updateAccSet), false));
		}

		for(Database.DeleteResult dRes: delRes){
			if(!dRes.isSuccess()){
				database.rollback(sp);
				for(Database.Error objErr:dRes.getErrors())
				{	
                    ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                    errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                    errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(dRes.getId());
                    errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                    errLog.ffps_custRem__Message__c = 'JVCO_TransformationOFAccountTypeBatch_DeleteContracts';
                    errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                    errLogList.add(errLog);
                }
			}

		}

		if(Test.isRunningTest() && failTest){

				Contract contTest = new Contract();
				res.addAll(database.insert(new List<Contract>{contTest}, false));

		}

		for(Database.SaveResult sRes: res){
			
			if(!sRes.isSuccess()){
				database.rollback(sp);
				for(Database.Error objErr:sRes.getErrors())
				{
                    ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                    errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                    errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(sRes.getId());
                    errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                    errLog.ffps_custRem__Message__c = 'JVCO_TransformationOFAccountTypeBatch_DeleteContracts';
                    errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
					errLogList.add(errLog);
				}

				if(sucessAndFail.containsKey('Fail')){
					if(processedRecAndAccountNAme.containsKey(sRes.getId())){
						Set<String> tmp = new Set<String>(sucessAndFail.get('Fail'));
						tmp.add(processedRecAndAccountNAme.get(sRes.getId()));
						sucessAndFail.put('Fail', tmp);
					}
					if(accMap.containsKey(sRes.getId())){
						Set<String> tmp = new Set<String>(sucessAndFail.get('Fail'));
						tmp.add(accMap.get(sRes.getId()).Name);
						sucessAndFail.put('Fail', tmp);
					}
				}else{
					if(processedRecAndAccountNAme.containsKey(sRes.getId())){
						sucessAndFail.put('Fail',new Set<String>{processedRecAndAccountNAme.get(sRes.getId())});
					}
					if(accMap.containsKey(sRes.getId())){
						sucessAndFail.put('Fail',new Set<String>{accMap.get(sRes.getId()).Name});
					}
				}

			}else{

				if(sucessAndFail.containsKey('Success')){
					if(processedRecAndAccountNAme.containsKey(sRes.getId())){
						Set<String> tmp = new Set<String>(sucessAndFail.get('Success'));
						tmp.add(processedRecAndAccountNAme.get(sRes.getId()));
						sucessAndFail.put('Success', tmp);
					}
					if(accMap.containsKey(sRes.getId())){
						Set<String> tmp = new Set<String>(sucessAndFail.get('Success'));
						tmp.add(accMap.get(sRes.getId()).Name);
						sucessAndFail.put('Success', tmp);
					}
				}else{
					if(processedRecAndAccountNAme.containsKey(sRes.getId())){
						sucessAndFail.put('Success',new Set<String>{processedRecAndAccountNAme.get(sRes.getId())});
					}
					if(accMap.containsKey(sRes.getId())){
						sucessAndFail.put('Success',new Set<String>{accMap.get(sRes.getId()).Name});
					}
					
				}
			}

		} 

		if(!errLogList.isEmpty()){
		
			try
			{
				insert errLogList;
			}
			catch(Exception ex)
			{
				System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());

			}
            
		}

		return sucessAndFail;
	}

	public static void customerToKeyTransform(Set<id> accCustomerToKeyTransform,List<SBQQ__Subscription__c> subsList, Map<id,Account> accMap, Map<id, integer> contNumberOfVenueMap, Map<id, date> contStartDateMap, Map<id, date> contEndDateMap, Map<id,id> accContMap){
		Map<id, Set<id>> accAndVenueMap = new Map<Id,Set<id>>();
		Map<id, Set<Id>> accAndContractwithSubsNoVenueMap = new Map<id, Set<id>>();
		Map<id, Contract> venueAndContract = new Map<id, Contract>();
		List<SBQQ__Subscription__c> updateSubsList = new List<SBQQ__Subscription__c>();
		
		List<Contract> contractRecordToInsertList = new List<Contract>();
		boolean typeoftransfrom = true;
		//integer contractForNoAffiliation = 0;




		for(SBQQ__Subscription__c subs: subsList){
			
			if(subs.JVCO_Venue__c!=null && contNumberOfVenueMap.containsKey(subs.SBQQ__Contract__c)){

				if(contNumberOfVenueMap.get(subs.SBQQ__Contract__c) > 1){

					if(accAndVenueMap.containsKey(subs.SBQQ__Account__c)){
						Set<id> tmpList = accAndVenueMap.get(subs.SBQQ__Account__c);
						tmpList.add(subs.JVCO_Venue__c);
						accAndVenueMap.put(subs.SBQQ__Account__c,tmpList);
					}else{
						accAndVenueMap.put(subs.SBQQ__Account__c, new Set<id>{subs.JVCO_Venue__c});
					}
				}

			}else{
				if(!accAndContractwithSubsNoVenueMap.containsKey(subs.SBQQ__Account__c)){
					accAndContractwithSubsNoVenueMap.put(subs.SBQQ__Account__c,new Set<id>{subs.SBQQ__Contract__c});
				}else{
					Set<id> tmpList = accAndContractwithSubsNoVenueMap.get(subs.SBQQ__Account__c);
					tmpList.add(subs.SBQQ__Contract__c);
					accAndContractwithSubsNoVenueMap.put(subs.SBQQ__Account__c,tmpList);
				}
			}	
		}

		for(id accId: accCustomerToKeyTransform){

			if(accAndVenueMap.containsKey(accId)){
				for(id venueId:accAndVenueMap.get(accId)){
					venueAndContract.put(venueId,createContract(accId, subsList, accMap, contStartDateMap, contEndDateMap, accContMap, typeoftransfrom));
				}				
			}

			if(!accAndContractwithSubsNoVenueMap.isEmpty()){
				if(accAndContractwithSubsNoVenueMap.containsKey(accId)){
					if(accAndContractwithSubsNoVenueMap.get(accId).size()>1){
						venueAndContract.put(accId,createContract(accId, subsList, accMap, contStartDateMap, contEndDateMap, accContMap, typeoftransfrom));
					}
				}
			}

		}

			
		

		if(!venueAndContract.isEmpty()){
			
			res.addAll(database.insert(venueAndContract.values(),false));
		}

		for(SBQQ__Subscription__c subs: subsList){
			if(contNumberOfVenueMap.containsKey(subs.SBQQ__Contract__c)){
				if(venueAndContract.containsKey(subs.JVCO_Venue__c)){
					contractsToDeleteSet.add(subs.SBQQ__Contract__c);
					subs.SBQQ__Contract__c = venueAndContract.get(subs.JVCO_Venue__c).id;
					updateSubsList.add(subs);
					processedRecAndAccountNAme.put(subs.id, subs.SBQQ__Account__r.Name );
					Account acc = new Account();
					acc.id = subs.SBQQ__Account__r.JVCO_Customer_Account__c;
					acc.JVCO_Account_Type_Change__c = system.today();
					acc.Type = 'Key Account';
					updateAccSet.add(acc);
					accMap.get(subs.SBQQ__Account__c).Type = 'Key Account';
					accMap.get(subs.SBQQ__Account__c).c2g__CODADimension3__c = keyDimension;
					accMap.get(subs.SBQQ__Account__c).Transformation__c = 'Customer to Key';
					updateAccSet.add(accMap.get(subs.SBQQ__Account__c));
					
				}
			}
			if(venueAndContract.containsKey(subs.SBQQ__Account__c)){
				contractsToDeleteSet.add(subs.SBQQ__Contract__c);
				processedRecAndAccountNAme.put(subs.id, subs.SBQQ__Account__r.Name );
				subs.SBQQ__Contract__c = venueAndContract.get(subs.SBQQ__Account__c).id;
				updateSubsList.add(subs);
				Account acc = new Account();
				acc.id = subs.SBQQ__Account__r.JVCO_Customer_Account__c;
				acc.JVCO_Account_Type_Change__c = system.today();
				acc.Type = 'Key Account';
				updateAccSet.add(acc);
				accMap.get(subs.SBQQ__Account__c).Type = 'Key Account';
				accMap.get(subs.SBQQ__Account__c).c2g__CODADimension3__c = keyDimension;
				accMap.get(subs.SBQQ__Account__c).Transformation__c = 'Customer to Key';
				updateAccSet.add(accMap.get(subs.SBQQ__Account__c));
			}else{

				processedRecAndAccountNAme.put(subs.id, subs.SBQQ__Account__r.Name );
				system.debug(' contNumberOfVenueMap' +  contNumberOfVenueMap);
				system.debug('processedRecord 2'+ processedRecAndAccountNAme);
				Account acc = new Account();
				acc.id = subs.SBQQ__Account__r.JVCO_Customer_Account__c;
				acc.JVCO_Account_Type_Change__c = system.today();
				acc.Type = 'Key Account';
				
				updateAccSet.add(acc);
				accMap.get(subs.SBQQ__Account__c).Type = 'Key Account';
				accMap.get(subs.SBQQ__Account__c).c2g__CODADimension3__c = keyDimension;
				accMap.get(subs.SBQQ__Account__c).Transformation__c = 'Customer to Key';
				updateAccSet.add(accMap.get(subs.SBQQ__Account__c));

			}
			
		}



		if(!updateSubsList.isEmpty()){
			res.addAll(database.update(updateSubsList, false));
		}

		
		
	}

	public static Contract createContract(Id accId, List<SBQQ__Subscription__c> subsList, Map<id, Account> accMap, Map<id, date> contStartDateMap, Map<id, date> contEndDateMap, Map<id,id> accContMap, boolean typeoftransfrom){
		
		Id standardPriceBook = (Id)JVCO_Constants__c.getOrgDefaults().JVCO_StandardPricebookId__c;

		Contract contRec = new Contract();
		contRec.AccountId = accId;
		contRec.JVCO_Cust_Account__c = accMap.get(accId).JVCO_Customer_Account__c;
		contRec.Status = 'Draft';
		contRec.Pricebook2Id = standardPriceBook;
		contRec.JVCO_Review_Type__c = accMap.get(accId).JVCO_Review_Type__c;
		if(typeoftransfrom){
			contRec.StartDate = contStartDateMap.get(accContMap.get(accId));
			contRec.EndDate = contEndDateMap.get(accContMap.get(accId));
		}
		
		return contRec;
	}

	public static void sendBeginEmail(Integer toProcess){

		//Send Begin Job Email
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(UserInfo.getUserId());
        mail.setSaveAsActivity(false);

        mail.setReplyTo('no-reply@salesforce.com');
        mail.setSenderDisplayName('Transformation Batch');
        mail.setSubject('Transformation Batch Started');

        String emailMsg = '<html><body>This is to inform you that the process for Account Transformation Batch has started ' + System.now() + ' for ' + toProcess +' customer accounts.';

        emailMsg +=' <body></html>';

        mail.setHTMLbody(emailMsg);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}

	public static void sendCompletionEmail(String success, String fail){

		//Send Complete Job Email
		
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(UserInfo.getUserId());
        mail.setSaveAsActivity(false);

        mail.setReplyTo('no-reply@salesforce.com');
        mail.setSenderDisplayName('Transformation Batch');
        mail.setSubject('Transformation Batch Started');

       
    	String emailMsg = '<html><body>This is to inform you that the process for Account Transformation Batch has completed ' + System.now()+'.';


    	if(success != ''){
    		emailMsg +='Following record have been successfully transformed: '+success;
    	}
    

    	if(fail != ''){

    	 emailMsg += '<br> Following record have not been transformed: '+fail;
    	}
      
        


        emailMsg +=' <body></html>';

        mail.setHTMLbody(emailMsg);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
}