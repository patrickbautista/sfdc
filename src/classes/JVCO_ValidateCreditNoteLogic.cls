/* ----------------------------------------------------------------------------------------------
    Name: JVCO_ValidateCreditNoteLogic.cls 
    Description: Business logic class for checking the Sales Invoice's contract whether it is Renewed or Migrated

    Date          Version     Author                           Summary of Changes 
    -----------   -------     -----------------                -----------------------------------------
    06-Oct-2017   0.1         jasper.j.figueroa                Intial creation - fix for GREEN-22873
    08-Nov-2017   0.2         jasper.j.figueroa                Added Surcharge and Pre-Contract - fix for GREEN-25533
    04-Dec-2017   0.3         jasper.j.figueroa                Allowed Migrated Surcharge to be Converted to Credit Note - fix for GREEN-26549
    06-Dec-2017   0.4         jasper.j.figueroa                Changes undone - fix for GREEN-26549
    03-May-2018   0.5         filip.bezak@accenture.com        GREEN-31699 - [UAT]: Error in Converting a credit note
    04-Jun-2018   0.6         john.patrick.valdez              GREEN-32178 - Added an eror message if the Surcharge is converted to a Credit Note
    12-Jun-2018   0.7         franz.g.a.dimaapi                GREEN-32321 - Refactor class and fix migrated issue
    07-Aug-2018   0.8         franz.g.a.dimaapi                GREEN-32724 - Removed Amendment Checking
    09-Sep-2018   0.9         franz.g.a.dimaapi                GREEN-33406 - Update Renewal Checking Query
    25-Oct-2018   1.0         franz.g.a.dimaapi                GREEN-33757 - Fixed Surcharge Cancellation
    24-Jun-2019   1.1         franz.g.a.dimaapi                GREEN-34717 - Refactor and Add Enforcement Checker
    17-Jul-2019   1.2         franz.g.a.dimaapi                GREEN-34683 - Removed checking for amendment before renew
    29-Jan-2020   1.3         patrick.t.bautista               GREEN-35351 - Removed ! at checkIfTheAmendmentOrderIsIncluded return value
    11-May-2020   1.4         patrick.t.bautista               GREEN-35592 - SmarterPay validation, Error with Active IDD
----------------------------------------------------------------------------------------------- */
public class JVCO_ValidateCreditNoteLogic
{
    private ApexPages.StandardController stdCtrl;
    private c2g__codaInvoice__c invoice;
    private User u;
    // Constants used for page messages
    private String INVALID_INVOICE_ERR1 = 'You cannot convert this Sales Invoice to a Credit Note because:';
    private String INVALID_INVOICE_ERR2 = '1. You already have a Surcharge Credit Note, kindly post and match it.';
    private String INVALID_INVOICE_ERR3 = '2. Original Sales Invoice was already cancelled';
    public c2g__codaInvoice__c sInv {get;set;}
    public Boolean isValid {get;set;}
    
    public JVCO_ValidateCreditNoteLogic(ApexPages.StandardController stdCtrl)
    {
        sInv = new c2g__codaInvoice__c();
        this.stdCtrl = stdCtrl;
        invoice = [SELECT id, Name, c2g__Account__c FROM c2g__codaInvoice__c WHERE Id =:stdCtrl.getRecord().Id];
        u = [SELECT Profile.Name, 
             (SELECT PermissionSet.Name 
              FROM PermissionSetAssignments 
              WHERE PermissionSet.Name = 'JVCO_Enforcement_Advisors')
             FROM User 
             WHERE Id = :UserInfo.getUserId()];
        isValid = true;
    }
    
    public PageReference init()
    {
        //Check if there's a valid IDD related to Invoice, if yes throw an error
        Boolean withActiveIDD = checkIfHasValidIDD();
        
        List<Order> orderList = [SELECT Id,
                                 JVCO_Invoice_Type__c,
                                 SBQQ__Quote__r.SBQQ__MasterContract__c,
                                 CreatedDate,
                                 JVCO_Sales_Invoice__r.JVCO_Invoice_Legacy_Number__c,
                                 JVCO_Sales_Invoice__r.CreatedDate,
                                 JVCO_Sales_Invoice__r.JVCO_Original_Invoice__r.JVCO_From_Convert_to_Credit__c,
                                 JVCO_Sales_Invoice__r.JVCO_Invoice_Type__c,
                                 Account.JVCO_No_of_Venues__c, 
                                 Account.JVCO_Customer_Account__r.JVCO_Account_Type_Change__c,
                                 Account.JVCO_In_Infringement__c,
                                 Account.JVCO_In_Enforcement__c,
                                 Account.JVCO_Injunction__c
                                 FROM Order
                                 WHERE JVCO_Sales_Invoice__c = :invoice.Id
                                 AND JVCO_Manual_Invoice__c = false];
        if(!orderList.isEmpty())
        {
            Boolean isSurcharge = false;
            Boolean isOriginalCancelled = false;
            Set<Id> amendmentQuoteIdSet = new Set<Id>();
            Map<Id, DateTime> masterContractToOrderCreatedDateMap = new Map<Id, DateTime>();

            for(Order ord : orderList)
            {
                //Check if Invoice Type equals Credit Cancellation
                if(ord.JVCO_Invoice_Type__c == 'Credit Cancellation' || ord.JVCO_Sales_Invoice__r.JVCO_Invoice_Type__c == 'Credit Cancellation')
                {
                    isValid = false;
                    break;
                //Check if Migrated
                }else if(ord.JVCO_Sales_Invoice__r.JVCO_Invoice_Legacy_Number__c != null ||
                    (ord.Account.JVCO_No_of_Venues__c > 1 && 
                        ord.JVCO_Sales_Invoice__r.CreatedDate.date() < ord.Account.JVCO_Customer_Account__r.JVCO_Account_Type_Change__c))
                {
                    isValid = false;
                    break;
                //Check if In Enforcement, Infringement, Injunction
                }else if((u.Profile.Name != 'System Administrator' && u.PermissionSetAssignments.isEmpty()) &&
                            (ord.Account.JVCO_In_Infringement__c || ord.Account.JVCO_In_Enforcement__c || ord.Account.JVCO_Injunction__c))
                {
                    isValid = false;
                    break;
                //Checks if it's a Surcharge
                }else if(ord.JVCO_Invoice_Type__c == 'Surcharge' && ord.JVCO_Sales_Invoice__r.JVCO_Original_Invoice__c != null)
                {
                    isSurcharge = true;
                    isOriginalCancelled = ord.JVCO_Sales_Invoice__r.JVCO_Original_Invoice__r.JVCO_From_Convert_to_Credit__c;
                    break;
                }else if(ord.SBQQ__Quote__c != null)
                {
                    if(ord.SBQQ__Quote__r.SBQQ__MasterContract__c != null)
                    {
                        amendmentQuoteIdSet.add(ord.SBQQ__Quote__c);
                        //Assuming 1 amendment order per Contract
                        masterContractToOrderCreatedDateMap.put(ord.SBQQ__Quote__r.SBQQ__MasterContract__c, ord.CreatedDate);
                    }
                }
            }
            
            if(isSurcharge)
            {
                //Check for Existing Surcharge Credit Note
                List<c2g__codaCreditNote__c> surchargeCredNoteList = [SELECT Id
                                                                        FROM c2g__codaCreditNote__c 
                                                                        WHERE c2g__Invoice__c = :invoice.Id
                                                                        AND c2g__Invoice__r.JVCO_Cancelled__c = TRUE
                                                                        AND c2g__CreditNoteStatus__c = 'Complete'
                                                                        AND c2g__Invoice__c != NULL
                                                                        AND c2g__OutstandingValue__c != 0];
                if(!surchargeCredNoteList.isEmpty() || isOriginalCancelled)
                {
                    isValid = false;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                               INVALID_INVOICE_ERR1 + '</p>&emsp;' +
                                                               INVALID_INVOICE_ERR2 + '</p>&emsp;' +
                                                               INVALID_INVOICE_ERR3));
                    return null;
                }
            }

            if(isValid)
            {
                isValid = isValid && checkForExistingCreditNote();
                //Check if the Amendment Order is included
                isValid = isValid && checkIfTheAmendmentOrderIsIncluded(amendmentQuoteIdSet);
                //Check if the Contract was renewed
                isValid = isValid && checkIfTheContractWasRenewed();
                //Check if it's the latest Amendment Order
                isValid = isValid && checkIfLatestAmendmentOrder(masterContractToOrderCreatedDateMap);
            }

            if(!isValid)
            {
                returnErrorMessage(withActiveIDD);
                return null;
            }
            return null;
        }
        //If Manual Sales Invoice or No Billing Invoice/s
        return proceedToConvert();
    }
    
    public Boolean checkIfHasValidIDD(){
        Set<Id> incomeDDSet = new Set<Id>();
        Boolean withActiveIDD = false;
        for(SMP_DirectDebit_GroupInvoice__c incomeIDDGroup : [SELECT Income_Direct_Debit__c,
                                                              Invoice_Group__c
                                                              FROM SMP_DirectDebit_GroupInvoice__c   
                                                              WHERE Invoice_Group__c 
                                                              IN (SELECT JVCO_CC_Invoice_Group__c 
                                                                  FROM JVCO_CC_Outstanding_Mapping__c
                                                                  WHERE JVCO_Sales_Invoice_Name__c =: invoice.Name
                                                                 )
                                                              AND Invoice_Group__c != null
                                                              AND Income_Direct_Debit__c != null])
        {
            incomeDDSet.add(incomeIDDGroup.Income_Direct_Debit__c);
        }
        List<Income_Direct_Debit__c> incomeDDList = [SELECT DD_Status__c
                                                     FROM Income_Direct_Debit__c
                                                     WHERE DD_Status__c NOT IN ('Cancelled', 'Cancelled by Originator', 'Cancelled by Payer', 'Expired')
                                                     AND Account__c =: invoice.c2g__Account__c
                                                     AND Id IN : incomeDDSet];
        if(!incomeDDList.isEmpty()){
            isValid = false;
            withActiveIDD = true;
        }
        return withActiveIDD;
    }
    
    public PageReference submitCreditReason()
    {
        updateSInvConvertFlag(true);
        return proceedToConvert();
    }

    public PageReference proceedToConvert()
    {
        PageReference page = new PageReference('/apex/c2g__codaconverttocreditnote?id=' + ApexPages.currentPage().getParameters().get('id'));
        return page;
    }
    
    private Boolean checkForExistingCreditNote()
    {
        List<c2g__codaCreditNote__c> cNoteList = [SELECT Id FROM c2g__codaCreditNote__c
                                                    WHERE c2g__Invoice__c = :invoice.Id
                                                    AND c2g__CreditNoteStatus__c NOT IN ('Discarded')];

        return cNoteList.isEmpty();
    }

    private Boolean checkIfTheAmendmentOrderIsIncluded(Set<Id> amendmentQuoteIdSet)
    {
        Map<Id, SBQQ__Quote__c> amendmentQuoetMap = new Map<Id, SBQQ__Quote__c>([
                                                            SELECT Id FROM SBQQ__Quote__c
                                                            WHERE SBQQ__MasterContract__r.SBQQ__Quote__r.JVCO_Sales_Invoice__c = :invoice.Id
                                                            AND SBQQ__Opportunity2__r.SBQQ__Ordered__c = true]);

        return amendmentQuoetMap.isEmpty() || amendmentQuoteIdSet.containsAll(amendmentQuoetMap.keySet());
    }

    private Boolean checkIfTheContractWasRenewed()
    {
        List<Contract> contractList = [SELECT Id
                                       FROM Contract
                                       WHERE SBQQ__Order__r.JVCO_Sales_Invoice__c = :invoice.Id
                                       AND SBQQ__RenewalOpportunity__r.SBQQ__RenewedContract__c != NULL];
        return contractList.isEmpty();
    }

    private Boolean checkIfLatestAmendmentOrder(Map<Id, DateTime> masterContractToOrderCreatedDateMap)
    {
        for(AggregateResult ar : [SELECT SBQQ__Quote__r.SBQQ__MasterContract__c masterContractId,
                                    MAX(CreatedDate) maxOrderCreatedDate
                                    FROM Order
                                    WHERE SBQQ__Quote__r.SBQQ__MasterContract__c IN : masterContractToOrderCreatedDateMap.keySet()
                                    GROUP BY SBQQ__Quote__r.SBQQ__MasterContract__c])
        {
            DateTime orderCreatedDate = masterContractToOrderCreatedDateMap.get((Id)ar.get('masterContractId'));
            if(orderCreatedDate != (DateTime)ar.get('maxOrderCreatedDate'))
            {
                return false;
            }
        }

        return true;
    }

    private void updateSInvConvertFlag(Boolean isValid)
    {
        sInv.Id = invoice.Id;
        sInv.JVCO_From_Convert_to_Credit__c = isValid;
        update sInv;
    }

    private void returnErrorMessage(boolean withActiveIDD)
    {
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, withActiveIDD ? Label.JVCO_CancelInvoiceWithActiveIDD 
                                                   : Label.JVCO_Convert_To_CreditNote_Error_Message));
    }

    //Returns to the Sales Invoice after clicking the 'Back' button
    public PageReference returnToInvoice()
    {
        PageReference page = new PageReference(stdCtrl.cancel().getUrl());
        page.setRedirect(true);
        return page;
    }
}