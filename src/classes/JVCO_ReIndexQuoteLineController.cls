/* ----------------------------------------------------------------------------------------------------------
    Name:            JVCO_ReIndexQuoteLineController.cls 
    Description:     Controller for JVCO_ReIndexQuoteLine.page
    Test class:      JVCO_ReIndexQuoteLineControllerTest.cls 

    Date             Version     Author                            Summary of Changes 
    -----------      -------     -----------------                 -------------------------------------------
    19-Jul-2018      0.1         Accenture-reymark.j.l.arlos        Initial version of the code GREEN-32595
    02-Aug-2018      0.2         Accenture-reymark.j.l.arlos        Updated to complete the functionality
------------------------------------------------------------------------------------------------------------*/
public class JVCO_ReIndexQuoteLineController {

    public class reIndexQuoteLineWrapper
    {
        public String quoteLineGroupId {get; set;}
        public String quoteLineGroupName {get; set;}
        public Integer numberCtr {get; set;}
        public String quoteLineId {get; set;}
        public String productName {get; set;}
        public String productCode {get; set;}
        public String requiredbyId {get; set;}
        public String requiredByProductName {get; set;}
    }

    
    public SBQQ__Quote__c quoteRec {get; set;}
    public String quoteNumber {get; set;}
    public List<reIndexQuoteLineWrapper> lstQuoteLineController {get; set;}
    

    //public Boolean renderNoQuoteLines {get; set;}
    public Boolean renderReIndex {get; set;}
    public Boolean renderTable {get; set;}
    public Boolean processingReIndex {get; set;}
    //start 02-Aug-2018 reymark.j.l.arlos
    public Boolean isPromoter {get; set;}
    public List<SBQQ__QuoteLine__c> quoteLineToUpdateList {get; set;}
    //end 02-Aug-2018 reymark.j.l.arlos

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public JVCO_ReIndexQuoteLineController(ApexPages.StandardController stdController) {
        this.quoteRec = (SBQQ__Quote__c)stdController.getRecord();

        lstQuoteLineController = new List<reIndexQuoteLineWrapper>();

        quoteRec = [SELECT Id, Name, SBQQ__Account__r.Type
                        FROM SBQQ__Quote__c
                        WHERE Id =: quoteRec.Id
                        ];

        quoteNumber = quoteRec.Name;

        List<SBQQ__QuoteLine__c> quoteLineList = [SELECT Id, SBQQ__Group__c, SBQQ__Group__r.Name, SBQQ__Number__c, SBQQ__ProductName__c, SBQQ__ProductCode__c, SBQQ__RequiredBy__c, SBQQ__RequiredBy__r.SBQQ__ProductName__c
                                                        FROM SBQQ__QuoteLine__c
                                                        WHERE SBQQ__Quote__c =: quoteRec.Id
                                                        ORDER BY SBQQ__Number__c ASC
                                                        ];

        if(!quoteLineList.isEmpty() && quoteLineList.size() > 1)
        {
            //renderNoQuoteLines = false;
            renderReIndex = true;
            renderTable = true;
            processingReIndex = false;
            for(SBQQ__QuoteLine__c qLineRec : quoteLineList)
            {
                reIndexQuoteLineWrapper qLineWrap = new reIndexQuoteLineWrapper();
                qLineWrap.quoteLineGroupId = qLineRec.SBQQ__Group__c;
                qLineWrap.quoteLineGroupName = qLineRec.SBQQ__Group__r.Name;
                qLineWrap.numberCtr = Integer.valueOf(qLineRec.SBQQ__Number__c);
                qLineWrap.quoteLineId = qLineRec.Id;
                qLineWrap.productName = qLineRec.SBQQ__ProductName__c;
                qLineWrap.productCode = qLineRec.SBQQ__ProductCode__c;
                qLineWrap.requiredById = qLineRec.SBQQ__RequiredBy__c;
                qLineWrap.requiredByProductName = qLineRec.SBQQ__RequiredBy__r.SBQQ__ProductName__c;

                lstQuoteLineController.add(qLineWrap);
            }
        }
        else
        {
            //renderNoQuoteLines = true;
            renderReIndex = false;
            renderTable = false;
            processingReIndex = false;

            if(quoteLineList.size() == 1)
            {
                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.WARNING, 'Only 1 Quote Line record available for ' + quoteNumber) );
            }
            else
            {
                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.WARNING, 'No available Quote Line records for ' + quoteNumber) );
            }
            
            
        }
    }

    /* -----------------------------------------------------------------------------------------------------------------------
        Author:         reymark.j.l.arlos
        Company:        Accenture
        Description:    Method that handles the numbering arrangement of the quote lines once the Re-Index button is clicked
        <Date>          <Authors Name>                      <Brief Description of Change> 
        19-Jul-2018      Accenture-reymark.j.l.arlos        Initial version of the code GREEN-32595
        02-Aug-2018      Accenture-reymark.j.l.arlos        Updated to complete the functionality
    ------------------------------------------------------------------------------------------------ ------------------------*/
    public PageReference reIndex()
    {
        processingReIndex = true;
        renderTable = false;

        //start 02-Aug-2018 reymark.j.l.arlos
        if(quoteRec.SBQQ__Account__r.Type == 'Promoter')
        {
            isPromoter = true;
        }
        else 
        {
            isPromoter = false;
        }

        Map<String, List<SBQQ__QuoteLine__c>> childQuoteLineRequiredByMap = new Map<String, List<SBQQ__QuoteLine__c>>();
        Map<Id, Map<String, List<SBQQ__QuoteLine__c>>> parentQuoteLineVenueMap = new Map<Id, Map<String, List<SBQQ__QuoteLine__c>>>();
        Map<String, List<SBQQ__QuoteLine__c>> parentQuoteLineLstMap = new Map<String, List<SBQQ__QuoteLine__c>>();
        Integer numberCtr = 0;
        quoteLineToUpdateList = new List<SBQQ__QuoteLine__c>();

        List<SBQQ__QuoteLine__c> quoteLineLst = [Select Id, SBQQ__ProductCode__c, SBQQ__Quote__c, SBQQ__RequiredBy__c, JVCO_Venue__c, SBQQ__Number__c
                                                    From SBQQ__QuoteLine__c 
                                                    where SBQQ__Quote__c =: quoteRec.Id];

        if(!isPromoter)
        {
            Map<String, Set<String>> venueTariffCodes = new Map<String, Set<String>>();

            if(!quoteLineLst.isEmpty())
            {
                for(SBQQ__QuoteLine__c quoteLineRec : quoteLineLst)
                {
                    if(!string.isBlank(quoteLineRec.SBQQ__RequiredBy__c))
                    {
                        if(!childQuoteLineRequiredByMap.containsKey(quoteLineRec.SBQQ__RequiredBy__c))
                        {
                            List<SBQQ__QuoteLine__c> childQuoteLineTempList = new List<SBQQ__QuoteLine__c>();
                            childQuoteLineTempList.add(quoteLineRec);
                            childQuoteLineRequiredByMap.put(quoteLineRec.SBQQ__RequiredBy__c, childQuoteLineTempList);

                        }
                        else
                        {
                            childQuoteLineRequiredByMap.get(quoteLineRec.SBQQ__RequiredBy__c).add(quoteLineRec);
                        }
                    }
                    else
                    {
                        if(!parentQuoteLineVenueMap.containsKey(quoteLineRec.JVCO_Venue__c))
                        {
                            Map<String, List<SBQQ__QuoteLine__c>> parentQuoteLineTempMap = new Map<String, List<SBQQ__QuoteLine__c>>();
                            List<SBQQ__QuoteLine__c> quoteLineTempList = new List<SBQQ__QuoteLine__c>();
                            quoteLineTempList.add(quoteLineRec);
                            parentQuoteLineTempMap.put(quoteLineRec.SBQQ__ProductCode__c, quoteLineTempList);
                            parentQuoteLineVenueMap.put(quoteLineRec.JVCO_Venue__c, parentQuoteLineTempMap);
                        }
                        else
                        {
                            if(parentQuoteLineVenueMap.get(quoteLineRec.JVCO_Venue__c).containsKey(quoteLineRec.SBQQ__ProductCode__c)) 
                            {
                                parentQuoteLineVenueMap.get(quoteLineRec.JVCO_Venue__c).get(quoteLineRec.SBQQ__ProductCode__c).add(quoteLineRec);
                            }
                            else
                            {
                                List<SBQQ__QuoteLine__c> quoteLineTempList = new List<SBQQ__QuoteLine__c>();
                                quoteLineTempList.add(quoteLineRec);
                                parentQuoteLineVenueMap.get(quoteLineRec.JVCO_Venue__c).put(quoteLineRec.SBQQ__ProductCode__c, quoteLineTempList);
                            }
                        }

                        if(!venueTariffCodes.containsKey(quoteLineRec.JVCO_Venue__c))
                        {
                            venueTariffCodes.put(quoteLineRec.JVCO_Venue__c, new Set<String>());
                        }

                        if(venueTariffCodes.containsKey(quoteLineRec.JVCO_Venue__c))
                        {
                            if(!venueTariffCodes.get(quoteLineRec.JVCO_Venue__c).contains(quoteLineRec.SBQQ__ProductCode__c))
                            {
                                venueTariffCodes.get(quoteLineRec.JVCO_Venue__c).add(quoteLineRec.SBQQ__ProductCode__c);
                            }
                        }
                    }
                }
            }

            List<SBQQ__QuoteLineGroup__c> quoteLineGroupLst = [Select Id, JVCO_Venue__c
                                                                    From SBQQ__QuoteLineGroup__c
                                                                    Where SBQQ__Quote__c = : quoteRec.Id
                                                                    Order By SBQQ__Number__c ASC];

            Set<Id> venueIds = new Set<Id>();

            if(!quoteLineGroupLst.isEmpty())
            {
                for(SBQQ__QuoteLineGroup__c quoteLineGroupRec : quoteLineGroupLst)
                {
                    venueIds.add(quoteLineGroupRec.JVCO_Venue__c);
                }
            }

            if(!venueIds.isEmpty())
            {
                for(Id venId : venueIds)
                {
                    if(parentQuoteLineVenueMap.containsKey(venId) && venueTariffCodes.containsKey(venId))
                    {
                        for(String tariffCode : venueTariffCodes.get(venId))
                        {
                            tariffCode = tariffCode.replace('%', '');
                            system.debug('@RJ tariff code ' + tariffCode);
                            if(parentQuoteLineVenueMap.get(venId).containskey(tariffCode))
                            {
                                for(SBQQ__QuoteLine__c quoteLineRec : parentQuoteLineVenueMap.get(venId).get(tariffCode))
                                {
                                    numberCtr++;
                                    quoteLineRec.SBQQ__Number__c = numberCtr;
                                    quoteLineToUpdateList.add(quoteLineRec);
                                    system.debug('@@RJ parent quote line: ' + quoteLineRec);
                                    numberCtr = setQuoteLineNumber(quoteLineToUpdateList, quoteLineRec, childQuoteLineRequiredByMap, numberCtr);
                                }
                            }
                        }
                    }
                }
            }


        }
        else
        {
            Set<String> tariffCodes = new Set<String>();
            if(!quoteLineLst.isEmpty())
            {
                for(SBQQ__QuoteLine__c quoteLineRec : quoteLineLst)
                {
                    if(!string.isBlank(quoteLineRec.SBQQ__RequiredBy__c))
                    {
                        if(!childQuoteLineRequiredByMap.containsKey(quoteLineRec.SBQQ__RequiredBy__c))
                        {
                            List<SBQQ__QuoteLine__c> childQuoteLineTempList = new List<SBQQ__QuoteLine__c>();
                            childQuoteLineTempList.add(quoteLineRec);
                            childQuoteLineRequiredByMap.put(quoteLineRec.SBQQ__RequiredBy__c, childQuoteLineTempList);

                        }
                        else
                        {
                            childQuoteLineRequiredByMap.get(quoteLineRec.SBQQ__RequiredBy__c).add(quoteLineRec);
                        }
                    }
                    else
                    {
                        if(parentQuoteLineLstMap.containsKey(quoteLineRec.SBQQ__ProductCode__c))
                        {
                            parentQuoteLineLstMap.get(quoteLineRec.SBQQ__ProductCode__c).add(quoteLineRec);
                        }
                        else
                        {
                            List<SBQQ__QuoteLine__c> quoteLineTempList = new List<SBQQ__QuoteLine__c>(); 
                            quoteLineTempList.add(quoteLineRec);
                            parentQuoteLineLstMap.put(quoteLineRec.SBQQ__ProductCode__c, quoteLineTempList); 
                        }

                        if(!tariffCodes.contains(quoteLineRec.SBQQ__ProductCode__c))
                        {
                            tariffCodes.add(quoteLineRec.SBQQ__ProductCode__c);
                        }
                    }
                }
            }
            
            if(!tariffCodes.isEmpty() && !parentQuoteLineLstMap.isEmpty())
            {
                system.debug('parentQuoteLinesLstMap: ' + parentQuoteLineLstMap);
                for(String tariffCode : tariffCodes)
                {
                    tariffCode = tariffCode.replace('%', '');
                    system.debug('@RJ tariff code ' + tariffCode);
                    if(parentQuoteLineLstMap.containsKey(tariffCode))
                    {
                        for(SBQQ__QuoteLine__c quoteLineRec : parentQuoteLineLstMap.get(tariffCode))
                        {
                            numberCtr++;
                            quoteLineRec.SBQQ__Number__c = numberCtr;
                            quoteLineToUpdateList.add(quoteLineRec);
                            system.debug('@@RJ parent quote line: ' + quoteLineRec);
                            numberCtr = setQuoteLineNumber(quoteLineToUpdateList, quoteLineRec, childQuoteLineRequiredByMap, numberCtr);
                        }
                    }
                }
            }

        }

        if(!quoteLineToUpdateList.isEmpty())
        {
            Boolean isSuccess = true;
            for(SBQQ__QuoteLine__c qlRec : quoteLineToUpdateList)
            {
                system.debug('@@RJ update ql' + qlRec);
            }
            List<Database.SaveResult> updateResults =  database.update(quoteLineToUpdateList,false);

            for(Database.SaveResult updateResult: updateResults)
            {
                if(!updateResult.isSuccess())
                {
                    isSuccess = false;
                    break;
                }
            }

            if(isSuccess)
            {
                lstQuoteLineController.clear();
                List<SBQQ__QuoteLine__c> successQuoteLineList = [SELECT Id, SBQQ__Group__c, SBQQ__Group__r.Name, SBQQ__Number__c, SBQQ__ProductName__c, SBQQ__ProductCode__c, SBQQ__RequiredBy__c, SBQQ__RequiredBy__r.SBQQ__ProductName__c
                                                        FROM SBQQ__QuoteLine__c
                                                        WHERE SBQQ__Quote__c =: quoteRec.Id
                                                        ORDER BY SBQQ__Number__c ASC
                                                        ];

                for(SBQQ__QuoteLine__c qLineRec : successQuoteLineList)
                {
                    reIndexQuoteLineWrapper qLineWrap = new reIndexQuoteLineWrapper();
                    qLineWrap.quoteLineGroupId = qLineRec.SBQQ__Group__c;
                    qLineWrap.quoteLineGroupName = qLineRec.SBQQ__Group__r.Name;
                    qLineWrap.numberCtr = Integer.valueOf(qLineRec.SBQQ__Number__c);
                    qLineWrap.quoteLineId = qLineRec.Id;
                    qLineWrap.productName = qLineRec.SBQQ__ProductName__c;
                    qLineWrap.productCode = qLineRec.SBQQ__ProductCode__c;
                    qLineWrap.requiredById = qLineRec.SBQQ__RequiredBy__c;
                    qLineWrap.requiredByProductName = qLineRec.SBQQ__RequiredBy__r.SBQQ__ProductName__c;

                    lstQuoteLineController.add(qLineWrap);
                }

                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.CONFIRM, 'Re-Indexing of Quote Lines is successful. <br/>Please see below the new arrangement of Quote Lines.') );
                renderReIndex = false;
                processingReIndex = false;
                renderTable = true;
            }
            else
            {
                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, 'Re-Indexing of Quote Lines is failed.') );
                renderReIndex = false;
                processingReIndex = false;
                renderTable = true;
            }
        }
        //end 02-Aug-2018 reymark.j.l.arlos

        return null;
    }

    /* ----------------------------------------------------------------------------------------------------------------------
        Author:         reymark.j.l.arlos
        Company:        Accenture
        Description:    Method that handles the numbering arreangement of the child quote lines based on thier parent product 
        <Date>          <Authors Name>                      <Brief Description of Change> 
        02-Aug-2018     reymark.j.l.arlos                   Initial version of the code GREEN-32595
    ------------------------------------------------------------------------------------------------------------------------*/
    public static Integer setQuoteLineNumber(List<SBQQ__QuoteLine__c> quoteLineUpdateList, SBQQ__QuoteLine__c parentQuoteLine, Map<String, List<SBQQ__QuoteLine__c>> childQuoteLineRequiredByMap, Integer numberCtr)
    {
        if(!childQuoteLineRequiredByMap.isEmpty())
        {   
            if(childQuoteLineRequiredByMap.containsKey(parentQuoteLine.Id))
            {   
                List<SBQQ__QuoteLine__c> childQuoteLines = new List<SBQQ__QuoteLine__c>();
                childQuoteLines = childQuoteLineRequiredByMap.get(parentQuoteLine.Id);
                if(!childQuoteLines.isEmpty())
                {
                    for(SBQQ__QuoteLine__c qlRec : childQuoteLines)
                    {
                        numberCtr++;
                        qlRec.SBQQ__Number__c = numberCtr;
                        quoteLineUpdateList.add(qlRec);
                        numberCtr = setQuoteLineNumber(quoteLineUpdateList, qlRec, childQuoteLineRequiredByMap, numberCtr);
                    }
                }
            }
        }
        return numberCtr;
    }

    /* -----------------------------------------------------------------------------------------------------------------------
        Author:         reymark.j.l.arlos
        Company:        Accenture
        Description:    Method that redirect to Quote page
        <Date>          <Authors Name>                      <Brief Description of Change> 
        19-Jul-2018      Accenture-reymark.j.l.arlos        Initial version of the code GREEN-32595
    ------------------------------------------------------------------------------------------------ ------------------------*/
    public PageReference returnToQuote() {
    
        PageReference orderPage = new PageReference('/' + quoteRec.id);
        return orderPage;
      
    }
}