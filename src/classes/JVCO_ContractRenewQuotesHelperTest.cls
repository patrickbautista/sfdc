/* ----------------------------------------------------------------------------------------------
    Name: JVCO_ContractRenewQuotesHelperTest
    Description: Test Class for JVCO_ContractRenewQuotesHelper

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    08-Aug-2018     0.1         Jules Pablo         Intial creation
----------------------------------------------------------------------------------------------- */
@IsTest
private class JVCO_ContractRenewQuotesHelperTest 
{
    
    @testSetup static void setupTestData() 
    {
        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        insert dt;
        
        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;
        
        Product2 prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;

        Opportunity o = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        Test.startTest();
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Renewal';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = acc2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        q.JVCO_Number_Quote_Lines__c = 1;
        insert q;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        qL.SPVAvailability__c = true;
        insert ql;
        
        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();           
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, venueRecord.id);  
        Insert  affilRecord;    
        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;
        Test.stopTest();

        JVCO_Renewal_Settings__c rqcs = new JVCO_Renewal_Settings__c();
        rqcs.Name = 'Automatic Quote Completion';
        rqcs.days__c = 28;
        insert rqcs;

        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'ErrorNoSPV', Value__c = 'You can not contract the Quote since there are Quote Line(s) w/o SPV'));
        insert settings;
    }

    private static testMethod void testMethodContactRenewQuotesExecute() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Test.startTest();
        List<Opportunity> opportunityList = Database.query(JVCO_ContractRenewQuotesHelper.getQueryString(accountRecord));

        String errorMsg = JVCO_ContractRenewQuotesHelper.executeContractRenewalQuotes(opportunityList, accountRecord);
        
        List<Opportunity> updatedOppList = [SELECT Id, SBQQ__Contracted__c FROM Opportunity];
        //System.assertEquals(TRUE, updatedOppList[0].SBQQ__Contracted__c);
        JVCO_ContractRenewQuotesHelper.sendCompletionEmail(accountRecord, 'test@test.com', 1, 0, 'null', 1);
        Test.stopTest();
    }

    private static testMethod void testMethodContactRenewQuotesExecuteError() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Test.startTest();
        JVCO_ContractRenewQuotesHelper.testError = true;
        List<Opportunity> opportunityList = Database.query(JVCO_ContractRenewQuotesHelper.getQueryString(accountRecord));
    
        String errorMsg = JVCO_ContractRenewQuotesHelper.executeContractRenewalQuotes(opportunityList, accountRecord);
        
        List<Opportunity> updatedOppList = [SELECT Id, SBQQ__Contracted__c FROM Opportunity];
        System.assertEquals(FALSE, updatedOppList[0].SBQQ__Contracted__c);
        Test.stopTest();
    }

    private static testMethod void testMethodErrorSPV() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        SBQQ__Quote__c q = [select Id, SBQQ__Account__c from SBQQ__Quote__c where SBQQ__Account__c =: accountRecord.ID];
        SBQQ__QuoteLine__c qL = [select Id,SPVAvailability__c,ParentProduct__c from SBQQ__QuoteLine__c where SBQQ__Quote__c =: q.ID];
        qL.SPVAvailability__c = false;
        qL.ParentProduct__c = false;

        update qL;

        Test.startTest();
        List<Opportunity> opportunityList = Database.query(JVCO_ContractRenewQuotesHelper.getQueryString(accountRecord));
        
        String errorMsg = JVCO_ContractRenewQuotesHelper.executeContractRenewalQuotes(opportunityList, accountRecord);

        List<Opportunity> updatedOppList = [SELECT Id, SBQQ__Contracted__c FROM Opportunity];
        System.assertEquals(1, updatedOppList.size());  
        Test.stopTest();
    }

    private static testMethod void testMethodContactRenewSendBeginJobEmail() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Test.startTest();
        //List<Opportunity> opportunityList = Database.query(JVCO_ContractReviewQuotesHelper.getQueryString(accountRecord));

        //String errorMsg = JVCO_ContractReviewQuotesHelper.executeContractAmendmentQuotes(opportunityList);
        
        //List<Opportunity> updatedOppList = [SELECT Id, SBQQ__Contracted__c FROM Opportunity ];
        //System.assertEquals(TRUE, updatedOppList[0].SBQQ__Contracted__c);
        JVCO_ContractRenewQuotesHelper.sendBeginJobEmail(accountRecord, 1, true);
        Test.stopTest();
    }
}