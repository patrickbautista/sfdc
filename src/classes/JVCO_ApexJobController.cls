/*
 * Author: Accenture Inc.
 * Date Created: 25 June 2018
 * Description: Controller Class to monitor Apex Jobs triggered from Contract or Opportunity by Renewal of Quote
 *
 * -----------      -------     -----------------                 -------------------------------------------
  26-Jul-2018         1.1         mel.andrei.b.santos             GREEN-32500 - Changed error message for Renewed Msg
  
*/
public class JVCO_ApexJobController 
{
    private static final String [] ApexClassName = new String []{'JVCO_RenewalContext'};
    private static final String [] ApexJobStatus = new String []{'Completed', 'Failed'};
    //Start 26-Jul-2018 mel.andrei.b.santos
    //public static final String RenewedMsg = 'You are refrain to perform this process due to your contract has already been renewed. ';
    public static final String RenewedMsg = 'Error: You are not able to continue because a contract has already been renewed.  ';
    //END
    public static final String RenewedInProgMsg = 'You are refrain to perform this process due to another Renewal Contract Job is in progress.';
    public static List<AsyncApexJob> lstApexJobs {get;set;}
    
    public JVCO_ApexJobController(ApexPages.StandardController stdController)
    {
        sObject ObjRec = stdController.getRecord();
        system.debug(ObjRec.Id);
        if(ObjRec.getsObjectType() == Contract.sObjectType)
        {
            Contract AcctRec = [SELECT LastModifiedById FROM Contract WHERE Id =: ObjRec.Id];
            queryListOfApexJobs(new List<Id>{AcctRec.LastModifiedById}, new String[]{Null}, ApexClassName);
        } else if(ObjRec.getsObjectType() == Opportunity.sObjectType){
            Opportunity OppoRec = [SELECT LastModifiedById FROM Opportunity WHERE Id =: ObjRec.Id];
            queryListOfApexJobs(new List<Id>{OppoRec.LastModifiedById}, new String[]{Null}, ApexClassName);
        }
    }
    
    public static void retrieveJobs()
    {
        queryListOfApexJobs(new List<Id>{userinfo.getuserid()},ApexJobStatus, ApexClassName);
    }
    
    public static List<String> retrieveJobs(List<Contract> lstContract)
    {
        List<String> lstContractName = new List<String>();
        for(Contract cont :[SELECT LastModifiedById, Name, SBQQ__RenewalQuoted__c, ContractNumber FROM Contract WHERE Id IN: lstContract])
        {
            if(cont.SBQQ__RenewalQuoted__c)
                lstContractName.add(cont.ContractNumber);
        }
        
        return lstContractName;
    }
    /*
    public static void retrieveJobs(sObject ObjRec)
    {
        if(ObjRec.getsObjectType() == Contract.sObjectType)
        {
            Contract AcctRec = [SELECT LastModifiedById FROM Contract WHERE Id =: ObjRec.Id];
            //queryListOfApexJobs(new List<Id>{AcctRec.LastModifiedById}, new List<String>{'Completed', 'Failed'});
            queryListOfApexJobs(new List<Id>{AcctRec.LastModifiedById}, ApexJobStatus, ApexClassName);
        } else if(ObjRec.getsObjectType() == Opportunity.sObjectType){
            Opportunity OppoRec = [SELECT LastModifiedById FROM Opportunity WHERE Id =: ObjRec.Id];
            //queryListOfApexJobs(new List<Id>{OppoRec.LastModifiedById}, new List<String>{'Completed', 'Failed'});
            queryListOfApexJobs(new List<Id>{OppoRec.LastModifiedById}, ApexJobStatus, ApexClassName);
        }
    }
    
    public static void queryListOfApexJobs(List<Id> lstOfIds, List<String> StatusVar)
    {
            lstApexJobs  = new List<AsyncApexJob>([SELECT Id, ApexClass.Name, NumberOfErrors, ParentJobId, CreatedDate,JobType, LastProcessed, MethodName, Status, CompletedDate, CreatedBy.Name FROM AsyncApexJob WHERE CreatedById IN: lstOfIds AND  CreatedDate = TODAY AND Status NOT IN: StatusVar ORDER BY CreatedDate DESC]);
    }
    */
    public static void queryListOfApexJobs(List<Id> lstOfIds, String[] StatusVar, String[] ApexClassName)
    {
            lstApexJobs  = new List<AsyncApexJob>([SELECT Id, ApexClass.Name, NumberOfErrors, ParentJobId, CreatedDate,JobType, LastProcessed, MethodName, Status, CompletedDate, CreatedBy.Name FROM AsyncApexJob WHERE ApexClass.Name IN: ApexClassName AND CreatedById IN: lstOfIds AND  CreatedDate = TODAY AND Status NOT IN: StatusVar ORDER BY CreatedDate DESC]);
    }
}