/* ----------------------------------------------------------------------------------------------
   Name: JVCO_FixSubsAttributeValues
   Description: Batch that sets attribute values based on actual value of fields in subscription line

   Date         Version     Author               Summary of Changes 
   -----------  -------     -----------------    -----------------------------------------
  24-March-2018   0.1      Robert John B. Lacatan   Intial creation
  ----------------------------------------------------------------------------------------------- */
global class JVCO_FixSubsAttributeValues implements Database.Batchable<sObject> {


    public Set<Id> multiSubId = new Set<Id>();
    global JVCO_FixSubsAttributeValues() {
       
    }
    global JVCO_FixSubsAttributeValues(Set<Id> lSubId) {
        multiSubId = lSubId;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
      JVCO_FixProRateValuesCustomSetting__c settings = JVCO_FixProRateValuesCustomSetting__c.getValues('FixProrate');
      String OwnerId = settings.DataMigId__c;

      DescribeSObjectResult describeResult = SBQQ__Subscription__c.getSObjectType().getDescribe();

      List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );  
      String query = 'SELECT ' +  String.join( fieldNames, ',' ) + ', SBQQ__Product__r.ProductCode, ' + 'JVCO_Event__r.JVCO_Event_Name__c, '+ 'JVCO_Event__r.JVCO_Room_Names__c' + ' FROM ' + describeResult.getName() + ' WHERE ';
      query += 'CreatedById = \'' + OwnerId + '\'';
      
      if(!multiSubId.isEmpty()){
        query += ' AND ';

        integer num = 0;
        for(Id i: multiSubId){
          if(num == 0){
            query+= '(id = \'' + i + '\'';
            num++;
          }
          else{
            query += ' OR id = \'' + i + '\'';
          }

        }

        query += ')';    
        
      }


      return Database.getQueryLocator(query);
       
    }

    global void execute(Database.BatchableContext BC, List<SBQQ__Subscription__c> uSubs)
    {
       JVCO_FixSubsAttributeValuesCycle1.correctSub(uSUbs);
    }
    global void finish(Database.BatchableContext BC)
    {
        
    }
    
}