@isTest
private class JVCO_DM_UpdateOriginalInvoiceTest
{
    @testSetup static void createTestData() 
    {
        List<RecordType> rtypesCENLIH = [SELECT Name, Id FROM RecordType WHERE sObjectType='JVCO_CashEntryLineItemHistory__c' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> RecordTypesCENLIH  = new Map<String,String>{};
        for(RecordType rt: rtypesCENLIH)
        {
          RecordTypesCENLIH.put(rt.Name,rt.Id);
        }
        List<CashEntryLineItemHistory_CS__c> settingsCENLIH = new List<CashEntryLineItemHistory_CS__c>();
        settingsCENLIH.add(new CashEntryLineItemHistory_CS__c(Name = 'Matched', Value__c = RecordTypesCENLIH.get('Matched Cash')));
        settingsCENLIH.add(new CashEntryLineItemHistory_CS__c(Name = 'Reversed', Value__c = RecordTypesCENLIH.get('Reversed Cash')));
        insert settingsCENLIH;
        
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        //Settings
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_codaCreditNoteTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        insert dt;
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        c2g__codaDimension2__c dim2 = JVCO_TestClassHelper.setDim2('PPL');
        insert dim2;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        JVCO_SLA_Constants__c cs = JVCO_TestClassObjectBuilder.createSLAConstants();
        insert cs;

        Entitlement e = JVCO_TestClassObjectBuilder.createEntitlement(licAcc.id);
        insert e;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        
        Test.startTest();
        List<c2g__codaInvoice__c> sInvList = new List<c2g__codaInvoice__c>();
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        sInv.JVCO_Original_Invoice__c = null;
        sInvList.add(sInv);
        c2g__codaInvoice__c sInv2 = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        sInv2.JVCO_Original_Invoice__c = sInv.id;
        sInvList.add(sInv2);
        insert sInvList;
        
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv2.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);
        
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        List<c2g.CODAAPICommon.Reference> refList = new List<c2g.CODAAPICommon.Reference>();
            Double totalAmount = 0.0;
            c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = sInv.Id;
            refList.add(reference);
        c2g.CODAAPISalesInvoice_10_0.BulkPostInvoice(context, refList);
        
        //insert invoice1
        blng__Invoice__c invoice1 = new blng__Invoice__c();
        invoice1.blng__Account__c = licAcc.id;
        invoice1.JVCO_Sales_Invoice__c = sInv2.id;
        invoice1.JVCO_Original_Invoice__r = null;
        insert invoice1;
        
        //insert invoice2
        blng__Invoice__c invoice2 = new blng__Invoice__c();
        invoice2.blng__Account__c = licAcc.id;
        invoice2.JVCO_Sales_Invoice__c = sInv.id;
        invoice2.JVCO_Original_Invoice__c = invoice1.id;
        insert invoice2;
    }

    @isTest
    static void itShould()
    {
         JVCO_DM_UpdateOriginalInvoice b = new JVCO_DM_UpdateOriginalInvoice();
         Database.executeBatch(b);
    }
}