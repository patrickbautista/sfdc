/* --------------------------------------------------------------------------------------------------
    Name:            JVCO_RecalculateHistoricAndCurrentValues.cls 
    Description:     Class that handles recalculation of Historic and Current values on Account
    Test class:    JVCO_CalcHistoricCurrentValuesBatchTest.cls 

    Date                 Version                Author                                      Summary of Changes 
    -----------          -------     ---------------------------             -------------------------------------------
    15-Jun-2018           0.1        Accenture-reymark.j.l.arlos             Intial draft
    22-Jun-2018           0.2        Accenture-reymark.j.l.arlos             Added logic so it can calculate if the Merge Account is customer account
    05-Jul-2018           0.3        Accenture-reymark.j.l.arlos             Updated to calculate current values using migrated records if there are no latest non migrated contract
    11-Jul-2018           0.4        Accenture-reymark.j.l.arlos             Moved to new method (calculateHistoricAndCurrentValues) the calculation logic of historic and current values so it can be call by other class that is using the same logic
    30-Jul-2018           0.5        Accenture-reymark.j.l.arlos             Updated so the values can be roll up even if after merge process the values is same GREEN-32851
    19-Sep-2018           0.6        Accenture-reymark.j.l.arlos             Updated based on the new requirements in GREEN-33362
    21-Nov-2018           0.7        Accenture-reymark.j.l.arlos             Updated to include 1 time data update when the calculation logic is updated GREEN-34018
    28-Dec-2018           0.8        Accenture-reymark.j.l.arlos             Updated the query condition of current values GREEN-34171
    05-Apr-2019           0.9        rhys.j.c.dela.cruz                      GREEN-34465 - Adjusted code to accomodate Past Year Values
------------------------------------------------------------------------------------------------ */
global class JVCO_CalcHistoricCurrentValuesBatch implements Database.Batchable<sObject> {
    
    public Id accntId;
    public Set<Id> accntIdSet = new Set<Id>();
    private static final Id licAccRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    private static final Id custAccRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
    //start reymark.j.l.arlos 21-11-2018 GREEN-34018
    private static final JVCO_HistoricCurrentValuesSetting__c customSet = JVCO_HistoricCurrentValuesSetting__c.getOrgDefaults();
    //end reymark.j.l.arlos GREEN-34018
    
    global JVCO_CalcHistoricCurrentValuesBatch () 
    {   
    }

    global JVCO_CalcHistoricCurrentValuesBatch (Id accId) 
    {
        accntId = accId;    
    }

    global JVCO_CalcHistoricCurrentValuesBatch (Set<Id> accIdSet)
    {
        accntIdSet = accIdSet;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query;

        query = 'Select Id, JVCO_Review_Type__c, MasterRecordId, RecordTypeId ';
        query += 'From Account ';

        //start reymark.j.l.arlos 21-11-2018 GREEN-34018
        if(customSet.JVCO_EnableInitialDataUpdate__c)
        {
            query += 'Where JVCO_StockValueInitialUpdate__c = false ';
            query += 'AND RecordType.Name = \'' + customset.JVCO_AccountRecordTypeName__c + '\' Limit 50000';
        }
        //end reymark.j.l.arlos GREEN-34018
        else if(!string.isBlank(String.valueOf(accntId)))
        {
            query += 'Where Id = \''+accntId+'\' ';
        }
        else if(!accntIdSet.isEmpty())
        {
            String strIds = '';
            for(Id i : accntIdSet)
            {
                strIds += '\'' + i + '\',';
            }

            strIds = strIds.substring(0, strIds.length() - 1);

            query += 'Where Id in ('+strIds+') ';
        }
        else
        {
            query += 'Where isDeleted = true ';
            query += 'AND LastModifiedDate >= LAST_N_DAYS:' + Integer.valueof(customSet.JVCO_DaysCovered__c) +  ' ';
            query += 'AND LastModifiedDate <= TODAY ';
            query += 'ALL ROWS';
        }

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> accountList)
    {   
        system.debug('Execute');
        if(accountList.size() > 0)
        {
            Set<Id> historicLicAccIdSet = new Set<Id>();
            Set<Id> currentLicAccIdSet = new Set<Id>();
            Set<Id> pastYearAccIdSet = new Set<Id>();
            //start 22-Jun-2018 reymark.j.l.arlos Added logic so it can calculate if the Merge Account is customer account
            Set<Id> custAccIdSet = new Set<Id>();
            //end 22-Jun-2018 reymark.j.l.arlos
            Map<Id, Account> accntsToUpdate = new Map<Id,Account>();
            //start 31-Jul-2018 reymark.j.l.arlos
            Set<Id> doNotRenewAcntId = new Set<Id>();
            //end 31-Jul-2018 reymark.j.l.arlos

            Set<Id> accIdsForExpiryDate = new Set<Id>();

            for(Account acntRec : accountList)
            {
                //start 22-Jun-2018 reymark.j.l.arlos Added logic so it can calculate if the Merge Account is customer account
                if(acntRec.RecordTypeId == custAccRecTypeId)
                {
                    if(!string.isBlank(string.valueOf(acntRec.MasterRecordId)))
                    {
                        custAccIdSet.add(acntRec.MasterRecordId);
                    }
                    else
                    {
                        custAccIdSet.add(acntRec.Id);
                    }
                }
                else if(acntRec.RecordTypeId == licAccRecTypeId)
                {
                    if(!string.isBlank(string.valueOf(acntRec.MasterRecordId)))
                    {
                        historicLicAccIdSet.add(acntRec.MasterRecordId);
                        currentLicAccIdSet.add(acntRec.MasterRecordId);
                        pastYearAccIdSet.add(acntRec.MasterRecordId);
                    }
                    else
                    {
                        historicLicAccIdSet.add(acntRec.Id);
                        currentLicAccIdSet.add(acntRec.Id);
                        pastYearAccIdSet.add(acntRec.MasterRecordId);
                    }
                }
                //end 22-Jun-2018 reymark.j.l.arlos
            }

            //start 19-Sep-2018 reymark.j.l.arlos updated based on the new requirements in GREEN-33362
            if(!historicLicAccIdSet.isEmpty())
            {
                List<Account> historicAccntList = [SELECT Id, JVCO_Review_Frequency__c, JVCO_PRS_Historic_Value_Total__c, JVCO_PPL_Historic_Value_Total__c, JVCO_VPL_Historic_Value_Total__c, JVCO_HistoricCurrentValuesBatchProcessed__c,
                                                        (SELECT Id, SBQQ__EndDate__c, SBQQ__Product__r.JVCO_Society__c, JVCO_NetTotal__c
                                                                FROM SBQQ__Subscriptions__r
                                                                WHERE SBQQ__QuoteLine__c = null AND SBQQ__RenewalQuantity__c > 0 AND JVCO_Subscription_Temp_External_ID__c != null AND SBQQ__Contract__r.JVCO_PPL_Gap__c = false 
                                                                ORDER BY SBQQ__EndDate__c DESC, SBQQ__Product__r.JVCO_Society__c ASC
                                                        )
                                                    FROM Account 
                                                    WHERE Id IN: historicLicAccIdSet
                                                    ];

                for(Account accRec : historicAccntList)
                {
                    accRec.JVCO_PRS_Historic_Value_Total__c = 0;
                    accRec.JVCO_PPL_Historic_Value_Total__c = 0;
                    accRec.JVCO_VPL_Historic_Value_Total__c = 0;
                    accRec.JVCO_HistoricCurrentValuesBatchProcessed__c = true;
                    //start reymark.j.l.arlos 21-11-2018 GREEN-34018
                    if(customSet.JVCO_EnableInitialDataUpdate__c)
                    {
                        accRec.JVCO_StockValueInitialUpdate__c = true;
                    }
                    //end reymark.j.l.arlos GREEN-34018

                    Date prsHistoricDate;
                    Date pplHistoricDate;
                    Date vplHistoricDate;
                    Boolean prsSkip = false;
                    Boolean pplSkip = false;
                    Boolean vplSkip = false;

                    if(!accRec.SBQQ__Subscriptions__r.isEmpty())
                    {
                        for(SBQQ__Subscription__c subRec : accRec.SBQQ__Subscriptions__r)
                        {
                            if(!prsSkip || !pplSkip || !vplSkip)
                            {
                                if(subRec.SBQQ__Product__r.JVCO_Society__c == 'PRS')
                                {
                                    if(!prsSkip)
                                    {
                                        if(prsHistoricDate == null)
                                        {
                                            Date tempDate = subRec.SBQQ__EndDate__c;
                                            prsHistoricDate = tempDate.addDays(-364);
                                        }

                                        if(subRec.SBQQ__EndDate__c >= prsHistoricDate)
                                        {
                                            if(subRec.JVCO_NetTotal__c != null)
                                            {
                                                accRec.JVCO_PRS_Historic_Value_Total__c += subRec.JVCO_NetTotal__c;
                                            }
                                        }
                                        else
                                        {
                                            prsSkip = true;
                                        }
                                    }
                                }
                                else if(subRec.SBQQ__Product__r.JVCO_Society__c == 'PPL')
                                {
                                    if(!pplSkip)
                                    {
                                        if(pplHistoricDate == null)
                                        {
                                            Date tempDate = subRec.SBQQ__EndDate__c;
                                            pplHistoricDate = tempDate.addDays(-364);
                                        }

                                        if(subRec.SBQQ__EndDate__c >= pplHistoricDate)
                                        {
                                            if(subRec.JVCO_NetTotal__c != null)
                                            {
                                                accRec.JVCO_PPL_Historic_Value_Total__c += subRec.JVCO_NetTotal__c;
                                            }
                                        }
                                        else
                                        {
                                            pplSkip = true;
                                        }
                                    }
                                }
                                else if(subRec.SBQQ__Product__r.JVCO_Society__c == 'VPL')
                                {
                                    if(!vplSkip)
                                    {
                                        if(vplHistoricDate == null)
                                        {
                                            Date tempDate = subRec.SBQQ__EndDate__c;
                                            vplHistoricDate = tempDate.addDays(-364);
                                        }

                                        if(subRec.SBQQ__EndDate__c >= vplHistoricDate)
                                        {
                                            if(subRec.JVCO_NetTotal__c != null)
                                            {
                                                accRec.JVCO_VPL_Historic_Value_Total__c += subRec.JVCO_NetTotal__c;
                                            }
                                        }
                                        else
                                        {
                                            vplSkip = true;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                    accntsToUpdate.put(accRec.Id, accRec);
                }
            }

            if(!currentLicAccIdSet.isEmpty())
            {
                //28-Dec-2018 reymark.j.l.arlos GREEN-34171 updated inner where clause to check either JVCO_Renewal_Generated__c = false or JVCO_IncludeCurrentValueCalculation__c = true
                List<Account> currentAccntList = [Select Id, JVCO_PRS_Current_Value_Total__c, JVCO_PPL_Current_Value_Total__c, JVCO_VPL_Current_Value_Total__c, JVCO_HistoricCurrentValuesBatchProcessed__c, JVCO_PRS_Last_Year_Value__c, JVCO_PPL_Last_Year_Value__c, JVCO_VPL_Last_Year_Value__c,
                                                        (SELECT Id, JVCO_PRS_Current_Value_Total__c, JVCO_PPL_Current_Value_Total__c, JVCO_VPL_Current_Value_Total__c, JVCO_PRS_Last_Year_ValueContract__c, JVCO_PPL_Last_Year_ValueContract__c, JVCO_VPL_Last_Year_ValueContract__c
                                                                FROM Contracts 
                                                                WHERE JVCO_PPL_Gap__c = false AND JVCO_RenewableQuantity__c > 0 AND (JVCO_Renewal_Generated__c = false OR JVCO_IncludeCurrentValueCalculation__c = true) AND (JVCO_PRS_Current_Value_Total__c > 0 OR JVCO_PPL_Current_Value_Total__c > 0 OR JVCO_VPL_Current_Value_Total__c > 0)
                                                                ORDER BY EndDate DESC
                                                        ) 
                                                    FROM Account 
                                                    WHERE Id IN: currentLicAccIdSet
                                                    ];

                for(Account accRec : currentAccntList)
                {
                    accRec.JVCO_PRS_Current_Value_Total__c = 0;
                    accRec.JVCO_PPL_Current_Value_Total__c = 0;
                    accRec.JVCO_VPL_Current_Value_Total__c = 0;

                    accRec.JVCO_HistoricCurrentValuesBatchProcessed__c = true;
                    //start reymark.j.l.arlos 21-11-2018 GREEN-34018
                    if(customSet.JVCO_EnableInitialDataUpdate__c)
                    {
                        accRec.JVCO_StockValueInitialUpdate__c = true;
                    }
                    //end reymark.j.l.arlos GREEN-34018

                    if(!accRec.Contracts.isEmpty())
                    {
                        for(Contract contRec : accRec.Contracts)
                        {
                            if(contRec.JVCO_PRS_Current_Value_Total__c != null)
                            {
                                accRec.JVCO_PRS_Current_Value_Total__c += contRec.JVCO_PRS_Current_Value_Total__c;
                            }
                            if(contRec.JVCO_PPL_Current_Value_Total__c != null)
                            {
                                accRec.JVCO_PPL_Current_Value_Total__c += contRec.JVCO_PPL_Current_Value_Total__c;
                            }
                            if(contRec.JVCO_VPL_Current_Value_Total__c != null)
                            {
                                accRec.JVCO_VPL_Current_Value_Total__c += contRec.JVCO_VPL_Current_Value_Total__c;
                            }
                        }
                    }

                    if(accntsToUpdate.containsKey(accRec.Id))
                    {
                        accntsToUpdate.get(accRec.Id).JVCO_PRS_Current_Value_Total__c = accRec.JVCO_PRS_Current_Value_Total__c;
                        accntsToUpdate.get(accRec.Id).JVCO_PPL_Current_Value_Total__c = accRec.JVCO_PPL_Current_Value_Total__c;
                        accntsToUpdate.get(accRec.Id).JVCO_VPL_Current_Value_Total__c = accRec.JVCO_VPL_Current_Value_Total__c;
                    }
                    else
                    {
                        accntsToUpdate.put(accRec.Id, accRec);
                    }
                }
            }
            //end 19-Sep-2018 reymark.j.l.arlos

            //rhys.j.c.dela.cruz 29-Apr-2018
            if(!pastYearAccIdSet.isEmpty())
            {
                Integer lastYear = Date.today().year() - 1;
                
                //28-Dec-2018 reymark.j.l.arlos GREEN-34171 updated inner where clause to check either JVCO_Renewal_Generated__c = false or JVCO_IncludeCurrentValueCalculation__c = true
                //05-Apr-2019 rhys.j.c.dela.cruz GREEN-34465 - adjusted query to have Last Year Value fields
                List<Account> pastAccntList = [Select Id, JVCO_PRS_Last_Year_Value__c, JVCO_PPL_Last_Year_Value__c, JVCO_VPL_Last_Year_Value__c, JVCO_HistoricCurrentValuesBatchProcessed__c, 
                                                    (SELECT Id, JVCO_PRS_Last_Year_ValueContract__c, JVCO_PPL_Last_Year_ValueContract__c, JVCO_VPL_Last_Year_ValueContract__c
                                                            FROM Contracts 
                                                            WHERE JVCO_PPL_Gap__c = false AND JVCO_RenewableQuantity__c > 0 AND (JVCO_Renewal_Generated__c = true OR JVCO_IncludeCurrentValueCalculation__c = true OR JVCO_Renewal_Generated__c = false) AND JVCO_Previous_Year__c =: lastYear AND (JVCO_PRS_Last_Year_ValueContract__c > 0 OR JVCO_PPL_Last_Year_ValueContract__c > 0 OR JVCO_VPL_Last_Year_ValueContract__c > 0)
                                                            ORDER BY EndDate DESC
                                                    ) 
                                                FROM Account 
                                                WHERE Id IN: pastYearAccIdSet
                                                ];

                for(Account accRec : pastAccntList)
                {
                    //Last Year Value Fields
                    accRec.JVCO_PRS_Last_Year_Value__c = 0;
                    accRec.JVCO_PPL_Last_Year_Value__c = 0;
                    accRec.JVCO_VPL_Last_Year_Value__c = 0;

                    //accRec.JVCO_HistoricCurrentValuesBatchProcessed__c = true;
                    //start reymark.j.l.arlos 21-11-2018 GREEN-34018
                    //if(customSet.JVCO_EnableInitialDataUpdate__c)
                    //{
                    //    system.debug('Update: true');
                    //    accRec.JVCO_StockValueInitialUpdate__c = true;
                    //}
                    ////end reymark.j.l.arlos GREEN-34018

                    if(!accRec.Contracts.isEmpty())
                    {
                        for(Contract contRec : accRec.Contracts)
                        {
                            //Last Year Value Fields
                            if(contRec.JVCO_PRS_Last_Year_ValueContract__c != null){
                                accRec.JVCO_PRS_Last_Year_Value__c += contrec.JVCO_PRS_Last_Year_ValueContract__c;
                            }
                            if(contRec.JVCO_PPL_Last_Year_ValueContract__c != null){
                                accRec.JVCO_PPL_Last_Year_Value__c += contrec.JVCO_PPL_Last_Year_ValueContract__c;
                            }
                            if(contRec.JVCO_VPL_Last_Year_ValueContract__c != null){
                                accRec.JVCO_VPL_Last_Year_Value__c += contrec.JVCO_VPL_Last_Year_ValueContract__c;
                            }
                        }
                    }

                    if(accntsToUpdate.containsKey(accRec.Id))
                    {
                        //Last Year Values
                        accntsToUpdate.get(accRec.Id).JVCO_PRS_Last_Year_Value__c = accRec.JVCO_PRS_Last_Year_Value__c;
                        accntsToUpdate.get(accRec.Id).JVCO_PPL_Last_Year_Value__c = accRec.JVCO_PPL_Last_Year_Value__c;
                        accntsToUpdate.get(accRec.Id).JVCO_VPL_Last_Year_Value__c = accRec.JVCO_VPL_Last_Year_Value__c;                        
                    }
                    else
                    {
                        accntsToUpdate.put(accRec.Id, accRec);
                    }
                }
            }

            //start 22-Jun-2018 reymark.j.l.arlos Added logic so it can calculate if the Merge Account is customer account
            if(!custAccIdSet.isEmpty())
            {
                List<AggregateResult> licAccntList = [SELECT JVCO_Customer_Account__c, SUM(JVCO_PRS_Historic_Value_Total__c) prsHistoric, SUM(JVCO_PPL_Historic_Value_Total__c) pplHistoric, SUM(JVCO_VPL_Historic_Value_Total__c) vplHistoric, SUM(JVCO_PRS_Current_Value_Total__c) prsCurrent, SUM(JVCO_PPL_Current_Value_Total__c) pplCurrent, SUM(JVCO_VPL_Current_Value_Total__c) vplCurrent, SUM(JVCO_PRS_Last_Year_Value__c) prsLastYear, SUM(JVCO_PPL_Last_Year_Value__c) pplLastYear, SUM(JVCO_VPL_Last_Year_Value__c) vplLastYear
                                                        FROM Account 
                                                        WHERE JVCO_Customer_Account__c IN: custAccIdSet
                                                        GROUP BY JVCO_Customer_Account__c
                                                        ];

                if(!licAccntList.isEmpty())
                {
                    for(AggregateResult licAccntAggResult : licAccntList)
                    {
                        if(accntsToUpdate.containsKey((Id)licAccntAggResult.get('JVCO_Customer_Account__c')))
                        {
                            accntsToUpdate.get((Id)licAccntAggResult.get('JVCO_Customer_Account__c')).JVCO_PRS_Historic_Value_Total__c = (decimal)licAccntAggResult.get('prsHistoric');
                            accntsToUpdate.get((Id)licAccntAggResult.get('JVCO_Customer_Account__c')).JVCO_PPL_Historic_Value_Total__c = (decimal)licAccntAggResult.get('pplHistoric');
                            accntsToUpdate.get((Id)licAccntAggResult.get('JVCO_Customer_Account__c')).JVCO_VPL_Historic_Value_Total__c = (decimal)licAccntAggResult.get('vplHistoric');
                            accntsToUpdate.get((Id)licAccntAggResult.get('JVCO_Customer_Account__c')).JVCO_PRS_Current_Value_Total__c = (decimal)licAccntAggResult.get('prsCurrent');
                            accntsToUpdate.get((Id)licAccntAggResult.get('JVCO_Customer_Account__c')).JVCO_PPL_Current_Value_Total__c = (decimal)licAccntAggResult.get('pplCurrent');
                            accntsToUpdate.get((Id)licAccntAggResult.get('JVCO_Customer_Account__c')).JVCO_VPL_Current_Value_Total__c = (decimal)licAccntAggResult.get('vplCurrent');

                            //Last Year Fields
                            accntsToUpdate.get((Id)licAccntAggResult.get('JVCO_Customer_Account__c')).JVCO_PRS_Last_Year_Value__c = (decimal)licAccntAggResult.get('prsLastYear');
                            accntsToUpdate.get((Id)licAccntAggResult.get('JVCO_Customer_Account__c')).JVCO_PPL_Last_Year_Value__c = (decimal)licAccntAggResult.get('pplLastYear');
                            accntsToUpdate.get((Id)licAccntAggResult.get('JVCO_Customer_Account__c')).JVCO_VPL_Last_Year_Value__c = (decimal)licAccntAggResult.get('vplLastYear');
                        }
                        else
                        {
                            Account custAccntRec = new Account();
                            custAccntRec.Id = (Id)licAccntAggResult.get('JVCO_Customer_Account__c');
                            custAccntRec.JVCO_PRS_Historic_Value_Total__c = (decimal)licAccntAggResult.get('prsHistoric');
                            custAccntRec.JVCO_PPL_Historic_Value_Total__c = (decimal)licAccntAggResult.get('pplHistoric');
                            custAccntRec.JVCO_VPL_Historic_Value_Total__c = (decimal)licAccntAggResult.get('vplHistoric');
                            custAccntRec.JVCO_PRS_Current_Value_Total__c = (decimal)licAccntAggResult.get('prsCurrent');
                            custAccntRec.JVCO_PPL_Current_Value_Total__c = (decimal)licAccntAggResult.get('pplCurrent');
                            custAccntRec.JVCO_VPL_Current_Value_Total__c = (decimal)licAccntAggResult.get('vplCurrent');

                            //Last Year Fields
                            custAccntRec.JVCO_PRS_Last_Year_Value__c = (decimal)licAccntAggResult.get('prsLastYear');
                            custAccntRec.JVCO_PPL_Last_Year_Value__c = (decimal)licAccntAggResult.get('pplLastYear');
                            custAccntRec.JVCO_VPL_Last_Year_Value__c = (decimal)licAccntAggResult.get('vplLastYear');

                            accntsToUpdate.put(custAccntRec.Id, custAccntRec);

                        }
                    }
                }
            }
            //end 22-Jun-2018 reymark.j.l.arlos

            if(!accntsToUpdate.isEmpty())
            {
                List<Account> acntUpdateList = new List<Account>();
                acntUpdateList = accntsToUpdate.values();
                if(Test.isRunningTest())
                {
                    acntUpdateList.add(new Account());
                }
                List<Database.SaveResult> updateResults = Database.update(acntUpdateList,false);
                List<ffps_custRem__Custom_Log__c> errorLogList = new List<ffps_custRem__Custom_Log__c>();

                for(Integer ctr = 0; ctr < acntUpdateList.size(); ctr++)
                {
                    Database.SaveResult updateResult = updateResults[ctr];
                    Account acntRec = acntUpdateList[ctr];
                    if(!updateResult.isSuccess())
                    {
                        for(Database.Error updateError: updateResult.getErrors())
                        {
                            ffps_custRem__Custom_Log__c errorLog = new ffps_custRem__Custom_Log__c();
                            /*
                            errorLog.Name = String.valueOf(acntRec.ID);
                            errorLog.JVCO_FailedRecordObjectReference__c = String.valueOf(acntRec.ID);
                            errorLog.blng__FullErrorLog__c = string.valueof(updateError.getMessage());
                            errorLog.blng__ErrorCode__c = string.valueof(updateError.getStatusCode());
                            errorLog.JVCO_ErrorBatchName__c = 'JVCO_CalcHistoricCurrentValuesBatch';
                            errorLogList.add(errorLog);
                            */

                            errorLog.ffps_custRem__Related_Object_Key__c = String.valueOf(acntRec.ID);
                            errorLog.ffps_custRem__Detail__c = string.valueof(updateError.getMessage());
                            errorLog.ffps_custRem__Grouping__c = string.valueof(updateError.getStatusCode());
                            errorLog.ffps_custRem__Message__c = 'JVCO_CalcHistoricCurrentValuesBatch';
                            errorLogList.add(errorLog);
                        }
                    }
                }

                if(!errorLogList.isEmpty())
                {
                    insert errorLogList;
                }
            }

        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}