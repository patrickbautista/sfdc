@isTest
public class JVCO_CreateRenewalQuoteSchedulerTest{
    @testSetup
    static void testData(){
        
        
    }
    
    static testMethod void testJVCO_CreateRenewalQuoteScheduler(){
        
            SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();  
        insert qProcess;

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Account acc =  JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.JVCO_Review_Type__c = 'Automatic';
        //acc2.JVCO_Renewal_Scenario__c = 'PPL First: Greater Than 60 Days';
        insert acc2;

        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;
        Test.startTest();
        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_Closure_Reason__c = '';
        aff.JVCO_End_Date__c = null;
        insert aff;

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        insert opp;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
      // 
        insert q;
        
        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c order by CreatedDate DESC limit 1];

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, prod.Id);
        insert ql;

        q.SBQQ__Type__c = 'Renewal';
        q.JVCO_Quote_Auto_Renewed__c = false;
        q.SBQQ__Status__c = 'Draft';
        update q;

        Contract c = JVCO_TestClassObjectBuilder.createContract(acc2.id, q.id);
        c.JVCO_Licence_Period_End_Date__c = date.today() -5 ;
        insert c;

        SBQQ__Subscription__c sub = JVCO_TestClassObjectBuilder.createSubscription(acc2.id, prod.id);
        sub.sbqq__contract__c = c.id;
        insert sub;

        Contract c2 = new Contract();
        //System.assertEquals(c, c2, 'error');

        List<JVCO_Renewal_Settings__c> settings = new List<JVCO_Renewal_Settings__c>();
        settings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Completion', Days__c = 1.0));
        settings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Generation', Days__c = 400.0));
        insert settings;

        //JVCO_CreateRenewalQuoteScheduler reminder = New JVCO_CreateRenewalQuoteScheduler();
        //String schedrun = '0 46 16 * * ?';
        //String jobId = system.schedule('Create Renewal Quote', schedrun, reminder);
        
        //query original list of contracts
        List<Contract> originalList = [ SELECT Id, SBQQ__RenewalQuoted__c, Account.JVCO_Review_Type__c From Contract WHERE Account.JVCO_Review_Type__c = 'Automatic' LIMIT 200 ];
                
        //assertion method
        for(Contract contrList: originalList)
        {
            system.assertEquals(FALSE, contrList.SBQQ__RenewalQuoted__c, 'Renewal Quoted is not FALSE');
        }
        
        //JVCO_CreateRenewalQuoteBatch createRQBatch = new JVCO_CreateRenewalQuoteBatch();
        //Database.executeBatch(createRQBatch);
                
        JVCO_CreateRenewalQuoteScheduler.CallExecuteMomentarily();
                
        Test.stopTest();    
           
        //query corrected contracts
        List<Contract> correctedList = [ SELECT Id, SBQQ__RenewalQuoted__c From Contract Where Account.JVCO_Review_Type__c = 'Automatic' LIMIT 200 ];
        
        //assertion method
        //for(Contract contrList: correctedList)
        //{
        //    system.assertEquals(TRUE, contrList.SBQQ__RenewalQuoted__c, 'Renewal Quoted is not TRUE');
        //}
        System.assert(!correctedList.isEmpty());
        

    }
}