public class JVCO_CashUndoMatching_Queueable implements Queueable
{
    private List<c2g__codaCashMatchingHistory__c> mhlist;
    
    public JVCO_CashUndoMatching_Queueable(List<c2g__codaCashMatchingHistory__c> mhlist)
    {
        this.mhlist = mhlist;
    }
    
    public void execute(QueueableContext context) 
    {   
        if(!mhlist.isEmpty())
        {
            //Get First Record
            c2g__codaCashMatchingHistory__c mh = mhlist.get(0);
            System.debug('@@@@@@ mh: '+mh);
            
            //unmatch cash
            try{
                JVCO_CashUnmatchingLogic.undoCashMatching(mh);
            }
            catch (exception e){
                //create error logs to trace failed records          
                
                ffps_custRem__Custom_Log__c newErr = new ffps_custRem__Custom_Log__c();
                
                newErr.ffps_custRem__Running_User__c = UserInfo.getUserId();
                newErr.ffps_custRem__Related_Object_Key__c= String.valueOf(mh.Id);
                newErr.ffps_custRem__Detail__c = String.valueof(e.getMessage());
                newErr.ffps_custRem__Message__c = 'JVCO_CashUndoMatching_Queueable';
                newErr.ffps_custRem__Grouping__c = 'CUQ-001';
                
                insert newErr;
                System.debug('@@@@@ error: '+ newErr.id);
            }   
            
            //Remove the first record to disinclude in recursion
            mhlist.remove(0);          
            
        }
        
        //Recursion until the map is empty
        if(!mhlist.isEmpty())
        {
            System.enqueueJob(new JVCO_CashUndoMatching_Queueable(mhlist));
        }
        /*
//insert errorlogs after the last record
if (mhlist.isEmpty() && !errorlogs.isEmpty()){
System.debug('@@@@@ errorLogs.size(): '+errorLogs.size());
insert errorlogs;
}*/
    }
}