@isTest
public class JVCO_DocumentQueueCreationControllerTest {

    static testMethod void testGetCurrentRecord(){
    
        Account acc1 =  JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc1;
        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc1.Id);
        insert acc2;
        //List<account> accountList = [SELECT Name FROM Account];
        List<Account> testacc = new List<Account>([SELECT Id, Name, JVCO_Customer_Account_Name__c from Account]);
        Apexpages.currentpage().getparameters().put('id', testacc[1].Id); 
        ApexPages.standardSetController controller = new ApexPages.standardSetController(testacc);
      
        JVCO_DocumentQueueCreationController jvco_DocQCreateCtrl =  new JVCO_DocumentQueueCreationController(controller);
        test.startTest();
        Account acc = jvco_DocQCreateCtrl.getCurrentRecord();
        PageReference sendDocu = jvco_DocQCreateCtrl.SendDocument();
        PageReference downLoad = jvco_DocQCreateCtrl.ForDownload();
        PageReference downLoadPrev = jvco_DocQCreateCtrl.ForDownloadPreview();
        Map<String,String> testMap = new map<String, String>();
        //testMap = jvco_DocQCreateCtrl.getFieldId();
        test.stopTest();
        
        System.assertEquals(ApexPages.currentPage().getParameters().get('id'), acc.Id);
    }
    
     static testMethod void forSendDocLead_Test(){
    
        Lead lead1 = JVCO_TestClassObjectBuilder.createLead();
        insert lead1;
        
        List<Lead> testlead = new List<Lead>([SELECT Id, Name from Lead]);
        Apexpages.currentpage().getparameters().put('id', testlead[0].Id); 
        ApexPages.standardSetController controller = new ApexPages.standardSetController(testlead);
      
        JVCO_DocumentQueueCreationController jvco_DocQCreateCtrl =  new JVCO_DocumentQueueCreationController(controller);
        test.startTest();
        PageReference sendDocLead = jvco_DocQCreateCtrl.forSendDocLead();
        test.stopTest();
        
        System.assertEquals(ApexPages.currentPage().getParameters().get('id'), testlead[0].Id);
    }

}