    /* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_CompleteQuoteController.cls 
   Description:     Controller for JVCO_CompleteQuote.page
   Test class:      JVCO_CompleteQuoteControllerTest.cls 

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
   07-Dec-2016       0.1        Accenture-robert.j.b.lacatan      Initial version of the code      
   29-Mar-2017       0.2        Accenture-samuel.a.d.oberes       Refactored contorller and page | added functionality. REF: https://projectgreen.atlassian.net/browse/GREEN-9030 --- attachment [...]_v0.4.docx
   10-Apr-2017       0.3        Accenture-ryan.i.r.limlingan      Added GI functionality (GREEN-11574) 
   17-Apr-2017       0.4        Accenture-ryan.i.r.limlingan      Added GI functionality (GREEN-11574)
   12-Oct-2017       0.5        Accenture-mel.andrei.b.santos     Updated DML statements to avoid duplication of subscription upon completing quote and generating contract as per GREEN-23460
   26-Dec-2017       0.6        Accenture-mel.andrei.b.santos     Updated controller to run JVCO_CompleteQuoteOrdered first before JVCO_CompleteQuoteController as per GREEN-26719
   25-Jan-2018       0.7        Accenture-reymark.j.l.arlos       Moved the query query of quote from updateOppAndQuote to constructor
   21-Feb-2018       0.8        Accenture-reymark.j.l.arlos       Added additional condition on validating quote record before contracting
   04-Apr-2018       0.9        Accenture-reymark.j.l.arlos       GREEN-31213 Throws error message for quote lines with cancelled events
   24-Apr-2018       1.0        Accenture-rhys.j.c.dela.cruz      GREEN-31491 Throw error message for quotes without End Dates
   29-Jun-2018       1.1        Accenture-jules.osberg.a.pablo    GREEN-31760, GREEN-32139 Added requirement of T&C's before Contracting Quotes.
   11-Jul-2018       1.2        Accenture-mel.andrei.b.santos     GREEN-31830, added condition preventing the contracting of quotes if # of quote lines w/o SPV > 0
   10-Sept-2018      1.3        Accenture-rhys.j.c.dela.cruz      GREEN-19346 Prevent completion of quote if one of the QLGs has no Quote Line
   19-Nov-2018       1.4        Accenture-rhys.j.c.dela.cruz      GREEN-34012 - Moved KA Quote Usage Summary Validation to Contract Reniew/Review and Complete Quote Buttons
   03-Dec-2018        1.5         mel.andrei.b.santos               GREEN-34091 - Update controller to consider initial value for rollback
   03-Feb-2020       1.6        Accenture-rhys.j.c.dela.cruz      GREEN-35358 - Adjust controller to show fields that are blank
   30-June-2020    sean.g.perez                         GREEN-35674 Add Error Message if Covid 19 Impacted field is blank/null
   26-Oct-2020        1.8        Accenture-sean.g.perez            GREEN-35962 - Terms and Conditions Accepted Via Fields
   ---------------------------------------------------------------------------------------------------------- */


public class JVCO_CompleteQuoteController {
    public Boolean tcAccepted {get; set;}
    public string selectedTC {get;set;}
    public string selectedTCVia {get;set;}
    public Boolean disableTCVia {get; set;}

    private JVCO_TermsAndConditionHelper termsAndConditionHelper;

    public SBQQ__Quote__c cQuote { get; set; }
    public String quoteNumber {get; set;}

    public Boolean kaUsageSummarySent {get;set;}

    public List<String> apiFieldList = new List<String>();
    public Boolean renderMissingFields {get;set;}

    public JVCO_CompleteQuoteController(ApexPages.StandardController stdController){
        disableTCVia = TRUE;

        this.cQuote = (SBQQ__Quote__c)stdController.getRecord();

        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;

        termsAndConditionHelper = new JVCO_TermsAndConditionHelper();

        // start reymark.j.l.arlos 25-Jan-2018 moved the query of quote from updateOppAndQuote to constructor
        cQuote = [SELECT SBQQ__Opportunity2__c, 
                                        SBQQ__Opportunity2__r.SBQQ__Contracted__c, 
                                        SBQQ__Opportunity2__r.SBQQ__Ordered__c,
                                        SBQQ__Opportunity2__r.StageName, 
                                        SBQQ__Opportunity2__r.Probability,
                                        SBQQ__Opportunity2__r.SBQQ__PrimaryQuote__c,
                                        SBQQ__Primary__c, 
                                        SBQQ__StartDate__c, 
                                        Start_Date__c,
                                        End_Date__c,
                                        SBQQ__SubscriptionTerm__c, 
                                        SBQQ__NetAmount__c, 
                                        SBQQ__Status__c, 
                                        JVCO_QuoteComplete__c,
                                        JVCO_Cancelled__c,
                                        JVCO_Contact_Type__c,
                                        Covid_19_Impacted1__c,
                                        Accept_TCs__c,
                                        Name,
                                        SBQQ__Type__c,
                                        JVCO_Single_Society__c,
                                        NoOfQuoteLinesWithoutSPV__c,
                                        JVCO_Cust_Account__r.TCs_Accepted__c,
                                        JVCO_Cust_Account__c,
                                        JVCO_CreditReason__c,
                                        JVCO_Recalculated__c,
                                        CreatedDate,
                                        SBQQ__MasterContract__c,
                                        DateContracted__c, 
                                        SBQQ__Account__c,
                                        SBQQ__Account__r.Type,
                                        SBQQ__Account__r.JVCO_Live__c
                                   FROM SBQQ__Quote__c 
                                  WHERE id = :cQuote.id];

        quoteNumber = cQuote.Name;
        //end reymark.j.l.arlos 25-Jan-2018

      
        tcAccepted = termsAndConditionHelper.tcIsYes(cQuote.JVCO_Cust_Account__r.TCs_Accepted__c);
        if(cQuote.JVCO_Single_Society__c){
            tcAccepted = true;
        }
        selectedTC = cQuote.JVCO_Cust_Account__r.TCs_Accepted__c;

        //GREEN-19346
        String groupWithNoProductError = 'One or more Group has no Quote Line under them: ';
        Boolean hasGroupWithNoLine = false;
        List<SBQQ__QuoteLineGroup__c> qlgWithoutLine = new List<SBQQ__QuoteLineGroup__c>([SELECT id, Name FROM SBQQ__QuoteLineGroup__c WHERE SBQQ__Quote__c =: cQuote.id AND id NOT IN (SELECT SBQQ__Group__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =: cQuote.id)]);

        if(qlgWithoutLine.size() > 0){
            hasGroupWithNoLine = true;

            for(SBQQ__QuoteLineGroup__c qlg : qlgWithoutLine){
                groupWithNoProductError = groupWithNoProductError + '<br/>' + 'Group Name: ' + qlg.Name;
            }
        }

        if(hasGroupWithNoLine){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, groupWithNoProductError));
        }

        //GREEN-34012
        Account accountRecord = new Account();
        accountRecord.Id = cQuote.SBQQ__Account__c;
        accountRecord.Type = cQuote.SBQQ__Account__r.Type;
        accountRecord.JVCO_Live__c = cQuote.SBQQ__Account__r.JVCO_Live__c;
        kaUsageSummarySent = false;
        if(accountRecord.Type == 'Key Account' && accountRecord.JVCO_Live__c == false){

            List<SBQQ__QuoteLine__c> relatedQuoteLine = [Select Id, CreatedDate, LastModifiedDate from SBQQ__QuoteLine__c where SBQQ__Quote__r.SBQQ__Account__c = :accountRecord.Id order by LastModifiedDate desc limit 1];

            List<JVCO_Document_Queue__c> relatedDQ  = [select id, CreatedDate from JVCO_Document_Queue__c where JVCO_Related_Account__c = :accountRecord.Id AND (JVCO_Scenario__c = 'Key Account Quote Usage Summary PDF' OR JVCO_Scenario__c = 'Key Account Quote Usage Summary XLS' ) order by CreatedDate desc limit 1];

            if(relatedDQ.isEmpty() || ((!relatedQuoteLine.isEmpty() && !relatedDQ.isEmpty()) && (relatedQuoteLine[0].LastModifiedDate > relatedDQ[0].CreatedDate || relatedQuoteLine[0].CreatedDate > relatedDQ[0].CreatedDate))){
                kaUsageSummarySent = false;
            }else{
                kaUsageSummarySent = true;
            }
        }
        else{
            kaUsageSummarySent = true;
        }

        //GREEN-35358 Add Field API Names to List
        List<SBQQ__QuoteLine__c> quoteLineBlankEndDateList = new List<SBQQ__QuoteLine__c>([SELECT id, Name, End_Date__c FROM SBQQ__QuoteLine__c WHERE End_Date__c = null AND SBQQ__Quote__c =: cQuote.Id]);
        
        if(cQuote.Start_Date__c == null){
            apiFieldList.add('Start_Date__c');
        }
        if(cQuote.End_Date__c == null || !quoteLineBlankEndDateList.isEmpty()){
            apiFieldList.add('End_Date__c');
        }
        if(cQuote.JVCO_Contact_Type__c == null){
            apiFieldList.add('JVCO_Contact_Type__c');
        }
        if(!cQuote.SBQQ__Primary__c){
            apiFieldList.add('SBQQ__Primary__c');
        }
        if(cQuote.SBQQ__SubscriptionTerm__c == null){
            apiFieldList.add('SBQQ__SubscriptionTerm__c');
        }
        if(cQuote.SBQQ__Opportunity2__c == null){
            apiFieldList.add('SBQQ__Opportunity2__c');
        }

        if(cQuote.Covid_19_Impacted1__c == null){
            apiFieldList.add('Covid_19_Impacted1__c');
        }
        
        if(!apiFieldList.isEmpty()){
            renderMissingFields = true;
        }
        else{
            renderMissingFields = false;
        }
    
        getQuoteOutputFields();
    }

    public void disableTCVia(){
        if(selectedTC == 'Yes'){
            disableTCVia = FALSE;
        }
        else{
            disableTCVia = TRUE;
            selectedTCVia = NULL;
        }
    }
    
    public void isEmailSelected(){
        if(selectedTCVia == 'Email'){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, System.Label.Accepted_Via_Email_Reminder));
        }
    }
    
    public List<Selectoption> gettcAcceptedViaValues(){
        return termsAndConditionHelper.getTCAcceptedViaValues(); 
    }

    public List<Selectoption> getselectedTCfields(){
        return termsAndConditionHelper.getselectedTCfields(cQuote.JVCO_Cust_Account__r.TCs_Accepted__c); 
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         raus.k.b.ablaza
    Company:        Accenture
    Description:    Updates Quote field JVCO_QuoteComplete__c and related Opportunity to Closed Won
    Inputs:         n/a
    Returns:        n/a
    <Date>          <Authors Name>                      <Brief Description of Change> 
    07-Dec-2016     robert.j.b.lacatan                  Initial version of the code
    08-Mar-2017     robert.j.b.lacatan                  Remove space between cQuote and "." inside the if statement.
    29-Mar-2017     samuel.a.d.oberes                   Refactored the whole method to streamline it and add new functionality from GREEN-9030
    10-Apr-2017     ryan.i.r.limlingan                  Added Ordered as field to update in Opportunity
    10-Apr-2017     reymark.j.l.arlos                   Changed SBQQ__StartDate__c to Start_Date__c
    29-May-2017     mariel.m.buena                      Added a validation for JVCO_Contact_Type__c and Accept_TCs__c
    21-Jun-2017     raus.k.b.ablaza                     Moved updating of SBQQ__Ordered__c of opportunity to 
                                                        an asynchronous method; GREEN-18195
    29-Sept-2017    eu.rey.t.cadag                      update of Opportunity and Quote is removed in this class and move to JVCO_CompleteQuoteContracted
                                                        JVCO_CompleteQuoteContracted is Queueable class that when error encountered it rollbacks changes made for Quote and Opportunity.
                                                        The class also determines if Opportunity has already been contracted to prevent contracting and accumulation of amount in contract.
    12-Oct-2017     mel.andrei.b.santos                 Updated DML statements to avoid duplication of subscription upon completing quote and generating contract as per GREEN-23460
    22-Dec-2017     mel.andrei.b.santos                 Updated controller to run JVCO_CompleteQuoteOrdered first before JVCO_CompleteQuoteController as per GREEN-26719
    25-Jan-2018     reymark.j.l.arlos                   moved the query of the quote record to the constructor to show the quote number on load GREEN-29504
    21-Feb-2018     reymark.j.l.arlos                   added condition before showing error message for Accept T&C's
    04-May-2018     reymark.j.l.arlos                   GREEN-31213 Throws error message for quote lines with cancelled events
    24-Apr-2018     rhys.j.c.dela.cruz                  GREEN-31491 Throw error message for quotes without End Dates
    11-Jul-2018     mel.andrei.b.santos                 GREEN-31830, added condition preventing the contracting of quotes if # of quote lines w/o SPV > 0
    ----------------------------------------------------------------------------------------------------------*/    
    public PageReference updateOppAndQuote() {
        
        Map<String, JVCO_Complete_Quote_Settings__c> settings = JVCO_Complete_Quote_Settings__c.getAll();
        Set<Id> oppIdSet = new Set<Id>();
        SBQQ__Quote__c oldQuote = new SBQQ__Quote__c();
        Opportunity oldOpp = new Opportunity();
        Boolean completedQuoteAvailable = false;

        oldQuote = cQuote.clone(false,true); //03-Dec-2018  mel.andrei.b.santos GREEN-34091 

         //start 12-Oct-2018 mariel.m.buena GREEN-33542 Throws an error message when Contracting an Amendment Quote if the MasterContract has already a Completed Quote
        List<SBQQ__Quote__c> completedAmendmentQuotes = [Select Id, LastModifiedDate,DateContracted__c FROM SBQQ__Quote__c 
                                                     WHERE SBQQ__MasterContract__c =: cQuote.SBQQ__MasterContract__c AND (SBQQ__Status__c='Accepted' OR SBQQ__Status__c='Complete and Invoiced' OR SBQQ__Status__c='Approved') AND id !=: cQuote.Id AND SBQQ__MasterContract__c != NULL AND SBQQ__Type__c='Amendment' AND    DateContracted__c!= NULL AND DateContracted__c >: cQuote.CreatedDate
                                                     ORDER By DateContracted__c DESC LIMIT 1];
                                          
        if(completedAmendmentQuotes != NULL)
         {
             for (SBQQ__Quote__c compQuote: completedAmendmentQuotes)
             {
                 if(System.now() > compQuote.DateContracted__c)
                 {
                     completedQuoteAvailable = true;
                 }
             }
         } 
         //end 12-Oct-2018 mariel.m.buena

        //start 04-May-2018 reymark.j.l.arlos GREEN-31213 Throws error message for quote lines with cancelled events
        String cancelledEventErrorMessage = 'One or more of the quote lines has an Event that is cancelled:';
        List<SBQQ__QuoteLine__c> cancelledQuoteLineLst = [Select Id, Name, JVCO_Event__r.Name
                                                            From SBQQ__QuoteLine__c
                                                            Where JVCO_Event__c != null and JVCO_Event__r.JVCO_CancelledEvent__c = true and SBQQ__Quote__c =: cQuote.Id];

        List<SBQQ__QuoteLine__c> quoteLineBlankEndDateList = new List<SBQQ__QuoteLine__c>([SELECT id, Name, End_Date__c FROM SBQQ__QuoteLine__c WHERE End_Date__c = null AND SBQQ__Quote__c =: cQuote.Id]);

        if(selectedTC == null && tcAccepted == false){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.TermsAndConditionErrorText));
        }else{
            tcAccepted = true;

            if(!cancelledQuoteLineLst.isEmpty())
            {
                for(SBQQ__QuoteLine__c qlRec : cancelledQuoteLineLst)
                {
                    cancelledEventErrorMessage = cancelledEventErrorMessage + '<br/>' +  'Quote Line: ' + qlRec.Name + ' and Event: ' + qlRec.JVCO_Event__r.Name;
                }

                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, cancelledEventErrorMessage) );
            }
            //end 04-May-2018 reymark.j.l.arlos

            //start 12-Oct-2018 mariel.m.buena
             else if (completedQuoteAvailable) 
             {
                 ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR,  settings.get('SecondAmendmentError').Value__c) );
             }
             //end 12-Oct-2018 mariel.m.buena
                                      
            else if (cQuote.SBQQ__Primary__c && cQuote.Start_Date__c != null && cQuote.SBQQ__SubscriptionTerm__c != null && cQuote.SBQQ__Opportunity2__c != null && cQuote.End_Date__c != null && quoteLineBlankEndDateList.isEmpty() && cQuote.JVCO_Recalculated__c && cQuote.JVCO_Contact_Type__c != null && cQUote.Covid_19_Impacted1__c != null) {
                if ( cQuote.SBQQ__Status__c != 'User Cancelled'){ //Removed Contact type in If

                    renderMissingFields = false;
                     
                    // GREEN-32099 Set JVCO_CreditReason Field as mandatory if the Net Amount is < 0 or Amendment patrick.t.bautista
                    if(cQuote.SBQQ__NetAmount__c < 0 && cQuote.JVCO_CreditReason__c == null)
                    {
                        ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, settings.get('ErrorBlankCreditReason').Value__c) );
                    }
                    //Start 11-Jul-2017 mel.andrei.b.santos    GREEN-31830, added condition preventing the contracting of quotes if # of quote lines w/o SPV > 0

                    else if ( cQuote.NoOfQuoteLinesWithoutSPV__c > 0)
                    {
                        ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, settings.get('ErrorNoSPV').Value__c) );

                    }
                    else if ((cQuote.SBQQ__NetAmount__c < Integer.valueOf(settings.get('AmountThreshold').Value__c)) && cQuote.SBQQ__Status__c != 'Approved' && (cQuote.SBQQ__Type__c == 'Amendment' || cQuote.SBQQ__Type__c == 'Quote' || cQuote.SBQQ__Type__c == 'Renewal' )) {                        
                        
                        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                        req1.setComments('Submitting request for approval.');
                        req1.setObjectId(cQuote.id);
                        req1.setProcessDefinitionNameOrId('Credit_Note_Approval_Process');
                        req1.setSkipEntryCriteria(false);
                        try{
                            Approval.ProcessResult result = Approval.process(req1);
                            if(result.isSuccess()){
                                //ApexPages.addmessage( new ApexPages.message(ApexPages.severity.CONFIRM, 'Quote has been submitted for approval. You will be notified when the quote has been approved.') );
                                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.CONFIRM, settings.get('ThresholdAndStatusErrorMsg').Value__c) );
                            }
                            
                        }catch(System.DmlException e){
                            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                            //errLog.Name = String.valueOf(cQuote.id);
                            errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(cQuote.id);
                            errLog.ffps_custRem__Detail__c = e.getMessage();
                            errLog.ffps_custRem__Grouping__c   = 'Approval Process';
                            errLog.ffps_custRem__Message__c  = 'Approval Process via Complete Quote Button';
                            insert errLog;

                            ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, 'An error has occured in submitting the record for approval. Please contact your administrator.') );
                        }
                    }
                    else {
                    
                        Savepoint sp = Database.setSavepoint();

                        try {
                            System.debug('@@@Check JVCO_QuoteComplete__c: ' +  cQuote.JVCO_QuoteComplete__c);

                            //04-Nov-2019 robert.j.b.lacatan GREEN-35072
                            if(cQuote.SBQQ__Type__c == 'Amendment'){
                                List<SBQQ__QuoteLine__c> qlRecList = new List<SBQQ__QuoteLine__c>();
                                for(SBQQ__QuoteLine__c ql:[SELECT id, SBQQ__Quantity__c, SBQQ__PriorQuantity__c from SBQQ__QuoteLine__c where SBQQ__Quote__c =: cquote.id AND SBQQ__EffectiveQuantity__c = 0]){
                                    if(ql.SBQQ__PriorQuantity__c != ql.SBQQ__Quantity__c){
                                         ql.SBQQ__PriorQuantity__c = ql.SBQQ__Quantity__c;
                                         qlRecList.add(ql);
                                    } 
                                }
                                SBQQ.TriggerControl.disable();
                                if(!qlRecList.isEmpty()){
                                    update qlRecList;
                                }
                                SBQQ.TriggerControl.enable();
                            }
                            
                            if(!cQuote.JVCO_QuoteComplete__c)
                            {
                                if(cQuote.NoOfQuoteLinesWithoutSPV__c == 0)
                                {
                                    //jules.osberg.a.pablo GREEN-31760, GREEN-32139 Added requirement of T&C's before Contracting Quotes.
                                    cQuote.Accept_TCs__c = selectedTC;

                                    cQuote.JVCO_T_Cs_Accepted_Via__c = selectedTCVia;
                                    
                                    cQuote.JVCO_QuoteComplete__c = true;
                                    cQuote.JVCO_Cancelled__c = false;
                                    cQuote.SBQQ__Status__c = 'Accepted';
                                    cQuote.SBQQ__Primary__c = true;
                                    cQuote.DateContracted__c = system.now();

                                    update cQuote;
                                    System.debug('@@@cQuote updated: ' + cQuote.JVCO_QuoteComplete__c);
                                    
                                    // Start mel.andrei.b.santos Updated controller to run JVCO_CompleteQuoteOrdered first before JVCO_CompleteQuoteContracted as per GREEN-26719
                                    if(!cQuote.SBQQ__Opportunity2__r.SBQQ__Contracted__c)
                                    {
                                        /*
                                        cQuote.SBQQ__Opportunity2__r.StageName = 'Closed Won';
                                        cQuote.SBQQ__Opportunity2__r.Probability = 100;
                                        cQuote.SBQQ__Opportunity2__r.SBQQ__Contracted__c = true; */
                                        //cQuote.SBQQ__Opportunity2__r.SBQQ__Ordered__c = true;

                                        System.debug('@@@ Entering Limits.getQueueableJobs: ' + Limits.getQueueableJobs());
                                        if(Limits.getQueueableJobs() == 0)
                                        {
                                            System.debug('@@@Limits.getQueueableJobs: ' + Limits.getQueueableJobs());
                                           // JVCO_CompleteQuoteOrdered CompleteQuoteOrdered = new JVCO_CompleteQuoteOrdered(cQuote.SBQQ__Opportunity2__r, oldQuote);
                                            JVCO_CompleteQuoteContracted CompleteQuoteContracted = new JVCO_CompleteQuoteContracted(cQuote.SBQQ__Opportunity2__r , cQuote, oldQuote); //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
                                            System.enqueueJob(CompleteQuoteContracted);
                                            //System.enqueueJob(CompleteQuoteOrdered);
                                            ApexPages.addmessage( new ApexPages.message(ApexPages.severity.CONFIRM, settings.get('UpdateOppAndQuoteSuccess').Value__c) );
                                        }
                                    }
                                    else if(!cQuote.SBQQ__Opportunity2__r.SBQQ__Ordered__c)
                                    {
                                        cQuote.SBQQ__Opportunity2__r.SBQQ__Ordered__c = true;
                                        //JVCO_CompleteQuoteContracted CompleteQuoteContracted = new JVCO_CompleteQuoteContracted(cQuote.SBQQ__Opportunity2__r , oldQuote);
                                        //System.enqueueJob(CompleteQuoteContracted);
                                        JVCO_CompleteQuoteOrdered CompleteQuoteOrdered = new JVCO_CompleteQuoteOrdered(cQuote.SBQQ__Opportunity2__r, cQuote, oldQuote); //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
                                        System.enqueueJob(CompleteQuoteOrdered);
                                        ApexPages.addmessage( new ApexPages.message(ApexPages.severity.CONFIRM, settings.get('UpdateOppAndQuoteSuccess').Value__c) );
                                    }
                                    else
                                    {
                                        ApexPages.addmessage( new ApexPages.message(ApexPages.severity.WARNING, settings.get('UpdateOppAndQuoteAlreadyContracted').Value__c) );
                                    }
                                }
                            }
                            else
                            {
                                system.debug('@@@ UpdateOppAndQuoteAlreadyContracted processing ');
                                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.WARNING, settings.get('UpdateOppAndQuoteAlreadyContracted').Value__c) );
                            }
                            
                        } catch (Exception e) {
                            Database.rollback(sp);
                            ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, settings.get('UpdateOppAndQuoteFail').Value__c + ' ' + e.getMessage()) );
                        }

                    }
                } 
                else {
                    if (cQuote.JVCO_Contact_Type__c == null) {
                        ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, settings.get('MissingMandatoryField_ContactType').Value__c) );
                    }

                    if (cQuote.SBQQ__Status__c == 'User Cancelled') {
                        ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, settings.get('CancelledQuoteErrorMsg').Value__c) );
                    }
               
                }
            } else {

            	//Moved Contact Type message
                if (cQuote.JVCO_Contact_Type__c == null) {
                    ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, settings.get('MissingMandatoryField_ContactType').Value__c) );
                }

                if (cQuote.Covid_19_Impacted1__c == null) {
                    ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, settings.get('MissingMandatoryField_Covid19Impacted').Value__c) );
                }

                if (!cQuote.SBQQ__Primary__c) {
                    ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, settings.get('MissingMandatoryField_Primary').Value__c) );
                }
                
                if (cQuote.Start_Date__c == null) {
                    ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, settings.get('MissingMandatoryField_StartDate').Value__c) );
                }

                if (cQuote.SBQQ__SubscriptionTerm__c == null) {
                    ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, settings.get('MissingMandatoryField_SubscriptionTerm').Value__c) );
                }

                if (cQuote.SBQQ__Opportunity2__c == null) {
                    ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, settings.get('MissingMandatoryField_Opportunity').Value__c) );
                }

                if(cQuote.End_Date__c == null || !quoteLineBlankEndDateList.isEmpty()) {
                    ApexPages.addmessage( new ApexPages.message(ApexPages.severity.Error, settings.get('MissingMandatoryField_EndDate').Value__c) );
                }

                if(!cQuote.JVCO_Recalculated__c) {
                    ApexPages.addmessage( new ApexPages.message(ApexPages.severity.Error, settings.get('Recalculation').Value__c) );
                }
            }
        }
        
        return null; 
    }

    public List<String> getQuoteOutputFields(){

        return apiFieldList;
    }
    
    public PageReference returnToQuote() {
    
        PageReference orderPage = new PageReference('/' + cQuote.id); //25-Jan-2018 reymark.j.l.arlos updated from quote to cQuote
        return orderPage;
      
    }

}