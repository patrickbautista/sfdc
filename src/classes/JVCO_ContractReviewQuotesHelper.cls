/* ------------------------------------------------------------------------------------------------
   Name:            JVCO_ContractReviewQuotesHelper
   Description:     Helper class for JVCO_ContractReviewQuotesBatch and JVCO_ContractReviewQuotes_Queueable

   Date            Version     Author                       Summary of Changes 
   -----------     -------     -----------------            ---------------------------------------
   09-Jul-2018     0.1         jules.osberg.a.pablo         Initial Version
   27-Sept-2018    0.2         rhys.j.c.dela.cruz           Added Queueable Status
   22-Mar-2019     0.3         mel.andrei.b.Santos          excluding 0 net amount amendment quotes GREEN-33800
   22-May-2019     0.4         rhys.j.c.dela.cruz           GREEN-33618 - Changes to queueable to accomodate new Temp Object
------------------------------------------------------------------------------------------------ */
public class JVCO_ContractReviewQuotesHelper {
    @testVisible
    private static Boolean testError = false;

  public JVCO_ContractReviewQuotesHelper() {
    
  }

  public static string getQueryString(Account accountRecord) {

    //rhys.j.c.dela.cruz - added addtlQuery, change query if queueable
        String addtlQuery = '';

        if(JVCO_KABatchSetting__c.getInstance(UserInfo.getUserId()).JVCO_ContractReviewQuotesQueueable__c){

            addtlQuery = ' and (SBQQ__Contracted__c = false OR (SBQQ__Contracted__c = true AND SBQQ__Ordered__c = false)) ';
        }
        else{

            addtlQuery = ' and SBQQ__Contracted__c = false ';
        }

        //String query = 'select Id, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName from Opportunity where StageName != \'Closed Won\' and SBQQ__Contracted__c = false and SBQQ__Ordered__c = false and AccountId = ' + '\'' + accountRecord.Id + '\'';
        //String query = 'select Id, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName from Opportunity where SBQQ__Contracted__c = false and AccountId = ' + '\'' + accountRecord.Id + '\'';
        String query = 'select Id, AccountId, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName, SBQQ__PrimaryQuote__r.Name, SBQQ__PrimaryQuote__c from Opportunity where  SBQQ__PrimaryQuote__r.JVCO_Number_Quote_Lines__c != 0 and SBQQ__PrimaryQuote__r.JVCO_Number_Quote_Lines__c != null ' + addtlQuery + ' and SBQQ__RenewedContract__c = NULL and AccountId = ' + '\'' + accountRecord.Id + '\'';
        // jules.osberg.a.pablo 17-10-2017 GREEN-23493 // louis.a.del.rosario 11-30-2017 
        // mel.andrei.b.santos 12-Apr-2018 GREEN-31233 - removed SBQQ__PrimaryQuote__c from condition
        // mariel.m.buena 25-Sep-2018 GREEN-32399 - added Amount in the condition
        query += ' AND SBQQ__PrimaryQuote__r.SBQQ__NetAmount__c != 0 ' ; // mel.andrei.santos 22-03-2019 GREEN-33800

        return query;
    }

  public static string executeContractAmendmentQuotes(List<Opportunity> processOpportunityList) {
        String returnErrorMessage = '';
        String recID = '';

        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>(); // mel.andrei.b.santos 27-07-2017 GREEN-17932
        Map<Id, List<SBQQ__QuoteLineGroup__c>> MapQuoteAndQlg = new  Map<Id, List<SBQQ__QuoteLineGroup__c>>();
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
        Set<Id> quoteIdSet = new Set<Id>();
        Set<Id> tmpOppIds = new Set<Id>();
        Map<String, JVCO_Complete_Quote_Settings__c> settings = JVCO_Complete_Quote_Settings__c.getAll(); //mel.andrei.b.santos GREEN-31830

        //New added
        String quoteRecId = '';
        Map<Id, Id> quoteIdMap = new Map<Id, Id>();

        Savepoint sp = Database.setSavepoint();
    
        //recCount++ ;
        List<Opportunity> lstOfOpportunity = new List<Opportunity>();
        List<sObject> lstObject = new List<sObject>();
        for (Opportunity opp : processOpportunityList) 
        {
            tmpOppIds.add(opp.Id);
            //oppIds.add(opp.Id);
            lstOfOpportunity.add(opp);
            oppMap.put(opp.Id, opp);
        }
        
        if(lstOfOpportunity.size()>0)
        {
            List<SBQQ__Quote__c> quoteList = new List<SBQQ__Quote__c>([select Id, SBQQ__Status__c, SBQQ__Opportunity2__c, SBQQ__NetAmount__c, NoOfQuoteLinesWithoutSPV__c, SBQQ__Type__c, SBQQ__Opportunity2__r.SBQQ__PrimaryQuote__c from SBQQ__Quote__c where (SBQQ__Type__c = 'Quote' or SBQQ__Type__c = 'Amendment') and SBQQ__Primary__c = true and SBQQ__Opportunity2__c in: tmpOppIds]); //23-01-2018 reymark.j.l.arlos added SBQQ__NetAmount__c on query
            if(quoteList.size()>0)
            {
                Set<Id> affilSetId = new Set<Id>();
                for(SBQQ__QuoteLineGroup__c qlg: [SELECT Id, JVCO_IsComplete__c, JVCO_Affiliated_Venue__c, JVCO_Affiliated_Venue__r.JVCO_IsComplete__c,  SBQQ__Quote__c FROM SBQQ__QuoteLineGroup__c WHERE SBQQ__Quote__c in :quoteList AND JVCO_Affiliated_Venue__c != NULL])
                {
                    if(!MapQuoteAndQlg.containsKey(qlg.SBQQ__Quote__c) && qlg.JVCO_Affiliated_Venue__r.JVCO_IsComplete__c)
                        MapQuoteAndQlg.put(qlg.SBQQ__Quote__c, new List<SBQQ__QuoteLineGroup__c>());

                    if(qlg.JVCO_Affiliated_Venue__r.JVCO_IsComplete__c){
                        qlg.JVCO_IsComplete__c = false;
                        MapQuoteAndQlg.get(qlg.SBQQ__Quote__c).add(qlg);    
                    }
                }

                //Update of Quote Object and cast the list of Quote Line Group and Affiliation to List of sObject
                Set<sObject> stObjects = new Set<sObject>();
                for(SBQQ__Quote__c qRec:quoteList)
                {
                    //Start 11-Jul-2018 mel.andrei.b.santos GREEN-31830, added condition preventing the contracting of quotes if # of quote lines w/o SPV > 0  
                    if(qRec.NoOfQuoteLinesWithoutSPV__c > 0)
                    {
                        ffps_custRem__Custom_Log__c qErrLog = new ffps_custRem__Custom_Log__c();

                        //qErrLog.Name = String.valueOf(qRec.ID);
                        qErrLog.ffps_custRem__Related_Object_Key__c = String.valueOf(qRec.ID);
                        qErrLog.ffps_custRem__Detail__c = string.valueof(settings.get('ErrorNoSPV').Value__c);
                        qErrLog.ffps_custRem__Grouping__c = string.valueof(qRec.SBQQ__Opportunity2__c);
                        qErrLog.ffps_custRem__Message__c = 'JVCO_ContractReviewQuotesBatch: No SPV';
                        qerrLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                        errLogList.add(qErrLog); 
                        returnErrorMessage += 'Quote: ' + qRec + ' Cause: '  + qErrLog.ffps_custRem__Detail__c + '<br>';

                    }
                    //END mel.andrei.b.santos     
                    else
                    {
                        //04-Nov-2019 rhys.j.c.dela.cruz GREEN-35072
                        try{
                            
                            if(qRec.SBQQ__Type__c == 'Amendment'){
                                List<SBQQ__QuoteLine__c> qlRecList = new List<SBQQ__QuoteLine__c>();
                                for(SBQQ__QuoteLine__c ql : [SELECT id, SBQQ__Quantity__c, SBQQ__PriorQuantity__c from SBQQ__QuoteLine__c where SBQQ__Quote__c =: qRec.Id AND SBQQ__EffectiveQuantity__c = 0]){
                                    if(ql.SBQQ__PriorQuantity__c != ql.SBQQ__Quantity__c){
                                            ql.SBQQ__PriorQuantity__c = ql.SBQQ__Quantity__c;
                                            qlRecList.add(ql);
                                    } 
                                }

                                if(Test.isRunningTest() && testError){
                                    qlRecList.add(new SBQQ__QuoteLine__c());
                                }

                                SBQQ.TriggerControl.disable();
                                if(!qlRecList.isEmpty()){
                                    update qlRecList;
                                }
                                SBQQ.TriggerControl.enable();
                            }
                        }
                        catch(Exception e){

                            ffps_custRem__Custom_Log__c qErrLog = new ffps_custRem__Custom_Log__c();

                            //qErrLog.Name = String.valueOf(qRec.ID);
                            qErrLog.ffps_custRem__Related_Object_Key__c = String.valueOf(qRec.ID);
                            qErrLog.ffps_custRem__Detail__c = 'Error: ' + e.getMessage();
                            qErrLog.ffps_custRem__Grouping__c = string.valueof(qRec.SBQQ__Opportunity2__c);
                            qErrLog.ffps_custRem__Message__c = 'JVCO_ContractReviewQuotesBatch: Update of Prior Quantity on QL';
                            qerrLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                            errLogList.add(qErrLog); 
                            returnErrorMessage += 'Quote: ' + qRec + ' Cause: '  + qErrLog.ffps_custRem__Related_Object_Key__c + '<br>';
                        }
                        
                        qRec.SBQQ__Status__c = 'Approved';
                        qRec.JVCO_QuoteComplete__c = true;
                        qRec.JVCO_Cancelled__c = false;
                        if(stObjects.add((sObject)qRec)){
                            lstObject.add((sObject) qRec);
                            //quoteIdMap.put(qRec.Id, qRec.Id);
                        }
                        
                        if(oppMap.containsKey(qRec.SBQQ__Opportunity2__c))
                        {
                            Opportunity OppRec = new Opportunity();
                            OppRec = oppMap.get(qRec.SBQQ__Opportunity2__c);
                            OppRec.StageName = 'Closed Won';
                            OppRec.JVCO_OpportunityCancelled__c = false;

                            //Moved setPrimaryQuote here
                            if(OppRec.SBQQ__PrimaryQuote__c == null){
                                OppRec.SBQQ__PrimaryQuote__c = qRec.Id;
                            }
                            OppRec.Amount = qRec.SBQQ__NetAmount__c; //23-Jan-2018 reymark.j.l.arlos setting amount on opp from quote
                            if(stObjects.add((sObject)OppRec)){
                                lstObject.add((sObject) OppRec);
                                quoteIdMap.put(oppRec.Id, qRec.Id);
                            }
                        }
                        
                        if(MapQuoteAndQlg.containsKey(qRec.Id))
                        {
                            for(SBQQ__QuoteLineGroup__c qlg: MapQuoteAndQlg.get(qRec.Id))
                            {
                                if(stObjects.add((sObject)qlg)){
                                    lstObject.add((sObject)qlg);
                                    quoteIdMap.put(qlg.Id, qlg.SBQQ__Quote__c);
                                }
                            }
                            //lstObject.addAll((List<sObject>)MapQuoteAndQlg.get(qRec.Id));
                        }
                    }

                }

                if(lstObject.size() > 0)
                {
                    if(!lstObject.isEmpty())
                    {
                        if(Test.isRunningTest() && testError){                  //Added isRunningTest to create DML error - louis.a.del.rosario
                             lstObject.add(new Opportunity());
                             lstObject.add(new SBQQ__Quote__c());
                             lstObject.add(new SBQQ__QuoteLineGroup__c());
                        }
                        
                        // update oppListForUpdate; start Changed DML statement to database.update as per Ticket GREEN-17932 - Mel Andrei Santos 27-07-2017
                        List<Database.SaveResult> res = Database.update(lstObject,false); // Start 24-08-2017 mel.andrei.b.santos
                        for(Integer i = 0; i < lstObject.size(); i++)
                        {
                            Database.SaveResult srOppList = res[i];
                            sObject origrecord = lstObject[i];
                            if(!srOppList.isSuccess())
                            {
                                Database.rollback(sp);
                                System.debug('Update sObject fail: ');
                                for(Database.Error objErr:srOppList.getErrors())
                                {
                                    ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();

                                    //errLog.Name = String.valueOf(origrecord.ID);
                                    errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(origrecord.ID);
                                    errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                                    errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                                    errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();

                                    if(origrecord.getSObjectType() == Opportunity.sObjectType){
                                        errLog.ffps_custRem__Message__c = 'JVCO_ContractReviewQuotesBatch: Update Opportunity Record';
                                        if(quoteIdMap.containsKey(origrecord.Id)){
                                            quoteRecId = 'Quote: ' + String.valueOf(quoteIdMap.get(origrecord.id));
                                            errLog.ffps_custRem__Related_Object_Key__c += '\nQuote: ' + String.valueOf(quoteIdMap.get(origrecord.id));
                                        }
                                    }
                                    else if(origrecord.getSObjectType() == SBQQ__Quote__c.sObjectType){
                                        errLog.ffps_custRem__Message__c = 'JVCO_ContractReviewQuotesBatch: Update Quote Record';
                                        quoteRecId = 'Quote: ' + String.valueOf(origrecord.Id);
                                        errLog.ffps_custRem__Related_Object_Key__c += '\nQuote: ' + String.valueOf(origrecord.Id);
                                    }
                                    else if(origrecord.getSObjectType() == SBQQ__QuoteLineGroup__c.sObjectType){
                                        errLog.ffps_custRem__Message__c = 'JVCO_ContractReviewQuotesBatch: Update SBQQ__QuoteLineGroup__c Record';
                                        if(quoteIdMap.containsKey(origrecord.Id)){
                                            quoteRecId = 'Quote: ' + String.valueOf(quoteIdMap.get(origrecord.id));
                                            errLog.ffps_custRem__Related_Object_Key__c += '\nQuote: ' + String.valueOf(quoteIdMap.get(origrecord.id));
                                        }
                                    }
                                    // Start  05-Dec-2017   mel.andrei.b.Santos Updated code to consider the error logs in the finish method as fix for GREEN-26379
                                    recID = String.valueOf(origrecord.ID);
                                    //if(errorMessage.length() <= 80000)
                                    //{
                                    //    errorMessage = recId + ' '  + string.valueof(objErr.getStatusCode()) + '<br>';
                                    //}
                                    //else if(errorMessage1.length() <= 80000)
                                    //{
                                    //    errorMessage1 = recId + ' ' + string.valueof(objErr.getStatusCode()) + '<br>';
                                    //}
                                    //else if(errorMessage2.length() <= 80000)
                                    //{
                                    //    errorMessage2 = recId + ' ' + string.valueof(objErr.getStatusCode()) + '<br>';
                                    //}
                                    returnErrorMessage += quoteRecId + ' Record: ' + recId + ' '  + string.valueof(objErr.getStatusCode()) + '<br>';
                                    //recErrCount++;

                                    //END

                                    errLogList.add(errLog);   

                                }
                            }
                        }
                    }
                }

                if(errLogList.isempty())
                {
                    /* Moved code to first part of class
                    // Start mel.andrei.b.santos 12-04-2018 GREEN-31233
                    Set<ID> idOppSet = new Set<ID>();
                    for(SBQQ__Quote__c qRec:quoteList)
                    {
                        idOppSet.add(qRec.SBQQ__Opportunity2__c);
                        system.debug('@@@ check idOppSet ' + idOppset);
                    }
                    
                    List<Opportunity> setOpp = new List<Opportunity>();
                    setOpp = [Select ID,SBQQ__PrimaryQuote__c from Opportunity where id in :idOppSet];
                    system.debug('@@@ check setOpp ' + setOpp);
                    JVCO_OpportunityTriggerHandler.setPrimaryQuote(setOpp);

                    // END */

                    for(SBQQ__Quote__c qRec:quoteList)
                    {

                        try
                        {                                 
                            SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractGenerator',qRec.SBQQ__Opportunity2__c , null);
                        }
                        catch(Exception e)
                        {
                            String ErrorLineNumber = e.getLineNumber() != Null ? String.ValueOf(e.getLineNumber()):'None';
                            String ErrorMessage = 'Cause: ' + e.getCause() + ' ErrMsg: ' + e.getMessage();
                            if(System.isQueueable()){
                                errLogList.add(JVCO_ErrorUtil.logError(qRec.SBQQ__Opportunity2__c, ErrorMessage , ErrorLineNumber , 'JVCO_ContractReviewQuotesQueueable:SBQQ.ContractManipulationAPI'));    
                            }
                            else{
                                errLogList.add(JVCO_ErrorUtil.logError(qRec.SBQQ__Opportunity2__c, ErrorMessage , ErrorLineNumber , 'JVCO_ContractReviewQuotesBatch:SBQQ.ContractManipulationAPI'));    
                            }

                            returnErrorMessage += 'Quote: ' + quoteRecId +  ' Line Number: ' + ErrorLineNumber + ' '  + ErrorMessage + '<br>';
                            Database.rollback(sp);
                        }
                    }
                }

                if(!errLogList.isempty()){
                     try
                     {
                         insert errLogList;
                     }
                     catch(Exception ex)
                     {
                         System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                         
                     }
                }
            }
        }
        

        if(!errLogList.isempty())
        {
            try
            {
                insert errLogList;

                //03-June-2019 rhys.j.c.dela.cruz Upserting kaTempHolder record if Contracting Failed
                 if(System.isQueueable()){

                    JVCO_KATempHolder__c kaTempHolder = new JVCO_KATempHolder__c();
                    kaTempHolder.Name = processOpportunityList[0].Id;
                    kaTempHolder.JVCO_AccountId__c = processOpportunityList[0].AccountId;
                    kaTempHolder.JVCO_OppId__c = processOpportunityList[0].Id;
                    kaTempHolder.JVCO_Contracted__c = false;
                    kaTempHolder.JVCO_ErrorMessage__c = returnErrorMessage;

                    upsert kaTempHolder Name;
                }
            }
            catch(Exception ex)
            {
                System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
            }
        }
        else{

            if(System.isQueueable()){

                JVCO_KATempHolder__c kaTempHolder = new JVCO_KATempHolder__c();
                kaTempHolder.Name = processOpportunityList[0].Id;
                kaTempHolder.JVCO_AccountId__c = processOpportunityList[0].AccountId;
                kaTempHolder.JVCO_OppId__c = processOpportunityList[0].Id;
                kaTempHolder.JVCO_Contracted__c = true;

                upsert kaTempHolder Name;
            }
        }

        return returnErrorMessage;
    }

    public static void sendCompletionEmail(Account accountRecord, Id userId, Integer recCount, Integer recErrCount, string batchErrorMessage, Integer querySize) {
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        //Set to setTargetObjectId to not use limtis
        mail.setTargetObjectId(userId);
        mail.setSaveAsActivity(false);

        mail.setReplyTo('no-reply@salesforce.com');
        if(System.isQueueable()){
            mail.setSenderDisplayName('Queueable Processing');
            mail.setSubject('Contract Review Quotes Queueable Process Completed');
        }else{
            mail.setSenderDisplayName('Batch Processing');
            mail.setSubject('Contract Review Quotes Batch Process Completed');
        }
        
        List<JVCO_KATempHolder__c> kaTempList = new List<JVCO_KATempHolder__c>();
        Integer totalContracts = 0;

        //29-May-2019 rhys.j.c.dela.cruz - GREEN-33618 - Use KA Temp Holder for count. Check if queueable
        if(System.isQueueable()){

            kaTempList = [SELECT Id, JVCO_AccountId__c, JVCO_OppId__c, JVCO_Contracted__c, JVCO_Ordered__c, JVCO_ProcessCompleted__c, JVCO_ErrorMessage__c FROM JVCO_KATempHolder__c WHERE JVCO_AccountId__c =: accountRecord.Id AND JVCO_OppId__c != null];

            totalContracts = 0;
            recErrCount = 0;
            batchErrorMessage = '';

            if(!kaTempList.isEmpty()){

                for(JVCO_KATempHolder__c kaTempRec : kaTempList){

                    if(kaTempRec.JVCO_Contracted__c){
                        totalContracts++;
                    }
                    else if(!kaTempRec.JVCO_Contracted__c){
                        recErrCount++;
                        batchErrorMessage += '<li>' + kaTempRec.JVCO_ErrorMessage__c + '</li>';
                    }
                }
            }
        }
        else{

            totalContracts = querySize - recErrCount;
        }

        //Integer totalContracts = contractBatchSize * a.JobItemsProcessed; // 05-Dec-2017   mel.andrei.b.Santos Updated code to consider the error logs in the finish method as fix for GREEN-26379
        //Integer totalContracts = querySize - recErrCount;
        //mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus);
        //mail.setPlainTextBody('This is to notify you that the process to Contract Review Quotes has now completed for ' + accountRecord.Name + '.');
        String emailMsg = '<html><body>This is to notify you that the process to Contract Review Quotes has now completed for ' + accountRecord.Name + ' (' +System.now() + ') .<br><br>Number of Review Quotes Contracted: ' + totalContracts;
        
         // Start  05-Dec-2017   mel.andrei.b.Santos Updated code to consider the error logs in the finish method as fix for GREEN-26379
         /*
        if(errorMessage != '') {
            emailMsg = emailMsg + '. Number of failures: ' + (contractBatchSize * a.NumberOfErrors) + '</br> + Error: ' + errorMessage;
        } */

        //if(recErrCount > 0) 
        if(batchErrorMessage != '') {
            //emailMsg = emailMsg + '<br><br>Number of failures: ' + recErrCount + '<br>Error: <br>' + errorMessage + '' + errorMessage1 + '' + errorMessage2;
            emailMsg += '<br><br>Number of failures: ' + recErrCount + '<br>Please see errors below: <br><ul>' + batchErrorMessage + '</ul>';
        }

        emailMsg += ' </body></html> ';

        //End
        
        mail.setHTMLBody(emailMsg);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

        if(System.isQueueable()){
            JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_ContractReviewQuotesQueueable__c', '','JVCO_ContractReviewQuotesLastSubmitted__c');
        }else{
            JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_ContractReviewQuotesApexJob__c', '','JVCO_ContractReviewQuotesLastSubmitted__c');
        }
    }

    //Method for sending Email of Started Job
    public static void sendBeginJobEmail(Account accountRecord, Integer sizeOfJob, Boolean queueableProcess){

        User currentUser = [SELECT Id, Email FROM USER WHERE Id = :UserInfo.getUserId() LIMIT 1];

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(currentUser.Id);
        mail.setSaveAsActivity(false);

        mail.setReplyTo('no-reply@salesforce.com');
        if(queueableProcess){
            mail.setSenderDisplayName('Queueable Processing');
            mail.setSubject('Contract Review Quotes Queueable Process Started');
        }else{
            mail.setSenderDisplayName('Batch Processing');
            mail.setSubject('Contract Review Quotes Batch Process Started');
        }
        
        String emailMsg = '<html><body>This is to notify you that the process to Contract Review Quotes has now been started for ' + accountRecord.Name + ' ('+ System.now() + '). <br><br>Number of Review Quotes to be Contracted: ' + sizeOfJob + ' records.';
        emailMsg += ' </body></html> ';

        mail.setHTMLBody(emailMsg);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

}