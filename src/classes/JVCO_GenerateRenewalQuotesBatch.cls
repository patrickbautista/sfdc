/* ----------------------------------------------------------------------------------------------
  Name: JVCO_GenerateRenewalQuotesBatch
  Description: Batch Class for updating Contracts related to an Account
 
  Date            Version     Author              Summary of Changes 
  -----------     -------     -----------------   -----------------------------------------
  03-Mar-2017     0.1         Marlon Ocillos      Intial creation
  13-Oct-2017     0.2         Jules pablo         Added GREEN-24781 Fix, Updated Email message
  02-Nov-2017     0.3         Jules pablo         Added GREEN-25156 Functionality. 
  If a new venue has been added with start date which falls under the time frame of the true-up (amendment quotes), a blank quote of quote type should be generated 
  If a new venue has been added with start date which falls under the renewal year, then a blank quote of quote type should not be created but a blank quote should be created for it.
  20-Nov-2017     0.4         Jules pablo         Chained JVCO_KeyAccountQuoteCreationBatch in the finish method to create blank quotes
  06-Feb-2018     0.5         JUles pablo         GREEN-28126 Added JVCO_Omit_Quote_Line_Group__c = FALSE Filter in the Affiliation query
  02-Apr-2018     0.6         August Del rosario  GREEN-30531 Removed the blank renewal quotes that is being generated.
  ----------------------------------------------------------------------------------------------- */
global class JVCO_GenerateRenewalQuotesBatch implements Database.Batchable<sObject>, Database.Stateful
{
    global final Account accountRecord;
    global Integer contractBatchSize;
    global Set<Id> contractIds; //02-Nov-2017 GREEN-25156 jules.osberg.a.pablo
    global Integer quoteCount;
    global Integer errorQuoteCount;
    global String batchErrorMessage;

    global JVCO_GenerateRenewalQuotesBatch(Account a)
    {
        contractIds = new Set<Id> (); //02-Nov-2017 GREEN-25156 jules.osberg.a.pablo
        accountRecord = a;
        contractBatchSize = 1;   
        quoteCount = 0;
        errorQuoteCount = 0;
        batchErrorMessage = '';

        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        if (asCS.JVCO_Contract_Batch_Size__c != null && asCS.JVCO_Contract_Batch_Size__c > 0) {
            contractBatchSize = Integer.valueOf(asCS.JVCO_Contract_Batch_Size__c);
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = JVCO_GenerateRenewalQuotesHelper.getQueryString(accountRecord);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        if (scope.size() > 0)
        {   
            String errorMessage = JVCO_GenerateRenewalQuotesHelper.executeRenewalQuotes((List<Contract>) scope, accountRecord);
            if(errorMessage == ''){
                quoteCount++;
            }else{
                errorQuoteCount++;
                batchErrorMessage += '<li>' + errorMessage + '</li>';
            }
            for (Contract contractRecord : (List<Contract>) scope) {
                contractIds.add(contractRecord.Id);
            }
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        /*List<Contract> contractsToFix = [Select Id from contract where Id in :contractIds and SBQQ__RenewalQuoted__c = false and JVCO_RenewableQuantity__c > 0];
        if (contractsToFix.Size() > 0)
        {
            // Send the failed Contracts to a Future method which will recursively repeat until all Failed Contracts are completed
            ID jobID = System.enqueueJob(new JVCO_GenerateRenewalQuoteQueueable(contractsToFix, 0));
        }*/

        //START 02-Nov-2017 GREEN-25156 jules.osberg.a.pablo
        List<JVCO_Affiliation__c> affilationsForCreation = new List<JVCO_Affiliation__c> ();
        List<SBQQ__Quote__c> generatedRenewalQuotes = new List<SBQQ__Quote__c> ();
        Date endDate = NULL;
        List<Contract> listContractProcessed = [select Id, EndDate, ForceDeferred__c,SBQQ__RenewalQuoted__c,JVCO_Renewal_Generated__c,JVCO_RenewableQuantity__c from Contract where SBQQ__RenewalQuoted__c = true and JVCO_Renewal_Generated__c = false and JVCO_RenewableQuantity__c > 0 and AccountId =:accountRecord.Id];
        List<Contract> listContractErrors =    [select Id, EndDate, ForceDeferred__c,SBQQ__RenewalQuoted__c,JVCO_Renewal_Generated__c,JVCO_RenewableQuantity__c from Contract where SBQQ__RenewalQuoted__c = false and JVCO_Renewal_Generated__c = false and JVCO_RenewableQuantity__c > 0 and AccountId =:accountRecord.Id];
        generatedRenewalQuotes = [select Id, Name, End_Date__c from SBQQ__Quote__c where SBQQ__Opportunity2__r.SBQQ__RenewedContract__c in :contractIds];
        if (!generatedRenewalQuotes.isEmpty()) {
            Integer loopCtr = 0;
            for (SBQQ__Quote__c qRec : generatedRenewalQuotes) {
                if (loopCtr == 0) {
                    endDate = qRec.End_Date__c;
                } else {
                    if (qRec.End_Date__c > endDate) {
                        endDate = qRec.End_Date__c;
                    }
                }
                loopCtr++;
            }
        }

        //GREEN-28126 jules.osberg.a.pablo Added JVCO_Omit_Quote_Line_Group__c = FALSE Filter
        if (endDate == NULL) {
            affilationsForCreation = [select Id, Name, JVCO_Venue__r.Name, JVCO_Account__c, JVCO_Venue__c, JVCO_Start_Date__c, JVCO_End_Date__c, JVCO_Omit_Quote_Line_Group__c from JVCO_Affiliation__c where JVCO_Omit_Quote_Line_Group__c = FALSE and JVCO_Account__c = :accountRecord.Id and JVCO_End_Date__c = null AND Id NOT in (Select Affiliation__c from SBQQ__Subscription__c where SBQQ__Contract__r.JVCO_Renewal_Generated__c = false and SBQQ__Contract__r.AccountId = :accountRecord.Id) AND Id NOT in (Select JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c where SBQQ__Quote__r.SBQQ__Status__c = 'Draft' and SBQQ__Quote__r.SBQQ__Account__c = :accountRecord.Id)]; //Add GREEN-24997 jules.osberg.a.pablo
        } else {
            affilationsForCreation = [select Id, Name, JVCO_Venue__r.Name, JVCO_Account__c, JVCO_Venue__c, JVCO_Start_Date__c, JVCO_End_Date__c, JVCO_Omit_Quote_Line_Group__c from JVCO_Affiliation__c where JVCO_Omit_Quote_Line_Group__c = FALSE and JVCO_Account__c = :accountRecord.Id and JVCO_Start_Date__c <= :endDate and JVCO_End_Date__c = null AND Id NOT in (Select Affiliation__c from SBQQ__Subscription__c where SBQQ__Contract__r.JVCO_Renewal_Generated__c = false and SBQQ__Contract__r.AccountId = :accountRecord.Id) AND Id NOT in (Select JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c where SBQQ__Quote__r.SBQQ__Status__c = 'Draft' and SBQQ__Quote__r.SBQQ__Account__c = :accountRecord.Id)]; //02-Nov-2017 GREEN-25156 jules.osberg.a.pablo
        }

        //start 06-11-2017 mariel.m.buena GREEN-24769 Querying pricebook to map it to opportunity
        Map<Id, Pricebook2> pricebookId = new Map<Id, Pricebook2> ([Select Id, Name
                                                                   From Pricebook2
                                                                   Where isStandard = true and isActive = true]);
        //end

        //start GREEN-25866 20-11-2017 jules.osberg.a.pablo
        Id pbId = pricebookId.values().size() > 0 ? pricebookId.values().get(0).Id : Null;
        AsyncApexJob aJob = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id = :BC.getJobId()];
        //start GREEN-26655 15-12-2017 louis.a.del.rosario
        //Integer totalQuotes = listContractProcessed.size();
        //Integer totalErrorQuotes = listContractErrors.size();
        Set<String> setContractIdFailed = new Set<String>();
        for(Contract c: listContractErrors){
            setContractIdFailed.add(c.Id);
        }
        //String errorMessage = String.valueOf(aJob.ExtendedStatus);

        Integer quoteBatchSize = 50;
        JVCO_Constants__c asCS = JVCO_Constants__c.getOrgDefaults();
        if (asCS.Key_Account_Blank_Quote_Batch_Size__c != null && asCS.Key_Account_Blank_Quote_Batch_Size__c > 0) {
            quoteBatchSize = Integer.valueOf(asCS.Key_Account_Blank_Quote_Batch_Size__c);
        }

        if (!affilationsForCreation.isEmpty()) {
            if (affilationsForCreation.size() > quoteBatchSize) {
                Id batchId = database.executeBatch(new JVCO_GenerateBlankQuotesBatch(accountRecord, endDate, pbId, quoteCount, errorQuoteCount, batchErrorMessage, false), quoteBatchSize);
            } else {
                JVCO_KeyAccountQuote.createQuote(affilationsForCreation, pbId, endDate, accountRecord);
                JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_GenerateRenewQuotesApexJob__c', '','JVCO_GenerateRenewQuotesLastSubmitted__c');
                JVCO_KeyAccountQuote.sendEmailNotification(accountRecord, affilationsForCreation.size(), 0, batchErrorMessage, quoteCount, errorQuoteCount, aJob.Createdby.Email, false, setContractIdFailed);
            }
        } else {
            JVCO_KeyAccountQuote.sendEmailNotification(accountRecord, 0, 0, batchErrorMessage, quoteCount, errorQuoteCount, aJob.Createdby.Email, false, setContractIdFailed);
             JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_GenerateRenewQuotesApexJob__c', '','JVCO_GenerateRenewQuotesLastSubmitted__c');
        }
        //End GREEN-26655 15-12-2017 louis.a.del.rosario
        //end
    }
}