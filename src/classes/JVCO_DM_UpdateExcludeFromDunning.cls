/*----------------------------------------------------------------------------------------------
Name: JVCO_DM_UpdateExcludeFromDunning.cls 
Description: Batch class that updates the Excluded from Dunning Cycle checkbox of the Account to true if the credit status is Excluded from Dunning
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
05-Feb-2018  0.1         desiree.m.quijada     Initial creation
----------------------------------------------------------------------------------------------- */

public class JVCO_DM_UpdateExcludeFromDunning implements Database.Batchable<sObject> {
    
    public static List<Account> accountList;

    public static void init()
    {   
        List<Account> accountUpdateList = new List<Account>();
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();

        if(!accountList.isEmpty()){
            //for every account whose credit status = Excluded from Dunning update the Excluded from Dunning Cycle checkbox to true
            for(Account a : accountList)
            {
                a.ffps_custRem__Exclude_From_Reminder_Process__c = true;
                accountUpdateList.add(a);
            }   
        }

        if(Test.isRunningTest()){
            accountUpdateList[0].JVCO_Customer_Account__c = null;
        }

        //Update the Accounts
        List<Database.SaveResult> saveResList = Database.update(accountUpdateList,false);

        for(Integer i=0; i < accountUpdateList.size(); i++){
        Database.SaveResult indRes = saveResList[i];
        Account acc = accountUpdateList[i];

        //every failed update or error will be logged to the error log object ffps_custRem__Custom_Log__c
        if(!indRes.isSuccess()){
          for(Database.Error err : indRes.getErrors()) {
              ffps_custRem__Custom_Log__c newErr =  new ffps_custRem__Custom_Log__c();
              if(acc.ID != null || !String.isBlank(String.valueOf(acc.ID))){
                if(err.getMessage() != null || !String.isBlank(String.valueOf(err.getMessage()))){
                    if(err.getStatusCode() != null || !String.isBlank(String.valueOf(err.getStatusCode()))){
                        /*
                        newErr.JVCO_FailedRecordObjectReference__c = String.valueOf(acc.ID);
                        system.debug(acc.id);
                        newErr.Name = String.valueOf(acc.ID);
                        system.debug(newErr.Name);
                        newErr.blng__ErrorCode__c = String.valueof(err.getStatusCode());
                        newErr.blng__FullErrorLog__c = String.valueof(err.getMessage());
                        newErr.JVCO_ErrorBatchName__c = 'Update In Exclude from Dunning Cycle';
                        */

                        newErr.ffps_custRem__Related_Object_Key__c = String.valueOf(acc.ID);
                        newErr.ffps_custRem__Grouping__c = String.valueof(err.getStatusCode());
                        newErr.ffps_custRem__Message__c = String.valueof(err.getMessage());
                        newErr.ffps_custRem__Message__c= 'Update In Exclude from Dunning Cycle';
                    } 
                }
                errLogList.add(newErr);
              }
          }
        }
        else{
          System.debug('SUCCESS');
        }
      }
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT ID, JVCO_Credit_Status__c, ffps_custRem__Exclude_From_Reminder_Process__c
                                        FROM Account
                                        WHERE JVCO_Credit_Status__c = 'Excluded from Dunning']);
    }

    public void execute(Database.BatchableContext BC, List<Account> scope) {

        accountList = scope;
        init();
    }
    
    public void finish(Database.BatchableContext BC) {
        System.debug('Done');
    }
    
}