/* ----------------------------------------------------------------------------------------------------------
    Name:            JVCO_LicAccOrderAmendmentBatchTest.cls 
    Description:     Batch class for JVCO_LicAccOrderAmendmentBatch
    Test class:      

    Date             Version     Author                            Summary of Changes 
    -----------      -------     -----------------                 -------------------------------------------
    17-July-2020       0.1        Accenture-rhys.j.c.dela.cruz         Initial version of the code 
 ---------------------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_LicAccOrderAmendmentBatchTest {

    @testSetup static void setupTestData() 
    {
        List<JVCO_Constants__c> custSetting = new List<JVCO_Constants__c>();
        custSetting.add(new JVCO_Constants__c(JVCO_Quote_Recalculation_Time_Buffer__c = 20000));
        insert custSetting;
        
        List<JVCO_Renewal_Settings__c> settings = new List<JVCO_Renewal_Settings__c>();
        settings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Generation', Days__c = 29));
        insert settings;
        
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();  
        insert qProcess;

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;
		
        //Account acc =  JVCO_TestClassObjectBuilder.createCustomerAccount();
        //insert acc;

        //Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        //acc2.JVCO_Renewal_Scenario__c = '';
        //insert acc2;
		
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        insert dt;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account acc = new Account();
        acc.RecordTypeId = customerRT;
        acc.Name = 'Test Account 83202-C';
        acc.Type = 'Key Account';

        c2g__codaDimension1__c dim = new c2g__codaDimension1__c();
        dim.c2g__ReportingCode__c = 'BC008TEST';
        dim.Name = 'General Purpose';
        insert dim;

        acc.c2g__CODADimension1__c = dim.Id;

        insert acc;
        
        Account acc2 = new Account();
        acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.Id);
        insert acc2;


        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_End_Date__c = null;
        aff.JVCO_Closure_Reason__c = null;
        insert aff;

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        opp.JVCO_OpportunityCancelled__c = false;
        insert opp;

        Contract c1 = new Contract();
        c1.SBQQ__RenewalQuoted__c = false;  
        c1.AccountID = acc2.Id;
        c1.StartDate = System.today();
        c1.ContractTerm = 12;
        c1.SBQQ__RenewalQuoted__c = false;
        c1.JVCO_Renewal_Generated__c = false;
        c1.JVCO_RenewableQuantity__c = 1;
        c1.EndDate = System.today().addMonths(12);
        c1.Name = 'Test Contract 18023-L';
        c1.SBQQ__Opportunity__c = opp.Id;    
        c1.JVCO_TempStartDate__c = System.today().addMonths(3);
        c1.JVCO_TempEndDate__c = System.today().addMonths(4);
        c1.JVCO_ProcessedByCreditsBatch__c = true;
        insert c1;
        

        SBQQ__Subscription__c sub = new SBQQ__Subscription__c();
        sub.SBQQ__Contract__c = c1.Id;
        sub.SBQQ__Account__c = acc2.Id;
        sub.SBQQ__Quantity__c = 300;
        sub.SBQQ__Product__c = prod.Id;
        sub.SBQQ__ListPrice__c = 1;
        sub.SBQQ__NetPrice__c = 300;
        sub.SBQQ__RenewalQuantity__c = 300;
        sub.ChargeYear__c = 'Current Year';
        sub.SBQQ__BillingFrequency__c = 'Annual';
        sub.SBQQ__BillingType__c = 'Advance';
        sub.SBQQ__ChargeType__c = 'Recurring';
        sub.SBQQ__CustomerPrice__c = 1;
        sub.SBQQ__ProrateMultiplier__c = 1;
        sub.SBQQ__RegularPrice__c = 1;
        sub.LicenceFee__c = 300;
        sub.Start_Date__c = System.today();
        sub.SBQQ__SubscriptionStartDate__c = System.today();
        sub.SBQQ__SubscriptionEndDate__c = System.today().addMonths(12);
        sub.End_Date__c = System.today().addMonths(12);

        insert sub;
		
        Test.startTest();
        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, 'Annually');
        q.JVCO_Quote_Auto_Renewed__c = true;
        q.SBQQ__Status__c = 'Draft';
        q.Recalculate__c = true;
        q.SBQQ__Type__c = 'Quote';
        q.SBQQ__MasterContract__c = c1.Id;
        q.SBQQ__Primary__c = true;
        insert q;
        SBQQ.TriggerControl.disable();
        
        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];
        
        q.SBQQ__Type__c = 'Amendment';
        update q;
        
        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, prod.Id);

        //ql.CreatedDate = Datetime.Now();
        //ql.JVCO_Salesforce_Last_Recalculated_Time__c = Datetime.Now().addDays(3);
        ql.SPVAvailability__c = true;
        insert ql;
		SBQQ.TriggerControl.enable();
        
        JVCO_TempInvoiceProcessingData__c tempRec = new JVCO_TempInvoiceProcessingData__c();
        tempRec.JVCO_Batch_Name__c = 'JVCO_LicAccGenerateAmendmentBatch';
        tempRec.JVCO_Id1__c = c1.Id;
        insert tempRec;

        Test.stopTest();
        //q.SBQQ__Type__c = 'Renewal';
        //q.JVCO_Quote_Auto_Renewed__c = true;
        //q.SBQQ__Status__c = 'Draft';
        //q.Recalculate__c = true;
        //ql.JVCO_Salesforce_Last_Recalculated_Time__c = Datetime.Now().addDays(3);
        //update q;

        //system.assertEquals(null, [select id, SBQQ__LineItemCount__c, JVCO_Recalculated__c from  sbqq__quote__c where id = :q.id]);

    }

    @isTest
    static void testLicAccOrderAmendmentBatch()
    {

        SBQQ__Quote__c q = [select Id, JVCO_Quote_Auto_Renewed__c, JVCO_Recalculated__c from SBQQ__Quote__c  limit 1];
        Opportunity o = [select id,JVCO_OpportunityCancelled__c,SBQQ__Contracted__c,SBQQ__Ordered__c, JVCO_Auto_Renewed__c, SBQQ__PrimaryQuote__r.SBQQ__NetAmount__c,
                         SBQQ__PrimaryQuote__r.SBQQ__Type__c,
                         SBQQ__PrimaryQuote__r.JVCO_Recalculated__c,
                         SBQQ__PrimaryQuote__r.SBQQ__LineItemCount__c,
                         SBQQ__PrimaryQuote__r.SBQQ__Primary__c,
                         SBQQ__PrimaryQuote__r.NoOfQuoteLinesWithoutSPV__c,                   
                         SBQQ__PrimaryQuote__r.SBQQ__MasterContract__c,
                         SBQQ__PrimaryQuote__r.SBQQ__MasterContract__r.JVCO_ContractScenario__c,
                         SBQQ__PrimaryQuote__r.JVCO_Last_Recalculated_Time_Delta__c from Opportunity limit 1];

        q.Recalculate__c = true;
        update q;

        o.SBQQ__Contracted__c = TRUE;
        o.SBQQ__Ordered__c = FALSE;
        update o;
        
        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_OrderRenewalBatch(), 1);
        Test.stopTest();

    }
}