/* ----------------------------------------------------------------------------------------------
Name: JVCO_TermsAndConditionHelper
Description: Helper Class for GREEN-31760 and GREEN-32139 enhancement.
             Added requirement of T&C's before Contracting Quotes.

Date            Version     Author              Summary of Changes 
-----------     -------     -----------------   -----------------------------------------
07-Jul-2018     0.1         jules.osberg.a.pablo      Intial creation
26-October-2020             sean.g.perez              adding getTCAcceptedViaValues method  
----------------------------------------------------------------------------------------------- */

public class JVCO_TermsAndConditionHelper {
    public JVCO_TermsAndConditionHelper(){

    }


    /*
     *   @Description    : gets the picklist values of JVCO_T_C_s_Accepted_Via__c to be used in vf page
     *   @Return         : picklist values of JVCO_T_C_s_Accepted_Via__c field
     *   @Author         : sean.g.perez
     *   @Date Created   : 26-Oct-2020
     *   @Date Modified  : 
    */
    public List<Selectoption> getTCAcceptedViaValues(){
        List<Selectoption> picklistValue = new List<selectoption>();
        
        Schema.DescribeFieldResult fieldResult = Account.JVCO_T_C_s_Accepted_Via__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            if(System.Label.Payment_of_Pre_Contract_Invoice != pickListVal.getValue()){
                picklistValue.add(new selectOption(pickListVal.getValue(), pickListVal.getLabel()));
            }
        }
        
        return picklistValue; 
    }

    public List<Selectoption> getselectedTCfields(string tcValue){
        List<Selectoption> picklistValue = new List<selectoption>();
        if(tcValue != 'No'){
            picklistValue.add(new selectOption('', '--None--'));
        }
        //picklistValue.add(new selectOption('Yes', 'Yes'));
        //picklistValue.add(new selectOption('No', 'No'));
        
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Account.TCs_Accepted__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            picklistValue.add(new selectOption(pickListVal.getValue(), pickListVal.getLabel()));
        }

        return picklistValue; 
    }

    public boolean updateCustomerTC(string selectedTC, string savedTC, id customerId){
        if(selectedTC != savedTC){
            List<Account> accountList = [SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :customerId LIMIT 1];
            List<Account> updateAccountList = new List<Account>();
            for(account acc :accountList){
                acc.TCs_Accepted__c = selectedTC;
                updateAccountList.add(acc);
            }

            try{
                if(!updateAccountList.isEmpty()){
                    update accountList;
                    return true;
                }
            }catch (Exception e){
                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()) );
                return false;
            }
        }

        return true;
    }

    public boolean tcIsYes(string tcValue){
        if(tcValue == 'Yes'){
            return true;   
        }else{
            return false;
        }
    }

    //public PageReference tcUpdateAction(string tcValue, PageReference returnRecord){
    //  if(tcIsYes(tcValue)){
    //      //Dont Redirect the page when t&c is 'yes'
    //      return null;
    //  }else{
    //      //Redirect the page when t&c is not 'yes'
    //      return returnRecord;
    //  }
         
    //}

    public boolean bipassTCWindow(List<Opportunity> oppsToProcess, boolean defaultValue){
        Set<Id> oppIdSet = new Set<Id>();
        for(Opportunity opp :oppsToProcess){
            if(opp.SBQQ__PrimaryQuote__c != NULL){
                oppIdSet.add(opp.Id);
            }
        }

        AggregateResult[] singleSocietyQuotes = [SELECT Count(Id) singleSocietyQuotes FROM SBQQ__Quote__c WHERE JVCO_Single_Society__c = TRUE AND SBQQ__Opportunity2__c IN :oppIdSet];

        if(singleSocietyQuotes[0].get('singleSocietyQuotes') != NULL){
            if((oppIdSet.size() - Integer.valueOf(singleSocietyQuotes[0].get('singleSocietyQuotes'))) == 0){
                return true;
            }
        }
        return defaultValue;
    }
}