/* ----------------------------------------------------------------------------------------------
    Name: JVCO_BillingLineUtil.cls 
    Description: Handler Class for Billing Invoice Line

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    06-Jul-2017   0.1         franz.g.a.dimaapi   Initial creation
    12-Sep-2017   0.2         eu.rey.t.cadag      The routine JVCO_BillingLineUtil.populateVenueAndAffiliation should be modified based on GREEN-22927
    14-Sep-2017   0.3         franz.g.a.dimaapi   Added createCopyOfDimension2 method - GREEN-21231
    11-Oct-2017   0.4         franz.g.a.dimaapi   Added removedChargedDates method - GREEN-23782
    05-11-2017    0.5         zara.h.ferjani      Changes made to fix GREEN-25146 issue
    16-Nov-2017   0.5         reymark.j.l.arlos   refactored the code in populateVenueAndAffiliation for GREEN-26048
    20-Dec-2017   0.6         ashok.kumar.ramanna  Week and Dimension 1 were empty on the True Up Report for Sales Credit Note.for GREEN-27088
    26-Jan_2018   0.7         mary.ann.a.ruelan    GREEN-28394 updated updateTrueUpFlag
    16-Mar-2018   0.8         mary.ann.a.ruelan    GREEN-30413 updated populateVenueAndAffiliation to populate Terminated Line  
    21-Aug-2018   0.9         franz.g.a.dimaapi    GREEN-30040 - Added populateBInvLineOrderProduct method
    23-Nov-2018   1.0         franz.g.a.dimaapi    GREEN-32839 - Stamped Start/End Date to Temp Dates
----------------------------------------------------------------------------------------------- */
public class JVCO_BillingLineUtil
{
    private static final Id dummyOrderProduct = JVCO_General_Settings__c.getInstance().JVCO_Dummy_Order_Product_Id__c;
    public static void populateVenueAndAffiliation(List<bLng__InvoiceLine__c> bInvLineList)
    {
        Set<Id> SetOfOrder = new Set<Id>();
        for(bLng__InvoiceLine__c invoiceLine: bInvLineList)
        {
            if(invoiceLine.blng__OrderProduct__c != null)
            {
                SetOfOrder.add(invoiceLine.blng__OrderProduct__c);
            }
        }
        
        if(!SetOfOrder.isEmpty())
        {
            // Start 16-11-2017 reymark.j.l.arlos refactored the code to make sure venue and affiliation will be populated GREEN-26048
            Map<Id, OrderItem> MapOrderItem = new Map<Id, OrderItem>([
                                                    SELECT Id, JVCO_Venue__c, Affiliation__c, blng__NextBillingDate__c, 
                                                    blng__NextChargeDate__c, blng__BillThroughDateOverride__c, 
                                                    blng__OverrideNextBillingDate__c, Order.JVCO_WasCancelled__c,
                                                    Order.JVCO_Cancelled__c,
                                                    SBQQ__QuoteLine__r.SBQQ__StartDate__c,
                                                    SBQQ__QuoteLine__r.SBQQ__EndDate__c,
                                                    SBQQ__QuoteLine__r.SBQQ__NetTotal__c, 
                                                    SBQQ__QuoteLine__r.Terminate__c,
                                                    blng__HoldBilling__c
                                                    FROM OrderItem
                                                    WHERE Id IN: SetOfOrder]);

            if(!MapOrderItem.isEmpty())
            {
                for(bLng__InvoiceLine__c invoiceLine: bInvLineList)
                {
                    if(!string.isBlank(invoiceLine.blng__OrderProduct__c))
                    {
                        if(MapOrderItem.containsKey(invoiceLine.blng__OrderProduct__c))
                        {
                            OrderItem ordProd = MapOrderItem.get(invoiceLine.blng__OrderProduct__c);
                            if(string.isBlank(invoiceLine.JVCO_Venue__c))
                            {
                                invoiceLine.JVCO_Venue__c = ordProd.JVCO_Venue__c;
                            }

                            if(string.isBlank(invoiceLine.JVCO_Affiliation__c))
                            {
                                invoiceLine.JVCO_Affiliation__c = ordProd.Affiliation__c;
                            }
                            
                            //GREEN-32902 - Change from OrderItem.UnitPrice to QuoteLine.NetTotal
                            invoiceLine.blng__Subtotal__c = ordProd.SBQQ__QuoteLine__r.SBQQ__NetTotal__c;
                            //GREEN-30413 - checks if the quote line is terminated upon creation of invoice line
                            invoiceLine.JVCO_Terminated_Line__c = ordProd.SBQQ__QuoteLine__r.Terminate__c;
                            invoiceLine.JVCO_Temp_Start_Date__c = ordProd.SBQQ__QuoteLine__r.SBQQ__StartDate__c;
                            invoiceLine.JVCO_Temp_End_Date__c = ordProd.SBQQ__QuoteLine__r.SBQQ__EndDate__c;
                            invoiceLine.JVCO_Date_Override__c = true;
                        }
                    }
                }
                removedChargedDates(MapOrderItem); 
            }
        }
    }
 
    /* ----------------------------------------------------------------------------------------------------------
        Author:         franz.g.a.dimaapi
        Company:        Accenture
        Description:    Creates a copy of dimension 2 formula text field - GREEN-23782
        Inputs:         Map<Id, OrderItem>
        Returns:        n/a
        <Date>          <Authors Name>                      <Brief Description of Change> 
        11-Oct-2017     franz.g.a.dimaapi                    Initial version of the code
        11-Nov-2019     franz.g.a.dimaapi                    GREEN-35075 - Removed OrderItem deactivation and Billing Rule
        ----------------------------------------------------------------------------------------------------------*/
    private static void removedChargedDates(Map<Id, OrderItem> oiMap)
    {
        Set<OrderItem> updatedOrderItemSet = new Set<OrderItem>();
        for(OrderItem oi : oiMap.values())
        {
            if(oi.blng__NextBillingDate__c != null || oi.blng__NextChargeDate__c != null || oi.blng__BillThroughDateOverride__c != null || oi.blng__OverrideNextBillingDate__c != null)
            {
                oi.blng__NextBillingDate__c = null; 
                oi.blng__NextChargeDate__c = null; 
                oi.blng__BillThroughDateOverride__c = null; 
                oi.blng__OverrideNextBillingDate__c = null;
                oi.blng__HoldBilling__c = 'Yes';
                /*if(UserInfo.getUserType() == 'Standard')
                {
                    oi.blng__BillingRule__c = null;
                    oi.SBQQ__Activated__c = false; 
                }*/
                updatedOrderItemSet.add(oi);
            }
        }
        update new List<OrderItem>(updatedOrderItemSet);
    }
    
    /* ----------------------------------------------------------------------------------------------------------
        Author:         mary.ann.a.ruelan
        Company:        Accenture
        Description:    updates dimension 2
        Inputs:         List of invoice line
        Returns:        n/a
        <Date>          <Authors Name>                    <Brief Description of Change> 
        27-Oct-2017     mary.ann.a.ruelan                  Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/ 
    public static void updateDimension2 (List<blng__InvoiceLine__c> invoiceLineList){
        
        Set<Id> InvSet = new Set<Id>();
        Set<Id> ProdSet = new Set<Id>();
        Set<id> OrdProdSet = new Set<id>();
       
        Map<ID, double> InvToInvLineMap = new Map<ID, double>();
        for(blng__InvoiceLine__c blngInvoiceLine : invoiceLineList)
        { 
            //all invoice lines have blng_invoice__c and blng_product__c (required field)
            InvSet.add(blngInvoiceLine.blng__Invoice__c);
            ProdSet.add(blngInvoiceLine.blng__Product__c);
            //only if the invoice line has an existing order product. migrated invoice lines do not have order products
            if(!string.isblank(blngInvoiceLine.blng__OrderProduct__c))
            {
                OrdProdSet.add(blngInvoiceLine.blng__OrderProduct__c);
            }
            
            Double bInvLineSubTotal = blngInvoiceLine.blng__Subtotal__c != null ? blngInvoiceLine.blng__Subtotal__c : 0;
            //summing all invoice lines with the same invoice
            if(!InvToInvLineMap.containsKey(blngInvoiceLine.blng__Invoice__c))
            {
                InvToInvLineMap.put(blngInvoiceLine.blng__Invoice__c, bInvLineSubTotal);
            }else
            {
                InvToInvLineMap.put(blngInvoiceLine.blng__Invoice__c, InvToInvLineMap.get(blngInvoiceLine.blng__Invoice__c)+ bInvLineSubTotal);
            }   
        }

        //Map of related invoices
        Map<Id, blng__Invoice__c> IdInvoiceMap = new Map<Id, blng__Invoice__c>([SELECT Id, JVCO_Migrated__c, 
                                                                                JVCO_Invoice_Legacy_Number__c, 
                                                                                blng__Order__r.JVCO_Gross_Total__c, 
                                                                                blng__Order__r.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c, 
                                                                                JVCO_Total_Amount__c 
                                                                                FROM blng__Invoice__c where Id in : InvSet]);
        //Map of related order products
        Map<Id, OrderItem> IdOrdProdMap = new Map<Id, OrderItem>([Select Id,
                                                                Order.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c 
                                                                FROM OrderItem 
                                                                WHERE Id IN : OrdProdSet]);
        //Map of related products
        Map<Id, Product2> IdProdMap = new Map<Id, Product2>([SELECT Id, JVCO_Legacy_Joint_Tariff_Licence__c, JVCO_Society__c FROM Product2 where Id in : ProdSet]);
        
        //updating dimension 2 and dimension 2 copy
        for(blng__InvoiceLine__c blngInvoiceLine : invoiceLineList)
        {
            Double InvTotalAmt = 0;
            //evaluating invoice total amount
            if (IdInvoiceMap.get(blngInvoiceLine.blng__Invoice__c).JVCO_Total_Amount__c!=NULL)
            {             
                //invoice is existing, sum old invoice total amt and sum of new invoice lines, taking into consideration invoices that already has existing invoice lines          
                InvTotalAmt = IdInvoiceMap.get(blngInvoiceLine.blng__Invoice__c).JVCO_Total_Amount__c + InvToInvLineMap.get(blngInvoiceLine.blng__Invoice__c) ;         
            }else
            {
                InvTotalAmt = InvToInvLineMap.get(blngInvoiceLine.blng__Invoice__c);
            }
            
            //populating dimension_2s __c     
            if(IdInvoiceMap.get(blngInvoiceLine.blng__Invoice__c).JVCO_Migrated__c
                && IdProdMap.get(blngInvoiceLine.blng__Product__c).JVCO_Legacy_Joint_Tariff_Licence__c
                && !String.ISBLANK(IdInvoiceMap.get(blngInvoiceLine.blng__Invoice__c).JVCO_Invoice_Legacy_Number__c))
            {
                blngInvoiceLine.Dimension_2s__c = IdInvoiceMap.get(blngInvoiceLine.blng__Invoice__c).JVCO_Invoice_Legacy_Number__c.LEFT(3);   
            }else if(InvTotalAmt<0 && IdOrdProdMap.containsKey(blngInvoiceLine.blng__OrderProduct__c)
                    && IdOrdProdMap.get(blngInvoiceLine.blng__OrderProduct__c).Order.SBQQ__Quote__r.SBQQ__MasterContract__c!=NULL
                    && !String.ISBLANK(IdOrdProdMap.get(blngInvoiceLine.blng__OrderProduct__c).Order.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c)
                    && IdProdMap.get(blngInvoiceLine.blng__Product__c).JVCO_Legacy_Joint_Tariff_Licence__c)
            {
                blngInvoiceLine.Dimension_2s__c = IdOrdProdMap.get(blngInvoiceLine.blng__OrderProduct__c).Order.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c.LEFT(3);
            }else
            {
                blngInvoiceLine.Dimension_2s__c = IdProdMap.get(blngInvoiceLine.blng__Product__c).JVCO_Society__c;
            }
        }
    }
    
   /* ----------------------------------------------------------------------------------------------------------
        Author:         zara.h.ferjani
        Company:        Accenture
        Description:    Update True Up Flag on record 
        Inputs:         List of invoice line
        Returns:        n/a
        <Date>          <Authors Name>                      <Brief Description of Change> 
        05-Nov-2017     zara.h.ferjani                      Changes made to fix GREEN-25146 issue 
        26-Jan-2018     mary.ann.a.ruelan                   Fix to GREEN-23894, edited logic to also check true up flag based on period start date of invoice
    ----------------------------------------------------------------------------------------------------------*/ 
    public static void updateTrueUpFlag(List<blng__InvoiceLine__c> invoiceLineList)
    {
        Set<Id> invoiceIDSet = new Set<Id>();
        for(blng__InvoiceLine__c invoiceItemObj : invoiceLineList)
        {
            invoiceIDSet.add(invoiceItemObj.blng__Invoice__c);
        }
       
        Map<ID,blng__Invoice__c>  invoiceMap = new Map<ID,blng__Invoice__c>([Select id,  blng__Order__r.SBQQ__Quote__r.SBQQ__Type__c, JVCO_Licence_Start_Date__c from blng__Invoice__c where id in :invoiceIDSet]);        
       
        for(blng__InvoiceLine__c invoiceItemObj : invoiceLineList)
        {
            blng__Invoice__c invoice = invoiceMap.get(invoiceItemObj.blng__Invoice__c);
            if((invoiceItemObj.JVCO_Start_Date__c!=null && invoice.JVCO_Licence_Start_Date__c!=null && invoiceItemObj.JVCO_Start_Date__c < invoice.JVCO_Licence_Start_Date__c)
               || (!string.isblank(invoice.blng__Order__r.SBQQ__Quote__c) && invoice.blng__Order__r.SBQQ__Quote__r.SBQQ__Type__c=='Amendment'))
            {
                invoiceItemObj.JVCO_True_Up__c =  true;
            }else
            {
                invoiceItemObj.JVCO_True_Up__c =  false;
            }
        }
    }

    /* ----------------------------------------------------------------------------------------------------------
        Author:         franz.g.a.dimaapi
        Company:        Accenture
        Description:    GREEN-30040 - Populate Order Product for Billing Line for Manual Invoice
        Inputs:         List<blng__InvoiceLine__c>
        Returns:        n/a
        <Date>          <Authors Name>                      <Brief Description of Change> 
        21-Aug-2018     franz.g.a.dimaapi                    Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public static void populateBInvLineOrderProduct(List<blng__InvoiceLine__c> bInvLineList)
    {   
        Set<Id> bInvIdSet = new Set<Id>();
        for(blng__InvoiceLine__c bInvLine : bInvLineList)
        {
            if(bInvLine.blng__OrderProduct__c == null)
            {
                bInvIdSet.add(bInvLine.blng__Invoice__c);
            }    
        }

        if(!bInvIdSet.isEmpty())
        {
            Map<Id, blng__Invoice__c> bInvMap = new Map<Id, blng__Invoice__c>([SELECT Id FROM blng__Invoice__c 
                                                                                WHERE Id IN : bInvIdSet 
                                                                                AND JVCO_Manual_Invoice__c = TRUE]);
            for(blng__InvoiceLine__c bInvLine : bInvLineList)
            {
                if(bInvMap.containsKey(bInvLine.blng__Invoice__c))
                {
                    bInvLine.blng__OrderProduct__c = dummyOrderProduct;
                }
            }
        }   
    }   

    /* ----------------------------------------------------------------------------------------------------------
        Author:         rhys.j.c.dela.cruz
        Company:        Accenture
        Description:    GREEN-35471 - Populate Temp Budget Category Field
        Inputs:         List<blng__InvoiceLine__c>
        Returns:        n/a
        <Date>          <Authors Name>                      <Brief Description of Change> 
        07-Oct-2020     rhys.j.c.dela.cruz                    Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public static void populateTempBudgetCategory(List<blng__InvoiceLine__c> bInvLineList)
    {
        Set<Id> ordItemSet = new Set<Id>();
        Set<Id> affSet = new Set<Id>();

        for(blng__InvoiceLine__c bInvLine : bInvLineList)
        {
            ordItemSet.add(bInvLine.blng__OrderProduct__c);
            affSet.add(bInvLine.JVCO_Affiliation__c);
        }

        //Query for Venue
        Map<Id, OrderItem> ordItemMap = new Map<Id, OrderItem>([SELECT Id, SBQQ__QuoteLine__r.JVCO_Venue__r.JVCO_Budget_Category__c, SBQQ__QuoteLine__r.Affiliation__r.JVCO_Venue__r.JVCO_Budget_Category__c FROM OrderItem WHERE Id IN: ordItemSet]);

        Map<Id, JVCO_Affiliation__c> affMap = new Map<Id, JVCO_Affiliation__c> ([SELECT Id, JVCO_Venue__r.JVCO_Budget_Category__c FROM JVCO_Affiliation__c WHERE Id IN: affSet]);
        
        if(!ordItemMap.isEmpty() || !affMap.isEmpty())
        {
            for(blng__InvoiceLine__c bInvLine : bInvLineList)
            {
                if(affMap.containsKey(bInvLine.JVCO_Affiliation__c))
                {
                    if(affMap.get(bInvLine.JVCO_Affiliation__c).JVCO_Venue__r.JVCO_Budget_Category__c != null)
                    {
                        bInvLine.JVCO_Temp_Budget_Category__c = affMap.get(bInvLine.JVCO_Affiliation__c).JVCO_Venue__r.JVCO_Budget_Category__c;
                    }
                }

                if(bInvLine.JVCO_Temp_Budget_Category__c == null)
                {
                    if(ordItemMap.containsKey(bInvLine.blng__OrderProduct__c))
                    {
                        if(ordItemMap.get(bInvLine.blng__OrderProduct__c).SBQQ__QuoteLine__r.JVCO_Venue__r.JVCO_Budget_Category__c != null)
                        {
                            bInvLine.JVCO_Temp_Budget_Category__c = ordItemMap.get(bInvLine.blng__OrderProduct__c).SBQQ__QuoteLine__r.JVCO_Venue__r.JVCO_Budget_Category__c;
                        }
                        else if(ordItemMap.get(bInvLine.blng__OrderProduct__c).SBQQ__QuoteLine__r.Affiliation__r.JVCO_Venue__r.JVCO_Budget_Category__c != null)
                        {
                            bInvLine.JVCO_Temp_Budget_Category__c = ordItemMap.get(bInvLine.blng__OrderProduct__c).SBQQ__QuoteLine__r.Affiliation__r.JVCO_Venue__r.JVCO_Budget_Category__c;
                        }
                    }
                }
            }
        }
    }
}