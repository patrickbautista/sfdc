/* ----------------------------------------------------------------------------------------------------------
Name:            JVCO_SearchInvoiceControllertest.cls
Description:     websit payment Test class

Date             Version     Author                            Summary of Changes
-----------      -------     -----------------                 -------------------------------------------
20-sep-2017      0.1         Accenture-saket.mohan.jha         Initial version of the code

---------------------------------------------------------------------------------------------------------- */
@IsTest
private class JVCO_SearchInvoiceControllerTest
{
    @testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        //JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        c.MailingPostalCode = 'ne6 5ls';
        c.MailingCity = 'Test City';
        c.MailingCountry = 'Test Country';
        c.MailingState = 'Test State';
        c.MailingStreet = 'Test Street';
        c.FirstName = 'Test Name';
        c.LastName = 'Test Last Name';
        insert c;
        
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        Test.startTest();
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        Test.stopTest();
        
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        o.JVCO_Processed_By_BillNow__c = true;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        
        Set<Id> orderGroupIdSet = new Set<Id>();  
        orderGroupIdSet.add(orderGroup.Id);
        
        JVCO_TestClassHelper.setGeneralSettingsCS();
        
        List<JVCO_TransitionManualReviewNotifSettings__c> settings = new List<JVCO_TransitionManualReviewNotifSettings__c>();
        settings.add(new JVCO_TransitionManualReviewNotifSettings__c(Name = 'Limit', Limit__c = '3000'));
        insert settings;
        
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        
        Database.executeBatch(new JVCO_OrderMultipleBillNowLogicBatch(orderGroupIdSet));
        
        JVCO_Website_Payment_Setting__c st = new  JVCO_Website_Payment_Setting__c();
        st.reCaptchaPublicKey__c = '6Lc4B7sUAAAAAOCIV0yMRPJtMVn60Ew98fr7NRMU';
        st.reCaptchaPrivateKey__c = '6Lc4B7sUAAAAAEJjWV0yiv37wr3GNZlsDvCIo7LA';
        st.name = 'WebsitePaymentSetting';
        st.ThanksPageURL__c = 'https://www.google.com/';
        st.EnvURL__c = 'www.google.com';
        st.CardMaxLimit__c = 100000;
        st.ProductName__c = 'Payonomy Validator';
        st.Version__c = '1.1';
        st.Invalid_Account__c = 'test';
        st.Long_Billing_Street__c = 'test';
        st.Invalid_Postcode__c = 'test';
        st.PaymentLimtMsg__c = 'test';
        st.Recaptcha_Incomplete__c = 'test';
        st.Unpaid_Credit_Note__c = 'test';
        st.reCaptchaPublicKey__c = 'test';
        st.No_Billing_Address__c = 'test';
        st.Invalid_Invoice__c = 'test';
        st.Active_Direct_Debit__c = 'test';
        st.In_Infringement__c = 'test';
        st.OneInvMustBeSelected__c = 'test';
        st.Missing_Invoice_Or_Postcode__c = 'test';
        st.Invalid_Email__c = 'test';
        st.In_Enforcement__c = 'test';
        st.All_Invoices_Paid__c = 'test';
        Insert st;
    }
    static testmethod void constructorTest()
    {
        Account licAcc = [SELECT Id, JVCO_Billing_Contact__c, AccountNumber FROM Account WHERE RecordType.Name = 'Licence Account' AND JVCO_Live__c = false];
        Account custAcc = [SELECT Id, Name, JVCO_Billing_Contact__c FROM Account WHERE RecordType.Name = 'Customer Account'];
        c2g__codaInvoice__c sInv = [SELECT id, name, c2g__Account__c, c2g__Account__r.JVCO_Customer_Account__r.name,
                                   c2g__Account__r.AccountNumber, JVCO_Payment_Method__c, c2g__OutstandingValue__c,
                                   c2g__InvoiceDate__c, c2g__Account__R.JVCO_Billing_Contact__R.MailingStreet, 
                                   c2g__Account__R.JVCO_Billing_Contact__R.MailingCity,c2g__Account__R.JVCO_Billing_Contact__R.MailingState,
                                   c2g__Account__R.JVCO_Billing_Contact__R.MailingPostalCode, c2g__Account__R.JVCO_Billing_Contact__R.FirstName,
                                   c2g__Account__R.JVCO_Billing_Contact__R.lastName, c2g__Account__R.JVCO_Billing_Contact__R.Email,
                                   c2g__Account__R.JVCO_Billing_Contact__R.MailingCountry
                                   FROM c2g__codaInvoice__c limit 1];
        Test.startTest();
        Map<string, JVCO_Website_Payment_Setting__c> allData = JVCO_Website_Payment_Setting__c.getAll();
        List<JVCO_Website_Payment_Setting__c> settings = allData.values();
        
        JVCO_SearchInvoiceController controller = new  JVCO_SearchInvoiceController();
        controller.actNumber = licAcc.AccountNumber;
        controller.pCode = 'ne6 5ls';
        controller.emailRecp = 'Test@gmail.com';
        controller.invn = sInv.name;
        
        controller.submit();
        controller.getSelected();
        controller.totalOutstandingBal = sInv.c2g__OutstandingValue__c;
        controller.selectedInvoices.add(sInv);
        controller.contact.LastName = 'TestLast';
        controller.contact.FirstName = 'testFirst';
        controller.getInvoiceGroup();
        controller.sagePayCallout();
        controller.startAgain();
        Test.stopTest();
    }
    static testmethod void constructorTest2()
    {
        SMP_Payment_Config__c config = new SMP_Payment_Config__c(MOTO_Payment_URL__c = 'https://testpayment.com',
                                                                                        Ecommerce_Payment_URL__c = 'https://testpayment.com',
                                                                                        Authenticate_Internal_URL__c = 'https://testpayment.com',
                                                                                        Authenticate_Ecommerce_URL__c = 'https://testpayment.com');
        insert config;
        Account licAcc = [SELECT Id, JVCO_Billing_Contact__c, AccountNumber FROM Account WHERE RecordType.Name = 'Licence Account' AND JVCO_Live__c = false];
        Account custAcc = [SELECT Id, Name, JVCO_Billing_Contact__c FROM Account WHERE RecordType.Name = 'Customer Account'];
        c2g__codaInvoice__c sInv = [SELECT id, name, c2g__Account__c, c2g__Account__r.JVCO_Customer_Account__r.name,
                                   c2g__Account__r.AccountNumber, JVCO_Payment_Method__c, c2g__OutstandingValue__c,
                                   c2g__InvoiceDate__c, c2g__Account__R.JVCO_Billing_Contact__R.MailingStreet, 
                                   c2g__Account__R.JVCO_Billing_Contact__R.MailingCity,c2g__Account__R.JVCO_Billing_Contact__R.MailingState,
                                   c2g__Account__R.JVCO_Billing_Contact__R.MailingPostalCode, c2g__Account__R.JVCO_Billing_Contact__R.FirstName,
                                   c2g__Account__R.JVCO_Billing_Contact__R.lastName, c2g__Account__R.JVCO_Billing_Contact__R.Email,
                                   c2g__Account__R.JVCO_Billing_Contact__R.MailingCountry
                                   FROM c2g__codaInvoice__c limit 1];
        Test.startTest();
        Map<string, JVCO_Website_Payment_Setting__c> allData = JVCO_Website_Payment_Setting__c.getAll();
        List<JVCO_Website_Payment_Setting__c> settings = allData.values();
        Pagereference ref = Page.Invoices;
        Test.setCurrentPage(ref);
        Apexpages.currentPage().getParameters().put('accc', licAcc.Id);
        JVCO_SearchInvoiceController controller = new  JVCO_SearchInvoiceController();
        controller.actNumber = licAcc.AccountNumber;
        controller.pCode = 'ne6 5ls';
        controller.emailRecp = 'Test@gmail.com';
        controller.invn = sInv.name;
        controller.accountId = licAcc.Id;
        controller.invoiceList = controller.getInvoices(JVCO_WPUtil.checkAndGetAccountifValid(licAcc.Id));
        controller.submit();
        controller.getSelected();
        controller.totalOutstandingBal = sInv.c2g__OutstandingValue__c;
        controller.selectedInvoices.add(sInv);
        controller.firstCollectionDate = date.today().addDays(14);
        controller.contact.LastName = 'TestLast';
        controller.contact.FirstName = 'testFirst';
        controller.getInvoiceGroupDD();
        controller.validateBankDetails();
        controller.validatePaymentDates();
        controller.showSpinner();
        controller.getInstallmentRange();
        controller.back();
        controller.cancel();
        controller.next();
        controller.selectedInvoices.add(sInv);
        controller.finalisePaymentDetails();
        controller.constructPaymentLink();
        controller.directDebit.DD_Bank_Account_Number__c = '00000000';
        controller.directDebit.DD_Bank_Sort_Code__c = '000000';
        controller.directDebit.DD_Account_Number_OK__c = true;
        controller.directDebit.DD_Sort_Code_OK__c = true;
        controller.directDebit.DD_OK__c = true;
        controller.contact = JVCO_TestClassHelper.setContact(custAcc.id);
        String invoiceIdString = '';
        for(c2g__codaInvoice__c invoice :controller.selectedInvoices){
            invoiceIdString = invoiceIdString + invoice.Id + ',';
        }
        controller.constructDirectDebitAndLinkToGin(invoiceIdString);
        Test.stopTest();
    }
    static testmethod void testInvalidAccount()
    {
        JVCO_SearchInvoiceController controller = new  JVCO_SearchInvoiceController();
        controller.actNumber = 'test1 test2 test3';
        controller.invn = '';
        controller.pCode = '';
        test.startTest();
        controller.submit();
        test.stopTest();
    }
    static testmethod void testInvalidInvoice()
    {
        Account licAcc = [SELECT Id, JVCO_Billing_Contact__c, AccountNumber FROM Account WHERE RecordType.Name = 'Licence Account' AND JVCO_Live__c = false];
        JVCO_SearchInvoiceController controller = new  JVCO_SearchInvoiceController();
        controller.actNumber = licAcc.AccountNumber;
        controller.pCode = '';
        controller.invn = 'test error';
        test.startTest();
        controller.submit();
        test.stopTest();
    }
    static testmethod void testInvalidPostCode()
    {
        Account licAcc = [SELECT Id, JVCO_Billing_Contact__c, AccountNumber FROM Account WHERE RecordType.Name = 'Licence Account' AND JVCO_Live__c = false];
        JVCO_SearchInvoiceController controller = new  JVCO_SearchInvoiceController();
        controller.actNumber = licAcc.AccountNumber;
        controller.pCode = 'Test Error';
        controller.invn = '';
        test.startTest();
        controller.submit();
        test.stopTest();
    }
    static testmethod void testMissingInvoiceOrInvoice()
    {
        Account licAcc = [SELECT Id, JVCO_Billing_Contact__c, AccountNumber FROM Account WHERE RecordType.Name = 'Licence Account' AND JVCO_Live__c = false];
        
        JVCO_SearchInvoiceController controller = new  JVCO_SearchInvoiceController();
        controller.actNumber = licAcc.AccountNumber;
        controller.pCode = '';
        controller.invn ='';
        test.startTest();
        controller.submit();
        test.stopTest();
    }
    static testmethod void testInfringement()
    {
        Account licAcc = [SELECT Id, JVCO_Billing_Contact__c, AccountNumber, JVCO_In_Infringement__c FROM Account WHERE RecordType.Name = 'Licence Account' AND JVCO_Live__c = false];
        licAcc.JVCO_In_Infringement__c = true;
        
        JVCO_SearchInvoiceController controller = new  JVCO_SearchInvoiceController();
        controller.actNumber = licAcc.AccountNumber;
        controller.pCode = 'ne6 5ls';
        controller.invn ='';
        test.startTest();
        update licAcc;
        controller.submit();
        test.stopTest();
    }
    static testmethod void testEnforcement()
    {
        Account licAcc = [SELECT Id, JVCO_Billing_Contact__c, AccountNumber, JVCO_In_Enforcement__c FROM Account WHERE RecordType.Name = 'Licence Account' AND JVCO_Live__c = false];
        licAcc.JVCO_In_Enforcement__c = true;
        
        JVCO_SearchInvoiceController controller = new  JVCO_SearchInvoiceController();
        controller.actNumber = licAcc.AccountNumber;
        controller.pCode = 'ne6 5ls';
        controller.invn ='';
        test.startTest();
        update licAcc;
        controller.submit();
        test.stopTest();
    }
    static testmethod void testLongMailingStreet()
    {
        Account licAcc = [SELECT Id, JVCO_Billing_Contact__c, AccountNumber, JVCO_In_Enforcement__c FROM Account WHERE RecordType.Name = 'Licence Account' AND JVCO_Live__c = false];
        Contact cont = new Contact();
        cont.id = licAcc.JVCO_Billing_Contact__c;
        cont.MailingStreet = '12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901';
        update cont;
        JVCO_SearchInvoiceController controller = new  JVCO_SearchInvoiceController();
        controller.actNumber = licAcc.AccountNumber;
        controller.pCode = 'ne6 5ls';
        controller.invn ='';
        test.startTest();
        controller.submit();
        test.stopTest();
    }
    static testmethod void testInvInDD()
    {
        Account licAcc = [SELECT Id, JVCO_Billing_Contact__c, AccountNumber FROM Account WHERE RecordType.Name = 'Licence Account' AND JVCO_Live__c = false];
        Account custAcc = [SELECT Id, Name, JVCO_Billing_Contact__c FROM Account WHERE RecordType.Name = 'Customer Account'];
        c2g__codaInvoice__c sInv = [SELECT id, name, c2g__Account__c, c2g__Account__r.JVCO_Customer_Account__r.name,
                                   c2g__Account__r.AccountNumber, JVCO_Payment_Method__c, c2g__OutstandingValue__c,
                                   c2g__InvoiceDate__c, c2g__Account__R.JVCO_Billing_Contact__R.MailingStreet, 
                                   c2g__Account__R.JVCO_Billing_Contact__R.MailingCity,c2g__Account__R.JVCO_Billing_Contact__R.MailingState,
                                   c2g__Account__R.JVCO_Billing_Contact__R.MailingPostalCode, c2g__Account__R.JVCO_Billing_Contact__R.FirstName,
                                   c2g__Account__R.JVCO_Billing_Contact__R.lastName, c2g__Account__R.JVCO_Billing_Contact__R.Email,
                                   c2g__Account__R.JVCO_Billing_Contact__R.MailingCountry
                                   FROM c2g__codaInvoice__c limit 1];
        JVCO_SearchInvoiceController controller = new  JVCO_SearchInvoiceController();
        controller.actNumber = licAcc.AccountNumber;
        controller.pCode = 'ne6 5ls';
        controller.invn = '';
        controller.checkInvoiceDD = true;
        test.startTest();
        controller.submit();
        controller.getSelected();
        controller.selectedInvoices.add(sInv);
        controller.contact.LastName = 'TestLast';
        controller.contact.FirstName = 'testFirst';
        controller.getInvoiceGroup();
        controller.sagePayCallout();
        test.stopTest();
    }
    static testmethod void testInvalidEmail()
    {
        Account licAcc = [SELECT Id, JVCO_Billing_Contact__c, AccountNumber FROM Account WHERE RecordType.Name = 'Licence Account' AND JVCO_Live__c = false];
        JVCO_SearchInvoiceController controller = new  JVCO_SearchInvoiceController();
        controller.actNumber = licAcc.AccountNumber;
        controller.pCode = 'ne6 5ls';
        controller.emailRecp = 'testtesttest';
        controller.invn = '';
        controller.checkInvoiceDD = false;
        test.startTest();
        controller.submit();
        controller.contact.LastName = 'TestLast';
        controller.contact.FirstName = 'testFirst';
        controller.getInvoiceGroup();
        controller.sagePayCallout();
        test.stopTest();
    }
    static testmethod void testMaxLimit()
    {
        Account licAcc = [SELECT Id, JVCO_Billing_Contact__c, AccountNumber FROM Account WHERE RecordType.Name = 'Licence Account' AND JVCO_Live__c = false];
        JVCO_SearchInvoiceController controller = new  JVCO_SearchInvoiceController();
        controller.actNumber = licAcc.AccountNumber;
        controller.invn = '';
        controller.pCode = 'ne6 5ls';
        controller.totalOutstandingBal = 200000;
        controller.checkInvoiceDD = false;
        test.startTest();
        controller.submit();
        controller.contact.LastName = 'TestLast';
        controller.contact.FirstName = 'testFirst';
        controller.getInvoiceGroup();
        controller.sagePayCallout();
        test.stopTest();
    }
}