/* ----------------------------------------------------------------------------------------------
    Name: JVCO_SurchargeInvoiceCancellationBatch.cls 
    Description: Batch class that flags Surcharge Billing Invoice records to be cancelled;
                Cancellation happens after generation of Sales Credit Notes for each Billing Invoice

    Date          Version     Author              Summary of Changes 
    -----------   -------     -----------------   -----------------------------------------
    20-Apr-2017   0.1         ryan.i.r.limlingan  Intial creation
    16-Nov-2017   0.2         franz.g.a.dimaapi   Changed Class Functionality
    04-Dec-2017   0.3         filip.bezak         Due to Issue #26335 changed value on Credit Note field Credit Reason from 'Invoice Amendment' to 'Surcharge Cancelled'
    08-Mar-2018   0.6         franz.g.a.dimaapi   GREEN-30828 - Refactor class and change logics to improve performance
    08-May-2018   0.7         franz.g.a.dimaapi   GREEN-31765 - Update the period, currency, duedate logic.
    10-Oct-2018   0.8         franz.g.a.dimaapi   GREEN-32724 - Refactor
    30-May-2019   0.9         franz.g.a.dimaapi   GREEN-34648 - Add Auto Cancellation of Surcharge
----------------------------------------------------------------------------------------------- */
public class JVCO_SurchargeInvoiceCancellationBatch implements Database.Batchable<sObject>, Database.Stateful
{   
    private Map<Id, c2g__codaCreditNote__c> sInvIdToCNoteMap;
    private Map<Id, c2g__codaCreditNote__c> origSInvIdToCnoteMap = new Map<Id, c2g__codaCreditNote__c>();
    
    public JVCO_SurchargeInvoiceCancellationBatch(Map<Id, c2g__codaCreditNote__c> sInvIdToCNoteMap)
    {
        this.sInvIdToCNoteMap = sInvIdToCNoteMap;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([SELECT Id, c2g__Account__c,
                                         c2g__Account__r.JVCO_Customer_Account__c,
                                         JVCO_Surcharge_Generation_Date__c,
                                         JVCO_Licence_Start_Date__c,
                                         JVCO_Licence_End_Date__c,
                                         JVCO_Original_Invoice__c,
                                         (
                                             SELECT Id, Name
                                             FROM Surcharge_Invoices__r
                                             WHERE JVCO_From_Convert_to_Credit__c = FALSE
                                             AND c2g__PaymentStatus__c NOT IN ('Part Paid','Paid')
                                         )
                                         FROM c2g__codaInvoice__c
                                         WHERE Id IN :sInvIdToCNoteMap.keySet()]);
    }

    public void execute(Database.BatchableContext BC, List<c2g__codaInvoice__c> scope)
    {   
        stopperMethod();
        createCreditNoteForSurcharge(scope);
        bulkPostCreditNote(origSInvIdToCnoteMap.values());
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Create Credit Note Mapping
        Inputs: List<c2g__codaInvoice__c>, Map<Id, c2g__codaCreditNote__c>
        Returns: c2g__codaCreditNote__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void createCreditNoteForSurcharge(List<c2g__codaInvoice__c> sInvList)
    {
        List<c2g__codaCreditNote__c> tempCnoteList = new List<c2g__codaCreditNote__c>();
        List<c2g__codaInvoice__c> surSInvList = new List<c2g__codaInvoice__c>();
        Set<Id> excludeUpdateForCancelOrderSet = new Set<Id>();
        for(c2g__codaInvoice__c sInv : sInvList)
        {
            if(sInv.JVCO_Original_Invoice__c != null){
                c2g__codaInvoice__c origInvoice = new c2g__codaInvoice__c();
                origInvoice.Id = sInv.JVCO_Original_Invoice__c;
                origInvoice.JVCO_Surcharge_Generated__c = false;
                origInvoice.JVCO_Surcharge_Generation_Date__c = null;
                //origInvoice.JVCO_Cancelled__c = true;
                surSInvList.add(origInvoice);
                excludeUpdateForCancelOrderSet.add(origInvoice.Id);
            }
            
            //If there's a surcharge then create credit note.
            if(!sInv.Surcharge_Invoices__r.isEmpty())
            {
                c2g__codaCreditNote__c cNote = createCreditNote(sInv.c2g__Account__c, sInv.JVCO_Licence_Start_Date__c, sInv.JVCO_Licence_End_Date__c);
                //In case there's a multiple surcharge per original salesinvoice due to some issues
                cNote.c2g__Invoice__c = sInv.Surcharge_Invoices__r.size() == 1 ? sInv.Surcharge_Invoices__r[0].Id : null;
                cNote.JVCO_Reference_Document__c = sInv.Surcharge_Invoices__r.size() == 1 ? sInv.Surcharge_Invoices__r[0].Name : null;
                for(c2g__codaInvoice__c surSInv : sInv.Surcharge_Invoices__r)
                {
                    surSInv.JVCO_From_Convert_to_Credit__c = true;
                    surSInv.JVCO_Cancelled__c = true;
                    surSInv.JVCO_Credit_Reason__c = 'Original Invoice Cancelled';
                    surSInvList.add(surSInv);
                }
                tempCnoteList.add(cNote);
                origSInvIdToCnoteMap.put(sInv.Id, cNote);
                sInvIdToCNoteMap.put(sInv.Surcharge_Invoices__r[0].Id, cNote);
            }
        }
        
        if(!surSInvList.isEmpty())
        {
            update surSInvList;
            
            if(!origSInvIdToCnoteMap.isEmpty()){
                insert tempCnoteList;
                createCreditNoteLineForSurcharge(surSInvList, origSInvIdToCnoteMap);
            }
            
            List<Order> orderUpdatedList = new List<Order>();
            
            for(Order ord : [SELECT Id, JVCO_Surcharge_Generated__c,
                             JVCO_Cancelled__c,
                             JVCO_Sales_Credit_Note__c,
                             JVCO_Sales_Invoice__c
                             FROM Order
                             WHERE JVCO_Sales_Invoice__c IN: surSInvList])
            {
                ord.JVCO_Cancelled__c = excludeUpdateForCancelOrderSet.contains(ord.JVCO_Sales_Invoice__c) ? ord.JVCO_Cancelled__c : true;
                ord.JVCO_Surcharge_Generated__c = false;
                if(sInvIdToCNoteMap.containsKey(ord.JVCO_Sales_Invoice__c))
                    ord.JVCO_Sales_Credit_Note__c = sInvIdToCNoteMap.get(ord.JVCO_Sales_Invoice__c).Id;
                orderUpdatedList.add(ord);
            }
            update orderUpdatedList;
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Create Aggregate Mapping for Credit Note Line
        Inputs: List<c2g__codaInvoice__c>, Map<Id, c2g__codaCreditNote__c>
        Returns: c2g__codaCreditNote__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void createCreditNoteLineForSurcharge(List<c2g__codaInvoice__c> surSInvList, Map<Id, c2g__codaCreditNote__c> origSInvIdToCnoteMap)
    {   
        List<c2g__codaCreditNoteLineItem__c> cNoteLineItemList = new List<c2g__codaCreditNoteLineItem__c>();
        for(AggregateResult ar : [SELECT c2g__Invoice__r.JVCO_Original_Invoice__c originalSInvId, 
                                    c2g__Dimension1__c dim1Id,
                                    c2g__Dimension2__c dim2Id, 
                                    c2g__Dimension3__c dim3Id,
                                    c2g__Product__c prodId, 
                                    c2g__TaxCode1__c taxCodeId,
                                    SUM(c2g__TaxValue1__c)totalTaxValue,
                                    SUM(c2g__NetValue__c)totalNetValue
                                    FROM c2g__codaInvoiceLineItem__c
                                    WHERE c2g__Invoice__c IN : surSInvList
                                    GROUP BY c2g__Invoice__r.JVCO_Original_Invoice__c, 
                                    c2g__Dimension2__c, 
                                    c2g__Dimension1__c,
                                    c2g__Dimension3__c, 
                                    c2g__Product__c, 
                                    c2g__TaxCode1__c])
        {
            Id origSInvId = (Id)ar.get('originalSInvId');
            cNoteLineItemList.add(createCredNoteLineItem(ar, origSInvIdToCNoteMap.get(origSInvId).Id));
        }

        insert cNoteLineItemList;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Credit Note
        Inputs: Id, Date, Date
        Returns: c2g__codaCreditNote__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private c2g__codaCreditNote__c createCreditNote(Id accId, Date periodStartDate, Date periodEndDate)
    {
        c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
        cNote.c2g__Account__c = accId;
        cNote.c2g__CreditNoteDate__c = System.today();
        cNote.c2g__DeriveCurrency__c = true;
        cNote.c2g__DerivePeriod__c = true;
        cNote.c2g__DeriveDueDate__c = true;
        cNote.JVCO_Credit_Reason__c = 'Surcharge Cancelled';
        cNote.JVCO_Period_Start_Date__c = periodStartDate;
        cNote.JVCO_Period_End_Date__c = periodEndDate;
        cNote.JVCO_From_Post_And_Match__c = true;
        return cNote;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Credit Note Line
        Inputs: AggregateResult, Id
        Returns: c2g__codaCreditNoteLineItem__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private c2g__codaCreditNoteLineItem__c createCredNoteLineItem(AggregateResult ar, Id cNoteId)
    {
        c2g__codaCreditNoteLineItem__c cNoteLine = new c2g__codaCreditNoteLineItem__c();
        cNoteLine.c2g__CreditNote__c = cNoteId;
        cNoteLine.c2g__Dimension1__c = (Id)ar.get('dim1Id');
        cNoteLine.c2g__Dimension3__c = (Id)ar.get('dim3Id');
        cNoteLine.c2g__TaxCode1__c = (Id)ar.get('taxCodeId');
        cNoteLine.c2g__DeriveLineNumber__c = true;
        cNoteLine.c2g__DeriveUnitPriceFromProduct__c = false;
        cNoteLine.c2g__CalculateTaxValueFromRate__c = false;
        cNoteLine.c2g__TaxValue1__c = (Decimal)ar.get('totalTaxValue');
        cNoteLine.c2g__Quantity__c = 1;
        cNoteLine.c2g__UnitPrice__c = (Decimal)ar.get('totalNetValue');
        cNoteLine.c2g__Dimension2__c = (Id)ar.get('dim2Id');
        cNoteLine.c2g__Product__c = (Id)ar.get('prodId');
        return cNoteLine;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Bulk Post CreditNote
        Inputs: List<c2g__codaCreditNote__c> cNoteList
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        16-Nov-2017 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void bulkPostCreditNote(List<c2g__codaCreditNote__c> cNoteList)
    {
        try{
            JVCO_BackgroundMatchingLogic.stopMatching = true;
            c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context(); 
            List<c2g.CODAAPICommon.Reference> refList = new List<c2g.CODAAPICommon.Reference>();
            for (c2g__codaCreditNote__c cNote : cNoteList)
            {
                c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
                reference.Id = cNote.Id;
                refList.add(reference);
            }
            c2g.CODAAPISalesCreditNote_10_0.BulkPostCreditNote(context, refList);
        }
        catch(Exception e){
            insert createErrorLog(cNoteList[0].Id, 'SC-001', e.getMessage(), 'Surcharge Cancellation Batch');
        }
    }

    private void stopperMethod()
    {
        JVCO_FFUtil.stopInvoiceCancellation = true;
        JVCO_FFUtil.stopAccountDLRSHandler = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
    }

    public void finish(Database.BatchableContext BC)
    {
        stopperMethod();
        if(!Test.isRunningTest() && !sInvIdToCNoteMap.isEmpty()){
            Database.executeBatch(new JVCO_CreditNoteBackgroundMatchingBatch(sInvIdToCNoteMap), 1);
        }
    }

    @TestVisible
    private ffps_custRem__Custom_Log__c createErrorLog(Id refId, String errCode, String errMessage, String batchName)
    {
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
        errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(refId);
        errLog.ffps_custRem__Detail__c = errMessage;
        errLog.ffps_custRem__Message__c = batchName;
        errLog.ffps_custRem__Grouping__c = errCode;

        return errLog;
    }
}