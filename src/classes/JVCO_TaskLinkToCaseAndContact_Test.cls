/* ----------------------------------------------------------------------------------------------
   Name: JVCO_TaskLinkToCaseAndContact_Test.cls 
   Description: Test class for JVCO_TaskLinkToCaseAndContact

   Date         Version     Author                Summary of Changes 
   -----------  -------     -----------------     -----------------------------------------
   09-Nov-2016  0.1         kristoffer.d.martin   Intial creation
  ----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_TaskLinkToCaseAndContact_Test {

    /* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Sets up the needed records for testing
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>       <Brief Description of Change> 
    10-Nov-2016 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    @testSetup static void createTestData() 
    {

        JVCO_SLA_Constants__c cs = new JVCO_SLA_Constants__c();
        cs.Name = 'JVCO_SLA_Custom_Settings';
        cs.JVCO_Entitlement_Name__c = 'Customer Service Support';
        cs.JVCO_Case_Origins_with_Entitlement__c = 'Email Webform Live_Chat Request_Callback'
                                                    + ' Inbound_Call White_Mail';
        cs.JVCO_First_Milestone__c = 'First Response';
        cs.JVCO_First_Milestone_Origin__c = 'Email Webform Request_Callback';
        cs.JVCO_First_Milestone_Criteria__c = 'Responded';
        cs.JVCO_Second_Milestone__c = 'Resolution Time';
        cs.JVCO_Second_Milestone_Origin__c = cs.JVCO_Case_Origins_with_Entitlement__c;
        cs.JVCO_Second_Milestone_Criteria__c = 'Closed';
        insert cs;

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 83202-C';
        a1.Type = 'Key Account';
        insert a1;
        
        Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
        a2.RecordTypeId = licenceRT;
        a2.Name = 'Test Account 83202-License';
        a2.JVCO_Customer_Account__c = a1.Id;
        insert a2;

        SlaProcess ep = [SELECT Id FROM SlaProcess WHERE Name='Standard Support Clone' LIMIT 1];

        Entitlement testEntitlement = new Entitlement(Name='Customer Service Support',
                                                      AccountId=a2.Id, SlaProcessId=ep.Id);
        insert testEntitlement;

        // Create New Contact
        Contact contact1 = new Contact();
        contact1.LastName = 'Doe';
        contact1.FirstName = 'John';
        contact1.Email = 'surveyAppUser@hotmail.com';
        contact1.JVCO_Merge_Id__c = '1234';
        insert contact1;

        // Create New Case
        Case case1 = new Case();
        case1.Subject = 'Test--Email';
        case1.Origin = 'Email';
        case1.JVCO_Temp_Case_External_Id__c = '12345';
        insert case1;

        // Create New Activity
        Task task1 = new Task();
        task1.Subject  = 'Follow up Status';
        task1.Type     = 'Call';
        task1.Status   = 'Open';
        task1.Priority = 'Normal';
        task1.JVCO_Temp_Case_External_Id__c = '12345';
        insert task1; 

        // Create New Activity
        Task task2 = new Task();
        task2.Subject  = 'Follow up Status2';
        task2.Type     = 'Call';
        task2.Status   = 'Open';
        task2.Priority = 'Normal';
        task2.JVCO_Temp_Contact_External_Id__c = '1234';
        insert task2; 
    }

    /* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Test the batch run
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>       <Brief Description of Change> 
    10-Nov-2016 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    @isTest static void taskLinkToCaseAndContactRunTest()
    {
        Test.startTest();
        JVCO_TaskLinkToCaseAndContact batchRun = new JVCO_TaskLinkToCaseAndContact();
        Database.executeBatch(batchRun);
        List<Task> taskList = [ select id , lastModifiedDate,  JVCO_Temp_Case_External_Id__c, JVCO_Temp_Contact_External_Id__c, OwnerId , WhoId , WhatId  from Task
                                 where ( ( JVCO_Temp_Case_External_Id__c != null ) OR ( JVCO_Temp_Contact_External_Id__c != null ) )];
        system.assert(taskList != null);

        JVCO_TaskLinkToCaseAndContact batchRun2 = new JVCO_TaskLinkToCaseAndContact(1);     
        Map<Id, String> errorMapList = new Map<Id, String>();
        errorMapList.put(taskList[0].Id, taskList[0].WhoId);
        batchRun2.FailedTask = errorMapList;
        Database.executeBatch(batchRun2);
        taskList = [ select id , lastModifiedDate,  JVCO_Temp_Case_External_Id__c, JVCO_Temp_Contact_External_Id__c, OwnerId , WhoId , WhatId  from Task
                                 where ( ( JVCO_Temp_Case_External_Id__c != null ) OR ( JVCO_Temp_Contact_External_Id__c != null ) )];
        system.assert(taskList != null);

        Test.stopTest();
    }
}