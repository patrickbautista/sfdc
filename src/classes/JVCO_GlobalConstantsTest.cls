@isTest
private class JVCO_GlobalConstantsTest{
    
     private static testMethod void testStaticVariables(){
         System.assertEquals(null, JVCO_GlobalConstants.DEFAULT_TRADING_CURRENCY);
         System.assertEquals(null, JVCO_GlobalConstants.DEFAULT_RECEIVABLE_CONTROL);
         System.assertEquals(null, JVCO_GlobalConstants.DEFAULT_TAX_CODE);
         System.assertNotEquals(null, JVCO_GlobalConstants.ACCOUNT_CUSTOMER_RECORDTYPE_ID);
         System.assertNotEquals(null, JVCO_GlobalConstants.ACCOUNT_LICENCE_RECORDTYPE_ID);
         System.assertNotEquals(null, JVCO_GlobalConstants.ACCOUNT_SUPPLIER_RECORDTYPE_ID);
     }
}