/*
	Created by: Eu Rey Cadag 
	 Date Created: 11-Sept-2017
	 Details: Queueable class to create contract for Opportunity
	 Version: v1.0
	 Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
  13-Oct-2017         1.1         mel.andrei.b.santos              Updated code to avoid duplication of subscription upon completing quote and generating contract as per GREEN-23460
  26-Dec-2017         1.2        Accenture-mel.andrei.b.santos     Updated controller to run JVCO_CompleteQuoteOrdered first before JVCO_CompleteQuoteController as per GREEN-26719
  26-Jan-2018         1.3         Accenture - mel.andrei.b.santos   Updated code to rollback if contract and order fails, and in case order fails, "complete quote" button can still be click to continue generating order as per GREEN-26719 and GREEN-27821
  12-Apr-2018         1.4         mel.andrei.b.santos               GREEN-31233 - added code to call setPrimaryQuote from JVCO_OpportunityTriggerHandler
  23-Apr-2018         1.5         Accenture - reymark.j.l.arlos     GREEN-31608 moved the fix for GREEN-31233 and added a condition before calling the setPrimaryQuote
  04-May-2018         1.6         Accenture-rhys.j.c.dela.cruz      GREEN-31707 - Updated controller to have a flag that will prevent Account Trigger (onUpdate) from running to prevent encountering Locked Row error.
  03-Dec-2018		  1.7		  mel.andrei.b.santos 				GREEN-34091 - Update controller to consider initial value for rollback
 */
public class JVCO_CompleteQuoteContracted implements Queueable {
	 
	Opportunity OppoRecord;
	SBQQ__Quote__c Quote;
	List<sObject> lstObjectRecs = new List<sObject>();
	Boolean noContract = false;
	SBQQ__Quote__c oldQuote; //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
 
	public JVCO_CompleteQuoteContracted(Opportunity OppoRecord , SBQQ__Quote__c Quote, SBQQ__Quote__c QuoteOld) //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
	{
		this.OppoRecord = OppoRecord;
		lstObjectRecs.add((sObject) this.OppoRecord);
		this.Quote = Quote;   
		oldQuote = QuoteOld; //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
	}
 
	public void execute(QueueableContext context)
	{
		JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
		JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;

		if(OppoRecord != null)
		{
			// start reymark.j.l.arlos 23-04-2018 GREEN-31608 moved the fix for GREEN-31233 and added a condition before calling the setPrimaryQuote
			if(string.isBlank(string.valueOf(OppoRecord.SBQQ__PrimaryQuote__c)))
			{
				List<Opportunity> setOpp = new List<Opportunity>();
				setOpp.add(this.OppoRecord);

				JVCO_OpportunityTriggerHandler.setPrimaryQuote(setOpp);
			}
			//end reymark.j.l.arlos 23-04-2018 GREEN-31608
			Savepoint sp = Database.setSavepoint();
			system.debug('### OppoRecord: ' + OppoRecord);
			OppoRecord.StageName = 'Closed Won';
			OppoRecord.Probability = 100;
			OppoRecord.SBQQ__Contracted__c = true;
			OppoRecord.Amount = Quote.SBQQ__NetAmount__c;
			OppoRecord.JVCO_OpportunityCancelled__c = false; //GREEN-33732

			List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>(); 
			try
			{
				if(!lstObjectRecs.isEmpty())
				{
					system.debug('@@@Exec:lstObjectRecs: ' + lstObjectRecs);

					SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractGenerator',OppoRecord.Id , null); 

					if(Test.isRunningTest())
					{
						lstObjectRecs.add(new Opportunity());
					}
					List<Database.SaveResult> res = Database.update(lstObjectRecs,false);
					for(Integer i = 0; i < lstObjectRecs.size(); i++)
					{
						Database.SaveResult srOppList = res[i];
						
						sObject origrecord = lstObjectRecs[i];
						system.debug('Check sropplist: ' + srOppList.isSuccess());
						if(!srOppList.isSuccess())
						{
							Database.rollback(sp);
							noContract = true;
							for(Database.Error objErr:srOppList.getErrors())
							{
								ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                                errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(origrecord.id);
                                errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                                errLog.ffps_custRem__Grouping__c   = string.valueof(objErr.getStatusCode());
                                errLog.ffps_custRem__Message__c  = 'JVCO_CompleteQuoteContracted: Update Opportunity Record';
								errLogList.add(errLog);
							}

							Quote.SBQQ__Opportunity2__r.StageName = 'Quoting';
							Quote.SBQQ__Opportunity2__r.Probability = 50;
							Quote.SBQQ__Opportunity2__r.SBQQ__Contracted__c = false;
							Quote.SBQQ__Status__c = oldQuote.SBQQ__Status__c; //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
							Quote.JVCO_QuoteComplete__c = oldQuote.JVCO_QuoteComplete__c; //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
							Quote.DateContracted__c = system.now();	
							update Quote;
						}
					}
				}
				
				insert errLogList;

				if(Quote.SBQQ__Type__c != 'Amendment')
				{
					Contract conCheck = [Select id from Contract where SBQQ__Opportunity__c = :OppoRecord.id];
					system.debug('@@@ Query Contract ' + conCheck);
				}

			}catch(Exception objErr)
			{
				Database.rollback(sp);
				noContract = true;
				String ErrorLineNumber = objErr.getLineNumber() != Null ? String.ValueOf(objErr.getLineNumber()):'None';
				String ErrorMessage = 'Cause: ' + objErr.getCause() + ' ErrMsg: ' + objErr.getMessage();
				errLogList.add(JVCO_ErrorUtil.logError(String.valueOf(OppoRecord.ID), ErrorMessage, ErrorLineNumber, 'JVCO_CompleteQuoteContracted:ContractManipulationAPI'));    

				if(!Test.isRunningTest())
				{
					insert errLogList;
				}

				Quote.SBQQ__Status__c = oldQuote.SBQQ__Status__c; //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
				Quote.JVCO_QuoteComplete__c = oldQuote.JVCO_QuoteComplete__c; //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
				update Quote; 
			}

		}

		if(!noContract && !Quote.SBQQ__Opportunity2__r.SBQQ__Ordered__c)
		{
			if(!Test.isRunningTest())
			{
				Quote.JVCO_QuoteComplete__c = false;  
				update Quote; 
				JVCO_CompleteQuoteOrdered CompleteQuoteOrdered = new JVCO_CompleteQuoteOrdered(Quote.SBQQ__Opportunity2__r, Quote, oldQuote); //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
				System.enqueueJob(CompleteQuoteOrdered);
			}
		}
	}
}