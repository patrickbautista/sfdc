/* ----------------------------------------------------------------------------------------------
    Name: JVCO_GenerateRenewalQuotesBatchTest
    Description: Test Class for JVCO_GenerateRenewalQuotesBatch

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    03-Mar-2017     0.1         Jules Pablo         Intial creation
----------------------------------------------------------------------------------------------- */
@IsTest
private class JVCO_GenerateRenewalQuotesBatchTest 
{
    @testSetup static void setupTestData() 
    {
        String cat;

        List<JVCO_Primary_Tariff_Mapping__c> settings = new List<JVCO_Primary_Tariff_Mapping__c>();
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Airport'));
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Aircraft')); 
        insert settings;

        JVCO_TestClassObjectBuilder.createBillingConfig();

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        Product2 prod1 = new Product2();
        prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 83202-C';
        a1.Type = 'Key Account';

        c2g__codaDimension1__c dim = new c2g__codaDimension1__c();
        dim.c2g__ReportingCode__c = 'BC008TEST';
        dim.Name = 'General Purpose';
        insert dim;

        a1.c2g__CODADimension1__c = dim.Id;

        insert a1;
        
        Account a2 = new Account();
        a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.Id);
        a2.JVCO_Preferred_Contact_Method__c = 'Letter';
        a2.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
        insert a2;

        Pricebook2 testPB2 = new Pricebook2();
        testPB2.name = 'Standard Price Book';
        testPB2.IsActive = true;
        insert testPB2;
        
        PriceBookEntry pbe = new PriceBookEntry();
        pbe= JVCO_TestClassObjectBuilder.createPriceBookEntry(prod1.Id);
        insert pbe;
        
        
        
        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 83202';
        o.AccountId = a2.Id;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Renewal';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(2);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        insert q;
        
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(2);
        insert ql;
        
        Test.startTest();

        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();
        venueRecord.Name = 'test1';              
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = new JVCO_Affiliation__c();
        affilRecord.JVCO_Account__c = a2.Id;
        affilRecord.JVCO_Venue__c = venueRecord.id;   
        affilRecord.JVCO_Start_Date__c = system.today();   
        Insert  affilRecord;   
        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;   

        Opportunity opp = new Opportunity();
        opp.Id = o.Id;
        opp.Amount = 10;
        opp.StageName = 'Closed Won';
        opp.SBQQ__Contracted__c = true;
        update opp;
       
        
        cat = 'BB';
        JVCO_Venue__c nvenue3 = new JVCO_Venue__c();
        nvenue3.Name = 'Test Venue '+cat;
        nvenue3.JVCO_Lead_Source__c = 'Churn';
        nvenue3.JVCO_Venue_Type__c = 'Airport';
        nvenue3.JVCO_Venue_Sub_Type__c = 'Airport';
        nvenue3.JVCO_Budget_Category__c = 'Transports & Terminals';
        nvenue3.JVCO_City__c = 'City'+cat;
        nvenue3.JVCO_Country__c = 'United Kingdom';
        nvenue3.JVCO_Street__c = 'Street'+cat;
        nvenue3.JVCO_Postcode__c = 'N'+cat+' 7TH';
        insert nvenue3;

        JVCO_Affiliation__c  affilRecord3 = new JVCO_Affiliation__c();
        affilRecord3.JVCO_Account__c = a2.Id;
        affilRecord3.JVCO_Venue__c = nvenue3.id;   
        affilRecord3.JVCO_Start_Date__c = system.today().addMonths(13);
        insert affilRecord3;

        Contract c1 = new Contract();
        c1.SBQQ__RenewalQuoted__c = false;  
        c1.AccountID = a2.Id;
        c1.StartDate = System.today();
        c1.ContractTerm = 12;
        c1.SBQQ__RenewalQuoted__c = false;
        c1.JVCO_Renewal_Generated__c = false;
        c1.JVCO_RenewableQuantity__c = 1;
        c1.EndDate = System.today().addMonths(12);
        c1.Name = 'Test Contract 18023-L';
        c1.SBQQ__Quote__c = q.Id;
        c1.SBQQ__Opportunity__c = o.Id;
        c1.AccountId = a2.Id;
        c1.JVCO_Value__c = 1000;
        insert c1;

        SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(a2.id, prod1.id);
        s.Affiliation__c = affilRecord.Id;
        s.JVCO_Venue__c = venueRecord.Id;
        s.SBQQ__Contract__c = c1.Id;
        s.SBQQ__RenewalQuantity__c = 1;
        s.SBQQ__ListPrice__c = 1;
        s.SBQQ__NetPrice__c = 1;
        s.SBQQ__Quantity__c = 1;
        s.SBQQ__QuoteLine__c = ql.Id;
        s.End_Date__c = System.today().addMonths(12);
        s.Start_Date__c = System.today();
        insert s;

        
        c1.JVCO_RenewableQuantity__c = 1;
        c1.JVCO_Value__c = 1000;
        update c1;

        
        Test.stopTest();
    }


    private static testMethod void testMethodGenerateRenewal() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name, Licence_Account_Number__c from Account where RecordTypeId =: licenceRT limit 1];
        
        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        asCS.JVCO_Contract_Batch_Size__c = 1;

        upsert asCS JVCO_Array_Size__c.Id;
        
        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_GenerateRenewalQuotesBatch(a), 1);
        //List<SBQQ__Quote__c> renewalQuoteList = [SELECT Id FROM SBQQ__Quote__c WHERE SBQQ__Account__c = :a.Id];
        //System.assertEquals(2, renewalQuoteList.size());
        Test.stopTest();
    }

    private static testMethod void testMethodGenerateRenewalwithEndDatedAff() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a2 = [select Id, Name, Licence_Account_Number__c from Account where RecordTypeId =: licenceRT limit 1];
        
        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        asCS.JVCO_Contract_Batch_Size__c = 1;
        upsert asCS JVCO_Array_Size__c.Id;

        List<JVCO_Affiliation__c> updateAfflist = new List<JVCO_Affiliation__c>();
        List<JVCO_Affiliation__c> affList = [SELECT Id, JVCO_End_Date__c, JVCO_Closure_Reason__c FROM JVCO_Affiliation__c];
        for(JVCO_Affiliation__c aff: afflist) {
            aff.JVCO_End_Date__c = system.today().addMonths(20);
            aff.JVCO_Closure_Reason__c = 'Account restructure';
            updateAfflist.add(aff);
        }
        update updateAfflist;

        //Product2 prod1 = [select Id from Product2 limit 1];

        Test.startTest();  

        //string cat = 'AA';
        //JVCO_Venue__c nvenue2 = new JVCO_Venue__c();
        //nvenue2.Name = 'Test Venue '+cat;
        //nvenue2.JVCO_Lead_Source__c = 'Churn';
        //nvenue2.JVCO_Venue_Type__c = 'Airport';
        //nvenue2.JVCO_Venue_Sub_Type__c = 'Airport';
        //nvenue2.JVCO_Budget_Category__c = 'Transports & Terminals';
        //nvenue2.JVCO_City__c = 'City'+cat;
        //nvenue2.JVCO_Country__c = 'United Kingdom';
        //nvenue2.JVCO_Street__c = 'Street'+cat;
        //nvenue2.JVCO_Postcode__c = 'N'+cat+' 7TH';
        //insert nvenue2;

        //JVCO_Affiliation__c  affilRecord2 = new JVCO_Affiliation__c();
        //affilRecord2.JVCO_Account__c = a2.Id;
        //affilRecord2.JVCO_Venue__c = nvenue2.id;   
        //affilRecord2.JVCO_Start_Date__c = system.today();
        //affilRecord2.JVCO_End_Date__c = system.today().addMonths(4);
        //affilRecord2.JVCO_Closure_Reason__c = 'Ceased Trading';
        //affilRecord2.JVCO_Status__c = 'No Longer Trading';
        //insert affilRecord2;

        //Opportunity o2 = new Opportunity();
        //o2.Name = 'Test opportunity 83202';
        //o2.AccountId = a2.Id;
        //o2.CloseDate = System.today() - 5;
        //o2.StageName = 'Draft';
        //insert o2;
        
        //SBQQ__Quote__c q2 = new SBQQ__Quote__c();
        //q2.SBQQ__Type__c = 'Renewal';
        //q2.SBQQ__Status__c = 'Draft';
        //q2.SBQQ__Primary__c = true;
        //q2.SBQQ__Account__c = a2.Id;
        //q2.SBQQ__Opportunity2__c = o2.Id;
        //q2.SBQQ__StartDate__c = System.today() - 5;
        //q2.SBQQ__EndDate__c = System.today().addMonths(3);
        //q2.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        //insert q2;
        
        
        //SBQQ__QuoteLine__c ql2 = new SBQQ__QuoteLine__c();
        //ql2.SBQQ__Quote__c = q2.Id;
        //ql2.SBQQ__Product__c = prod1.Id;
        //ql2.SBQQ__Quantity__c = 1;
        //ql2.SBQQ__Number__c = 1;
        //ql2.SBQQ__PricingMethod__c = 'List';
        //ql2.SBQQ__ListPrice__c = 10;
        //ql2.SBQQ__CustomerPrice__c = 10;
        //ql2.SBQQ__NetPrice__c = 10;
        //ql2.SBQQ__SpecialPrice__c = 10;
        //ql2.SBQQ__RegularPrice__c = 10;
        //ql2.SBQQ__UnitCost__c = 10;
        //ql2.SBQQ__ProratedListPrice__c = 10;
        //ql2.SBQQ__SpecialPriceType__c = 'Custom';
        //q2.SBQQ__StartDate__c = System.today() - 5;
        //q2.SBQQ__EndDate__c = System.today().addMonths(3);
        //insert ql2;
        
           

        //SBQQ__QuoteLineGroup__c  quoteLineGroupRecord2 = new SBQQ__QuoteLineGroup__c();
        //quoteLineGroupRecord2.JVCO_Affiliated_Venue__c = affilRecord2.id;
        //quoteLineGroupRecord2.JVCO_IsComplete__c = true;    
        //quoteLineGroupRecord2.SBQQ__Quote__c = q2.Id;
        //Insert  quoteLineGroupRecord2;   

        //Contract c2 = new Contract();
        //c2.SBQQ__RenewalQuoted__c = false;  
        //c2.AccountID = a2.Id;
        //c2.StartDate = System.today();
        //c2.ContractTerm = 12;
        //c2.SBQQ__RenewalQuoted__c = false;
        //c2.JVCO_Renewal_Generated__c = false;
        //c2.JVCO_RenewableQuantity__c = 1;
        //c2.EndDate = System.today().addMonths(5);
        ////c2.JVCO_Contract_EndDate__c = System.today().addMonths(3);
        //c2.Name = 'Test Contract 18023-L';
        //c2.SBQQ__Quote__c = q2.Id;
        //c2.SBQQ__Opportunity__c = o2.Id;
        //c2.AccountId = a2.Id;
        //c2.JVCO_Value__c = 1000;
        //insert c2;

        //SBQQ__Subscription__c s2 = JVCO_TestClassObjectBuilder.createSubscription(a2.id, prod1.id);
        //s2.Affiliation__c = affilRecord2.Id;
        //s2.JVCO_Venue__c = nvenue2.Id;
        //s2.SBQQ__Contract__c = c2.Id;
        //s2.SBQQ__RenewalQuantity__c = 1;
        //s2.SBQQ__ListPrice__c = 1;
        //s2.SBQQ__NetPrice__c = 1;
        //s2.SBQQ__Quantity__c = 1;
        ////s2.SBQQ__QuoteLine__c = ql2.Id;
        //s2.End_Date__c = System.today().addMonths(12);
        //s2.Start_Date__c = System.today();
        //insert s2;

        //c2.JVCO_RenewableQuantity__c = 1;
        //c2.JVCO_Value__c = 1000;
        
              
        Id batchId = database.executeBatch(new JVCO_GenerateRenewalQuotesBatch(a2), 1);
        Test.stopTest();
    }

    private static testMethod void testMethodGenerateRenewalKeyAccounts() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

        Account accList = [Select Id, Type from Account where RecordTypeId = :licenceRT];

        accList.Type = 'Key Account';
        update accList;
        
        Account a = [select Id, Name, Licence_Account_Number__c from Account where id =: accList.id limit 1];
        
        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        asCS.JVCO_Contract_Batch_Size__c = 1;

        upsert asCS JVCO_Array_Size__c.Id;
        
        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_GenerateRenewalQuotesBatch(a), 1);
        Test.stopTest();
    }
}