/* ----------------------------------------------------------------------------------------------
    Name: JVCO_RecalculateQuotesBatch
    Description: Extension Class for updating Contracts related to an Account from Disposals

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    03-Jan-2018     0.2         Csaba Feher         Query refactoring to reduce view state size
    08-Apr-2019     0.4         Rhys Dela Cruz      GREEN-34460 - include for recalc Opps with blank Primary Quote field
----------------------------------------------------------------------------------------------- */
public class JVCO_RecalculateQuotesExtension {
    
    transient Account fetchedAccountRecord;
    public Account accountRecord;
    transient Map<Id, SBQQ__Quote__c> quoteMap;
    public Boolean isContinued {get; set;}
    public Integer numberOfRecords {get; set;}
    
    public JVCO_RecalculateQuotesExtension(ApexPages.StandardController stdController) {
        isContinued = false;
        numberOfRecords = 0;
        fetchedAccountRecord = (Account)stdController.getRecord();
        accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where Id =: fetchedAccountRecord.Id];
        Id accRecId = accountRecord.Id;
        numberOfRecords = Database.countQuery('SELECT count() FROM SBQQ__Quote__c where (JVCO_Recalculated__c = false OR SBQQ__Opportunity2__r.SBQQ__PrimaryQuote__c = null) and SBQQ__Account__c =: accRecId and SBQQ__Opportunity2__r.SBQQ__Contracted__c = false');
    }
    
    public void recalculateQuotes() {
        //08-Apr-19 rhys.j.c.dela.cruz Check if Primary Quote of Opp is null
        quoteMap = new Map<Id, SBQQ__Quote__c>([select Id, SBQQ__Account__c from SBQQ__Quote__c where (JVCO_Recalculated__c = false OR SBQQ__Opportunity2__r.SBQQ__PrimaryQuote__c = null) and SBQQ__Account__c =: accountRecord.Id and SBQQ__Opportunity2__r.SBQQ__Contracted__c = false]);

        Id batchId = database.executeBatch(new JVCO_RecalculateQuotesBatch(quoteMap.keySet(), 0, false, accountRecord), 1);
        isContinued = true;
    }
    
    public PageReference backToRecord() {
        PageReference pRef = new PageReference('/' + accountRecord.Id);
        return pRef;
    }
}
