/* ----------------------------------------------------------------------------------------------
   Name: JVCO_codaJournalLineItemHandler.cls 
   Description: Handler class for c2g__codaJournalLineItem__c trigger

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  07-Sep-2017   0.1         L.B.Singh           Intial creation
  25-Nov-2017   0.2         franz.g.a.dimaapi   Create populateDefaultDim2 metod, renamed class
  19-Dec-2017   0.3         chun.yin.tang       Add disabling trigger to Journal LIne Item Handler
  28-Mar-2019   0.4         patrick.t.bautista  GREEN-34440 - Added logic to retrict certain profile
  10-June-2019  0.5         rhys.j.c.dela.cruz  GREEN-33666 - added checking for Sales Credit note to be used in Write Off Report
  ----------------------------------------------------------------------------------------------- */
public class JVCO_codaJournalLineItemHandler
{
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_JournalLIneItemTrigger__c : false;  
    public static boolean skipJournalTrigger = false;
    
     /* ------------------------------------------------------------------------------------------
    Author: L.B.Singh
    Company: Accenture
    Description: This method handles beforeInsert trigger events for c2g__codaJournalLineItem__c
    Input: List<c2g__codaJournalLineItem__c>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    07-Sep-2017 L.B.Singh  Initial version of function   
  ----------------------------------------------------------------------------------------------- */
    public static void beforeInsert(List<c2g__codaJournalLineItem__c> journalLineItemList)
    {
        if(!skipTrigger) {
            if(!skipJournalTrigger){
            restrictCreateUpdate(journalLineItemList, true);
            }
            JournalLineDescriptionCheck(journalLineItemList);
            populateDefaultDim2(journalLineItemList);
        }
    }
    public static void beforeUpdate(List<c2g__codaJournalLineItem__c> journalLineItemList)
    {
        if(!skipTrigger && !skipJournalTrigger) {
            restrictCreateUpdate(journalLineItemList, false);
        }
    }
    
    /* ------------------------------------------------------------------------------------------
        Author: L.B.Singh
        Company: Accenture
        Description: This method checks the sales invoice name in Line Description field of Journal line item object and populates the respective
                     sales invoice to the sales invoice field in Journal Line item Object.
        Input: List<c2g__codaJournalLineItem__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        07-Sep-2017  L.B.Singh          Initial version of function
        10-May-2018  jasper.j.figueroa  Fixed Mapping issue - GREEN-31145
        10-June-2019 rhys.j.c.dela.cruz GREEN-33666 - added checking for Sales Credit note to be used in Write Off Report
    ----------------------------------------------------------------------------------------------- */
    public static void JournalLineDescriptionCheck(List<c2g__codaJournalLineItem__c> journalLineItemList){

        Set<String> sInvName = new Set<String>();
        Set<String> sCredNoteName = new Set<String>();

        for(c2g__codaJournalLineItem__c journalLine : journalLineItemList)
        {
            if(journalLine.c2g__LineDescription__c != null && journalLine.c2g__LineDescription__c.containsIgnoreCase('SIN'))
            {
                sInvName.add(journalLine.c2g__LineDescription__c);
            }
            //10-June-2019 rhys.j.c.dela.cruz Sales Credit Note Checker
            if(journalLine.c2g__LineDescription__c != null && journalLine.c2g__LineDescription__c.containsIgnoreCase('SCR'))
            {
                sCredNoteName.add(journalLine.c2g__LineDescription__c);
            }
        }
        if(!sInvName.isEmpty())
        {
            Map<String, Id> sInvNameToSInvIdMap = new Map<String, Id>();
            for(c2g__codaInvoice__c sInv : [SELECT Id, Name FROM c2g__codaInvoice__c
                                            WHERE Name IN :sInvName])
            {
                sInvNameToSInvIdMap.put(sInv.Name, sInv.Id);
            }
            for(c2g__codaJournalLineItem__c journalLine : journalLineItemList)
            {
                if(sInvNameToSInvIdMap.containsKey(journalLine.c2g__LineDescription__c))
                {
                    journalLine.JVCO_Sales_Invoice__c = sInvNameToSInvIdMap.get(journalLine.c2g__LineDescription__c);
                }
            }
        }

        //10-June-2019 rhys.j.c.dela.cruz Stamp Sales Credit Note to JLI
        if(!sCredNoteName.isEmpty())
        {
            Map<String, Id> sCredNoteNameToSInvIdMap = new Map<String, Id>();

            for(c2g__codaCreditNote__c sCredNote : [SELECT Id, Name FROM c2g__codaCreditNote__c WHERE Name IN: sCredNoteName])
            {
                sCredNoteNameToSInvIdMap.put(sCredNote.Name, sCredNote.Id);
            }

            for(c2g__codaJournalLineItem__c journalLine : journalLineItemList)
            {
                if(sCredNoteNameToSInvIdMap.containsKey(journalLine.c2g__LineDescription__c))
                {
                    journalLine.JVCO_Sales_Credit_Note__c = sCredNoteNameToSInvIdMap.get(journalLine.c2g__LineDescription__c);
                }
            }
        }
    }

    /* ------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Update Dimension2 based on GLAs Criteria
        Input: List<c2g__codaJournalLineItem__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        25-Nov-2017 franz.g.a.dimaapi    Initial version of the code
    ----------------------------------------------------------------------------------------------- */
    private static void populateDefaultDim2(List<c2g__codaJournalLineItem__c> journalLineList)
    {   
        final JVCO_General_Settings__c GeneralSettings = JVCO_General_Settings__c.getInstance();
        final String NONACCEPTABLEGLAS = GeneralSettings.JVCO_Non_Acceptable_GLAs__c;
        final String DIM2REPORTINGCODE = GeneralSettings.JVCO_Reporting_Code_JVCO__c;

        if(NONACCEPTABLEGLAS != null)
        {   
            List<String> nonAcceptableGLAList = new List<String>();
            for(String gla : NONACCEPTABLEGLAS.split(','))
            {
                nonAcceptableGLAList.add(gla);
                System.debug(gla);
            }

            Set<Id> journalIdSet = new Set<Id>();
            Set<Id> glaIdSet = new Set<Id>();
            for(c2g__codaJournalLineItem__c journalLine : journalLineList)
            {
                journalIdSet.add(journalLine.c2g__Journal__c);
                glaIdSet.add(journalLine.c2g__GeneralLedgerAccount__c);
            }

            Map<Id, c2g__codaJournal__c> journalMap = new Map<Id, c2g__codaJournal__c>([
                                                        SELECT Id
                                                        FROM c2g__codaJournal__c
                                                        WHERE Id IN :journalIdSet
                                                        AND c2g__JournalDescription__c
                                                            NOT IN ('Transfer Cash to Parent')
                                                        AND c2g__JournalStatus__c = 'In Progress'
                                                        AND c2g__Type__c IN ('Manual Journal')
                                                    ]);
            if(!journalMap.isEmpty())
            {
                Map<Id, c2g__codaGeneralLedgerAccount__c> glaMap = new Map<Id, c2g__codaGeneralLedgerAccount__c>([
                                                                SELECT Id, c2g__ReportingCode__c
                                                                FROM c2g__codaGeneralLedgerAccount__c
                                                                WHERE c2g__ReportingCode__c IN :nonAcceptableGLAList
                                                            ]);
                List<c2g__codaDimension2__c> dim2List = [SELECT Id 
                                                        FROM c2g__codaDimension2__c
                                                        WHERE c2g__ReportingCode__c = :DIM2REPORTINGCODE];

                for(c2g__codaJournalLineItem__c journalLine : journalLineList)
                {
                    if(journalLine.c2g__GeneralLedgerAccount__c != null &&
                        !glaMap.containsKey(journalLine.c2g__GeneralLedgerAccount__c) &&
                        !dim2List.isEmpty())
                    {
                        journalLine.c2g__Dimension2__c = dim2List.get(0).Id;
                    }
                }
            }                                   
        }    
    }
    
    /* ------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Update Dimension2 based on GLAs Criteria
        Input: List<c2g__codaJournalLineItem__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        25-Mar-2019 patrick.t.bautista  Initial version of the code
    ----------------------------------------------------------------------------------------------- */
    public static void restrictCreateUpdate(List<c2g__codaJournalLineItem__c> newJournalList, boolean isInsertChecker)
    {
        String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
        String errMessage = isInsertChecker ? 'Can\'t create a record' : 'Can\'t update a record';
        if(profileName == 'Account Receivable Assistant' || profileName == 'Licensing User' || profileName == 'Recovery & Collection')
        {
            for(c2g__codaJournalLineItem__c journalline : newJournalList)
            {
                if((journalline.c2g__LineDescription__c != null 
                   && !journalline.c2g__LineDescription__c.contains('Cash Matching - Write-off'))
                   || journalline.c2g__LineDescription__c == null)
                {
                    journalline.addError(errMessage);
                }
            }
        }
    }
}