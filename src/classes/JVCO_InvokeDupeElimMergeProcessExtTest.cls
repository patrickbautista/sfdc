/* ----------------------------------------------------------------------------------------------
Author: Recuerdo Bregente
Company: Accenture
Description: Test Class for JVCO_InvokeDupeElimMergeProcessExtension
<Date>      <Authors Name>       <Brief Description of Change> 
22-Aug-2017 Recuerdo Bregente  Initial version
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_InvokeDupeElimMergeProcessExtTest {
   private static List<Account> licAccounts;
  
  @testSetup static void setUpTestData(){
        
    Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='JVCO_Venue__c'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q2);
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }

        JVCO_Venue__c venue = new JVCO_Venue__c();
        venue.Name = 'test venue 1';
        venue.JVCO_Field_Visit_Requested__c = false;
        venue.JVCO_Street__c = 'testStreet';
        venue.JVCO_City__c = 'testCoty';
        venue.JVCO_Postcode__c ='TN32 4SL';
        venue.JVCO_Country__c ='UK';

        insert venue;
        
        JVCO_Venue__c venue2 = new JVCO_Venue__c();
        venue2.Name = 'update venue';
        venue2.JVCO_Field_Visit_Requested__c = false;
        venue2.JVCO_Street__c = 'updateStreet';
        venue2.JVCO_City__c = 'updateCoty';
        venue2.JVCO_Postcode__c ='TN32 4SL';
        venue2.JVCO_Country__c ='UK';

        insert venue2;
        
        //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
        testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc.Dimension_1_Required__c = false;
        testGeneralLedgerAcc.Dimension_2_Required__c = true;
        testGeneralLedgerAcc.Dimension_3_Required__c = false;
        testGeneralLedgerAcc.Dimension_4_Required__c = false;
        testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
        testGeneralLedgerAcc.ownerid = testGroup.Id;
        testGeneralLedgerAcc.c2g__ReportingCode__c = '30030';
        insert testGeneralLedgerAcc;
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedgerAcc.id);
        insert taxCode;

        //Create Account
        Account testAccCust = new Account(Name='Test Customer Account');
        testAccCust.AccountNumber = '999999';
        testAccCust.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust.Type='Customer';
        testAccCust.RecordTypeId = accountRecordTypes.get('Customer Account');
        insert testAccCust;
        
        Account testAccCust2 = new Account(Name='Test Customer Account 2');
        testAccCust2.AccountNumber = '2222222';
        testAccCust2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust2.Type='Customer';
        testAccCust2.RecordTypeId = accountRecordTypes.get('Customer Account');
        insert testAccCust2;

         Contact c = JVCO_TestClassObjectBuilder.createContact(testAccCust.id);
        insert c;

        Contact con = JVCO_TestClassObjectBuilder.createContact(testAccCust2.id);
        con.LastName = 'LastName';
        con.FirstName = 'FirstName';
        con.Email = 'test@hotmail.com';
        insert con;

        
        Account testLicAcc1 = new Account(Name='Test Lic Account 1');
        testLicAcc1.AccountNumber = '987654';
        testLicAcc1.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testLicAcc1.c2g__CODAAllowDeleteInUse__c = false;
        testLicAcc1.c2g__CODACreditLimitEnabled__c = false;
        testLicAcc1.c2g__CODACreditStatus__c = 'Credit Allowed';
        testLicAcc1.c2g__CODADaysOffset1__c = 0.0;
        testLicAcc1.c2g__CODADiscount1__c = 0.0;
        testLicAcc1.c2g__CODAFederallyReportable1099__c = false;
        testLicAcc1.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testLicAcc1.c2g__CODAIntercompanyAccount__c = false;
        testLicAcc1.c2g__CODASalesTaxStatus__c = 'Taxable';
        testLicAcc1.JVCO_Customer_Account__c = testAccCust.id;
        testLicAcc1.RecordTypeId = accountRecordTypes.get('Licence Account');
        testLicAcc1.c2g__CODAOutputVATCode__c = taxCode.id;
        testLicAcc1.JVCO_Preferred_Contact_Method__c = 'Email';
        testLicAcc1.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        testLicAcc1.JVCO_Billing_Contact__c = c.id;
        testLicAcc1.JVCO_Licence_Holder__c = c.id;
        testLicAcc1.JVCO_Review_Contact__c = c.id;
        testLicAcc1.JVCO_DD_Payee_Contact__c = c.id;
        insert testLicAcc1;

        JVCO_DD_Mandate__C testDDMandate = new JVCO_DD_Mandate__C();
        testDDMandate.JVCO_Account_Number__c = '12345678';
        testDDMandate.JVCO_Sort_Code__c = '000001';
        testDDMandate.JVCO_Collection_Day__c = 1;
        testDDMandate.JVCO_Licence_Account__c = testLicAcc1.id;
        testDDMandate.JVCO_Number_of_Payments__c = 4;
        testDDMandate.JVCO_AUDIS_Ref__c = '00000002';
        insert testDDMandate;
        
        Account testLicAcc2 = new Account(Name='Test Lic Account 2');
        testLicAcc2.AccountNumber = '9876543';
        testLicAcc2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testLicAcc2.c2g__CODAAllowDeleteInUse__c = false;
        testLicAcc2.c2g__CODACreditLimitEnabled__c = false;
        testLicAcc2.c2g__CODACreditStatus__c = 'Credit Allowed';
        testLicAcc2.c2g__CODADaysOffset1__c = 0.0;
        testLicAcc2.c2g__CODADiscount1__c = 0.0;
        testLicAcc2.c2g__CODAFederallyReportable1099__c = false;
        testLicAcc2.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testLicAcc2.c2g__CODAIntercompanyAccount__c = false;
        testLicAcc2.c2g__CODASalesTaxStatus__c = 'Taxable';
        testLicAcc2.JVCO_Customer_Account__c = testAccCust2.id;
        testLicAcc2.RecordTypeId = accountRecordTypes.get('Licence Account');
        testLicAcc2.c2g__CODAOutputVATCode__c = taxCode.id;
        testLicAcc2.JVCO_Preferred_Contact_Method__c = 'Email';
        testLicAcc2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        testLicAcc2.JVCO_Billing_Contact__c = con.id;
        testLicAcc2.JVCO_Licence_Holder__c = con.id;
        testLicAcc2.JVCO_Review_Contact__c = con.id;
        insert testLicAcc2;
        
        licAccounts = new List<Account>();
        licAccounts.add(testLicAcc1);
        licAccounts.add(testLicAcc2);

        
    }
    
    @isTest static void testRedirectCustAcctToDupeElimPage()
    {
        List<Account> accountsList = [SELECT Id, Name FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 2];
        Test.startTest();
        
        ApexPages.StandardSetController con = 
                           new ApexPages.StandardSetController(accountsList);
            
        con.setSelected(accountsList);
        
        JVCO_InvokeDupeElimMergeProcessExtension dupeElimMergeController = new JVCO_InvokeDupeElimMergeProcessExtension(con);
        PageReference pageRef = dupeElimMergeController.redirectLicAcctToDupeElimPage();
        
        system.assert(pageRef != null);
        
        Boolean isException = false;
        try{
            pageRef = dupeElimMergeController.redirectLicAcctToDupeElimPage();
        }catch(Exception ex){
            isException = true;
        }
        
        //System.assert(isException);
        
        Test.stopTest();
    }
    
    @isTest static void testRedirectLicAcctToDupeElimPage()
    {
        List<Account> accountsList = [SELECT Id, Name FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 2];
        Test.startTest();
        
        ApexPages.StandardSetController con = 
                           new ApexPages.StandardSetController(accountsList);
            
        con.setSelected(accountsList);
        
        JVCO_InvokeDupeElimMergeProcessExtension dupeElimMergeController = new JVCO_InvokeDupeElimMergeProcessExtension(con);
        PageReference pageRef = dupeElimMergeController.redirectLicAcctToDupeElimPage();
        system.assert(pageRef != null);
        
        Boolean isException = false;
        try{
            pageRef = dupeElimMergeController.redirectLicAcctToDupeElimPage();
        }catch(Exception ex){
            isException = true;
        }
        
        //System.assert(isException);
        
        isException = false;
        try{
            pageRef = dupeElimMergeController.redirectLicAcctToDupeElimPage();
        }catch(Exception ex){
            isException = true;
        }
        
        //System.assert(isException);
        
        Test.stopTest();
    }
    
    @isTest static void testRedirectCustAcctToDupeElimPage2()
    {
        List<Account> accList = [SELECT Id, Name FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 2];
        Test.startTest();
        
        ApexPages.StandardSetController con = 
                           new ApexPages.StandardSetController(accList);
            
        con.setSelected(accList);
        
        JVCO_InvokeDupeElimMergeProcessExtension dupeElimMergeController = new JVCO_InvokeDupeElimMergeProcessExtension(con);
        PageReference pageRef = dupeElimMergeController.redirectLicAcctToDupeElimPage();
        system.assert(pageRef != null);
        
        Test.stopTest();
    }

    @isTest static void testErrorNoDDPayee()
    {

        List<Account> accountsListCheck = [SELECT Id, Name, JVCO_DD_Payee_Contact__c FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 2];

        List<Account> accountsListtoupdate = new List<Account>();

        for(Account a : accountsListCheck)
        {
            a.JVCO_DD_Payee_Contact__c = null;
            accountsListtoupdate.add(a);
        }

        //update accountsListtoupdate;

        List<Account> accountsList = [SELECT Id, Name FROM Account WHERE RecordType.Name = 'Licence Account' AND id in :accountsListtoupdate LIMIT 2];
        Test.startTest();
        
        ApexPages.StandardSetController con = 
                           new ApexPages.StandardSetController(accountsList);
            
        con.setSelected(accountsList);
        
        JVCO_InvokeDupeElimMergeProcessExtension dupeElimMergeController = new JVCO_InvokeDupeElimMergeProcessExtension(con);
        PageReference pageRef = dupeElimMergeController.redirectLicAcctToDupeElimPage();
        
        Boolean isException = false;
        try{
            pageRef = dupeElimMergeController.redirectLicAcctToDupeElimPage();
        }catch(Exception ex){
            isException = true;
        }
        
        //System.assert(isException);
        
        isException = false;
        try{
            pageRef = dupeElimMergeController.redirectLicAcctToDupeElimPage();
        }catch(Exception ex){
            isException = true;
        }
        
        //System.assert(isException);
        
        Test.stopTest();
    }

    @isTest static void testErrorNoLA()
    {
        List<Account> accountsList = new List<Account>();
        
        Test.startTest();
        ApexPages.StandardSetController con = 
                           new ApexPages.StandardSetController(accountsList);
            
        con.setSelected(accountsList);
        
        JVCO_InvokeDupeElimMergeProcessExtension dupeElimMergeController = new JVCO_InvokeDupeElimMergeProcessExtension(con);
        PageReference pageRef = dupeElimMergeController.redirectLicAcctToDupeElimPage();
        
        Boolean isException = false;
        try{
            pageRef = dupeElimMergeController.redirectLicAcctToDupeElimPage();
        }catch(Exception ex){
            isException = true;
        }
        
        //System.assert(isException);
        
        isException = false;
        try{
            pageRef = dupeElimMergeController.redirectLicAcctToDupeElimPage();
        }catch(Exception ex){
            isException = true;
        }
        
        //System.assert(isException);
        
        Test.stopTest();
    }

}