/* --------------------------------------------------------------------------------------------------
    Name:            JVCO_OverdueInstalmentHandler.cls 
    Description:     Batch Class for handler JVCO_OverdueInstalmentHandler

    Date            Version     Author                     Summary of Changes 
    -----------     -------     -----------------          -----------------------------------
    02-Feb-2017     0.1         kristoffer.d.martin        Intial draft  
    04-Aug-2017     0.2         Mel Andrei Santos          Changed DML statement to database.delete as per Ticket GREEN-17932
    22-Aug-2017     0.3         mel.andrei.b.santos        Added try catch method for error handling in inserting error log records
    24-Aug-2017     0.4         mel.andrei.b.santos        refactored inputting of fields
    14-Sep-2017     0.5         mel.andrei.b.Santos        Removed the if conditions as the null check in the error logs as per James
------------------------------------------------------------------------------------------------ */
public class JVCO_OverdueInstalmentHandler
{

    /* --------------------------------------------------------------------------------------------------
        Author:         kristoffer.d.martin
        Company:        Accenture
        Description:    This method will delete the Sales Invoice Installment Line Item
        Inputs:         List of Installment Line Items - List<c2g__codaInvoiceInstallmentLineItem__c> 
        Returns:        none
        <Date>          <Authors Name>            <Brief Description of Change> 
        09-Dec-2016     kristoffer.d.martin        Initial version of the code
        13-Aug-2018     franz.g.a.dimaapi          Clean Method
    --------------------------------------------------------------------------------------------- */
    public static void JVCO_OverdueInstalmentLogic(List<c2g__codaInvoiceInstallmentLineItem__c> sInvInstallmentLine)
    {
        Set<Id> sInvIdSet = new Set<Id>();
        for (c2g__codaInvoiceInstallmentLineItem__c installmentLine : sInvInstallmentLine)
        {
            sInvIdSet.add(installmentLine.c2g__Invoice__c);
        }

        List<c2g__codaInvoiceInstallmentLineItem__c> installmentLineItemsForDeletion = [SELECT Id
                                                                                        FROM c2g__codaInvoiceInstallmentLineItem__c
                                                                                        WHERE c2g__Invoice__c in :sInvIdSet];
        if(!installmentLineItemsForDeletion.isEmpty())
        {
            try
            {
                delete installmentLineItemsForDeletion;
            }catch(Exception e)
            {
                createInstallmentLineErrorLog(installmentLineItemsForDeletion, e.getMessage());
            }
        }    
    }

    @TestVisible
    private static void createInstallmentLineErrorLog(List<c2g__codaInvoiceInstallmentLineItem__c> installmentLineList, String errMessage)
    {   
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        for(c2g__codaInvoiceInstallmentLineItem__c installmentLine : installmentLineList)
        {
            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
            errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
            errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(installmentLine.Id);
            errLog.ffps_custRem__Detail__c = errMessage;
            errLog.ffps_custRem__Message__c = 'Overdue Instalment Deletion';
            errLog.ffps_custRem__Grouping__c = 'OD-001';            
            errLogList.add(errLog);
        }
        
        insert errLogList;
    }
}