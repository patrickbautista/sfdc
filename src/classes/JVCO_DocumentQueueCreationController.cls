public with sharing class JVCO_DocumentQueueCreationController{

    public Account acc{get; set;}
    public Lead leadObj {get; set;}
     
    public JVCO_DocumentQueueCreationController(ApexPages.StandardSetController controller) {
        String accObjPrefix = Account.sobjecttype.getDescribe().getKeyPrefix();
        String leadObjPrefix = Lead.sobjecttype.getDescribe().getKeyPrefix();
        String idParam = ApexPages.currentPage().getParameters().get('id');
        //if the ID is account ID
        if(idParam!= null && idParam.startsWith(accObjPrefix ))
        {
            acc = [SELECT Id, Name, JVCO_Customer_Account__c, JVCO_Customer_Account__r.Name, JVCO_In_Enforcement__c, JVCO_In_Infringement__c, JVCO_In_Insolvency__c
                    FROM Account WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        }
        if(idParam!= null && idParam.startsWith(leadObjPrefix ))            
        {
            leadObj = [SELECT Id, Name FROM Lead WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        }
    } 
    
    public Account getCurrentRecord(){
        return acc;
    }

    public PageReference SendDocument(){
        Map<String,String> customFieldIdMap = new Map<String,String>();
        Map<String,Id> recTypeMap = new Map<String,Id>();
        String sendDocRTId = '';
        String customerAccName = '';
        String customerAccId = ''; //mariel.m.buena GREEN-35364 
        Account accRec = new Account();
        Account customerAcct = new Account();

        recTypeMap = getRecordTypeId();
        if(recTypeMap.containskey('Send Document')){
            sendDocRTId = String.valueOf(recTypeMap.get('Send Document'));
        }
        
        List<PermissionSetAssignment> psa= [SELECT Id FROM PermissionSetAssignment WHERE PermissionSet.Name = 'JVCO_Enforcement_Advisors' AND AssigneeId=: UserInfo.getUserId()];

        if ((acc.JVCO_In_Enforcement__c || acc.JVCO_In_Infringement__c) && psa.size()==0 ){  
            PageReference pageRef = Page.JVCO_DisplayError;
            return pageRef;
        }
        accRec = getCurrentRecord();
        customerAcct.Id = accRec.JVCO_Customer_Account__c;
        customerAcct.Name = accRec.JVCO_Customer_Account__r.Name;
        customerAccName = customerAcct.Name;
        customerAccId = customerAcct.Id; //mariel.m.buena GREEN-35364 
        //PageReference pr = new PageReference('/' + getObjectPrefixID('JVCO_Document_Queue__c') + '/e?CF' + Label.JVCO_DQ_RelatedCustomerAccountFieldId + '=' + EncodingUtil.urlEncode(customerAccName,'UTF-8') + '&CF' + Label.JVCO_DQ_RelatedAccountFieldId + '=' + EncodingUtil.urlEncode(accRec.name,'UTF-8') + '&RecordType=' + sendDocRTId + '&retURL='+acc.id    );
        String uiTheme = UserInfo.getUiThemeDisplayed();
        //Create URL for Lightning or Classic GREEN-36267
        if(uiTheme == 'Theme4d' || uiTheme == 'Theme4u'){
            system.debug('Lightning Page Reference');
            PageReference lightPR = new PageReference('/lightning/o/JVCO_Document_Queue__c/new?recordTypeId='+sendDocRTId+'&defaultFieldValues=JVCO_Related_Account__c='+acc.id+',JVCO_Related_Customer_Account__c='+customerAccId);  
            return lightPR;
        } else {
            system.debug('Classic Page Reference');
            PageReference classPR = new PageReference('/' + getObjectPrefixID('JVCO_Document_Queue__c') + '/e?CF' + Label.JVCO_DQ_RelatedAccountFieldId + '=' + EncodingUtil.urlEncode(accRec.name,'UTF-8') + '&CF' + Label.JVCO_DQ_RelatedAccountFieldId + '_lkid=' + acc.id + '&CF' + Label.JVCO_DQ_RelatedCustomerAccountFieldId + '=' + EncodingUtil.urlEncode(customerAccName,'UTF-8') + '&RecordType=' + sendDocRTId + '&retURL=' + acc.id + '&CF' + Label.JVCO_DQ_RelatedCustomerAccountFieldId + '_lkid=' + customerAccId); //mariel.m.buena GREEN-35364 
            return classPR;
        }
    }

    public PageReference ForDownload(){
        Map<String,String> customFieldIdMap = new Map<String,String>();
        Map<String,Id> recTypeMap = new Map<String,Id>();
        String downloadRTId = '';
        String customerAccName = '';
        String customerAccId = ''; //mariel.m.buena GREEN-35364 
        Account accRec = new Account();
        Account customerAcct = new Account();

        recTypeMap = getRecordTypeId();
        if(recTypeMap.containskey('Download')){
            downloadRTId = String.valueOf(recTypeMap.get('Download'));
        }
        accRec = getCurrentRecord();
        customerAcct.Id = accRec.JVCO_Customer_Account__c;
        customerAcct.Name = accRec.JVCO_Customer_Account__r.Name;
        customerAccName = customerAcct.Name;
        customerAccId = customerAcct.Id; //mariel.m.buena GREEN-35364
           
        String uiTheme = UserInfo.getUiThemeDisplayed();
        
        //Create URL for Lightning or Classic
        if(uiTheme == 'Theme4d' || uiTheme == 'Theme4u'){
            system.debug('Lightning Page Reference');
            PageReference lightPR = new PageReference('/lightning/o/JVCO_Document_Queue__c/new?recordTypeId='+downloadRTId+'&defaultFieldValues=JVCO_Related_Account__c='+acc.id+',JVCO_Related_Customer_Account__c='+customerAccId);  
            return lightPR;
        } else {
            system.debug('Classic Page Reference');
            PageReference classPR = new PageReference('/' + getObjectPrefixID('JVCO_Document_Queue__c') + '/e?CF' + Label.JVCO_DQ_RelatedAccountFieldId + '=' + EncodingUtil.urlEncode(accRec.Name,'UTF-8') + '&CF' + Label.JVCO_DQ_RelatedAccountFieldId + '_lkid=' + acc.id + '&CF' + Label.JVCO_DQ_RelatedCustomerAccountFieldId + '=' + EncodingUtil.urlEncode(customerAccName,'UTF-8') + '&RecordType=' + downloadRTId + '&retURL=' + acc.id + '&CF' + Label.JVCO_DQ_RelatedCustomerAccountFieldId + '_lkid=' + customerAccId ); //mariel.m.buena GREEN-35364
            return classPR;
        }
        //PageReference pr = new PageReference('/' + getObjectPrefixID('JVCO_Document_Queue__c') + '/e?CF' + Label.JVCO_DQ_RelatedCustomerAccountFieldId + '=' + EncodingUtil.urlEncode(customerAccName,'UTF-8') + '&CF' + Label.JVCO_DQ_RelatedAccountFieldId + '=' + EncodingUtil.urlEncode(accRec.Name,'UTF-8') + '&RecordType=' + downloadRTId + '&retURL='+acc.id    );
    }
    
    //Hazel Jurna Limot 06-13-2018 - GREEN-31001
    public PageReference ForDownloadPreview(){
        Map<String,String> customFieldIdMap = new Map<String,String>();
        Map<String,Id> recTypeMap = new Map<String,Id>();
        String downloadRTId = '';
        String customerAccName = '';
        String customerAccId = '';
        Account accRec = new Account();
        Account customerAcct = new Account();
        
        recTypeMap = getRecordTypeId();
        if(recTypeMap.containskey('Download - Preview')){
            downloadRTId = String.valueOf(recTypeMap.get('Download - Preview'));
        }
        accRec = getCurrentRecord();
        customerAcct.Id = accRec.JVCO_Customer_Account__c;
        customerAcct.Name = accRec.JVCO_Customer_Account__r.Name;
        customerAccName = customerAcct.Name;
        customerAccId = customerAcct.Id;
        
        String uiTheme = UserInfo.getUiThemeDisplayed();
        
        //Create URL for Lightning or Classic
        if(uiTheme == 'Theme4d' || uiTheme == 'Theme4u'){
            system.debug('Lightning Page Reference');
            PageReference lightPR = new PageReference('/lightning/o/JVCO_Document_Queue__c/new?recordTypeId='+downloadRTId+'&defaultFieldValues=JVCO_Related_Account__c=' + acc.id +',JVCO_Related_Customer_Account__c=' + customerAccId);  
            return lightPR;
        } else {
            system.debug('Classic Page Reference');
            PageReference classPR = new PageReference('/' + getObjectPrefixID('JVCO_Document_Queue__c') + '/e?CF' + Label.JVCO_DQ_RelatedCustomerAccountFieldId + '=' + EncodingUtil.urlEncode(customerAccName,'UTF-8') + '&CF' + Label.JVCO_DQ_RelatedAccountFieldId + '=' + EncodingUtil.urlEncode(accRec.Name,'UTF-8') + '&RecordType=' + downloadRTId + '&retURL='+acc.id    );
            return classPR;
        }
    }
    
     public PageReference forSendDocLead()
    {
        Map<String,String> customFieldIdMap = new Map<String,String>();
        Map<String,Id> recTypeMap = new Map<String,Id>();
        String sendDocLeadRTId = '';       
        recTypeMap = getRecordTypeId();
        if(recTypeMap.containskey('Send Document - Lead')){
            sendDocLeadRTId = String.valueOf(recTypeMap.get('Send Document - Lead'));
        }
        
        String uiTheme = UserInfo.getUiThemeDisplayed();
        
        //Create URL for Lightning or Classic
        if(uiTheme == 'Theme4d' || uiTheme == 'Theme4u'){
            system.debug('Lightning Page Reference');
            PageReference lightPR = new PageReference('/lightning/o/JVCO_Document_Queue__c/new?recordTypeId='+sendDocLeadRTId+'&defaultFieldValues='+'Related_Lead__c='+leadObj.ID);  
            return lightPR;
        } else {
            system.debug('Classic Page Reference');
            PageReference classPR = new PageReference('/' + getObjectPrefixID('JVCO_Document_Queue__c') + '/e?CF' + Label.JVCO_DQ_RelatedLeadFieldId+ '=' + EncodingUtil.urlEncode(leadObj.Name,'UTF-8') +'&CF' + Label.JVCO_DQ_RelatedLeadFieldId+ '_lkid=' + leadObj.ID + '&RecordType=' + sendDocLeadRTId + '&retURL='+leadObj.id    );
            return classPR;
        }
    }

    public static String getObjectPrefixID(String obj){
        String keyPrefix = '';

        Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe() ;
        Schema.SObjectType s = m.get(obj) ;
        system.debug('Sobject Type: '+ s);
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        keyPrefix = r.getKeyPrefix();
        system.debug('keyPrefix: '+ keyPrefix);
        return keyPrefix;
    }

    public static Map<String,Id> getRecordTypeId(){
        Map<String,Id> rtypeDocq = new Map<String,Id>();

        Schema.DescribeSObjectResult R = JVCO_Document_Queue__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
        
        for(Schema.RecordTypeInfo rtype: RT){
            rtypeDocq.put(rtype.getName(), rtype.getRecordTypeId());
        }

        return rtypeDocq;
    }
}