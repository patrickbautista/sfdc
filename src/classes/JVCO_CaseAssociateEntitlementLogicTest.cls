@isTest
private class JVCO_CaseAssociateEntitlementLogicTest
{
	@isTest
	static void testCaseAssocEntLogic()
	{
		List<JVCO_SLA_Constants__c> settings = new List<JVCO_SLA_Constants__c>();
    	settings.add(new JVCO_SLA_Constants__c(Name = 'JVCO_SLA_Custom_Settings', 
    											JVCO_Entitlement_Name__c = 'Customer Service Support', 
    											JVCO_Case_Origins_with_Entitlement__c = 'Email,Webform,Live Chat,Request Callback,Inbound Call,White Mail',
    											JVCO_First_Milestone_Criteria__c = 'Responded',
										    	JVCO_First_Milestone_Origin__c = 'Email,Webform,Request Callback',
										    	JVCO_First_Milestone__c = 'First Response',
										    	JVCO_Second_Milestone_Criteria__c = 'Closed',
										    	JVCO_Second_Milestone_Origin__c = 'Email,Webform,Live Chat,Request Callback,Inbound Call,White Mail',
										    	JVCO_Second_Milestone__c = 'Resolution Time'));
										    	insert settings;

		Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
	    insert acc;
	  
	    Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
	    insert acc2;

		Entitlement e = JVCO_TestClassObjectBuilder.createEntitlement(acc.id);
		insert e;

    	Case c = JVCO_TestClassObjectBuilder.createCase(e.id);  
    	insert c;

    	List<Case> l_c = [SELECT Type FROM Case];
		System.Assert(!l_c.isEmpty());
	}
}