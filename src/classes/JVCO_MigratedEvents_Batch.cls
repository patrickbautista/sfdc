/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_MigratedEvents_Batch.cls 

   Date            Version     Author                             Summary of Changes 
   -----------     -------     -----------------                  -----------------------------------
   07-DEC-2017     0.1         gypt.d.minierva                    Intial draft       
   23-Mar-2018     0.2         franz.g.a.dimaapi                  Fixed GREEN-31062
------------------------------------------------------------------------------------------------ */
global class JVCO_MigratedEvents_Batch implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator([SELECT Id,JVCO_Migrated__c, JVCO_Distribution_Status__c,blng__Account__r.JVCO_Live__c FROM blng__Invoice__c
                                         WHERE JVCO_Migrated__c = true 
                                         AND blng__Account__r.JVCO_Live__c = true
                                         AND JVCO_Temp_Legacy_Contracts__c != NULL]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        //handler
        JVCO_MigratedEventsHandler.JVCO_MigratedEventsLogic(scope);
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        //next batch
    }
    
}