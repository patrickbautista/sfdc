@isTest
private class JVCO_QueryBuilderTest
{
    public static testMethod void testSearchMerge() {

        long now = System.currentTimeMillis();

        Account acc1 = JVCO_TestClassObjectBuilder.createCustomerAccount();
        acc1.Name = 'LIC Customer';
        acc1.Phone='123';
        acc1.Type='Prospect';
        acc1.AnnualRevenue=100000; 
        acc1.NumberOfEmployees=1000;
        acc1.ShippingStreet = 'Test Street';
        insert acc1;

        Account acc2 = JVCO_TestClassObjectBuilder.createCustomerAccount();
        acc1.Name = 'LIC Licence';
        acc2.Phone='456';
        acc2.Type='Prospect';
        acc2.AnnualRevenue=200000; 
        acc2.NumberOfEmployees=2000;
        acc2.ShippingStreet = 'Test Street';
        insert acc2;

        List<Account> accs = [SELECT Name, Phone, Type, AnnualRevenue, NumberOfEmployees, RecordTypeId, CreatedDate FROM Account];

        List<Contact> cons = new List<Contact>();
        List<Note> notes = new List<Note>();
        for (Account acc : accs) {
            cons.add(new Contact(LastName=acc.name, FirstName='contact', accountId=acc.Id));
            notes.add(new Note(title=acc.name+' note', parentId=acc.Id ));
        }
        insert(cons);

        // Use the PageReference Apex class to instantiate a page
        PageReference pageRef = Page.JVCO_SearchMerge;

        // The Visualforce page named 'JVCO_SearchMerge' is the starting point of this test method.
        Test.setCurrentPage(pageRef);

        // Add parameters to page URL
        Map<String, String> params = ApexPages.currentPage().getParameters();
        params.put('debug', 'true');
        params.put('showQuery', 'true');
        params.put('hideFilter', 'false');
        params.put('hideHeader', 'false');
        params.put('hideSidebar', 'false');
        params.put('find', 'true');
        params.put('object', 'Account');
        params.put('field1', 'Name');
        params.put('field2', 'Phone');
        params.put('field3', 'Type');
        params.put('field4', 'RecordTypeId');
        params.put('field5', 'CreatedDate');
        params.put('op1', 'contains');
        params.put('op2', 'not equal to');
        params.put('op3', 'equals');
        params.put('op4', 'contains');
        params.put('op5', 'greater or equal');
        params.put('value1', 'LIC'); 
        params.put('value2', '');
        params.put('value3', 'Prospect');
        params.put('value4', 'Customer');
        params.put('value5', DateTime.newInstance(now).format('MM/dd/yyyy'));
        params.put('limit', '10');

        // Instantiate a new controller with all parameters in the page
        JVCO_SearchMergeController controller = new JVCO_SearchMergeController();

        // Set searh criteria thru JVCO_QueryBuilder
        //JVCO_QueryBuilder q = queryBuilder;
        /*
        controller.querybuilder.objectName = 'Account';
        controller.querybuilder.fieldName1 = 'Name';
        controller.querybuilder.fieldName2 = 'Phone';
        controller.querybuilder.fieldName3 = 'Type';
        controller.querybuilder.fieldName4 = 'AnnualRevenue';
        controller.querybuilder.fieldName5 = 'NumberOfEmployees';
        controller.querybuilder.operatorValue1 = 'contains';
        controller.querybuilder.operatorValue2 = 'not equal to';
        controller.querybuilder.operatorValue3 = 'equals';
        controller.querybuilder.operatorValue4 = 'greater than';
        controller.querybuilder.operatorValue5 = 'less than';

        controller.querybuilder.inputValue2 = '';
        controller.querybuilder.inputValue3 = 'Prospect';
        controller.querybuilder.inputValue4 = '1000';
        controller.querybuilder.inputValue5 = '10000';
        controller.querybuilder.recordLimit = '10';
        */
        controller.querybuilder.getObjects();
        controller.querybuilder.getFields();
        controller.querybuilder.getOperators();
        controller.querybuilder.getChildRelationships();
        controller.getFieldLabels();
        controller.querybuilder.objectName = 'Account';

        System.assertEquals(controller.querybuilder.getObjectLabel(), 'Account');
        System.assertEquals(controller.querybuilder.getFieldLabel1(), 'Account Name');
        //System.assertEquals(controller.querybuilder.getFieldLabel2(), 'Account Phone');
        System.assertEquals(controller.querybuilder.getFieldLabel3(), 'Account Type');
     //   System.assertEquals(controller.querybuilder.getFieldLabel4(), 'Annual Revenue');
        System.assertEquals(controller.querybuilder.getFieldLabel5(), 'Created Date');

        System.assert(controller.getShowQuery());
        System.assert(controller.debug);
        System.assert(!controller.hideFilter);
        System.assert(!controller.hideHeader);
        System.assert(!controller.hideSidebar);

        // Find records to merge
        //controller.find();

        // Select records to merge
        List<JVCO_DynamicSObject> records = controller.getResults();
        System.assertEquals(1, records.size());
        for (JVCO_DynamicSObject s : records) {
            s.setSelected(true);
            System.assert(s.getField1().getValue() != null);
            System.assert(s.getField2().getValue() != null);
            System.assert(s.getField3().getValue() != null);
            System.assert(s.getField4().getValue() != null);
            //System.assert(s.getField5().getValue() != null);
        }

        // Go to JVCO_MergeRecord page
        PageReference nextPage = controller.selectMerge();
        // Verify that next() method returns the proper URL.
        System.assertEquals(null, nextPage);

        controller.previous();
        controller.selectMerge();

        // Check clone non-reparantable child records option
        controller.cloneNonReparentableChild = true;
        controller.showMasterAfterMerge = true;

        List<String> childRelationships = controller.getChildRelationships();
        for (SelectOption s : controller.getChildRelationshipSelection()) {
            childRelationships.add(s.getValue());
        }
        controller.setChildRelationships(childRelationships);
    }
    
    @isTest static void testConstructor()
    {
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='JVCO_Venue__c'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q2);
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }

        JVCO_Venue__c venue = new JVCO_Venue__c();
        venue.Name = 'test venue 1';
        venue.JVCO_Field_Visit_Requested__c = false;
        venue.JVCO_Street__c = 'testStreet';
        venue.JVCO_City__c = 'testCoty';
        venue.JVCO_Postcode__c ='TN32 4SL';
        venue.JVCO_Country__c ='UK';

        insert venue;
        
        JVCO_Venue__c venue2 = new JVCO_Venue__c();
        venue2.Name = 'update venue';
        venue2.JVCO_Field_Visit_Requested__c = false;
        venue2.JVCO_Street__c = 'updateStreet';
        venue2.JVCO_City__c = 'updateCoty';
        venue2.JVCO_Postcode__c ='TN32 4SL';
        venue2.JVCO_Country__c ='UK';

        insert venue2;
        
        //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
        testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc.Dimension_1_Required__c = false;
        testGeneralLedgerAcc.Dimension_2_Required__c = true;
        testGeneralLedgerAcc.Dimension_3_Required__c = false;
        testGeneralLedgerAcc.Dimension_4_Required__c = false;
        testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
        testGeneralLedgerAcc.ownerid = testGroup.Id;
        testGeneralLedgerAcc.c2g__ReportingCode__c = '30030';
        insert testGeneralLedgerAcc;
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedgerAcc.id);
        insert taxCode;

        //Create Account
        Account testAccCust = new Account(Name='Test Customer Account');
        testAccCust.AccountNumber = '999999';
        testAccCust.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust.Type='Customer';
        testAccCust.JVCO_Additional_Information__c = '';
        testAccCust.JVCO_Date_TCs_Accepted__c = System.Today().addDays(1);
        testAccCust.RecordTypeId = accountRecordTypes.get('Customer Account');
        insert testAccCust;
        
        Account testAccCust2 = new Account(Name='Test Customer Account 2');
        testAccCust2.AccountNumber = '2222222';
        testAccCust2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust2.Type='Customer';
        testAccCust2.JVCO_Additional_Information__c = '';
        testAccCust2.JVCO_Date_TCs_Accepted__c = System.Today().addDays(1);
        testAccCust2.RecordTypeId = accountRecordTypes.get('Customer Account');
        insert testAccCust2;

         Contact c = JVCO_TestClassObjectBuilder.createContact(testAccCust.id);
        insert c;

        Contact con = JVCO_TestClassObjectBuilder.createContact(testAccCust2.id);
        con.LastName = 'LastName';
        con.FirstName = 'FirstName';
        con.Email = 'test@hotmail.com';
        insert con;

        
        Account testLicAcc1 = new Account(Name='Test Lic Account 1');
        testLicAcc1.AccountNumber = '987654';
        testLicAcc1.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testLicAcc1.c2g__CODAAllowDeleteInUse__c = false;
        testLicAcc1.c2g__CODACreditLimitEnabled__c = false;
        testLicAcc1.c2g__CODACreditStatus__c = 'Credit Allowed';
        testLicAcc1.c2g__CODADaysOffset1__c = 0.0;
        testLicAcc1.c2g__CODADiscount1__c = 0.0;
        testLicAcc1.c2g__CODAFederallyReportable1099__c = false;
        testLicAcc1.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testLicAcc1.c2g__CODAIntercompanyAccount__c = false;
        testLicAcc1.c2g__CODASalesTaxStatus__c = 'Taxable';
        testLicAcc1.JVCO_Customer_Account__c = testAccCust.id;
        testLicAcc1.RecordTypeId = accountRecordTypes.get('Licence Account');
        testLicAcc1.c2g__CODAOutputVATCode__c = taxCode.id;
        testLicAcc1.JVCO_Preferred_Contact_Method__c = 'Email';
        testLicAcc1.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        testLicAcc1.JVCO_Billing_Contact__c = c.id;
        testLicAcc1.JVCO_Licence_Holder__c = c.id;
        testLicAcc1.JVCO_Review_Contact__c = c.id;
        insert testLicAcc1;
        
        Account testLicAcc2 = new Account(Name='Test Lic Account 2');
        testLicAcc2.AccountNumber = '987653';
        testLicAcc2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testLicAcc2.c2g__CODAAllowDeleteInUse__c = false;
        testLicAcc2.c2g__CODACreditLimitEnabled__c = false;
        testLicAcc2.c2g__CODACreditStatus__c = 'Credit Allowed';
        testLicAcc2.c2g__CODADaysOffset1__c = 0.0;
        testLicAcc2.c2g__CODADiscount1__c = 0.0;
        testLicAcc2.c2g__CODAFederallyReportable1099__c = false;
        testLicAcc2.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testLicAcc2.c2g__CODAIntercompanyAccount__c = false;
        testLicAcc2.c2g__CODASalesTaxStatus__c = 'Taxable';
        testLicAcc2.JVCO_Customer_Account__c = testAccCust2.id;
        testLicAcc2.RecordTypeId = accountRecordTypes.get('Licence Account');
        testLicAcc2.c2g__CODAOutputVATCode__c = taxCode.id;
        testLicAcc2.JVCO_Preferred_Contact_Method__c = 'Email';
        testLicAcc2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        testLicAcc2.JVCO_Billing_Contact__c = con.id;
        testLicAcc2.JVCO_Licence_Holder__c = con.id;
        testLicAcc2.JVCO_Review_Contact__c = con.id;
        insert testLicAcc2;
    
        List<Account> accList = [SELECT Id, Name, JVCO_Additional_Information__c, JVCO_Date_TCs_Accepted__c, c2g__CODAAccountsReceivableControl__c 
                                FROM Account WHERE RecordType.Name = 'Customer Account' LIMIT 2];
        System.assert(accList.size() > 0);
        Test.startTest();
        
        JVCO_QueryBuilder queryBuilder = new JVCO_QueryBuilder();
        queryBuilder.objectName = 'Account';
        queryBuilder.isValidObject('Account');
        queryBuilder.setObjectFields('JVCO_Venue__c', 'Licence Account');
        queryBuilder.setObjectFields('Account', 'Licence Account');
        queryBuilder.setObjectFields('Account', 'Customer Account');
        queryBuilder.isValidField('Name');
        queryBuilder.setFieldName(1, 'Name');
        queryBuilder.isValidOperator('equals');
        queryBuilder.setOperatorValue(1, 'equals');
        queryBuilder.setInputValue(1, accList[0].Name);
        
        String queryStr = queryBuilder.getQueryString(TRUE);
        List<SObject> sobjectResults = Database.query(queryStr);
        List<JVCO_DynamicSObject> results = new List<JVCO_DynamicSObject>();
        
        for (SObject s : sobjectResults) {
            results.add(new JVCO_DynamicSObject(s, queryBuilder.objectName, queryBuilder.getQueryFields(), queryBuilder.getReferenceFields()));
        }
        
        results[0].setSelected(true);
        
        queryBuilder.getNonReparentableChild(TRUE);
        queryBuilder.getAllFields(TRUE);
        queryBuilder.getAllFields(FALSE);
        queryBuilder.getFieldLabels(TRUE);
        queryBuilder.getAllFieldNames(TRUE);
        queryBuilder.getQueryAllFields(new List<String>{testAccCust.Id, testAccCust2.Id}, TRUE);
        queryBuilder.reset();
        queryBuilder.getNotes();
        
        Test.stopTest();
    }
}