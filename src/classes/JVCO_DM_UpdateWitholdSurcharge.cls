/* ----------------------------------------------------------------------------------------------
Name: JVCO_DM_UpdateWitholdSurcharge.cls 
Description: Batch class that updates the withhold surcharge field to true for migrated key accounts and or agency accounts on sales invoices.
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
07-Nov-2017  0.1         desiree.m.quijada     Initial creation
----------------------------------------------------------------------------------------------- */

public class JVCO_DM_UpdateWitholdSurcharge implements Database.Batchable<sObject> {
    
    public static List<c2g__codaInvoice__c> salesInvoiceList;

    public static void init()
    {   
        List<c2g__codaInvoice__c> salesInvoiceUpdateList = new List<c2g__codaInvoice__c>();

        if(!salesInvoiceList.isEmpty()){
            for(c2g__codaInvoice__c si : salesInvoiceList)
            {
                si.JVCO_Exempt_from_Surcharge__c = true;
                salesInvoiceUpdateList.add(si);
            }   
        }

        //Update the sales invoices
        if(!salesInvoiceUpdateList.isEmpty()){
            update salesInvoiceUpdateList;       
        }
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT ID, JVCO_Exempt_from_Surcharge__c, JVCO_Protected__c, c2g__Account__r.Type,
                                        JVCO_Number_of_Installment_Line_Items__c
                                        FROM c2g__codaInvoice__c
                                        WHERE JVCO_Number_of_Installment_Line_Items__c > 0
                                        OR c2g__Account__r.Type IN ('Key Account', 'Agency')
                                        OR JVCO_Protected__c = true]);
    }

    public void execute(Database.BatchableContext BC, List<c2g__codaInvoice__c> scope) {

        salesInvoiceList = scope;
        init();
    }
    
    public void finish(Database.BatchableContext BC) {
        System.debug('Done');
    }
    
}