/*
    Created by: Desiree Quijada
    Date Created: 27-July-2017
    Date Modified: 01-Dec-2017 (GREEN-26732 | refactor to improve performance after hitting governor limit - apex cpu time limit in DR3-Delta)
    Details: Bulk update the In Dispute field in Sales Invoice object to match with temp_in_dispute field from the Invoice Object
    Version: v1.0, v2.0
*/

global class JVCO_Batch_InDispute implements Database.Batchable<sObject>, Database.Stateful {
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id, Name, ffps_custRem__In_Dispute__c, JVCO_Number_of_Installment_Line_Items__c, 
                                         JVCO_Protected__c, JVCO_Promise_to_Pay__c, JVCO_Surcharge_Generated__c,                        
                                         (SELECT ID, Name, JVCO_Temp_In_Dispute__c, JVCO_Surcharge_Generated__c FROM Invoices__r) 
                                         FROM c2g__codaInvoice__c order by id asc limit 200000]);
    }
    
    global void execute(Database.BatchableContext BC, List<c2g__codaInvoice__c> salesInvoiceList) {
        
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        
        
        for(c2g__codaInvoice__c si : salesInvoiceList ){
            for(blng__Invoice__c child : si.Invoices__r){
                if(si.ffps_custRem__In_Dispute__c != child.JVCO_Temp_In_Dispute__c){ 
                    si.ffps_custRem__In_Dispute__c = child.JVCO_Temp_In_Dispute__c;
                }
                if(si.JVCO_Surcharge_Generated__c != child.JVCO_Surcharge_Generated__c){
                    si.JVCO_Surcharge_Generated__c = child.JVCO_Surcharge_Generated__c;
                }
            }
        }
        System.debug('@original id' +salesInvoiceList[0].id);
        
        if(!salesInvoiceList.isEmpty()){
            
            //update salesInvoiceList
            
            if(Test.isRunningTest()){
                salesInvoiceList[0].ffps_custRem__In_Dispute__c = null;
            }
            
            List<Database.SaveResult> saveResList = Database.update(salesInvoiceList,false);
            
            for(Integer i=0; i < salesInvoiceList.size(); i++){
                Database.SaveResult indRes = saveResList[i];
                c2g__codaInvoice__c salesInv = salesInvoiceList[i];
                system.debug(salesInv.id);
                
                //every failed update or error will be logged to the error log object blng__ErrorLog__c
                if(!indRes.isSuccess()){
                    for(Database.Error err : indRes.getErrors()) {
                        ffps_custRem__Custom_Log__c errLog =  new ffps_custRem__Custom_Log__c();
                        if(salesInv.ID != null || !String.isBlank(String.valueOf(salesInv.ID))){
                            if(err.getMessage() != null || !String.isBlank(String.valueOf(err.getMessage()))){
                                if(err.getStatusCode() != null || !String.isBlank(String.valueOf(err.getStatusCode()))){
                                    
                                    errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                                    errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(salesInv.Id);
                                    errLog.ffps_custRem__Detail__c = string.valueof(err.getMessage());
                                    errLog.ffps_custRem__Message__c = 'Update In Dispute';
                                    errLog.ffps_custRem__Grouping__c = 'UID-001';
                                } 
                            }
                            errLogList.add(errLog);
                        }
                    }
                }
                else{
                    System.debug('SUCCESS');
                }
            }
            
            if(!errLogList.isEmpty()){
                insert errLogList;
            }  
        }
    }
    
    global void finish(Database.BatchableContext BC) {
    }
}