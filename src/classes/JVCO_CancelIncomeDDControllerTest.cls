@isTest
public class JVCO_CancelIncomeDDControllerTest {
	@testSetup static void createTestData() 
    {
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        //Settings
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_codaCreditNoteTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        c2g__codaDimension2__c dim2 = JVCO_TestClassHelper.setDim2('PPL');
        insert dim2;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;

        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        insert sInv;
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference sInvRef = new c2g.CODAAPICommon.Reference();
        sInvRef.Id = sInv.Id;

        Test.startTest();
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(context, sInvRef);
        Test.stopTest();
        
        Income_Direct_Debit__c idd = new Income_Direct_Debit__c();
        idd.Account__c = licAcc.id;
        idd.DD_Bank_Sort_Code__c = '938600';
        idd.DD_Bank_Account_Number__c = '42368003';
        idd.DD_Collection_Day__c = '3';
        idd.DD_Collection_Period__c = 'Monthly';
        idd.DD_Status__c = 'New Instruction';
        idd.DD_Bank_Account_Name__c = 'Test';
        idd.DD_Account_Email__c = c.Email;
        idd.Contact__c = c.id;
        insert idd;
        
        JVCO_Invoice_Group__c invgrp = new JVCO_Invoice_Group__c();
        invgrp.CC_SINs__c = sInv.Name;
		invgrp.JVCO_Status__c = 'Active';
        invgrp.JVCO_Total_Amount__c = sInv.c2g__OutstandingValue__c;
        insert invgrp;
       
        
        SMP_DirectDebit_GroupInvoice__c smpgroup = new SMP_DirectDebit_GroupInvoice__c();
        smpgroup.Income_Direct_Debit__c = idd.id;
        smpgroup.Invoice_Group__c = invgrp.id;
        insert smpgroup;
        
    }
        @isTest static void testcancelIdd() {
            c2g__codaInvoice__c siRec = [select id, JVCO_Invoice_Group__c from c2g__codaInvoice__c limit 1];
            List<Income_Direct_Debit__c> iddRec = [select id from Income_Direct_Debit__c limit 1];
            
            JVCO_Invoice_Group__c invoicegroupRec = [Select id from JVCO_Invoice_Group__c limit 1];
            siRec.JVCO_Invoice_Group__c = invoicegroupRec.id;
            update siRec;
            
            Test.setCurrentPage(Page.JVCO_CancelIncomeDD);
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(iddRec);
            sc.setSelected(iddRec);
            JVCO_CancelIncomeDDController cont = new JVCO_CancelIncomeDDController(sc);
            cont.init();
            cont.cancelIncomeDD();
            
            Income_Direct_Debit__c rec = [select id, DD_Status__c FROM Income_Direct_Debit__c limit 1];
            
            system.assertEquals('Cancelled', rec.DD_Status__c);
            
        }
    
    	@isTest static void testNoSelected() {
            c2g__codaInvoice__c siRec = [select id from c2g__codaInvoice__c limit 1];
            List<Income_Direct_Debit__c> iddRec = new List<Income_Direct_Debit__c>();
            String errorMessage = '';
            Test.setCurrentPage(Page.JVCO_CancelIncomeDD);
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(iddRec);
            sc.setSelected(iddRec);
            JVCO_CancelIncomeDDController cont = new JVCO_CancelIncomeDDController(sc);
            try{
               cont.init(); 
            }catch(exception e){
                       errorMessage = e.getMessage();
            }
            
            //cont.cancelIncomeDD();
            
            Income_Direct_Debit__c rec = [select id, DD_Status__c FROM Income_Direct_Debit__c limit 1];
            
            system.assertEquals('', errorMessage);
            
        }
}