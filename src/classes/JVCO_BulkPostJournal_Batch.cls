global class JVCO_BulkPostJournal_Batch implements Database.Batchable<sObject>
{
    private String query;
    
    global JVCO_BulkPostJournal_Batch(String pickQuery)
    {
        if(pickQuery == 'Zara')
        {
            query = 'SELECT Id '
                + 'FROM c2g__codaJournal__c '
                + 'WHERE c2g__JournalStatus__c = \'In Progress\' '
                + 'AND c2g__JournalDescription__c = \'Transfer Cash to Parent\'';
        }
        else if(pickQuery == 'Ana')
        {
            query = 'SELECT Id '
                + 'FROM c2g__codaJournal__c '
                + 'WHERE c2g__JournalStatus__c = \'In Progress\' '
              //  + 'AND c2g__JournalDescription__c = \'Transfer Cash to Parent\' '
                + 'AND JVCO_Temp_Journal_Ex_Id__c != \'\'';
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<c2g.CODAAPICommon.Reference> refList = new List<c2g.CODAAPICommon.Reference>();
        c2g.CODAAPICommon.Reference reference;
        for(sObject sRec : scope)
        {
            reference = new c2g.CODAAPICommon.Reference();
            reference.Id = sRec.Id;
            refList.add(reference);
        }
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        c2g.CODAAPIJournal_12_0.BulkPostJournal(context, refList);
    }

    global void finish(Database.BatchableContext BC)
    {
    }
}