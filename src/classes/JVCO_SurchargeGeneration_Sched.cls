global class JVCO_SurchargeGeneration_Sched implements Schedulable
{
  
	global void execute(SchedulableContext sc)
    {

		//JVCO_SurchargeGeneration_Batch surchargeGeneration = new JVCO_SurchargeGeneration_Batch();
		// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
		//String sch = '0 0 0 * * ?';
		//String jobID = System.schedule('Surcharge Generation Batch', sch, surchargeGeneration);
        Database.executeBatch(new JVCO_SurchargeGeneration_Batch(), 50);
	}
    
}