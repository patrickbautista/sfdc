/* ----------------------------------------------------------------------------------------------
    Name: JVCO_InvoiceCancellationLogic.cls 
    Description: Logic class for Invoice cancellation

    Date          Version     Author              Summary of Changes 
    -----------   -------     -----------------   -----------------------------------------
    27-Mar-2017   0.1         ryan.i.r.limlingan  Intial creation
    27-Aug-2017   0.2         franz.g.a.dimaapi   Added Methods to moved some logics inside cancelInvoice method
    16-Nov-2017   0.3         franz.g.a.dimaapi   Refactor Class
    30-Jan-2018   0.4         mel.andrei.b.santos Updated cancelOpportunity which will set JVCO_OpportunityCancelled__c to true if triggered based on GREEN-29724
    02-Feb-2018   0.5         mary.ann.a.ruelan   Updated linkToBillingInvoice to avoid duplication of credit note ids - GREEN-29526
    08-Mar-2018   0.6         franz.g.a.dimaapi   GREEN-30828 - Refactor class and change logics to improve performance
    10-Oct-2018   0.7         franz.g.a.dimaapi   GREEN-32724 - Refactor class and change logics to improve performance
----------------------------------------------------------------------------------------------- */
public class JVCO_InvoiceCancellationLogic
{
    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Links Credit Note generated to the Billing Invoice of the Sales Invoice to match
        Inputs: List<blng__Invoice__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        27-Mar-2017 ryan.i.r.limlingan  Initial version of function
        17-Jul-2017 franz.g.a.dimaapi   Added VAT Legacy Number Logic
        01-Aug-2017 L.B.Singh           added a set to remove the duplication of the credit note.
        04-Dec-2017 jasper.j.figueroa   Added values for Credit Note parent, migrated checkbox, vat reg number for credit note with migrated invoice
        06-Dec-2017 jasper.j.figueroa   Changes undone - fix for GREEN-26549
        02-Feb-2018 mary.ann.a.ruelan   updated code to avoid duplication of credit note IDs and call it in before update instead - GREEN-29526
        12-Jun-2018 franz.g.a.dimaapi   GREEN-32321 - Used JVCO_From_Convert_to_Credit__c flag to identify the cancellation function
    ----------------------------------------------------------------------------------------------- */
    public static void linkToBillingInvoice(List<c2g__codaCreditNote__c> creditNoteList)
    {
        Map<Id, c2g__codaCreditNote__c> sInvIdToCNoteMap = new Map<Id, c2g__codaCreditNote__c>(); //Mary
        for(c2g__codaCreditNote__c cNote : creditNoteList)
        {
            // Only process for Cancellation Credit Note records with Invoice replaced as Reason
            if(cNote.JVCO_From_Post_And_Match__c && cNote.JVCO_VAT_Registration_Number__c == null)
            {
                sInvIdToCNoteMap.put(cNote.c2g__Invoice__c, cNote);                 
            }
        }

        if(!sInvIdToCNoteMap.isEmpty())
        {
            List<Order> orderListToUpdate = [SELECT Id, JVCO_Sales_Credit_Note__c,
                                                       JVCO_Sales_Invoice__c,
                                                       JVCO_Sales_Invoice__r.JVCO_Licence_Start_Date__c,
                                                       JVCO_Sales_Invoice__r.JVCO_Licence_End_Date__c,
                                                       JVCO_Sales_Invoice__r.c2g__OwnerCompany__r.c2g__VATRegistrationNumber__c,
                                                       JVCO_Invoice_Type__c,
                                                       CreatedById,
                                                       SBQQ__Quote__c,
                                                       SBQQ__Quote__r.SBQQ__Opportunity2__c
                                                       FROM Order
                                                       WHERE JVCO_Sales_Invoice__c
                                                       IN :sInvIdToCNoteMap.keySet()
                                                       AND JVCO_Sales_Credit_Note__c = NULL];

            List<User> surUserList = [SELECT Id, Name FROM User WHERE Name = 'Surcharge User']; 
            for(Order ord : orderListToUpdate)
            {                         
                c2g__codaCreditNote__c creditNote = new c2g__codaCreditNote__c();                 
                creditNote = sInvIdToCNoteMap.get(ord.JVCO_Sales_Invoice__c);             
                creditNote.JVCO_VAT_Registration_Number__c = ord.JVCO_Sales_Invoice__r.c2g__OwnerCompany__r.c2g__VATRegistrationNumber__c;
                //setting credit note period start date/end date from sales invoice
                creditNote.JVCO_Period_Start_Date__c = ord.JVCO_Sales_Invoice__r.JVCO_Licence_Start_Date__c;
                creditNote.JVCO_Period_End_Date__c = ord.JVCO_Sales_Invoice__r.JVCO_Licence_End_Date__c; 
                //GREEN-31875 08-May-2018 john.patrick valdez
                creditNote.JVCO_Sales_Rep__c = ord.JVCO_Invoice_Type__c == 'Surcharge' ? surUserList[0].Id :creditNote.CreatedById;
            }

            if(!orderListToUpdate.isEmpty())
            {
                cancelInvoice(sInvIdToCNoteMap);
            }
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Deletes records associated to the Billing Invoice, and updates the Billing Invoice
        Inputs: LMap<Id, c2g__codaCreditNote__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        27-Mar-2017 ryan.i.r.limlingan  Initial version of function
        06-Apr-2017 ryan.i.r.limlingan  Reflected which fields to update on related objects upon cancellation
        26-Jul-2017 r.p.valencia.iii    Added Quote Status to be updated to cancelled on logic.
        11-Sep-2017 franz.g.a.dimaapi   Changed Logic - Used BInvLine Order Product instead of BInv Order
                                        Moved Opportunity Update Fields Logic to cancelOpportunity method
                                        GREEN-22873
        13-Nov-2017 franz.g.a.dimaapi   Refactor method, and moved quotecancel logic to separate method.
    ----------------------------------------------------------------------------------------------- */
    private static void cancelInvoice(Map<Id, c2g__codaCreditNote__c> sInvIdToCNoteMap)
    {   
        cancelSalesInvoice(sInvIdToCNoteMap.keySet());
        //Update Related Document Queue Status to Cancel
        cancelDocumentQueue(sInvIdToCNoteMap.keySet());
        Id jobId = Database.executeBatch(new JVCO_BillingCancellationLogic(sInvIdToCNoteMap), 1);
        //START
        if(!sInvIdToCNoteMap.isEmpty()){
            String key = '';
            for (String S: sInvIdToCNoteMap.keySet()){
                key = s;
                system.debug(key);
                break;
            }
            Account accRec = new Account();
            accRec.Id = sInvIdToCNoteMap.get(key).c2g__Account__c;
            accRec.JVCO_BillingCancellationApexJob__c = jobId;
            update accRec;  
        }
        //END GREEN-35872 - mariel.m.buena - 23/10/2020
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: GREEN-32724 - Cancel Sales Invoice

        Inputs: 
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        10-Oct-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static void cancelSalesInvoice(Set<Id> sInvIdSet)
    {   
        List<c2g__codaInvoice__c> sInvList = new List<c2g__codaInvoice__c>();
        for(Id sInvId : sInvIdSet)
        {
            c2g__codaInvoice__c sInv = new c2g__codaInvoice__c();
            sInv.Id = sInvId;
            sInv.JVCO_Cancelled__c = true;
            sInv.JVCO_Surcharge_Generation_Date__c = null;
            sInv.JVCO_Surcharge_Generated__c = false;
            sInvList.add(sInv);
        }
        update sInvList;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Change Document Queue's Generation Status to Cancelled 
                     After Cancelling Sales Invoice- GREEN-22474

        Inputs: 
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        27-Aug-2017 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static void cancelDocumentQueue(Set<Id> sInvIdSet)
    {
        // Set Status as Cancelled on all Document Queue records related to Invoice
        List<JVCO_Document_Queue__c> docQueueToCancelList = [SELECT Id, JVCO_Generation_Status__c
                                                             FROM JVCO_Document_Queue__c
                                                             WHERE JVCO_Related_SalesInvoice__c
                                                             IN :sInvIdSet];
        if(!docQueueToCancelList.isEmpty())
        {
            for(JVCO_Document_Queue__c docQueue : docQueueToCancelList)
            {
                docQueue.JVCO_Generation_Status__c = 'Cancelled';
            }
            update docQueueToCancelList;    
        }
    }
}