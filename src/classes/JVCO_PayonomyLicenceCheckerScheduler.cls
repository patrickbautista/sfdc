public class JVCO_PayonomyLicenceCheckerScheduler implements Schedulable
{
    public void execute(SchedulableContext sc)
    {
        delete [SELECT Id FROM PAYLIC1__Licence__c];
        sagePayLicenceChecker();
        directDebitLicenceChecker();
    }
    
    private static String getSchedulerExpression(DateTime dt) 
    {
         return '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();    
    }

    public static void callExecuteMomentarily(String scheduledName)  
    {
        System.schedule(scheduledName, getSchedulerExpression(DateTime.Now().addSeconds(5)), new JVCO_PayonomyLicenceCheckerScheduler());
    }
    
    @Future(callout = true)
    public static void sagePayLicenceChecker()
    {
        String PRODUCT_NAME = PAYSPCP1.SagePayPaymentExtension.PRODUCT_NAME;
        String PRODUCT_VERSION = PAYSPCP1.SagePayPaymentExtension.PRODUCT_VERSION;
        PAYLIC1.LicenceManager.LicenceCheck a = PAYLIC1.LicenceManager.checkLicence(PRODUCT_NAME, PRODUCT_VERSION);
    }
    
    @Future(callout = true)
    public static void directDebitLicenceChecker()
    {
        PAYLIC1.LicenceManager.LicenceCheck a = PAYLIC1.LicenceManager.checkLicence('Payonomy Validator','1.1');
    }
}