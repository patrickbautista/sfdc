@isTest
private class JVCO_AffiliationMergeTest
{
  @testSetup
  public static void testSetup(){
    
    //Account customerAccount = JVCO_TestClassObjectBuilder.createCustomerAccount();
    //insert customerAccount;

    //Account licencAccount = JVCO_TestClassObjectBuilder.createLicenceAccount(customerAccount.id);
    //insert licencAccount;
    //licencAccount.JVCO_MasterRecordId__c = licencAccount.id;
    //update licencAccount;
    //system.debug('licencAccount' + licencAccount);

    JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        //JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        Test.startTest();
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        //custAcc.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        c.MailingPostalCode = 'ne6 5ls';
        c.MailingCity = 'Test City';
        c.MailingCountry = 'Test Country';
        c.MailingState = 'Test State';
        c.MailingStreet = 'Test Street';
        c.FirstName = 'Test Name';
        c.LastName = 'Test Last Name';
        insert c;
        
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc.AccountNumber = '832020';
        //licAcc.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
        insert licAcc;  

        Account licAcc2 = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc2.AccountNumber = '832021';
        insert licAcc2;

        Account licAcc3 = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc3.AccountNumber = '832022';
        LicAcc3.JVCO_Live__c = true;
        insert licAcc3; 

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        JVCO_Venue__c ven = JVCO_TestClassHelper.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassHelper.createAffiliation(licAcc.Id, ven.Id);
        insert aff;


        
        JVCO_Event__c event = new JVCO_Event__c();
        event.JVCO_Venue__c = ven.id;
        event.JVCO_Tariff_Code__c = 'LC';
        event.JVCO_Event_Type__c = 'Concert - Qualifies for Old Rate';
        event.JVCO_Event_Start_Date__c = date.today();
        event.JVCO_Event_End_Date__c = date.today() + 7;
        event.License_Account__c=licAcc3.id;
        event.Headline_Type__c = 'No Headliner';
        event.JVCO_Number_of_Set_Lists_Expected__c = 2;
        insert event;
        
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        q.JVCO_Cust_Account__c = custAcc.Id;
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        ql.Affiliation__c = aff.id;
        ql.JVCO_Event__c = event.id;
        ql.JVCO_Venue__c = ven.id;
        insert ql;
        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = aff.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;   


        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;

        SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(licAcc.id, p.id);
      s.SBQQ__Contract__c = contr.Id;
      s.Affiliation__c = aff.Id;
      s.SBQQ__QuoteLine__c = ql.Id;
      insert s;

        q.SBQQ__MasterContract__c = contr.Id;
        update q;
        
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        OrderItem oi1 = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi1;
        Test.stopTest();

        Set<Id> orderGroupIdSet = new Set<Id>();  
        orderGroupIdSet.add(orderGroup.Id);

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_TestClassHelper.setGeneralSettingsCS();
        insert new GLA_for_Cash_Transfer__c(name = '10100 - Bank Cash'); 
        //JVCO_BackgroundMatchingLogic.stopMatching = true;
        JVCO_FFUtil.stopCashTransferLogic = true;
        
        Database.executeBatch(new JVCO_OrderMultipleBillNowLogicBatch(orderGroupIdSet));
        
  }

  @isTest
  static void mergeAffil_Scene1()
  {
    id idLic =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    system.debug('idLic' + idLic);

    Account selectAccount = new Account();
    selectAccount = [select id, name, RecordTypeId, JVCO_MasterRecordId__c from Account  where RecordTypeId = :idLic order by CreatedDate ASC limit 1];
    system.debug(selectAccount); 

    list<id> idList = new list<id>();
    idList.add(selectAccount.Id);                                     

    test.StartTest();

    JVCO_Venue__c testVenue = JVCO_TestClassObjectBuilder.createVenue();
    testVenue.name = 'Test 1';
    testVenue.External_Id__c  = 'JVCO_QLTHTestsamplevenexid002';
    insert testVenue;

    JVCO_Venue__c testVenue2 = JVCO_TestClassObjectBuilder.createVenue();
    testVenue2.name = 'Test 2';
    testVenue2.JVCO_Email__c = 'JVCO_QuoteLineTriggerHandlerTest2@email.com';
        testVenue2.External_Id__c = 'JVCO_QLTHTest2samplevenexid';
        testVenue2.JVCO_Street__c = 'Test2 Street';
        testVenue2.JVCO_City__c = 'Test2 City';
        testVenue2.JVCO_Country__c = 'Test2 Country';
        testVenue2.JVCO_Postcode__c = 'TN32 5SL';
    insert testVenue2;

    JVCO_Affiliation__c testAffil = JVCO_TestClassObjectBuilder.createAffiliation(selectAccount.id, testVenue.id);
    testAffil.JVCO_Closure_Reason__c = 'Deceased';
    testAffil.JVCO_End_Date__c = date.today().addDays(10);
    testAffil.JVCO_Status__c = 'Trading On Hold';
    testAffil.JVCO_Omit_Quote_Line_Group__c = true;
    testAffil.JVCO_Revenue_Type__c = 'New Business';
    insert testAffil;
    system.debug(testAffil);

    JVCO_Affiliation__c testAffil3 = JVCO_TestClassObjectBuilder.createAffiliation(selectAccount.id, testVenue.id);
    testAffil3.JVCO_Closure_Reason__c = 'Dissolved';
    testAffil3.JVCO_End_Date__c = date.today().addDays(20);
    testAffil3.JVCO_Status__c = 'No Longer Trading';
    testAffil3.JVCO_Omit_Quote_Line_Group__c = true;
    testAffil3.JVCO_Affiliation_Temp_External_ID__c = 'PRS';
    testAffil3.JVCO_Revenue_Type__c = 'Review';
    insert testAffil3;
    system.debug(testAffil3);

    /*JVCO_Affiliation__c testAffil2 = JVCO_TestClassObjectBuilder.createAffiliation(selectAccount.id, testVenue.id);
    testAffil2.JVCO_Closure_Reason__c = null;
    testAffil2.JVCO_End_Date__c = null;
    testAffil2.JVCO_Omit_Quote_Line_Group__c = true;
    insert testAffil2;
    system.debug(testAffil2);*/

    JVCO_AffiliationMerge.mergeAccountAffiliation(idList);
    test.StopTest();
  }

  @isTest
  static void mergeAffil_Scene2()
  {
    id idLic =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    system.debug('idLic' + idLic);

    Account selectAccount = new Account();
    selectAccount = [select id, name, RecordTypeId, JVCO_MasterRecordId__c from Account  where RecordTypeId = :idLic order by CreatedDate ASC limit 1];
    system.debug(selectAccount); 

    list<id> idList = new list<id>();
    idList.add(selectAccount.Id);                                     

    test.StartTest();

    JVCO_Venue__c testVenue = JVCO_TestClassObjectBuilder.createVenue();
    testVenue.name = 'Ven 1';
    testVenue.External_Id__c  = 'JVCO_QLTHTestsamplevenexid002';
    insert testVenue;

    JVCO_Venue__c testVenue2 = JVCO_TestClassObjectBuilder.createVenue();
    testVenue2.name = 'Test 2';
    testVenue2.JVCO_Email__c = 'JVCO_QuoteLineTriggerHandlerTest2@email.com';
        testVenue2.External_Id__c = 'JVCO_QLTHTest2samplevenexid';
        testVenue2.JVCO_Street__c = 'Test2 Street';
        testVenue2.JVCO_City__c = 'Test2 City';
        testVenue2.JVCO_Country__c = 'Test2 Country';
        testVenue2.JVCO_Postcode__c = 'TN32 5SL';
    insert testVenue2;

    JVCO_Affiliation__c testAffil = JVCO_TestClassObjectBuilder.createAffiliation(selectAccount.id, testVenue.id);
    testAffil.JVCO_Closure_Reason__c = null;
    testAffil.JVCO_End_Date__c = null;
    testAffil.JVCO_Status__c = 'Trading On Hold';
    testAffil.JVCO_Omit_Quote_Line_Group__c = true;
    testAffil.JVCO_Revenue_Type__c = 'New Business';
    insert testAffil;
    system.debug(testAffil);

    JVCO_Affiliation__c testAffil3 = JVCO_TestClassObjectBuilder.createAffiliation(selectAccount.id, testVenue.id);
    testAffil3.JVCO_Closure_Reason__c = 'Dissolved';
    testAffil3.JVCO_End_Date__c = date.today().addDays(20);
    testAffil3.JVCO_Status__c = 'No Longer Trading';
    testAffil3.JVCO_Omit_Quote_Line_Group__c = true;
    testAffil3.JVCO_Affiliation_Temp_External_ID__c = 'PRS';
    testAffil3.JVCO_Revenue_Type__c = 'Review';
    insert testAffil3;
    system.debug(testAffil3);

    JVCO_Affiliation__c testAffil2 = JVCO_TestClassObjectBuilder.createAffiliation(selectAccount.id, testVenue.id);
    testAffil2.JVCO_Closure_Reason__c = null;
    testAffil2.JVCO_End_Date__c = null;
    testAffil2.JVCO_Omit_Quote_Line_Group__c = true;
    insert testAffil2;
    system.debug(testAffil2);

    JVCO_AffiliationMerge.mergeAccountAffiliation(idList);
    test.StopTest();
  }

  @isTest
  static void venueMerge_Scene1()
  {
    id idLic =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    system.debug('idLic' + idLic);

    Account selectAccount = new Account();
    selectAccount = [select id, name, RecordTypeId, JVCO_MasterRecordId__c from Account  where RecordTypeId = :idLic order by CreatedDate ASC limit 1];
    system.debug(selectAccount);                                  

    test.StartTest();

    JVCO_Venue__c testVenue = JVCO_TestClassObjectBuilder.createVenue();
    testVenue.name = 'Ven1';
    testVenue.External_Id__c  = 'JVCO_QLTHTestsamplevenexid002';
    insert testVenue;

    JVCO_Venue__c testVenue2 = JVCO_TestClassObjectBuilder.createVenue();
    testVenue2.name = 'Test 2';
    testVenue2.JVCO_Email__c = 'JVCO_QuoteLineTriggerHandlerTest2@email.com';
        testVenue2.External_Id__c = 'JVCO_QLTHTest2samplevenexid';
        testVenue2.JVCO_Street__c = 'Test2 Street';
        testVenue2.JVCO_City__c = 'Test2 City';
        testVenue2.JVCO_Country__c = 'Test2 Country';
        testVenue2.JVCO_Postcode__c = 'TN32 5SL';
    insert testVenue2;

    JVCO_Affiliation__c testAffil = JVCO_TestClassObjectBuilder.createAffiliation(selectAccount.id, testVenue.id);
    testAffil.JVCO_Closure_Reason__c = 'Deceased';
    testAffil.JVCO_End_Date__c = date.today().addDays(10);
    testAffil.JVCO_Status__c = 'Trading On Hold';
    testAffil.JVCO_Omit_Quote_Line_Group__c = true;
    testAffil.JVCO_Revenue_Type__c = 'New Business';
    insert testAffil;
    system.debug(testAffil);

    JVCO_Affiliation__c testAffil3 = JVCO_TestClassObjectBuilder.createAffiliation(selectAccount.id, testVenue.id);
    testAffil3.JVCO_Closure_Reason__c = 'Dissolved';
    testAffil3.JVCO_End_Date__c = date.today().addDays(20);
    testAffil3.JVCO_Status__c = 'No Longer Trading';
    testAffil3.JVCO_Omit_Quote_Line_Group__c = true;
    testAffil3.JVCO_Affiliation_Temp_External_ID__c = 'PRS';
    testAffil3.JVCO_Revenue_Type__c = 'Review';
    insert testAffil3;
    system.debug(testAffil3);

    list<id> idList = new list<id>();
    idList.add(testVenue.id); 

    JVCO_AffiliationMerge.mergeVenueAffiliation(idList);
    test.StopTest();
  }

  @isTest
  static void venueMerge_Scene2()
  {
    id idLic =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    system.debug('idLic' + idLic);

    Account selectAccount = new Account();
    selectAccount = [select id, name, RecordTypeId, JVCO_MasterRecordId__c from Account where RecordTypeId = :idLic order by CreatedDate ASC limit 1];
    system.debug(selectAccount); 

                                      

    test.StartTest();

    JVCO_Venue__c testVenue = JVCO_TestClassObjectBuilder.createVenue();
    testVenue.name='Ven1';
    testVenue.External_Id__c  = 'JVCO_QLTHTestsamplevenexid002';
    insert testVenue;

    JVCO_Venue__c testVenue2 = JVCO_TestClassObjectBuilder.createVenue();
    testVenue2.name = 'Test 2';
    testVenue2.JVCO_Email__c = 'JVCO_QuoteLineTriggerHandlerTest2@email.com';
        testVenue2.External_Id__c = 'JVCO_QLTHTest2samplevenexid';
        testVenue2.JVCO_Street__c = 'Test2 Street';
        testVenue2.JVCO_City__c = 'Test2 City';
        testVenue2.JVCO_Country__c = 'Test2 Country';
        testVenue2.JVCO_Postcode__c = 'TN32 5SL';
    insert testVenue2;

    JVCO_Affiliation__c testAffil = JVCO_TestClassObjectBuilder.createAffiliation(selectAccount.id, testVenue.id);
    testAffil.JVCO_Closure_Reason__c = null;
    testAffil.JVCO_End_Date__c = null;
    testAffil.JVCO_Status__c = 'Trading On Hold';
    testAffil.JVCO_Omit_Quote_Line_Group__c = true;
    testAffil.JVCO_Revenue_Type__c = 'New Business';
    insert testAffil;
    system.debug(testAffil);

    JVCO_Affiliation__c testAffil3 = JVCO_TestClassObjectBuilder.createAffiliation(selectAccount.id, testVenue.id);
    testAffil3.JVCO_Closure_Reason__c = 'Dissolved';
    testAffil3.JVCO_End_Date__c = date.today().addDays(20);
    testAffil3.JVCO_Status__c = 'No Longer Trading';
    testAffil3.JVCO_Omit_Quote_Line_Group__c = true;
    testAffil3.JVCO_Affiliation_Temp_External_ID__c = 'PRS';
    testAffil3.JVCO_Revenue_Type__c = 'Review';
    insert testAffil3;
    system.debug(testAffil3);

    /*JVCO_Affiliation__c testAffil2 = JVCO_TestClassObjectBuilder.createAffiliation(selectAccount.id, testVenue.id);
    testAffil2.JVCO_Closure_Reason__c = null;
    testAffil2.JVCO_End_Date__c = null;
    testAffil2.JVCO_Omit_Quote_Line_Group__c = true;
    insert testAffil2;
    system.debug(testAffil2);*/

    list<id> idList = new list<id>();
    idList.add(testVenue.Id);   

    JVCO_AffiliationMerge.mergeVenueAffiliation(idList);
    test.StopTest();
  }

}