@isTest
private class JVCO_PayonomyPaymentHandlerTest
{
    /*@testSetup static void createTestData() 
    {
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
            listQueue.add(q2);
            queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
            listQueue.add(q3);
            queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
            listQueue.add(q4);
            queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
            listQueue.add(q5);
            queuesobject q6 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankAccount__c'); 
            listQueue.add(q6);
            queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q7);
            queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatement__c'); 
            listQueue.add(q8);          
            queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntry__c'); 
            listQueue.add(q9);  
            queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='PAYBASE2__Payment__c'); 
            listQueue.add(q10);
            queuesobject q11 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransaction__c'); 
            listQueue.add(q11);
            //queuesobject q12 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoiceInstallmentLineItem__c'); 
            //listQueue.add(q12);
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }

        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
        testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc.Dimension_1_Required__c = false;
        testGeneralLedgerAcc.Dimension_2_Required__c = false;
        testGeneralLedgerAcc.Dimension_3_Required__c = false;
        testGeneralLedgerAcc.Dimension_4_Required__c = false;
        testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
        testGeneralLedgerAcc.ownerid = testGroup.Id;

        insert testGeneralLedgerAcc;

        c2g__codaGeneralLedgerAccount__c accRecvblControlGLA = new c2g__codaGeneralLedgerAccount__c();
        accRecvblControlGLA.c2g__AdjustOperatingActivities__c = false;
        accRecvblControlGLA.c2g__AllowRevaluation__c = false;
        accRecvblControlGLA.c2g__CashFlowCategory__c = 'Operating Activities';
        accRecvblControlGLA.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        accRecvblControlGLA.c2g__GLAGroup__c = 'Accounts Receivable';
        accRecvblControlGLA.c2g__ReportingCode__c = '10040';
        accRecvblControlGLA.c2g__TrialBalance1__c = 'Assets';
        accRecvblControlGLA.c2g__TrialBalance2__c = 'Current Assets';
        accRecvblControlGLA.c2g__TrialBalance3__c = 'Accounts Receivables';
        accRecvblControlGLA.c2g__Type__c = 'Balance Sheet';
        accRecvblControlGLA.Name = '10040 - Accounts Receivable';
        accRecvblControlGLA.ownerid = testGroup.Id;
        insert accRecvblControlGLA;

       
        //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedgerAcc.id);
        insert taxCode;


        //Create Account
        Account testAccCust = new Account(Name='Test Customer Account');
        testAccCust.AccountNumber = '999999';
        testAccCust.c2g__CODAAccountsReceivableControl__c = accRecvblControlGLA.id;
        testAccCust.Type='Customer';
        testAccCust.RecordTypeId = accountRecordTypes.get('Customer Account');
        insert testAccCust;
        
        Account testAcc = JVCO_TestClassObjectBuilder.createLicenceAccount(testAccCust.id);
        testAcc.AccountNumber = '987654';
        testAcc.c2g__CODAAccountsReceivableControl__c = accRecvblControlGLA.id;
        testAcc.c2g__CODAAllowDeleteInUse__c = false;
        testAcc.c2g__CODACreditLimitEnabled__c = false;
        testAcc.c2g__CODACreditStatus__c = 'Credit Allowed';
        testAcc.c2g__CODADaysOffset1__c = 0.0;
        testAcc.c2g__CODADiscount1__c = 0.0;
        testAcc.c2g__CODAFederallyReportable1099__c = false;
        testAcc.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testAcc.c2g__CODAIntercompanyAccount__c = false;
        testAcc.c2g__CODASalesTaxStatus__c = 'Taxable';
        testAcc.JVCO_Customer_Account__c = testAccCust.id;
        testAcc.RecordTypeId = accountRecordTypes.get('Licence Account');
        testAcc.c2g__CODAOutputVATCode__c = taxCode.id;
        testAcc.ffps_custRem__Preferred_Communication_Channel__c='Email';
        insert testAcc;

        /*Account testAcc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(testAccCust.id);
        testAcc2.AccountNumber = '123456';
        testAcc2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAcc2.c2g__CODAAllowDeleteInUse__c = false;
        testAcc2.c2g__CODACreditLimitEnabled__c = false;
        testAcc2.c2g__CODACreditStatus__c = 'Credit Allowed';
        testAcc2.c2g__CODADaysOffset1__c = 0.0;
        testAcc2.c2g__CODADiscount1__c = 0.0;
        testAcc2.c2g__CODAFederallyReportable1099__c = false;
        testAcc2.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testAcc2.c2g__CODAIntercompanyAccount__c = false;
        testAcc2.c2g__CODASalesTaxStatus__c = 'Taxable';
        testAcc2.JVCO_Customer_Account__c = testAccCust.id;
        testAcc2.RecordTypeId = accountRecordTypes.get('Licence Account');
        testAcc2.c2g__CODAOutputVATCode__c = taxCode.id;
        insert testAcc2;*/

        /*c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
        testCompany.c2g__CashMatchingCurrencyMode__c = 'Account';
        testCompany.c2g__Country__c = 'GB';
        testCompany.c2g__ECCountryCode__c = 'GB';
        testCompany.c2g__UnitOfWork__c = 1.0;
        testCompany.Name = 'Test Company';
        testCompany.ownerid = testGroup.Id;

        insert testCompany;

        c2g__codaAccountingCurrency__c accCurrency = new c2g__codaAccountingCurrency__c();
        accCurrency.c2g__OwnerCompany__c = testCompany.id;
        accCurrency.c2g__DecimalPlaces__c = 2;
        accCurrency.Name = 'GBP';
        accCurrency.c2g__Dual__c = true ;
        accCurrency.ownerid = testGroup.Id;
        accCurrency.c2g__Home__c = true;
        accCurrency.c2g__UnitOfWork__c = 2.0;
        insert accCurrency;

        c2g__codaBankAccount__c testBankAccount = new c2g__codaBankAccount__c();
        testBankAccount.c2g__AccountName__c = 'Test Bank Account';
        testBankAccount.c2g__AccountNumber__c = '987654';
        testBankAccount.c2g__BankName__c = 'Barclays Bank - Current';
        testBankAccount.c2g__GeneralLedgerAccount__c = testGeneralLedgerAcc.id;
        testBankAccount.Name = 'Test Bank Account 1';
        testBankAccount.c2g__OwnerCompany__c = testCompany.Id;
        testBankAccount.c2g__ReportingCode__c = 'BARC-CURR-JV';
        testBankAccount.c2g__BankAccountCurrency__c = accCurrency.id;
        insert testBankAccount;

        //added by edmundCua
        JVCO_Payonomy_Error_Codes__c testPayErrorCode = new JVCO_Payonomy_Error_Codes__c();
        testPayErrorCode.name = 'Refer to Payer';
        testPayErrorCode.JVCO_Deliberate__c = 'Deliberate';
        testPayErrorCode.JVCO_Description__c = 'Error object in testClass';
        insert testPayErrorCode;


        testCompany.c2g__BankAccount__c = testBankAccount.id;
        update testCompany;

        c2g__codaYear__c yr= new c2g__codaYear__c();
        yr.Name ='2016';
        yr.c2g__AutomaticPeriodList__c =  true;
        yr.c2g__OwnerCompany__c = testCompany.id;
        yr.c2g__ExternalId__c = 'yzsd1234';
        yr.c2g__NumberOfPeriods__c =11;
        yr.c2g__StartDate__c =  system.today() - 10;
        yr.c2g__Status__c = 'Open';
        yr.c2g__PeriodCalculationBasis__c = '445';
        yr.c2g__YearEndMode__c = 'Full Accounting Code' ; 
        yr.c2g__UnitOfWork__c = 12;
        yr.ownerid = testGroup.Id;
        insert yr;

        c2g__codaPeriod__c testPeriod = new c2g__codaPeriod__c();
        testPeriod.c2g__OwnerCompany__c = testCompany.id;
        testPeriod.c2g__StartDate__c = System.today()-10;
        testPeriod.c2g__EndDate__c = System.today()+10;
        testPeriod.c2g__PeriodNumber__c ='123';
        testPeriod.c2g__Description__c ='test Desc';
        testPeriod.c2g__PeriodGroup__c = 'Q1';
        testPeriod.c2g__PeriodNumber__c = '1';
        testPeriod.c2g__YearName__c = yr.id;
        insert testPeriod;

        c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c();
        userCompany.c2g__Company__c = testCompany.id;
        userCompany.c2g__User__c = userInfo.getUserId();
        userCompany.c2g__ExternalId__c = 'ABCDE1234567876';
        userCompany.c2g__UnitOfWork__c = 111 ;
        insert userCompany;

        //Create Sales Invoice
        c2g__codaInvoice__c testInvoice = new c2g__codaInvoice__c();
        testInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testInvoice.c2g__InvoiceDate__c = date.today().addDays(-7);
        testInvoice.c2g__DueDate__c = date.today().addDays(-7);
        testInvoice.c2g__Account__c = testAccCust.Id;
        testInvoice.JVCO_Customer_Type__c = 'New Business';
        testInvoice.c2g__OwnerCompany__c = testCompany.id;
        testInvoice.ownerid = UserInfo.getUserId();
        testInvoice.JVCO_Invoice_Legacy_Number__c = 'SIN00001';
        insert testInvoice;

        JVCO_Invoice_Group__c grp = new JVCO_Invoice_Group__c();
        grp.JVCO_invoiceNumbers__c = testCompany.name;
        grp.JVCO_Total_Amount__c = 1;
        grp.JVCO_invNumbers__c = testInvoice.Name;
        grp.CC_SINs__c = testInvoice.Name + ',';
        insert grp;
        
        //jason.e.mactal 11.8.2017
        c2g__codaInvoiceInstallmentLineItem__c testInvInstallmentLI = new c2g__codaInvoiceInstallmentLineItem__c();
        testInvInstallmentLI.c2g__Invoice__c = testInvoice.id;
        testInvInstallmentLI.c2g__DueDate__c = date.today().addMonths(2);
        insert testInvInstallmentLI;

        c2g__codaCashMatchingSettings__c settings = new c2g__codaCashMatchingSettings__c();
        settings.JVCO_Write_off_Limit__c = 1.00;
        insert settings;

        Product2 testProduct = new Product2();
        testProduct.name = 'Test Product';
        testProduct.ProductCode = 'TP-001';
        testProduct.SBQQ__ChargeType__c = 'One-Time';
        testProduct.c2g__CODASalesRevenueAccount__c = testGeneralLedgerAcc.id;
        insert testProduct;

        PriceBook2 testPriceBook = new Pricebook2();
        testPriceBook.name = 'Standard Price Book';
        testPriceBook.isActive = true;
        insert testPriceBook;

        PriceBookEntry testPBE = new PriceBookEntry();
        testPBE.Pricebook2Id = test.getStandardPriceBookId();
        testPBE.product2id = testProduct.id;
        testPBE.isActive = true;
        testPBE.UnitPrice = 100.0;
        insert testPBE;

         List<c2g__codaInvoiceLineItem__c> testInvoiceLines = new List<c2g__codaInvoiceLineItem__c>{
         new c2g__codaInvoiceLineItem__c(
         c2g__Invoice__c = testInvoice.id,
         c2g__Product__c = testProduct.id
         )
        };
        insert testInvoiceLines;

        //Create Cash Entry
        c2g__codaCashEntry__c testCashEntry = new c2g__codaCashEntry__c();
        testCashEntry.c2g__PaymentMethod__c = 'Electronic';
        testCashEntry.c2g__OwnerCompany__c = testCompany.id;
        testCashEntry.ownerid = testGroup.Id;
        testCashEntry.c2g__Status__c = 'In Progress';
        testCashEntry.c2g__Date__c = Date.today();
        testCashEntry.c2g__Value__c = 0.00;
        insert testCashEntry;

        List<c2g__codaInvoice__c> salesInvoiceList = [SELECT Id, Name, c2g__Transaction__c from c2g__codaInvoice__c];

        //Create Cash Entry Line Item
        c2g__codaCashEntryLineItem__c cashEntryLineItem1 = new c2g__codaCashEntryLineItem__c();
        cashEntryLineItem1.c2g__CashEntry__c = testCashEntry.id;
        cashEntryLineItem1.c2g__Account__c = testAccCust.id;
        cashEntryLineItem1.c2g__CashEntryValue__c = 15000;
        cashEntryLineItem1.c2g__LineNumber__c = 1;
        cashEntryLineItem1.c2g__AccountReference__c = salesInvoiceList[0].Name;
        cashEntryLineItem1.c2g__AccountPaymentMethod__c = 'Electronic';
        cashEntryLineItem1.c2g__OwnerCompany__c = testCompany.id;
        cashEntryLineItem1.JVCO_Temp_AccountReference__c = 'SIN00001';
        insert cashEntryLineItem1;

        PAYACCVAL1__Bank_Account__c payBankAccount = new PAYACCVAL1__Bank_Account__c();
        payBankAccount.PAYACCVAL1__Bank_Name__c = 'test pay bank';
        payBankAccount.PAYFISH3__Originator__c = false;
        payBankAccount.PAYACCVAL1__Account_Number__c = '12345678';
        payBankAccount.PAYACCVAL1__Sort_Code__c = '123456';
        payBankAccount.PAYFISH3__Account_Name__c = 'test account name';
        insert payBankAccount;

        PAYREC2__Payment_Schedule__c paySchedule = new PAYREC2__Payment_Schedule__c();
        paySchedule.PAYFISH3__Bank_Account__c = payBankAccount.id;
        paySchedule.PAYREC2__Day__c = '1';
        paySchedule.PAYFISH3__First_Collection_Interval__c = 1;
        paySchedule.PAYREC2__Frequency__c = 'month';
        paySchedule.PAYFISH3__FTA_Sub_Type__c = 'Default';
        paySchedule.PAYFISH3__Fund_Transfer_Agent__c = 'Unknown';
        paySchedule.PAYREC2__Interval__c = 1;
        paySchedule.PAYREC2__Max__c = 10;
        paySchedule.PAYREC2__Type__c = 'Fixed';
        insert paySchedule;

        PAYREC2__Payment_Agreement__c paymentAgreement = new PAYREC2__Payment_Agreement__c();
        paymentAgreement.PAYREC2__Account__c = testAcc.Id;
        paymentAgreement.PAYFISH3__Current_Bank_Account__c = payBankAccount.id;
        paymentAgreement.PAYREC2__Ongoing_Collection_Amount__c = 100;
        paymentAgreement.PAYFISH3__Payee_Agreed__c = true;
        paymentAgreement.PAYREC2__Payment_Schedule__c = paySchedule.Id;
        insert paymentAgreement;
        
        PAYREC2__Payment_Agreement__c paymentAgreement2 = new PAYREC2__Payment_Agreement__c();
        paymentAgreement2.PAYREC2__Account__c = testAcc.Id;
        paymentAgreement2.PAYFISH3__Current_Bank_Account__c = payBankAccount.id;
        paymentAgreement2.PAYREC2__Ongoing_Collection_Amount__c = 100;
        paymentAgreement2.PAYFISH3__Payee_Agreed__c = true;
        paymentAgreement2.PAYREC2__Payment_Schedule__c = paySchedule.Id;
        insert paymentAgreement2;
        

        //Get record type of Paymnet
        List<RecordType> rtypes1 = [SELECT Name, Id FROM RecordType WHERE sObjectType='PAYBASE2__Payment__c' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> paymentRecordTypes = new Map<String,String>{};
        for(RecordType rt1: rtypes1)
        {
          paymentRecordTypes.put(rt1.Name,rt1.Id);
        }

        PAYBASE2__Payment__c paybasePayment = new PAYBASE2__Payment__c();
        paybasePayment.PAYREC2__Payment_Agreement__c = paymentAgreement.id;
        paybasePayment.PAYFISH3__Type__c = 'Collection';
        paybasePayment.RecordTypeId = paymentRecordTypes.get('Direct Debit Collection');
        paybasePayment.Name = 'Test Payment 01';
        paybasePayment.Ownerid = UserInfo.getUserId();
        paybasePayment.PAYBASE2__Is_Paid__c = true;
        paybasePayment.PAYBASE2__Amount_Refunded__c = 100;
        paybasePayment.PAYCP2__Is_Test__c = true;
        paybasePayment.PAYREC2__Is_Actual_Collection__c = true;
        paybasePayment.PAYSPCP1__Redirect_Parent__c= true;
        paybasePayment.PAYSPCP1__Gift_Aid__c= true;
        paybasePayment.PAYBASE2__Amount__c = 1250.00;
        paybasePayment.PAYCP2__Payment_Description__c = 'GIN-000044';
        paybasePayment.PAYBASE2__Status__c = 'Pending';
        paybasePayment.PAYBASE2__Status_Text__c = 'AUDDIS M'; 
        insert paybasePayment;

        //added by edmundCua
        cashEntryLineItem1.JVCO_Payonomy_Payment__c = paybasePayment.id;
        update cashEntryLineItem1;

        Test.startTest();

        //c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        //List<c2g.CODAAPICommon.Reference> refList = new List<c2g.CODAAPICommon.Reference>();
        //    Double totalAmount = 0.0;
        //    c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
        //    reference.Id = testInvoice.Id;
        //    refList.add(reference);
        // c2g.CODAAPISalesInvoice_10_0.BulkPostInvoice(context, refList);

    
        
        //added by edmundCua
        //c2g.CODAAPICommon_7_0.Context contextObj = new c2g.CODAAPICommon_7_0.Context();
        //    List<c2g.CODAAPICommon.Reference> referenceList = new List<c2g.CODAAPICommon.Reference>();   
        //    c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
        //    ref.Id = testCashEntry.id;
        //    referenceList.add(ref);
        //    c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(contextObj, referenceList);

        /*list<c2g__codaTransactionLineItem__c> testTranLine = new list<c2g__codaTransactionLineItem__c>();
        testTranLine = [select id,c2g__LineType__c from c2g__codaTransactionLineItem__c];
        system.assertEquals(null, testTranLine);
        system.assertNotEquals(null, testTranLine);*/

        /*Test.stopTest();

        list<c2g__codaTransactionLineItem__c> listTranLine = new list<c2g__codaTransactionLineItem__c>();
        listTranLine = [select id, c2g__MatchingStatus__c from c2g__codaTransactionLineItem__c];
        //system.assertEquals(null, listTranLine);

        //for(c2g__codaTransactionLineItem__c c2Line : listTranLine){
        //    c2Line.c2g__MatchingStatus__c = 'Matched';
        //}
       // update listTranLine;
    }
/*
    @isTest
    static void itShould()
    {
        Test.startTest();

         List<RecordType> rtypes1 = [SELECT Name, Id FROM RecordType WHERE sObjectType='PAYBASE2__Payment__c' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> paymentRecordTypes = new Map<String,String>{};
        for(RecordType rt1: rtypes1)
        {
          paymentRecordTypes.put(rt1.Name,rt1.Id);
        }
        PAYBASE2__Payment__c paybasePayment = [SELECT Id FROM PAYBASE2__Payment__c Limit 1];
        paybasePayment.PAYBASE2__Status__c = 'AUTHORISED';
        paybasePayment.RecordTypeId = paymentRecordTypes.get('SagePay');
        paybasePayment.PAYREC2__Payment_Agreement__c = null;
        paybasePayment.PAYBASE2__Created_From_Account__c = [SELECT Id FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1].id;//jason

        update paybasePayment;
        System.debug('@@@@@@after update paybasepayment'); //Mary 1/4/2018
        JVCO_PayonomyPaymentProcessingBatch payonomyPaymentBatch = new JVCO_PayonomyPaymentProcessingBatch('SagePay');
        Database.executeBatch(payonomyPaymentBatch);
        System.debug('@@@@@@after payonomyPaymentBatch'); //Mary 1/4/2018
        Test.stopTest();

    }
    @isTest
    /*static void FirstFailure()
    {
        List<RecordType> rtypes1 = [SELECT Name, Id FROM RecordType WHERE sObjectType='PAYBASE2__Payment__c' AND isActive=true];        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> paymentRecordTypes = new Map<String,String>{};
        for(RecordType rt1: rtypes1)
        {
          paymentRecordTypes.put(rt1.Name,rt1.Id);
        }
        PAYBASE2__Payment__c paybasePayment = [SELECT Id FROM PAYBASE2__Payment__c Limit 1];
        //paybasePayment.PAYBASE2__Status__c = 'AUTHORISED';
        paybasePayment.PAYBASE2__Status__c = 'Error';
        paybasePayment.PAYBASE2__Status_Text__c='Refer to Payer';
        paybasePayment.RecordTypeId = paymentRecordTypes.get('Direct Debit Collection');
        paybasePayment.PAYBASE2__Pay_Date__c = system.today();
        Test.startTest();
        update paybasePayment;
        Test.stopTest();
        PAYBASE2__Payment__c testPayment = [SELECT Id, PAYREC2__Payment_Agreement__r.Count_0_Fail__c FROM PAYBASE2__Payment__c where id=:paybasePayment.id Limit 1];
        System.assertEquals(testPayment.PAYREC2__Payment_Agreement__r.Count_0_Fail__c, 1);      
        
    }
    @isTest
    static void testLinkInvoiceGroupToSinv()
    {
        Account acct = [SELECT id, JVCO_Billing_Contact__c, JVCO_Billing_Contact__r.Email FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];
        JVCO_Invoice_Group__c invGrp = [SELECT Id, Name, JVCO_invoiceNumbers__c, JVCO_Total_Amount__c, CC_SINs__c, JVCO_invNumbers__c
                                FROM JVCO_Invoice_Group__c LIMIT 1];     
        Id rec = Schema.SObjectType.PAYBASE2__Payment__c .getRecordTypeInfosByName().get('SagePay').getRecordTypeId();
        
        PAYBASE2__Payment__c paybasePayment = new PAYBASE2__Payment__c();
        paybasePayment.PAYBASE2__Status__c = 'AUTHORISED';
        paybasePayment.PAYCP2__Payment_Description__c = invGrp.Name;
        paybasePayment.RecordTypeId = rec;
        paybasePayment.PAYBASE2__Pay_Date__c = system.today();
        paybasePayment.PAYBASE2__Created_From_Account__c = acct.id;
        Test.startTest();
        insert paybasePayment;
        Test.stopTest();
        PAYBASE2__Payment__c testPayment = [SELECT Id, PAYREC2__Payment_Agreement__r.Count_0_Fail__c FROM PAYBASE2__Payment__c where id=:paybasePayment.id Limit 1];
        //System.assertEquals(testPayment.PAYREC2__Payment_Agreement__r.Count_0_Fail__c, 1);      
        
    }
    @isTest
    static void testPreventPaymentDeletion()
    {
        Account acct = [SELECT id, JVCO_Billing_Contact__c, JVCO_Billing_Contact__r.Email FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];
        JVCO_Invoice_Group__c invGrp = [SELECT Id, Name, JVCO_invoiceNumbers__c, JVCO_Total_Amount__c, CC_SINs__c, JVCO_invNumbers__c
                                FROM JVCO_Invoice_Group__c LIMIT 1];     
        Id rec = Schema.SObjectType.PAYBASE2__Payment__c .getRecordTypeInfosByName().get('SagePay').getRecordTypeId();
        
        PAYBASE2__Payment__c paybasePayment = new PAYBASE2__Payment__c();
        paybasePayment.PAYBASE2__Status__c = 'AUTHORISED';
        paybasePayment.PAYCP2__Payment_Description__c = invGrp.Name;
        paybasePayment.RecordTypeId = rec;
        paybasePayment.PAYBASE2__Pay_Date__c = system.today();
        paybasePayment.PAYBASE2__Created_From_Account__c = acct.id;
        insert paybasePayment;
        Test.startTest();
        delete paybasePayment;
        Test.stopTest();  
    }*/
}