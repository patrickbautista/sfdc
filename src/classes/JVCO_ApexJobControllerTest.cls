/*
 * Author: Accenture Inc.
 * Date Created: 25 June 2018
 * Description: Test Class of JVCO_ApexJobController 
 *
 *
*/
@isTest
public class JVCO_ApexJobControllerTest{

    @testSetup
    public static void testData(){

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPricebook();
        insert pb;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;

        Opportunity o = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        insert o;      

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, o.id, null, 'Annually');
        insert q;
        
        Contract contr = JVCO_TestClassObjectBuilder.createContract(acc2.id, q.id);
        insert contr;       
    }

    static testMethod void test_JVCO_ApexJobControllerContract(){
        Contract c = [select id, SBQQ__RenewalQuoted__c from Contract order by CreatedDate DESC limit 1];
        Opportunity o = [select id from Opportunity order by CreatedDate DESC LIMIT 1];
        PageReference dmpRef;

        Test.startTest();

        Test.setCurrentPage(Page.JVCO_GenerateRenewalQuote);
        JVCO_GenerateRenewalQuoteExtension ext = new JVCO_GenerateRenewalQuoteExtension(new ApexPages.StandardController(c));
        JVCO_ApexJobController extContract = new JVCO_ApexJobController(new ApexPages.StandardController(c));
        ext.renewalUpdate();
        ext.renewalUpdate();
        dmpRef = ext.quoteCreated();

        Test.stopTest();

        Contract cCheck = [select SBQQ__RenewalQuoted__c from Contract where id = :c.Id order by CreatedDate DESC limit 1];

        system.assert(cCheck.SBQQ__RenewalQuoted__c);
        
        //Test Coverage for ApexJobController.retrieveJobs
        JVCO_ApexJobController.retrieveJobs(new List<Contract>{c});
    }

    static testMethod void test_JVCO_ApexJobControllerOpportunity(){
        Opportunity o = [select id from Opportunity order by CreatedDate DESC LIMIT 1];
        PageReference dmpRef;

        Test.startTest();

        Test.setCurrentPage(Page.JVCO_GenerateRenewalQuote);
        JVCO_ApexJobController extContract = new JVCO_ApexJobController(new ApexPages.StandardController(o));
        JVCO_ApexJobController.retrieveJobs();
        Test.stopTest();
    }
    
    static testMethod void testErrorException(){

       
        Contract contr2 = new Contract();
        PageReference dmpRef;

        try {

            Test.startTest();

            Test.setCurrentPage(Page.JVCO_GenerateRenewalQuote);
            JVCO_GenerateRenewalQuoteExtension ext = new JVCO_GenerateRenewalQuoteExtension(new ApexPages.StandardController(contr2));
            
            ext.renewalUpdate();
            dmpRef = ext.quoteCreated();

            Test.stopTest();

        }
        catch (Exception ex)
        {
              System.assertEquals(true, ApexPages.hasMessages(ApexPages.SEVERITY.WARNING), 'No Error');
        }
    }
}