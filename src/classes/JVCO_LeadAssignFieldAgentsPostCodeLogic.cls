/* ----------------------------------------------------------------------------------------------
   Name: JVCO_LeadAssignFieldAgentsPostCodeLogic.cls 
   Description: Business logic class for updating Ownership of a lead record before insert or before update

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  06-Sep-2016   0.1         mark.glenn.b.ayson  Intial creation
  ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_LeadAssignFieldAgentsPostCodeLogic 
{
    /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: Function that updates the owner ID based on the postcode from the lead record
    Inputs: List<Lead>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    06-Sep-2016 mark.glenn.b.ayson  Initial version of function
    27-Sep-2016 mark.glenn.b.ayson  Added wrapper class
    29-Sep-2016 mark.glenn.b.ayson  Updated the logic for the postcode
    18-Jan-2017 joseph.g.barrameda  Updated the logic where it checks only the 2 first characters of the postcode and ignoring numeral
    20-Oct-2017 reymark.j.l.arlos    Updated to make sure the lead owner will not change
  ----------------------------------------------------------------------------------------------- */
    public static void updateLeadOwnerID(List<Lead> leads, Boolean isUpdate)
    {
        List<Lead> updatedLeads = new List<Lead>();
        Boolean bHasMatch = false;
        Map<Id, String> leadIdAndPostCodeMap = new Map<Id, String>();
        Map<String, JVCO_AssignFieldAgentWrapper> postCodeAndRegionMap = new Map<String, JVCO_AssignFieldAgentWrapper>();
        JVCO_AssignFieldAgentWrapper retrievedRecord = new JVCO_AssignFieldAgentWrapper();
        String recordPostCode = '';
        Map<Id, Id> leadTaskOwnerId = new Map<Id, Id>(); //20-Oct-2017 reymark.j.l.arlos

        // Get the Venue ID and PostCode
        leadIdAndPostCodeMap = createLeadIdAndPostCodeMap(leads);
        // Get the region and postcode and save it to a map
        postCodeAndRegionMap = JVCO_AssignFieldAgentsPostCodeUtil.getFieldSalesDetails(leadIdAndPostCodeMap);

        // Get the Lead from the trigger
        for(Lead leadRecord : leads)
        {
            // Get the first string in the postcode 
            recordPostCode = JVCO_AssignFieldAgentsPostCodeUtil.removeNumeric(leadRecord.JVCO_Venue_Postcode__c.substring(0, 2).trim());
            
            for(String postCode : postCodeAndRegionMap.keySet())
            {
                // Get the retrieved postcode after the split
                if(postcode.equals(recordPostCode)) 
                {
                    // There is a match
                    bHasMatch = true;
                    // Save the details to a wrapper class
                    retrievedRecord = postCodeAndRegionMap.get(postCode);

                    // Exit the loop if a record is saved to the wrapper
                    if(!String.isBlank(retrievedRecord.postCodeLabel))
                    {
                        break;
                    }
                }
                /***** Commented by joseph.g.barrameda - 01/18/2017
                else if(postCode.equals(JVCO_AssignFieldAgentsPostCodeUtil.removeNumeric(recordPostCode)))
                {
                    // There is a match
                    bHasMatch = true;
                    // Save the details to a wrapper class
                    retrievedRecord = postCodeAndRegionMap.get(postCode);

                    // Exit the loop if a record is saved to the wrapper
                    if(!String.isBlank(retrievedRecord.postCodeLabel))
                    {
                        break;
                    }
                }
                *****/
            }

            if(bHasMatch)
            {
                // Update the lead OwnerID
                leadTaskOwnerId.put(leadRecord.Id, retrievedRecord.userName);//20-Oct-2017 reymark.j.l.arlos
                // Update the lead region
                if(Trigger.isBefore){leadRecord.JVCO_Region__c = retrievedRecord.postCodeLabel;}
            }
            else
            {
                if(Trigger.isBefore){leadRecord.ownerId =  JVCO_AssignFieldAgentsPostCodeUtil.getQueueID();}
            }

            updatedLeads.add(leadRecord);
        }

        if(isUpdate && updatedLeads.size() > 0)
        {
            // Create a new Task record
            //createTaskRecord(updatedLeads, leadTaskOwnerId);//20-Oct-2017 reymark.j.l.arlos
        }
    }

    /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: Create a map that contains the lead ID and postcode
                Map<LeadId, Postcode>
    Inputs: List<Lead>
    Returns: Map<Id, String>
    <Date>      <Authors Name>      <Brief Description of Change> 
    14-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public static Map<Id, String> createLeadIdAndPostCodeMap(List<Lead> leadList)
    {
        Map<Id, String> leadIdAndPostCodeMap = new Map<Id, String>(); 

        // Get the records from the list and save it to a map
        for(Lead leadRecord : leadList)
        {
            if(!String.isBlank(leadRecord.JVCO_Venue_Postcode__c) && leadRecord.JVCO_Field_Visit_Requested__c == true)
            {
                leadIdAndPostCodeMap.put(leadRecord.Id, leadRecord.JVCO_Venue_Postcode__c);
            }
        }

        return leadIdAndPostCodeMap;
    }

    /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: Function that saves the lead record to a list and call the createNewTask method in the util
    Inputs: JVCO_Venue__c, Boolean
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    07-Sep-2016 mark.glenn.b.ayson  Initial version of function
    20-Oct-2017 reymark.j.l.arlos   Updated to make sure the lead owner will not change
    28-Nov-2019   robert.j.b.lacatan  Removed this method as per Luker as it is no longer needed.
  ----------------------------------------------------------------------------------------------- */
    /*private static void createTaskRecord(List<Lead> leadList, Map<Id, Id> taskOwnerId)
    {
        List<JVCO_AssignFieldAgentWrapper> taskRecordList = new List<JVCO_AssignFieldAgentWrapper>();
        JVCO_AssignFieldAgentWrapper wrapper = new JVCO_AssignFieldAgentWrapper();      

        for(Lead leadRecord : leadList)
        {
            wrapper = new JVCO_AssignFieldAgentWrapper(); 
            // Add the venue name
            wrapper.venueName = leadRecord.Company;

            // Check if the newOwnerID is not a queue, add the new owner ID else, add the current logged-in user
            //start 20-Oct-2017 reymark.j.l.arlos
            if(taskOwnerId.containsKey(leadRecord.Id))
            {
                if(taskOwnerId.get(leadRecord.Id) != JVCO_AssignFieldAgentsPostCodeUtil.queueID)
                {
                    wrapper.ownerID = taskOwnerId.get(leadRecord.Id);
                }
                else
                {
                    wrapper.ownerID = UserInfo.getUserId();
                }
            }
            //end 20-Oct-2017 reymark.j.l.arlos

            // Add the record ID
            wrapper.recordID = leadRecord.Id;

            // Add to list
            taskRecordList.add(wrapper);
        }
        
        if(taskRecordList.size() > 0)
        {
            // Call the create task methid in the util class (List<String>, boolean value that will check if the object is a lead)
            JVCO_AssignFieldAgentsPostCodeUtil.createNewTask(taskRecordList, true);
        }
    }*/
}