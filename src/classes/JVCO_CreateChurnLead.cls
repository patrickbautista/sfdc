/* --------------------------------------------------------------------------------------
Name: JVCO_CreateChurnLead
Description: Batch Class to create churn leads from closed affiliations

Date            Version     Author              Summary of Changes 
-----------     -------     -----------------   -----------------------------------------
08-Jan-2021     0.1         Luke.Walker         Intial creation
----------------------------------------------------------------------------------------- */

public class JVCO_CreateChurnLead implements Database.Batchable<sObject>, Database.Stateful{

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT ID, 
                                         JVCO_Closure_Reason__c,
                                         JVCO_Venue__c, 
										 JVCO_Venue__r.JVCO_City__c, 
                                         JVCO_Venue__r.JVCO_Country__c,
                                         JVCO_Venue__r.JVCO_County__c,
                                         JVCO_Venue__r.JVCO_Postcode__c,
                                         JVCO_Venue__r.JVCO_Street__c, 
                                         JVCO_Venue__r.JVCO_Venue_Sub_Type__c,
                                         JVCO_Venue__r.JVCO_Venue_Type__c,
                                         JVCO_Venue__r.Name,
                                         JVCO_Venue__r.Number_of_Active_Affiliations__c,
                                         JVCO_Account__r.JVCO_LiveCategory__c,
                                         JVCO_Account__r.JVCO_Review_Contact__r.Email, 
                                         JVCO_Account__r.JVCO_Review_Contact__r.FirstName, 
                                         JVCO_Account__r.JVCO_Review_Contact__r.LastName, 
                                         JVCO_Account__r.JVCO_Review_Contact__r.MobilePhone, 
                                         JVCO_Account__r.JVCO_Review_Contact__r.Phone, 
                                         JVCO_Account__r.Type
                                         FROM JVCO_Affiliation__c 
                                         WHERE JVCO_Create_Churn_Lead__c = TRUE]);
    }
    
    public void execute(Database.BatchableContext bc, List<JVCO_Affiliation__c> scope){
		Map<id, JVCO_Affiliation__c> churnRequired = new Map<id,JVCO_Affiliation__c>();
        Map<id, double> potentialAmount = new Map<id,double>(); 
        List<JVCO_Affiliation__c> churnNotRequired = new List<JVCO_Affiliation__c>();
        List<Lead> churnLeads = new List<Lead>();  
        Group defaultQueue = [SELECT Id FROM Group WHERE Type = 'Queue' AND NAME = 'Default Queue' LIMIT 1];
		
        //Create mapping for subscription value
        for(SBQQ__Subscription__c subs : [SELECT ID, JVCO_NetTotal__c, affiliation__c FROM SBQQ__Subscription__c WHERE Affiliation__c IN :scope AND JVCO_NetTotal__c  != 0 AND Start_Date__c = LAST_N_DAYS:365]){
            if(!potentialAmount.containsKey(subs.affiliation__c)){
                potentialAmount.put(subs.affiliation__c,subs.JVCO_NetTotal__c);
            } else {
                Double value = potentialAmount.get(subs.affiliation__c) + subs.JVCO_NetTotal__c;
                potentialAmount.put(subs.affiliation__c,value);
            }
        }
        
        for(JVCO_Affiliation__c aff : scope){
            if(!churnRequired.containsKey(aff.JVCO_Venue__c) && aff.JVCO_Venue__r.Number_of_Active_Affiliations__c == 0){
                //setup new Churn Lead
                Lead churnLead = new Lead();
                churnLead.Company = aff.JVCO_Venue__r.Name;
                churnLead.JVCO_Closure_Reason__c = aff.JVCO_Closure_Reason__c;
                churnLead.JVCO_Venue_City__c = aff.JVCO_Venue__r.JVCO_City__c;
                churnLead.JVCO_Venue_County__c = aff.JVCO_Venue__r.JVCO_County__c;
                churnLead.JVCO_Venue_Postcode__c = aff.JVCO_Venue__r.JVCO_Postcode__c;
                churnLead.JVCO_Venue_Street__c = aff.JVCO_Venue__r.JVCO_Street__c;
                churnLead.JVCO_Venue_Sub_Type__c = aff.JVCO_Venue__r.JVCO_Venue_Sub_Type__c;
                churnLead.JVCO_Venue_Type__c = aff.JVCO_Venue__r.JVCO_Venue_Type__c;
                churnLead.LeadSource = 'Churn';
                churnLead.OwnerID = defaultQueue.id;
                churnLead.JVCO_Potential_Amount__c = potentialAmount.containsKey(aff.id) ? potentialAmount.get(aff.id) : 0;
                
                //if Live Category not empty then classify account as a Live account
                if(aff.JVCO_Account__r.JVCO_LiveCategory__c != NULL){
                    churnLead.Previous_Account_Type__c = 'Live';
                } else {
                    churnLead.Previous_Account_Type__c = aff.JVCO_Account__r.Type ;
                }
                
                //Check if lapser. If lapser then we can reuse the contact information
                if(aff.JVCO_Closure_Reason__c == '14 Day Music Discontinued' || aff.JVCO_Closure_Reason__c == 'Music not subject to our control' || aff.JVCO_Closure_Reason__c == 'Stopped playing music'){
                    churnLead.FirstName = aff.JVCO_Account__r.JVCO_Review_Contact__r.FirstName;
                    churnLead.LastName = aff.JVCO_Account__r.JVCO_Review_Contact__r.LastName;
                    churnLead.Phone = aff.JVCO_Account__r.JVCO_Review_Contact__r.Phone;
                    churnLead.Email = aff.JVCO_Account__r.JVCO_Review_Contact__r.Email;
                } else {
                    churnLead.LastName = 'Unknown';
                    //Check if phone is landline because we can reuse it (starts 01, 02 or 03)
                    if(aff.JVCO_Account__r.JVCO_Review_Contact__r.Phone != NULL){
                        system.debug('Phone: '+aff.JVCO_Account__r.JVCO_Review_Contact__r.Phone);
                        system.debug('starts with 01'+aff.JVCO_Account__r.JVCO_Review_Contact__r.Phone.startsWith('01'));
                        if(aff.JVCO_Account__r.JVCO_Review_Contact__r.Phone.startsWith('01') || aff.JVCO_Account__r.JVCO_Review_Contact__r.Phone.startsWith('02') || aff.JVCO_Account__r.JVCO_Review_Contact__r.Phone.startsWith('03')){
                            churnLead.Phone = aff.JVCO_Account__r.JVCO_Review_Contact__r.Phone;
                        }
                    }
                }
                
                aff.JVCO_Churn_Created__c = true;
                churnRequired.put(aff.JVCO_Venue__c, aff);
                churnLeads.add(churnLead);
            } else {
                aff.JVCO_Churn_Not_Required__c = true;
                churnNotRequired.add(aff);
            }
        }
        
        if(!churnNotRequired.isEmpty())
        {
            try{
                update churnNotRequired;
            }
            catch(Exception e){
                insert createErrorLog('Update Not Required Affiliations', e.getMessage(), 'JVCO_CreateChurnLead');
            }
        }
        
        if(!churnLeads.isEmpty())
        {
            try{
                insert churnLeads;
            }
            catch(Exception e){
                insert createErrorLog('Create Churn Leads', e.getMessage(), 'JVCO_CreateChurnLead');
            }
        }
        
        if(!churnRequired.isEmpty())
        {
            try{
                update churnRequired.values();
            }
            catch(Exception e){
                insert createErrorLog('Update Required Affiliations', e.getMessage(), 'JVCO_CreateChurnLead');
            }
        }
    }

    @TestVisible
    private ffps_custRem__Custom_Log__c createErrorLog(String errCode, String errMessage, String batchName)
    {
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Grouping__c = errCode;
        errLog.ffps_custRem__Detail__c = errMessage;
        errLog.ffps_custRem__Message__c = batchName;
        return errLog;
    }    
    public void finish(Database.BatchableContext bc){}    
}