@isTest
private class JVCO_codaTransactionHandlerTest
{
    
    @testSetup static void createTestData() 
    {
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        //JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        Test.startTest();
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        Set<Id> orderGroupIdSet = new Set<Id>();  
        orderGroupIdSet.add(orderGroup.Id);

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_TestClassHelper.setGeneralSettingsCS();
        insert new GLA_for_Cash_Transfer__c(name = '10100 - Bank Cash'); 
        //JVCO_BackgroundMatchingLogic.stopMatching = true;
        JVCO_FFUtil.stopCashTransferLogic = true;
        
        Database.executeBatch(new JVCO_OrderMultipleBillNowLogicBatch(orderGroupIdSet));
        
        Test.stopTest();
        
        Income_Direct_Debit__c incomeDD = new Income_Direct_Debit__c();
        incomeDD.Account__c = licAcc.Id;
        incomeDD.DD_Collection_Day__c = '1';
        incomeDD.DD_Collection_Period__c = 'Monthly';
        incomeDD.DD_Collection_Stretch__c = '1';
        incomeDD.DD_Collection_Type__c = 'Fixed';
        incomeDD.DD_Collection_Reference__c = 'Monthly Subscription';
        incomeDD.DD_Status__c = 'New Instruction';
        incomeDD.DD_Bank_Account_Number__c = '12345678';
        incomeDD.DD_Bank_Sort_Code__c = '123456';
        incomeDD.Number_Of_Payments__c = 1;
        insert incomeDD;
        
        JVCO_Invoice_Group__c invoiceGroup = new JVCO_Invoice_Group__c();
        invoiceGroup.JVCO_Total_Amount__c = 100;
        insert invoiceGroup;
            
        SMP_DirectDebit_GroupInvoice__c IDDInvGroup = new SMP_DirectDebit_GroupInvoice__c();
        IDDInvGroup.Income_Direct_Debit__c = incomeDD.Id;
        IDDInvGroup.Invoice_Group__c = invoiceGroup.Id;
        insert IDDInvGroup;
        c2g__codaInvoice__c invoice = [SELECT Id, Name,c2g__PaymentStatus__c,c2g__OutstandingValue__c,c2g__InvoiceTotal__c,c2g__NetTotal__c,
                                       JVCO_DocGen_Number_of_Invoice_Lines__c
                                       FROM c2g__codaInvoice__c LIMIT 1];
        invoice.JVCO_Invoice_Group__c = invoiceGroup.Id;
        update invoice;
        
        c2g__codaInvoiceInstallmentLineItem__c invoiceInstalmentLine = new c2g__codaInvoiceInstallmentLineItem__c();
        invoiceInstalmentLine.c2g__Amount__c = 12;
        invoiceInstalmentLine.c2g__DueDate__c = Date.today();
        invoiceInstalmentLine.JVCO_Paid_Amount__c = 12;
        invoiceInstalmentLine.JVCO_Payment_Status__c = 'Paid';
        invoiceInstalmentLine.c2g__Invoice__c = invoice.id;
        insert invoiceInstalmentLine;
        
        JVCO_CC_Outstanding_Mapping__c CCInvoiceGrp = new JVCO_CC_Outstanding_Mapping__c();
        CCInvoiceGrp.JVCO_Sales_Invoice_Name__c = invoice.Name;
        CCInvoiceGrp.JVCO_CC_Invoice_Group__c = invoiceGroup.Id;
        CCInvoiceGrp.JVCO_CC_Outstanding_Value__c = 12;
        insert CCInvoiceGrp;
        
        Income_Debit_History__c incomeDDHistory = new Income_Debit_History__c();
        incomeDDHistory.Amount__c = 100;
        incomeDDHistory.DD_Status__c = 'First Collection';
        incomeDDHistory.Income_Direct_Debit__c = incomeDD.Id;
        incomeDDHistory.DD_Stage__c = 'Submitted';
        incomeDDHistory.DD_Collection_Date__c = Date.today();
        insert incomeDDHistory;
    }
    
    @isTest
    static void matchInvoiceToUpdateIDD(){
        
        c2g__codaCompany__c testCompany = [SELECT id FROM c2g__codaCompany__c LIMIT 1];
        Account licAcc = [SELECT id FROM Account WHERE RecordType.Name = 'Licence Account'];
        c2g__codaInvoice__c invoice = [SELECT Id, Name, c2g__Transaction__c, c2g__OutstandingValue__c from c2g__codaInvoice__c];
        Group testGroup = [SELECT id FROM Group LIMIT 1];
        
        test.startTest();
        Set<Id> cashEntrySet = new Set<Id>();
        //Create Cash Entry
        c2g__codaCashEntry__c testCashEntry = new c2g__codaCashEntry__c();
        testCashEntry.c2g__PaymentMethod__c = 'Electronic';
        testCashEntry.c2g__OwnerCompany__c = testCompany.id;
        testCashEntry.ownerid = testGroup.Id;
        testCashEntry.c2g__Status__c = 'In Progress';
        testCashEntry.c2g__Date__c = Date.today();
        testCashEntry.c2g__Value__c = 0.00;
        insert testCashEntry;
        
        //Create Cash Entry Line Item
        c2g__codaCashEntryLineItem__c cashEntryLineItem1 = new c2g__codaCashEntryLineItem__c();
        cashEntryLineItem1.c2g__CashEntry__c = testCashEntry.id;
        cashEntryLineItem1.c2g__Account__c = licAcc.id;
        cashEntryLineItem1.c2g__CashEntryValue__c = invoice.c2g__OutstandingValue__c;
        cashEntryLineItem1.c2g__LineNumber__c = 1;
        cashEntryLineItem1.c2g__AccountReference__c = invoice.Name;
        cashEntryLineItem1.c2g__AccountPaymentMethod__c = 'Electronic';
        cashEntryLineItem1.c2g__OwnerCompany__c = testCompany.id;
        cashEntryLineItem1.JVCO_Temp_AccountReference__c = 'SIN00001';
        insert cashEntryLineItem1;
        cashEntrySet.add(testCashEntry.Id);
        test.stopTest();
        Database.executeBatch(new JVCO_CashEntriesPostingLogicBatch(cashEntrySet, true));
    }
}