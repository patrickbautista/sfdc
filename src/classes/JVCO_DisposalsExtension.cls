public class JVCO_DisposalsExtension 
{  
    private JVCO_VenueTransfer venueTransfer;
    public String searchFieldValue { get; set; }
    
    public JVCO_Affiliation__c overarchingValuesBinder { get; set; }
    public Account accountRecord { get; set; }

    public String venueSelectorIdFromPage { get; set; }
    public String venueSelectedIdFromPage { get; set; }
    public String venueSelectedMessageLimit { get; set; }

    public Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferCandidatesMap { get; set; }
    public Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsMap { get; set; }
    public Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsDisplayMap { get; set; }
    
    //public Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsToPassMap { get; set; }
    public Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsCheckerMap { get; set; }
    //Start LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
    public Map<String, Date> mapTempIdJVCOAffiliationDate {get; set;}
    public Map<String, String> mapTempIdJVCOAffiliationCloseReason{get; set;}
    public Map<String, String> mapTempIdJVCOAffiliationAccId {get; set;}
    //End LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
    public String overarchingAffiliationValues { get; set; }
    public String affiliationToBeRemovedFromTs { get; set; }
    public List<Contract> contractsToAmendList { get; set; }
    public Boolean hasAmendments { get; set; }
    public Boolean isAmended { get; set; }
    public Boolean hasAffiliations { get; set; }
    public Set<String> licenceAccountIdSet { get; set; }
    public String defaultLicenceAccount { get; set; }

    public Integer stepNumber {get; set;}
    public Integer loopCtr {get; set;}
    public Integer processSize { get; set; }
    public Integer venueProgress { get; set; }
    public Integer venueSize { get; set; }
    public Integer venuePrprcnt { get; set; }

    //07-09-19 mel.andrei.b.santos GREEN-34760 
    public Boolean processQueueable = (Boolean)JVCO_General_Settings__c.getOrgDefaults().JVCO_Disposal_Queueuable__c; 


    public List<JVCO_VenueTransfer.FieldSetWrapper> overarchingFieldNamesList {
        get {
            return venueTransfer.convertToFieldSetWrapperList(venueTransfer.getAffiliationDisposalsDefaultsFields());
        }
    }

    public List<JVCO_VenueTransfer.FieldSetWrapper> selectedVenuesFieldNamesList {
        get {
            return venueTransfer.convertToFieldSetWrapperList(venueTransfer.getVenueDisposalsSelectedFields());
        }
    }

    public List<JVCO_VenueTransfer.FieldSetWrapper> selectorVenuesFieldNamesList {
        get {
            return venueTransfer.convertToFieldSetWrapperList(venueTransfer.getVenueDisposalsSelectorFields());
        }
    }
    
    public List<JVCO_VenueTransfer.FieldSetWrapper> additionalFieldsList {
        get {
            return venueTransfer.convertToFieldSetWrapperList(venueTransfer.getAffiliationAdditionalFields());
        }
    }
    
    public JVCO_DisposalsExtension (ApexPages.StandardController stdController) {

        accountRecord = (Account)stdController.getRecord();
        accountRecord = [select Id, Name, Licence_Account_Number__c, JVCO_VenDisposalQueueFlag__c from Account where Id = :accountRecord.Id];
        overarchingValuesBinder = new JVCO_Affiliation__c();
        venueTransfer = new JVCO_VenueTransfer();
        transferCandidatesMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
        transferSelectionsMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
        transferSelectionsDisplayMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
        //transferSelectionsToPassMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
        //Start LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
        mapTempIdJVCOAffiliationDate = new Map<String,Date>();
        mapTempIdJVCOAffiliationCloseReason = new Map<String,String>();
        mapTempIdJVCOAffiliationAccId = new Map<String,String>();
        //End LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
        contractsToAmendList = new List<Contract>();
        hasAmendments = false;
        isAmended = false;
        hasAffiliations = false;
        licenceAccountIdSet = new Set<String>();
        defaultLicenceAccount = '';

        stepNumber = 1;
        loopCtr = 0;
        venueProgress = 0;
        venueSize = 0;
        venuePrprcnt = 0;
        if((Integer)JVCO_Constants__c.getOrgDefaults().JVCO_Disposals_Process_Size__c != null) {
            processSize = (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_Disposals_Process_Size__c;
        } else {
            processSize = 5;
        }

        // START    raus.k.b.ablaza     08/31/2017      GREEN-22460 Populate selection list upon loading
        PageReference dmpVal = search();
        // END    raus.k.b.ablaza     08/31/2017      GREEN-22460 Populate selection list upon loading
    }

    public PageReference receiveSelectorValues() {

        for(String affiliationId : (List<String>)System.JSON.deserialize(venueSelectorIdFromPage, List<String>.class)) {
            transferSelectionsMap.put(affiliationId, transferCandidatesMap.get(affiliationId));
            transferCandidatesMap.remove(affiliationId);
        }

        return null;
    }

    public PageReference receiveSelectedValues() {

        for(String affiliationId : (List<String>)System.JSON.deserialize(venueSelectedIdFromPage, List<String>.class)) {
            transferCandidatesMap.put(affiliationId, transferSelectionsMap.get(affiliationId));
            transferSelectionsMap.remove(affiliationId);
        }

        return null;
    }

    public PageReference search() {
        
        // account: '0015E00000ByfvFQAR' : 'Hilton International Hotels UK Ltd'
        // venue
        //  by name: Hilton*        Hilton Bracknell
        //  by street: *each B*     Beach Boulevard   ---> doesn't work. 
        //  by city: *MOUTH         BOURNEMOUTH       ---> doesn't work. 
        // anoother hilton example: 0015E00000ByfvKQAR
        Set<String> filterIdSet = new Set<String>();
        /*if(transferSelectionsMap.size() > 0) {
            filterIdSet.addAll(transferSelectionsMap.keySet());
        }
        
        if(transferSelectionsDisplayMap.size() > 0) {
            filterIdSet.addAll(transferSelectionsDisplayMap.keySet());
        }*/
        
        try {
            transferCandidatesMap.clear();
            Map<String, JVCO_VenueTransfer.JVCO_TransferObject> tempMapHolder = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
            Set<Id> venuesSelected = new Set<Id>();
            
            if(!transferSelectionsDisplayMap.isEmpty()) {
                for(JVCO_VenueTransfer.JVCO_TransferObject tsdmVal : transferSelectionsDisplayMap.values()) {
                    venuesSelected.add(tsdmVal.venueRecord.Id);
                }
            }
            
            if(!transferSelectionsMap.isEmpty()) {
                for(JVCO_VenueTransfer.JVCO_TransferObject tsdmVal : transferSelectionsMap.values()) {
                    venuesSelected.add(tsdmVal.venueRecord.Id);
                }
            }
            
            tempMapHolder.putAll(venueTransfer.searchAffiliations(JVCO_VenueTransfer.TransferType.DISPOSAL, searchFieldValue, accountRecord.Id, filterIdSet, false));
            if(!tempMapHolder.isEmpty()) {
                for(String tempMapKey : tempMapHolder.keySet()) {
                    JVCO_VenueTransfer.JVCO_TransferObject tempMapVal = tempMapHolder.get(tempMapKey);
                    if(!venuesSelected.contains(tempMapVal.venueRecord.Id) && !venuesSelected.contains(tempMapVal.venueRecord.Id)) {
                        transferCandidatesMap.put(tempMapKey, tempMapVal);
                    }
                }
            }

            // START    raus.k.b.ablaza     13/09/2017      GREEN-22850     Added limit for venue result
            if(tempMapHolder.size() >= 200){
                venueSelectedMessageLimit = 'More Venues are available, please use the search functionality to find these additional venues.';
            }
            else{
                venueSelectedMessageLimit = '';
            }

            // END    raus.k.b.ablaza     13/09/2017      GREEN-22850     Added limit for venue result


            //transferCandidatesMap.putAll(venueTransfer.searchAffiliations(JVCO_VenueTransfer.TransferType.DISPOSAL, searchFieldValue, accountRecord.Id, filterIdSet, false));
        } catch (Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }

        return null;
    }

    public PageReference removeFromTransferSelections() {
        transferSelectionsDisplayMap.remove(affiliationToBeRemovedFromTs);
        if(transferSelectionsDisplayMap.size() < 1) {
            hasAffiliations = false;
        }
        
        return null;
    }

    public PageReference addToList() {

        try {
            transferSelectionsDisplayMap.putAll(venueTransfer.applyOverarchingValuesToTransferSelections(JVCO_VenueTransfer.TransferType.DISPOSAL, transferSelectionsMap, overarchingValuesBinder, defaultLicenceAccount));
            transferSelectionsMap.clear();
            if(transferSelectionsDisplayMap.size() > 0) {
                hasAffiliations = true;
            }
        } catch (Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
        
        return null;

    }
    public PageReference confirmDisposals(){
        Boolean noErrors = true;
        licenceAccountIdSet = new Set<String>();
        venueSize = transferSelectionsDisplayMap.size();
        updateVenueProgress(); 
        for(JVCO_VenueTransfer.JVCO_TransferObject toWrap : transferSelectionsDisplayMap.values()) {
            if(toWrap.accountId != null && toWrap.accountId != '') {
                String licenceAccountNumber = toWrap.accountId;
                licenceAccountNumber = licenceAccountNumber.trim();

                if(!licenceAccountNumber.contains('LIC-')) { //GREEN-26390 jules.osberg.a.pablo add LIC- to account numbers if it doesn't contain it
                    licenceAccountNumber = 'LIC-' + licenceAccountNumber;
                }
                
                licenceAccountIdSet.add(licenceAccountNumber);
            }
        }

        if(!licenceAccountIdSet.isEmpty()) {
            noErrors = isValidLicenceAccountId(licenceAccountIdSet);
        }
        
        if(noErrors) {
            stepNumber = 2;
        }  
        
        return null;
    }

    public PageReference confirmDisposals2() {
        PageReference pr = null;
        Boolean retDisposals = true;
        licenceAccountIdSet.clear();
        Savepoint sp = Database.setSavepoint();
        
        if(!processQueueable)
        {   
            updateVenueProgress(); //000
                
            transferSelectionsCheckerMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
    
            //Start LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
            if(!mapTempIdJVCOAffiliationDate.isEmpty()) {
                for(String kAffli: mapTempIdJVCOAffiliationDate.keySet()){
                    transferSelectionsDisplayMap.get(kAffli).affiliationRecord.JVCO_End_Date__c = Date.valueOf(mapTempIdJVCOAffiliationDate.get(kAffli));
                }
            }
    
            if(!mapTempIdJVCOAffiliationCloseReason.isEmpty()) {
                for(String kAffli: mapTempIdJVCOAffiliationCloseReason.keySet()){
                    transferSelectionsDisplayMap.get(kAffli).affiliationRecord.JVCO_Closure_Reason__c = String.valueOf(mapTempIdJVCOAffiliationCloseReason.get(kAffli));
                }
            }
    
            if(!mapTempIdJVCOAffiliationAccId.isEmpty()) {
                for(String kAffli: mapTempIdJVCOAffiliationAccId.keySet()){
                    transferSelectionsDisplayMap.get(kAffli).accountId = String.valueOf(mapTempIdJVCOAffiliationAccId.get(kAffli));
                }
            }
            //End LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
    
            if(transferSelectionsDisplayMap.size() > 0) {
                // START jules.osberg.a.pablo     23/10/2017      GREEN-25128
                Integer ctr2 = 0;
    
                for(String tsdmKey : transferSelectionsDisplayMap.keySet()) {
                    if(ctr2 < processSize){
                        transferSelectionsCheckerMap.put(tsdmKey, transferSelectionsDisplayMap.remove(tsdmKey));
                        ctr2++;
                    }
                }
                //Start LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
                mapTempIdJVCOAffiliationDate.clear();
                if(transferSelectionsDisplayMap.size() > 0) {  
                    for(String affKey: transferSelectionsDisplayMap.keySet()){
                        mapTempIdJVCOAffiliationDate.put(affKey,Date.valueOf(transferSelectionsDisplayMap.get(affKey).affiliationRecord.JVCO_End_Date__c));
                    }
                }
    
                mapTempIdJVCOAffiliationCloseReason.clear();
                if(transferSelectionsDisplayMap.size() > 0) {  
                    for(String affKey: transferSelectionsDisplayMap.keySet()){
                        mapTempIdJVCOAffiliationCloseReason.put(affKey,String.valueOf(transferSelectionsDisplayMap.get(affKey).affiliationRecord.JVCO_Closure_Reason__c));
                    }
                }
    
                mapTempIdJVCOAffiliationAccId.clear();
                if(transferSelectionsDisplayMap.size() > 0) {  
                    for(String affKey: transferSelectionsDisplayMap.keySet()){
                        mapTempIdJVCOAffiliationAccId.put(affKey,String.valueOf(transferSelectionsDisplayMap.get(affKey).accountId));
                    }
                }
                //Start LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
                //GREEN-28159 fix for the cpu time limit.
                Boolean retAcquisition = true;
    
                System.debug('TRANSFER SIZE: ' + transferSelectionsCheckerMap.size());
                
                    JVCO_VenueTransfer venuetransfer = new JVCO_VenueTransfer();
                    retAcquisition =  venuetransfer.confirmDisposals(transferSelectionsCheckerMap, true);
    
                //if(!transferSelectionsToPassMap.isEmpty()) { // Commented jules.osberg.a.pablo     23/10/2017      GREEN-25128
                if(!transferSelectionsCheckerMap.isEmpty()) {    
                    if(retAcquisition) {
                        if(!transferSelectionsDisplayMap.isEmpty()) {
                            stepNumber = 2;
                        } else {
                            stepNumber = 3;
                        }
                    } else {
                        stepNumber = 1;
                        Database.rollback(sp);
                    }   
                }
            } else {
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select Affiliations to process'));
                stepNumber = 3; //00
            }
            loopCtr++; //00

        }
        // 07-09-19 mel.andrei.b.santos GREEN-34760 - process queueueable
        else
        {
            if(accountRecord.JVCO_VenDisposalQueueFlag__c == true)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Venue Acquisition for ' + accountRecord.Name + ' is currently processing, please wait for the job to finish.'));
                stepNumber = 1;
            }
            else
            {
                Id jobId = System.enqueueJob(new JVCO_DisposeVenueQueueable( accountRecord, mapTempIdJVCOAffiliationDate , mapTempIdJVCOAffiliationCloseReason, mapTempIdJVCOAffiliationAccId, transferSelectionsDisplayMap,  processSize));
                accountRecord.JVCO_VenDisposalQueueFlag__c = true;
                update accountRecord;
                stepNumber = 3;
            }            
        }

        return null; //00
    }
    
    public PageReference confirmDisposals3(){
        PageReference pr;
        pr = new PageReference('/'+accountRecord.Id);
        pr.setRedirect(true);
        return pr;
    }

    private void updateVenueProgress() {
        if(((loopCtr+1) * processSize) > venueSize) {
            venueProgress = venueSize;
        } else {
            venueProgress = (loopCtr+1) * processSize;
        }
    }

    public Boolean isValidLicenceAccountId(Set<String> idsToValidate) {
        List<Account> accountsForAcquisition = new List<Account>();
        Set<String> errorSet = new Set<String>();
        Set<String> validLicenceNumbers = new Set<String>();
        Boolean isValid = true;
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        try {
            accountsForAcquisition = [select Id, Name, Licence_Account_Number__c from Account where Licence_Account_Number__c in: idsToValidate and RecordTypeId =: licenceRT];
            if(accountsForAcquisition.size() > 0) {
                for(Account acc : accountsForAcquisition) {
                    validLicenceNumbers.add(acc.Licence_Account_Number__c);
                }
                for(String lan : idsToValidate) {
                    // START    raus.k.b.ablaza     Nov-15-2017     GREEN-25425 Exclude origin account
                    if( (!validLicenceNumbers.contains(lan)) || (lan == accountRecord.Licence_Account_Number__c) ) {
                        errorSet.add('Invalid Licence Account: ' + lan);
                    }
                    // END    raus.k.b.ablaza     Nov-15-2017     GREEN-25425 Exclude origin account
                }
            } else {
                errorSet.add('Acquiring Account/s not Found');
            }

            if(!errorSet.isEmpty()){
                throw new CustomException('There is a problem with venue disposal.');
            }
        } catch(Exception ex) {
            isValid = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'One or more Accounts for Acquisition is invalid or not a Licence Account'));
            for(String err : errorSet){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, err));
            }
        }
        
        return isValid;
    }
    
    public Boolean isId(String stringValue) {
        return stringValue InstanceOf Id;
    }
    
    public class CustomException extends Exception {}
}