/* ----------------------------------------------------------------------------------------------------------
Name:            JVCO_WebsitePaymentBatch.cls
Description:     Website Payment Batch class
Test class:      JVCO_WebsitePaymentBatch.cls

Date             Version     Author                            Summary of Changes
-----------      -------     -----------------                 -------------------------------------------
10-Jul-2018      0.1         jasper.j.figueroa                 Initial version of the code
21-Aug-2018      0.2         mel.andrei.b.Santos               GREEN-33186 - Updated class to only run Sage Pay but after Sage Pay, will trigger Direct Debit
13-Nov-2018      0.3         patrick.t.bautista                GREEN-33650 - Updated class to remove SagePay batch and put it on JVCO_deleteUnlinkedInvoiceGroupBatch
---------------------------------------------------------------------------------------------------------- */
public class JVCO_WebsitePaymentBatch /*implements Database.Batchable<sObject>*/
{
    //private Boolean isSingleRecord = false;
    /*private Boolean isSingleRecord = false;
    //private Id paymentId;

    public JVCO_WebsitePaymentBatch(){}
    /*public JVCO_WebsitePaymentBatch(Id paymentId)
    {
        this.paymentId = paymentId;
        isSingleRecord = true;
    }*/
    /*public Database.QueryLocator start(Database.BatchableContext bc)
    {
        /*if(isSingleRecord)
        {
            return Database.getQueryLocator([SELECT Id, Name, PAYCP2__Payment_Description__c,
                                            PAYBASE2__Created_From_Account__c, PAYBASE2__Status__c
                                            FROM PAYBASE2__Payment__c
                                            WHERE PAYBASE2__Status__c IN ('AUTHORISED', 'Pending', 'CANCELLED')
                                            AND PAYCP2__Payment_Description__c != Null
                                            AND PAYCP2__Payment_Description__c LIKE 'GIN%'
                                            AND RecordType.Name = 'SagePay'
                                            AND Id NOT IN (SELECT JVCO_Payonomy_Payment__c
                                                            FROM c2g__codaCashEntryLineItem__c
                                                            WHERE JVCO_Payonomy_Payment__c != null)
                                            AND id =: paymentId]);
        }
        else
        {
            return Database.getQueryLocator([SELECT Id, Name, PAYCP2__Payment_Description__c,
                                            PAYBASE2__Created_From_Account__c, PAYBASE2__Status__c
                                            FROM PAYBASE2__Payment__c
                                            WHERE PAYBASE2__Status__c IN ('AUTHORISED', 'Pending', 'CANCELLED')
                                            AND PAYCP2__Payment_Description__c != Null
                                            AND PAYCP2__Payment_Description__c LIKE 'GIN%'
                                            AND RecordType.Name = 'SagePay'
                                            AND Id NOT IN (SELECT JVCO_Payonomy_Payment__c
                                                            FROM c2g__codaCashEntryLineItem__c
                                                            WHERE JVCO_Payonomy_Payment__c != null)]);
        }*/
        /*return Database.getQueryLocator([select id, c2g__OwnerCompany__c from c2g__codaInvoice__c where id = 'a3H0Q000000Lcac']);
    }

    public void execute(Database.BatchableContext bc, List<c2g__codaInvoice__c> scope)
    {
        for(c2g__codaInvoice__c invoice : scope){
            system.debug('company'+ invoice.c2g__OwnerCompany__c);
            c2g__codaInvoiceInstallmentLineItem__c installine = new c2g__codaInvoiceInstallmentLineItem__c();
            installine.c2g__Amount__c = 10;
            installine.c2g__Invoice__c = invoice.Id;
            insert installine;
        }
        
        /*Map<String, JVCO_Invoice_Group__c> invGrpToMap = new Map<String, JVCO_Invoice_Group__c>();
        Map<String,List<String>> invGrpToSinvNameMap = new Map<String,List<String>>();
        List<String> slsinvNameList = new List<String>();
        Map<Id,String> invGroupToUpdateMap = new Map<Id,String>();
        Map<Id,String> autorizedPaymentToProcessMap = new Map<id, String>();
        Map<Id, PAYBASE2__Payment__c> paymentToMap = new Map<Id, PAYBASE2__Payment__c>(scope);

        for(PAYBASE2__Payment__c p : scope)
        {
            invGroupToUpdateMap.put(p.id, p.PAYCP2__Payment_Description__c);
            if(p.PAYBASE2__Status__c == 'AUTHORISED')
            {
                autorizedPaymentToProcessMap.put(p.id, p.PAYCP2__Payment_Description__c);  
            }
        }

        List<JVCO_Invoice_Group__c> invGrpList = [SELECT Id, Name, CC_SINs__c, JVCO_invNumbers__c 
                                                  FROM JVCO_Invoice_Group__c 
                                                  WHERE JVCO_invNumbers__c != null
                                                  AND Name IN: invGroupToUpdateMap.values()];
        //Invoice Group to be process based on Sage Payment Description
        for(JVCO_Invoice_Group__c invGrp: invGrpList)
        {
            if (!string.isBlank(invGrp.JVCO_invNumbers__c))
            {
                for(String invNum : invGrp.JVCO_invNumbers__c.split(','))
                {
                    slsinvNameList.add(invNum);
                    if(!invGrpToSinvNameMap.containsKey(invGrp.Name))
                    {
                        invGrpToSinvNameMap.put(invGrp.Name, new List<String>());
                    }
                    invGrpToSinvNameMap.get(invGrp.Name).add(invNum);
                }
                invGrp.CC_SINs__c = invGrp.JVCO_invNumbers__c;
                invGrpToMap.put(invGrp.Name, invGrp);
            }
        }

        if(!slsinvNameList.isEmpty())
        {
            //Mapping of Sales Invoice
            Map<string,c2g__codaInvoice__c> slsinvsNameMap = new Map<string,c2g__codaInvoice__c>();
            for(c2g__codaInvoice__c slsInvList : [SELECT Id, Name, JVCO_Invoice_Group__c, JVCO_Payment_Method__c 
                                                  FROM c2g__codaInvoice__c 
                                                  WHERE Name IN:slsinvNameList])
            {
                slsinvsNameMap.put(slsInvList.Name, slsInvList);
            }
            //List of Sales Invoice to be update
            Set<c2g__codaInvoice__c> slsToUpdate = new Set<c2g__codaInvoice__c>();
            //
            for(Id paymentId: autorizedPaymentToProcessMap.keyset()){
                {   
                    if(autorizedPaymentToProcessMap.containsKey(paymentId) && invGrpToSinvNameMap.containsKey(autorizedPaymentToProcessMap.get(paymentId)))
                    {
                        for(string slsInvStr: invGrpToSinvNameMap.get(autorizedPaymentToProcessMap.get(paymentId)))
                        {
                            if(slsinvsNameMap.containsKey(slsInvStr) && invGrpToMap.containsKey(autorizedPaymentToProcessMap.get(paymentId)))
                            {
                                c2g__codaInvoice__c invoice = slsinvsNameMap.get(slsInvStr);
                                invoice.JVCO_Invoice_Group__c = invGrpToMap.get(autorizedPaymentToProcessMap.get(paymentId)).id;
                                invoice.JVCO_Payment_Method__c='Debit/Credit Card';
                                slsToUpdate.add(invoice); 
                            }
                        }
                    }
                }
            }
            if(!slsToUpdate.isEmpty()){
                update new List<c2g__codaInvoice__c>(slsToUpdate);
            }
            if(!invGrpToMap.isEmpty()){ 
                update invGrpToMap.values();
            }
        }*/
/*}
    
    public void finish(Database.BatchableContext bc)
    {
        //Calling JVCO_deleteUnlinkedInvoiceGroupBatch to Delete floating Invoice Group
        //Database.executebatch(new JVCO_deleteUnlinkedInvoiceGroupBatch(), 10000);
    }*/
    
}