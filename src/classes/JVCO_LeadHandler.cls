/* ----------------------------------------------------------------------------------------------
   Name: JVCO_LeadHandler.cls 
   Description: Handler class for Lead object

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  06-Sep-2016   0.1         mark.glenn.b.ayson  Intial creation
  ----------------------------------------------------------------------------------------------- */
  
public with sharing class JVCO_LeadHandler 
{

  public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
  public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_LeadTrigger__c : false;

  public static void onAfterInsert(List<Lead> leadList, Map<id, Lead> oldLeadMap) {
    if(!skipTrigger) 
        {
          //createTaskAfterInsert(leadList);
		  updateLeadOwnerID(leadList, true);
          updateOwnerToDefaultQueue(leadList);
        }
  }
  public static void onBeforeInsert(List<Lead> leadList, Map<id, Lead> oldLeadMap) {
    if(!skipTrigger) 
        {
          updateLeadOwnerID(leadList, false);
        }
  }
  public static void onBeforeUpdate(List<Lead> leadList, Map<id, Lead> oldLeadMap) {
    if(!skipTrigger) 
        {
          updateLeadOwnerID(leadList, oldLeadMap);
        }
  }
  /* ------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: This method handles beforeUpdate and beforeInsert trigger events for Lead
    Input: List<Lead>, Map<Id, Lead>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    06-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
  public static void updateLeadOwnerID(List<Lead> newValue, Map<Id, Lead> oldValue)
  {
      List<Lead> leadRecordList = new List<Lead>();

      // Check if the postcode or field visit checkbox is changed to trigger the handler
      for(Lead leadRecord : newValue)
      {
        // Get the values from the old map
        Lead oldLead = oldValue.get(leadRecord.Id);

        if(!String.isBlank(leadRecord.JVCO_Venue_Postcode__c) && leadRecord.JVCO_Field_Visit_Requested__c == true)
        {
            // Check if either one of the fields has changed
            if(!leadRecord.JVCO_Venue_Postcode__c.equals(oldLead.JVCO_Venue_Postcode__c) || !(leadRecord.JVCO_Field_Visit_Requested__c == oldLead.JVCO_Field_Visit_Requested__c))
            {
                // If either changes, fire the trigger handler
                System.debug('There is a change.');
                leadRecordList.add(leadRecord);
            }
        }
      }

      if(leadRecordList.size() > 0)
      {
          // This will load the lead handler method (List<Lead>, Boolean if the trigger is an update)
          JVCO_LeadAssignFieldAgentsPostCodeLogic.updateLeadOwnerID(leadRecordList, true);
      }
  }

  /* ------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: This method handles beforeUpdate and beforeInsert trigger events for Lead
    Input: List<Lead>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    21-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
  public static void updateLeadOwnerID(List<Lead> newValue, Boolean isCreateTask)
  {
      List<Lead> leadRecordList = new List<Lead>();

      // Get all the new record detail for the lead
      for(Lead leadRecord : newValue)
      {
          // Add the record if postcode has a value and the field visit request is checked
          if(!String.isBlank(leadRecord.JVCO_Venue_Postcode__c) && leadRecord.JVCO_Field_Visit_Requested__c == true)
          {
              leadRecordList.add(leadRecord);
          }
      }

      if(leadRecordList.size() > 0)
      {
          // This will load the lead handler method (List<Lead>, Boolean if the trigger is an update)
          JVCO_LeadAssignFieldAgentsPostCodeLogic.updateLeadOwnerID(leadRecordList, isCreateTask);
      }
  }

  /* ------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: This method handles beforeUpdate and beforeInsert trigger events for Lead
    Input: List<Lead>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    21-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
  /**
  public static void createTaskAfterInsert(List<Lead> newValue)
  {
      // Create a task after the record is inserted
      List<JVCO_AssignFieldAgentWrapper> taskRecordList = new List<JVCO_AssignFieldAgentWrapper>();
      JVCO_AssignFieldAgentWrapper wrapper = new JVCO_AssignFieldAgentWrapper();  

      // Get the detail of the new record
      for(Lead lead : newValue)
      {
          if(lead.JVCO_Field_Visit_Requested__c == true && !String.isBlank(lead.JVCO_Venue_Postcode__c))
          {
              wrapper = new JVCO_AssignFieldAgentWrapper(); 
              
              // Add the venue name
              wrapper.venueName = lead.Company;

              // Check if the newOwnerID is not a queue, add the new owner ID else, add the current logged-in user
              if(lead.OwnerId != JVCO_AssignFieldAgentsPostCodeUtil.queueID)
              {
                wrapper.ownerID = lead.ownerID;
              }
              else
              {
                wrapper.ownerID = UserInfo.getUserId();
              }

              // Add the record ID
              wrapper.recordID = lead.Id;

              // Add to list
              taskRecordList.add(wrapper);
          }
      }

      if(taskRecordList.size() > 0)
      {
          // Call the create task methid in the util class (List<String>, a boolean value if the originating object is a Lead)
          JVCO_AssignFieldAgentsPostCodeUtil.createNewTask(taskRecordList, true);
      }
  }*/
  
  
   /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    Populates the Budget Category Field based from the Venue Type against the Budget Category Dimension Mapping list
    Inputs:         Lead record list
    Returns:        Boolean
    <Date>          <Authors Name>                    <Brief Description of Change> 
    04-Apr-2017     joseph.g.barrameda                 Initial version of the code
    21-Sep-2017     joseph.g.barrameda                 Changed argument from Venue SubType to Venue Type
    ----------------------------------------------------------------------------------------------------------*/    
    public static void updateBudgetCategory(List<Lead> leadList)
    {
    
        List<JVCO_Budget_Category_Dimension_Mapping__c> budgetCategoryList =JVCO_Budget_Category_Dimension_Mapping__c.getall().values(); 
        Map<String,String> getBudgetCategoryByVenueType = new Map<String,String>();
        
        for(JVCO_Budget_Category_Dimension_Mapping__c bcdm: budgetCategoryList ){
            getBudgetCategoryByVenueType.put(bcdm.JVCO_Venue_Type_Label__c, bcdm.JVCO_Budget_Category_Label__c);
        }
        
        for(Lead leadRec: leadList){
            leadRec.JVCO_Budget_Category__c = getBudgetCategoryByVenueType.get(leadRec.JVCO_Venue_Type__c);
        }
            
    }

  /* ------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Update Lead Owner to JVCO_Default_Queue
    Input: List<Lead>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    19-Jul-2017 kristoffer.d.martin  Initial version of function
  ----------------------------------------------------------------------------------------------- */
  public static void updateOwnerToDefaultQueue(List<Lead> newValue)
  {
      Set<Id> leadIds = new Set<Id>();

      for (Lead leadObj : newValue) 
      {
          leadIds.add(leadObj.Id);
      }

      List<Group> ownerGroup = [SELECT Id, Name, DeveloperName FROM Group WHERE DeveloperName ='JVCO_Default_Queue' LIMIT 1];
      List<Lead> leadList = [SELECT Id, OwnerId, LeadSource FROM Lead WHERE Id in :leadIds];
      List<Lead> leadForUpdate = new List<Lead>();

      // Get the detail of the new record
      for(Lead lead : leadList)
      {
          if(lead.LeadSource == 'Churn' && lead.OwnerId != ownerGroup[0].Id)
          {
              lead.OwnerId = ownerGroup[0].Id;
              leadForUpdate.add(lead);
          }
      }

      if (!leadForUpdate.isEmpty())
      {
          update leadForUpdate;
      }
      
  }
}