/* ----------------------------------------------------------------------------------------------
   Name: JVCO_codaJournalHandler.cls 
   Description: Handler class for c2g__codaJournal__c trigger

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  28-Mar-2019   0.1         patrick.t.bautista  Intial creation - GREEN-34440
  ----------------------------------------------------------------------------------------------- */
public class JVCO_codaJournalHandler {
    public static boolean skipJournalTrigger = false;
    public static void beforeInsert(List<c2g__codaJournal__c> newJournalList)
    {
        if(!skipJournalTrigger)
        {
            restrictCreateUpdate(newJournalList, true);
        }
    }
    public static void beforeUpdate(List<c2g__codaJournal__c> newJournalList)
    {
        if(!skipJournalTrigger)
        {
            restrictCreateUpdate(newJournalList, false);
        }
    }
    /* ------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Update Dimension2 based on GLAs Criteria
        Input: List<c2g__codaJournalLineItem__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        25-Mar-2019 patrick.t.bautista  Initial version of the code
    ----------------------------------------------------------------------------------------------- */
    public static void restrictCreateUpdate(List<c2g__codaJournal__c> newJournalList, boolean isInsertChecker)
    {
        String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
        String errMessage = isInsertChecker ? 'Can\'t create a record' : 'Can\'t update a record';
        if(profileName == 'Account Receivable Assistant' || profileName == 'Licensing User' || profileName == 'Recovery & Collection')
        {
            for(c2g__codaJournal__c journal : newJournalList)
            {
                if(journal.c2g__Type__c != 'Cash Matching Journal')
                {
                    journal.addError(errMessage);
				}
            }
        }
    }
}