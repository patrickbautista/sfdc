@isTest
private class JVCO_VenueHandlerTest
{
	@testSetup
    private static void setupData(){
       
        test.StartTest();

        List<JVCO_Primary_Tariff_Mapping__c> settings = new List<JVCO_Primary_Tariff_Mapping__c>();
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Aircraft', JVCO_Primary_Tariff__c = 'AC_PRS'));
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Hotel', JVCO_Primary_Tariff__c = 'HRH_PRS'));
		settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Airport', JVCO_Primary_Tariff__c = 'GP_PRS'));
        insert settings;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        PriceBook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
		acc2.JVCO_Live__c = true;
        insert acc2;
      
        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
		ven.JVCO_Primary_Tariff__c = 'AC_PRS';
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_End_Date__c = null;
        aff.JVCO_Closure_Reason__c = null;
        insert aff;

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        insert opp;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
        insert q;
        
        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Primary_Tariff__c, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.id, prod.id);
        insert ql;

        SBQQ__Subscription__c sub = JVCO_TestClassObjectBuilder.createSubscription(acc2.id, prod.id);
        insert sub;      

        Test.StopTest();
    }

	@isTest
	static void testUpdateAndDelete() 
	{
		JVCO_Venue__c ven = new JVCO_Venue__c();
		ven = [SELECT JVCO_Postcode__c, JVCO_Venue_Type__c FROM JVCO_Venue__c limit 1];
		ven.JVCO_Postcode__c = 'AL2 1AA';
        ven.JVCO_Venue_Type__c = 'Airport';

		Test.StartTest();
		update ven;
		delete ven;
		List<JVCO_Venue__c> l_ven = [SELECT Id FROM JVCO_Venue__c];
		System.Assert(l_ven.isEmpty());
		Test.StopTest();
	}

	@isTest
	static void testErrorUpdate() 
	{
		try {
				JVCO_Venue__c ven = new JVCO_Venue__c();
				ven = [SELECT JVCO_Postcode__c FROM JVCO_Venue__c limit 1];
				ven.JVCO_Postcode__c = 'AL2 1AA';
		        ven.JVCO_Venue_Type__c = 'Bandstand';

				Test.StartTest();
				update ven;
				Test.StopTest();	
		}
		catch (exception e) 
		{
			System.assert((String.valueof(e)).contains('Primary Tariff'));
		}
	}

	@isTest
	static void testErrorInsert() 
	{
		try {
				JVCO_Venue__c ven = new JVCO_Venue__c();
				ven.JVCO_Postcode__c = 'AL2 1AA';
		        ven.JVCO_Venue_Type__c = 'Bandstand';

				Test.StartTest();
				insert ven;
				Test.StopTest();	
		}
		catch (exception e) 
		{
			System.assert((String.valueof(e)).contains('Primary Tariff'));
		}
	}
}