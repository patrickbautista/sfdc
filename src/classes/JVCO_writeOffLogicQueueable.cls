public class JVCO_writeOffLogicQueueable implements Queueable {
    
   private List<Case> caseList = new List<Case>();
   
    public JVCO_writeOffLogicQueueable(List<Case> caseList){
    this.caseList = caseList;
    }
    
    public void execute(QueueableContext context) {
        try{
            JVCO_caseAccountWriteOffLogic.updateCaseId(caseList);
            if(Test.isRunningTest()){
                CalloutException e = new CalloutException();
                e.setMessage('Test Exception in Test Class');
                throw e;
            }
        }catch(Exception e){
            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
            errLog.ffps_custRem__Detail__c = string.valueof(e.getMessage());
            errLog.ffps_custRem__Message__c = 'Write Off Updates';
            errLog.ffps_custRem__Grouping__c = 'Write Off';
            
            insert errLog;
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            //mail.setToAddresses(new String[] {aJob.CreatedBy.Email});
            mail.setTargetObjectId(UserInfo.getUserId());
            mail.setReplyTo('no-reply@salesforce.com');
            mail.setSenderDisplayName('Write Off Update');
            mail.setSubject('Write Off Update');
            mail.setSaveAsActivity(false);
    
            String emailMsg = 'An error was encountered in updating case id';
            
            emailMsg += '<br><br>';
            emailMsg += e.getmessage();
            mail.setHTMLBody(emailMsg);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });             
        }
   }
}