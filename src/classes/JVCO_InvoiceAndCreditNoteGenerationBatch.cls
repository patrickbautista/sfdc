/* ----------------------------------------------------------------------------------------------
    Name: JVCO_InvoiceAndCreditNoteGenerationBatch
    Description: Batch for SalesInvoice And CreditNote Generation Logic

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    27-Dec-2017  0.1         franz.g.a.dimaapi     Intial creation
    20-Jan-2018  0.2         franz.g.a.dimaapi     GREEN-27518 - Refactor Class
    29-Jan-2018  0.3         franz.g.a.dimaapi     GREEN-28491 - Surcharge Date Missing
    17-Mar-2018  0.4         franz.g.a.dimaapi     GREEN-30762 - Fix Duedate issue for different payment terms
    28-Mar-2018  0.5         franz.g.a.dimaapi     GREEN-31166 - Fix Parent issue in creditnote
    18-Apr-2018  0.6         franz.g.a.dimaapi     GREEN-31002 - Enhancement for Separating Payment Terms
    18-Jul-2018  0.7         patrick.t.bautista    GREEN-32099 - Separating Credit Reason
    26-Jul-2018  0.8         joseph.g.barrameda    GREEN-32865 - Remove Database.Stateful
    25-Nov-2019  0.9         franz.g.a.dimaapi     GREEN-33284 - Calculate Tax Value From Billing Parent Tax Total
    13-May-2020  1.0         patrick.t.bautista    GREEN-35525 - change payonomy to smarterpay
    25-Nov-2020  1.1         patrick.t.bautista    GREEN-36039 - added criteria Re-issued
----------------------------------------------------------------------------------------------- */
public class JVCO_InvoiceAndCreditNoteGenerationBatch implements Database.Batchable<sObject>
{
    //Sets of Invoice Run
    private Set<Id> invRunIdSet;
    //Invoice Scheduler Invoice Date
    private Date invoiceDate;

    //Map of Blng Invoice Id to SalesInvoice/CreditNote Id
    private Map<Id, Id> bInvIdToDocHeaderIdMap = new Map<Id, Id>();

    //Dim2,Prod,Account, userCompany Mapping, VATRegistration Mapping
    private Map<String, Id> dim2Map;
    private Map<String, Id> prodMap;
    private Map<Id, Account> accMap;
    private c2g__codaUserCompany__c userCompany;
    // Map Account Name to Tax Registration Number
    private Map<String, String> taxRegNumMap;
    private Map<Id, Id> accIdToSalesRepIdMap;

    public JVCO_InvoiceAndCreditNoteGenerationBatch(Set<Id> invRunIdSet, Date invoiceDate)
    {
        this.invRunIdSet = invRunIdSet;
        this.invoiceDate = invoiceDate;
        setDim2AndProdMapping();
        setCompanyVATRegistrationNum();
        constructSupplierRecordTaxRegistrationNumber();
    }

    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT Id, JVCO_Dunning_Letter_Language__c,
                                        c2g__CODADimension1__c, c2g__CODADimension3__c,
                                        c2g__CODAOutputVATCode__c
                                        FROM Account
                                        WHERE Id IN (SELECT blng__Account__c
                                                    FROM blng__Invoice__c
                                                    WHERE blng__InvoiceRunCreatedBy__c IN :invRunIdSet
                                                    AND JVCO_Sales_Invoice__c = NULL
                                                    AND JVCO_Sales_Credit_Note__c = NULL)
                                        AND Id NOT IN (SELECT blng__Account__c
                                                        FROM blng__Invoice__c
                                                        WHERE blng__InvoiceRunCreatedBy__c IN :invRunIdSet
                                                        AND (blng__NumberOfInvoiceLines__c = 0
                                                        OR JVCO_Matched_With_Total_Order_Products__c = false))
                                        ]);    
    }

    public void execute(Database.BatchableContext bc, List<Account> scope)
    {
        //Set Account Mapping
        accMap = new Map<Id, Account>(scope);
        //Set Quote Sales Rep to Sales Invoice
        setAccIdToSalesRepIdMap(scope);
        //Create Invoice/CreditNote
        setupInvoiceAndCreditNoteHeader(scope);
        //Create Document Id Map Records
        createTmpInvoiceProcessingDataRecords();        
    }

    /* ----------------------------------------------------------------------------------------------
        Author: patrick.t.bautista
        Company: Accenture
        Description: Getting Sales Rep from Quote to Sales Invoice/Sales Credit Note
        Inputs: List<Account>
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        16-Mar-2018 patrick.t.bautista    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void setAccIdToSalesRepIdMap(List<Account> accList)
    {
        accIdToSalesRepIdMap = new Map<Id, Id>();
        Integer counter = 0;
        Id acct = null;
        for(AggregateResult ar : [SELECT blng__Account__c accId, 
                                   blng__Order__r.SBQQ__Quote__r.SBQQ__SalesRep__c salesRepId, 
                                   COUNT(blng__Order__r.SBQQ__Quote__c) salesRepCount
                                   FROM blng__Invoice__c 
                                   WHERE blng__Order__r.SBQQ__Quote__r.SBQQ__SalesRep__c != null
                                   AND blng__InvoiceRunCreatedBy__c IN : invRunIdSet
                                   AND blng__Account__c != NULL
                                   AND blng__Account__c IN :accList 
                                   GROUP BY blng__Account__c, blng__Order__r.SBQQ__Quote__r.SBQQ__SalesRep__c
                                   ORDER BY blng__Account__c ASC
                                  ])
        {
            //Refresh counter if the account is new
            if((acct != (id)ar.get('accId')) || acct == null) 
            {
                counter = 0;
            }
            //Mapping for Sales Invoice SalesRep
            if((Integer)ar.get('salesRepCount') > counter)
            {
                acct = (id)ar.get('accId');
                counter = (Integer)ar.get('salesrepcount');
                accIdToSalesRepIdMap.put((id)ar.get('accId'),(Id)ar.get('salesRepId'));
            }
        }
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Create Invoice And CreditNote Header
        Inputs: List<Account>
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void setupInvoiceAndCreditNoteHeader(List<Account> accList)
    {
        Map<Id, Map<String, Map<String, AggregateResult>>> accIdToPaymentTermToAggregateResultMap = getAccIdToPaymentTermToAggregateResultMap(accList);
        Map<Id, Map<String, Map<String, c2g__codaInvoice__c>>> accIdToPaymentTermToSInvMap = new Map<Id, Map<String, Map<String, c2g__codaInvoice__c>>>();
        Map<Id, Map<String, Map<String, c2g__codaCreditNote__c>>> accIdToPaymentTermToCNoteMap = new Map<Id, Map<String, Map<String, c2g__codaCreditNote__c>>>();
        List<c2g__codaInvoice__c> sInvList = new List<c2g__codaInvoice__c>();
        List<c2g__codaCreditNote__c> cNoteList = new List<c2g__codaCreditNote__c>();
        if(!accIdToPaymentTermToAggregateResultMap.isEmpty())
        {
            for(Account acc : accList)
            {
                for(String paymentTerm : accIdToPaymentTermToAggregateResultMap.get(acc.Id).keySet())
                {
                    for(String credReason : accIdToPaymentTermToAggregateResultMap.get(acc.Id).get(paymentTerm).keySet())
                    {
                        AggregateResult ar = accIdToPaymentTermToAggregateResultMap.get(acc.Id).get(paymentTerm).get(credReason);
                        //Set Total Amount
                        Double totalAmt = (Double)ar.get('totalAmt');
                        Date minStartDate = (Date)ar.get('minStartDate');
                        Date maxEndDate = (Date)ar.get('maxEndDate');
                        Date minDueDate = invoiceDate.addDays(Integer.valueOf(paymentTerm));
                        //Invoice 
                        if(totalAmt >= 0 && credReason == null)
                        {
                            Decimal totalSurAmount = (Decimal)ar.get('totalSurAmount');
                            Decimal totalSurTariffs = (Decimal)ar.get('totalSurTariffs');
                            Boolean ifReissuedInvoice = (Decimal)ar.get('ifReissuedInvoice') == 1 ? true : false;
                            c2g__codaInvoice__c sInv = createSalesInvoice(acc, totalSurAmount, totalSurTariffs, minStartDate, maxEndDate, minDueDate, ifReissuedInvoice);
                            if(!accIdToPaymentTermToSInvMap.containsKey(acc.Id))
                            {
                                accIdToPaymentTermToSInvMap.put(acc.Id, new Map<String, Map<String, c2g__codaInvoice__c>>());
                            }
                            if(!accIdToPaymentTermToSInvMap.get(acc.Id).containskey(paymentTerm))
                            {
                                accIdToPaymentTermToSInvMap.get(acc.Id).put(paymentTerm, new Map<String, c2g__codaInvoice__c>());
                            }
                            accIdToPaymentTermToSInvMap.get(acc.Id).get(paymentTerm).put(credReason, sInv);
                            sInvList.add(sInv);
                        //CreditNote
                        }else if(totalAmt < 0)
                        {
                            Boolean migratedContract = (Decimal)ar.get('migratedContractCounter') > 0;
                            String parent = 'COMPANY';
                            if(migratedContract)
                            {
                                if((Decimal)ar.get('prsTotalLines') > 0)
                                {
                                    parent = 'PRS';
                                }else
                                {
                                    parent = (Decimal)ar.get('pplTotalLines') > 0 ? 'PPL' : 'VPL';
                                }
                            }

                            Id sInvId = (Decimal)ar.get('origSInvCounter') == 1 ? (Id)ar.get('origSInvId') : null;
                            String sInvName = (String)ar.get('origSInvName') != '' ? (String)ar.get('origSInvName') : '';
                            c2g__codaCreditNote__c cNote = createCreditNote(acc, minStartDate, maxEndDate, parent, minDueDate, sInvId, sInvName);
                            if(!accIdToPaymentTermToCNoteMap.containsKey(acc.Id))
                            {
                                accIdToPaymentTermToCNoteMap.put(acc.Id, new Map<String, Map<String, c2g__codaCreditNote__c>>());
                            }
                            if(!accIdToPaymentTermToCNoteMap.get(acc.Id).containskey(paymentTerm))
                            {
                                accIdToPaymentTermToCNoteMap.get(acc.Id).put(paymentTerm, new Map<String, c2g__codaCreditNote__c>());
                            }
                            accIdToPaymentTermToCNoteMap.get(acc.Id).get(paymentTerm).put(credReason, cNote);
                            cNoteList.add(cNote);
                        }
                    }
                }
            }
            //Insert Sales Invoice
            insert sInvList;

            //Insert Credit Note
            insert cNoteList;

            setupSInvAndCNoteLine(accIdToPaymentTermToAggregateResultMap, accIdToPAymentTermToSInvMap, accIdToPaymentTermToCNoteMap);
            setupBInvIdToDocumentHeaderIdMap(accIdToPAymentTermToSInvMap, accIdToPaymentTermToCNoteMap);
        }
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Call Invoice And CreditNote Lines Setup
        Inputs: Map<Id, Map<String, AggregateResult>>, 
                Map<Id, Map<String, c2g__codaInvoice__c>>,
                Map<Id, Map<String, c2g__codaCreditNote__c>>
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void setupSInvAndCNoteLine(Map<Id, Map<String, Map<String, AggregateResult>>> accIdToPaymentTermToAggregateResultMap,
                                        Map<Id, Map<String, Map<String, c2g__codaInvoice__c>>> accIdToPaymentTermToSInvMap,
                                        Map<Id, Map<String, Map<String, c2g__codaCreditNote__c>>> accIdToPaymentTermToCNoteMap)
    {
        //Sales Invoice Line
        if(!accIdToPaymentTermToSInvMap.isEmpty())
        {
            setupSInvLine(accIdToPaymentTermToAggregateResultMap, accIdToPaymentTermToSInvMap);
        }

        //Credit Note Line
        if(!accIdToPaymentTermToCNoteMap.isEmpty())
        {
            setupCNoteLine(accIdToPaymentTermToAggregateResultMap, accIdToPaymentTermToCNoteMap);
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Create Invoice And CreditNote Lines
        Inputs: Map<Id, Map<String, AggregateResult>>, 
                Map<Id, Map<String, c2g__codaInvoice__c>>
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        28-Feb-2020 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void setupSInvLine(Map<Id, Map<String, Map<String, AggregateResult>>> accIdToPaymentTermToAggregateResultMap,
                                Map<Id, Map<String, Map<String, c2g__codaInvoice__c>>> accIdToPaymentTermToSInvMap)
    {
        //Set Sales Invoice Line
        Set<c2g__codaInvoiceLineItem__c> sInvLineItemSet = new Set<c2g__codaInvoiceLineItem__c>();
        for(Id accId : accIdToPaymentTermToSInvMap.keySet())
        {   
            for(String paymentTerm : accIdToPaymentTermToAggregateResultMap.get(accId).keySet())
            {
                for(String credReason : accIdToPaymentTermToAggregateResultMap.get(accId).get(paymentTerm).keySet())
                {
                    if(accIdToPaymentTermToSInvMap.get(accId).get(paymentTerm).containsKey(credReason))
                    {
                        c2g__codaInvoice__c sInv = accIdToPaymentTermToSInvMap.get(accId).get(paymentTerm).get(credReason);
                        AggregateResult ar = accIdToPaymentTermToAggregateResultMap.get(accId).get(paymentTerm).get(credReason);
                        //PRS
                        if((Decimal)ar.get('prsTotalLines') > 0)
                        {
                            sInvLineItemSet.add(createSalesInvLineItem(sInv.Id, accId, (Decimal)ar.get('prsTotalAmt'), (Decimal)ar.get('prsTotalTaxAmt'), 'PRS'));
                        }
                        //PPL
                        if((Decimal)ar.get('pplTotalLines') > 0)
                        {
                            sInvLineItemSet.add(createSalesInvLineItem(sInv.Id, accId, (Decimal)ar.get('pplTotalAmt'), (Decimal)ar.get('pplTotalTaxAmt'), 'PPL'));
                        }
                        //VPL
                        if((Decimal)ar.get('vplTotalLines') > 0)
                        {
                            sInvLineItemSet.add(createSalesInvLineItem(sInv.Id, accId, (Decimal)ar.get('vplTotalAmt'), (Decimal)ar.get('vplTotalTaxAmt'), 'VPL'));
                        } 
                    }
                }
            }    
        }
        //Insert Invoice Line
        insert new List<c2g__codaInvoiceLineItem__c>(sInvLineItemSet);
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Create CreditNote Lines
        Inputs: Map<Id, Map<String, AggregateResult>>,
                Map<Id, Map<String, c2g__codaCreditNote__c>>
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        28-Feb-2020 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void setupCNoteLine(Map<Id, Map<String, Map<String, AggregateResult>>> accIdToPaymentTermToAggregateResultMap,
                                    Map<Id, Map<String, Map<String, c2g__codaCreditNote__c>>> accIdToPaymentTermToCNoteMap)
    {
        //Setup Sales Credit Note Line
        Set<c2g__codaCreditNoteLineItem__c> cNoteLineItemSet = new Set<c2g__codaCreditNoteLineItem__c >();
        for(Id accId : accIdToPaymentTermToCNoteMap.keySet())
        {  
            for(String paymentTerm : accIdToPaymentTermToCNoteMap.get(accId).keySet())
            {
                for(String credReason : accIdToPaymentTermToAggregateResultMap.get(accId).get(paymentTerm).keySet())
                {
                    if(accIdToPaymentTermToCNoteMap.get(accId).get(paymentTerm).containsKey(credReason))
                    {
                        c2g__codaCreditNote__c cNote = accIdToPaymentTermToCNoteMap.get(accId).get(paymentTerm).get(credReason);
                        AggregateResult ar = accIdToPaymentTermToAggregateResultMap.get(accId).get(paymentTerm).get(credReason);
                        //PRS
                        if((Decimal)ar.get('prsTotalLines') > 0)
                        {
                            cNoteLineItemSet.add(createCredNoteLineItem(cNote.Id, accId, (Decimal)ar.get('prsTotalAmt'), (Decimal)ar.get('prsTotalTaxAmt'), 'PRS'));
                        }
                        //PPL
                        if((Decimal)ar.get('pplTotalLines') > 0)
                        {
                            cNoteLineItemSet.add(createCredNoteLineItem(cNote.Id, accId, (Decimal)ar.get('pplTotalAmt'), (Decimal)ar.get('pplTotalTaxAmt'), 'PPL'));
                        }
                        //VPL
                        if((Decimal)ar.get('vplTotalLines') > 0)
                        {
                            cNoteLineItemSet.add(createCredNoteLineItem(cNote.Id, accId, (Decimal)ar.get('vplTotalAmt'), (Decimal)ar.get('vplTotalTaxAmt'), 'VPL'));
                        }
                    }
                }
            }    
        }
        //Insert Sales Credit Note Line
        insert new List<c2g__codaCreditNoteLineItem__c>(cNoteLineItemSet);
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Create Billing Invoice Id to Document Header Id Map
        Inputs: Map<Id, Map<String, c2g__codaInvoice__c>>, Map<Id, Map<String, c2g__codaCreditNote__c>>
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void setupBInvIdToDocumentHeaderIdMap(Map<Id, Map<String, Map<String, c2g__codaInvoice__c>>> accIdToPaymentTermToSInvMap, 
                                                    Map<Id, Map<String, Map<String, c2g__codaCreditNote__c>>> accIdToPaymentTermToCNoteMap)
    {
        for(blng__Invoice__c bInv : [SELECT Id, blng__Account__c, blng__Order__r.SBQQ__Quote__r.JVCO_Payment_Terms__c,
                                     blng__Order__r.SBQQ__Quote__r.JVCO_Quote_Id_to_Scheduler__c
                                     FROM blng__Invoice__c
                                     WHERE blng__InvoiceRunCreatedBy__c IN : invRunIdSet])
        {   
            String paymentTerm = bInv.blng__Order__r.SBQQ__Quote__r.JVCO_Payment_Terms__c;
            String credReason = bInv.blng__Order__r.SBQQ__Quote__r.JVCO_Quote_Id_to_Scheduler__c;
            //Invoice
            if(accIdToPaymentTermToSInvMap.containsKey(bInv.blng__Account__c) &&
                accIdToPaymentTermToSInvMap.get(bInv.blng__Account__c).containsKey(paymentTerm) &&
                accIdToPaymentTermToSInvMap.get(bInv.blng__Account__c).get(paymentTerm).containsKey(credReason))
            {
                c2g__codaInvoice__c sInv = accIdToPaymentTermToSInvMap.get(bInv.blng__Account__c).get(paymentTerm).get(credReason);
                bInvIdToDocHeaderIdMap.put(bInv.Id, sInv.Id);
            //Credit Note
            }else if(accIdToPaymentTermToCNoteMap.containsKey(bInv.blng__Account__c) &&
                     accIdToPaymentTermToCNoteMap.get(bInv.blng__Account__c).containsKey(paymentTerm) &&
                     accIdToPaymentTermToCNoteMap.get(bInv.blng__Account__c).get(paymentTerm).containsKey(credReason))
            {
                c2g__codaCreditNote__c cNote = accIdToPaymentTermToCNoteMap.get(bInv.blng__Account__c).get(paymentTerm).get(credReason);
                bInvIdToDocHeaderIdMap.put(bInv.Id, cNote.Id);
            }
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Sales Invoice
        Inputs: Account, Decimal, Decimal, Date, Date, Date
        Returns: c2g__codaInvoice__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private c2g__codaInvoice__c createSalesInvoice(Account a, Decimal totalSurAmount, Decimal totalSurTariffs, Date minStartDate, Date maxEndDate, Date minDueDate, Boolean ifReissuedInvoice)
    {
        c2g__codaInvoice__c sInv = new c2g__codaInvoice__c();
        sInv.c2g__Account__c = a.Id;
        sInv.c2g__DueDate__c = minDueDate;
        sInv.c2g__InvoiceDate__c = invoiceDate;
        sInv.c2g__DeriveCurrency__c = true;
        sInv.c2g__DerivePeriod__c = true;
        sInv.c2g__DeriveDueDate__c = false;
        sInv.JVCO_Licence_Start_Date__c = minStartDate;
        sInv.JVCO_Licence_End_Date__c = maxEndDate;
        sInv.JVCO_Customer_Type__c = a.JVCO_Dunning_Letter_Language__c;
        sInv.JVCO_Surcharge_Tariffs__c = totalSurTariffs;
        sInv.JVCO_Surcharge_Generation_Date__c = JVCO_DocumentGenerationWrapper.getSurchargeDate(a.JVCO_Dunning_Letter_Language__c, totalSurAmount, totalSurTariffs, sInv.c2g__DueDate__c);
        sInv.Total_Surchargeable_Amount__c = totalSurAmount;
        sInv.JVCO_Sales_Rep__c = accIdToSalesRepIdMap.get(a.Id);
        sInv.Re_issued__c = ifReissuedInvoice;
        return sInv;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Credit Note
        Inputs: Account, Date, Date, String, Date
        Returns: c2g__codaCreditNote__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2018 franz.g.a.dimaapi    Initial version of function
        01-Jan-2018 franz.g.a.dimaapi    GREEN-34200 - Removed JVCO_Migrated_Credit_Note__c flagging
    ----------------------------------------------------------------------------------------------- */
    private c2g__codaCreditNote__c createCreditNote(Account a, Date minStartDate, Date maxEndDate, String parent, Date minDueDate, Id sInvId, String sInvName)
    {
        c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
        cNote.c2g__Account__c = a.Id;
        cNote.c2g__DueDate__c = minDueDate;
        cNote.c2g__CreditNoteDate__c = invoiceDate;
        cNote.c2g__DeriveCurrency__c = true;
        cNote.c2g__DerivePeriod__c = true;
        cNote.c2g__DeriveDueDate__c = true;
        cNote.JVCO_Credit_Reason__c = 'Invoice amendment'; 
        cNote.JVCO_Period_Start_Date__c = minStartDate;
        cNote.JVCO_Period_End_Date__c = maxEndDate;
        cNote.JVCO_Sales_Rep__c = accIdToSalesRepIdMap.get(a.Id);
        cNote.c2g__Invoice__c = sInvId;
        cNote.JVCO_Reference_Document__c = sInvName; 
        if(parent == 'COMPANY')
        {
            cNote.JVCO_VAT_Registration_Number__c = userCompany.c2g__Company__r.c2g__VATRegistrationNumber__c;
        }else
        {
            cNote.JVCO_VAT_Registration_Number__c = taxRegNumMap.get(parent);
            cNote.Parent__c = parent;
        }
        return cNote;
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Sales Invoice
        Inputs: Id, Id, Decimal, String
        Returns: c2g__codaInvoiceLineItem__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private c2g__codaInvoiceLineItem__c createSalesInvLineItem(Id sInvId, Id accId, Decimal amt, Decimal taxAmount, String type)
    {
        c2g__codaInvoiceLineItem__c sInvLine = new c2g__codaInvoiceLineItem__c();
        sInvLine.c2g__Invoice__c = sInvId;
        sInvLine.c2g__Dimension1__c = accMap.get(accId).c2g__CODADimension1__c;
        sInvLine.c2g__Dimension3__c = accMap.get(accId).c2g__CODADimension3__c;
        sInvLine.c2g__TaxCode1__c = accMap.get(accId).c2g__CODAOutputVATCode__c;
        sInvLine.c2g__DeriveLineNumber__c = true;
        sInvLine.c2g__DeriveUnitPriceFromProduct__c = false;
        sInvLine.c2g__CalculateTaxValue1FromRate__c = false;
        sInvLine.c2g__TaxValue1__c = taxAmount.setScale(2);
        sInvLine.c2g__Quantity__c = 1;
        sInvLine.c2g__UnitPrice__c = amt;
        sInvLine.c2g__Dimension2__c = dim2Map.get(type);
        sInvLine.c2g__Product__c = prodMap.get(type);
        return sInvLine;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Credit Note Line
        Inputs: Id, Id, Decimal, String
        Returns: c2g__codaCreditNoteLineItem__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private c2g__codaCreditNoteLineItem__c createCredNoteLineItem(Id cNoteId, Id accId, Decimal amt, Decimal taxAmount, String type)
    {
        c2g__codaCreditNoteLineItem__c cNoteLine = new c2g__codaCreditNoteLineItem__c();
        cNoteLine.c2g__CreditNote__c = cNoteId;
        cNoteLine.c2g__Dimension1__c = accMap.get(accId).c2g__CODADimension1__c;
        cNoteLine.c2g__Dimension3__c = accMap.get(accId).c2g__CODADimension3__c;
        cNoteLine.c2g__TaxCode1__c = accMap.get(accId).c2g__CODAOutputVATCode__c;
        cNoteLine.c2g__DeriveLineNumber__c = true;
        cNoteLine.c2g__DeriveUnitPriceFromProduct__c = false;
        cNoteLine.c2g__CalculateTaxValueFromRate__c = false;
        cNoteLine.c2g__TaxValue1__c = (taxAmount * -1).setScale(2);
        cNoteLine.c2g__Quantity__c = 1;
        cNoteLine.c2g__UnitPrice__c = amt * -1;
        cNoteLine.c2g__Dimension2__c = dim2Map.get(type);
        cNoteLine.c2g__Product__c = prodMap.get(type);
        return cNoteLine;
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Set Account Id to Payment Term To Aggregate Result Map
        Inputs: List<Account>
        Returns: Map<Id, AggregateResult>
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private Map<Id, Map<String, Map<String, AggregateResult>>> getAccIdToPaymentTermToAggregateResultMap(List<Account> accList)
    {
        Map<Id, Map<String, Map<String, AggregateResult>>> accIdToPaymentTermToAggregateResultMap = new Map<Id, Map<String, Map<String, AggregateResult>>>();
        for(AggregateResult ar : [SELECT blng__Account__c accId, 
                                  blng__Order__r.SBQQ__Quote__r.JVCO_Payment_Terms__c paymentTerm,
                                  blng__Order__r.SBQQ__Quote__r.JVCO_Quote_Id_to_Scheduler__c  credReason,
                                  SUM(JVCO_PRS_Total_Lines__c)prsTotalLines,
                                  SUM(JVCO_PPL_Total_Lines__c)pplTotalLines,
                                  SUM(JVCO_VPL_Total_Lines__c)vplTotalLines,
                                  SUM(JVCO_PRS_Total_Amount__c)prsTotalAmt,
                                  SUM(JVCO_PPL_Total_Amount__c)pplTotalAmt,
                                  SUM(JVCO_VPL_Total_Amount__c)vplTotalAmt,
                                  SUM(JVCO_PRS_Tax_Total_Amount__c)prsTotalTaxAmt,
                                  SUM(JVCO_PPL_Tax_Total_Amount__c)pplTotalTaxAmt,
                                  SUM(JVCO_VPL_Tax_Total_Amount__c)vplTotalTaxAmt,
                                  SUM(JVCO_Total_Amount__c)totalAmt,
                                  MIN(JVCO_Licence_Start_Date__c)minStartDate,
                                  MAX(JVCO_Licence_End_Date__c)maxEndDate,
                                  SUM(JVCO_Total_Surcharge_Amount__c)totalSurAmount,
                                  SUM(JVCO_Surcharge_Tariffs__c)totalSurTariffs,
                                  COUNT(blng__Order__r.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c)migratedContractCounter,
                                  COUNT_DISTINCT(blng__Order__r.SBQQ__Quote__r.SBQQ__MasterContract__r.SBQQ__Quote__r.JVCO_Sales_Invoice__c)origSInvCounter,
                                  MAX(blng__Order__r.SBQQ__Quote__r.SBQQ__MasterContract__r.SBQQ__Quote__r.JVCO_Sales_Invoice__c)origSInvId,
                                  MAX(blng__Order__r.SBQQ__Quote__r.SBQQ__MasterContract__r.SBQQ__Quote__r.JVCO_Sales_Invoice__r.Name)origSInvName,
                                  MAX(blng__Order__r.SBQQ__Quote__r.JVCO_Re_issued__c)ifReissuedInvoice,
                                  COUNT(Id)totalNumber
                                  FROM blng__Invoice__c
                                  WHERE blng__InvoiceRunCreatedBy__c IN : invRunIdSet
                                  AND blng__Account__c IN :accList
                                  GROUP BY blng__Account__c, blng__Order__r.SBQQ__Quote__r.JVCO_Payment_Terms__c,
                                  blng__Order__r.SBQQ__Quote__r.JVCO_Quote_Id_to_Scheduler__c])
        {
            //Check if accId is not in accIdToPaymentTermToAggregateResultMap then add empty paymentterm to aggregateresult map
            if(!accIdToPaymentTermToAggregateResultMap.containsKey((Id)ar.get('accId')))
            {
                accIdToPaymentTermToAggregateResultMap.put((Id)ar.get('accId'), new Map<String, Map<String, AggregateResult>>());
            }
            if(!accIdToPaymentTermToAggregateResultMap.get((Id)ar.get('accId')).containskey((String)ar.get('paymentTerm')))
            {
                accIdToPaymentTermToAggregateResultMap.get((Id)ar.get('accId')).put((String)ar.get('paymentTerm'), new Map<String, AggregateResult>());
            }
            accIdToPaymentTermToAggregateResultMap.get((Id)ar.get('accId')).get((String)ar.get('paymentTerm')).put((String)ar.get('credReason'), ar);
        }
        return accIdToPaymentTermToAggregateResultMap;
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Dimension2 and Product Mapping
        Inputs: 
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void setDim2AndProdMapping()
    {
        if(dim2Map == null)
        {
            dim2Map = new Map<String, Id>();
            for(c2g__codaDimension2__c dim2 : [SELECT Name, Id FROM c2g__codaDimension2__c
                                                WHERE Name IN ('PRS', 'PPL', 'VPL')])
            {
                dim2Map.put(dim2.Name, dim2.Id);
            }
        }
        
        if(prodMap == null)
        {
            prodMap = new Map<String, Id>();
            for(Product2 prod : [SELECT Id, Name FROM Product2 
                                    WHERE Name IN ('PPL Product', 'PRS Product', 'VPL Product')])
            {
                prodMap.put(prod.Name.substring(0,3), prod.Id);
            }
        }   
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup User Company
        Inputs: 
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void setCompanyVATRegistrationNum()
    {
        if(userCompany == null)
        {
            userCompany = [SELECT c2g__Company__c, c2g__Company__r.c2g__VATRegistrationNumber__c, 
                            c2g__User__c 
                            FROM c2g__codaUserCompany__c 
                            WHERE c2g__User__c =: UserInfo.getUserId()
                            LIMIT 1];
        }    
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Supplier Tax Registration Number Map
        Inputs: 
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void constructSupplierRecordTaxRegistrationNumber()
    {
        if(taxRegNumMap == null)
        {
            taxRegNumMap = new Map<String, String>();                        
            for(Account a : [SELECT Id, Name, c2g__CODAVATRegistrationNumber__c
                            FROM Account 
                            WHERE Name IN ('PPL','PRS','VPL') AND RecordType.Name ='Supplier'])
            {
                taxRegNumMap.put(a.Name, a.c2g__CODAVATRegistrationNumber__c);
            }
        }   
    }
    
    /* ----------------------------------------------------------------------------------------------------------
        Author:         joseph.g.barrameda
        Company:        Accenture
        Description:    This method is used to create records that will be used for mapping and will be deleted in JVCO_LinkBlngLineToSInvAndCnoteLineBatch 
        Inputs:         None
        Returns:        None
        <Date>          <Authors Name>             <Brief Description of Change> 
        26-Jul-2018     joseph.g.barrameda         GREEN-32865 - Initial version of the code
    ----------------------------------------------------------------------------------------------------------- */
    private void createTmpInvoiceProcessingDataRecords()
    {
        List<JVCO_TempInvoiceProcessingData__c> tmpInvoiceProcDataList = new List<JVCO_TempInvoiceProcessingData__c>();
        for(Id docId: bInvIdToDocHeaderIdMap.keyset())
        {
            JVCO_TempInvoiceProcessingData__c  tmpInvProcData = new JVCO_TempInvoiceProcessingData__c();
            tmpInvProcData.JVCO_Batch_Name__c = 'JVCO_InvoiceAndCreditNoteGenerationBatch';                   
            tmpInvProcData.JVCO_Id1__c = docId; 
            tmpInvProcData.JVCO_Id2__c = bInvIdToDocHeaderIdMap.get(docId);             
            tmpInvProcData.JVCO_Job_Identifier__c = new List<Id>(invRunIdSet)[0];           
            tmpInvProcData.JVCO_Type__c  = 'bInvIdToDocHeaderIdMap';
            tmpInvoiceProcDataList.add(tmpInvProcData);
        }

        insert tmpInvoiceProcDataList;
    }

    public void finish(Database.BatchableContext bc)
    {
        Map<Id,Id> finalBlngInvIdMap = new Map<Id,Id>();
        for(JVCO_TempInvoiceProcessingData__c tmp: [SELECT JVCO_Id1__c, JVCO_Id2__c
                                                    FROM JVCO_TempInvoiceProcessingData__c
                                                    WHERE JVCO_Job_Identifier__c IN :invRunIdSet
                                                    AND JVCO_Batch_Name__c IN ('JVCO_InvoiceAndCreditNoteGenerationBatch')])
        {
            finalBlngInvIdMap.put(tmp.JVCO_Id1__c, tmp.JVCO_Id2__c);
        }
        
        Decimal BILLINGINVOICESCOPE = JVCO_DocumentGenerationWrapper.generalSettings.JVCO_Link_Billing_Line_Scope__c != null ? 
                                        JVCO_DocumentGenerationWrapper.generalSettings.JVCO_Link_Billing_Line_Scope__c : 1;
        JVCO_LinkBlngLineToSInvAndCnoteLineBatch batch = new JVCO_LinkBlngLineToSInvAndCnoteLineBatch(finalBlngInvIdMap, new List<Id>(invRunIdSet)[0]);
        batch.stopMatching = true;
        Id jobId = Database.executeBatch(batch, Integer.valueOf(BILLINGINVOICESCOPE));
    }

}