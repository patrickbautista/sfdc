/* ----------------------------------------------------------------------------------------------
Author: Recuerdo Bregente
Company: Accenture
Description: Test Class for JVCO_InvokeDupeElimMergeVenueAffExt
<Date>      <Authors Name>       <Brief Description of Change> 
18-Aug-2017 Recuerdo Bregente  Initial version
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_InvokeDupeElimMergeVenueAffExtTest {
  
  @testSetup static void setUpTestData(){
        
    Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='JVCO_Venue__c'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q2);
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }

        JVCO_Venue__c venue = new   JVCO_Venue__c();
        venue.Name = 'update venue';
        venue.JVCO_Field_Visit_Requested__c = false;
        venue.JVCO_Street__c = 'testStreet';
        venue.JVCO_City__c = 'testCoty';
        venue.JVCO_Postcode__c ='TN32 4SL';
        venue.JVCO_Country__c ='UK';

        insert venue;
        
        //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
        testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc.Dimension_1_Required__c = false;
        testGeneralLedgerAcc.Dimension_2_Required__c = true;
        testGeneralLedgerAcc.Dimension_3_Required__c = false;
        testGeneralLedgerAcc.Dimension_4_Required__c = false;
        testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
        testGeneralLedgerAcc.ownerid = testGroup.Id;
        testGeneralLedgerAcc.c2g__ReportingCode__c = '30030';
        insert testGeneralLedgerAcc;
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedgerAcc.id);
        insert taxCode;

        //Create Account
        Account testAccCust = new Account(Name='Test Customer Account');
        testAccCust.AccountNumber = '999999';
        testAccCust.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust.Type='Customer';
        testAccCust.RecordTypeId = accountRecordTypes.get('Customer Account');
        insert testAccCust;

         Contact c = JVCO_TestClassObjectBuilder.createContact(testAccCust.id);
        insert c;
        
        Account testAcc = new Account(Name='Test Account');
        testAcc.AccountNumber = '987654';
        testAcc.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAcc.c2g__CODAAllowDeleteInUse__c = false;
        testAcc.c2g__CODACreditLimitEnabled__c = false;
        testAcc.c2g__CODACreditStatus__c = 'Credit Allowed';
        testAcc.c2g__CODADaysOffset1__c = 0.0;
        testAcc.c2g__CODADiscount1__c = 0.0;
        testAcc.c2g__CODAFederallyReportable1099__c = false;
        testAcc.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testAcc.c2g__CODAIntercompanyAccount__c = false;
        testAcc.c2g__CODASalesTaxStatus__c = 'Taxable';
        testAcc.JVCO_Customer_Account__c = testAccCust.id;
        testAcc.RecordTypeId = accountRecordTypes.get('Licence Account');
        testAcc.c2g__CODAOutputVATCode__c = taxCode.id;
        testAcc.JVCO_Preferred_Contact_Method__c = 'Email';
        testAcc.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        testAcc.JVCO_Billing_Contact__c = c.id;
        testAcc.JVCO_Licence_Holder__c = c.id;
        testAcc.JVCO_Review_Contact__c = c.id;
        insert testAcc;
        
        JVCO_Affiliation__c aff = new JVCO_Affiliation__c();
        aff.JVCO_Account__c = testAcc.Id;
        aff.JVCO_Venue__c = venue.Id;
        aff.JVCO_Start_Date__c = date.today() + 1;
        insert aff;
    }
  
  /* ----------------------------------------------------------------------------------------------
    Author: Recuerdo Bregente
    Company: Accenture
    Description: Test JVCO_InvokeDupeElimMergeProcessVenueExt.redirectVenueToDupeElimPage
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>       <Brief Description of Change> 
    18-Aug-2017 Recuerdo Bregente  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    @isTest static void testRedirectVenueToDupeElimPage()
    {

      List<JVCO_Affiliation__c> affList = [SELECT Id, Name FROM JVCO_Affiliation__c order by CreatedDate DESC limit 1];

        Test.startTest();

        ApexPages.StandardSetController con = 
                           new ApexPages.StandardSetController(affList);
            
        con.setSelected(affList);

        JVCO_InvokeDupeElimMergeVenueAffExt dupeElimMergeController = new JVCO_InvokeDupeElimMergeVenueAffExt(con);
        PageReference pageRef = dupeElimMergeController.redirectVenueToDupeElimPage();
        system.assert(pageRef != null);

        Test.stopTest();

        PageReference pageRef2 = dupeElimMergeController.redirectVenueaffiliationToDupeElimPage();
    }
}