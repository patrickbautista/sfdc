global class JVCO_TransOfAccTypeScheduler implements Schedulable {

	global void execute(SchedulableContext sc) {
		Integer toProcess = [SELECT id FROM Account WHERE RecordType.Name = 'Customer Account' and JVCO_Target_Account_Type__c!=null].size();
		JVCO_TransformationOfAccountTypeBatch bc = new JVCO_TransformationOfAccountTypeBatch(null, toProcess);
		Database.executeBatch(bc, 1);
	}
}