/* -----------------------------------------------------------------------------------------------
   Name: JVCO_RemoveFromCaseLogicTest.cls 
   Description: Business logic class for removing cash entry lines from Case

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   --------------------------------------------------
   05-Feb-2021   0.1         Luke.Walker            Intial creation
   ----------------------------------------------------------------------------------------------- */

@isTest
public class JVCO_RemoveFromCaseLogicTest {
    
    @testSetup static void setupTestData()
    {
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        
        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim2('PRS');
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        c2g__codaPeriod__c period = JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert period;
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;
        
        //Setup SLA Custom Setting
        JVCO_SLA_Constants__c slaConstants = new JVCO_SLA_Constants__c();
        slaConstants.Name = 'JVCO_SLA_Custom_Settings';
        slaConstants.JVCO_Case_Origins_with_Entitlement__c = 'Email,Webform,Live Chat,Request Callback,Inbound Call,White Mail';
        slaConstants.JVCO_Entitlement_Name__c = 'Customer Service Support';
        slaConstants.JVCO_First_Milestone__c = 'First Response';
        slaConstants.JVCO_First_Milestone_Criteria__c = 'Responded';
        slaConstants.JVCO_First_Milestone_Origin__c = 'Email,Webform,Request Callback';
        slaConstants.JVCO_Second_Milestone__c = 'Resolution Time';
        slaConstants.JVCO_Second_Milestone_Criteria__c = 'Closed';
        slaConstants.JVCO_Second_Milestone_Origin__c = 'Email,Webform,Live Chat,Request Callback,Inbound Call,White Mail';
        insert slaConstants;
        
        //Create Entitlement Account
        Account entitlementAccount = new Account();
        entitlementAccount.Name = 'Entitlement Account';
        insert entitlementAccount;
        
        //Create Entitlement
        Entitlement entitle = new Entitlement();
        entitle.Name = 'Customer Service Support';
        entitle.Type = 'Phone Support';
        entitle.AccountId = entitlementAccount.id;
        entitle.StartDate = system.today()-30;
        entitle.EndDate = system.today()+30;
        insert entitle;

        //Insert Write Off Case
        Case writeOffCase = new Case();
        writeOffCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Write Off').getRecordTypeId();
        writeOffCase.Origin = 'Iternal';
        writeOffCase.Type = 'Write Off';
        writeOffCase.Status = 'New';
        writeOffCase.AccountId = custAcc.id;
        insert writeOffCase;
    
        //Insert Cash Entry
        c2g__codaCashEntry__c cashEntry = new c2g__codaCashEntry__c();
        cashEntry.c2g__Date__c = System.today();
        cashEntry.c2g__Account__c = custAcc.id;
        cashEntry.c2g__PaymentMethod__c = 'Cheque';
        cashEntry.c2g__BankAccount__c = bAcc.id;
        cashEntry.c2g__Type__c = 'Receipt';
        cashEntry.c2g__CashEntryCurrency__c = accCurrency.id;
        cashEntry.ffcash__DerivePeriod__c = true;
        cashEntry.c2g__OwnerCompany__c = comp.id;
        cashEntry.c2g__Status__c = 'In Progress';
        insert cashEntry;
        
        //Insert Cash Entry Line
        c2g__codaCashEntryLineItem__c cashEntryLine = new c2g__codaCashEntryLineItem__c();
        cashEntryLine.c2g__Account__c = custAcc.id;
        cashEntryLine.c2g__CashEntry__c = cashEntry.id;
        cashEntryLine.c2g__OwnerCompany__c = comp.id;
        cashEntryLine.c2g__CashEntryValue__c = 1000;
        cashEntryLine.JVCO_Case_ID__c = writeOffCase.id;
        cashEntryLine.ffcash__DeriveLineNumber__c = true;
        insert cashEntryLine;

    }
  
    @isTest
    static void removeCashLines(){
            List<c2g__codaCashEntryLineItem__c> cashList = [SELECT ID FROM c2g__codaCashEntryLineItem__c];
        test.startTest();
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(cashList);
            sc.setSelected(cashList);
            JVCO_RemoveFromCaseLogic caseLogic = new JVCO_RemoveFromCaseLogic(sc);
            caseLogic.remove();
        test.stopTest();
        
        List<c2g__codaCashEntryLineItem__c> cashLines = [SELECT ID, JVCO_Case_ID__c FROM c2g__codaCashEntryLineItem__c WHERE JVCO_Case_ID__c != NULL];
        system.assertEquals(0, cashLines.size());
    }
       
    @isTest
    static void selectNoLines(){
        List<c2g__codaCashEntryLineItem__c> cashList = [SELECT ID FROM c2g__codaCashEntryLineItem__c];
        test.startTest();
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(cashList);
            JVCO_RemoveFromCaseLogic caseLogic = new JVCO_RemoveFromCaseLogic(sc);
            caseLogic.remove();
        test.stopTest();

        List<c2g__codaCashEntryLineItem__c> cashLines = [SELECT ID, JVCO_Case_ID__c FROM c2g__codaCashEntryLineItem__c WHERE JVCO_Case_ID__c != NULL];
        system.assertNotEquals(0,cashLines.size());
    }
    
    @isTest
    static void returnToCase(){
        List<c2g__codaCashEntryLineItem__c> cashList = [SELECT ID FROM c2g__codaCashEntryLineItem__c];
        test.startTest();
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(cashList);
            sc.setSelected(cashList);
            JVCO_RemoveFromCaseLogic caseLogic = new JVCO_RemoveFromCaseLogic(sc);
            caseLogic.returnToCase();
        test.stopTest();

        List<c2g__codaCashEntryLineItem__c> cashLines = [SELECT ID, JVCO_Case_ID__c FROM c2g__codaCashEntryLineItem__c WHERE JVCO_Case_ID__c != NULL];
        system.assertNotEquals(0,cashLines.size());
    }

}