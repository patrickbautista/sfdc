/**
* Created by: Accenture.
* Created Date: 05.10.2017
* Description: Test class for JVCO_ResendDocumentBundleCtrl class 
**/
@isTest
public class JVCO_ResendDocumentBundleCtrlTest
{
    public static JVCO_Document_Queue__c  docQueueObj;
    
    public static void setupTestData() 
    {
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
            listQueue.add(q2);
            queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
            listQueue.add(q3);
            queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
            listQueue.add(q4);
            queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
            listQueue.add(q5);
            queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q7);
            queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransaction__c'); 
            listQueue.add(q8);
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }
              
                   //record type Account
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        
        c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
        testCompany.c2g__CashMatchingCurrencyMode__c = 'Account';
        testCompany.c2g__Country__c = 'GB';
        testCompany.c2g__ECCountryCode__c = 'GB';
        testCompany.c2g__UnitOfWork__c = 1.0;
        testCompany.Name = 'Test Company';
        testCompany.ownerid = testGroup.Id;
        insert testCompany;
        
        c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c();
        userCompany.c2g__Company__c = testCompany.id;
        userCompany.c2g__User__c = userInfo.getUserId();
        userCompany.c2g__ExternalId__c = 'ABCDE1234567876';
        userCompany.c2g__UnitOfWork__c = 111 ;
        insert userCompany;
        
        c2g__codaYear__c yr= new c2g__codaYear__c();
        yr.Name ='2016';
        yr.c2g__AutomaticPeriodList__c =  true;
        yr.c2g__OwnerCompany__c = testCompany.id;
        yr.c2g__ExternalId__c = 'yzsd1234';
        yr.c2g__NumberOfPeriods__c =11;
        yr.c2g__StartDate__c =  system.today() - 10;
        yr.c2g__Status__c = 'Open';
        yr.c2g__PeriodCalculationBasis__c = '445';
        yr.c2g__YearEndMode__c = 'Full Accounting Code' ; 
        yr.c2g__UnitOfWork__c = 12;
        yr.ownerid = testGroup.Id;
        insert yr;
        
        c2g__codaPeriod__c testPeriod = new c2g__codaPeriod__c();
        testPeriod.c2g__OwnerCompany__c = testCompany.id;
        testPeriod.c2g__StartDate__c = System.today()-10;
        testPeriod.c2g__EndDate__c = System.today()+10;
        testPeriod.c2g__PeriodNumber__c ='123';
        testPeriod.c2g__Description__c ='test Desc';
        testPeriod.c2g__PeriodGroup__c = 'Q1';
        testPeriod.c2g__PeriodNumber__c = '1';
        testPeriod.c2g__YearName__c = yr.id;
        insert testPeriod;
        
        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc10040 = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc10040.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc10040.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc10040.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc10040.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc10040.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc10040.c2g__ReportingCode__c = '10040';
        testGeneralLedgerAcc10040.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc10040.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc10040.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc10040.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc10040.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc10040.Dimension_1_Required__c = false;
        testGeneralLedgerAcc10040.Dimension_2_Required__c = false;
        testGeneralLedgerAcc10040.Dimension_3_Required__c = false;
        testGeneralLedgerAcc10040.Dimension_4_Required__c = false;
        testGeneralLedgerAcc10040.Name = '10040 - Accounts receivables control';
        insert testGeneralLedgerAcc10040;
        c2g__codaGeneralLedgerAccount__c testGeneralLedger = JVCO_TestClassObjectBuilder.createGeneralLedger();
        insert testGeneralLedger;

        c2g__codaTaxCode__c testTaxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedger.Id);
        insert testTaxCode;
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 83202-C';
        a1.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc10040.id;
        a1.Type = 'Key Account';
        insert a1;

        //Create Contact
        Contact testContact = JVCO_TestClassObjectBuilder.createContact(a1.Id);
        testContact.Firstname = 'Test';
        testContact.LastName = 'TestContact';
        insert testContact;
        
        Account a2 = new Account();
        a2.RecordTypeId = licenceRT;
        a2.Name = 'Test Account 83202-License';

        a2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
        a2.JVCO_Customer_Account__c = a1.id;
        a2.c2g__CODAOutputVATCode__c = testTaxCode.id;
        a2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc10040.id;
        //a2.JVCO_Customer_Account__c = a1.Id;
        a2.JVCO_Preferred_Contact_Method__c = 'Email';
        a2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        a2.JVCO_Billing_Contact__c = testContact.id;
        a2.JVCO_Licence_Holder__c = testContact.id;
        a2.JVCO_Review_Contact__c = testContact.id;
        insert a2;
        
        c2g__codaAccountingCurrency__c accCurrency = new c2g__codaAccountingCurrency__c();
        accCurrency.c2g__OwnerCompany__c = testCompany.id;
        accCurrency.c2g__DecimalPlaces__c = 2;
        accCurrency.Name = 'GBP';
        accCurrency.c2g__Dual__c = true ;
        accCurrency.ownerid = testGroup.Id;
        accCurrency.c2g__Home__c = true;
        accCurrency.c2g__UnitOfWork__c = 2.0;
        insert accCurrency;
        
        Account testLicAccount = [select id from Account where RecordTypeId = :licenceRT limit 1];
        Account testCusAccount = [select id from Account where RecordTypeId = :customerRT limit 1];
        testContact = [SELECT ID FROM Contact limit 1];
        
        Product2 testProduct = JVCO_TestClassObjectBuilder.createProduct();
        testProduct.c2g__CODASalesRevenueAccount__c = testGeneralLedgerAcc10040.id;
        insert testProduct;

        PriceBookEntry testPBE = new PriceBookEntry();
        testPBE.Pricebook2Id = test.getStandardPriceBookId();
        testPBE.product2id = testProduct.id;
        testPBE.IsActive = true;
        testPBE.UnitPrice = 100.0;
        insert testPBE;
        
        //Create Sales Invoice
        c2g__codaInvoice__c testInvoice = new c2g__codaInvoice__c();
        testInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testInvoice.c2g__InvoiceStatus__c = 'In Progress';
        testInvoice.c2g__InvoiceDate__c = date.today().addDays(-7);
        testInvoice.c2g__DueDate__c = date.today().addMonths(7);
        testInvoice.c2g__Account__c = testLicAccount.Id;
        testInvoice.JVCO_Customer_Type__c = 'New Business';
        testInvoice.c2g__OwnerCompany__c = testCompany.id;
        testInvoice.JVCO_Generate_Payment_Schedule__c = false;
        insert testInvoice;
        
        c2g__codaInvoiceLineItem__c testInvoiceItem = new c2g__codaInvoiceLineItem__c();
        testInvoiceItem.c2g__Product__c = testProduct.id;
        testInvoiceItem.c2g__OwnerCompany__c = testCompany.id;
        testInvoiceItem.c2g__Invoice__c = testInvoice.id;
        testInvoiceItem.c2g__Quantity__c = 1.0;
        testInvoiceItem.c2g__UnitPrice__c = 200.0;
        insert testInvoiceItem;
        
        Test.startTest();
        //system.assertEquals(null, [Select Id,Name from APXTConga4__Conga_Solution__c ]);
        c2g.CODAAPICommon_10_0.Context contextInvoice = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference invoiceRef = c2g.CODAAPICommon.getRef(testInvoice.Id, null); 
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(contextInvoice, invoiceRef);
        
        JVCO_Document_Queue__c testDocument = new JVCO_Document_Queue__c();
        testDocument.JVCO_Recipient__c = testContact.id;
        testDocument.JVCO_Related_Account__c = testLicAccount.id;
        testDocument.JVCO_Related_Customer_Account__c = testCusAccount.id;
        testDocument.JVCO_Scenario__c = 'Manual Review Notification Transition';
        testDocument.JVCO_Transmission__c = 'Letter';
        testDocument.JVCO_Related_SalesInvoice__c = testInvoice.id;
        insert TestDocument;
        docQueueObj = TestDocument;


    }
    public static testMethod void testController()
    {
        setupTestData();
        PageReference pageRef = Page.JVCO_ResendDocumentBundle;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('cloneID', String.valueOf(docQueueObj.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(docQueueObj);
        JVCO_ResendDocumentBundleCtrl ctrlObj  = new JVCO_ResendDocumentBundleCtrl(sc);
        ctrlObj.saveRecord();
    }
}