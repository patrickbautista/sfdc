/* --------------------------------------------------------------------------------------------------
    Name:            JVCO_RegenerateStuckDocQueSched.cls 
    Description:     Class that handles JVCO_RegenerateStuckDocQueBatch
    Test class:      

    Date             Version     Author                  Summary of Changes 
    -----------      -------     -------------------     --------------------
    21-Jan-2019       0.1        hazel.jurna.m.limot      Intial draft
------------------------------------------------------------------------------------------------ */
public class JVCO_RegenerateStuckDocQueSched implements Schedulable{
    
    public void execute(SchedulableContext sc)
    { 
        Database.executeBatch(new JVCO_RegenerateStuckDocQueBatch(), 1);
    }

    private static String getSchedulerExpression(DateTime dt) 
    {
         return '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();    
    }

    public static void callExecuteMomentarily()  
    {
        System.schedule('JVCO_RegenerateStuckDocQueSched', getSchedulerExpression(DateTime.Now().addSeconds(5)), new JVCO_RegenerateStuckDocQueSched());
    }
}