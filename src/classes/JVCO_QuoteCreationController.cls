/* ----------------------------------------------------------------------------------------------
   Name: JVCO_QuoteCreationController.cls 
   Description: Controller class for Quote Creation

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
   22-Mar-2017   0.1         kristoffer.d.martin Intial creation
----------------------------------------------------------------------------------------------- */
public with sharing class JVCO_QuoteCreationController {

    private ApexPages.StandardSetController standardController;
    @TestVisible private String uiThemeDisplayed;

    public JVCO_QuoteCreationController(ApexPages.StandardSetController standardController) 
    {
        this.standardController = standardController;  
        JVCO_QuoteCreationController.getPriceBooks();
        uiThemeDisplayed = UserInfo.getUiThemeDisplayed();
    }


    public static Map<ID,Pricebook2> pbIdToPriceBookWithPbeMap;

    public static void getPriceBooks(){
        JVCO_QuoteCreationController.pbIdToPriceBookWithPbeMap = new map<ID,Pricebook2> ([select id, name
      from Pricebook2 where isStandard = true and isActive = true]);
    }

    /* ----------------------------------------------------------------------------------------------------------
    Author:         kristoffer.d.martin
    Company:        Accenture
    Description:    Quote Creation
    Inputs:         n/a
    Returns:        n/a
    <Date>          <Authors Name>                      <Brief Description of Change> 
    23-Mar-2017     kristoffer.d.martin                 Initial version of the code
    06-Apr-2017     robert.j.b.lacatan                  Updated line 42
    18-Apr-2017     mel.andrei.b.santos                 Updated Line 59, changed value to false
    18-Apr-2017     mel.andrei.b.santos                 Updated Line 59, changed value back to true
    19-Apr-2017     ma.princess.b.nacepo                Added line to autopopulate Customer Account
    24-Apr-2017     ma.princess.b.nacepo                Removed  line to autopopulate Customer Account. Transferred to JVCO_QuoteTriggerHandler
    17-Aug-2017     r.p.valencia.iii                    Added In Enforcement and In Infringement restriction
    ----------------------------------------------------------------------------------------------------------*/   
    public PageReference quoteCreate() 
    {

        String recordId = ApexPages.CurrentPage().getparameters().get('id');

        Account acc = [SELECT Id, Name, JVCO_Billing_Contact__c, 
                        //r.p.valencia.iii - 16Aug2017 - BEGIN
                        JVCO_In_Enforcement__c, JVCO_In_Infringement__c
                        //r.p.valencia.iii - 16Aug2017 - END
                        FROM Account WHERE Id =: recordId];
        //r.p.valencia.iii - 16Aug2017 - if condition for in infringement and in enforcement restriction - BEGIN
        if((acc.JVCO_In_Infringement__c || acc.JVCO_In_Enforcement__c) && checkUserIfRestricted() ){ //GREEN-31226 added "&& checkUserIfRestricted()"
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'This Licence Account should not be quoted while in Enforcement/Infringement'));
            return null;
        }
        //r.p.valencia.iii - 16Aug2017 - if condition for in infringement and in enforcement restriction - END

        else{

            //Create Savepoint
            Savepoint sp = Database.setSavepoint();  

            try {

                Opportunity opportunityObj = new Opportunity();
                opportunityObj.Name = acc.Name;
                opportunityObj.AccountId = acc.Id;
                opportunityObj.StageName = 'Quoting';
                opportunityObj.CloseDate = Date.today();
                opportunityObj.PriceBook2Id = JVCO_QuoteCreationController.pbIdToPriceBookWithPbeMap.values().size()>0 ?JVCO_QuoteCreationController.pbIdToPriceBookWithPbeMap.values().get(0).Id:null;
                insert opportunityObj;

                SBQQ__Quote__c quoteRecord = new SBQQ__Quote__c();
                quoteRecord.SBQQ__Account__c = acc.Id;
                quoteRecord.SBQQ__Opportunity2__c = opportunityObj.Id;
                if (acc.JVCO_Billing_Contact__c != null) 
                {
                    quoteRecord.SBQQ__PrimaryContact__c = acc.JVCO_Billing_Contact__c;
                }
                quoteRecord.SBQQ__QuoteProcessId__c = '';
                quoteRecord.SBQQ__Status__c = 'Draft';
                quoteRecord.SBQQ__Type__c = 'Quote';
                quoteRecord.SBQQ__Primary__c = true;
                quoteRecord.SBQQ__LineItemsGrouped__c = true;
                quoteRecord.SBQQ__LineItemsPrinted__c = true;
                insert quoteRecord;
                //Redirect Page           
                //Pagereference pr = new pagereference('https://sbqq' + ApexPages.currentPage().getHeaders().get('Host').substring(1) + '/apex/sb?id=' + quoteRecord.Id + '&scontrolCaching=1#quote/le?qId=' + quoteRecord.Id);
                //Pagereference pr = new pagereference(ApexPages.currentPage().getHeaders().get('Host').substring(1) + '/apex/sb?id=' + quoteRecord.Id + '&scontrolCaching=1#quote/le?qId=' + quoteRecord.Id);  
                
                PageReference pr = Page.SBQQ__sb;

                pr.getParameters().put('id', quoteRecord.Id);
                pr.getParameters().put('qId', quoteRecord.Id);
                pr.getParameters().put('retUrl',quoteRecord.Id);
                system.debug('PR'+pr);
               //Create URL for Lightning or Classic
                if(uiThemeDisplayed == 'Theme4d' || uiThemeDisplayed == 'Theme4u'){
                	aura.redirect(pr);
                	return pr;
                } else {
                    pr.setRedirect(true);    
                    return pr;
                }

            } catch (Exception ex) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
                Database.rollback(sp);        
                return null;    
            }
        }
        
    } 

    /******************************************************************************************************
    Author:         ranielle.j.c.tabora
    Company:        Accenture
    Description:    Checks if the user has JVCO_Enforcement Advisor perofile 
    <Date>          <Authors Name>                      <Brief Description of Change> 
    13-Apr-2018     ranielle.j.c.tabora                 Added checkUserIfRestricted method //GREEN-31226
    *******************************************************************************************************/
   
    
    public static Boolean checkUserIfRestricted(){
        String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
        Map<Id,PermissionSet> enAdPermSet = new Map<Id,PermissionSet>([SELECT Id FROM PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors']);
        List<PermissionSetAssignment> permissionSetList = [SELECT Id FROM PermissionSetAssignment WHERE 
                                                            AssigneeId = :UserInfo.getUserId() AND PermissionSetId IN :enAdPermSet.keySet()];
                                                            
        return (profileName <> 'System Administrator' && permissionSetList.isEmpty());
    }

}