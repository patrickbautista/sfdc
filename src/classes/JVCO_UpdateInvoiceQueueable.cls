public class JVCO_UpdateInvoiceQueueable implements Queueable
{
    private final JVCO_General_Settings__c generalSettings = JVCO_General_Settings__c.getInstance();
    private final Decimal BATCHRECORDLIMIT = generalSettings.JVCO_UpdateInvoiceQueueableLimit__c;
    private Map<Id, Order> updatedOrderMap;
    private Map<Id, OrderItem> updatedOrderItemMap;
    private Map<Integer, List<Order>> batchNumberToBInvListMap;
    private Integer batchNumber = 1;

    public JVCO_UpdateInvoiceQueueable(Map<Id, Order> updatedOrderMap, Map<Id, OrderItem> updatedOrderItemMap)
    {
        this.updatedOrderMap = updatedOrderMap;
        this.updatedOrderItemMap = updatedOrderItemMap;
        batchNumberToBInvListMap = new Map<Integer, List<Order>>();
    }

    public JVCO_UpdateInvoiceQueueable(Map<Integer, List<Order>> batchNumberToBInvListMap, Integer batchNumber, Map<Id, OrderItem> updatedOrderItemMap)
    {
        this.batchNumberToBInvListMap = batchNumberToBInvListMap;
        this.batchNumber = batchNumber;
        this.updatedOrderItemMap = updatedOrderItemMap;
    }

    public void execute(QueueableContext context)
    {
        if(updatedOrderMap != null)
        {
            Integer batchNumberCounter = 1;
            Integer recordSize = 0;
            for(Order ord : updatedOrderMap.values())
            {
                if(recordSize == BATCHRECORDLIMIT)
                {
                    batchNumberCounter++;
                    recordSize = 0;
                }

                recordSize++;
                if(!batchNumberToBInvListMap.containsKey(batchNumberCounter))
                {
                    batchNumberToBInvListMap.put(batchNumberCounter, new List<Order>());
                }
                batchNumberToBInvListMap.get(batchNumberCounter).add(ord);
            }
        }
        
        update batchNumberToBInvListMap.get(batchNumber);
        if(++batchNumber <= batchNumberToBInvListMap.size())
        {
            System.enqueueJob(new JVCO_UpdateInvoiceQueueable(batchNumberToBInvListMap, batchNumber, updatedOrderItemMap));
        }else
        {
            if(!Test.isRunningTest())
            {
                System.enqueueJob(new JVCO_UpdateInvoiceLineQueueable(updatedOrderItemMap));
            }
        }
    }
}