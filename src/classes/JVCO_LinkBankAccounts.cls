/* ----------------------------------------------------------------------------------------------
Name: JVCO_LinkBankAccounts.cls 
Description: Batch class that handles Bank Accounts update
             the account reference             
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
11-Apr-2017  0.1         kristoffer.d.martin   Intial creation
----------------------------------------------------------------------------------------------- */
global class JVCO_LinkBankAccounts implements Database.Batchable<sObject> {
	
	private static Map<String, ID> subscriptionIdMap;
    private static List<PAYBASE2__Payment__c> paymentListForUpdate;
    private static List<PAYBASE2__Payment__c> paymentList;

    private static void init(){

    	paymentListForUpdate = new List<PAYBASE2__Payment__c>();

        for(PAYBASE2__Payment__c payment : paymentList)
        {
        	if (payment.PAYREC2__Payment_Agreement__r.PAYFISH3__Current_Bank_Account__c != null)
        	{
        		payment.PAYFISH3__Payee_Bank_Account__c = payment.PAYREC2__Payment_Agreement__r.PAYFISH3__Current_Bank_Account__c;
        		paymentListForUpdate.add(payment);
        	}
        }
        update paymentListForUpdate;
        
    }

    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id, Name, PAYFISH3__Payee_Bank_Account__c, PAYFISH3__Originator_Bank_Account__c,
        										PAYREC2__Payment_Agreement__c, PAYREC2__Payment_Agreement__r.PAYFISH3__Current_Bank_Account__c
        								 FROM PAYBASE2__Payment__c]);

    }

    global void execute(Database.BatchableContext bc, List<PAYBASE2__Payment__c> scope)
    {
        paymentList = scope;
        init();
                
    }

    global void finish(Database.BatchableContext BC) 
    {
        
        System.debug('DONE');
    
    }
	
}