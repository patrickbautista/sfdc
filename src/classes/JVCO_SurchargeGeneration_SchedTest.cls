@isTest
private class JVCO_SurchargeGeneration_SchedTest
{
    @isTest
    static void surChargeGenSchedTest()
    {
        List<JVCO_TransitionManualReviewNotifSettings__c> settings = new List<JVCO_TransitionManualReviewNotifSettings__c>();
        settings.add(new JVCO_TransitionManualReviewNotifSettings__c(Name = 'Limit', Limit__c = '3000'));
        insert settings;

        Test.StartTest();
        JVCO_SurchargeGeneration_Sched sgs = new JVCO_SurchargeGeneration_Sched();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, sgs); 
        Test.stopTest(); 
    }
                
}