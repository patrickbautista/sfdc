/* ----------------------------------------------------------------------------------------------
   Name: JVCO_VenueAssignFieldAgentsPostCodeLogic.cls 
   Description: Business logic class for updating Ownership of a venue record before insert or before update

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  07-Sep-2016   0.1         mark.glenn.b.ayson  Intial creation
  ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_VenueAssignFieldAgentsPostCodeLogic {
    /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: Function that updates the owner ID based on the postcode from the venue record
    Inputs: List<JVCO_Venue__c>
    Returns: void
    <Date>      <Authors Name>        <Brief Description of Change> 
    07-Sep-2016 mark.glenn.b.ayson      Initial version of function
    27-Sep-2016 mark.glenn.b.ayson      Added wrapper class
    29-Sep-2016 mark.glenn.b.ayson      Updated the logic for the postcode
    24-Dec-2016 Robert John B. Lacatan  Added error whenever owner of Venue is Unresolve Addresses Queue
    18-Oct-2017 reymark.j.l.arlos       Updated to make sure the venue owner will not change
  ----------------------------------------------------------------------------------------------- */
  
    public static void updateVenueOwnerID(List<JVCO_Venue__c> venues, Boolean isUpdate)
    {

        List<String> fieldSalesList = new List<String>();
        List<JVCO_Venue__c> updatedVenues = new List<JVCO_Venue__c>();
        Boolean isOwnerQueue = true;
        //String postCode = '';
        String fieldSalesRegion = '';
        Id newOwnerID;
        Boolean bHasMatch = false;
        Map<Id, String> venueIdAndPostCodeMap = new Map<Id, String>();
        Map<String, JVCO_AssignFieldAgentWrapper> postCodeAndRegionMap = new Map<String, JVCO_AssignFieldAgentWrapper>();
        JVCO_AssignFieldAgentWrapper retrievedRecord = new JVCO_AssignFieldAgentWrapper();
        String mapData = '';
        String recordPostCode = '';
        Map<Id, Id> venueTaskOwnerId = new Map<Id, Id>(); //18-Oct-2017 reymark.j.l.arlos

        // Get the Venue ID and PostCode
        venueIdAndPostCodeMap = createVenueIdAndPostCodeMap(venues);
        // Get the region and postcode and save it to a map
        postCodeAndRegionMap = JVCO_AssignFieldAgentsPostCodeUtil.getFieldSalesDetails(venueIdAndPostCodeMap);

        // Get the Lead from the trigger
        for(JVCO_Venue__c venueRecord : venues)
        {
            // Get the first string in the postcode 
            recordPostCode = JVCO_AssignFieldAgentsPostCodeUtil.getPostCode(venueRecord.JVCO_Postcode__c.trim());

            for(String postCode : postCodeAndRegionMap.keySet())
            {
                // Get the retrieved postcode after the split
                if(postcode.equals(recordPostCode))
                {
                    // There is a match
                    bHasMatch = true;
                    // Save the details to a wrapper class
                    retrievedRecord = postCodeAndRegionMap.get(postCode);

                    // Exit the loop if a record is saved to the wrapper
                    if(!String.isBlank(retrievedRecord.postCodeLabel))
                    {
                        break;
                    }
                }
                else if(postCode.equals(JVCO_AssignFieldAgentsPostCodeUtil.removeNumeric(recordPostCode)))
                {
                    // There is a match
                    bHasMatch = true;
                    // Save the details to a wrapper class
                    retrievedRecord = postCodeAndRegionMap.get(postCode);

                    // Exit the loop if a record is saved to the wrapper
                    if(!String.isBlank(retrievedRecord.postCodeLabel))
                    {
                        break;
                    }
                }   
            }

            if(bHasMatch)
            {
                // Update the Venue OwnerID 
                venueTaskOwnerId.put(venueRecord.Id, retrievedRecord.userName);//18-Oct-2017 reymark.j.l.arlos
                venueRecord.JVCO_Region__c = retrievedRecord.postCodeLabel;
            }
            else
            {
                venueRecord.ownerID =  JVCO_AssignFieldAgentsPostCodeUtil.getQueueID();
                //added error whenever owner is Unresolve Queue
                venueRecord.addError(Label.JVCO_Venue_Unresolve_Error);
            }

            updatedVenues.add(venueRecord);
        }

        if(isUpdate && updatedVenues.size() > 0)
        {
            // Create a new Task record
            createTaskRecord(updatedVenues, venueTaskOwnerId); //18-Oct-2017 reymark.j.l.arlos
        }
    }

    
    /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: Create a map that contains the venue ID and postcode
                Map<VenueId, Postcode>
    Inputs: List<JVCO_Venue__c>
    Returns: Map<Id, String>
    <Date>      <Authors Name>      <Brief Description of Change> 
    14-Sep-2016 mark.glenn.b.ayson  Initial version of function
  ----------------------------------------------------------------------------------------------- */
  
    public static Map<Id, String> createVenueIdAndPostCodeMap(List<JVCO_Venue__c> venueList)
    {
        Map<Id, String> venueIdAndPostCodeMap = new Map<Id, String>(); 

        // Get the records from the list and save it to a map
        for(JVCO_Venue__c venueRecord : venueList)
        {
            if(!String.isBlank(venueRecord.JVCO_Postcode__c) && venueRecord.JVCO_Field_Visit_Requested__c == true)
            {    system.debug('@@@'+venueRecord.Id);
                venueIdAndPostCodeMap.put(venueRecord.Id, venueRecord.JVCO_Postcode__c);
            }
        }

        return venueIdAndPostCodeMap;
    }
    

    /* ----------------------------------------------------------------------------------------------
    Author: mark.glenn.b.ayson
    Company: Accenture
    Description: Function that saves the venue record to a list and call the createNewTask method in the util
    Inputs: JVCO_Venue__c, Boolean
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    07-Sep-2016 mark.glenn.b.ayson  Initial version of function
    18-Oct-2017 reymark.j.l.arlos   Updated to make sure the venue owner will not change
  ----------------------------------------------------------------------------------------------- */
  
    private static void createTaskRecord(List<JVCO_Venue__c> venueList, Map<Id, Id> taskOwnerId)
  {
    List<JVCO_AssignFieldAgentWrapper> taskRecordList = new List<JVCO_AssignFieldAgentWrapper>();
      JVCO_AssignFieldAgentWrapper wrapper = new JVCO_AssignFieldAgentWrapper(); 

    for(JVCO_Venue__c venueRecord : venueList)
    {
       wrapper = new JVCO_AssignFieldAgentWrapper(); 
      // Add the venue name
      wrapper.venueName = venueRecord.Name;

      // Check if the newOwnerID is not a queue, add the new owner ID else, add the current logged-in user
      //start 18-Oct-2017 reymark.j.l.arlos
      if(taskOwnerId.containsKey(venueRecord.id))
      {
        if(taskOwnerId.get(venueRecord.id) != JVCO_AssignFieldAgentsPostCodeUtil.queueID)
        {
            wrapper.ownerID = taskOwnerId.get(venueRecord.id);
        }
        else
        {
            wrapper.ownerID = UserInfo.getUserId();
        }
      }
      //end 18-Oct-2017 reymark.j.l.arlos

      // Add the record ID
       wrapper.recordID = venueRecord.Id;
      // Add to list
       taskRecordList.add(wrapper);

    }

    if(taskRecordList.size() > 0)
    {
      // Call the create task methid in the util class (List<String>, boolean value that will check if the object is a lead)
      JVCO_AssignFieldAgentsPostCodeUtil.createNewTask(taskRecordList, false);
    }
  }
    
}