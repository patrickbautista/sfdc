public class JVCO_UpdateOrderByBillingBatch implements Database.Batchable<sObject>
{
    public List<Integer> yearList = new List<Integer>();
    public List<Integer> monthList = new List<Integer>();
    
    public JVCO_UpdateOrderByBillingBatch(List<Integer> yearList, List<Integer> monthList){
        this.yearList = yearList;
        this.monthList = monthList;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT 
                                         blng__Account__c, 
                                         blng__DueDate__c, 
                                         blng__InvoiceStatus__c, 
                                         CreatedById, 
                                         CreatedDate, 
                                         Id, 
                                         JVCO_Cancelled__c, 
                                         JVCO_Contract__c, 
                                         JVCO_Distribution_Status__c, 
                                         JVCO_Invoice_Legacy_Number__c, 
                                         JVCO_Invoice_Type__c, 
                                         JVCO_Legacy_Contracts__c, 
                                         JVCO_Licence_End_Date__c, 
                                         JVCO_Licence_Start_Date__c, 
                                         JVCO_Manual_Invoice__c, 
                                         JVCO_Migrated__c, 
                                         JVCO_Migrated_Credit_Note__c, 
                                         JVCO_Original_Invoice__r.blng__Order__c, 
                                         JVCO_Original_Invoice_Cancelled__c, 
                                         JVCO_Review_Type__c, 
                                         JVCO_Sales_Credit_Note__c, 
                                         JVCO_Sales_Invoice__c, 
                                         JVCO_Status_Change_Date__c, 
                                         JVCO_Surcharge_Date__c, 
                                         JVCO_Surcharge_Generated__c, 
                                         JVCO_Unmatched__c, 
                                         JVCO_Week__c, 
                                         blng__Order__c,
                                         Name
                                         FROM blng__Invoice__c
                                         WHERE blng__Order__c != null
                                         AND blng__Order__r.JVCO_Is_Updated__c = false
                                         AND CALENDAR_YEAR(CreatedDate) IN: yearList
                                         AND CALENDAR_MONTH(CreatedDate) IN: monthList
                                         AND JVCO_Invoice_Type__c NOT IN ('Surcharge','Credit Cancellation')]);
    }
    
    public void execute(Database.BatchableContext bc, List<blng__Invoice__c> scope)
    {
        Map<Id, Order> orderMap = new Map<Id, Order>();
        
        for(blng__Invoice__c bInv: scope){
            if(!orderMap.containsKey(bInv.blng__Order__c)){
                Order ord = new Order();
                ord.Id = bInv.blng__Order__c;
                ord.JVCO_Order_Status__c = bInv.blng__InvoiceStatus__c;
                ord.JVCO_Cancelled__c = bInv.JVCO_Cancelled__c;
                ord.ContractId = bInv.JVCO_Contract__c;
                ord.JVCO_Contract__c = bInv.JVCO_Contract__c;
                ord.JVCO_Distribution_Status__c = bInv.JVCO_Distribution_Status__c;
                ord.JVCO_Invoice_Legacy_Number__c = bInv.JVCO_Invoice_Legacy_Number__c;
                ord.JVCO_Invoice_Type__c = bInv.JVCO_Invoice_Type__c;
                ord.JVCO_Legacy_Contracts__c = bInv.JVCO_Legacy_Contracts__c;
                ord.JVCO_Manual_Invoice__c = bInv.JVCO_Manual_Invoice__c;
                ord.JVCO_Migrated__c = bInv.JVCO_Migrated__c;
                ord.JVCO_Migrated_Credit_Note__c = bInv.JVCO_Migrated_Credit_Note__c;
                ord.JVCO_Original_Order__c = bInv.JVCO_Original_Invoice__r.blng__Order__c;
                ord.JVCO_Original_Invoice_Cancelled__c = bInv.JVCO_Original_Invoice_Cancelled__c;
                ord.JVCO_Sales_Credit_Note__c = bInv.JVCO_Sales_Credit_Note__c;
                ord.JVCO_Sales_Invoice__c = bInv.JVCO_Sales_Invoice__c;
                ord.JVCO_Status_Change_Date__c = bInv.JVCO_Status_Change_Date__c;
                ord.JVCO_Surcharge_Date__c = bInv.JVCO_Surcharge_Date__c;
                ord.JVCO_Surcharge_Generated__c = bInv.JVCO_Surcharge_Generated__c;
                ord.JVCO_Unmatched__c = bInv.JVCO_Unmatched__c;
                ord.JVCO_Week__c = bInv.JVCO_Week__c;
                ord.JVCO_Temp_End_Date__c = bInv.JVCO_Licence_End_Date__c;
                ord.JVCO_Temp_Start_Date__c = bInv.JVCO_Licence_Start_Date__c;
                ord.JVCO_Invoice_Type__c = bInv.JVCO_Invoice_Type__c;
                ord.JVCO_Is_Updated__c = true;
                orderMap.put(bInv.blng__Order__c, ord);
            }
        }
        if(!orderMap.isEmpty()){
            try{
                update orderMap.values();
            }
            catch(Exception ex){
                insert createErrorLog(orderMap.values()[0].Id, ex.getMessage());
            }
        }
    }
    
    public ffps_custRem__Custom_Log__c createErrorLog(Id relatedObjectId, String errMessage){
        
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
        errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(relatedObjectId);
        errLog.ffps_custRem__Detail__c = errMessage;
        errLog.ffps_custRem__Message__c = 'JVCO_UpdateOrderByBillingBatch';
        errLog.ffps_custRem__Grouping__c = 'UPO-001';
        
        return errLog;
    }
    
    public void finish(Database.BatchableContext bc){}
}