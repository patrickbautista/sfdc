@isTest
private class JVCO_InvoiceAndCreditNoteGenerationTest {

    @testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
         System.runAs(new User(Id=UserInfo.getUserId(), Email = 'test@gmail.com'))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
		
		RenewalMonthLimit__c renewalCustomsetting = new RenewalMonthLimit__c();
        renewalCustomsetting.Amount__c = 3;
        insert renewalCustomsetting;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        insert glaList;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim2('PRS');
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Test.startTest();
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        insert JVCO_TestClassHelper.setQuoteProcess();
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        Test.stopTest();
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        JVCO_TestClassHelper.setGeneralSettingsCS();
        JVCO_TestClassHelper.setCashMatchingCS();

        blng__InvoiceRun__c invRun = new blng__InvoiceRun__c();
        invrun.blng__InvoiceDate__c = Date.today();
        invrun.blng__TargetDate__c = Date.today();
        insert invRun;

        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        bInv.blng__InvoiceRunCreatedBy__c = invRun.Id;
        insert bInv;
    }
    
    @isTest
    static void testCreateSingleOrderInvoice()
    {   
        blng__Invoice__c bInv = [SELECT Id FROM blng__Invoice__c LIMIT 1];
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        OrderItem oi = [SELECT Id FROM OrderItem LIMIT 1];  
        insert JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);
        blng__InvoiceRun__c bInvRun = [SELECT Id FROM blng__InvoiceRun__c LIMIT 1];
        
        Test.startTest();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        bInvRun.blng__Status__c = 'Completed';
        SBQQ.TriggerControl.disable();
        update bInvRun;
        Test.stopTest();
    }

    @isTest
    static void testCreateCreditNoteFromSingleOrder()
    {
        Test.startTest();
        blng__Invoice__c bInv = [SELECT Id,JVCO_Total_Amount__c,blng__Subtotal__c FROM blng__Invoice__c LIMIT 1];
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        OrderItem oi = [SELECT Id FROM OrderItem LIMIT 1];
        blng__InvoiceLine__c bInvLine = JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, -50, true);
        bInvLine.Dimension_2s__c = 'PPL';
        JVCO_FFutil.stopBlngInvoiceLineHandlerBeforeInsert = true;
        insert bInvLine;
        
        blng__InvoiceRun__c bInvRun = [SELECT Id FROM blng__InvoiceRun__c LIMIT 1];
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        bInvRun.blng__Status__c = 'Completed';
        SBQQ.TriggerControl.disable();
        update bInvRun;
        Test.stopTest();
    }
    
    @isTest
    static void testDDPayeeBlank()
    {   
        blng__Invoice__c bInv = [SELECT Id, blng__Account__c FROM blng__Invoice__c LIMIT 1];
        Account a = new Account();
        a.Id = bInv.blng__Account__c;
        a.JVCO_DD_Payee_Contact__c = null;
        update a;
        
        Income_Direct_Debit__c incomeDD = new Income_Direct_Debit__c();
        incomeDD.Account__c = a.Id;
        incomeDD.DD_Collection_Day__c = '1';
        incomeDD.DD_Collection_Period__c = 'Monthly';
        incomeDD.DD_Collection_Stretch__c = '1';
        incomeDD.DD_Collection_Type__c = 'Fixed';
        incomeDD.DD_Collection_Reference__c = 'Monthly Subscription';
        incomeDD.DD_Status__c = 'New Instruction';
        incomeDD.DD_Bank_Account_Number__c = '12345678';
        incomeDD.DD_Bank_Sort_Code__c = '123456';
        incomeDD.Number_Of_Payments__c = 2;
        insert incomeDD;
        
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        OrderItem oi = [SELECT Id FROM OrderItem LIMIT 1];  
        insert JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);
        blng__InvoiceRun__c bInvRun = [SELECT Id FROM blng__InvoiceRun__c LIMIT 1];
        
        Test.startTest();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        bInvRun.blng__Status__c = 'Completed';
        SBQQ.TriggerControl.disable();
        update bInvRun;
        Test.stopTest();
    }
    
    @isTest
    static void testUpdateIDD()
    {
        blng__Invoice__c bInv = [SELECT Id, blng__Account__c FROM blng__Invoice__c LIMIT 1];
       
        Income_Direct_Debit__c incomeDD = new Income_Direct_Debit__c();
        incomeDD.Account__c = bInv.blng__Account__c;
        incomeDD.DD_Collection_Day__c = '1';
        incomeDD.DD_Collection_Period__c = 'Monthly';
        incomeDD.DD_Collection_Stretch__c = '1';
        incomeDD.DD_Collection_Type__c = 'Fixed';
        incomeDD.DD_Collection_Reference__c = 'Monthly Subscription';
        incomeDD.DD_Status__c = 'New Instruction';
        incomeDD.DD_Bank_Account_Number__c = '12345678';
        incomeDD.DD_Bank_Sort_Code__c = '123456';
        incomeDD.Number_Of_Payments__c = 2;
        insert incomeDD;
        
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        OrderItem oi = [SELECT Id FROM OrderItem LIMIT 1];  
        insert JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);
        blng__InvoiceRun__c bInvRun = [SELECT Id FROM blng__InvoiceRun__c LIMIT 1];
        String datestr = String.valueOf(Date.today().year()) + '-' + String.valueOf(Date.today().month()) + '-' + String.valueOf(Date.today().day());
        String responseBody = '{"ProcessListNewDDFirstCollectionDateResult":[{"Error":"","FirstCollectionDate":"'+datestr+'"},{"Error":"","FirstCollectionDate":"'+datestr+'"}]}';
        HttpRestCallOutMock fakeResponse = new HttpRestCallOutMock(200, responseBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        Test.startTest();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        bInvRun.blng__Status__c = 'Completed';
        SBQQ.TriggerControl.disable();
        update bInvRun;
        Test.stopTest();
    }
    
    @isTest
    static void testEmptyConstructor()
    {
        JVCO_PostSalesCreditNoteBatch pscb = new JVCO_PostSalesCreditNoteBatch();
        JVCO_PostSalesInvoiceBatch psib = new JVCO_PostSalesInvoiceBatch();
    }
}