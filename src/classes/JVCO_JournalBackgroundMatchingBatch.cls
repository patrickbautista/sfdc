/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_JournalBackgroundMatchingBatch.cls 
   Description:     Batch Class for JVCO_JournalBackgroundMatchingBatch.cls
   Test class:      

   Date            Version     Author              Summary of Changes 
   -----------     -------     -----------------   -----------------------------------
   22-Mar-2019     0.1         franz.g.a.dimaapi   Intial creation
  ------------------------------------------------------------------------------------------------ */
public class JVCO_JournalBackgroundMatchingBatch implements Database.Batchable<sObject>, Database.Stateful
{
    private static final JVCO_General_Settings__c generalSettings = JVCO_General_Settings__c.getInstance();
    public Map<Id, Id> licAndCustAccIdMap = new Map<Id, Id>();

    public Database.QueryLocator start(Database.BatchableContext bc) 
    {   
        return Database.getQueryLocator([SELECT Id,
                                         c2g__Account__c,
                                         c2g__Account__r.JVCO_Customer_Account__c,
                                         c2g__AccountOutstandingValue__c,
                                         c2g__OwnerCompany__r.c2g__CustomerWriteOff__c,
                                         c2g__Transaction__r.c2g__Period__r.c2g__Closed__c,
                                         c2g__Transaction__r.c2g__TransactionType__c,
                                         c2g__Transaction__r.c2g__Journal__r.JVCO_Reference_Document__c,
                                         c2g__Transaction__r.c2g__Journal__r.JVCO_Reference_Document_Type__c
                                         FROM c2g__codaTransactionLineItem__c
                                         WHERE c2g__AccountOutstandingValue__c != 0
                                         AND c2g__LineType__c IN ('Account')
                                         AND c2g__LineDescription__c != 'Not for Matching'
                                         AND c2g__Account__r.JVCO_In_Infringement__c = FALSE
                                         AND c2g__Account__r.JVCO_In_Enforcement__c = FALSE
                                         AND c2g__Account__r.JVCO_Exclude_From_Background_Matching__c = FALSE
                                         AND c2g__Transaction__r.c2g__Journal__r.JVCO_Reference_Document_Type__c IN ('Invoice', 'Credit', 'Journal', 'Cash')
                                        ]);
    }
    
    public void execute(Database.BatchableContext bc, List<c2g__codaTransactionLineItem__c> scope)
    {
        if(!generalSettings.JVCO_Disable_Journal_Background_Matching__c)
        {
            setMatchingReferenceMapping(scope);
            removeReferenceDocument(scope);
            getAccountToRunQueable(scope);
        }
    }
    
    private void setMatchingReferenceMapping(List<c2g__codaTransactionLineItem__c> transLineList)
    {
        Map<Id, String> journalTransLineIdToSInvNameMap = new Map<Id, String>();
        Map<Id, String> journalTransLineIdToCNoteNameMap = new Map<Id, String>();
        Map<Id, String> journalTransLineIdToJournalNameMap = new Map<Id, String>();
        Map<Id, String> journalTransLineIdToReceiptLineNameMap = new Map<Id, String>();
        Map<Id, String> journalTransLineIdToRefundLineNameMap = new Map<Id, String>();
        Map<Id, c2g__codaTransactionLineItem__c> transLineMap = new Map<Id, c2g__codaTransactionLineItem__c>();
        
        for(c2g__codaTransactionLineItem__c tli : transLineList)
        {
            String journalReferenceDocument = tli.c2g__Transaction__r.c2g__Journal__r.JVCO_Reference_Document__c;
            if(tli.c2g__Transaction__r.c2g__Journal__r.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Invoice'))
            {
                journalTransLineIdToSInvNameMap.put(tli.Id, journalReferenceDocument);
            }else if(tli.c2g__Transaction__r.c2g__Journal__r.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Credit'))
            {
                journalTransLineIdToCNoteNameMap.put(tli.Id, journalReferenceDocument);
            }else if(tli.c2g__Transaction__r.c2g__Journal__r.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Journal'))
            {
                journalTransLineIdToJournalNameMap.put(tli.Id, journalReferenceDocument);
            }else if(tli.c2g__Transaction__r.c2g__Journal__r.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Cash') &&
                     tli.c2g__AccountOutstandingValue__c > 0)
            {
                journalTransLineIdToReceiptLineNameMap.put(tli.Id, journalReferenceDocument);
            }else
            {
                journalTransLineIdToRefundLineNameMap.put(tli.Id, journalReferenceDocument);
            }
            transLineMap.put(tli.Id, tli);
        }
        
        executeMatching(journalTransLineIdToSInvNameMap, journalTransLineIdToCNoteNameMap, journalTransLineIdToJournalNameMap,
                        journalTransLineIdToReceiptLineNameMap, journalTransLineIdToRefundLineNameMap, transLineMap);
    }
    
    private void executeMatching(Map<Id, String> journalTransLineIdToSInvNameMap, Map<Id, String> journalTransLineIdToCNoteNameMap, 
                                 Map<Id, String> journalTransLineIdToJournalNameMap, Map<Id, String> journalTransLineIdToReceiptLineNameMap,
                                 Map<Id, String> journalTransLineIdToRefundLineNameMap, Map<Id, c2g__codaTransactionLineItem__c> transLineMap)
    {
        JVCO_MatchingReferenceDocumentInterface mrdi = null;
        if(!journalTransLineIdToSInvNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToInvoiceLogic(journalTransLineIdToSInvNameMap, transLineMap);
        }else if(!journalTransLineIdToCNoteNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToCreditNoteLogic(journalTransLineIdToCNoteNameMap, transLineMap);
        }else if(!journalTransLineIdToJournalNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToJournalLogic(journalTransLineIdToJournalNameMap, transLineMap);
        }else if(!journalTransLineIdToReceiptLineNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToCashReceiptLogic(journalTransLineIdToReceiptLineNameMap, transLineMap);
        }else
        {
            mrdi = new JVCO_MatchToCashRefundLogic(journalTransLineIdToRefundLineNameMap, transLineMap);
        }
        mrdi.execute();
    }
    
    private void getAccountToRunQueable(List<c2g__codaTransactionLineItem__c> scope){
        for(c2g__codaTransactionLineItem__c tli : scope)
        {
            if(!licAndCustAccIdMap.containsKey(tli.c2g__Account__c) && tli.c2g__Account__c != null && tli.c2g__Account__r.JVCO_Customer_Account__c != null){
                licAndCustAccIdMap.put(tli.c2g__Account__c, tli.c2g__Account__r.JVCO_Customer_Account__c);
            }
        }
    }

    private void removeReferenceDocument(List<c2g__codaTransactionLineItem__c> transLineList)
    {
        Set<c2g__codaJournal__c> journalSet = new Set<c2g__codaJournal__c>();
        for(c2g__codaTransactionLineItem__c transLine : transLineList)
        {
            c2g__codaJournal__c journal = new c2g__codaJournal__c();
            journal.Id = transLine.c2g__Transaction__r.c2g__Journal__c;
            journal.JVCO_Reference_Document__c = null;
            journalSet.add(journal);
        }
        update new List<c2g__codaJournal__c>(journalSet);
    }

    public void finish(Database.BatchableContext bc)
    {
        JVCO_CashBackgroundMatchingBatch cbmb = new JVCO_CashBackgroundMatchingBatch();
        cbmb.licAndCustAccIdMap = licAndCustAccIdMap;
        Database.executeBatch(cbmb, 1);
    }
}