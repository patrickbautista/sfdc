@isTest
public class JVCO_GenerateRenewalQuoteExtensionTest {

    @testSetup
    public static void testData(){

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPricebook();
        insert pb;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;

        Opportunity o = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        insert o;      

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, o.id, null, 'Annually');
        insert q;
        
        Contract contr = JVCO_TestClassObjectBuilder.createContract(acc2.id, q.id);
        insert contr;       
    }

    static testMethod void testJVCO_GenerateRenewalQuoteExtension(){
        Contract c = [select id from Contract order by CreatedDate DESC limit 1];
        PageReference dmpRef;

        Test.startTest();

        Test.setCurrentPage(Page.JVCO_GenerateRenewalQuote);
        JVCO_GenerateRenewalQuoteExtension ext = new JVCO_GenerateRenewalQuoteExtension(new ApexPages.StandardController(c));
        
        ext.renewalUpdate();
        dmpRef = ext.quoteCreated();

        Test.stopTest();

        Contract cCheck = [select SBQQ__RenewalQuoted__c from Contract where id = :c.Id order by CreatedDate DESC limit 1];

        system.assert(cCheck.SBQQ__RenewalQuoted__c);
    }

    static testMethod void testErrorException(){

       
        Contract contr2 = new Contract();
        PageReference dmpRef;

        try {

            Test.startTest();

            Test.setCurrentPage(Page.JVCO_GenerateRenewalQuote);
            JVCO_GenerateRenewalQuoteExtension ext = new JVCO_GenerateRenewalQuoteExtension(new ApexPages.StandardController(contr2));
            
            ext.renewalUpdate();
            dmpRef = ext.quoteCreated();

            Test.stopTest();

        }
        catch (Exception ex)
        {
              System.assertEquals(true, ApexPages.hasMessages(ApexPages.SEVERITY.WARNING), 'No Error');
        }
    }
}