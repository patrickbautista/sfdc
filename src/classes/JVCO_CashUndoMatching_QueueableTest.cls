/* ----------------------------------------------------------------------------------------------
Name: JVCO_CashUndoMatching_QueueableTest.cls 
Description: Queueable class to unmatch matching histories from a list

Date         Version     Author              Summary of Changes 
-----------  -------     -----------------   -----------------------------------------
01-08-2018  0.1         mary.ann.a.ruelan  Intial creation. GREEN-32947  
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_CashUndoMatching_QueueableTest
{
    @testSetup static void createTestData()
    {
        List<RecordType> rtypesCENLIH = [SELECT Name, Id FROM RecordType WHERE sObjectType='JVCO_CashEntryLineItemHistory__c' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> RecordTypesCENLIH  = new Map<String,String>{};
        for(RecordType rt: rtypesCENLIH)
        {
          RecordTypesCENLIH.put(rt.Name,rt.Id);
        }
        List<CashEntryLineItemHistory_CS__c> settingsCENLIH = new List<CashEntryLineItemHistory_CS__c>();
        settingsCENLIH.add(new CashEntryLineItemHistory_CS__c(Name = 'Matched', Value__c = RecordTypesCENLIH.get('Matched Cash')));
        settingsCENLIH.add(new CashEntryLineItemHistory_CS__c(Name = 'Reversed', Value__c = RecordTypesCENLIH.get('Reversed Cash')));
        insert settingsCENLIH;
        
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        //Settings
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_codaCreditNoteTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        insert dt;
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        c2g__codaDimension2__c dim2 = JVCO_TestClassHelper.setDim2('PPL');
        insert dim2;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        JVCO_SLA_Constants__c cs = JVCO_TestClassObjectBuilder.createSLAConstants();
        insert cs;

        Entitlement e = JVCO_TestClassObjectBuilder.createEntitlement(licAcc.id);
        insert e;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Test.startTest();
        List<c2g__codaInvoice__c> sInvList = new List<c2g__codaInvoice__c>();
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        sInvList.add(sInv);
        c2g__codaInvoice__c sInv2 = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        sInvList.add(sInv2);
        insert sInvList;
        
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv2.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);

        //Create Cash Entry
        c2g__codaCashEntry__c testCashEntry = new c2g__codaCashEntry__c();
        testCashEntry.c2g__PaymentMethod__c = 'Electronic';
        testCashEntry.c2g__OwnerCompany__c = comp.id;
        testCashEntry.ownerid = testGroup.Id;
        testCashEntry.c2g__Status__c = 'In Progress';
        testCashEntry.c2g__Date__c = Date.today();
        testCashEntry.c2g__Value__c = 100.00;
        insert testCashEntry;
        List<c2g__codaInvoice__c> invoiceList = [select id, Name from c2g__codaInvoice__c limit 2];
        //Create Cash Entry Line Item
        List<c2g__codaCashEntryLineItem__c> cashEntryLIList = new List<c2g__codaCashEntryLineItem__c>();
        c2g__codaCashEntryLineItem__c cashEntryLineItem1 = new c2g__codaCashEntryLineItem__c();
        cashEntryLineItem1.c2g__CashEntry__c = testCashEntry.id;
        cashEntryLineItem1.c2g__Account__c = licAcc.id;
        cashEntryLineItem1.c2g__CashEntryValue__c = 100;
        cashEntryLineItem1.c2g__LineNumber__c = 1;
        cashEntryLineItem1.c2g__AccountReference__c = invoiceList[0].Name;
        cashEntryLineItem1.c2g__AccountPaymentMethod__c = 'Cheque';
        cashEntryLineItem1.c2g__OwnerCompany__c = comp.id;
        cashEntryLineItem1.JVCO_Temp_AccountReference__c = 'SIN00001';
        cashEntryLineItem1.JVCO_CaseRecordType_Insolvency__c = '';
        cashEntryLIList.add(cashEntryLineItem1);

        c2g__codaCashEntryLineItem__c cashEntryLineItem2 = new c2g__codaCashEntryLineItem__c();
        cashEntryLineItem2.c2g__CashEntry__c = testCashEntry.id;
        cashEntryLineItem2.c2g__Account__c = licAcc.id;
        cashEntryLineItem2.c2g__CashEntryValue__c = 100;
        cashEntryLineItem2.c2g__LineNumber__c = 2;
        cashEntryLineItem2.c2g__AccountReference__c = invoiceList[1].Name;
        cashEntryLineItem2.c2g__AccountPaymentMethod__c = 'Cheque';
        cashEntryLineItem2.c2g__OwnerCompany__c = comp.id;
        cashEntryLineItem2.JVCO_Temp_AccountReference__c = 'SIN00002';
        cashEntryLineItem2.JVCO_CaseRecordType_Insolvency__c = '';
        cashEntryLIList.add(cashEntryLineItem2);
        insert cashEntryLIList;
        Test.stopTest();
        
        //Turn off Background Matching
        JVCO_BackgroundMatchingLogic.stopMatching = true;

        //Post Sales Invoice
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        List<c2g.CODAAPICommon.Reference> refList = new List<c2g.CODAAPICommon.Reference>();
            Double totalAmount = 10.0;
            c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = sInv.Id;
            refList.add(reference);
            c2g.CODAAPICommon.Reference reference2 = new c2g.CODAAPICommon.Reference();
            reference2.Id = sInv2.Id;
            refList.add(reference2);
        c2g.CODAAPISalesInvoice_10_0.BulkPostInvoice(context, refList);

        //Post Cash Entry
        c2g.CODAAPICommon_7_0.Context cashEntryContext = new c2g.CODAAPICommon_7_0.Context();
        List<c2g.CODAAPICommon.Reference> cashEntryRefList = new List<c2g.CODAAPICommon.Reference>();
            c2g.CODAAPICommon.Reference reference3 = new c2g.CODAAPICommon.Reference();
            reference3.Id = testCashEntry.Id;
            cashEntryRefList.add(reference3);
        c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(cashEntryContext, cashEntryRefList);
    }

    //Unmatch
    @isTest static void unmatchSuccessful()
    {
        c2g__codaTransactionLineItem__c cashTransaction = [SELECT Id, c2g__Account__c FROM c2g__codaTransactionLineItem__c WHERE c2g__LineType__c = 'Account' AND c2g__Transaction__r.c2g__TransactionType__c = 'Cash' LIMIT 1];
        c2g__codaTransactionLineItem__c invoiceTransaction = [SELECT Id FROM c2g__codaTransactionLineItem__c WHERE c2g__LineType__c = 'Account' AND c2g__Transaction__r.c2g__TransactionType__c = 'Invoice' LIMIT 1];
        c2g__codaPeriod__c period = [SELECT Id FROM c2g__codaPeriod__c LIMIT 1];

        List<c2g.CODAAPICashMatchingTypes_8_0.Item> itemsList = new List<c2g.CODAAPICashMatchingTypes_8_0.Item>();
        c2g.CODAAPICashMatchingTypes_8_0.Item item1 = new c2g.CODAAPICashMatchingTypes_8_0.Item();
        item1.TransactionLineItem = c2g.CODAAPICommon.getRef(cashTransaction.Id, null);
        item1.Paid = -100;
        itemsList.add(item1);

        c2g.CODAAPICashMatchingTypes_8_0.Item item2 = new c2g.CODAAPICashMatchingTypes_8_0.Item();
        item2.TransactionLineItem = c2g.CODAAPICommon.getRef(invoiceTransaction.Id, null);
        item2.Paid = 100;
        itemsList.add(item2);

        c2g.CODAAPICommon_8_0.Context context = new c2g.CODAAPICommon_8_0.Context();
        c2g.CODAAPICashMatchingTypes_8_0.Analysis analysisInfo = new c2g.CODAAPICashMatchingTypes_8_0.Analysis();

        c2g.CODAAPICashMatchingTypes_8_0.Configuration configuration = new c2g.CODAAPICashMatchingTypes_8_0.Configuration();
        configuration.Account = c2g.CODAAPICommon.getRef(cashTransaction.c2g__Account__c, null);
        configuration.MatchingDate = System.today();
        configuration.MatchingCurrencyMode = c2g.CODAAPICashMatchingTypes_8_0.enumMatchingCurrencyMode.Account; 
        configuration.MatchingPeriod = c2g.CODAAPICommon.getRef(period.id, null);

        Test.startTest();
        
        //Match
        boolean isExpectedError = false;
        try
        {
            Id matchRefId = c2g.CODAAPICashMatching_8_0.Match(context, configuration, itemsList, analysisInfo).Id;
        }
        catch(exception e)
        {
            isExpectedError = e.getMessage().contains(Label.JVCO_MultipleMatchingError);
        }

        Test.stopTest();    
        
        List<c2g__codaCashMatchingHistory__c> mhList = [Select id, c2g__MatchingReference__c, c2g__Account__c, c2g__Period__c
                                                        From c2g__codaCashMatchingHistory__c limit 1];         
        //unmatch
        System.enqueueJob(new JVCO_CashUndoMatching_Queueable(mhList));    
    }
    
    //Create error logs by unmatching already unmatched matching history
    @isTest static void matchError()
    {
        c2g__codaTransactionLineItem__c cashTransaction = [SELECT Id, c2g__Account__c FROM c2g__codaTransactionLineItem__c WHERE c2g__LineType__c = 'Account' AND c2g__Transaction__r.c2g__TransactionType__c = 'Cash' LIMIT 1];
        c2g__codaTransactionLineItem__c invoiceTransaction = [SELECT Id FROM c2g__codaTransactionLineItem__c WHERE c2g__LineType__c = 'Account' AND c2g__Transaction__r.c2g__TransactionType__c = 'Invoice' LIMIT 1];
        c2g__codaPeriod__c period = [SELECT Id FROM c2g__codaPeriod__c LIMIT 1];

        List<c2g.CODAAPICashMatchingTypes_8_0.Item> itemsList = new List<c2g.CODAAPICashMatchingTypes_8_0.Item>();
        c2g.CODAAPICashMatchingTypes_8_0.Item item1 = new c2g.CODAAPICashMatchingTypes_8_0.Item();
        item1.TransactionLineItem = c2g.CODAAPICommon.getRef(cashTransaction.Id, null);
        item1.Paid = -100;
        itemsList.add(item1);

        c2g.CODAAPICashMatchingTypes_8_0.Item item2 = new c2g.CODAAPICashMatchingTypes_8_0.Item();
        item2.TransactionLineItem = c2g.CODAAPICommon.getRef(invoiceTransaction.Id, null);
        item2.Paid = 100;
        itemsList.add(item2);

        c2g.CODAAPICommon_8_0.Context context = new c2g.CODAAPICommon_8_0.Context();
        c2g.CODAAPICashMatchingTypes_8_0.Analysis analysisInfo = new c2g.CODAAPICashMatchingTypes_8_0.Analysis();

        c2g.CODAAPICashMatchingTypes_8_0.Configuration configuration = new c2g.CODAAPICashMatchingTypes_8_0.Configuration();
        configuration.Account = c2g.CODAAPICommon.getRef(cashTransaction.c2g__Account__c, null);
        configuration.MatchingDate = System.today();
        configuration.MatchingCurrencyMode = c2g.CODAAPICashMatchingTypes_8_0.enumMatchingCurrencyMode.Account; 
        configuration.MatchingPeriod = c2g.CODAAPICommon.getRef(period.id, null);

        Test.startTest();
        
        //Match
        boolean isExpectedError = false;
        try
        {
            Id matchRefId = c2g.CODAAPICashMatching_8_0.Match(context, configuration, itemsList, analysisInfo).Id;
        }
        catch(exception e)
        {
            isExpectedError = e.getMessage().contains(Label.JVCO_MultipleMatchingError);
        }

        Test.stopTest();        
        
        List<c2g__codaCashMatchingHistory__c> mhList = [Select id, c2g__MatchingReference__c, c2g__Account__c, c2g__Period__c
                                                        From c2g__codaCashMatchingHistory__c limit 1];
        if(mhList.size() > 0 ){
        JVCO_CashUnmatchingLogic.undoCashMatching(mhList.get(0));
        }
        
        //unmatch
        System.enqueueJob(new JVCO_CashUndoMatching_Queueable(mhList));
    }    
}