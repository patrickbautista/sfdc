@isTest
private class JVCO_NotesTriggerHandlerTest{
    
    private static Account licAcc;
     
    @testSetup
    public static void setupData(){
        Profile p = [select id from Profile where Name = 'Licensing User' limit 1];
        User runningUser = new User();
        User systemUser = new User();
        UserRole uRole = new UserRole();

        uRole.DeveloperName = 'JVCO_Generic_Test_User'; 
        uRole.Name = 'Generic Test User';
        insert uRole;

        runningUser = new User(
            Alias = 'athr',
            Email='JVCO_AccountTriggerHandlerTest1@gmail.com', 
            EmailEncodingKey='UTF-8',
            FirstName = 'Generic', 
            LastName='User',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            UserRoleId = uRole.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='guuserstand3541987*@testorg.com',
            Division='Legal', Department='Legal'
        );
        
        insert runningUser;

        systemUser = new User(
            Alias = 'athr',
            Email='JVCO_systemcreation@gmail.com', 
            EmailEncodingKey='UTF-8',
            FirstName = 'Generic', 
            LastName='User',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            UserRoleId = uRole.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='guuserstand3541234822@testorg.com',
            Division='Legal', Department='Legal'
        );

        insert systemUser;

        PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors' limit 1];
        PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = ps.id, AssigneeId = systemUser.id);
        insert psa;
    }
    
    static void prepareTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        Test.startTest();
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        insert glaList;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;

        User runUser = [SELECT Id FROM User where Email = 'JVCO_systemcreation@gmail.com' Limit 1];

        System.runAs(runUser){

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc.JVCO_In_Enforcement__c = true;
        licAcc.JVCO_Credit_Status__c = 'Enforcement - Debt Recovery';
        insert licAcc;

        }
        
        Test.stopTest();
    }
    
    private static testMethod void testInsertEventWithRestriction(){
        prepareTestData();

        
        User runUser = [SELECT Id, ProfileId FROM User where Alias = 'athr' Limit 1];
        System.assertNotEquals(runUser, null);
        Boolean errorExists = false;
        
        System.runAs(runUser){

            Note noteRec = new Note();
            noteRec.Title = 'Meeting';
            noteRec.Body = 'Meeting about escalation.';
            noteRec.ParentId = licAcc.Id;
            noteRec.OwnerId = runUser.Id;
            
            try{
                insert noteRec;
            }
            catch(Exception e){
                errorExists = true;
            }
        }
        
        //System.assertEquals(true, errorExists);
    }
    
    private static testMethod void testEditEventWithRestriction(){
        prepareTestData();

        User runUser = [SELECT Id, ProfileId FROM User where Email = 'JVCO_systemcreation@gmail.com' Limit 1];
        System.assertNotEquals(runUser, null);
        Boolean errorExists = false;
        
        Note noteRec = new Note();
        noteRec.Title = 'Meeting';
        noteRec.Body = 'Meeting about escalation.';
        noteRec.ParentId = licAcc.Id;
        noteRec.OwnerId = runUser.Id;
        insert noteRec;

        System.runAs(runUser){
            try{
                JVCO_NotesTriggerHandler.onBeforeUpdate(new List<Note>{noteRec});
            }
            catch(Exception e){
                errorExists = true;
            }

            System.assertEquals(false, errorExists);
            noteRec.Body = 'Meeting about Project.';
            update noteRec;
        }
        
    }
    
    private static testMethod void testDeleteEventWithRestriction(){
        prepareTestData();

        User runUser = [SELECT Id, ProfileId FROM User where Email = 'JVCO_systemcreation@gmail.com' Limit 1];
        System.assertNotEquals(runUser, null);
        Boolean errorExists = false;
		Note noteRec = new Note();
        noteRec.Title = 'Meeting';
        noteRec.Body = 'Meeting about escalation.';
        noteRec.ParentId = licAcc.Id;
        noteRec.OwnerId = runUser.Id;
        insert noteRec;  
        
        
        System.runAs(runUser){ 
            try{
                JVCO_NotesTriggerHandler.onBeforeDelete(new List<Note>{noteRec});
            }
            catch(Exception e){
                errorExists = true;
            }

            System.assertEquals(false, errorExists);
            delete noteRec;
        }
           
    }
}