@isTest
private class JVCO_CampaignMemberTest
{
	@testSetup static void setUpTestData(){

		Lead testLead = new lead(firstname='07042017', lastname='TestLead', Company = 'Test Company');
		insert testLead;

		Contact testContact = new Contact(firstname = 'Contact', lastName='testContact');
		insert testContact;

		Date startDate = Date.newInstance(2016, 10, 10);
        Date endDate = Date.newInstance(2016, 11, 11);

		Campaign testCamp = new campaign(name='TestCamp' , Type='Churn',
                JVCO_Campaign_Channel__c='Telesales', JVCO_Campaign_Category__c='Shop',
                JVCO_Telesales_Partner__c='Ventrica', StartDate=startDate, EndDate=endDate);
		insert testCamp;

		

	}

	@isTest static void afterInsert_test(){

		test.starttest();

		list<Lead> listLead = new list<Lead>();
		list<Lead> listLead_Tele1 = new list<Lead>();
		list<Campaign> listCampaign = new list<Campaign>();
		list<campaign> listCampaign_tele1 = new list<campaign>();
		list<Contact> listContact = new list<Contact>();

		list<Group> listGroup = new list<Group>([SELECT Id, Name FROM Group
                                             WHERE Name = 'Ventrica']);

		list<Group> listGroupTele1 = new list<Group>([SELECT Id, Name FROM Group
                                             WHERE Name = 'MarketMakers']);

		listLead = [select id, ownerId from lead where lastname = 'TestLead'];
		listCampaign = [select id, JVCO_Campaign_Channel__c, JVCO_Telesales_Partner__c from campaign where name = 'TestCamp'];
		listContact = [select id from contact where lastName = 'testContact'];

		id idLead = listLead.get(0).id;
		id idCampaign = listCampaign.get(0).id;
		id idContact = listContact.get(0).id;
		id idGroup = listGroup.get(0).id;
		id idPrevLeadOwner = listLead.get(0).ownerId;
		id idPrevLeadOwnerTele1;
		id idGroupTele1 = listGroupTele1.get(0).id;

		system.debug('List Lead' + idLead);
		system.debug('Campaign Lead' + idCampaign);
		system.debug('Contact ' + idContact);
		system.debug('Group ' + idGroup);
		system.debug('Group Tele 1 ' + idGroupTele1);

		list<CampaignMember> listCampMember = new list<CampaignMember>();

		listCampMember.add(new CampaignMember(LeadId = idLead, campaignid = idCampaign));
		listCampMember.add(new Campaignmember(Contactid = idContact, campaignid = idCampaign));

		system.debug(listLead.get(0));
		
		insert listCampMember;
		
		
		listLead = [select id, ownerId from lead where lastname = 'TestLead'];
		system.debug('Tele 2' + listLead);
		idPrevLeadOwnerTele1 = listLead.get(0).ownerId;

		system.assertEquals(listLead.get(0).ownerId, idGroup);
		system.assertNotEquals(listLead.get(0).ownerId, idPrevLeadOwner);

		listCampaign.get(0).jvco_telesales_partner__c = 'MarketMakers';
		update listCampaign;
		listCampaign_tele1 = [select id, JVCO_Campaign_Channel__c, JVCO_Telesales_Partner__c from campaign where name = 'TestCamp'];
		system.debug('tele1 camp '+ listCampaign_tele1);

		

		//listLead_Tele1 = [select id, ownerId from lead where lastname = 'TestLead'];
		//system.debug('tele 1' + listLead_Tele1);

		test.stoptest();

		list<Lead> listLead_TeleTest = new list<Lead>();
		listLead_TeleTest = [select id, ownerId from lead where lastname = 'TestLead'];
		system.debug('tele 1' + listLead_TeleTest);

		system.assertEquals(listLead_TeleTest.get(0).ownerId, idGroupTele1);
		system.assertNotEquals(listLead_TeleTest.get(0).ownerId, idPrevLeadOwnerTele1);
		
	}

		@isTest static void afterInsert_test_withErrors(){

		test.starttest();

		list<Lead> listLead = new list<Lead>();
		list<Lead> listLead_Tele1 = new list<Lead>();
		list<Campaign> listCampaign = new list<Campaign>();
		list<campaign> listCampaign_tele1 = new list<campaign>();
		list<Contact> listContact = new list<Contact>();

		list<Group> listGroup = new list<Group>([SELECT Id, Name FROM Group
                                             WHERE Name = 'Ventrica']);

		list<Group> listGroupTele1 = new list<Group>([SELECT Id, Name FROM Group
                                             WHERE Name = 'MarketMakers']);

		listLead = [select id, ownerId from lead where lastname = 'TestLead'];
		listCampaign = [select id, JVCO_Campaign_Channel__c, JVCO_Telesales_Partner__c from campaign where name = 'TestCamp'];
		listContact = [select id from contact where lastName = 'testContact'];

		id idLead = listLead.get(0).id;
		id idCampaign = listCampaign.get(0).id;
		id idContact = listContact.get(0).id;
		id idGroup = listGroup.get(0).id;
		id idPrevLeadOwner = listLead.get(0).ownerId;
		id idPrevLeadOwnerTele1;
		id idGroupTele1 = listGroupTele1.get(0).id;

		system.debug('List Lead' + idLead);
		system.debug('Campaign Lead' + idCampaign);
		system.debug('Contact ' + idContact);
		system.debug('Group ' + idGroup);
		system.debug('Group Tele 1 ' + idGroupTele1);

		list<CampaignMember> listCampMember = new list<CampaignMember>();

		listCampMember.add(new CampaignMember(LeadId = idLead, campaignid = idCampaign));
		listCampMember.add(new Campaignmember(Contactid = idContact, campaignid = idCampaign));

		system.debug(listLead.get(0));

		JVCO_CampaignSharedLogic.shouldInvalidate = TRUE;
		
		insert listCampMember;
		
		
		listLead = [select id, ownerId from lead where lastname = 'TestLead'];
		system.debug('Tele 2' + listLead);
		idPrevLeadOwnerTele1 = listLead.get(0).ownerId;

		//system.assertEquals(listLead.get(0).ownerId, idGroup);
		//system.assertNotEquals(listLead.get(0).ownerId, idPrevLeadOwner);

		listCampaign.get(0).jvco_telesales_partner__c = 'MarketMakers';
		update listCampaign;
		listCampaign_tele1 = [select id, JVCO_Campaign_Channel__c, JVCO_Telesales_Partner__c from campaign where name = 'TestCamp'];
		system.debug('tele1 camp '+ listCampaign_tele1);

		

		//listLead_Tele1 = [select id, ownerId from lead where lastname = 'TestLead'];
		//system.debug('tele 1' + listLead_Tele1);

		test.stoptest();

		list<Lead> listLead_TeleTest = new list<Lead>();
		listLead_TeleTest = [select id, ownerId from lead where lastname = 'TestLead'];
		system.debug('tele 1' + listLead_TeleTest);

		JVCO_CampaignSharedLogic.shouldInvalidate = FALSE;

		//system.assertEquals(listLead_TeleTest.get(0).ownerId, idGroupTele1);
		//system.assertNotEquals(listLead_TeleTest.get(0).ownerId, idPrevLeadOwnerTele1);
		
	}
}