/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_TaskTriggerHandler.cls 
   Description:     Handler Class for JVCO_TaskTrigger
   Test class:      JVCO_TaskTriggerHandlerTest.cls 

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
  05-Apr-2018       0.1         Accenture-recuerdo.b.bregente     Initial draft           
  ---------------------------------------------------------------------------------------------------------- */
Public Class JVCO_TaskTriggerHandler{
    
    public static void onBeforeInsert(List<Task> tasks){
        List<Task> tasksToProcessForRestriction = new List<Task>();
        Set<Id> accountIdsRestriction = new Set<Id>();
        
        for(Task taskRec : tasks){
            if(taskRec.WhatId <> null){
                accountIdsRestriction.add(taskRec.WhatId);
                tasksToProcessForRestriction.add(taskRec);
            }
        }
        restrictOnInfringementOrEnforcement(tasksToProcessForRestriction, accountIdsRestriction);
    }
    
    public static void onBeforeUpdate(List<Task> tasks){
        List<Task> tasksToProcessForRestriction = new List<Task>();
        Set<Id> accountIdsRestriction = new Set<Id>();
        
        for(Task taskRec : tasks){
            if(taskRec.WhatId <> null){
                accountIdsRestriction.add(taskRec.WhatId);
                tasksToProcessForRestriction.add(taskRec);
            }
        }
        restrictUpdateAndDelete(tasksToProcessForRestriction, accountIdsRestriction, 'Updating');
    }
    
    public static void onBeforeDelete(List<Task> tasks){
        List<Task> tasksToProcessForRestriction = new List<Task>();
        Set<Id> accountIdsRestriction = new Set<Id>();
        
        for(Task taskRec : tasks){
            if(taskRec.WhatId <> null){
                accountIdsRestriction.add(taskRec.WhatId);
                tasksToProcessForRestriction.add(taskRec);
            }
        }
        restrictUpdateAndDelete(tasksToProcessForRestriction, accountIdsRestriction, 'Deleting');
    }
    
    private static void restrictOnInfringementOrEnforcement(List<Task> tasks,Set<Id> accountIdsRestriction){
        Map<Id, Account> restrictedAccountsMap = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN :accountIdsRestriction AND 
                                                    (JVCO_In_Enforcement__c = true OR JVCO_In_Infringement__c = true) AND 
                                                    (JVCO_Credit_Status__c = 'Enforcement - Debt Recovery' OR JVCO_Credit_Status__c = 'Enforcement - Infringement')]);
        
                                                            
        Boolean isRestricted = (restrictedAccountsMap.size() > 0 && checkUserIfRestricted());
       
        for(Task taskRec : tasks){
            if(isRestricted && restrictedAccountsMap.containsKey(taskRec.WhatId) && 
                (taskRec.Subject.containsIgnoreCase('Email:') || !taskRec.Subject.containsIgnoreCase('Call'))){
                String operation = 'Sending email';
                if(!taskRec.Subject.containsIgnoreCase('Email:')){ operation = 'Creating Tasks';}
                taskRec.addError(operation + ' on Accounts marked "In Enforcement" or "In Infringement" is not allowed.');
            }
        }
    }
    
    private static void restrictUpdateAndDelete(List<Task> tasks,Set<Id> accountIdsRestriction, String operation){
        Map<Id, Account> restrictedAccountsMap = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN :accountIdsRestriction AND 
                                                    (JVCO_In_Enforcement__c = true OR JVCO_In_Infringement__c = true) AND 
                                                    (JVCO_Credit_Status__c = 'Enforcement - Debt Recovery' OR JVCO_Credit_Status__c = 'Enforcement - Infringement')]);
        
                                                            
        Boolean isRestricted = (restrictedAccountsMap.size() > 0 && checkUserIfRestricted());
       
        for(Task taskRec : tasks){
            if(isRestricted && restrictedAccountsMap.containsKey(taskRec.WhatId)){
                taskRec.addError(operation + ' Tasks on Accounts marked "In Enforcement" or "In Infringement" is not allowed.');
            }
        }
    }
    
    
    private static Boolean checkUserIfRestricted(){
        String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
        Map<Id,PermissionSet> enAdPermSet = new Map<Id,PermissionSet>([SELECT Id FROM PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors']);
        List<PermissionSetAssignment> permissionSetList = [SELECT Id FROM PermissionSetAssignment WHERE 
                                                            AssigneeId = :UserInfo.getUserId() AND PermissionSetId IN :enAdPermSet.keySet()];
                                                            
        return (profileName <> 'System Administrator' && permissionSetList.isEmpty());
    }
}