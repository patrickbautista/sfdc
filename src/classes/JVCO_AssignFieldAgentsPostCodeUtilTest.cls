@isTest
private class JVCO_AssignFieldAgentsPostCodeUtilTest
{
	@testSetup
    private static void setupData(){
		
	    JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
   		ven.JVCO_Field_Visit_Requested__c  = true;
        ven.JVCO_Field_Visit_Requested_Date__c = date.today();
        ven.JVCO_Postcode__c = 'W2 1HQ'; 

        Test.startTest();
        insert ven; 
        Test.stopTest();
    }
	@isTest
	static void testAssignFieldsPostCodeUtilTest()
	{

		try
		{
			JVCO_Venue__c ven = [SELECT JVCO_Postcode__c FROM JVCO_Venue__c order by CreatedDate DESC limit 1];
			ven.JVCO_Postcode__c = 'YY1';

			Test.startTest();
			update ven;
			Test.stopTest();

			List<JVCO_Venue__c> l_ven = [SELECT JVCO_Postcode__c FROM JVCO_Venue__c];
			System.assert(!l_ven.isEmpty());
		}
		catch (exception ex)
		{
			//assert that record throws an error whenever it is assigned to Unresolved Addresses Queue
            //System.Assert(ex.getMessage().contains('The address of this Venue cannot be resolved.'));
		}
	}
}