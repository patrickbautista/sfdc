/* ----------------------------------------------------------------------------------------------
   Name: JVCO_codaBankStatementLineItemHandler.cls 
   Description: Handler class for c2g__codaBankStatementLineItem__c object

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
   07-Dec-2016   0.1        kristoffer.d.martin Intial creation
   19-Dec-2017  0.2         chun.yin.tang       Add disabling trigger to Bank Statement Line Handler
   21-Mar-2019  0.4         patrick.t.bautista  GREEN-34440 - Refactor class to allow SIN digits > 6
   24-Apr-2019  0.5         patrick.t.bautista  GREEN-34540 - Amended Invoice legacy number process
  ----------------------------------------------------------------------------------------------- */
public class JVCO_codaBankStatementLineItemHandler {
    
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_codaBankStatementLineItemTrigger__c : false;    
    
    public static void onAfterInsert(List<c2g__codaBankStatementLineItem__c> bankStatementLineItem){
        
        if(!skipTrigger) 
        {
            JVCO_codaBankStatementLineItemAllocation(bankStatementLineItem);
        }
    }
    
    
    /* ------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: This method handles afterInsert and afterUpdate trigger events for c2g__codaBankStatementLineItem__c
    Input: List<c2g__codaBankStatementLineItem__c>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    07-Dec-2016 kristoffer.d.martin Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static void JVCO_codaBankStatementLineItemAllocation(List<c2g__codaBankStatementLineItem__c> bankStatementLineItem)
    {
        List<c2g__codaBankStatementLineItem__c> bankStatementLineList = new List<c2g__codaBankStatementLineItem__c>();
        Account accountUnidentified = [SELECT Id, Name FROM Account WHERE Name =: Label.JVCO_Account_Unidentified];
        Map<String, String> refAndAccountIdMap = new Map<String, String>();
        Map<String, String> refAndsalesInvoiceMap = new Map<String, String>();
        List<String> refInvoiceLegacyList = new List<String>();
        Map<String, Boolean> refToIsExcludeMap = new Map<String, Boolean>();
        // Loop from the created Bank Statement Line Item records
        for (c2g__codaBankStatementLineItem__c item : bankStatementLineItem) 
        {
            if (item.c2g__Reference__c != null) //GREEN-21184 - kristoffer.d.martin - 08/08/2017
            {
                String reference = item.c2g__Reference__c;
                if(item.c2g__Reference__c.contains('``')){
                    refToIsExcludeMap.put(item.Name, item.c2g__Reference__c.split('``').get(1).tolowercase().equalsIgnoreCase('true') ? true : false);
                    reference = item.c2g__Reference__c.split('``').get(0);
                    system.debug('item.c2g__Reference__c'+reference);
                }
                if(reference.startsWith('PPL:') || reference.startsWith('PRS:INV:'))
                {
                    refAndsalesInvoiceMap.put(item.Name, reference);
                    refInvoiceLegacyList.add(reference);
                }
                else if (!reference.contains('SIN')) 
                {
                    refAndAccountIdMap.put(item.Name, reference);
                }
                else
                {
                    List<String> refSplit = reference.split('SIN');
                    if(refSplit.size() > 1 && !reference.startsWith('SIN'))
                    {
                        refAndAccountIdMap.put(item.Name, refSplit[0]);
                        refAndsalesInvoiceMap.put(item.Name, 'SIN' + refSplit[1]);
                    }
                    else
                    {
                        refAndsalesInvoiceMap.put(item.Name, reference);
                    }
                }
            }
        }

        Map<String, Account> accountMap = new Map<String, Account>();
        Map<String, c2g__codaInvoice__c> salesInvoiceMap = new Map<String, c2g__codaInvoice__c>();
        //Get Account Map
        if (!refAndAccountIdMap.isEmpty())
        {
            for(Account acc: [SELECT Id, Name, AccountNumber, c2g__CODAExternalId__c 
                              FROM Account 
                              WHERE c2g__CODAExternalId__c  IN: refAndAccountIdMap.values()])
            {
                accountMap.put(acc.c2g__CODAExternalId__c, acc);
            }
        }
        //Get Sales invoice Map
        if (!refAndsalesInvoiceMap.isEmpty())
        {
            for(c2g__codaInvoice__c invoice : [SELECT Id, Name, c2g__Account__c, JVCO_Invoice_Legacy_Number__c
                                               FROM c2g__codaInvoice__c 
                                               WHERE Name IN :refAndsalesInvoiceMap.values()
                                               OR JVCO_Invoice_Legacy_Number__c IN :refAndsalesInvoiceMap.values()])
            {
                if(refInvoiceLegacyList.contains(invoice.JVCO_Invoice_Legacy_Number__c))
                {
                    salesInvoiceMap.put(invoice.JVCO_Invoice_Legacy_Number__c, invoice);
                }
                else
                {
                    salesInvoiceMap.put(invoice.Name, invoice);
                }
            }
        }
        // Loop from the created Bank Statement Line Item records
        if (!refAndAccountIdMap.isEmpty() || !refAndsalesInvoiceMap.isEmpty())
        {
            for (c2g__codaBankStatementLineItem__c item : [SELECT id, Name, c2g__Reference__c, JVCO_Account__c,
                                                           JVCO_SalesInvoice__c, JVCO_Processed__c
                                                           FROM c2g__codaBankStatementLineItem__c 
                                                           WHERE id IN: bankStatementLineItem]) 
            {
                String tempReference = item.c2g__Reference__c;
                
                if(refToIsExcludeMap.containsKey(item.Name)){
                    item.JVCO_Exclude__c = refToIsExcludeMap.get(item.Name);
                    item.c2g__Reference__c = item.c2g__Reference__c.split('``').get(0);
                }
                
                //Sales invoice, Sales invoice with account and legacy invoice references
                if(refAndsalesInvoiceMap.containsKey(item.Name) && (item.c2g__Reference__c.contains('SIN') || 
                    item.c2g__Reference__c.startsWith('PPL:') || item.c2g__Reference__c.startsWith('PRS:INV:')))
                {
                    List<String> refSplit = item.c2g__Reference__c.split('SIN');
                    if(refSplit.size() > 1 && !item.c2g__Reference__c.startsWith('SIN'))
                    {//Check if account is related to Sales invoice if not put unidentified to account
                        item.JVCO_Account__c = accountMap.containsKey(refSplit[0]) && salesInvoiceMap.containsKey('SIN'+refSplit[1]) 
                            && (accountMap.get(refSplit[0]).id == salesInvoiceMap.get('SIN'+refSplit[1]).c2g__Account__c)
                            ? accountMap.get(refSplit[0]).id : accountUnidentified.id;
                        item.JVCO_SalesInvoice__c = salesInvoiceMap.containsKey('SIN'+refSplit[1]) ? salesInvoiceMap.get('SIN' + refSplit[1]).id : NULL;
                    }
                    else
                    {//Account and Sales invoice are match
                        if(salesInvoiceMap.containsKey(item.c2g__Reference__c))
                        {
                            item.JVCO_Account__c = salesInvoiceMap.get(item.c2g__Reference__c).c2g__Account__c;
                            item.JVCO_SalesInvoice__c = salesInvoiceMap.get(item.c2g__Reference__c).id;
                        }
                    }
                }
                //Account only
                else if(refAndAccountIdMap.containsKey(item.Name) && accountMap.containsKey(item.c2g__Reference__c)) 
                {
                    item.JVCO_Account__c = accountMap.get(item.c2g__Reference__c).id;
                }
                else{//Without Account and Sales invoice
                    item.JVCO_Account__c = accountUnidentified.id;
                }
                item.JVCO_Processed__c = true;
                if(item.JVCO_Account__c == null)
                {
                    item.JVCO_Account__c = accountUnidentified.id;
                }
                if(Test.isRunningTest()){
                    item.c2g__Reference__c = tempReference;
                }
                bankStatementLineList.add(item);
            }
            update bankStatementLineList;
        }
    }
}