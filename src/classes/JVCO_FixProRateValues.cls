/* ----------------------------------------------------------------------------------------------
   Name: JVCO_FixProRateValues
   Description: Batch that sets prorate multiplier of subscriptions to 1 affecting computation of net price and quantity

   Date         Version     Author               Summary of Changes 
   -----------  -------     -----------------    -----------------------------------------
  05-Jan-2018   0.1      Robert John B. Lacatan   Intial creation
  ----------------------------------------------------------------------------------------------- */
global class JVCO_FixProRateValues implements Database.Batchable<sObject> {

    global JVCO_FixProRateValues() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        JVCO_FixProRateValuesCustomSetting__c settings = JVCO_FixProRateValuesCustomSetting__c.getValues('FixProrate');
        
        return Database.getQueryLocator([SELECT id, JVCO_NetTotal__c,SBQQ__NetPrice__c,SBQQ__ListPrice__c,SBQQ__ProrateMultiplier__c,SBQQ__Quantity__c,JVCO_NOT_Renewable__c,ChargeYear__c ,SBQQ__RenewalQuantity__c, Lastmodifieddate, SBQQ__Product__r.ProductCode, createddate 
        FROM SBQQ__Subscription__c 
        WHERE SBQQ__Product__r.Proratable__c = false 
        AND SBQQ__Product__r.CustomMinMaxCalcType__c != :settings.Min__c 
        AND SBQQ__Product__r.CustomMinMaxCalcType__c != :settings.Max__c 
        AND SBQQ__ProrateMultiplier__c < 1 
        AND SBQQ__Product__r.ProductCode != :settings.MRPPPL__c 
        AND SBQQ__Product__r.ProductCode != :settings.MRPVPL__c  
        AND SBQQ__Product__r.ParentProduct__c = false 
        AND JVCO_NetTotal__c!=0
        AND CreatedById = :settings.DataMigId__c 
        ]);
    }

    global void execute(Database.BatchableContext BC, List<SBQQ__Subscription__c> uSubs)
    {
        for(SBQQ__Subscription__c uList:uSubs ){
            uList.SBQQ__ProrateMultiplier__c = 1;
            uList.SBQQ__NetPrice__c = uList.SBQQ__ListPrice__c;
            uList.SBQQ__Quantity__c = uList.JVCO_NetTotal__c/uList.SBQQ__ListPrice__c;

            if(uList.ChargeYear__c=='Current Year' && !uList.JVCO_NOT_Renewable__c){
                uList.SBQQ__RenewalQuantity__c = uList.SBQQ__Quantity__c;
            }

        }
        
        update uSubs;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
    
}