/* ----------------------------------------------------------------------------------------------
   Name: JVCO_codaBankStatementLineItemHandler.cls 
   Description: Handler class for c2g__codaBankStatementLineItem__c object

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
   19-Dec-2016  0.1         kristoffer.d.martin Intial creation
   04-Nov-2017  0.2         franz.g.a.dimaapi   GREEN-25465 - Fixed Soql 101
   08-Jul-2019  0.3         franz.g.a.dimaapi   GREEN-34731 - Fixed Double Post Batch
----------------------------------------------------------------------------------------------- */
public class JVCO_CashEntriesPostingLogic
{
    private ApexPages.StandardSetController standardController;

    public JVCO_CashEntriesPostingLogic(ApexPages.StandardSetController standardController) 
    {
        this.standardController = standardController;   
    }

    public PageReference postCashEntries()
    {   
        if(!checkIfUserHasPendingPostCashBatch())
        {
            final JVCO_General_Settings__c GeneralSettings = JVCO_General_Settings__c.getInstance();
            final Decimal CASHENTRY_RECORDS_PER_JOBS = GeneralSettings != null ? GeneralSettings.JVCO_Cash_Post_Queue_Rec_Per_Jobs__c : 55;

            //Selected BankStatement Lines    
            List<c2g__codaBankStatementLineItem__c> selectedBankStatementLineItems = (List<c2g__codaBankStatementLineItem__c>) standardController.getSelected();
            Set<Id> cashEntryIdSet = new Set<Id>();
            for(c2g__codaBankStatementLineItem__c bsli : [SELECT Id, JVCO_Cash_Entry__c
                                                             FROM c2g__codaBankStatementLineItem__c
                                                             WHERE Id IN :selectedBankStatementLineItems 
                                                             AND JVCO_Status__c = 'In Progress'
                                                             AND JVCO_Cash_Entry__c != NULL])
            {
                cashEntryIdSet.add(bsli.JVCO_Cash_Entry__c);
            }

            if(!JVCO_BatchJobHelper.checkLargeJobs(cashEntryIdSet.size()))
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.JVCO_BatchJobHelperCannotProcess));
                return null;
            }else if(!cashEntryIdSet.isEmpty())
            {
                Database.executeBatch(new JVCO_CashEntriesPostingLogicBatch(cashEntryIdSet, true), Integer.valueOf(CASHENTRY_RECORDS_PER_JOBS));
                return standardController.cancel();
            }else
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please Select In Progress Cash Entries'));
                return null;
            }
        }else
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.JVCO_BatchJobHelperCannotProcess));
            return null;
        }
    }

    private Boolean checkIfUserHasPendingPostCashBatch()
    {
        List<AsyncApexJob> asynchApexJobList = [SELECT ID
                                                FROM AsyncApexJob
                                                WHERE ApexClass.Name = 'JVCO_CashEntriesPostingLogicBatch'
                                                AND CreatedById = :UserInfo.getUserId()
                                                AND Status IN ('Holding','Queued','Preparing','Processing')];
        return !asynchApexJobList.isEmpty();
    }
}