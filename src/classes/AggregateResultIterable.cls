public class AggregateResultIterable implements Iterable<AggregateResult>
{
    private String query;
    private Date JVCO_LAST_N_DAYS;
    private Set<Id> sInvIdSet;
    private Set<Id> orderIdSet;
    
    public AggregateResultIterable(String query, Date JVCO_LAST_N_DAYS)
    {
        this.JVCO_LAST_N_DAYS = JVCO_LAST_N_DAYS;
        this.query = query;
    }
    
    public AggregateResultIterable(String query, Set<Id> sInvIdSet)
    {
        this.sInvIdSet = sInvIdSet;
        this.query = query;
    }
    
    public AggregateResultIterable(String query)
    {
        this.query = query;
    }
    
    public AggregateResultIterable(Set<Id> orderIdSet, String query)
    {
        this.query = query;
        this.orderIdSet = orderIdSet;
    }
    
    public Iterator<AggregateResult> Iterator()
    { 
        return new AggregateResultIterator(query, JVCO_LAST_N_DAYS, sInvIdSet, orderIdSet);
    }
}