/* ----------------------------------------------------------------------------------------------
   Name: JVCO_AffiliationMerge.cls 
   Description: After delete triggers will be placed on the Account and Venue objects.

   Date         Version     Author                 Summary of Changes 
   -----------  -------     -----------------      -----------------------------------------
   01-Jun-2017  0.1         kristoffer.d.martin    Intial creation
   12-Jul-2018  0.2         jules.osberg.a.pablo   GREEN-32348 added method to reparent objects to the master affiliation before affiliation deletion
   25-Oct-2018  0.3         rhys.j.c.dela.cruz     GREEN-32348 - Changed delete statement to Database.delete to insert Error Log if concurrency occurs
----------------------------------------------------------------------------------------------- */
public with sharing class JVCO_AffiliationMerge {

    public class MergeException extends Exception {}
    public static Boolean isMerge = FALSE;
    public static Boolean passesAffMerge = FALSE;

    public JVCO_AffiliationMerge() {
        
    }

    /* ----------------------------------------------------------------------------------------------------------
    Author:         kristoffer.d.martin
    Company:        Accenture
    Description:    
    Inputs:         List of Accounts, Map of Accounts
    Returns:        
    <Date>          <Authors Name>                      <Brief Description of Change> 
    01-Jun-2017     kristoffer.d.martin                 Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/ 
    @future   
    public static void mergeAccountAffiliation(List<Id> accountIdList)
    {

        List<JVCO_Affiliation__c> affiliationListForDeletion = new List<JVCO_Affiliation__c>();
        List<JVCO_Affiliation__c> affiliationListForUpdate = new List<JVCO_Affiliation__c>();
        
        JVCO_Affiliation__c affiliationForMerge = new JVCO_Affiliation__c();
        JVCO_Affiliation__c masterRecord;
        Set<Id> customerIdSet = new Set<Id>();
        passesAffMerge = true;

        //List Error Log for deletion
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();

        List<JVCO_Affiliation__c> affiliationList = [SELECT Id, Name, JVCO_Account__c, JVCO_Venue__c, JVCO_Start_Date__c, 
                                                            JVCO_End_Date__c, JVCO_Omit_Quote_Line_Group__c, 
                                                            JVCO_Affiliation_Temp_External_ID__c, JVCO_Closure_Reason__c,
                                                            JVCO_Revenue_Type__c, JVCO_Status__c, JVCO_Account__r.JVCO_Customer_Account__c 
                                                     FROM JVCO_Affiliation__c 
                                                     WHERE JVCO_Account__c in :accountIdList
                                                     ORDER BY JVCO_Start_Date__c ASC];

        Map<Id,List<JVCO_Affiliation__c>> venueMap = new Map<Id,List<JVCO_Affiliation__c>>(); 

        if (!affiliationList.isEmpty())
        {
            for (JVCO_Affiliation__c aff : affiliationList)
            {
                if (!venueMap.containsKey(aff.JVCO_Venue__c))
                {
                    List<JVCO_Affiliation__c> affList = new List<JVCO_Affiliation__c>();
                    affList.add(aff);
                    venueMap.put(aff.JVCO_Venue__c, affList);
                }
                else
                {
                    List<JVCO_Affiliation__c> objVenueList = venueMap.get(aff.JVCO_Venue__c);
                    objVenueList.add(aff);
                }
                
            }
        }

        if (venueMap.size() > 0) 
        {
            for (Id affId : venueMap.keySet())
            {
                List<JVCO_Affiliation__c> affList = venueMap.get(affId);
                if (affList.size() > 0) 
                {
                    Integer counter = 1;
                    for (JVCO_Affiliation__c aff : affList)
                    {
                        if (counter == 1)
                        {
                            masterRecord = aff;
                            counter++;
                        }
                        else 
                        {
                            if(masterRecord.JVCO_Venue__c == aff.JVCO_Venue__c)
                            {
                                if (aff.JVCO_End_Date__c != null && masterRecord.JVCO_End_Date__c == null)
                                {
                                    masterRecord.JVCO_End_Date__c = aff.JVCO_End_Date__c;
                                }
                                else if (aff.JVCO_End_Date__c != null && masterRecord.JVCO_End_Date__c != null)
                                {
                                    if (aff.JVCO_End_Date__c > masterRecord.JVCO_End_Date__c)
                                    {
                                        masterRecord.JVCO_End_Date__c = aff.JVCO_End_Date__c;
                                    }
                                }

                                if (aff.JVCO_Status__c != null && masterRecord.JVCO_Status__c != null)
                                {
                                    if (aff.JVCO_Status__c != masterRecord.JVCO_Status__c)
                                    {
                                        if (aff.JVCO_Affiliation_Temp_External_ID__c != null && String.valueOf(aff.JVCO_Affiliation_Temp_External_ID__c).startsWith('PRS'))
                                        {
                                            masterRecord.JVCO_Status__c = aff.JVCO_Status__c;
                                        }
                                    }
                                }

                                if (aff.JVCO_Closure_Reason__c != null && masterRecord.JVCO_Closure_Reason__c != null)
                                {
                                    if (aff.JVCO_Closure_Reason__c != masterRecord.JVCO_Closure_Reason__c)
                                    {
                                        if (aff.JVCO_Affiliation_Temp_External_ID__c != null && String.valueOf(aff.JVCO_Affiliation_Temp_External_ID__c).startsWith('PRS'))
                                        {
                                            masterRecord.JVCO_Closure_Reason__c = aff.JVCO_Closure_Reason__c;
                                        }
                                    }
                                }

                                if (aff.JVCO_Revenue_Type__c != null && masterRecord.JVCO_Revenue_Type__c != null)
                                {
                                    if (aff.JVCO_Revenue_Type__c != masterRecord.JVCO_Revenue_Type__c)
                                    {
                                        if (aff.JVCO_Affiliation_Temp_External_ID__c != null && String.valueOf(aff.JVCO_Affiliation_Temp_External_ID__c).startsWith('PRS'))
                                        {
                                            masterRecord.JVCO_Revenue_Type__c = aff.JVCO_Revenue_Type__c;
                                        }
                                    }
                                }

                                if (aff.JVCO_Omit_Quote_Line_Group__c == true || masterRecord.JVCO_Omit_Quote_Line_Group__c == true)
                                {
                                     masterRecord.JVCO_Omit_Quote_Line_Group__c = true;
                                }
                                //-----Jules Test--7/10/2018--------set reparenting La field here
                                //masterRecord.JVCO_NewMergeAccountId__c = masterRecord.JVCO_Account__c;
                                //aff.JVCO_NewMergeAccountId__c = masterRecord.JVCO_Account__c;

                                //customerIdSet.add(aff.JVCO_Account__r.JVCO_Customer_Account__c);
                                
                                affiliationListForDeletion.add(aff);
                            }
                        }
                    }
                    affiliationListForUpdate.add(masterRecord);
                }                    
            }
        }

        Savepoint sp = Database.setSavepoint();
        try 
        {
            if(affiliationListForUpdate != null){
                update affiliationListForUpdate;   

                sp = Database.setSavepoint();
                restructureAffiliation(affiliationListForUpdate, affiliationList);             
            }

            if (!affiliationListForDeletion.isEmpty())
            {
                // Commenting out first because of the issue with quote line groups
                //delete affiliationListForDeletion;

                List<Database.DeleteResult> res = Database.delete(affiliationListForDeletion,false);
                for(Integer i = 0; i < affiliationListForDeletion.size(); i++)
                {
                    Database.DeleteResult srOppList = res[i];
                    sObject origrecord = affiliationListForDeletion[i];
                    if(!srOppList.isSuccess())
                    {
                        //Database.rollback(sp);
                        System.debug('Delete sObject fail: ');
                        for(Database.Error objErr:srOppList.getErrors())
                        {
                            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                            /*
                            errLog.Name = String.valueOf(origrecord.ID);
                            errLog.JVCO_FailedRecordObjectReference__c = String.valueOf(origrecord.ID);
                            errLog.blng__FullErrorLog__c = string.valueof(objErr.getMessage());
                            errLog.blng__ErrorCode__c = string.valueof(objErr.getStatusCode());
                            errLog.JVCO_ErrorBatchName__c = 'JVCO_AffiliationMerge: Error on deleting';
                            */

                            errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(origrecord.ID);
                            errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                            errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                            errLog.ffps_custRem__Message__c = 'JVCO_AffiliationMerge: Error on deleting';


                            errLogList.add(errLog);   

                        }
                    }
                }

                if(!errLogList.isempty())
                {
                    try
                    {
                        insert errLogList;
                    }
                    catch(Exception ex)
                    {
                        System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                         
                    }
                }
            }
        }
        catch (Exception ex)
        {
            System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
            System.debug('Restored reparented Objects and undeleted affiliations');
            Database.rollback(sp);
        }
    }

    /* ----------------------------------------------------------------------------------------------------------
    Author:         kristoffer.d.martin
    Company:        Accenture
    Description:    
    Inputs:         List of Venue, Map of Venue
    Returns:        
    <Date>          <Authors Name>                      <Brief Description of Change> 
    01-Jun-2017     kristoffer.d.martin                 Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    @future 
    public static void mergeVenueAffiliation(List<Id> venueIdList)
    {
        List<JVCO_Affiliation__c> affiliationListForDeletion = new List<JVCO_Affiliation__c>();
        List<JVCO_Affiliation__c> affiliationListForUpdate = new List<JVCO_Affiliation__c>();
        
        JVCO_Affiliation__c affiliationForMerge = new JVCO_Affiliation__c();
        JVCO_Affiliation__c masterRecord;
        passesAffMerge = true;

        List<JVCO_Affiliation__c> affiliationList = [SELECT Id, Name, JVCO_Account__c, JVCO_Venue__c, JVCO_Start_Date__c, 
                                                            JVCO_End_Date__c, JVCO_Omit_Quote_Line_Group__c, 
                                                            JVCO_Affiliation_Temp_External_ID__c, JVCO_Closure_Reason__c,
                                                            JVCO_Revenue_Type__c, JVCO_Status__c 
                                                     FROM JVCO_Affiliation__c 
                                                     WHERE JVCO_Venue__c in :venueIdList
                                                     ORDER BY JVCO_Start_Date__c ASC];

        Map<Id,List<JVCO_Affiliation__c>> accountMap = new Map<Id,List<JVCO_Affiliation__c>>(); 

        //List Error Log for deletion
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();

        if (!affiliationList.isEmpty())
        {
            for (JVCO_Affiliation__c aff : affiliationList)
            {
                if (!accountMap.containsKey(aff.JVCO_Account__c))
                {
                    List<JVCO_Affiliation__c> affList = new List<JVCO_Affiliation__c>();
                    affList.add(aff);
                    accountMap.put(aff.JVCO_Account__c, affList);
                }
                else
                {
                    List<JVCO_Affiliation__c> objVenueList = accountMap.get(aff.JVCO_Account__c);
                    objVenueList.add(aff);
                }
                
            }
        }

        if (accountMap.size() > 0) 
        {
            for (Id affId : accountMap.keySet())
            {
                List<JVCO_Affiliation__c> affList = accountMap.get(affId);
                if (affList.size() > 0) 
                {
                    Integer counter = 1;
                    for (JVCO_Affiliation__c aff : affList)
                    {
                        if (counter == 1)
                        {
                            masterRecord = aff;
                            counter++;
                        }
                        else 
                        {
                            if(masterRecord.JVCO_Account__c == aff.JVCO_Account__c)
                            {
                                if (aff.JVCO_End_Date__c != null && masterRecord.JVCO_End_Date__c == null)
                                {
                                    masterRecord.JVCO_End_Date__c = aff.JVCO_End_Date__c;
                                }
                                else if (aff.JVCO_End_Date__c != null && masterRecord.JVCO_End_Date__c != null)
                                {
                                    if (aff.JVCO_End_Date__c > masterRecord.JVCO_End_Date__c)
                                    {
                                        masterRecord.JVCO_End_Date__c = aff.JVCO_End_Date__c;
                                    }
                                }

                                if (aff.JVCO_Status__c != null && masterRecord.JVCO_Status__c != null)
                                {
                                    if (aff.JVCO_Status__c != masterRecord.JVCO_Status__c)
                                    {
                                        if (aff.JVCO_Affiliation_Temp_External_ID__c != null && String.valueOf(aff.JVCO_Affiliation_Temp_External_ID__c).startsWith('PRS'))
                                        {
                                            masterRecord.JVCO_Status__c = aff.JVCO_Status__c;
                                        }
                                    }
                                }

                                if (aff.JVCO_Closure_Reason__c != null && masterRecord.JVCO_Closure_Reason__c != null)
                                {
                                    if (aff.JVCO_Closure_Reason__c != masterRecord.JVCO_Closure_Reason__c)
                                    {
                                        if (aff.JVCO_Affiliation_Temp_External_ID__c != null && String.valueOf(aff.JVCO_Affiliation_Temp_External_ID__c).startsWith('PRS'))
                                        {
                                            masterRecord.JVCO_Closure_Reason__c = aff.JVCO_Closure_Reason__c;
                                        }
                                    }
                                }

                                if (aff.JVCO_Revenue_Type__c != null && masterRecord.JVCO_Revenue_Type__c != null)
                                {
                                    if (aff.JVCO_Revenue_Type__c != masterRecord.JVCO_Revenue_Type__c)
                                    {
                                        if (aff.JVCO_Affiliation_Temp_External_ID__c != null && String.valueOf(aff.JVCO_Affiliation_Temp_External_ID__c).startsWith('PRS'))
                                        {
                                            masterRecord.JVCO_Revenue_Type__c = aff.JVCO_Revenue_Type__c;
                                        }
                                    }
                                }

                                if (aff.JVCO_Omit_Quote_Line_Group__c == true || masterRecord.JVCO_Omit_Quote_Line_Group__c == true)
                                {
                                     masterRecord.JVCO_Omit_Quote_Line_Group__c = true;
                                }
                                affiliationListForDeletion.add(aff);
                            }
                        }
                    }
                    affiliationListForUpdate.add(masterRecord);
                }                    
            }
        }

        Savepoint sp = Database.setSavepoint();
        try 
        {
            if(affiliationListForUpdate != null){
                update affiliationListForUpdate;

                sp = Database.setSavepoint();
                restructureAffiliation(affiliationListForUpdate, affiliationList);
            }

            if (!affiliationListForDeletion.isEmpty())
            {
                //delete affiliationListForDeletion;

                List<Database.DeleteResult> res = Database.delete(affiliationListForDeletion,false);
                for(Integer i = 0; i < affiliationListForDeletion.size(); i++)
                {
                    Database.DeleteResult srOppList = res[i];
                    sObject origrecord = affiliationListForDeletion[i];
                    if(!srOppList.isSuccess())
                    {
                        //Database.rollback(sp);
                        System.debug('Delete sObject fail: ');
                        for(Database.Error objErr:srOppList.getErrors())
                        {
                            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();

                            /*
                            errLog.Name = String.valueOf(origrecord.ID);
                            errLog.JVCO_FailedRecordObjectReference__c = String.valueOf(origrecord.ID);
                            errLog.blng__FullErrorLog__c = string.valueof(objErr.getMessage());
                            errLog.blng__ErrorCode__c = string.valueof(objErr.getStatusCode());
                            errLog.JVCO_ErrorBatchName__c = 'JVCO_AffiliationMerge: Error on deleting';
                            */

                            errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(origrecord.ID);
                            errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                            errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                            errLog.ffps_custRem__Message__c = 'JVCO_AffiliationMerge: Error on deleting';

                            errLogList.add(errLog);   

                        }
                    }
                }

                if(!errLogList.isempty())
                {
                    try
                    {
                        insert errLogList;
                    }
                    catch(Exception ex)
                    {
                        System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                         
                    }
                }
            }
        }
        catch (Exception ex)
        {
            System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
            if(isMerge){
                throw new MergeException('Merging failed. ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
            }
            System.debug('Restored reparented Objects and undeleted affiliations');
            Database.rollback(sp);
        }
    }

    /* ----------------------------------------------------------------------------------------------------------
    Author:         jules.osberg.a.pablo
    Company:        Accenture
    Description:    GREEN-30463 Copy affiliations to subscriptions
    Inputs:         List of Affiliation, List of Affiliation
    Returns:        
    <Date>          <Authors Name>                      <Brief Description of Change> 
    24-02-2018      raus.k.b.ablaza                     Initial version of the code
    12-07-2018      jules.osberg.a.pablo                Refactored code segment and placed it into restructureAffiliation method. 
                                                        GREEN-32348 added affiliation field update for Quote Line Group, Quote Line, Order Item and Invoice Line
    ----------------------------------------------------------------------------------------------------------*/    

    public static void restructureAffiliation(List<JVCO_Affiliation__c> affiliationListForUpdate, List<JVCO_Affiliation__c> affiliationList){
        Set<Id> accIdSet = new Set<Id>();
        Set<Id> venIdSet = new Set<Id>();
        for(JVCO_Affiliation__c aff : affiliationList){
            accIdSet.add(aff.JVCO_Account__c);
            venIdSet.add(aff.JVCO_Venue__c);
        }

        //SUBSCRIPTIONS
        List<SBQQ__Subscription__c> subsList = new List<SBQQ__Subscription__c>();
        Map<String, List<SBQQ__Subscription__c>> subsMap = new Map<String, List<SBQQ__Subscription__c>>();
        Set<SBQQ__Subscription__c> subsDedupSet = new Set<SBQQ__Subscription__c>();
        
        subsList = [select id, Affiliation__c, SBQQ__Account__c, JVCO_Venue__c from SBQQ__Subscription__c where SBQQ__Account__c in :accIdSet and JVCO_Venue__c in :venIdSet];
        
        for(SBQQ__Subscription__c subs : subsList){
            String tmpKey = subs.SBQQ__Account__c + '::' + subs.JVCO_Venue__c;
            
            if(subsMap.containsKey(tmpKey)){
                List<SBQQ__Subscription__c> tmpList = new List<SBQQ__Subscription__c>();
                tmpList = subsMap.get(tmpKey);
                tmpList.add(subs);
                subsMap.put(tmpKey, tmpList);
            }
            else{
                subsMap.put(tmpKey, new List<SBQQ__Subscription__c>{subs});
            }
        }
        
        //QUOTE LINE GROUP
        List<SBQQ__QuoteLineGroup__c> qlgList = new List<SBQQ__QuoteLineGroup__c>();
        Map<String, List<SBQQ__QuoteLineGroup__c>> qlgMap = new Map<String, List<SBQQ__QuoteLineGroup__c>>();
        Set<SBQQ__QuoteLineGroup__c> qlgDedupSet = new Set<SBQQ__QuoteLineGroup__c>();

        qlgList = [select id, JVCO_Affiliated_Venue__c, SBQQ__Account__c, JVCO_Venue__c from SBQQ__QuoteLineGroup__c where SBQQ__Account__c in :accIdSet and JVCO_Venue__c in :venIdSet];
        
        for(SBQQ__QuoteLineGroup__c qlg : qlgList){
            String tmpKey = qlg.SBQQ__Account__c + '::' + qlg.JVCO_Venue__c;
            
            if(qlgMap.containsKey(tmpKey)){
                List<SBQQ__QuoteLineGroup__c> tmpList = new List<SBQQ__QuoteLineGroup__c>();
                tmpList = qlgMap.get(tmpKey);
                tmpList.add(qlg);
                qlgMap.put(tmpKey, tmpList);
            }
            else{
                qlgMap.put(tmpKey, new List<SBQQ__QuoteLineGroup__c>{qlg});
            }
        }
        
        //QUOTE LINE
        List<SBQQ__QuoteLine__c> qlList = new List<SBQQ__QuoteLine__c>();
        Map<String, List<SBQQ__QuoteLine__c>> qlMap = new Map<String, List<SBQQ__QuoteLine__c>>();
        Set<SBQQ__QuoteLine__c> qlDedupSet = new Set<SBQQ__QuoteLine__c>();

        qlList = [select id, Affiliation__c, JVCO_Venue__c, SBQQ__Quote__r.SBQQ__Account__c from SBQQ__QuoteLine__c where SBQQ__Quote__c in (select Id from SBQQ__Quote__c where SBQQ__Account__c in :accIdSet) and JVCO_Venue__c in :venIdSet];
        
        for(SBQQ__QuoteLine__c ql : qlList){
            String tmpKey = ql.SBQQ__Quote__r.SBQQ__Account__c + '::' + ql.JVCO_Venue__c;
            
            if(qlMap.containsKey(tmpKey)){
                List<SBQQ__QuoteLine__c> tmpList = new List<SBQQ__QuoteLine__c>();
                tmpList = qlMap.get(tmpKey);
                tmpList.add(ql);
                qlMap.put(tmpKey, tmpList);
            }
            else{
                qlMap.put(tmpKey, new List<SBQQ__QuoteLine__c>{ql});
            }
        }

        //ORDER ITEM
        List<OrderItem> orderItemList = new List<OrderItem>();
        Map<String, List<OrderItem>> orderItemMap = new Map<String, List<OrderItem>>();
        Set<OrderItem> orderItemDedupSet = new Set<OrderItem>();

        orderItemList = [select id, Affiliation__c, JVCO_Venue__c, Order.AccountId from OrderItem where OrderId in (select Id from Order where AccountId in :accIdSet) and JVCO_Venue__c in :venIdSet];
        
        for(OrderItem orderItem : orderItemList){
            String tmpKey = orderItem.Order.AccountId + '::' + orderItem.JVCO_Venue__c;
            
            if(orderItemMap.containsKey(tmpKey)){
                List<OrderItem> tmpList = new List<OrderItem>();
                tmpList = orderItemMap.get(tmpKey);
                tmpList.add(orderItem);
                orderItemMap.put(tmpKey, tmpList);
            }
            else{
                orderItemMap.put(tmpKey, new List<OrderItem>{orderItem});
            }
        }

        //INVOICE LINE
        List<blng__InvoiceLine__c> invLineList = new List<blng__InvoiceLine__c>();
        Map<String, List<blng__InvoiceLine__c>> invLineMap = new Map<String, List<blng__InvoiceLine__c>>();
        Set<blng__InvoiceLine__c> invLineDedupSet = new Set<blng__InvoiceLine__c>();

        invLineList = [select id, JVCO_Affiliation__c, JVCO_Venue__c, blng__Invoice__r.blng__Account__c from blng__InvoiceLine__c where blng__Invoice__c in (select Id from blng__Invoice__c where blng__Account__c in :accIdSet) and JVCO_Venue__c in :venIdSet];
        
        for(blng__InvoiceLine__c invLine : invLineList){
            String tmpKey = invLine.blng__Invoice__r.blng__Account__c + '::' + invLine.JVCO_Venue__c;
            
            if(invLineMap.containsKey(tmpKey)){
                List<blng__InvoiceLine__c> tmpList = new List<blng__InvoiceLine__c>();
                tmpList = invLineMap.get(tmpKey);
                tmpList.add(invLine);
                invLineMap.put(tmpKey, tmpList);
            }
            else{
                invLineMap.put(tmpKey, new List<blng__InvoiceLine__c>{invLine});
            }
        }

        
        for(JVCO_Affiliation__c aff : affiliationListForUpdate){
            String tmpKey = aff.JVCO_Account__c + '::' + aff.JVCO_Venue__c;

            //SUBSCRIPTION
            if(subsMap.containsKey(tmpKey)){
                for(SBQQ__Subscription__c subs : subsMap.get(tmpKey)){
                    subs.Affiliation__c = aff.Id;
                    subsDedupSet.add(subs);
                }
            }

            //QUOTE LINE GROUP
            if(qlgMap.containsKey(tmpKey)){
                for(SBQQ__QuoteLineGroup__c qlg : qlgMap.get(tmpKey)){
                    qlg.JVCO_Affiliated_Venue__c = aff.Id;
                    qlgDedupSet.add(qlg);
                }
            }

            //QUOTE LINE
            if(qlMap.containsKey(tmpKey)){
                for(SBQQ__QuoteLine__c ql : qlMap.get(tmpKey)){
                    ql.Affiliation__c = aff.Id;
                    qlDedupSet.add(ql);
                }
            }

            //ORDER ITEM
            if(orderItemMap.containsKey(tmpKey)){
                for(OrderItem orderItem : orderItemMap.get(tmpKey)){
                    orderItem.Affiliation__c = aff.Id;
                    orderItemDedupSet.add(orderItem);
                }
            }

            //INVOICE LINE
            if(invLineMap.containsKey(tmpKey)){
                for(blng__InvoiceLine__c invLine : invLineMap.get(tmpKey)){
                    invLine.JVCO_Affiliation__c = aff.Id;
                    invLineDedupSet.add(invLine);
                }
            }
        }

        
        //SUBSCRIPTION
        subsList = new List<SBQQ__Subscription__c>();   
        for(SBQQ__Subscription__c subs : subsDedupSet){
            subsList.add(subs);
        }

        //QUOTE LINE GROUP
        qlgList = new List<SBQQ__QuoteLineGroup__c>();
        for(SBQQ__QuoteLineGroup__c qlg : qlgDedupSet){
            qlgList.add(qlg);
        }

        //QUOTE LINE
        qlList = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : qlDedupSet){
            qlList.add(ql);
        }

        //ORDER ITEM
        orderItemList = new List<OrderItem>();
        for(OrderItem orderItem : orderItemDedupSet){
            orderItemList.add(orderItem);
        }

        //ORDER ITEM
        invLineList = new List<blng__InvoiceLine__c>();
        for(blng__InvoiceLine__c invLine : invLineDedupSet){
            invLineList.add(invLine);
        }

        
        if(!subsList.isEmpty()){
            update subsList;
        }

        if(!qlgList.isEmpty()){
            update qlgList;
        }

        if(!qlList.isEmpty()){
            update qlList;
        }

        if(!orderItemList.isEmpty()){
            update orderItemList;
        }

        if(!invLineList.isEmpty()){
            update invLineList;
        }
    }
}