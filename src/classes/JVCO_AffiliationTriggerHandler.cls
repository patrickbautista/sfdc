/* ----------------------------------------------------------------------------------------------

Name:        JVCO_AffiliationTriggerHandler.cls 
Description: Trigger handler for JVCO_AffiliationTrigger.trigger

Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     ----------------------------------------------------
04-Sep-2017  0.1         jules.osberg.a.pablo  Intial version
29-Sep-2017  0.2         recuerdo.b.bregente   GREEN-21488
18-Oct-2016  0.3         raus.k.b.ablaza       GREEN-24928
06-Jun-2018  0.4         jules.osberg.a.pablo  Added fix for GREEN-18745, a Valiadtion that prevents a user from creating an affiliation with a venue that has been assigned to an enforcement LA
------------------------------------------------------------------------------------------------- */
public with sharing class JVCO_AffiliationTriggerHandler {

  //Added flags for Validation on if active Affiliation record exists, GREEN-21488
  public static boolean runAffiliationExistValidation = true;
  private static boolean runValidationOnce = true;
  private static boolean checkRec = string.valueof(UserInfo.getUserName()).startsWith('autoproc');
  private static Profile profileRecord = !checkRec ? [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()]:null;

  public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
  public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_AffiliationTrigger__c : false;

  public static Boolean passesAffMerge = JVCO_AffiliationMerge.passesAffMerge;

  
  public JVCO_AffiliationTriggerHandler() {}

  public static void onBeforeInsert(List<JVCO_Affiliation__c> affiliationList){
    if(!skipTrigger) 
    {
      //System.assertEquals(true, runValidationOnce);
      //System.assertEquals(true, runAffiliationExistValidation);
      //System.assertNotEquals('System Administrator', profileRecord.Name);
      //if(runValidationOnce && runAffiliationExistValidation && profileRecord.Name <> 'System Administrator'){ //jules.osberg.a.pablo removed runValidationOnce because of testclass conflict
      if(profileRecord!=null){
        if(runAffiliationExistValidation && profileRecord.Name <> 'System Administrator' && !passesAffMerge){
          //runValidationOnce = false; //jules.osberg.a.pablo removed runValidationOnce because of testclass conflict
          validateActivieAffiliationAlreadyExists(affiliationList);
          
           
        }
      }

      
      
      if(!JVCO_BipassPermissionForEnforcement__c.getInstance(profileRecord.Id).JVCO_BipassEnforcementVRonAffiliation__c){
            if(checkUserIfRestricted()){
          validateNotEnforcementVenue(affiliationList, null); 
        }  
      }
      constructAffiliationName(affiliationList, null);
      updateVenuePrimariTariff(affiliationList);
      validatePromoterLA(affiliationList);
      
      
    }
  }

  public static void onBeforeUpdate(List<JVCO_Affiliation__c> affiliationList, Map<Id, JVCO_Affiliation__c> newAffiliationMap, Map<Id, JVCO_Affiliation__c> oldAffiliationMap){
    if(!skipTrigger) 
    {
        //if(runValidationOnce && runAffiliationExistValidation && profileRecord.Name <> 'System Administrator'){ //jules.osberg.a.pablo removed runValidationOnce because of testclass conflict
        if(profileRecord!=null){
          if(runAffiliationExistValidation && profileRecord.Name <> 'System Administrator' && !passesAffMerge){
            //runValidationOnce = false; //jules.osberg.a.pablo removed runValidationOnce because of testclass conflict 
            validateActivieAffiliationAlreadyExists(affiliationList);
          } 
        }   
        constructAffiliationName(affiliationList, oldAffiliationMap);
    }
  }


  public static void onAfterUpdate(List<JVCO_Affiliation__c> affiliationList, Map<Id, JVCO_Affiliation__c> newAffiliationMap, Map<Id, JVCO_Affiliation__c> oldAffiliationMap){
    if(!skipTrigger) 
    {
    //trigger code

    //start 03-01-2018 reymark.j.l.arlos GREEN-27809
    Map<Id, JVCO_Affiliation__c> closedAffiliation = new Map<Id, JVCO_Affiliation__c>();
    for(JVCO_Affiliation__c affRec : affiliationList)
    {
      if(!string.isblank(string.valueof(affRec.JVCO_End_Date__c)) && !string.isblank(affRec.JVCO_Closure_Reason__c))
      {
        if(oldAffiliationMap.containsKey(affRec.Id))
        {
          if(affRec.JVCO_Closure_Reason__c != oldAffiliationMap.get(affRec.Id).JVCO_Closure_Reason__c && string.isblank(string.valueof(oldAffiliationMap.get(affRec.Id).JVCO_End_Date__c)))
          {
            closedAffiliation.put(affRec.Id, affRec);
          }
        }
      }
    }

    if(!closedAffiliation.isEmpty())
    {
      ifClosedAffiliationZeroRenewalQuantity(closedAffiliation);
    }
    //end 03-01-2018 reymark.j.l.arlos

    //start 17-04-2020   mariel.m.buena   GREEN-35428
    Map<Id, JVCO_Affiliation__c> reopenedAffiliation = new Map<Id, JVCO_Affiliation__c>();
    for(JVCO_Affiliation__c reopenAffRec : affiliationList)
    {
      if(string.isblank(string.valueof(reopenAffRec.JVCO_End_Date__c)) && string.isblank(reopenAffRec.JVCO_Closure_Reason__c))
      {
        if(oldAffiliationMap.containsKey(reopenAffRec.Id))
        {
          if(reopenAffRec.JVCO_Closure_Reason__c != oldAffiliationMap.get(reopenAffRec.Id).JVCO_Closure_Reason__c && !string.isblank(string.valueof(oldAffiliationMap.get(reopenAffRec.Id).JVCO_End_Date__c)))
          {
            reopenedAffiliation.put(reopenAffRec.Id, reopenAffRec);
          }
        }
      }
    }

    if(!reopenedAffiliation.isEmpty())
    {
      ifAffiliationReopenSetRenewalQty(reopenedAffiliation);
    }
     //end  17-04-2020  mariel.m.buena
    }

  }

  
  /**
  * @author            Recuerdo B. Bregente
  * @description       Validates the existence of an active affiliation
  *                    between the parent Licence Account and Venue records.   
  * @param    List     List of JVCO_Affiliation__c
  * @version  0.01     Create for GREEN-21488
  */
  private static void validateActivieAffiliationAlreadyExists(List<JVCO_Affiliation__c> affiliationList){
    Map<String, Boolean> accountAndVenueIds = new Map<String, Boolean>();
    Set<Id> accountIds = new Set<Id>();
    Set<Id> venueIds = new Set<Id>();
    Set<Id> affiliationIds = new Set<Id>();

    //Check if inserted/updated records are active using start date and end date
    for(JVCO_Affiliation__c affRec : affiliationList){
      if(affRec.JVCO_Start_Date__c <> null && affRec.JVCO_End_Date__c == null){
        accountAndVenueIds.put(String.valueOf(affRec.JVCO_Account__c) + '-' +  String.valueOf(affRec.JVCO_Venue__c), false);
        accountIds.add(affRec.JVCO_Account__c);
        venueIds.add(affRec.JVCO_Venue__c);
        if(affRec.Id <> null){
          affiliationIds.add(affRec.Id);
        }
      }

    }
      
    //Store combination of Licence Account Ids and Venue Ids with active affiliation and set flag to true
    for(JVCO_Affiliation__c affRec : [SELECT Id, JVCO_Account__c, JVCO_Venue__c FROM JVCO_Affiliation__c WHERE 
                                Id NOT IN :affiliationIds AND JVCO_Account__c IN :accountIds AND JVCO_Venue__c IN :venueIds 
                                AND JVCO_Start_Date__c != null AND JVCO_End_Date__c = null]){
      String accountAndVenue = String.valueOf(affRec.JVCO_Account__c) + '-' + String.valueOf(affRec.JVCO_Venue__c);
      if(accountAndVenueIds.containsKey(accountAndVenue)){
        accountAndVenueIds.put(accountAndVenue, true);
      }
    }
      
    //Add error to affiliation record if parent venue and licence account is in the stored map with flag equals to true.                                                  
    for(JVCO_Affiliation__c affRec : affiliationList){
      String accountAndVenue = String.valueOf(affRec.JVCO_Account__c) + '-' + String.valueOf(affRec.JVCO_Venue__c);
        
      if(accountAndVenueIds.containsKey(accountAndVenue) && accountAndVenueIds.get(accountAndVenue)){
        affRec.addError('Active affiliation record already exists for Account Id: ' + affRec.JVCO_Account__c + ' and Venue Id: ' + affRec.JVCO_Venue__c + '.');
      }
    }
  }
    
  /*
    author: raus.k.b.ablaza
    description: refactorization for PB JVCO_Affiliation_Construct_Name_on_Create_and_Update GREEN-24928
    params: list of affiliation, map of old affiliation
    version:
    1.0       raus.k.b.ablaza       18/10/2017
  */
  public static void constructAffiliationName(List<JVCO_Affiliation__c> affList, Map<Id, JVCO_Affiliation__c> oldAffMap){
    Map<Id, Account> accMap = new Map<Id, Account>();
    Map<Id, JVCO_venue__c> venueMap = new Map<Id, JVCO_Venue__c>();
    Set<Id> accIdSet = new Set<Id>();
    Set<Id> venIdSet = new Set<Id>();

    for(JVCO_Affiliation__c aff : affList){
      accIdSet.add(aff.JVCO_Account__c);
      venIdSet.add(aff.JVCO_Venue__c);
    }

    if(!accIdSet.isEmpty()){
      accMap = new Map<Id, Account>([select Id, Name from Account where Id in :accIdSet]);    
    }

    if(!venIdSet.isEmpty()){
      venueMap = new Map<Id, JVCO_Venue__c>([select Id, Name from JVCO_Venue__c where Id in :venIdSet
      ]);    
    }

    for(JVCO_Affiliation__c aff : affList){
      if(oldAffmap != null){
        if( (aff.JVCO_Venue_Name__c != oldAffMap.get(aff.Id).JVCO_Venue_Name__c) || 
          (aff.JVCO_Account__c != oldAffMap.get(aff.Id).JVCO_Account__c) ) {
          aff.Name = checkNameLength(accMap.get(aff.JVCO_Account__c).Name + ':' + venueMap.get(aff.JVCO_Venue__c).Name);
        }
      }
      else{
        aff.JVCO_Venue_Name__c = venueMap.get(aff.JVCO_Venue__c).Name;
        aff.Name = checkNameLength(accMap.get(aff.JVCO_Account__c).Name + ':' + venueMap.get(aff.JVCO_Venue__c).Name);

      }
    }

  }

  /*
    author: raus.k.b.ablaza
    description: Checks string input and cut it if necessary GREEN-24928
    params: String
    version:
    1.0       raus.k.b.ablaza       18/10/2017
  */
  public static String checkNameLength(String str){
    String reStr = '';

    if(str.length() > 80){
      reStr = str.substring(0,76) + '...';
    }
    else{
      reStr = str;
    }
    return reStr;
  }

  /* ----------------------------------------------------------------------------------------------------------
  Author:         reymark.j.l.arlos
  Description:    After update of closed affiliation set zero renewal quantity for subscriptions
  Params:         List of Accounts with new values
  Returns:        void
  <Date>          <Authors Name>                      <Brief Description of Change> 
  03-01-2018      reymark.j.l.arlos                   Initial version of the code - GREEN-27809
  ----------------------------------------------------------------------------------------------------------*/
  public static void ifClosedAffiliationZeroRenewalQuantity(Map<Id, JVCO_Affiliation__c> newAffiliation)
  {
    Set<Id> affId = newAffiliation.keySet();
    
    List<SBQQ__Subscription__c> lstSubscriptionUpdate = [Select Id, SBQQ__RenewalQuantity__c
                                From SBQQ__Subscription__c
                                Where Affiliation__c in: affId];

    for(SBQQ__Subscription__c subRec : lstSubscriptionUpdate)
    {
      subRec.SBQQ__RenewalQuantity__c = 0;
    }

    if(!lstSubscriptionUpdate.isEmpty())
    {
      update lstSubscriptionUpdate;
    }
    
  }


  /* ----------------------------------------------------------------------------------------------------------
  Author:         reymark.j.l.arlos
  Description:    make the population of primary tariff in venue date aware for HDB and H&B
  Params:         List of Accounts with new values
  Returns:        void
  <Date>          <Authors Name>                      <Brief Description of Change> 
  25-05-2018     robert.j.b.lacatan                  Initial version of the code - GREEN-31516  
  ----------------------------------------------------------------------------------------------------------*/

  public static void updateVenuePrimariTariff(List<JVCO_Affiliation__c> lAff)
  {
    Map<Id,List<JVCO_Affiliation__c>> venueAffiliationMap = new Map<Id,List<JVCO_Affiliation__c>>();
    List<JVCO_venue__c> venueRec = new List<JVCO_venue__c>();
    Date hAndB = Date.newInstance(2018, 2, 26);
    JVCO_Venue__c vRec = new JVCO_venue__c();

    for(JVCO_Affiliation__c aff: lAff){

      if(venueAffiliationMap.containsKey(aff.JVCO_Venue__c)){
        List<JVCO_Affiliation__c> listOfAff = venueAffiliationMap.get(aff.JVCO_Venue__c);
        listOfAff.add(aff);
        venueAffiliationMap.put(aff.JVCO_Venue__c, listOfAff);
      }
      else{
        venueAffiliationMap.put(aff.JVCO_Venue__c, new List<JVCO_Affiliation__c>{aff});
      }
       
      }

      List<JVCO_Affiliation__c> checkExistingAff= [SELECT Id, JVCO_Start_Date__c, JVCO_End_Date__c, JVCO_Venue__c from JVCO_Affiliation__c where JVCO_Venue__c IN: venueAffiliationMap.keySet()];

    for(JVCO_Affiliation__c aff: checkExistingAff){

      if(venueAffiliationMap.containsKey(aff.JVCO_Venue__c)){
        List<JVCO_Affiliation__c> listOfAff = venueAffiliationMap.get(aff.JVCO_Venue__c);
        listOfAff.add(aff);
        venueAffiliationMap.put(aff.JVCO_Venue__c, listOfAff);
      }
    }

      for(JVCO_Affiliation__c aff: lAff){

    if(venueAffiliationMap.containsKey(aff.JVCO_Venue__c)){
      List<JVCO_Affiliation__c> listOfAff = venueAffiliationMap.get(aff.JVCO_Venue__c);
      system.debug('listOfAff'+listOfAff.size());
        if(listOfAff.size()==1){
        
            if(aff.JVCO_Venue_Type__c == 'Hairdresser/Beauty Salon' && aff.JVCO_Start_Date__c >= hAndB ){
            vRec.Id = aff.JVCO_Venue__c;
            vRec.JVCO_Primary_Tariff__c = 'H&B_PRS';
            venueRec.add(vRec);
            system.debug('VRECORDS'+vRec);
            }
        }
      }       
      }
    
      if(!venueRec.isEmpty()){
        update venueRec;
      }   
  }

  /* ----------------------------------------------------------------------------------------------------------
  Author:         jules.osberg.a.pablo
  Description:    Valiadtion that prevents a user from creating an affiliation with a venue that has been assigned to an enforcement LA
  <Date>          <Authors Name>                      <Brief Description of Change> 
  06-07-2018     jules.osberg.a.pablo                  Initial version of the code - GREEN-18745  
  ----------------------------------------------------------------------------------------------------------*/
  public static void validateNotEnforcementVenue(List<JVCO_Affiliation__c> affiliationList, Map<Id, JVCO_Affiliation__c> oldAffiliationMap){
    set<Id> venueIdToCheck = new set<Id>();
    Map<Id, JVCO_Affiliation__c> affEnforcementLAMap = new Map<Id, JVCO_Affiliation__c>();
    List<JVCO_Affiliation__c> affEnforcementLAList = new List<JVCO_Affiliation__c>();
    if(oldAffiliationMap != null){
      for(JVCO_Affiliation__c affRec :affiliationList){
        if(affRec.JVCO_Account__c != oldAffiliationMap.get(affRec.Id).JVCO_Account__c || affRec.JVCO_Venue__c != oldAffiliationMap.get(affRec.Id).JVCO_Venue__c){
          venueIdToCheck.add(affRec.JVCO_Venue__c);
        }
      }
    }else{  
      for(JVCO_Affiliation__c affRec :affiliationList){
        venueIdToCheck.add(affRec.JVCO_Venue__c);
      }
    }

    if(!venueIdToCheck.isEmpty()){
      affEnforcementLAList = [SELECT Id, JVCO_Venue__c FROM JVCO_Affiliation__c WHERE JVCO_Venue__c IN :venueIdToCheck AND JVCO_Account__r.JVCO_In_Enforcement__c = TRUE];
    }

    if(!affEnforcementLAList.isEmpty()){
      for(JVCO_Affiliation__c affRec :affiliationList){
        if(venueIdToCheck.contains(affRec.JVCO_Venue__c)){
          affRec.addError('You cannot create an affiliation if the Venue is related to a Licence Account in Enforcement');
        }
      }
    }
  }

  private static Boolean checkUserIfRestricted(){
        Map<Id,PermissionSet> enAdPermSet = new Map<Id,PermissionSet>([SELECT Id FROM PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors']);
        List<PermissionSetAssignment> permissionSetList = [SELECT Id FROM PermissionSetAssignment WHERE 
                                                            AssigneeId = :UserInfo.getUserId() AND PermissionSetId IN :enAdPermSet.keySet()];
                                                            
        return (permissionSetList.isEmpty());
    }
  /* ----------------------------------------------------------------------------------------------------------
  Author:         mariel.m.buenaa
  Description:    Validation that prevents a user from creating an affiliation if Licence Account = Promoter
  <Date>          <Authors Name>                      <Brief Description of Change> 
  12-09-2018      mariel.m.buena                  Initial version of the code - GREEN-32855  
  ----------------------------------------------------------------------------------------------------------*/

  private static void validatePromoterLA(List<JVCO_Affiliation__c> newAffList)
  {
    Map<Id, Account> accMap = new Map<Id, Account>();
    Set<Id> accIdSet = new Set<Id>();

    for(JVCO_Affiliation__c aff : newAffList)
    {
      accIdSet.add(aff.JVCO_Account__c);
    }

    if(!accIdSet.isEmpty())
    {
      accMap = new Map<Id, Account>([select Id, Type from Account where Id in :accIdSet]);    
    }

    for(JVCO_Affiliation__c aff : newAffList)
    {
      if(accMap.containsKey(aff.JVCO_Account__c))
      {
        if(accMap.get(aff.JVCO_Account__c).Type == 'Promoter')
        {
          aff.addError('You cannot create an Affiliation if the Licence Account is Promoter.');
        }
      }
    }
  }
  /* ----------------------------------------------------------------------------------------------------------
  Author:         mariel.m.buena
  Description:    After reopening of affiliation set renewal quantity to the quantity of subscriptions
  Params:         List of Accounts with new values
  Returns:        void
  <Date>          <Authors Name>                      <Brief Description of Change> 
  17-04-2018      mariel.m.buena                   Initial version of the code - GREEN-35428
  ----------------------------------------------------------------------------------------------------------*/
  public static void ifAffiliationReopenSetRenewalQty(Map<Id, JVCO_Affiliation__c> newAffiliation)
  {
    Set<Id> affId = newAffiliation.keySet();
    
    List<SBQQ__Subscription__c> updatedSubs = [Select Id, SBQQ__RenewalQuantity__c, SBQQ__Quantity__c
                                From SBQQ__Subscription__c
                                Where Affiliation__c in: affId];

    for(SBQQ__Subscription__c subRec : updatedSubs)
    {
      subRec.SBQQ__RenewalQuantity__c = subRec.SBQQ__Quantity__c;
    }

    if(!updatedSubs.isEmpty())
    {
      update updatedSubs;
    }
    
  }
}