/* ----------------------------------------------------------------------------------------------------------
    Name:            JVCO_LicAccCompleteAmendmentBatch.cls 
    Description:     Batch class for JVCO_LicAccCompleteAmendmentBatch
    Test class:      

    Date             Version     Author                            Summary of Changes 
    -----------      -------     -----------------                 -------------------------------------------
    25-June-2020       0.1        Accenture-rhys.j.c.dela.cruz         Initial version of the code 
 ---------------------------------------------------------------------------------------------------------- */
global class JVCO_LicAccCompleteAmendmentBatch implements Database.Batchable<sObject>, Database.Stateful
{
    @testVisible
    private static Boolean testError = false;
    
    global Set<Id> quoteIds;
    global Id batchId;

    global JVCO_LicAccCompleteAmendmentBatch()
    {
        quoteIds = new Set<Id>();
    }

    global JVCO_LicAccCompleteAmendmentBatch(Set<Id> quoteIdSet)
    {

        quoteIds = quoteIdSet;
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        List<JVCO_TempInvoiceProcessingData__c> tempDataList = [SELECT Id, JVCO_Batch_Name__c, JVCO_Id1__c, JVCO_Id2__c FROM JVCO_TempInvoiceProcessingData__c WHERE JVCO_Batch_Name__c = 'JVCO_LicAccGenerateAmendmentBatch'];
        Set<Id> tempDataSet = new Set<Id>();

        for(JVCO_TempInvoiceProcessingData__c tempRec : tempDataList){

            tempDataSet.add(tempRec.JVCO_Id1__c);
        }

        String query = 'SELECT Id, SBQQ__Type__c, SBQQ__Status__c, SBQQ__Opportunity2__c, SBQQ__Opportunity2__r.SBQQ__Contracted__c, SBQQ__NetAmount__c, SBQQ__MasterContract__c, SBQQ__MasterContract__r.JVCO_TempStartDate__c, SBQQ__MasterContract__r.JVCO_TempEndDate__c, SBQQ__MasterContract__r.JVCO_ContractScenario__c, Covid_19_Impacted1__c FROM SBQQ__Quote__c WHERE SBQQ__MasterContract__r.JVCO_ProcessedByCreditsBatch__c = true AND (SBQQ__MasterContract__r.JVCO_ContractScenario__c = \'Ends After Lockdown\' OR SBQQ__MasterContract__r.JVCO_ContractScenario__c = \'Ends During Lockdown\') AND (SBQQ__NetAmount__c != 0 AND SBQQ__NetAmount__c != null) AND SBQQ__Type__c = \'Amendment\' AND SBQQ__Status__c = \'Draft\' AND JVCO_Recalculated__c = TRUE AND SBQQ__LineItemCount__c > 0 AND SBQQ__Primary__c = TRUE AND NoOfQuoteLinesWithoutSPV__c = 0 AND SBQQ__Opportunity2__r.SBQQ__Contracted__c = FALSE ';

        if(!quoteIds.isEmpty()){

            query += ' AND Id IN: quoteIds ';
        }
        else{
            query += ' AND SBQQ__MasterContract__c IN: tempDataSet';
        }

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        Set<Id> oppIdSet = new Set<Id>();
        List<sObject> lstObjectRec = new List<sObject>();
        Savepoint sp = Database.setSavepoint();
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        Set<Id> oppSuccessSet = new Set<Id>();
        Set<Id> oppErrSet = new Set<Id>();

        for(SBQQ__Quote__c quote : (List<SBQQ__Quote__c>) scope)
        {
            if(quote.SBQQ__Opportunity2__c != Null)
            {
                if(!quote.SBQQ__Opportunity2__r.SBQQ__Contracted__c)
                {
                    oppIdSet.add(quote.SBQQ__Opportunity2__c);
                    Opportunity OppRec = quote.SBQQ__Opportunity2__r;
                    OppRec.StageName = 'Closed Won';

                    //start 28-12-2017 reymark.j.l.arlos GREEN-27464
                    OppRec.Amount = quote.SBQQ__NetAmount__c;
                    //end

                    lstObjectRec.add((sObject) OppRec);

                    quote.SBQQ__Status__c = 'Accepted';     //GREEN-23355
                    quote.JVCO_Contact_Type__c = 'No Customer Contact';         //GREEN-19227
                    quote.JVCO_QuoteComplete__c = true;// 20-12-2017 Accenture - mel.andrei.b.Santos  set JVCO_QuoteComplete__c to True if Quote is contracted successfully as per GREEN-27602
                    quote.Covid_19_Impacted1__c = 'Auto Credit';
                    quote.JVCO_CreditReason__c = 'Usage amendment, to be consolidated in PPLPRS invoice (True-up)';
                    lstObjectRec.add((sObject) quote );
                }

                try{
                            
                    if(quote.SBQQ__Type__c == 'Amendment'){
                        
                        List<SBQQ__QuoteLine__c> qlRecList = new List<SBQQ__QuoteLine__c>();
                        for(SBQQ__QuoteLine__c ql : [SELECT id, SBQQ__Quantity__c, SBQQ__PriorQuantity__c from SBQQ__QuoteLine__c where SBQQ__Quote__c =: quote.Id AND SBQQ__EffectiveQuantity__c = 0]){
                            if(ql.SBQQ__PriorQuantity__c != ql.SBQQ__Quantity__c){
                                    ql.SBQQ__PriorQuantity__c = ql.SBQQ__Quantity__c;
                                    qlRecList.add(ql);
                            } 
                        }

                        if(Test.isRunningTest() && testError){
                            qlRecList.add(new SBQQ__QuoteLine__c());
                        }

                        SBQQ.TriggerControl.disable();
                        if(!qlRecList.isEmpty()){
                            update qlRecList;
                        }
                        SBQQ.TriggerControl.enable();
                    }
                }
                catch(Exception e){

                    ffps_custRem__Custom_Log__c qErrLog = new ffps_custRem__Custom_Log__c();

                    /*
                    qErrLog.Name = String.valueOf(quote.ID);
                    qErrLog.JVCO_FailedRecordObjectReference__c = String.valueOf(quote.ID);
                    qErrLog.blng__FullErrorLog__c = 'Error: ' + e.getMessage();
                    qErrLog.blng__ErrorCode__c = string.valueof(quote.SBQQ__Opportunity2__c);
                    qErrLog.JVCO_ErrorBatchName__c = 'JVCO_LicAccCompleteAmendmentBatch: Update of Prior Quantity on QL';
                    */

                    qErrLog.ffps_custRem__Grouping__c = string.valueof(quote.SBQQ__Opportunity2__c);
                    qErrLog.ffps_custRem__Detail__c = 'Error: ' + e.getMessage();
                    qErrLog.ffps_custRem__Message__c = 'JVCO_LicAccCompleteAmendmentBatch: Update of Prior Quantity on QL';     
                    qErrLog.ffps_custRem__Related_Object_Key__c = string.valueof(quote.ID);
                    qErrLog.ffps_custRem__Running_User__c = UserInfo.getUserId();

                    errLogList.add(qErrLog); 
                }
            }
        }

        if(lstObjectRec.size() > 0){

            if(!lstObjectRec.isEmpty()){

                if(Test.isRunningTest()){                  //Added isRunningTest to create DML error - Rhys Dela Cruz
                     lstObjectRec.add(new Opportunity());
                }

                List<Database.SaveResult> res = Database.update(lstObjectRec,false);

                for(Integer i = 0; i < lstObjectRec.size(); i++){

                    Database.SaveResult srOppList = res[i];
                    sObject origrecord = lstObjectRec[i];

                    if(!srOppList.isSuccess()){

                        Database.rollback(sp);
                        System.debug('Update sObject fail: ');
                        for(Database.Error objErr:srOppList.getErrors())
                        {
                            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();

                            errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                            errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                            errLog.ffps_custRem__Related_Object_Key__c = string.valueof(origrecord.ID);
                            errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                            //errLog.Name = String.valueOf(origrecord.ID);

                            if(origrecord.getSObjectType() == Opportunity.sObjectType){
                                errLog.ffps_custRem__Message__c = 'JVCO_LicAccCompleteAmendmentBatch: Update Opportunity Record';
                                oppErrSet.add(origrecord.Id);
                            }
                            else if(origrecord.getSObjectType() == SBQQ__Quote__c.sObjectType){
                                errLog.ffps_custRem__Message__c = 'JVCO_LicAccCompleteAmendmentBatch: Update Quote Record';
                                oppErrSet.add(((SBQQ__Quote__c)origrecord).SBQQ__Opportunity2__c);
                            }
                            else if(origrecord.getSObjectType() == SBQQ__QuoteLineGroup__c.sObjectType)
                                errLog.ffps_custRem__Message__c = 'JVCO_LicAccCompleteAmendmentBatch: Update SBQQ__QuoteLineGroup__c Record';
                            else if(origrecord.getSObjectType() == JVCO_Affiliation__c.sObjectType)
                                errLog.ffps_custRem__Message__c = 'JVCO_LicAccCompleteAmendmentBatch: Update JVCO_Affiliation__c Record';
                                
                            errLogList.add(errLog);
                        }
                    }
                    else{
                        
                        if(origrecord.getSObjectType() == Opportunity.sObjectType){
                            oppSuccessSet.add(origrecord.Id);
                        }
                        else if(origrecord.getSObjectType() == SBQQ__Quote__c.sObjectType){
                            oppSuccessSet.add(((SBQQ__Quote__c)origrecord).SBQQ__Opportunity2__c);
                        }
                    }
                }

                if(!oppErrSet.isEmpty()){
                    oppSuccessSet.removeAll(oppErrSet);
                }
            }
        }

        if(!oppIdSet.isEmpty()){

            for(Id oppId: oppIdSet){

                if(oppSuccessSet.contains(oppId)){

                    try{

                        SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractGenerator',oppId , null); 
                    }
                    catch(exception e){
                        
                        String ErrorLineNumber = e.getLineNumber() != Null ? String.ValueOf(e.getLineNumber()):'None';
                        String ErrorMessage = 'Cause: ' + e.getCause() + ' ErrMsg: ' + e.getMessage();
                        errLogList.add(JVCO_ErrorUtil.logError(oppId, ErrorMessage , ErrorLineNumber , 'JVCO_LicAccCompleteAmendmentBatch:SBQQ.ContractManipulationAPI.ContractGenerator'));
    
                        Database.rollback(sp);
                    }
                }
            }
        }

        if(!errLogList.isEmpty()){
            insert errLogList;
        }
    }

    global void finish (Database.BatchableContext BC)
    {
        JVCO_LicAccOrderAmendmentBatch licAccOrderAmendments = new JVCO_LicAccOrderAmendmentBatch();
        Database.executeBatch(licAccOrderAmendments, 1);
    }
}