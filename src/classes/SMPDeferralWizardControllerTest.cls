@isTest
private class SMPDeferralWizardControllerTest 
{
    static Income_Direct_Debit__c createTestFramework() 
    {
        // SMP_Months_To_Defer__c CS = SMP_Months_To_Defer__c.getInstance();
        // CS.Months__c = 1;
        insert new SMP_Months_To_Defer__c(Months__c = '1');

        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        //Settings
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_codaCreditNoteTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        c2g__codaDimension2__c dim2 = JVCO_TestClassHelper.setDim2('PPL');
        insert dim2;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;

        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;

        custAcc.JVCO_DD_Payee_Contact__c = c.Id;
        //update custAcc;

        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        system.debug('#### account with contact: ' + licAcc);
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;

        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        insert sInv;
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference sInvRef = new c2g.CODAAPICommon.Reference();
        sInvRef.Id = sInv.Id;

        c2g__codaInvoiceInstallmentLineItem__c invoiceInstalmentLine = new c2g__codaInvoiceInstallmentLineItem__c();
        invoiceInstalmentLine.c2g__Amount__c = 12;
        invoiceInstalmentLine.c2g__DueDate__c = Date.today().addDays(10);
        invoiceInstalmentLine.JVCO_Paid_Amount__c = 12;
        invoiceInstalmentLine.JVCO_Payment_Status__c = 'Paid';
        invoiceInstalmentLine.c2g__Invoice__c = sInv.id;
        insert invoiceInstalmentLine;
        
    c2g__codaInvoiceInstallmentLineItem__c invoiceInstalmentLine2 = new c2g__codaInvoiceInstallmentLineItem__c();
        invoiceInstalmentLine2.c2g__Amount__c = 12;
        invoiceInstalmentLine2.c2g__DueDate__c = Date.today().addDays(10);
        invoiceInstalmentLine2.JVCO_Paid_Amount__c = 12;
        invoiceInstalmentLine2.JVCO_Payment_Status__c = 'Unpaid';
        invoiceInstalmentLine2.c2g__Invoice__c = sInv.id;
        insert invoiceInstalmentLine2;
        
        Income_Direct_Debit__c idd = new Income_Direct_Debit__c();
        idd.Account__c = licAcc.id;
        idd.DD_Bank_Sort_Code__c = '938600';
        idd.DD_Bank_Account_Number__c = '42368003';
        idd.DD_Collection_Day__c = '3';
        idd.DD_Next_Collection_Date__c = Date.today().addDays(10);
        idd.DD_Collection_Period__c = 'Monthly';
        idd.DD_Status__c = 'New Instruction';
        idd.DD_Bank_Account_Name__c = 'Test';
        idd.DD_Account_Email__c = c.Email;
        idd.Contact__c = c.id;
        insert idd;

        JVCO_Invoice_Group__c invoiceGroup = new JVCO_Invoice_Group__c();
        invoiceGroup.JVCO_Total_Amount__c = 100;
        insert invoiceGroup;

        sInv.JVCO_Invoice_Group__c = invoiceGroup.Id;
        update sInv;

        SMP_DirectDebit_GroupInvoice__c IDDInvGroup = new SMP_DirectDebit_GroupInvoice__c();
        IDDInvGroup.Income_Direct_Debit__c = idd.Id;
        IDDInvGroup.Invoice_Group__c = invoiceGroup.Id;
        insert IDDInvGroup;
        system.debug('######CLASS' + [SELECT Income_Direct_Debit__c, Id, Invoice_Group__c, JVCO_Number_of_Paid_Installments__c, Name FROM SMP_DirectDebit_GroupInvoice__c WHERE Income_Direct_Debit__c = : idd.Id]);
        // c2g__codaInvoiceInstallmentLineItem__c sobj = new c2g__codaInvoiceInstallmentLineItem__c(
        // // c2g__OwnerCompany__c = 'a2k1X000000PjfV',    // Company
        // c2g__DueDate__c = Date.Parse('1/1/2099'),  // Due Date
        // c2g__Invoice__c = sInv.Id,           // Invoice Number
        // c2g__Amount__c = 342.02,                     // Amount
        // c2g__LineNumber__c = 1,                      // Line Number
        // JVCO_Paid_Amount__c = 0.00,                    // Paid Amount
        // JVCO_DD_Failed__c = false,                     // DD Failed
        // JVCO_Payment_Status__c = 'Unpaid'             // Payment Status
        // );
        // insert sobj;

        // Income_Direct_Debit__c dd = new Income_Direct_Debit__c(
        //     );
        // dd.Account__c = licAcc.Id;
        // dd.DD_First_Collection_Amount__c = 1;
        // dd.DD_First_Collection_Date__c = Date.Parse('1/1/2000');
        // dd.DD_Bank_Account_Number__c = '88888888';
        // dd.DD_Bank_Sort_Code__c = '666666';
        // dd.DD_Next_Collection_Date__c = Date.Parse('1/1/2000');

        // insert dd;
        
        return idd;
    }
    
   static testMethod void testNext() 
   {
      ApexPages.StandardController sc = new ApexPages.StandardController(createTestFramework());
      system.debug('#### sc'+ sc);
      SMPDeferralWizardController controller = new SMPDeferralWizardController(sc);
      // controller.directDebitWrapperList[0].selected = true;

      controller.next();

      System.assertEquals(3, controller.stepNo);

   }
   static testMethod void testback() 
   {
      ApexPages.StandardController sc = new ApexPages.StandardController(createTestFramework());
      system.debug('#### sc'+ sc);

      SMPDeferralWizardController controller = new SMPDeferralWizardController(sc);
      // controller.directDebitWrapperList[0].selected = true;
      controller.monthsForDefer = 1;

      controller.back();

      System.assertEquals(1, controller.stepNo);

   }
   static testMethod void testcancel() 
   {
      ApexPages.StandardController sc = new ApexPages.StandardController(createTestFramework());
      system.debug('#### sc'+ sc);
      SMPDeferralWizardController controller = new SMPDeferralWizardController(sc);
      // controller.directDebitWrapperList[0].selected = true;
      controller.monthsForDefer = 1;
      PageReference test = controller.cancel();

      System.assertNotEquals(null, test);
      
   }
   static testMethod void testupdateDates() 
   {
      ApexPages.StandardController sc = new ApexPages.StandardController(createTestFramework());
      system.debug('#### sc'+ sc);
      SMPDeferralWizardController controller = new SMPDeferralWizardController(sc);
      // controller.directDebitWrapperList[0].selected = true;
      controller.monthsForDefer = 1;

      controller.collectAllRecords();

      controller.updateDates();
      
      System.assertEquals(Date.today().addDays(10), controller.directDebit.DD_Next_Collection_Date__c);

   }
   static testMethod void testcollectAllRecords() 
   {
      ApexPages.StandardController sc = new ApexPages.StandardController(createTestFramework());
      system.debug('#### sc'+ sc);
      SMPDeferralWizardController controller = new SMPDeferralWizardController(sc);
      // controller.directDebitWrapperList[0].selected = true;
      controller.monthsForDefer = 1;

      controller.collectAllRecords();
   }
   static testMethod void testprocess() 
   {
      ApexPages.StandardController sc = new ApexPages.StandardController(createTestFramework());
      system.debug('#### sc'+ sc);
      SMPDeferralWizardController controller = new SMPDeferralWizardController(sc);
      // controller.directDebitWrapperList[0].selected = true;
      controller.monthsForDefer = 1;

      controller.collectAllRecords();
      controller.updateDates();

      PageReference test = controller.process();
   }


}