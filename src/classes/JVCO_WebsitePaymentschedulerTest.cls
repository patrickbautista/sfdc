/* ----------------------------------------------------------------------------------------------------------
Name:            JVCO_WebsitePaymentschudler.cls
Description:     websit payment class
Test class:      JVCO_WebsitePaymentschudlerTest.cls

Date             Version     Author                            Summary of Changes
-----------      -------     -----------------                 -------------------------------------------
03-Nov-2017      0.1         Accenture-saket.mohan.jha         Initial version of the code
02-Feb-2018      0.2         jasper.j.figueroa                 Refactored the code
---------------------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_WebsitePaymentschedulerTest{

/*@testSetup
public static void setupData()
{
JVCO_WebsitePaymentsTestUtility.createDataForWebPay();  
  
}

static testmethod void testsch(){

 JVCO_General_Settings__c genSet = new JVCO_General_Settings__c();
 genSet.JVCO_PayonomyPaymentProcessingBatchLimit__c = 20;
 insert genSet;
 List<c2g__codaInvoice__c> invList = new  List<c2g__codaInvoice__c>();
        c2g__codaCompany__c testCompany = [SELECT
            id, 
            name
        FROM
            c2g__codaCompany__c
        limit 1];
        Account act = [SELECT
            id, 
            accountnumber
        FROM
            account
        WHERE
            recordtype.name = 'Licence Account'
        limit 1];
        c2g__codaInvoice__c inv = [SELECT
            id, 
            name, 
            c2g__Account__c, 
            c2g__InvoiceDate__c, 
            c2g__OwnerCompany__c, 
            JVCO_Customer_Type__c
        FROM
            c2g__codaInvoice__c
        limit 1];
 
JVCO_Invoice_Group__c grp = new JVCO_Invoice_Group__c();
grp.JVCO_invoiceNumbers__c = testCompany.name;
grp.JVCO_Total_Amount__c = 1;
grp.JVCO_invNumbers__c = inv.name;
insert grp;

JVCO_Invoice_Group__c invGrp = [SELECT Id, Name, JVCO_invoiceNumbers__c, JVCO_Total_Amount__c, JVCO_invNumbers__c
                                FROM JVCO_Invoice_Group__c LIMIT 1];

Id rec = Schema.SObjectType.PAYBASE2__Payment__c .getRecordTypeInfosByName().get('SagePay').getRecordTypeId();

PAYBASE2__Payment__c payment = new PAYBASE2__Payment__c();
payment.PAYBASE2__Status__c = 'AUTHORISED';
payment.PAYCP2__Payment_Description__c = invGrp .Name;
payment.PAYBASE2__Pay_Date__c = System.Today();
payment.RecordTypeId = rec;
insert payment;

JVCO_WebsitePaymentscheduler jc = new JVCO_WebsitePaymentscheduler(); 
jc.execute(null);
}
*/
}