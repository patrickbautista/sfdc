global class JVCO_TransformationOfAccountTypeBatch implements Database.Batchable<sObject>, Database.Stateful {


    public Set<Id> multiSubId = new Set<Id>();
    public Map<String, Set<String>> res = new Map<String, Set<String>>();

    global JVCO_TransformationOfAccountTypeBatch() {
       
    }
    global JVCO_TransformationOfAccountTypeBatch(Set<Id> lSubId, integer numberOfCustomerAccountToProcess) {
        multiSubId = lSubId;

        JVCO_TransOfAccountTypeHelper.sendBeginEmail(numberOfCustomerAccountToProcess);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
      
        String query = 'SELECT id, Name, JVCO_Customer_Account__r.JVCO_Target_Account_Type__c, Transformation__c, JVCO_Customer_Account__c, JVCO_Customer_Account__r.JVCO_Account_Type_Change__c, JVCO_In_Enforcement__c, JVCO_In_Infringement__c, Type, JVCO_Customer_Account__r.Type, JVCO_Review_Type__c FROM Account WHERE JVCO_Customer_Account__c != null AND (JVCO_Customer_Account__r.JVCO_Target_Account_Type__c = \'Customer to Key\' OR JVCO_Customer_Account__r.JVCO_Target_Account_Type__c = \'Key to Customer\') AND JVCO_Live__c != true AND JVCO_Closure_Reason__c = null AND JVCO_Closure_Reason__c = null';

        if(multiSubId != null){
            if(!multiSubId.isEmpty()){
                query += ' AND ';

                integer num = 0;
                for(Id i: multiSubId){
                    
                    if(num == 0){
                        query+= '(JVCO_Customer_Account__c = \'' + i + '\'';
                        num++;
                    }else{
                        query += ' OR JVCO_Customer_Account__c = \'' + i + '\'';
                    }
                }

                query += ')';                
            }    
        }

      return Database.getQueryLocator(query);
       
    }

    global void execute(Database.BatchableContext BC, List<Account> accountLists)
    {   
        Map<String, Set<String>> tmp = new Map<String, Set<String>>();
        system.debug('AccountLIst_IceFire: ' + accountLists.size());
        if(accountLists.size() > 0){
            system.debug('Account List' + accountLists);
            tmp = JVCO_TransOfAccountTypeHelper.doTransformation(accountLists, false);

            system.debug('tmp LIst Ice:  ' + tmp);

            For(String rec: tmp.keySet()){

                if(rec == 'Success'){

                    if(res.containsKey('Success')){
                        Set<String> tmpSet = new Set<String>(res.get('Success'));
                        tmpSet.addAll(tmp.get(rec));
                        res.put('Success', tmpSet);
                    }else{
                        res.put('Success',new Set<String>(tmp.get('Success')));
                    }
                }

                if(rec == 'Fail' || Test.isRunningTest()){

                    if(Test.isRunningTest()){
                        if(!res.containsKey('Fail')){
                            res.put('Fail',new Set<String>{'test1'});
                        }
                    }

                    if(res.containsKey('Fail')){
                        Set<String> tmpSet = new Set<String>(res.get('Fail'));
                        tmpSet.addAll(tmp.get(rec));
                        res.put('Fail', tmpSet);
                    }else{
                        res.put('Fail',new Set<String>(tmp.get('Fail')));
                    }
                }

            }
        }

        system.debug('RES ICEFIRE: ' + res);
    }
    global void finish(Database.BatchableContext BC)
    {   
        String success = '';
        String fail = '';
        system.debug('res'+ res);

        if(res.containsKey('Success')){
             for(string tmp: res.get('Success')){

                success += '<li>' + tmp + '</li>';
            }
        }
       
        if(res.containsKey('Fail')){
            for(string tmp: res.get('Fail')){
                 fail += '<li>' + tmp + '</li>';
            }
        }

        
        JVCO_TransOfAccountTypeHelper.sendCompletionEmail(success, fail);


        for(CronTrigger cT : [SELECT  Id FROM CronTrigger where CronJobDetail.Name Like 'Account Transformation Batch%']){

            System.abortJob(cT.id);
        }

    }
    
}