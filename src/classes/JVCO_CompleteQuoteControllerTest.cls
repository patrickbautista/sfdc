/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_CompleteQuoteControllerTest.cls 
   Description:     Test class for JVCO_CompleteQuoteController.cls

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
   28-Dec-2016       0.1        Accenture-raus.k.b.ablaza         Initial version of the code      
   31-Mar-2017       0.2        Accenture-samuel.a.d.oberes       Added custom setting initialization and new assertions
   05-Jul-2017       0.3        Accenture-erica.jane.w.matias     Modified test class to reach 85% code coverage
   18-Aug-2017       0.4        Accenture-carmela.santos   
   07-Jul-2018       0.5        Accenture-jules.osberg.a.pablo    Updated All testmethods to reflect changes for T&C's accepted        
   ---------------------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_CompleteQuoteControllerTest {

    @testSetup
    public static void testSetup() { 

        //Test.startTest();

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
        insert acc2;

        Account licAcc3 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        licAcc3.AccountNumber = '832022';
        LicAcc3.JVCO_Live__c = true;
        insert licAcc3; 

        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        blng__BillingRule__c br = JVCO_TestClassObjectBuilder.createBillingRule();
        insert br;

        SBQQ__PriceRule__c price = JVCO_TestClassObjectBuilder.createPriceRule();
        insert price;

        SBQQ__PriceCondition__c priceCon = JVCO_TestClassObjectBuilder.createPriceCondition(price.id);
        insert priceCon;

        List<SBQQ__PriceAction__c> priceAct = JVCO_TestClassObjectBuilder.createPriceAction(price.id);
        insert priceAct;

        SBQQ__ConfigurationAttribute__c configAttr =JVCO_TestClassObjectBuilder.createConfigurationAttribute(prod.id);
        insert configAttr;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_End_Date__c = null;
        aff.JVCO_Closure_Reason__c = null;
        insert aff;

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        insert opp;


        Test.startTest();
        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
        q.Start_Date__c  = date.today();
        q.End_Date__c  = date.today().addDays(180);
        q.SBQQ__EndDate__c = date.today().addDays(180);
        q.SBQQ__SubscriptionTerm__c  = 12;
        q.Recalculate__c = true;
        q.SBQQ__Status__c  = 'Draft';
        q.JVCO_Contact_Type__c = 'Email';
        q.Covid_19_Impacted1__c = 'No';
        q.Accept_TCs__c = null;
        insert q;

        SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(acc2.id, prod.id);
        insert s;

        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];

        system.debug('@@@Check Test QLG: ' + qlg);

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, prod.Id);
        //put start date and end date
        ql.SBQQ__StartDate__c = q.Start_Date__c;
        ql.SBQQ__EndDate__c = q.End_Date__c;
        ql.Start_Date__c = q.Start_Date__c;
        ql.End_Date__c = q.End_Date__c;
        ql.SPVAvailability__c = true;
        insert ql;
        system.debug('@@@Check Test QL: ' + ql);
        Test.stopTest();
        
        

        //Create Quote using quoteController
        //Apexpages.currentPage().getParameters().put('id',acc2.id);
        //Apexpages.StandardSetController std;

        //JVCO_QuoteCreationController quoteController = new JVCO_QuoteCreationController(std);
        //PageReference pageRef = quoteController.quoteCreate();

        //Opportunity opp = new Opportunity();
        //opp =  [SELECT  Id, AccountId, StageName FROM Opportunity order by CreatedDate DESC limit 1];
        //System.debug('@@OPPO - ' +  opp);
        //SBQQ__Quote__c q = [SELECT Id, SBQQ__Account__c, SBQQ__Opportunity2__c FROM SBQQ__Quote__c order by CreatedDate DESC limit 1];
        //End */
        /*
        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, prod.Id);
        insert ql;

        q.Start_Date__c  = date.today();
        q.SBQQ__SubscriptionTerm__c  = 12;
        q.Recalculate__c = true;
        q.SBQQ__Status__c  = 'Draft';
        q.JVCO_Contact_Type__c = 'Email';
        q.Accept_TCs__c = 'yes';
        update q;

        opp.Probability = 100;
        opp.Amount = 10.00;
        opp.StageName = 'Quoting';
        update opp;         
        */

        //Test.stopTest();
        //Test.startTest();
        List<JVCO_Constants__c> constantCS = new List<JVCO_Constants__c>();
        constantCS.add(new JVCO_Constants__c(JVCO_Quote_Recalculation_Time_Buffer__c = 5));
        insert constantCS;

        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_Opportunity', Value__c = 'Quote did not successfully complete because the \'Opportunity\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_ContactType', Value__c = 'Quote did not successfully complete because the \'Contact Type\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_Primary', Value__c = 'Quote did not successfully complete because the \'Primary\' checkbox was not ticked'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_StartDate', Value__c = 'Quote did not successfully complete because the \'Start Date\' was not provided'));
        //settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_EndDate', Value__c = 'Quote did not successfully complete because the \'End Date\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_SubscriptionTerm', Value__c = 'Quote did not successfully complete because the \'Subscription Term\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'ThresholdAndStatusErrorMsg', Value__c = 'Quote has been submitted for approval. You will be notified when the quote has been approved.'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'UpdateOppAndQuoteFail', Value__c = 'The update did not push through because an error occurred. Details are as follows:'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'UpdateOppAndQuoteSuccess', Value__c = 'Quote Successfully Completed'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_AcceptT&Cs', Value__c = 'Quote did not successfully complete because the \'T&C\' was not checked'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'CancelledQuoteErrorMsg', Value__c = 'You cannot complete and contract a Cancelled quote'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_EndDate', Value__c = 'Quote did not successfully complete because the \'End Date\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'ErrorNoSPV', Value__c = 'You can not contract the Quote since there are Quote Line(s) w/o SPV'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'Recalculation', Value__c = 'Please recalculate Quote before contracting. If you are in renewal, and recalculation is still in progress, please wait to finish before contracting.'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_Covid19Impacted', Value__c = 'Please provide a value for \'Covid 19 Impacted?\' field'));

        insert settings;

        //Test.stopTest();
    } 

    public static testMethod void testOpptyCompletion() {

        SBQQ__Quoteline__c qlRecUpdate =  [select id, JVCO_Steelbrick_Last_Recalculated_Time__c, LastModifiedDate FROM SBQQ__QuoteLine__c limit 1];
        qlRecUpdate.JVCO_Steelbrick_Last_Recalculated_Time__c = qlRecUpdate.LastModifiedDate;
        update qlRecUpdate;

        SBQQ__Quote__c q = [select Id, SBQQ__NetAmount__c, JVCO_Contact_Type__c, Covid_19_Impacted1__c, Recalculate__c, SBQQ__Opportunity2__c, JVCO_QuoteComplete__c, SBQQ__Status__c from SBQQ__Quote__c limit 1];
        PageReference dumpRef;
        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'AmountThreshold', Value__c = '10.00'));

        insert settings;

        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(sc);
        cont.selectedTC = 'Yes';
        
        Test.startTest();
        cont.updateOppAndQuote();
        SBQQ__Quote__c quoteCheck = [select Id, JVCO_QuoteComplete__c, JVCO_Cancelled__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c where Id = :q.Id];
        System.assertEquals(TRUE, quoteCheck.JVCO_QuoteComplete__c);
        System.assertEquals(FALSE, quoteCheck.JVCO_Cancelled__c);
        System.assertEquals('Accepted', quoteCheck.SBQQ__Status__c);
        System.assertEquals(TRUE, quoteCheck.SBQQ__Primary__c);
        dumpRef = cont.returnToQuote();
        Test.stopTest();
    }

    public static testMethod void TestErrorNull() {
        SBQQ__Quoteline__c qlRecUpdate =  [select id, JVCO_Steelbrick_Last_Recalculated_Time__c, LastModifiedDate FROM SBQQ__QuoteLine__c limit 1];
        qlRecUpdate.JVCO_Steelbrick_Last_Recalculated_Time__c = qlRecUpdate.LastModifiedDate;
        update qlRecUpdate;

        SBQQ__Quote__c q = [select Id, SBQQ__Primary__c, SBQQ__Type__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c, JVCO_Contact_Type__c, Covid_19_Impacted1__c, SBQQ__NetAmount__c  from SBQQ__Quote__c limit 1];
        q.JVCO_Contact_Type__c = null;
        q.End_Date__c = System.today() + 30;
        update q; 
        
        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'AmountThreshold', Value__c = '0.00'));
        insert settings;

        Map<String, JVCO_Complete_Quote_Settings__c> settings2 = JVCO_Complete_Quote_Settings__c.getAll();

        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(sc);
        Test.startTest();
        cont.selectedTC = 'Yes';
        cont.updateOppAndQuote();
                
        System.assert(wasMessageAdded(new ApexPages.Message(ApexPages.severity.ERROR, settings2.get('MissingMandatoryField_ContactType').Value__c)));
        Test.stopTest();
    }


    public static testMethod void TestStatusCancelled() {
        SBQQ__Quoteline__c qlRecUpdate =  [select id, JVCO_Steelbrick_Last_Recalculated_Time__c, LastModifiedDate FROM SBQQ__QuoteLine__c limit 1];
        qlRecUpdate.JVCO_Steelbrick_Last_Recalculated_Time__c = qlRecUpdate.LastModifiedDate;
        update qlRecUpdate;

        SBQQ__Quote__c q = [select Id, SBQQ__Primary__c, SBQQ__Type__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c, JVCO_Contact_Type__c, Covid_19_Impacted1__c, SBQQ__NetAmount__c  from SBQQ__Quote__c limit 1];
        q.SBQQ__Status__c  = 'User Cancelled';
        q.End_Date__c = System.today() + 30;
        update q; 

        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'AmountThreshold', Value__c = '0.00'));
        insert settings;

        Map<String, JVCO_Complete_Quote_Settings__c> settings2 = JVCO_Complete_Quote_Settings__c.getAll();

        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(sc);
        Test.startTest();
        cont.selectedTC = 'Yes';
        cont.updateOppAndQuote();
        
        System.assert(wasMessageAdded(new ApexPages.Message(ApexPages.severity.ERROR, settings2.get('CancelledQuoteErrorMsg').Value__c)));
        Test.stopTest();
    }

    public static testMethod void TestErrorNullPrimaryFields() {
        SBQQ__Quoteline__c qlRecUpdate =  [select id, JVCO_Steelbrick_Last_Recalculated_Time__c, LastModifiedDate FROM SBQQ__QuoteLine__c limit 1];
        qlRecUpdate.JVCO_Steelbrick_Last_Recalculated_Time__c = qlRecUpdate.LastModifiedDate;
        update qlRecUpdate;

        SBQQ__Quote__c q = [select Id, SBQQ__Primary__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c  from SBQQ__Quote__c limit 1];
        q.SBQQ__Primary__c = false;     

        update q; 

        Map<String, JVCO_Complete_Quote_Settings__c> settings2 = JVCO_Complete_Quote_Settings__c.getAll();
        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(sc);
        Test.startTest();
        cont.selectedTC = 'Yes';
        cont.updateOppAndQuote();
     
        System.assert(wasMessageAdded(new ApexPages.Message(ApexPages.severity.ERROR, settings2.get('MissingMandatoryField_Primary').Value__c)));
        Test.stopTest();
    }

    public static testMethod void TestErrorNullStartDateFields() {
        SBQQ__Quoteline__c qlRecUpdate =  [select id, JVCO_Steelbrick_Last_Recalculated_Time__c, LastModifiedDate FROM SBQQ__QuoteLine__c limit 1];
        qlRecUpdate.JVCO_Steelbrick_Last_Recalculated_Time__c = qlRecUpdate.LastModifiedDate;
        update qlRecUpdate;

        SBQQ__Quote__c q = [select Id, SBQQ__Primary__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c  from SBQQ__Quote__c limit 1];
        q.Start_Date__c  = null;    

        update q; 

        Map<String, JVCO_Complete_Quote_Settings__c> settings2 = JVCO_Complete_Quote_Settings__c.getAll();
        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(sc);
        Test.startTest();
        cont.selectedTC = 'Yes';
        cont.updateOppAndQuote();

        System.assert(wasMessageAdded(new ApexPages.Message(ApexPages.severity.ERROR, settings2.get('MissingMandatoryField_StartDate').Value__c)));
        Test.stopTest();
    }

    public static testMethod void TestErrorNullEndDateFields() {
        SBQQ__Quoteline__c qlRecUpdate =  [select id, JVCO_Steelbrick_Last_Recalculated_Time__c, LastModifiedDate FROM SBQQ__QuoteLine__c limit 1];
        qlRecUpdate.JVCO_Steelbrick_Last_Recalculated_Time__c = qlRecUpdate.LastModifiedDate;
        update qlRecUpdate;

        SBQQ__Quote__c q = [select Id, SBQQ__Primary__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c  from SBQQ__Quote__c limit 1];
        q.End_Date__c  = null;  

        update q; 

        Map<String, JVCO_Complete_Quote_Settings__c> settings2 = JVCO_Complete_Quote_Settings__c.getAll();
        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(sc);
        Test.startTest();
        cont.selectedTC = 'Yes';
        cont.updateOppAndQuote();
        
        System.assert(wasMessageAdded(new ApexPages.Message(ApexPages.severity.ERROR, settings2.get('MissingMandatoryField_EndDate').Value__c)));
        Test.stopTest();
    }

    public static testMethod void TestErrorNullSubscriptionTermFields() {
        SBQQ__Quoteline__c qlRecUpdate =  [select id, JVCO_Steelbrick_Last_Recalculated_Time__c, LastModifiedDate FROM SBQQ__QuoteLine__c limit 1];
        qlRecUpdate.JVCO_Steelbrick_Last_Recalculated_Time__c = qlRecUpdate.LastModifiedDate;
        update qlRecUpdate;

        SBQQ__Quote__c q = [select Id, SBQQ__Primary__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c  from SBQQ__Quote__c limit 1];
        q.SBQQ__SubscriptionTerm__c = null;     

        update q; 

        Map<String, JVCO_Complete_Quote_Settings__c> settings2 = JVCO_Complete_Quote_Settings__c.getAll();
        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(sc);
        Test.startTest();
        cont.selectedTC = 'Yes';
        cont.updateOppAndQuote();

        System.assert(wasMessageAdded(new ApexPages.Message(ApexPages.severity.ERROR, settings2.get('MissingMandatoryField_SubscriptionTerm').Value__c)));
        Test.stopTest();        
    }

    public static testMethod void TestErrorNullOppFields() {
        SBQQ__Quoteline__c qlRecUpdate =  [select id, JVCO_Steelbrick_Last_Recalculated_Time__c, LastModifiedDate FROM SBQQ__QuoteLine__c limit 1];
        qlRecUpdate.JVCO_Steelbrick_Last_Recalculated_Time__c = qlRecUpdate.LastModifiedDate;
        update qlRecUpdate;

        SBQQ__Quote__c q = [select Id, SBQQ__Primary__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c  from SBQQ__Quote__c limit 1];
        q.SBQQ__Opportunity2__c = null;

        update q; 

        Map<String, JVCO_Complete_Quote_Settings__c> settings2 = JVCO_Complete_Quote_Settings__c.getAll();
        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(sc);
        Test.startTest();
        cont.selectedTC = 'Yes';
        cont.updateOppAndQuote();

        System.assert(wasMessageAdded(new ApexPages.Message(ApexPages.severity.ERROR, settings2.get('MissingMandatoryField_Opportunity').Value__c)));
        Test.stopTest();
    }

    public static testMethod void TestStatusApprovedAmountThresholdGreaterThanNetAmount() {
        SBQQ__Quoteline__c qlRecUpdate =  [select id, JVCO_Steelbrick_Last_Recalculated_Time__c, LastModifiedDate FROM SBQQ__QuoteLine__c limit 1];
        qlRecUpdate.JVCO_Steelbrick_Last_Recalculated_Time__c = qlRecUpdate.LastModifiedDate;
        update qlRecUpdate;

        SBQQ__Quote__c q = [select Id, SBQQ__Primary__c, SBQQ__Type__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c, JVCO_Contact_Type__c, Covid_19_Impacted1__c, SBQQ__NetAmount__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c  from SBQQ__Quote__c limit 1];
        q.SBQQ__Status__c  = 'Draft';
        q.End_Date__c = System.today() + 30;
        update q; 
        q.SBQQ__Type__c = 'Amendment';
        update q; 

        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'AmountThreshold', Value__c = '10000.00'));
        insert settings;

        Map<String, JVCO_Complete_Quote_Settings__c> settings2 = JVCO_Complete_Quote_Settings__c.getAll();

        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(sc);
        Test.startTest();
        cont.selectedTC = 'Yes';
        cont.updateOppAndQuote();
     
        System.assert(wasMessageAdded(new ApexPages.Message(ApexPages.severity.ERROR, 'An error has occured in submitting the record for approval. Please contact your administrator.')));
        Test.stopTest();
    }

    public static testMethod void testContractNoOrder() {
        SBQQ__Quoteline__c qlRecUpdate =  [select id, JVCO_Steelbrick_Last_Recalculated_Time__c, LastModifiedDate FROM SBQQ__QuoteLine__c limit 1];
        qlRecUpdate.JVCO_Steelbrick_Last_Recalculated_Time__c = qlRecUpdate.LastModifiedDate;
        update qlRecUpdate;

        SBQQ__Quote__c q = [select Id, SBQQ__Primary__c, Start_Date__c , SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c  from SBQQ__Quote__c limit 1];
        q.SBQQ__Status__c  = 'Draft';

        Opportunity oppTest = [select id, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity limit 1];
        oppTest.SBQQ__Contracted__c = true;
        oppTest.SBQQ__Ordered__c = false;

        update q; 
        update oppTest;

        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'AmountThreshold', Value__c = '10.00'));
        insert settings;

        Map<String, JVCO_Complete_Quote_Settings__c> settings2 = JVCO_Complete_Quote_Settings__c.getAll();  

        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(sc);
        Test.startTest();
        cont.selectedTC = 'Yes';
        cont.updateOppAndQuote();
     
        System.assert(wasMessageAdded(new ApexPages.Message(ApexPages.severity.CONFIRM, settings2.get('UpdateOppAndQuoteSuccess').Value__c)));
        Test.stopTest();
    }

    public static testMethod void cancelledEvent()
    {
        SBQQ__Quote__c qtRec = [Select Id, SBQQ__Primary__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c,SBQQ__Status__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c From SBQQ__Quote__c Limit 1];
        JVCO_Venue__c venRec = [Select Id from JVCO_Venue__c Limit 1];
        SBQQ__QuoteLine__c qtlRec = [Select Id, SBQQ__Quote__c, JVCO_Event__c From SBQQ__QuoteLine__c where SBQQ__Quote__c =: qtRec.Id Limit 1 ];

        //load artist test data
        List<sObject> artistList = Test.loadData(JVCO_Artist__c.SObjectType, 'JVCO_TestData_Artist');

        Account accLive = [select id from Account where JVCO_Live__c = true limit 1];
        //create event configured for GP_PRS
        JVCO_Event__c eventRec = new JVCO_Event__c();
        eventRec.JVCO_Venue__c = venRec.Id;                
        eventRec.JVCO_Artist__c = artistList.get(0).Id;
        eventRec.JVCO_Event_Start_Date__c = Date.Today();
        eventRec.JVCO_Event_End_Date__c = Date.Today().addDays(25);
        eventRec.JVCO_Invoice_Paid__c = false;
        eventRec.JVCO_Event_Classification_Status__c = 'Classified';
        eventRec.JVCO_Tariff_Code__c = 'GP';
        eventRec.JVCO_Event_Type__c = 'Concert - Qualifies for Old Rate';
        eventRec.JVCO_Event_Quoted__c = false;
        eventRec.JVCO_CancelledEvent__c = false;
        eventRec.JVCO_Event_Invoiced__c = false;
        eventRec.JVCO_Percentage_Controlled__c = 75.0;
        eventRec.JVCO_Percentage_Controlled_Required__c = true;
        eventRec.License_Account__c=accLive.id;
        eventRec.Headline_Type__c = 'No Headliner';
        eventRec.JVCO_Number_of_Set_Lists_Expected__c = 2;
        insert eventRec;

        qtlRec.JVCO_Event__c = eventRec.Id;
        update qtlRec;

        JVCO_Event__c evntRec = [Select Id, JVCO_Event_Quoted__c, JVCO_CancelledEvent__c From JVCO_Event__c Limit 1];
        evntRec.JVCO_Event_Quoted__c = false;
        evntRec.JVCO_CancelledEvent__c = true;
        update evntRec;

        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'AmountThreshold', Value__c = '10000.00'));
        insert settings;

        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController sc = new ApexPages.StandardController(qtRec);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(sc);
        Test.startTest();
        cont.selectedTC = 'Yes';
        cont.updateOppAndQuote();
        
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean b = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('cancelled'))
                b = true;
        }
        system.assert(b);

        Test.stopTest();
    }
 
    private static testMethod void testMethodConfirmTCError() 
    {  
        Test.startTest();

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        List<Account> accountList = new List<Account>([SELECT Id, JVCO_Customer_Account__c, JVCO_Contract_Batch_Size__c FROM Account WHERE RecordTypeId =: licenceRT order by CreatedDate DESC LIMIT 1]);
        List<Account> customerAccountList = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
        customerAccountList[0].TCs_Accepted__c = null;
        update customerAccountList;
        
        SBQQ__Quote__c qtRec = [Select Id, SBQQ__Primary__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c,SBQQ__Status__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c From SBQQ__Quote__c Limit 1];

        ApexPages.StandardController standardController = new ApexPages.StandardController(qtRec);
        JVCO_CompleteQuoteController controller = new JVCO_CompleteQuoteController(standardController);

        controller.selectedTC = NULL;
        controller.updateOppAndQuote();

        System.assert(wasMessageAdded(new ApexPages.Message(ApexPages.severity.ERROR, Label.TermsAndConditionErrorText)));
        Test.stopTest();
    }

    private static testMethod void testMethodConfirmTCSingleSoc() 
    {  
        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();       
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'AmountThreshold', Value__c = '500.00'));       
        insert settings;

        Test.startTest();

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        List<Account> accountList = new List<Account>([SELECT Id, JVCO_Customer_Account__c, JVCO_Contract_Batch_Size__c FROM Account WHERE RecordTypeId =: licenceRT order by CreatedDate DESC LIMIT 1]);
        List<Account> customerAccountList = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
        customerAccountList[0].TCs_Accepted__c = null;
        update customerAccountList;

        SBQQ__Quoteline__c qlRecUpdate =  [select id, JVCO_Steelbrick_Last_Recalculated_Time__c, LastModifiedDate FROM SBQQ__QuoteLine__c limit 1];
        qlRecUpdate.JVCO_Steelbrick_Last_Recalculated_Time__c = qlRecUpdate.LastModifiedDate;
        update qlRecUpdate;
        
        SBQQ__Quote__c qtRec = [Select Id, JVCO_Single_Society__c, SBQQ__Primary__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c,SBQQ__Status__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c From SBQQ__Quote__c Limit 1];
        qtRec.JVCO_Single_Society__c = true;
        update qtRec;

        ApexPages.StandardController standardController = new ApexPages.StandardController(qtRec);
        JVCO_CompleteQuoteController controller = new JVCO_CompleteQuoteController(standardController);

        controller.selectedTC = NULL;
        controller.updateOppAndQuote();

        System.assertEquals(controller.tcAccepted, TRUE);
        Test.stopTest();
    }

    //private static testMethod void testMethodConfirmTCNo() 
    //{  
    //    Test.startTest();

    //    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
    //    List<Account> accountList = new List<Account>([SELECT Id, JVCO_Customer_Account__c, JVCO_Contract_Batch_Size__c FROM Account WHERE RecordTypeId =: licenceRT order by CreatedDate DESC LIMIT 1]);
    //    List<Account> customerAccountList = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
    //    customerAccountList[0].TCs_Accepted__c = null;
    //    update customerAccountList;
        
    //    SBQQ__Quote__c qtRec = [Select Id, SBQQ__Primary__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c,SBQQ__Status__c From SBQQ__Quote__c Limit 1];
        
    //    ApexPages.StandardController standardController = new ApexPages.StandardController(qtRec);
    //    JVCO_CompleteQuoteController controller = new JVCO_CompleteQuoteController(standardController);

    //    controller.selectedTC = 'No';
    //    controller.updateOppAndQuote();

    //    List<Account> customerAccountList2 = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
    //    System.assertEquals('No', customerAccountList2[0].TCs_Accepted__c);
    //    Test.stopTest();
    //}

    private static testMethod void testMethodConfirmTCYes() 
    {  
        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'AmountThreshold', Value__c = '10.00'));
        insert settings;

        Test.startTest();

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        List<Account> accountList = new List<Account>([SELECT Id, JVCO_Customer_Account__c, JVCO_Contract_Batch_Size__c FROM Account WHERE RecordTypeId =: licenceRT order by CreatedDate DESC LIMIT 1]);
        List<Account> customerAccountList = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
        customerAccountList[0].TCs_Accepted__c = null;
        update customerAccountList;

        SBQQ__Quoteline__c qlRecUpdate =  [select id, JVCO_Steelbrick_Last_Recalculated_Time__c, LastModifiedDate FROM SBQQ__QuoteLine__c limit 1];
        qlRecUpdate.JVCO_Steelbrick_Last_Recalculated_Time__c = qlRecUpdate.LastModifiedDate;
        update qlRecUpdate;

        SBQQ__Quote__c qtRec = [Select Id, SBQQ__Primary__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c,SBQQ__Status__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c From SBQQ__Quote__c Limit 1];
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(qtRec);
        JVCO_CompleteQuoteController controller = new JVCO_CompleteQuoteController(standardController);

        controller.selectedTC = 'Yes';
        controller.updateOppAndQuote();
        List<Selectoption> tcFields = controller.getselectedTCfields();

        List<Account> customerAccountList2 = new List<Account>([SELECT Id, TCs_Accepted__c FROM Account WHERE Id = :accountList[0].JVCO_Customer_Account__c LIMIT 1]);
        System.assertEquals('Yes', customerAccountList2[0].TCs_Accepted__c);
        Test.stopTest();
    }

    private static Boolean wasMessageAdded(ApexPages.Message message) {
         //System.assertNotEquals(TRUE, pageMessages.hasMessages());

        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertNotEquals(0, pageMessages.size());

        Boolean messageFound = false;

        for(ApexPages.Message msg : pageMessages) {
            if(msg.getSummary() == message.getSummary()
                && msg.getDetail() == message.getDetail()
                && msg.getSeverity() == message.getSeverity()) {
                messageFound = true;        
            }
        }

        return messageFound;
    }

    private static testMethod void testNoSPV() {
        SBQQ__Quote__c q = [select Id, SBQQ__NetAmount__c, JVCO_Contact_Type__c, Covid_19_Impacted1__c, Recalculate__c, SBQQ__Opportunity2__c, JVCO_QuoteComplete__c, SBQQ__Status__c, NoOfQuoteLinesWithoutSPV__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c, Start_Date__c from SBQQ__Quote__c limit 1];
        SBQQ__QuoteLine__c ql = [select Id, SPVAvailability__c, ParentProduct__c from SBQQ__QuoteLine__c where SBQQ__Quote__c = :q.Id];

        ql.SPVAvailability__c = false;
        ql.ParentProduct__c = false;

        update ql;

        SBQQ__Quote__c q1 = [select Id, SBQQ__NetAmount__c, JVCO_Contact_Type__c, Covid_19_Impacted1__c, Recalculate__c, SBQQ__Opportunity2__c, JVCO_QuoteComplete__c, SBQQ__Status__c, NoOfQuoteLinesWithoutSPV__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c, Start_Date__c from SBQQ__Quote__c limit 1];

        System.assertNotEquals(0, q1.NoOfQuoteLinesWithoutSPV__c);

        PageReference dumpRef;
        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'AmountThreshold', Value__c = '10.00'));

        insert settings;

        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController sc = new ApexPages.StandardController(q1);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(sc);
        cont.selectedTC = 'Yes';
        
        Test.startTest();
        cont.updateOppAndQuote();
        SBQQ__Quote__c quoteCheck = [select Id,SBQQ__Status__c, Start_Date__c, JVCO_Recalculated__c from SBQQ__Quote__c where Id = :q.Id];
        System.assertNotEquals('Accepted', quoteCheck.SBQQ__Status__c);
        dumpRef = cont.returnToQuote();
        Test.stopTest();
    }

    private static testMethod void testQLGWithNoLine() {
        SBQQ__Quote__c q = [select Id, SBQQ__NetAmount__c, JVCO_Contact_Type__c, Covid_19_Impacted1__c, Recalculate__c, SBQQ__Opportunity2__c, JVCO_QuoteComplete__c, SBQQ__Status__c, NoOfQuoteLinesWithoutSPV__c, JVCO_Last_Recalculated_Time_Delta__c, JVCO_Recalculated__c from SBQQ__Quote__c limit 1];

        SBQQ__QuoteLine__c ql = [select Id, SPVAvailability__c, ParentProduct__c, JVCO_Steelbrick_Last_Recalculated_Time__c, LastModifiedDate from SBQQ__QuoteLine__c where SBQQ__Quote__c = :q.Id];
        ql.JVCO_Steelbrick_Last_Recalculated_Time__c = ql.LastModifiedDate;
        update ql;

        SBQQ__QuoteLineGroup__c qlg = new SBQQ__QuoteLineGroup__c();
        qlg.Name = 'Test Group2';
        qlg.SBQQ__Quote__c = q.id;
        insert qlg;

        Test.startTest();
        PageReference dumpRef;
        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(sc);       

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean b = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('One or more Quote Groups (Venues)'))
                b = true;
        }
        //system.assert(b);
        dumpRef = cont.returnToQuote();
        Test.stopTest();
    }
}