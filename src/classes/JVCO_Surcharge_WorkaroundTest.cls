@isTest
public class JVCO_Surcharge_WorkaroundTest {
    
    @isTest 
    static void createSurchargeInvoice()
    {
        Set<Id> sInvIdSet = new Set<Id>();
        sInvIdSet.add(userInfo.getUserId());
        Test.startTest();
        JVCO_Surcharge_Workaround swa = new JVCO_Surcharge_Workaround(sInvIdSet);
        Database.QueryLocator ql = swa.start(null);
        swa.execute(null, new List<c2g__codaInvoice__c>());
        swa.finish(null);
        Test.stopTest();
            
    }
    
    @isTest
    static void createSurchargeInvoiceNull()
    {
        Set<Id> sInvIdSet = new Set<Id>();
        sInvIdSet.clear();
        Test.startTest();
        JVCO_Surcharge_Workaround swa = new JVCO_Surcharge_Workaround(sInvIdSet);
        Database.QueryLocator ql = swa.start(null);
        swa.execute(null, new List<c2g__codaInvoice__c>());
        swa.finish(null);
        Test.stopTest();
            
    }

}