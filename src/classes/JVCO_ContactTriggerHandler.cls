/* ----------------------------------------------------------------------------------------------
   Name: JVCO_ContactTriggerHandler
   Description: Logic for Updating fields on Account after Contact Update

   Date         Version     Author               Summary of Changes
   -----------  -------     -----------------    -----------------------------------------
  14-Sept-2017   0.1         Rhys Dela Cruz       Initial creation - GREEN-23061
  28-Aug-2018    0.2         joseph.g.barrameda   Added validation before updating or deleting Contact - GREEN-32807
  ----------------------------------------------------------------------------------------------- */

public class JVCO_ContactTriggerHandler {

  public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
  public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_ContactTrigger__c : false;

  public static void onAfterUpdate(List<Contact> contList, Map<id, Contact> oldContMap) {
    if(!skipTrigger) 
        {
          updateLicenceAccountBillingPhone(contList, oldContMap);
          updateLicenceAccountBillingBillingAddress(contList, oldContMap);
        }
  }
  
  public static void onBeforeUpdate(List<Contact> contList, Map<id,Contact> oldContMap){
        validateEmailOrAddress(contList, oldContMap);
  }
  
  public static void onBeforeDelete(List<Contact>contList, Map<id,Contact> oldContMap){
      preventContactDeletionIfAssociatedToLA(contList);
  }


  /* ----------------------------------------------------------------------------------------------------------
    Author:         rhys.j.c.dela.cruz
    Company:        Accenture
    Description:    Retrieves Billing Contacts that have Billing Phone and updates related Licence Account
    Inputs:         Contact
    Returns:        n/a
    <Date>          <Authors Name>                      <Brief Description of Change>
    14-Sept-2017     rhys.j.c.dela.cruz                   Initial version of the code
  ----------------------------------------------------------------------------------------------------------*/

  public static void updateLicenceAccountBillingPhone(List<Contact> contList, Map<id, Contact> oldContMap) {

    Map<Id, Account> accMap = new Map<Id, Account>();
    Set<Id> accIdSet = new Set<Id>();
    Set<Id> contIdSet = new Set<Id>();
    List<Account> accList = new List<Account>();
    //Id accLicenceRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    String billingPhone;

    Map<Id, Contact> contactMap = new Map<Id, Contact>(contList);

    System.debug('@@@contList: ' + contList);

    for (Contact cont : contList) {
      if (cont.AccountId != null) {
        if (cont.Phone != oldContMap.get(cont.Id).Phone || cont.MobilePhone != oldContMap.get(cont.Id).MobilePhone) {
          accIdSet.add(cont.AccountId);
          contIdSet.add(cont.Id);
        }
      }
    }

    List<Account> listAccount = new List<Account>([SELECT id, Phone, JVCO_Billing_Contact__c FROM Account WHERE JVCO_Customer_Account__c IN: accIdSet AND JVCO_Billing_Contact__c IN: contIdSet]);

    System.debug('@@@AccountMap: ' + accMap);

    if (!accIdSet.isEmpty()) {

      for(Account acc : listAccount)
      {
        if(contactMap.get(acc.JVCO_Billing_Contact__c).Phone == null && contactMap.get(acc.JVCO_Billing_Contact__c).MobilePhone != null){
          acc.Phone = contactMap.get(acc.JVCO_Billing_Contact__c).MobilePhone;
        }
        if((contactMap.get(acc.JVCO_Billing_Contact__c).Phone != null && contactMap.get(acc.JVCO_Billing_Contact__c).MobilePhone != null) || (contactMap.get(acc.JVCO_Billing_Contact__c).Phone != null && contactMap.get(acc.JVCO_Billing_Contact__c).MobilePhone == null))
        {
          acc.Phone = contactMap.get(acc.JVCO_Billing_Contact__c).Phone;
        }
        accList.add(acc);
      }
      update accList;
    }
  }
  /* ----------------------------------------------------------------------------------------------------------
    Author:         patrick.t.bautista
    Company:        Accenture
    Description:    Retrieves Mailing Address that have Billing Address and updates related Licence Account / GREEN-23771
    Inputs:         Contact
    Returns:        n/a
    <Date>          <Authors Name>                      <Brief Description of Change>
    17-Apr-2017     patrick.t.bautista                   Initial version of the code
  ----------------------------------------------------------------------------------------------------------*/
  public static void updateLicenceAccountBillingBillingAddress(List<Contact> contList, Map<id, Contact> oldContMap) 
    {
        Set<id> accSet = new Set<Id>();
        Map<id,Contact> conMap = new Map<id,Contact>();
        List<Account> accList = new List<Account>();
        for (Contact cont : contList) {
          if (cont.AccountId != null && (cont.MailingStreet != oldContMap.get(cont.Id).MailingStreet 
                                           || cont.MailingCity != oldContMap.get(cont.Id).MailingCity
                                           || cont.MailingPostalCode != oldContMap.get(cont.Id).MailingPostalCode 
                                           || cont.MailingState != oldContMap.get(cont.Id).MailingState))
            {
                accSet.add(cont.AccountId);
                conMap.put(cont.id, cont);
            }
        for(Account accToupdate: [SELECT id, BillingCity, BillingStreet, BillingPostalCode, BillingState, 
                                  JVCO_Billing_Contact__c
                                  FROM Account 
                                  WHERE RecordType.Name = 'Licence Account' 
                                  AND JVCO_Customer_Account__c IN: accSet
                                  AND JVCO_Billing_Contact__c IN: conMap.values()
                                  ])
        {
            Contact contct = conMap.get(accToupdate.JVCO_Billing_Contact__c);
            acctoUpdate.BillingCity = contct.MailingCity;
            acctoUpdate.BillingStreet = contct.MailingStreet;
            acctoUpdate.BillingPostalCode = contct.MailingPostalCode;
            acctoUpdate.BillingState = contct.MailingState;
            accList.add(acctoUpdate);
        }
        if(!accList.isEmpty())
        {
            update accList;
        }
      }
    }
    
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    Prevents contact email or address be cleared/updated if it is still associated in Licence Account - GREEN-32807
    Inputs:         Contact
    Returns:        n/a
    <Date>          <Authors Name>                      <Brief Description of Change>
    28-Aug-2018     joseph.g.barrameda                   Initial version of the code
  ----------------------------------------------------------------------------------------------------------*/
    public static void validateEmailOrAddress(List<Contact> contList, Map<id, Contact> oldContMap){
            
            Set<Id> checkEmailSet = new Set<Id>();
            Set<Id> checkAddressSet = new Set<Id>();
            Set<Id> checkStatusSet = new Set<Id>();
            
            for(Contact con: contList){
                //Check if email is being cleared/deleted
                if((con.Email == '' || con.Email == null) && oldContMap.get(con.Id).Email != null){              
                    checkEmailSet.add(con.Id);
                }
                //Check if Mailing Address is being cleared/deleted
                if((con.MailingStreet == null && oldContMap.get(con.Id).MailingStreet != null) ||          
                   (con.MailingCity == null && oldContMap.get(con.Id).MailingCity != null) ||
                   (con.MailingPostalCode == null && oldContMap.get(con.Id).MailingPostalCode != null)){
                   checkAddressSet.add(con.Id);               
                }                
                //Check if Contact Status is being changed to Inactive
                if(con.JVCO_Contact_Status__c == 'Inactive' && oldContMap.get(con.Id).JVCO_Contact_Status__c != 'Inactive'){              
                    checkStatusSet.add(con.Id);
                }                
            }
            
            if(checkEmailSet.size()>0){
                //Check if the Contact is associated to any Licence Account with Email as Preferred Communication Channel
                List<Account> acctList =[SELECT ID from Account WHERE ffps_custRem__Preferred_Communication_Channel__c='Email' AND 
                                    (JVCO_Billing_Contact__c IN:checkEmailSet OR JVCO_Review_Contact__c IN:checkEmailSet OR 
                                    JVCO_Licence_Holder__c IN:checkEmailSet OR JVCO_DD_Payee_Contact__c IN:checkEmailSet)];
                
                if(acctList.size()>0){
                    contList[0].addError('You cannot delete the Email of the Contact because it is used as a Billing/Review/Licence/DD Payee contact on a licence account');    
                }
            }
            
            if(checkAddressSet.size()>0){
                List<Account> acctList =[SELECT ID from Account WHERE ffps_custRem__Preferred_Communication_Channel__c='Print' AND 
                                    (JVCO_Billing_Contact__c IN: checkAddressSet OR JVCO_Review_Contact__c IN: checkAddressSet OR 
                                    JVCO_Licence_Holder__c IN: checkAddressSet OR JVCO_DD_Payee_Contact__c IN: checkAddressSet)];
          
                if(acctList.size()>0){
                    contList[0].addError('You cannot delete the address of the Contact because it is used as a Billing/Review/Licence/DD Payee contact on a licence account');    
                }  
            }
            
            if(checkStatusSet.size()>0){
                List<Account> acctList =[SELECT ID from Account WHERE 
                                    (JVCO_Billing_Contact__c IN: checkStatusSet OR JVCO_Review_Contact__c IN: checkStatusSet OR 
                                    JVCO_Licence_Holder__c IN: checkStatusSet OR JVCO_DD_Payee_Contact__c IN: checkStatusSet )];
          
                if(acctList.size()>0){
                    contList[0].addError('You cannot make the Contact inactive because it is used as a Billing/Review/Licence/DD Payee contact on a licence account');    
                }  
            }
            
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    Prevent contact deletion if Contact is associated in Licence Account - GREEN-32807
    Inputs:         Contact
    Returns:        n/a
    <Date>          <Authors Name>                      <Brief Description of Change>
    28-Aug-2018     joseph.g.barrameda                   Initial version of the code
  ----------------------------------------------------------------------------------------------------------*/    
    public static void preventContactDeletionIfAssociatedToLA(List<Contact> contList){
    
        Set<Id> conIdSet = new Set<Id>();
                
        for(Contact con: contList){
            conIdSet.add(con.Id);
        } 
        system.debug(conIDset);
        
        //Check if the Contact is associated to any Licence Account 
        List<Account> acctList =[SELECT ID from Account WHERE JVCO_Billing_Contact__c IN:conIdSet 
                                    OR JVCO_Review_Contact__c IN: conIdSet OR JVCO_Licence_Holder__c IN: conIdSet 
                                    OR JVCO_DD_Payee_Contact__c IN: conIdSet];
        
        if (acctList.size()>0){
            contList[0].addError('You cannot delete the Contact because it is used as a Billing/Review/Licence/DD Payee contact on a licence account');
        }                           
    }
    
}