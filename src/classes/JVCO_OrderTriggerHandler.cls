/* ----------------------------------------------------------------------------------------------------------
    Name:            JVCO_OrderTriggerHandler.cls 
    Description:     Trigger handler for JVCO_OrderTrigger.trigger
    Test class:      

    Date             Version     Author                            Summary of Changes 
    -----------      -------     -----------------                 -------------------------------------------
    29-Jun-2017      0.1         Accenture-marlon.c.ocillos         Initial version of the code     
    11-Sep-2017      0.2         Accenture-reymark.j.l.arlos        Updated setCreditReasonToBlank - Added the update of credit reason to blank if total amount is zero and
                                                                    update of credit reason to 'Invoice amendment' if total amount is negative GREEN-22813 
    30-Jan-2018      0.3        Accenture-rhys.j.c.dela.cruz        GREEN-29541 - Added logic for updating OrderItems to prevent where Order is active but OrderItem is not.                                                                  
---------------------------------------------------------------------------------------------------------- */
public class JVCO_OrderTriggerHandler
{
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_OrderTrigger__c : false;
    public static boolean approvalCreateOrder = false;
    
    public static void onBeforeInsert(List<Order> newOrderList, Map<Id, Order> oldOrderMap)
    {
        if(!skipTrigger) 
        {
            checkQuote(newOrderList);
            updateCurrentWeekEndDate(newOrderList, oldOrderMap);
            if(approvalCreateOrder)
            {
                for(Order rec:newOrderList)
                {
                    rec.orderViaApprovalProcess__c = TRUE;
                }
            }
        }
    }
    
    public static void onAfterInsert(List<Order> newList)
    {
        if(!skipTrigger) 
        {
            if(approvalCreateOrder)
            {
                createTaskRecord(newList);
            }
        }
    }
    
    public static void onBeforeUpdate(List<Order> newOrderList, Map<Id, Order> oldOrderMap)
    {
        if(!skipTrigger) 
        { 
            setCreditReasonToBlank(newOrderList);
            updateCurrentWeekEndDate(newOrderList, oldOrderMap);
            if(!JVCO_FFUtil.stopBillNowFuction){
                generateManualInvoiceOrCreditNote(newOrderList, oldOrderMap);
            }
        }
    }
    
    public static void onAfterUpdate(List<Order> newList, Map<Id, Order> oldMap)
    {
        if(!skipTrigger) 
        { 
            priceCalculationFailed(newList);
            updateOrderProducts(newList);
            closeTaskWhenOrderHasBeenActivated(newList, oldMap);
            createPaidAmtTrackerWhenUnmatched(newList, oldMap);
        }
    }
    
    /* ----------------------------------------------------------------------------------------------------------
        Author:         marlon.c.ocillos
        Company:        Accenture
        Description:    Sets JVCO_Credit_Reason__c to blank if SBQQ__TotalAmount__c > 0
        Inputs:         List of Accounts, Map of Accounts
        Returns:        n/a
        <Date>          <Authors Name>                      <Brief Description of Change> 
        29-Jun-2017     marlon.c.ocillos                     Initial version of the code
        11-Sep-2017     reymark.j.l.arlos                    Added the update of credit reason to blank if total amount is zero and
                                                             update of credit reason to 'Invoice amendment' if total amount is negative
        07-Nov-2019     franz.g.a.dimaapi                    GREEN-35079 - Refactor and Fix Credit Reason issue
    ----------------------------------------------------------------------------------------------------------*/
    private static void setCreditReasonToBlank(List<Order> newOrderList)
    {
        for(Order o : newOrderList)
        {
            if(o.JVCO_Gross_Total__c >= 0 && o.JVCO_Credit_Reason__c != null)
            {
                o.JVCO_Credit_Reason__c = '';
            }else if(o.JVCO_Gross_Total__c < 0 && o.JVCO_Credit_Reason__c != 'Invoice amendment')
            {
                o.JVCO_Credit_Reason__c = 'Invoice amendment';
            }
        }
    }
    
    /* ------------------------------------------------------------------------------------------
        Author: ashok.kumar.ramanna
        Company: Accenture
        Description: Update Week of invoice 
        Input: List<blng__Invoice__c> Map<Id,blng__Invoice__c> oldBInvMap
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Dec-2017 ashok.kumar.ramanna  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static void updateCurrentWeekEndDate(List<Order> orderList, Map<Id,Order> oldBInvMap)
    {
        Date currentWeekSundayDate = getCurrentWeekSundayDate();
        for(Order ordObj : orderList)
        {
            if((Trigger.isInsert && ordObj.JVCO_Order_Status__c == 'Posted') || 
                (Trigger.isUpdate  && ordObj.JVCO_Order_Status__c == 'Posted' && 
                    ordObj.JVCO_Order_Status__c != oldBInvMap.get(ordObj.id).JVCO_Order_Status__c 
                 && ordObj.JVCO_Week__c == null))
            {
                ordObj.JVCO_Week__c = currentWeekSundayDate;
            }
        }
    }
    
    /* ------------------------------------------------------------------------------------------
        Author: ashok.kumar.ramanna
        Company: Accenture
        Description: Calculate current weeks sunday date 
        Input: none
        Returns: Date
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Dec-2017 ashok.kumar.ramanna  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static Date getCurrentWeekSundayDate()
    {
        DateTime currentDate = System.now();
        Integer numberOfDays = Date.daysInMonth(currentDate.year(), currentDate.month());
        Date lastDayOfMonth = Date.newInstance(currentDate.year(), currentDate.month(), numberOfDays);
        
        while(!currentDate.format('E').equalsIgnoreCase('Sun'))
        {
            currentDate = Date.newInstance(currentDate.year(), currentDate.month(), currentDate.day()).addDays(1);
            if(currentDate.isSameDay(lastDayOfMonth)) 
            {
                break;
            }
        }
        
        return date.newInstance(currentDate.year(), currentDate.month(), currentDate.day());        
    }

    /* ----------------------------------------------------------------------------------------------------------
        Author:         eu.rey.t.cadag
        Company:        Accenture
        Description:    GREEN-21336: updates the failed Order Status to Completed and update related Order Products' Full Term Net Price to 0.0
        Inputs:         List of Order
        Returns:        n/a
        <Date>          <Authors Name>                      <Brief Description of Change> 
        29-Jun-2017     marlon.c.ocillos                     Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public static void priceCalculationFailed(List<Order> newList)
    {
        List<Order> lstOrder = new List<Order>();
        List<OrderItem> lstOrderProd = new List<OrderItem>();
        List<sObject> lstObject = new List<sObject>();
        for(Order ord: newList)
        {
            if(String.ValueOf(ord.SBQQ__PriceCalcStatus__c).equalsIgnoreCase('Failed') || String.ValueOf(ord.SBQQ__PriceCalcStatus__c).equalsIgnoreCase('Queued'))
            {
                Order ordRecUpdte = new Order();
                ordRecUpdte.Id = ord.Id;
                ordRecUpdte.SBQQ__PriceCalcStatus__c = 'Completed';
                lstOrder.add(ordRecUpdte);
            }
        }
        if(lstOrder.size()>0)
        {
            lstObject.addAll((List<sObject>)lstOrder);
            for(OrderItem OrdProd: [SELECT Id, SBQQ__UnproratedNetPrice__c FROM OrderItem WHERE OrderId IN: lstOrder])
            {
                OrdProd.SBQQ__UnproratedNetPrice__c = 0.0;
                lstOrderProd.add(OrdProd);
            }
            if(lstOrderProd.size() > 0)
            {
                lstObject.addAll((List<sObject>)lstOrderProd);
            }
        }
        if(lstObject.size() > 0)
        {
            JVCO_QuoteUtils.updateObject(lstObject);
        }
    }

    /* ----------------------------------------------------------------------------------------------------------
        Author:         rhys.j.c.dela.cruz
        Company:        Accenture
        Description:    Activate or Deactivate Order product records from the Order record
        Inputs:         List of Order
        Returns:        n/a
        <Date>          <Authors Name>                      <Brief Description of Change> 
        30-Jan-18        rhys.j.c.dela.cruz                  Initial version of the code
        04-Apr-2018      franz.g.a.dimaapi                   Hold Billing for Negative Migrated Contract
    ----------------------------------------------------------------------------------------------------------*/
    private static void updateOrderProducts(List<Order> orderList)
    {
        List<OrderItem> orderItemsUpdate = new List<OrderItem>();
        for(OrderItem ordItem : [SELECT id, Order.Status, SBQQ__Activated__c, blng__BillingRule__c
                                FROM OrderItem 
                                WHERE OrderId IN: orderList])
        {
            Boolean isActivated = (ordItem.blng__BillingRule__c != null && ordItem.Order.Status == 'Activated');
            if(isActivated != ordItem.SBQQ__Activated__c)
            {
                ordItem.SBQQ__Activated__c  = isActivated; 
                orderItemsUpdate.add(ordItem);
            } 
        }
        update orderItemsUpdate;
    }
    
    /* ----------------------------------------------------------------------------------------------------------
        Author:         robert.j.b.lacatan
        Company:        Accenture
        Description:    Create task record when an order is created after quote approval process
        Inputs:         List of Order
        Returns:        n/a
        <Date>          <Authors Name>                      <Brief Description of Change> 
        13-Jun-18       robert.j.b.lacatan                   Initial version of the code
        
    ----------------------------------------------------------------------------------------------------------*/
    private static void createTaskRecord(List<Order> orderList)
    {   Set<Id> ordIdSet = new Set<Id>();
        List<Task> createTaskRecordList = new List<Task>();
        
        for(Order ordRec: orderList){
            ordIdSet.add(ordRec.Id);
        }
        
        if(!ordIdSet.isEmpty()){

            for(Order orderRec: [SELECT Id, SBQQ__Quote__r.Name, OrderNumber, AccountId, SBQQ__Quote__r.OwnerId, SBQQ__Quote__c FROM Order where Id IN:ordIdSet]){
                String descriptionString = '';
                Task taskRec = new Task();
                taskRec.Subject = 'Quote Approved';
                taskRec.Order_Id__c = orderRec.Id;
                taskRec.OwnerId = orderRec.SBQQ__Quote__r.OwnerId;
                taskRec.WhatId = orderRec.AccountId;
                taskRec.Priority = 'High';
                taskRec.Status = 'Open';
                taskRec.JVCO_Type__c = 'Admin';
                taskRec.ReminderDateTime = DateTime.Now().AddDays(1);
                taskRec.ActivityDate= Date.Today().AddDays(1);
                descriptionString = 'Your Quote "'+ orderRec.SBQQ__Quote__r.Name+  '" has been approved, please ensure the related Order "'+ orderRec.OrderNumber+ '" is Activated to generate the Credit Note. Once the Order is activated this task will automatically close';
                taskRec.Description = descriptionString;
                createTaskRecordList.add(taskRec);
            
            }  
            insert createTaskRecordList;
        }
    }
    
    
    /* ----------------------------------------------------------------------------------------------------------
        Author:         robert.j.b.lacatan
        Company:        Accenture
        Description:    Close task record when an order is activated
        Inputs:         List of Order
        Returns:        n/a
        <Date>          <Authors Name>                      <Brief Description of Change> 
        13-Jun-18       robert.j.b.lacatan                   Initial version of the code
        
    ----------------------------------------------------------------------------------------------------------*/
    private static void closeTaskWhenOrderHasBeenActivated(List<Order> orderList, Map<Id,Order> oldMap)
    {   Set<Id> ordIdSet= new Set<Id>();
        List<Task> closeTaskRecordList = new List<Task>();
        
        for(Order ordRec: orderList)
        {
            if(ordRec.Status== 'Activated' && oldMap.get(ordRec.Id).Status == 'Draft' && ordRec.orderViaApprovalProcess__c)
            {
                ordIdSet.add(ordRec.Id);
            }
            
        
        }
        if(!ordIdSet.isEmpty()){

            for(Task taskRec: [SELECT Id, Status FROM Task where Order_Id__c IN:ordIdSet AND Status = 'Open']){
                
                taskRec.Status = 'Completed';
                closeTaskRecordList.add(taskRec);
            }  
            update closeTaskRecordList;
        }
    }
    
    /* ------------------------------------------------------------------------------------------
        Author: mariel.m.buena
        Company: Accenture
        Description: This method copies the account JVCO_Review_Type__c to billing JVCO_Review_Type__c
        Input: List<blng__Invoice__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        23-May-2017  mariel.m.buena      Initial version of function
        15-Oct-2019  franz.g.a.dimaapi   Combined Checkquote and Review Type method
    ----------------------------------------------------------------------------------------------- */
    private static void checkQuote(List<Order> orderList)
    {
        Set<Id> quoteIdSet = new Set<Id>();
        for(Order ord : orderList)
        {
            if(ord.SBQQ__Quote__c != null)
            {
                quoteIdSet.add(ord.SBQQ__Quote__c);
            }
        }

        Map<Id, SBQQ__Quote__c> quoteMap = new Map<Id, SBQQ__Quote__c>();
        if(!quoteIdSet.isEmpty())
        {
            quoteMap = new Map<Id, SBQQ__Quote__c>([SELECT Id,
                                                    SBQQ__Account__r.JVCO_Customer_Account__r.TCs_Accepted__c,
                                                    SBQQ__Type__c,
                                                    SBQQ__MasterContract__c,
                                                    Accept_TCs__c,
                                                    SBQQ__NetAmount__c,
                                                    JVCO_Payment_Terms__c,
                                                    Start_Date__c,
                                                    End_Date__c
                                                    FROM SBQQ__Quote__c
                                                    WHERE Id IN :quoteIdSet]);
        }

        for(Order ord : orderList)
        {
            if(quoteMap.containsKey(ord.SBQQ__Quote__c))
            {
                SBQQ__Quote__c quote = quoteMap.get(ord.SBQQ__Quote__c);
                // Set Licence Dates
                ord.JVCO_Temp_Start_Date__c = quote.Start_Date__c;
                ord.JVCO_Temp_End_Date__c = quote.End_Date__c;
                // Set Invoice Type according to field values; default value is Standard
                if(quote.SBQQ__Type__c.equals('Amendment') && quote.SBQQ__MasterContract__c != null && quote.SBQQ__NetAmount__c > 0)
                {
                    ord.JVCO_Invoice_Type__c = 'Supplementary';
                }else if(quote.SBQQ__Account__r.JVCO_Customer_Account__r.TCs_Accepted__c == 'No')
                {
                    ord.JVCO_Invoice_Type__c = 'Pre-Contract';
                }else if(quote.Accept_TCs__c == 'Yes' || quote.Accept_TCs__c == null)
                {
                    ord.JVCO_Invoice_Type__c = 'Standard';
                }
            }
        }
    }
    
    public static void generateManualInvoiceOrCreditNote(List<Order> newOrderList, Map<Id, Order> oldOrderMap){
        
        Set<Id> orderwithQuoteSet = new Set<Id>();
        List<Order> orderWithoutQuoteList = new List<Order>();
        for(Order ord : newOrderList)
        {
            if(ord.JVCO_Order_Status__c != oldOrderMap.get(ord.Id).JVCO_Order_Status__c
               && ord.JVCO_Order_Status__c == 'Posted' && ord.SBQQ__Quote__c != null
               && ord.JVCO_Sales_Invoice__c == null 
               && ord.Status == 'Activated'
               && ord.JVCO_Sales_Credit_Note__c == null)
            {
                orderwithQuoteSet.add(ord.Id);
            }
            else if(ord.JVCO_Order_Status__c != oldOrderMap.get(ord.Id).JVCO_Order_Status__c
                    && ord.JVCO_Order_Status__c == 'Posted' && ord.SBQQ__Quote__c == null 
                    && ord.JVCO_Sales_Invoice__c == null 
                    && ord.JVCO_Sales_Credit_Note__c == null
                    && ord.Status == 'Activated'
                    && ord.JVCO_Manual_Invoice__c)
            {
                orderWithoutQuoteList.add(ord);
            }
        }
        if(!orderwithQuoteSet.isEmpty()){
            Database.executeBatch(new JVCO_InvoiceAndCreditNoteSchedulerBatch(orderwithQuoteSet));
        }
        if(!orderWithoutQuoteList.isEmpty()){
            JVCO_InvoiceAndCreditNoteManualLogic.setupInvoiceAndCreditNoteHeader(orderWithoutQuoteList);
        }
    }
    
    /* ------------------------------------------------------------------------------------------
        Author: patrick.t.bautista
        Company: Accenture
        Description: This method creates JVCO_Invoice_Line_Paid_Amount_Tracker__c record when 
                     Distribution status equals Unmatched and Cancelled
        Input: List<blng__Invoice__c>
        Returns: void
        <Date>        <Authors Name>          <Brief Description of Change> 
        19-Mar-2019   patrick.t.bautista      Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static void createPaidAmtTrackerWhenUnmatched(List<Order> newOrdList, Map<Id, Order> oldOrdMap)
    {
        JVCO_General_Settings__c GeneralSettings = JVCO_General_Settings__c.getInstance();
        
        List<Order> ordList = new List<Order>();
        for(Order ord : newOrdList)
        {
            if(ord.JVCO_Distribution_Status__c != oldOrdMap.get(ord.id).JVCO_Distribution_Status__c
               && ord.JVCO_Distribution_Status__c == 'Unmatched and Cancelled')
            {
                ordList.add(ord);
            }
        }
        if(!ordList.isEmpty())
        {
            List<JVCO_Invoice_Line_Paid_Amount_Tracker__c> paidAmtTrackerList = new List<JVCO_Invoice_Line_Paid_Amount_Tracker__c>();
            for(OrderItem ordLine : [SELECT id, JVCO_Paid_Date__c,
                                     JVCO_Percentage_Paid__c, 
                                     JVCO_Paid_Amount_Tracker__c,
                                     JVCO_Total_Amount__c,
                                     Order.JVCO_Distribution_Status__c,
                                     Order.JVCO_Payment_Status__c,
                                     Order.JVCO_Status_Change_Date__c,
                                     Order.JVCO_Week__c
                                     FROM OrderItem
                                     WHERE OrderId IN: ordList])
            {
                JVCO_Invoice_Line_Paid_Amount_Tracker__c invPAT = new JVCO_Invoice_Line_Paid_Amount_Tracker__c();
                invPAT.JVCO_Order_Product__c = ordLine.id;
                invPAT.JVCO_Paid_Date__c = ordLine.JVCO_Paid_Date__c;
                invPAT.JVCO_Percentage_Paid__c = ordLine.JVCO_Percentage_Paid__c;
                invPAT.JVCO_Dynamic_Paid_Amount__c = 0;
                invPAT.JVCO_Paid_Amount__c = 0;
                invPAT.JVCO_Total_Amount__c = ordLine.JVCO_Total_Amount__c;
                invPAT.JVCO_Payment_Status__c = ordLine.Order.JVCO_Payment_Status__c;
                invPAT.JVCO_Week__c = ordLine.Order.JVCO_Week__c;
                invPAT.JVCO_Distribution_Status__c = ordLine.Order.JVCO_Distribution_Status__c;
                invPAT.JVCO_Status_Change_Date__c = ordLine.Order.JVCO_Status_Change_Date__c;
                paidAmtTrackerList.add(invPAT);
            }
            insert paidAmtTrackerList;
        }
    }
}