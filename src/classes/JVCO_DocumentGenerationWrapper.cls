public class JVCO_DocumentGenerationWrapper
{
	public static final JVCO_General_Settings__c generalSettings = JVCO_General_Settings__c.getInstance();

	/* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Computes for the Surcharge Generation Date
        Inputs: String, Decimal, Decimal, Date
        Returns: Date
        <Date>      <Authors Name>      <Brief Description of Change> 
        29-Jul-2019 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static Date getSurchargeDate(String dunningLanguage, Decimal totalSurAmount, Decimal totalSurTariffs, Date invoiceDueDate)
    {   
    	Decimal newBusinessSurchargeDays = generalSettings.JVCO_New_Business_Surcharge_Days__c != null ? generalSettings.JVCO_New_Business_Surcharge_Days__c : 1;
    	Decimal renewedSurchargeDays = generalSettings.JVCO_Renewed_Surcharge_Days__c != null ? generalSettings.JVCO_Renewed_Surcharge_Days__c : 29;
        //New Business
        if(totalSurAmount > 0 && totalSurTariffs > 0 && dunningLanguage.contains('New'))
        {
            return invoiceDueDate + (Integer)newBusinessSurchargeDays;
        //Renewed
        }else if(totalSurAmount > 0 && totalSurTariffs > 0 && dunningLanguage.contains('Renewed'))
        {
            return invoiceDueDate + (Integer)renewedSurchargeDays;
        }

        return null;
    }
}