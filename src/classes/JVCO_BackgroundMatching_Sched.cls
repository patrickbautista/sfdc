global class JVCO_BackgroundMatching_Sched implements Schedulable
{
    
	global void execute(SchedulableContext sc)
    {
        //String sched = '0 0 12 1/1 * ? *';

        //String jobId = System.schedule('...Running Batch Job', sched, backgroundMatchingBatch);
        //System.debug('.....Running Batch Job ID : '+jobId);
        
        Database.executeBatch(new JVCO_BackgroundMatching_Batch(),1);
	}
    
}