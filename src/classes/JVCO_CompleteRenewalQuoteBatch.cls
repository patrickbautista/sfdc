/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_CompleteRenewalBatch.cls 
   Description:     Class that handles CreateRenewalQuote Schedulable APEX class as per JIRA ticket GREEN-9202
   Test class:    JVCO_CompleteRenewalQuoteBatchTest.cls 

   Date                 Version       Author                             Summary of Changes 
   -----------          -------     -----------------                  -------------------------------------------
  20-Dec-2016           0.1           Accenture-Rolando Valencia       Intial draft
  21-Dec-2016           0.2           Accenture-Rolando Valencia         Added comments and method signatures.   
  05-May-2017           0.3           Accenture-Chuck Martin           Change query object to Quote and logic 
  12-May-2017           0.4           Accenture-Raus Ablaza            Added constructors for adhoc testing 
  19-May-2017           0.4           Accenture-kristoffer.d.martin    Added condition Transition Renewal Scenario  
  26-Jul-2017           0.5           Accenture-Mel Andrei Santos      Added call of next batch in the finish method as per Jira Ticket GREEN-17932
  27-JUl-2017           0.6           Accenture-Mel Andrei Santos      Changed DML statement to database.update as per Ticket GREEN-17932
  04-Aug-2017           0.7           Accenture-Mel Andrei Santos      update in error logs for the database.insert/update
  22-Aug-2017           0.8           Accenture-Mel Andrei Santos      Added try catch method for error handling in inserting error log records
  24-Aug-2017           0.9           mel.andrei.b.santos              refactored inputting of fields
  26-Aug-2017           1.0           Accenture-Gypt Minierva          Update query selecting Renewal Quotes for contracting
  12-Sep-2017           1.1           Accenture-Eu Rey Cadag            Update JVCO_CompleteRenewalQuoteBatch to use SBQQ.ContractManipulationAPI.ContractGenerator and remove SBQQ__Contracted__c
  14-Sep-2017           1.2           mel.andrei.b.Santos              removed the if conditions as the null check in the error logs as per James
  13-Oct-2017           1.3           Accenture- carmela.santos         Updated to use 1 DML instead of 2 DML when updating quote and opportunity records.
  13-Nov-2017           1.4           Accenture-Rhys Dela Cruz         Changed Status value from Complete and Invoiced to Accepted. GREEN-23355 and added isRunningTest for Test where DML would fail.
  21-Nov-2017           1.5           Accenture- Mel.andrei.b.Santos    Updated query condition to exclude quotes with 0 net amount
  20-Dec-2017           1.6           Accenture - mel.andrei.b.Santos   set JVCO_QuoteComplete__c to True if Quote is contracted successfully as per GREEN-27602
  30-Jan-2017           1.7           Accenture - mel.andrei.b.Santos   added JVCO_OpportunityCancelled__c as a condition for query as per GREEN-29724
  12-Apr-2018           1.8           Accenture - mel.andrei.b.Santos    GREEN-31233 - added code to call setPrimaryQuote from JVCO_OpportunityTriggerHandler
  11-Jun-2018           1.9           Accenture - mel.andrei.b.santos     added condition SBQQ__Account__r.JVCO_Transition_Status__c = 'PRS Renewal Completed' based on GREEN-31705
  18-Jun-2018           2.0           mariel.m.buena                     added condition based on GREEN-32444
------------------------------------------------------------------------------------------------ */
public class JVCO_CompleteRenewalQuoteBatch implements Database.Batchable<sObject>
{
    public Id qId;
    public Set<Id> qIdSet = new Set<Id>();

    //  START   raus.k.b.ablaza     04-May-2017     constructors for adhoc testing
    public JVCO_CompleteRenewalQuoteBatch(){}
    
    // adhoc testing
    public JVCO_CompleteRenewalQuoteBatch(String cId){
        qId = cId;
    }

    // adhoc testing for multiple ids
    public JVCO_CompleteRenewalQuoteBatch(Set<Id> idSet){
        qIdSet = idSet;
    }
    //  END   raus.k.b.ablaza     04-May-2017     
    
    public Database.QueryLocator start (Database.BatchableContext BC){
        //string renewalDateNum;
        string query;
        
        Integer renewalDateNum = Integer.valueOf(JVCO_Renewal_Settings__c.getValues('Automatic Quote Completion').days__c);
        
        /*for(JVCO_Renewal_Settings__c ren : JVCO_Renewal_Settings__c.getall().values())
        {
            if(ren.Name == 'Automatic Quote Completion')
            {
                renewalDateNum = String.valueOf(ren.days__c);
            }
        }*/
        
        query = 'SELECT ID, SBQQ__Type__c, JVCO_Quote_Auto_Renewed__c, SBQQ__Status__c, SBQQ__Opportunity2__c, SBQQ__Opportunity2__r.SBQQ__Contracted__c, SBQQ__NetAmount__c, Covid_19_Impacted1__c, JVCO_Days_in_Next_Contract__c, JVCO_Lockdown_Total_Days__c ' +
                'FROM SBQQ__Quote__c ' +
                'WHERE ' +
                'SBQQ__Type__c = \'Renewal\' ' +
                'AND JVCO_Quote_Auto_Renewed__c = true ' +
                'AND SBQQ__Status__c = \'Draft\' ' +
                'AND (JVCO_Transition_Renewal_Scenario__c = \'\' OR JVCO_Transition_Renewal_Scenario__c = \'PPL First: Greater Than 60 Days\' OR JVCO_Transition_Renewal_Scenario__c = \'Single Society - PPL\' OR JVCO_Transition_Renewal_Scenario__c = \'Single Society - PRS\' OR SBQQ__Account__r.JVCO_Transition_Status__c = \'PRS Renewal Completed\') ' +
                'AND SBQQ__LineItemCount__c > 0 ' +
                'AND JVCO_Recalculated__c = TRUE ' +
                'AND SBQQ__Primary__c = TRUE ' //18-06-17 mariel.m.buena added condition based on GREEN-32444
                + ' AND NoOfQuoteLinesWithoutSPV__c = 0 ' //18-07-17 mariel.m.buena added condition based on GREEN-32694
                + ' AND SBQQ__Opportunity2__r.SBQQ__Contracted__c = false'  //18-07-17 mariel.m.buena added condition based on GREEN-32694
                + ' AND (SBQQ__NetAmount__c != 0 AND SBQQ__NetAmount__c != null) '
                + ' AND SBQQ__Opportunity2__r.JVCO_OpportunityCancelled__c = False '; //21-11-17 mel.andrei.b.santos Updated query condition to exclude quotes with 0 net amount
                //30-01-2018 mel.andrei.b.santos added condition based on GREEN-29724
                //11-06-2018 mel.andrei.b.santos added condition SBQQ__Account__r.JVCO_Transition_Status__c = 'PRS Renewal Completed' based on GREEN-31705
        
        /* commented for GREEN-22241
        query =  'SELECT ID, SBQQ__Type__c, JVCO_Quote_Auto_Renewed__c, SBQQ__Status__c, SBQQ__Opportunity2__c ';
        query += 'FROM SBQQ__Quote__c ';
        query += 'WHERE (SBQQ__Type__c = \'Renewal\') '; 
        query += 'AND (JVCO_Quote_Auto_Renewed__c = false) ';
        query += 'AND (SBQQ__Status__c = \'Draft\') ';
        query += 'AND (id in (select sbqq__quote__c from sbqq__quoteline__c)) ';
        // 19.05.2017 - GREEN-16515 - kristoffer.d.martin - START
        query += 'AND (JVCO_Transition_Renewal_Scenario__c = \'\' OR JVCO_Transition_Renewal_Scenario__c = \'PPL First: Greater Than 60 Days\' OR JVCO_Transition_Renewal_Scenario__c = \'Single Society - PPL\' OR JVCO_Transition_Renewal_Scenario__c = \'Single Society - PRS\') ';
        // 19.05.2017 - GREEN-16515 - kristoffer.d.martin - END
        */
    
        //  START   raus.k.b.ablaza     04-May-2017     additional where statement for adhoc testing
        // single id adhoc
        if(!String.isBlank(String.valueOf(qId))){
            query += ' and id = \''+qId+'\' ';
        }

        // multiple ids adhoc
        if(!qIdSet.isEmpty()){
            String strIds = '';

            for(Id i : qIdSet){
                strIds += '\'' + i + '\',';
            }

            strIds = strIds.substring(0, strIds.length() - 1);

            query += ' and id in ('+strIds+') ';
        }
        //  END   raus.k.b.ablaza     04-May-2017 

        return Database.getQueryLocator(query);
        
    }
    
    public void execute(Database.BatchableContext BC, List<SBQQ__Quote__c> quoteList)
    {
        
        
            List<sObject> lstObjectRec = new List<sObject>();
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>(); // mel.andrei.b.santos 27-07-2017 GREEN-17932
        Set<Id> oppIdSet = new Set<Id>();
        Savepoint sp = Database.setSavepoint();

        for(SBQQ__Quote__c quote : quoteList)
        {
            if(quote.SBQQ__Opportunity2__c != Null)
            {
              if(!quote.SBQQ__Opportunity2__r.SBQQ__Contracted__c)
              {
                oppIdSet.add(quote.SBQQ__Opportunity2__c);
                Opportunity OppRec = quote.SBQQ__Opportunity2__r;
                OppRec.StageName = 'Closed Won';

                //start 28-12-2017 reymark.j.l.arlos GREEN-27464
                OppRec.Amount = quote.SBQQ__NetAmount__c;
                //end

                lstObjectRec.add((sObject) OppRec);

                quote.SBQQ__Status__c = 'Accepted';     //GREEN-23355
                quote.JVCO_Contact_Type__c = 'No Customer Contact';         //GREEN-19227
                quote.JVCO_Quote_Auto_Renewed__c = true;
                quote.JVCO_QuoteComplete__c = true;// 20-12-2017 Accenture - mel.andrei.b.Santos  set JVCO_QuoteComplete__c to True if Quote is contracted successfully as per GREEN-27602
                //quoteListForUpdate.add(QuoteRec);

                //Auto Credits - Populate Covid-19 Picklist
                 if(quote.JVCO_Days_in_Next_Contract__c != null && quote.JVCO_Lockdown_Total_Days__c != null){

                  quote.Covid_19_Impacted1__c = 'Shortened Renewal';
                }

                lstObjectRec.add((sObject) quote );
              }
            }
        }
        
        if(lstObjectRec.size() > 0)
        {
            if(!lstObjectRec.isEmpty())
            {
               // update oppListForUpdate; start Changed DML statement to database.update as per Ticket GREEN-17932 - Mel Andrei Santos 27-07-2017
               if(Test.isRunningTest()){                  //Added isRunningTest to create DML error - Rhys Dela Cruz
                     lstObjectRec.add(new Opportunity());
                }
              List<Database.SaveResult> res = Database.update(lstObjectRec,false); // Start 24-08-2017 mel.andrei.b.santos
                for(Integer i = 0; i < lstObjectRec.size(); i++)
                {
                    Database.SaveResult srOppList = res[i];
                    sObject origrecord = lstObjectRec[i];
                  if(!srOppList.isSuccess())
                    {
                        Database.rollback(sp);
                        System.debug('Update sObject fail: ');
                        for(Database.Error objErr:srOppList.getErrors())
                        {
                          ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();

                          //errLog.Name = String.valueOf(origrecord.ID);
                          errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                          errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(origrecord.ID);
                          errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                          errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());

                          if(origrecord.getSObjectType() == Opportunity.sObjectType)
                            errLog.ffps_custRem__Message__c = 'JVCO_CompleteRenewalQuoteBatch: Update Opportunity Record';
                          else if(origrecord.getSObjectType() == SBQQ__Quote__c.sObjectType)
                            errLog.ffps_custRem__Message__c = 'JVCO_CompleteRenewalQuoteBatch: Update Quote Record';
                          else if(origrecord.getSObjectType() == SBQQ__QuoteLineGroup__c.sObjectType)
                            errLog.ffps_custRem__Message__c = 'JVCO_CompleteRenewalQuoteBatch: Update SBQQ__QuoteLineGroup__c Record';
                          else if(origrecord.getSObjectType() == JVCO_Affiliation__c.sObjectType)
                            errLog.ffps_custRem__Message__c = 'JVCO_CompleteRenewalQuoteBatch: Update JVCO_Affiliation__c Record';


                          errLogList.add(errLog);                         
                       }
                    }
                }
            }
        }

        // Start mel.andrei.b.santos 13-04-2018 GREEN-31233
        
        List<Opportunity> setOpp = new List<Opportunity>();
        setOpp = [Select ID,SBQQ__PrimaryQuote__c from Opportunity where id in :oppIdSet];
        system.debug('@@@ check setOpp ' + setOpp);
        JVCO_OpportunityTriggerHandler.setPrimaryQuote(setOpp);

        // END */

        //Create Contract Using API method provided by SteelBrick
        for(Id oppId: oppIdSet)
        {
          try
          {
            SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractGenerator',oppId , null); 
          }catch(exception e)
          {
            String ErrorLineNumber = e.getLineNumber() != Null ? String.ValueOf(e.getLineNumber()):'None';
            String ErrorMessage = 'Cause: ' + e.getCause() + ' ErrMsg: ' + e.getMessage();
            errLogList.add(JVCO_ErrorUtil.logError(oppId, ErrorMessage , ErrorLineNumber , 'JVCO_CompleteRenewalQuoteBatch:SBQQ.ContractManipulationAPI.ContractGenerator'));

            Database.rollback(sp);
          }
        }

         if(!errLogList.isempty())
         {
             
               // insert errLogList; Start 22-08-2017 mel.andrei.b.santos  Added try catch method for error handling in inserting error log records
             try
             {
                 insert errLogList;
             }
             catch(Exception ex)
             {
                 System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                 
             }
             //end
             
         } 
    }
    
    public void finish(Database.BatchableContext BC)
    {
          //Calling next batch as per Ticket GREEN-17932 - Mel Andrei Santos 26-07-2017
            JVCO_OrderRenewalBatch orderRQBatch = new JVCO_OrderRenewalBatch();
            Id batchProcessId = Database.executeBatch(orderRQBatch,1);
    }
    
}