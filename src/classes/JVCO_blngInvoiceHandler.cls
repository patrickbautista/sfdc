/* ----------------------------------------------------------------------------------------------
   Name: JVCO_blngInvoiceHandler.cls 
   Description: Handler class for blng__Invoice__c trigger

   Date         Version     Author                      Summary of Changes 
   -----------  -------     -----------------           -----------------------------------------
  09-Mar-2017   0.1         ryan.i.r.limlingan          Intial creation
  23-Oct-2017   0.2         r.p.valencia.iii            added populateEventInvoicePaid method  GREEN-23239
  13-Dec-2017   0.3         ashok.kumar.ramanna         GREEN-27088- Week and Dimension 1 were empty on the True Up Report for Sales Credit Note.
  19-Dec-2017   0.4         chun.yin.tang               Add disabling trigger to Blng Invoice Handler
  18-Jan-2018   0.5         filip.bezak@accenture.com   GREEN-28476 - TC2P - Invoice file PPL showing Incorrect period start date and end date  
  ----------------------------------------------------------------------------------------------- */
public class JVCO_blngInvoiceHandler
{
    //public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    //public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_blngInvoiceTrigger__c : false;

    /* ------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: This method handles beforeInsert trigger events for blng__Invoice__c
        Input: List<blng__Invoice__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        20-Mar-2017 ryan.i.r.limlingan  Initial version of function   
    ----------------------------------------------------------------------------------------------- */
    /*public static void beforeInsert(List<blng__Invoice__c> bInvList)
    {
        if(!skipTrigger)
        {
            checkQuote(bInvList);
        }
    }
    
    /* ------------------------------------------------------------------------------------------
        Author: ashok.kumar.ramanna
        Company: Accenture
        Description: This method handles afterUpdate trigger events for blng__Invoice__c
        Input: List<blng__Invoice__c> Map<Id,blng__Invoice__c> oldBInvMap
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Dec-2017 ashok.kumar.ramanna  Initial version of function
        18-May-2018 franz.g.a.dimaapi    Added updateMigratedCheckbBox
    ----------------------------------------------------------------------------------------------- */
    /*public static void beforeUpdate(List<blng__Invoice__c> bInvList,Map<Id,blng__Invoice__c> oldBInvMap)
    {
        if(!skipTrigger) 
        {
            updateCurrentWeekEndDate(bInvList,oldBInvMap);
            for(blng__Invoice__c bInv : bInvList)
            {
                if(bInv.blng__DueDate__c == null && oldBInvMap.get(bInv.Id).blng__DueDate__c != null)
                {
                    bInv.blng__DueDate__c = oldBInvMap.get(bInv.Id).blng__DueDate__c;
                }
            }       
        }
    }

    /* ------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: This method handles afterUpdate trigger events for blng__Invoice__c
    Input: List<blng__Invoice__c>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    09-Mar-2017 ryan.i.r.limlingan  Initial version of function
    18-Jul-2017 franz.g.a.dimaapi   Added updateOrderBlngForScheduler
  ----------------------------------------------------------------------------------------------- */
    /*public static void afterUpdate(Map<Id, blng__Invoice__c> newBInvMap,
                                   Map<Id, blng__Invoice__c> oldBInvMap)
    {
        if(!skipTrigger)
        {
            if(newBInvMap.values() != oldBInvMap.values())
            {
                JVCO_BillingUtil.setupBlngInvoice(newBInvMap.values());
                //filip.bezak@accenture.com   GREEN-28476 - TC2P - Invoice file PPL showing Incorrect period start date and end date
                JVCO_InvoiceGenerationLogic.updateSalesInvoiceStartEndDates(newBInvMap, oldBInvMap);
                createPaidAmtTrackerWhenUnmatched(newBInvMap.values(), oldBInvMap);
            }
            populateEventInvoicePaid(newBInvMap.values(), oldBInvMap);
        }
    }

    /* ------------------------------------------------------------------------------------------
        Author: mariel.m.buena
        Company: Accenture
        Description: This method copies the account JVCO_Review_Type__c to billing JVCO_Review_Type__c
        Input: List<blng__Invoice__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        23-May-2017  mariel.m.buena      Initial version of function
        15-Oct-2019  franz.g.a.dimaapi   Combined Checkquote and Review Type method
    ----------------------------------------------------------------------------------------------- */
    /*private static void checkQuote(List<blng__Invoice__c> bInvList)
    {
        Set<Id> orderIdSet = new Set<Id>();
        Set<Id> accIdSet = new Set<Id>();
        for(blng__Invoice__c bInv : bInvList)
        {
            if(bInv.blng__Order__c != null && bInv.JVCO_Invoice_Type__c != 'Surcharge')
            {
                orderIdSet.add(bInv.blng__Order__c);
            }
            if(bInv.blng__Account__c != null)
            {
                accIdSet.add(bInv.blng__Account__c);
            }
        }

        Map<Id, Order> orderMap = new Map<Id, Order>();
        if(!orderIdSet.isEmpty())
        {
            orderMap = new Map<Id, Order>([SELECT Id,
                                            Account.JVCO_Customer_Account__r.TCs_Accepted__c,
                                            SBQQ__Quote__r.SBQQ__Type__c,
                                            SBQQ__Quote__r.SBQQ__MasterContract__c,
                                            SBQQ__Quote__r.Accept_TCs__c,
                                            SBQQ__Quote__r.SBQQ__NetAmount__c,
                                            SBQQ__Quote__r.JVCO_Payment_Terms__c,
                                            SBQQ__Quote__r.Start_Date__c,
                                            SBQQ__Quote__r.End_Date__c
                                            FROM Order
                                            WHERE Id IN :orderIdSet]);
        }

        Map<Id, Account> accMap = new Map<Id, Account>();
        if(!accIdSet.isEmpty())
        {
            accMap = new Map<Id, Account>([SELECT Id, JVCO_Review_Type__c FROM Account WHERE Id IN: accIdSet]);
        }

        for(blng__Invoice__c bInv : bInvList)
        {
            if(orderMap.containsKey(bInv.blng__Order__c))
            {
                Order o = orderMap.get(bInv.blng__Order__c);
                // Set Licence Dates
                bInv.JVCO_Licence_Start_Date__c = o.SBQQ__Quote__r.Start_Date__c;
                bInv.JVCO_Licence_End_Date__c = o.SBQQ__Quote__r.End_Date__c;
                // Set Due Date according to Payment Terms
                Integer paymentTerm = Integer.valueOf(o.SBQQ__Quote__r.JVCO_Payment_Terms__c);
                bInv.blng__DueDate__c = (bInv.blng__InvoiceDate__c).addDays(paymentTerm);
                // Set Invoice Type according to field values; default value is Standard
                if(o.SBQQ__Quote__r.SBQQ__Type__c.equals('Amendment') && o.SBQQ__Quote__r.SBQQ__MasterContract__c != null && o.SBQQ__Quote__r.SBQQ__NetAmount__c > 0)
                {
                    bInv.JVCO_Invoice_Type__c = 'Supplementary';
                }else if(o.Account.JVCO_Customer_Account__r.TCs_Accepted__c == 'No')
                {
                    bInv.JVCO_Invoice_Type__c = 'Pre-Contract';
                }else if(o.SBQQ__Quote__r.Accept_TCs__c == 'Yes' || o.SBQQ__Quote__r.Accept_TCs__c == null)
                {
                    bInv.JVCO_Invoice_Type__c = 'Standard';
                }
            }
            if(accMap.containsKey(bInv.blng__Account__c))
            {
                bInv.JVCO_Review_Type__c = accMap.get(bInv.blng__Account__c).JVCO_Review_Type__c;
            }
        }
    }

    /* ----------------------------------------------------------------------------------------------------------
        Author:         r.p.valencia.iii
        Company:        Accenture
        Description:    Sets the billing invoice to true if Billing Invoice's Distribution status = Fully Paid. GREEN-23239
        Inputs:         List of invoice line
        Returns:        n/a
        <Date>          <Authors Name>                      <Brief Description of Change> 
        23-Oct-2017     r.p.valencia.iii                    Initial version of the code
        20-Mar-2018     franz.g.a.dimaapi                   Refactor Code and fix duplicate event issue - GREEN-31021
    ----------------------------------------------------------------------------------------------------------*/
    /*public static void populateEventInvoicePaid (List<blng__Invoice__c> billingInvoiceList, Map<Id,blng__Invoice__c> oldBInvMap)
    {   
        Set<Id> bInvIdSet = new Set<Id>();
        for(blng__Invoice__c inv: billingInvoiceList)
        {
            if(inv.JVCO_Distribution_Status__c == 'Fully Paid')
            {
                bInvIdSet.add(inv.id);
            }
        }

        if(!bInvIdSet.IsEmpty())
        {
            Set<JVCO_Event__c> eventSet = new Set<JVCO_Event__c>();
            for(blng__InvoiceLine__c invLine : [SELECT Id, blng__OrderProduct__r.SBQQ__QuoteLine__r.JVCO_Event__c 
                                                FROM blng__InvoiceLine__c
                                                WHERE blng__Invoice__c IN :bInvIdSet 
                                                AND blng__OrderProduct__r.SBQQ__QuoteLine__r.JVCO_Event__c != NULL
                                                AND blng__OrderProduct__r.SBQQ__QuoteLine__r.JVCO_Event__r.JVCO_Invoice_Paid__c = FALSE])
            {
                JVCO_Event__c event = new JVCO_Event__c();
                event.Id = invLine.blng__OrderProduct__r.SBQQ__QuoteLine__r.JVCO_Event__c;
                event.JVCO_Invoice_Paid__c = TRUE;
                eventSet.add(event);
            }
            update new List<JVCO_Event__c>(eventSet);
        }  
    }
    
    /* ------------------------------------------------------------------------------------------
        Author: ashok.kumar.ramanna
        Company: Accenture
        Description: Calculate current weeks sunday date 
        Input: none
        Returns: Date
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Dec-2017 ashok.kumar.ramanna  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*private static Date getCurrentWeekSundayDate(List<blng__Invoice__c> bInvList)
    {
        DateTime currentDate = System.now();
        Boolean shouldGetCurrentWeekendDate = false;
        for(blng__Invoice__c bInv : bInvList)
        {
            if(bInv.JVCO_Week__c == null)
            {
                shouldGetCurrentWeekendDate = true;
            }
        }
        if(shouldGetCurrentWeekendDate)
        {
            Integer numberOfDays = Date.daysInMonth(currentDate.year(), currentDate.month());
            Date lastDayOfMonth = Date.newInstance(currentDate.year(), currentDate.month(), numberOfDays); 
            while(!currentDate.format('E').equalsIgnoreCase('Sun'))
            {
                currentDate = Date.newInstance(currentDate.year(), currentDate.month(), currentDate.day()).addDays(1);
                if(currentDate.isSameDay(lastDayOfMonth)) 
                {
                    break;
                }
            }
        }
        
        return date.newInstance(currentDate.year(), currentDate.month(), currentDate.day());        
    }

    /* ------------------------------------------------------------------------------------------
        Author: ashok.kumar.ramanna
        Company: Accenture
        Description: Update Week of invoice 
        Input: List<blng__Invoice__c> Map<Id,blng__Invoice__c> oldBInvMap
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Dec-2017 ashok.kumar.ramanna  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public static void updateCurrentWeekEndDate(List<blng__Invoice__c> bInvList, Map<Id,blng__Invoice__c> oldBInvMap)
    {
        Date currentWeekSundayDate = getCurrentWeekSundayDate(bInvList);
        for(blng__Invoice__c invObj : bInvList)
        {
            if((Trigger.isInsert && invObj.blng__InvoiceStatus__c == 'Posted') || 
                (Trigger.isUpdate  && invObj.blng__InvoiceStatus__c == 'Posted' && 
                    invObj.blng__InvoiceStatus__c != oldBInvMap.get(invObj.id).blng__InvoiceStatus__c))
            {
                invObj.JVCO_Week__c = currentWeekSundayDate;
            }
        }
    }

    /* ------------------------------------------------------------------------------------------
        Author: patrick.t.bautista
        Company: Accenture
        Description: This method creates JVCO_Invoice_Line_Paid_Amount_Tracker__c record when 
                     Distribution status equals Unmatched and Cancelled
        Input: List<blng__Invoice__c>
        Returns: void
        <Date>        <Authors Name>          <Brief Description of Change> 
        19-Mar-2019   patrick.t.bautista      Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public static void createPaidAmtTrackerWhenUnmatched(List<blng__Invoice__c> newBinvList, Map<Id, blng__Invoice__c> oldBInvMap)
    {
        List<blng__Invoice__c> bInvList = new List<blng__Invoice__c>();
        for(blng__Invoice__c bInv : newBinvList)
        {
            if(bInv.JVCO_Distribution_Status__c != oldBInvMap.get(bInv.id).JVCO_Distribution_Status__c
               && bInv.JVCO_Distribution_Status__c == 'Unmatched and Cancelled')
            {
                bInvList.add(bInv);
            }
        }
        if(!bInvList.isEmpty())
        {
            List<JVCO_Invoice_Line_Paid_Amount_Tracker__c> paidAmtTrackerList = new List<JVCO_Invoice_Line_Paid_Amount_Tracker__c>();
            for(blng__InvoiceLine__c bInvLine : [SELECT id, JVCO_Paid_Date__c,
                                                 JVCO_Percentage_Paid__c, 
                                                 JVCO_Paid_Amount_Tracker__c,
                                                 blng__TotalAmount__c,
                                                 blng__Invoice__r.JVCO_Distribution_Status__c,
                                                 blng__Invoice__r.JVCO_Payment_Status__c,
                                                 blng__Invoice__r.JVCO_Status_Change_Date__c,
                                                 blng__Invoice__r.JVCO_Week__c
                                                 FROM blng__InvoiceLine__c
                                                 WHERE blng__Invoice__c IN: bInvList])
            {
                JVCO_Invoice_Line_Paid_Amount_Tracker__c invPAT = new JVCO_Invoice_Line_Paid_Amount_Tracker__c();
                //invPAT.JVCO_Invoice_Line__c = bInvLine.id;
                invPAT.JVCO_Paid_Date__c = bInvLine.JVCO_Paid_Date__c;
                invPAT.JVCO_Percentage_Paid__c = bInvline.JVCO_Percentage_Paid__c;
                invPAT.JVCO_Dynamic_Paid_Amount__c = 0;
                invPAT.JVCO_Paid_Amount__c = 0;
                invPAT.JVCO_Total_Amount__c = bInvLine.blng__TotalAmount__c;
                invPAT.JVCO_Payment_Status__c = bInvLine.blng__Invoice__r.JVCO_Payment_Status__c;
                invPAT.JVCO_Week__c = bInvLine.blng__Invoice__r.JVCO_Week__c;
                invPAT.JVCO_Distribution_Status__c = bInvLine.blng__Invoice__r.JVCO_Distribution_Status__c;
                invPAT.JVCO_Status_Change_Date__c = bInvLine.blng__Invoice__r.JVCO_Status_Change_Date__c;
                paidAmtTrackerList.add(invPAT);
            }
            insert paidAmtTrackerList;
        }
    }*/
}