/* ----------------------------------------------------------------------------------------------
Name: JVCO_PopulateTaxCodeBatch_Test.cls 
Description: Test Class for JVCO_PopulateTaxCodeBatch          
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
09-May-2017  0.1         kristoffer.d.martin   Intial creation
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_PopulateTaxCodeBatch_Test {
    
/* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Sets up the needed records for testing
    Inputs: N/
    Returns: voidA
    <Date>      <Authors Name>       <Brief Description of Change> 
    11-Apr-2017 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    @testSetup static void createTestData() 
    {
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);

        c2g__codaGeneralLedgerAccount__c testGeneralLedger = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedger.JVCO_Active__c = true;
        testGeneralLedger.name = 'Test Geenral Ledger';
        testGeneralLedger.c2g__Type__c = 'Balance Sheet';
        testGeneralLedger.c2g__ReportingCode__c = '10390';
        testGeneralLedger.c2g__GLAGroup__c = 'Cash';
        testGeneralLedger.c2g__TrialBalance1__c= 'Assets';
        testGeneralLedger.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedger.c2g__TrialBalance3__c = 'Cash and Cash Equivalents';
        testGeneralLedger.c2g__UnitOfWork__c = 182.0;
        testGeneralLedger.c2g__CashFlowCategory__c = 'Operating';
        glaList.add(testGeneralLedger);
        insert glaList;

        c2g__codaTaxCode__c testTaxCode = new c2g__codaTaxCode__c();
        testTaxCode.name = 'GB-O-STD-LIC';
        testTaxCode.c2g__GeneralLedgerAccount__c = testGeneralLedger.id;
        testTaxCode.c2g__TaxGroup__c = 'Tax Group 1';
        testTaxCode.c2g__IsParent__c = false;
        testTaxCode.c2g__UnitOfWork__c = 1.0;
        testTaxCode.ffvat__NetBox__c = 'Box 6';
        testTaxCode.ffvat__TaxBox__c = 'Box 1';
        testTaxCode.JVCO_Rate__c = 20.0;
        insert testTaxCode;
        
        c2g__codaTaxCode__c testTaxCodeConv = new c2g__codaTaxCode__c();
        testTaxCodeConv.name = 'Vat Conversion';
        testTaxCodeConv.c2g__GeneralLedgerAccount__c = convGLA.id;
        testTaxCodeConv.c2g__TaxGroup__c = 'Tax Group 1';
        testTaxCodeConv.c2g__IsParent__c = false;
        testTaxCodeConv.c2g__UnitOfWork__c = 1.0;
        testTaxCodeConv.ffvat__NetBox__c = 'Box 6';
        testTaxCodeConv.ffvat__TaxBox__c = 'Box 1';
        testTaxCodeConv.JVCO_Rate__c = 20.0;
        insert testTaxCodeConv;
        
        c2g__codaTaxRate__c testTaxRate = new c2g__codaTaxRate__c();
        testTaxRate.c2g__Rate__c = 20.0;
        testTaxRate.c2g__UnitOfWork__c = 1.0;
        testTaxRate.c2g__TaxCode__c = testTaxCodeConv.id;
        insert testTaxRate;
            
        List<JVCO_BackgroundMatching__c> bgMatchingList = new List<JVCO_BackgroundMatching__c>();
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Trading Currency', JVCO_Default_Value__c = 'GBP'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Receivable Control', JVCO_Default_Value__c = '10040'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Output Vat Code', JVCO_Default_Value__c = 'GB-O-STD-LIC'));
        
        insert bgMatchingList;
        
        List<c2g__codaDimension3__c> dimensions = new List<c2g__codaDimension3__c>();
        dimensions.add(new c2g__codaDimension3__c(Name = 'Key', c2g__ReportingCode__c = 'Key'));
        dimensions.add(new c2g__codaDimension3__c(Name = 'Non Key', c2g__ReportingCode__c = 'Non Key'));
        insert dimensions;
        
        c2g__codaDimension1__c dim1 = new c2g__codaDimension1__c(Name = 'Live', c2g__ReportingCode__c = 'BC002', JVCO_Function__c = 'Budget');
        insert dim1;
        
        c2g__codaDimension1__c dim2 = new c2g__codaDimension1__c(Name = 'General Purpose', c2g__ReportingCode__c = 'BC004', JVCO_Function__c = 'Budget');
        insert dim2;
        
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        insert dt;
        
        Account customerAcc = new Account();
        customerAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        customerAcc.TCs_Accepted__c = 'No';
        customerAcc.Type = 'Agency';
        customerAcc.c2g__CODADimension1__c = null;
        insert customerAcc;
        
        Account licAcc = new Account();
        licAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        licAcc.Name = '9012345';
        licAcc.Type = 'Customer';
        licAcc.JVCO_Customer_Account__c = customerAcc.Id;
        licAcc.OwnerId = UserInfo.getUserId();
        licAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        licAcc.c2g__CODAOutputVATCode__c = testTaxCodeConv.Id;
        system.debug(licAcc.c2g__CODAOutputVATCode__r.Name);
        licAcc.c2g__CODADimension1__c = dim2.Id;
        insert licAcc;
        system.debug(licAcc.c2g__CODAOutputVATCode__r.Name);
    }

    /* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Call the JVCO_LinkTempAccountReference Batch
    Inputs: N/
    Returns: voidA
    <Date>      <Authors Name>       <Brief Description of Change> 
    11-Apr-2017 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    @isTest static void JVCO_LinkTempAccountReferenceBatch_test()
    {   

         Id licAccountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
   
        Group testGroup = [SELECT Id, Name FROM Group order by CreatedDate DESC limit 1];
        Account testAcc = [SELECT Id, Name FROM Account WHERE RecordTypeId = :licAccountRecTypeId order by CreatedDate DESC limit 1];

        Test.startTest();

        JVCO_PopulateTaxCodeBatch populateTaxCode = new JVCO_PopulateTaxCodeBatch();
        Database.executeBatch(populateTaxCode);

        Test.stopTest();

        Account acc = [SELECT Id, Name, c2g__CODAOutputVATCode__c, c2g__CODAOutputVATCode__r.Name
                       FROM Account
                       WHERE RecordTypeId = :licAccountRecTypeId order by CreatedDate DESC limit 1];
        System.assertEquals(acc.c2g__CODAOutputVATCode__r.Name, 'GB-O-STD-LIC');
    }
    
}