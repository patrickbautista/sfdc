/* ----------------------------------------------------------------------------------------------
    Name: JVCO_PostCashEntriesQueueable.cls 
    Description: Queueable Logic for Posting Cash Entries

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    15-Oct-2018   0.1        franz.g.a.dimaapi   GREEN-33408 - Intial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_PostCashEntriesQueueable implements Queueable
{
    @testVisible
    private static Boolean doChainJob = true;
    private Map<Integer, List<c2g__codaCashEntry__c>> batchNumberToCashEntryListMap;
    private Integer batchNumber;
    
    public JVCO_PostCashEntriesQueueable(Map<Integer, List<c2g__codaCashEntry__c>> batchNumberToCashEntryListMap, Integer batchNumber)
    {
        this.batchNumberToCashEntryListMap = batchNumberToCashEntryListMap;
        this.batchNumber = batchNumber;
    }

    public void execute(QueueableContext context) 
    {
        //Post Cash Entry by Batch Number
        postCashEntry(batchNumberToCashEntryListMap.get(batchNumber++));
        //Check if there's an existing batch
        if(batchNumberToCashEntryListMap.containsKey(batchNumber))
        {
            //Call this Post Queueable again
            System.enqueueJob(new JVCO_PostCashEntriesQueueable(batchNumberToCashEntryListMap, batchNumber));
        //Prevent Test Class Issue in chaining Queueables
        }else if(doChainJob)
        {
            System.enqueueJob(new JVCO_BackgroundMatching_Queueable(getTransactionLineMap(batchNumberToCashEntryListMap.values())));
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Post Cash Entry using FF API
        Inputs: List<Account>
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        15-Oct-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void postCashEntry(List<c2g__codaCashEntry__c> cashEntryList)
    {
        c2g.CODAAPICommon_7_0.Context contextObj = new c2g.CODAAPICommon_7_0.Context();
        List<c2g.CODAAPICommon.Reference> referenceList = new List<c2g.CODAAPICommon.Reference>();
        for(c2g__codaCashEntry__c cashEntry : cashEntryList)
        {
            c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
            ref.Id = cashEntry.Id;
            referenceList.add(ref);
        }

        try
        {
            //Bulk Post Cash Entry using FF API
            JVCO_BackgroundMatchingLogic.stopMatching = true;
            c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(contextObj, referenceList);
        //Create an Error Logs if an Exception happens
        }catch(Exception e)
        {   
            JVCO_ErrorUtil errorUtil = new JVCO_ErrorUtil();
            errorUtil.logErrorFromCashEntry(cashEntryList, 'Posting of Cash Entry', 'PC-001', e.getMessage());
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Create a Transaction Line Item Map for a Posted cash entry
        Inputs: List<Account>
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        15-Oct-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    @testVisible
    private Map<Id, c2g__codaTransactionLineItem__c> getTransactionLineMap(List<List<c2g__codaCashEntry__c>> listOfCashEntryList)
    {
        Map<Id, c2g__codaCashEntry__c> cashEntryMap = new Map<Id, c2g__codaCashEntry__c>();
        for(List<c2g__codaCashEntry__c> cashEntryList : listOfCashEntryList)
        {
            cashEntryMap.putAll(cashEntryList);
        }

        return new Map<Id, c2g__codaTransactionLineItem__c>([SELECT Id,
                                                             c2g__Account__c,
                                                             c2g__Account__r.JVCO_Customer_Account__c
                                                             FROM c2g__codaTransactionLineItem__c
                                                             WHERE c2g__Transaction__r.c2g__CashEntry__c IN :cashEntryMap.values()
                                                             AND c2g__LineType__c IN ('Account')
                                                             AND c2g__MatchingStatus__c IN ('Available')]);
    }
}