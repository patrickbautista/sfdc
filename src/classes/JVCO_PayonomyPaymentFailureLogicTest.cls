@isTest
private class JVCO_PayonomyPaymentFailureLogicTest
{
    /*@testSetup static void createTestData() 
    {
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        SBQQ.TriggerControl.disable();
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        Test.startTest();
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        List<Order> orderList = new List<Order>();
        orderList.add(o);
        

        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        insert bInv;      
        
        Id paybankId = JVCO_TestClassHelper.getPayBank();
        Id ddMandateId = JVCO_TestClassHelper.getDDMandate(bInv.blng__Account__c);
        Id paySchedId = JVCO_TestClassHelper.getPaySched(payBankId);

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_TestClassHelper.setGeneralSettingsCS();
        insert new GLA_for_Cash_Transfer__c(name = '10100 - Bank Cash'); 
        //JVCO_BackgroundMatchingLogic.stopMatching = true;
        JVCO_FFUtil.stopCashTransferLogic = true;
        
        blng__InvoiceLine__c bInvLine = JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);
        bInvLine.JVCO_Surcharge_Applicable__c = true;
        insert bInvLine;
        Database.executeBatch(new JVCO_OrderMultipleBillNowBatch(orderList));
        Test.stopTest();
        List<c2g__codaInvoice__c> selectedInvoices = [SELECT Id, Name,JVCO_Payment_Method__c, 
                                                      JVCO_Outstanding_Amount_to_Process__c, 
                                                      c2g__OutstandingValue__c 
                                                      FROM c2g__codaInvoice__c];
        
        JVCO_PaymentSharedLogic paymentLogic = new JVCO_PaymentSharedLogic(selectedInvoices, JVCO_PaymentSharedLogic.PaymentMode.Debit);
        JVCO_Invoice_Group__c invGroup = paymentLogic.createInvoiceGroup('Direct Debit', TRUE);
        
        PAYREC2__Payment_Agreement__c agreement = new PAYREC2__Payment_Agreement__c();
        agreement.PAYREC2__Account__c = bInv.blng__Account__c;
        agreement.PAYREC2__Ongoing_Collection_Amount__c = invGroup.JVCO_Total_Amount__c/4;
        agreement.PAYREC2__Description__c = invGroup.Name;
        agreement.PAYFISH3__Current_Bank_Account__c = payBankId;
        agreement.PAYFISH3__Payee_Agreed__c = true;
        agreement.PAYREC2__Payment_Schedule__c = paySchedId;
        agreement.PAYREC2__Status__c = 'New Instruction';
        agreement.RecordTypeId = Schema.SObjectType.PAYREC2__Payment_Agreement__c.getRecordTypeInfosByName().get('Direct Debit').getRecordTypeId();
        insert agreement;
        
        PAYBASE2__Payment__c payment = JVCO_TestClassHelper.getPayonomyPayment('DirectDebit');
        payment.PAYREC2__Payment_Agreement__c = agreement.Id;
        payment.PAYCP2__Payment_Description__c = invGroup.Name;
        payment.PAYBASE2__Status__c = 'Submitted';
        payment.PAYBASE2__Pay_Date__c=date.today().addMOnths(1);
        insert payment;
    }

   @isTest
    static void testNonDeliberate()
    {     
            
        Test.startTest();       
        List<PAYBASE2__Payment__c> paymentList = [SELECT id, PAYBASE2__Status__c, PAYBASE2__Status_Text__c  FROM PAYBASE2__Payment__c];      
        JVCO_PayonomyPaymentProcessingBatch paymentBatch = new JVCO_PayonomyPaymentProcessingBatch('Direct Debit Collection');
        Database.QueryLocator qlocator = paymentBatch.start(null);
        paymentBatch.execute(null, paymentList);
        paymentBatch.finish(null);
       
        
        JVCO_Payonomy_Error_Codes__c testPayErrorCode = new JVCO_Payonomy_Error_Codes__c();
        testPayErrorCode.name = 'Refer to Payer';
        testPayErrorCode.JVCO_Deliberate__c = 'Non-Deliberate';
        testPayErrorCode.JVCO_Description__c = 'Error object in testClass';
        insert testPayErrorCode;         
        
        //UPDATING PAYMENT
        paymentList[0].PAYBASE2__Status__c = 'Error';
        paymentList[0].PAYBASE2__Status_Text__c = 'Refer to Payer';       
        update paymentList[0]; 
        
        //calling individual methods
        JVCO_PayonomyPaymentFailureLogic.setInvoiceMap();
        JVCO_PayonomyPaymentFailureLogic.insertRefundCash();             
        Test.stopTest();        
    }
    
    @isTest
    static void testDeliberate()
    {      
        
        Test.startTest();       
        List<PAYBASE2__Payment__c> paymentList = [SELECT id, PAYBASE2__Status__c, PAYBASE2__Status_Text__c  FROM PAYBASE2__Payment__c];      
        JVCO_PayonomyPaymentProcessingBatch paymentBatch = new JVCO_PayonomyPaymentProcessingBatch('Direct Debit Collection');
        Database.QueryLocator qlocator = paymentBatch.start(null);
        paymentBatch.execute(null, paymentList);
        paymentBatch.finish(null); 
        
        JVCO_Payonomy_Error_Codes__c testPayErrorCode = new JVCO_Payonomy_Error_Codes__c();
        testPayErrorCode.name = 'Amount differs';
        testPayErrorCode.JVCO_Deliberate__c = 'Deliberate';
        testPayErrorCode.JVCO_Description__c = 'Error object in testClass';
        insert testPayErrorCode;         
        
        //UPDATING PAYMENT
        paymentList[0].PAYBASE2__Status__c = 'Error';
        paymentList[0].PAYBASE2__Status_Text__c = 'Amount differs';       
        update paymentList[0];         
        Test.stopTest();      
    }*/
}