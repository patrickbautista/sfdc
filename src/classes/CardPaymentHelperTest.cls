@isTest
public class CardPaymentHelperTest {
    @isTest public static void GenerateCustomSettings()
    {
        SMP_Payment_Config__c config = new SMP_Payment_Config__c(MOTO_Payment_URL__c = 'https://testpayment.com',
                                                                                        Ecommerce_Payment_URL__c = 'https://testpayment.com',
                                                                                        Authenticate_Internal_URL__c = 'https://testpayment.com',
                                                                                        Authenticate_Ecommerce_URL__c = 'https://testpayment.com');
        insert config;
    }
    
    @isTest static void GenerateMOTOCardPaymentURL_Success() {
        
        GenerateCustomSettings();
        CardPaymentHelper.generateMOTOCardPaymentURL(new CardPaymentHelper.MOTOPayment());
    }
    
    @isTest static void GenerateECommerceCardPaymentURL_Success() {
        
        GenerateCustomSettings();
        CardPaymentHelper.generateECommerceCardPaymentURL(new CardPaymentHelper.EcommercePayment());
    }

    @isTest static void GenerateAuthenticateCardURL_Success() {
        
        GenerateCustomSettings();
        CardPaymentHelper.generateAuthenticateCardURL(new CardPaymentHelper.AuthenticateCard());
    }

    @isTest static void GenerateECommerceAuthenticateCardURL_Success() {
        
        GenerateCustomSettings();
        CardPaymentHelper.generateECommerceAuthenticateCardURL(new CardPaymentHelper.AuthenticateCard());
    }  
}