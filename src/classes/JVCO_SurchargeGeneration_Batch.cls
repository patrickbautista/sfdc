/* --------------------------------------------------------------------------------------------------------------------
    Name:            JVCO_SurchargeGeneration_Batch.cls 
    Description:     Batch Class for Generating Surcharge Invoice
    Test class:      JVCO_SurchargeGeneration_BatchTest.cls 

    Date            Version     Author                             Summary of Changes 
    -----------     -------     -----------------                  -----------------------------------
    07-Mar-2017     0.1         franz.g.a.dimaapi@accenture.com    Intial draft
    24-May-2017     0.2         franz.g.a.dimaapi@accenture.com    Updated Original Sales Invoice Surcharge 
    Generated Checkbox to Trues
    20-July-2017    0.3         L.B.singh                          Updated the Surcharge invoice date to today's date
    25-July-2017    0.4         L.B.singh                          Updated the Query and making the checkbox false based upon withhold date 
                                                                   equals true and promise to pay date is smaller than plus 1 and making the 
                                                                   surcharge date null.
    27-July-2017    0.5         mel.andrei.b.santos@accenture.com  Changed DML statement to database.insert as per Ticket GREEN-17932
    04-Aug-2017     0.6         mel.andrei.b.santos                updated error log blocks for the database.insert/upadte/delete
    24-Aug-2017     0.7         mel.andrei.b.santos                refactored inputting of fields
    01-Sep-2017     0.8         L.B.singh                          Updated batch query for the promise to pay is smaller than today's date +1.
    14-Sep-2017     0.9         mel.andrei.b.Santos                removed the if conditions as the null check in the error logs as per James
    03-Oct-2017     1.0         franz.g.a.dimaapi                  Add single Record Surcharge to test it by QA using single record - GREEN-23645
    9-Oct-2017      1.1         franz.g.a.dimaapi                  Refactor methods, Create getInvoiceLineRecordsByInvId method,
                                                                   Moved QueryLocator Child to a query - GREEN-23856
    13-Oct-2017     1.2         franz.g.a.dimaapi                  Fixed Rollback Issue - GREEN-24811 
    15-Dec-2017     1.3         franz.g.a.dimaapi                  Fixed GREEN-27392    
    22-Dec-2017     1.4         mel.andrei.b.Santos                updated finish method to call JVCO_TransitionManualReviewNotification as per GREEN-27610                                                
    31-Jan-2018     1.5         filip.bezak@accenture.com          GREEN-29743 - Surcharge Invoice created for Past fee line is calculating incorrect Surcharge amount
    02-Feb-2018     1.6         patrick.t.bautista                 GREEN-29816 - 2 surcharge invoices generated from single consolidated sales invoice
    27-Feb-2018     1.7         franz.g.a.dimaapi                  GREEN-30626 - Fixed Null Pointer Exception
    07-Mar-2018     1.8         franz.g.a.dimaapi                  GREEN-30626 - Fixed UnitPrice Logic
    16-Mar-2018     1.9         mary.ann.a.ruelan                  GREEN-30413 - prevent creating invoice lines subtotal<0, quote line terminated
    21-May-2018     1.9         mary.ann.a.ruelan                  GREEN-31814 - Removed setting surcharge date to null.
    20-May-2019     2.0         mel.andrei.b.Santos                GREEN-34614 - created custom settings for enabling/disabling batch
    10-Oct-2019     2.1         franz.g.a.dimaapi                  GREEN-35018 - Removed Promise to Pay Filter, Added 2 New Filters
---------------------------------------------------------------------------------------------------------------------*/
public class JVCO_SurchargeGeneration_Batch implements Database.Batchable<sObject>, Database.Stateful
{   
    private Boolean isSingleRecord = false;
    private Id sInvId;
    public Map<Id, Id> licAndCustAccIdMap = new Map<Id, Id>();
    //public Boolean forceRun = false;
    public Boolean forceRun = (Boolean)JVCO_General_Settings__c.getOrgDefaults().JVCO_Enable_Surcharge_Gen_Batch__c;
    public User surUser = [SELECT Id, Name FROM User WHERE Name = 'Surcharge User' LIMIT 1];
    JVCO_InvoiceAndCreditNoteSurchargeLogic invAndCredSurchargeWrapper = new JVCO_InvoiceAndCreditNoteSurchargeLogic();
    
    public JVCO_SurchargeGeneration_Batch(){
        invAndCredSurchargeWrapper.surUser = surUser;
        invAndCredSurchargeWrapper.setDim2AndProdMapping();
    }
    public JVCO_SurchargeGeneration_Batch(Id sInvId)
    {
        this.sInvId = sInvId;
        isSingleRecord = true;
        invAndCredSurchargeWrapper.surUser = surUser;
        invAndCredSurchargeWrapper.setDim2AndProdMapping();
    }

    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
        if(isSingleRecord)
        {
            return Database.getQueryLocator([SELECT Id
                                            FROM c2g__codaInvoice__c
                                            WHERE Total_Surchargeable_Amount__c > 0
                                            AND JVCO_Exempt_from_Surcharge__c = FALSE
                                            AND JVCO_Surcharge_Generation_Date__c <= :Date.today()
                                            AND JVCO_Stop_Surcharge__c = FALSE
                                            AND ffps_custRem__In_Dispute__c = FALSE
                                            AND JVCO_Cancelled__c = FALSE
                                            AND JVCO_Protected__c = FALSE
                                            AND c2g__PaymentStatus__c != 'Paid' 
                                            AND JVCO_Surcharge_Generated__c = FALSE
                                            AND JVCO_Invoice_Type__c <> 'Surcharge'
                                            AND c2g__Account__r.JVCO_In_Enforcement__c != TRUE
                                            AND c2g__Account__r.JVCO_In_Infringement__c != TRUE
                                            AND c2g__NetTotal__c != 0
                                            AND c2g__Account__r.ffps_accbal__Account_Balance__c != 0
                                            AND id = : sInvId]);
        }else
        {
            return Database.getQueryLocator([SELECT Id
                                            FROM c2g__codaInvoice__c
                                            WHERE Total_Surchargeable_Amount__c > 0
                                            AND JVCO_Exempt_from_Surcharge__c = FALSE
                                            AND JVCO_Surcharge_Generation_Date__c <= :Date.today()
                                            AND JVCO_Stop_Surcharge__c = FALSE
                                            AND ffps_custRem__In_Dispute__c = FALSE
                                            AND JVCO_Cancelled__c = FALSE
                                            AND JVCO_Protected__c = FALSE
                                            AND c2g__PaymentStatus__c != 'Paid' // GREEN-24895 patrick.t.bautista
                                            //GREEN-21540(Surcharge invoice was generated for protected invoice)(15/08/2017) L.B.Singh 
                                            AND JVCO_Surcharge_Generated__c = FALSE
                                            AND JVCO_Invoice_Type__c <> 'Surcharge'
                                            AND c2g__Account__r.JVCO_In_Enforcement__c != TRUE
                                            AND c2g__Account__r.JVCO_In_Infringement__c != TRUE
                                            AND c2g__NetTotal__c != 0
                                            AND c2g__Account__r.ffps_accbal__Account_Balance__c != 0]);
        }
    }
    
    public void execute(Database.BatchableContext bc, List<c2g__codaInvoice__c> scope)
    {
        if(Test.isRunningTest() || forceRun)
        {
            JVCO_FFUtil.stopBlngInvoiceLineHandlerBeforeInsert = true;
            List<Order> orderToSurcharge = [SELECT Id, JVCO_Invoice_Type__c, Pricebook2Id,
                                            EffectiveDate, EndDate, TotalAmount,
                                            AccountId, JVCO_Sales_Invoice__c, 
                                            JVCO_Surcharge_Generated__c,
                                            JVCO_Sales_Invoice__r.JVCO_Promise_to_Pay__c,
                                            Status, JVCO_Sales_Invoice__r.c2g__PaymentStatus__c,
                                            JVCO_Licence_End_Date__c, JVCO_Licence_Start_Date__c,
                                            JVCO_Original_Order__c,
                                            JVCO_Temp_Start_Date__c,
                                            JVCO_Temp_End_Date__c,
                                            JVCO_DueDate__c
                                            FROM Order
                                            WHERE JVCO_Sales_Invoice__c IN :scope
                                            AND JVCO_Surcharge_Tariffs__c > 0
                                            AND JVCO_Total_Surcharge_Amount__c > 0];
            surchargeGenerationlLogic(orderToSurcharge);
        }
    }
    
    private void surchargeGenerationlLogic(List<Order> orderList)
    {   
        JVCO_Invoice_Surcharge__c surchargeSettings = JVCO_Invoice_Surcharge__c.getValues('JVCO');
        //Map by Invoice Id to List of Invoice Line
        Map<Id, List<OrderItem>> ordIdToOrderLineListMap = getInvoiceLineRecordsByInvId(orderList);
        //Map by Original Invoice Id to List of Invoice Line
        Map<Id, List<OrderItem>> OrderAndOrderItemMap = new Map<Id, List<OrderItem>>();
        //List of Updated Surcharge Invoice Line
        List<Order> updatedOriginalOrderList = new List<Order>();
        //List of Sales Invoice Id to be updated
        Set<Id> sInvIdSet = new Set<Id>();
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>(); // GREEN-17932 - Mel Andrei Santos 27-07-2017

        //List of Clone Surcharge Invoice
        List<Order> surchargeOrderList = new List<Order>();
        
        JVCO_Order_Group__c ordGroup = new JVCO_Order_Group__c();
        ordGroup.JVCO_Group_Order_Account__c = orderList[0].AccountId;
        insert ordGroup;
        
        invAndCredSurchargeWrapper.orderGroup = [SELECT Id, JVCO_Group_Order_Account__c,
                                                 JVCO_Group_Order_Account__r.c2g__CODADimension1__c,
                                                 JVCO_Group_Order_Account__r.c2g__CODADimension3__c,
                                                 JVCO_Group_Order_Account__r.c2g__CODAOutputVATCode__c,
                                                 JVCO_Group_Order_Account__r.JVCO_Dunning_Letter_Language__c
                                                 FROM JVCO_Order_Group__c 
                                                 WHERE Id =: ordGroup.Id LIMIT 1];
            
        for(Order order : orderList)
        {
            if(ordIdToOrderLineListMap.containsKey(order.Id))
            {
                //Clone Invoice
                Order surchargeOrder = order.clone(false, true, false, false);
                surchargeOrder.JVCO_Original_Order__c = Order.Id;
                surchargeOrder.JVCO_Order_Group__c = ordGroup.Id;
                surchargeOrder.JVCO_Invoice_Type__c = 'Surcharge'; 
                invAndCredSurchargeWrapper.originalInvoice = surchargeOrder.JVCO_Sales_Invoice__c;
                surchargeOrder.JVCO_Sales_Invoice__c = null;
                surchargeOrder.JVCO_Surcharge_Generated__c = false;
                //surchargeOrder.EndDate = Date.today();
                //Lalit Singh(20/07/2017) GREEN-20485 :Invoice Date on the related surcharge invoice is incorrect
                surchargeOrder.EffectiveDate = Date.today();
                surchargeOrder.Status = 'Draft';
                surchargeOrder.JVCO_Order_Status__c = 'Posted';
                surchargeOrderList.add(surchargeOrder);

                //List of Clone Surcharge Invoice Line
                List<OrderItem> surchargeOrderLineList = new List<OrderItem>();
                for(OrderItem orderItem : ordIdToOrderLineListMap.get(order.Id))
                {
                    //Clones invoice line only Surcharge Applicable = true, not terminated and amount is not negative
                    if(orderItem.JVCO_Surcharge_Applicable__c && !orderItem.SBQQ__QuoteLine__r.Terminate__c && 
                        orderItem.JVCO_Total_Amount__c > 0)
                    {
                        //Clone Invoice Line
                        OrderItem surchargeOrderLine = orderItem.clone(false, true, false, false);
                        surchargeOrderLine.ServiceDate = Date.today();
                        surchargeOrderLine.EndDate = Date.today();
                        surchargeOrderLine.UnitPrice = OrderItem.UnitPrice * surchargeSettings.JVCO_Surcharge_Multipiler__c; 
                        surchargeOrderLineList.add(surchargeOrderLine);
                    }    
                }

                //Map Original Inv to Surcharge Invoice Line List
                OrderAndOrderItemMap.put(order.Id, surchargeOrderLineList);
                order.JVCO_Surcharge_Generated__c = true;
                sInvIdSet.add(order.JVCO_Sales_Invoice__c);
                updatedOriginalOrderList.add(order);
            }    
        }
        
        Boolean hasError = false;
        Savepoint sp = Database.setSavepoint();
        try
        {
            JVCO_FFUtil.stopBillNowFuction = true;
            
            //insert surchargeInvoiceList; Changed DML statement to database.insert as per Ticket GREEN-17932 - Mel Andrei Santos 27-07-2017
            List<Database.SaveResult> res = Database.insert(surchargeOrderList,false); // Start 24-08-2017 mel.andrei.b.santos
            for(Integer i = 0; i < surchargeOrderList.size(); i++)
            {
                Database.SaveResult srOrderList = res[i];
                Order origrecord = surchargeOrderList[i];
                if(!srOrderList.isSuccess())
                {
                    hasError = true;
                    for(Database.Error objErr:srOrderList.getErrors())
                    {
                        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                        errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(origrecord.ID);
                        errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                        errLog.ffps_custRem__Message__c = 'Surcharge Generation Batch';
                        errLog.ffps_custRem__Grouping__c = 'SUR-001';
                        errLogList.add(errLog); // Mary Ruelan 04-08-2017  
                    }
                }
            }

            //Populate Invoice Line field of Surcharge Invoice Lines
            List<OrderItem> updatedSurchargeOrderLineList = new List<OrderItem>();
            Map<Id, List<OrderItem>> orderToOrderItemMap = new Map<Id, List<OrderItem>>();
            
            for(Order surOrder : surchargeOrderList)
            {
                orderToOrderItemMap.put(surOrder.Id, new List<OrderItem>());
                for(OrderItem surOrderLine : OrderAndOrderItemMap.get(surOrder.JVCO_Original_Order__c))
                {
                    orderToOrderItemMap.get(surOrder.Id).add(surOrderLine);
                    surOrderLine.OrderId = surOrder.id;
                    updatedSurchargeOrderLineList.add(surOrderLine);
                }
            }
            
            if(!hasError)
            {
                List<Database.SaveResult> res1 = Database.insert(updatedSurchargeOrderLineList,false);
                for(Integer i = 0; i < updatedSurchargeOrderLineList.size(); i++)
                {
                    Database.SaveResult srList = res1[i];
                    OrderItem origrecord = updatedSurchargeOrderLineList[i];
                    if(!srList.isSuccess())
                    {
                        hasError = true;
                        for(Database.Error objErr:srList.getErrors())
                        {
                            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                            errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                            errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(origrecord.ID);
                            errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                            errLog.ffps_custRem__Message__c = 'Surcharge Generation Batch';
                            errLog.ffps_custRem__Grouping__c = 'SUR-002';
                            errLogList.add(errLog); // Mary Ruelan 04-08-2017  
                        }           
                    }
                }
            }
            
            if(!hasError)
            {
                //update updatedSurchargeInvoiceList; Changed DML statement to database.insert as per Ticket GREEN-17932 - Mel Andrei Santos 27-07-2017
                Database.SaveResult[] res2 = Database.update(updatedOriginalOrderList,false);
                for(Integer i = 0; i < updatedOriginalOrderList.size(); i++)
                {
                    Database.SaveResult srList = res2[i];
                    Order origrecord = updatedOriginalOrderList[i];
                    if(!srList.isSuccess())
                    {
                        hasError = true;
                        System.debug('Update quote fail: ');
                        for(Database.Error objErr:srList.getErrors())
                        {
                            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                            errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                            errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(origrecord.ID);
                            errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                            errLog.ffps_custRem__Message__c = 'Surcharge Generation Batch';
                            errLog.ffps_custRem__Grouping__c = 'SUR-003';
                            errLogList.add(errLog); // Mary Ruelan 04-08-2017                        
                        }   
                    }
                }
            }

            if(!hasError)
            {
                invAndCredSurchargeWrapper.orderToOrderItemMap = orderToOrderItemMap;
                updateSInvListToSurchargeGenerated(sInvIdSet);
            }
        }catch(Exception e)
        {
            hasError = true;
        }finally
        {
            if(!hasError){
                invAndCredSurchargeWrapper.setupInvoiceAndCreditNoteHeader();
            }
            if(hasError)
            {
                Database.rollback(sp);
            }
            if(!errLogList.isEmpty())
            {
                insert errLogList;
            }
        }
    }
    
    
    /* --------------------------------------------------------------------------------------------------
       Author:         franz.g.a.dimaapi@accenture.com
       Company:        Accenture
       Description:    Update Sales Invoice List

       Inputs:         Set<Id>
       Returns:        
       <Date>          <Authors Name>             <Brief Description of Change> 
       09-Oct-2017     Franz Gerrard A. Dimaapi     Refactor Code
       21-May-2018     mary.ann.a.ruelan            removed setting JVCO_Surcharge_Generation_Date__c = null
    ------------------------------------------------------------------------------------------------------*/
    private void updateSInvListToSurchargeGenerated(Set<Id> sInvIdSet)
    {
        List<c2g__codaInvoice__c> updatedSInvList = new List<c2g__codaInvoice__c>();
        for(c2g__codaInvoice__c sInv : [SELECT Id, Name, JVCO_Surcharge_Generated__c, JVCO_Exempt_from_Surcharge__c,
                                        JVCO_Promise_to_Pay__c, JVCO_Surcharge_Generation_Date__c
                                        FROM c2g__codaInvoice__c
                                        WHERE Id IN: sInvIdSet])
        {
            //21/07/2017 GREEN-20331 Surcharge invoice after Promise to pay day Lalit Singh
            if(sInv.JVCO_Exempt_from_Surcharge__c = true && sInv.JVCO_Promise_to_Pay__c <= Date.today()+1)
            {
                sInv.JVCO_Exempt_from_Surcharge__c = false;
                //08/01/2018 john.patrick.valdez GREEN-27889 Reset Withhold SUrcharge and Exclude from dunning cycle
                sInv.ffps_custRem__Exclude_From_Reminder_Process__c = false;

            }
            sInv.JVCO_Surcharge_Generated__c = true;
            updatedSInvList.add(sInv);
        }
        update updatedSInvList;
    }
    
    /* --------------------------------------------------------------------------------------------------
       Author:         franz.g.a.dimaapi@accenture.com
       Company:        Accenture
       Description:    Set invIdToInvLineListMap using Invoice Id and InvoiceLine List

       Inputs:         List<blng__Invoice__c>
       Returns:        Map<Id, List<blng__InvoiceLine__c>>
       <Date>          <Authors Name>             <Brief Description of Change> 
       09-Oct-2017     Franz Gerrard A. Dimaapi    Initial version of the code
    ------------------------------------------------------------------------------------------------------*/
    private Map<Id, List<OrderItem>> getInvoiceLineRecordsByInvId(List<Order> orderList)
    {   
        Map<Id, List<OrderItem>> ordIdToOrderLineListMap = new Map<Id, List<OrderItem>>();
        for(OrderItem ordItem : [SELECT Id,SBQQ__BookingsIndicator__c, 
                                 PricebookEntryId, ListPrice, SBQQ__QuoteLine__r.Terminate__c,
                                 SBQQ__BillingFrequency__c,SBQQ__BillingType__c,
                                 SBQQ__ChargeType__c,SBQQ__SubscriptionPricing__c,
                                 TotalPrice, UnitPrice, Quantity, Product2Id,
                                 ServiceDate , EndDate, OrderId,
                                 JVCO_Surcharge_Applicable__c,
                                 JVCO_Dimension_2__c, JVCO_Venue__c,
                                 JVCO_Total_Amount__c,
                                 SBQQ__DefaultSubscriptionTerm__c,
                                 SBQQ__QuoteLine__c,
                                 SBQQ__Activated__c,
                                 blng__HoldBilling__c,
                                 Affiliation__c,
                                 JVCO_Temp_End_Date__c,
                                 JVCO_Temp_Start_Date__c
                                 FROM OrderItem
                                 WHERE OrderId IN :orderList])
        {
            //If the id doesn't exist then add the Id plus a new List
            if(!ordIdToOrderLineListMap.containsKey(ordItem.OrderId))
            {
                ordIdToOrderLineListMap.put(ordItem.OrderId, new List<OrderItem>{});
            }
            ordIdToOrderLineListMap.get(ordItem.OrderId).add(ordItem);
        }
        return ordIdToOrderLineListMap;
    }
    
    public void finish(Database.BatchableContext bc)
    {
        if(!isSingleRecord)
        {
            JVCO_MigratedEvents_Batch migratedEventBatch = new JVCO_MigratedEvents_Batch();
            Id batchProcessId = Database.executeBatch(migratedEventBatch,20);

            //mel.andrei.b.santos 26/10/2018 GREEN-33717 - batch size of transistion
            Map<String, JVCO_TransitionManualReviewNotifSettings__c> settingsbatchsize = JVCO_TransitionManualReviewNotifSettings__c.getAll();
            
            Decimal batchsize = settingsbatchsize.get('Limit').Batch_Size__c; 

            if(batchsize != null)
            {
                JVCO_TransitionManualReviewNotification  batch = new JVCO_TransitionManualReviewNotification();
                batch.licAndCustAccIdMap = licAndCustAccIdMap;
                Database.executeBatch(batch, Integer.valueof(batchsize) );
            }else
            {
                JVCO_TransitionManualReviewNotification  batch = new JVCO_TransitionManualReviewNotification();
                batch.licAndCustAccIdMap = licAndCustAccIdMap;
                Database.executeBatch(batch, 50 );
            }
            //end
        }
    }

}