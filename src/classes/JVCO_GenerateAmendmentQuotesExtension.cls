/* ----------------------------------------------------------------------------------------------
    Name: JVCO_GenerateAmendmentQuotesExtension
    Description: Extension Class for JVCO_GenerateAmendmentQuotes page

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    03-Mar-2017     0.1         Marlon Ocillos      Intial creation
    27-Sep-2017     0.2         Jasper Figueroa     Added GREEN-22136 fix
    12-Oct-2018     0.3         Rhys Dela Cruz      GREEN-33673 - Batch Job Helper
----------------------------------------------------------------------------------------------- */
public class JVCO_GenerateAmendmentQuotesExtension 
{
    public Account fetchedAccountRecord;
    public Account accountRecord;
    public Integer contractBatchSize;
    public Integer numberOfRecords {get; set;}
    public List<Contract> contractsToProcess {get; set;}
    public Boolean isContinued {get; set;}
    public Boolean isProcessing {get; set;}
    public String apexJobId {get; set;}
    public Boolean canContinue {get; set;}
    
    public JVCO_GenerateAmendmentQuotesExtension (ApexPages.StandardController stdController) 
    {
        numberOfRecords = 0;
        contractsToProcess = new List<Contract>();
        isContinued = false;
        fetchedAccountRecord = (Account)stdController.getRecord();
        accountRecord = [select Id, Name, Licence_Account_Number__c, JVCO_Contract_Batch_Size__c,JVCO_ProcessingGenerateReviewQuotes__c,JVCO_GenerateReviewQuotesApexJob__c from Account where Id =: fetchedAccountRecord.Id];
        isProcessing = accountRecord.JVCO_ProcessingGenerateReviewQuotes__c;
        apexJobId = accountRecord.JVCO_GenerateReviewQuotesApexJob__c;
        
        if (accountRecord.JVCO_Contract_Batch_Size__c != null && accountRecord.JVCO_Contract_Batch_Size__c != 0) 
        {
            contractBatchSize = (Integer)accountRecord.JVCO_Contract_Batch_Size__c;   
        } else {
            contractBatchSize = 1;
        }
        
        //Start Update GREEN-22316 jasper.j.figueroa
        //contractsToProcess = [select Id, EndDate, ForceDeferred__c from Contract where JVCO_Renewal_Generated__c = false and AccountId =: accountRecord.Id and Id not in (Select SBQQ__MasterContract__c from SBQQ__Quote__c where SBQQ__Account__c =: accountRecord.Id and SBQQ__Type__c = 'Amendment' and SBQQ__Status__c = 'Draft')];
        //contractsToProcess = [SELECT Id, EndDate, ForceDeferred__c FROM Contract WHERE JVCO_Renewal_Generated__c = false AND AccountId = :accountRecord.Id AND Id NOT IN (SELECT SBQQ__MasterContract__c FROM SBQQ__Quote__c WHERE SBQQ__Account__c = :accountRecord.Id AND SBQQ__Type__c = 'Amendment') AND Id IN (SELECT SBQQ__Contract__c FROM SBQQ__Subscription__c WHERE SBQQ__Account__c = :accountRecord.Id AND Affiliation__r.JVCO_End_Date__c = NULL)];
        contractsToProcess = [select Id, EndDate, ForceDeferred__c from Contract where JVCO_Renewal_Generated__c = false and AccountId =: accountRecord.Id and Id not in (Select SBQQ__MasterContract__c from SBQQ__Quote__c where SBQQ__Account__c =: accountRecord.Id and SBQQ__Type__c = 'Amendment' and (SBQQ__Status__c = 'Draft' OR (SBQQ__Status__c = 'Accepted' AND SBQQ__MasterContract__r.JVCO_RenewableQuantity__c = 0)))];
        //End GREEN-22316

        numberOfRecords = contractsToProcess.size();

        canContinue = true;
        if(!JVCO_BatchJobHelper.checkLargeJobs(numberOfRecords)){
            canContinue = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.JVCO_BatchJobHelperCannotProcess));
        }
    }
    
    public void queueGenerateAmendmentQuotes() 
    {
        Id batchId = database.executeBatch(new JVCO_GenerateAmendmentQuotesBatch(accountRecord), contractBatchSize);
        isContinued = true;
        JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_GenerateReviewQuotesApexJob__c', batchId,'JVCO_GenerateReviewQuotesLastSubmitted__c');
    }
    
    public PageReference backToRecord() 
    {
        PageReference pRef = new PageReference('/' + accountRecord.Id);
        return pRef;
    }

    //GREEN-33789
    public PageReference jobCheck(){
        
        Id batchId = apexJobId;
        Id accountId = fetchedAccountRecord.Id;

        JVCO_ApexJobProgressBarController progBar = new JVCO_ApexJobProgressBarController();
        PageReference tempRef = progBar.updateAcc(batchId, accountId);

        return tempRef;
    }
}