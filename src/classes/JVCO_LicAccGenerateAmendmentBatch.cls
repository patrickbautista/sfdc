/* ----------------------------------------------------------------------------------------------------------
    Name:            JVCO_LicAccGenerateAmendmentBatch.cls 
    Description:     Batch class for JVCO_LicAccGenerateAmendmentBatch
    Test class:      

    Date             Version     Author                            Summary of Changes 
    -----------      -------     -----------------                 -------------------------------------------
    25-June-2020       0.1        Accenture-rhys.j.c.dela.cruz         Initial version of the code 
 ---------------------------------------------------------------------------------------------------------- */
global class JVCO_LicAccGenerateAmendmentBatch implements Database.Batchable<sObject>, Database.Stateful, Schedulable
{
    global final Account accountRecord;
    global Integer contractBatchSize;
    global Set<Id> contractIds;
    global Integer quoteCount;
    global Integer errorQuoteCount;
    global String batchErrorMessage;

    global void execute(SchedulableContext sc){

        JVCO_LicAccGenerateAmendmentBatch batch = new JVCO_LicAccGenerateAmendmentBatch();
        Database.executeBatch(batch, 1);
    }

    //Can use static to allow method run in trigger

    global JVCO_LicAccGenerateAmendmentBatch ()
    {
        contractIds = new Set<Id>();
    }

    global JVCO_LicAccGenerateAmendmentBatch (Set<Id> contIds)
    {
        contractIds = contIds;
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        Integer renewalDateNum = Integer.valueOf(JVCO_Renewal_Settings__c.getValues('Automatic Quote Generation').days__c);

        String query = 'SELECT Id, EndDate, ForceDeferred__c, JVCO_HasNonProratableProduct__c, (SELECT Id FROM SBQQ__Subscriptions__r WHERE SBQQ__Product__r.Proratable__c = false LIMIT 1)  FROM Contract ';

        if(!contractIds.isEmpty()){
            query += ' WHERE Id IN: contractIds ';
        }
        else{
            query += ' WHERE JVCO_TempStartDate__c != null AND JVCO_TempEndDate__c != null AND JVCO_ProcessedByCreditsBatch__c = false AND (JVCO_ContractScenario__c = \'Ends After Lockdown\' OR JVCO_ContractScenario__c = \'Ends During Lockdown\') AND Id NOT IN (SELECT SBQQ__MasterContract__c FROM SBQQ__Quote__c WHERE SBQQ__Type__c = \'Amendment\' AND (SBQQ__MasterContract__r.JVCO_ContractScenario__c = \'Ends After Lockdown\' OR SBQQ__MasterContract__r.JVCO_ContractScenario__c = \'Ends During Lockdown\')) AND Account.Type != \'Key Account\' AND Account.JVCO_Review_Type__c = \'Automatic\' ';
            query += 'AND (EndDate <= NEXT_N_DAYS:' +renewalDateNum+ ') '; //GREEN-35778 - 2020-08-07 rhys.j.c.dela.cruz - Add filter for End Date
        }
        
        return Database.getQueryLocator(query);
    }
    
    global void execute (Database.BatchableContext BC, List<sObject> scope)
    {
        if(scope.size() > 0){

            List<Contract> contList = new List<Contract>();
            List<Contract> contractListToUpdate = new List<Contract>();
            List<Contract> contListToAmend = new List<Contract>();
            List<JVCO_TempInvoiceProcessingData__c> tempDataList = new List<JVCO_TempInvoiceProcessingData__c>();
            List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();

            Set<Id> contSetSuccess = new Set<Id>();

            contList.addAll((List<Contract>) scope);

            for (Contract contractRecord : contList) {

                if(contractRecord.SBQQ__Subscriptions__r.size() > 0){ //If there are any non-proratable Lines, remove from list to be amended

                    //contList.remove(contList.indexOf(contractRecord));
                    contractRecord.JVCO_ProcessedByCreditsBatch__c = true;
                    contractRecord.JVCO_HasNonProratableProduct__c = true;
                }
                else{
                    
                    contractRecord.JVCO_ProcessedByCreditsBatch__c = true;
                    contractRecord.ForceDeferred__c = 1;
                    contListToAmend.add(contractRecord);
                }

                contractListToUpdate.add(contractRecord);
            }

            try {

                if(!contractListToUpdate.isEmpty()){

                    List<Database.SaveResult> res = Database.update(contractListToUpdate);
                    
                    for(Integer i = 0; i < contractListToUpdate.size(); i++){

                        Database.SaveResult srRec = res[i];
                        sObject origRecord = contractListToUpdate[i];
    
                        if(srRec.isSuccess()){

                            contSetSuccess.add(origRecord.Id);
                        }
                    }
                }
            } catch (Exception ex) {
                System.debug('Error(ForceDeferred__c): ' + contractListToUpdate + ' ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
            }

            if(!contListToAmend.isEmpty()){

                for (Contract contractRecord : contListToAmend) {
                
                    if(contSetSuccess.contains(contractRecord.Id)){

                        try {
                            Id objectId = contractRecord.Id;
            
                            System.debug('@@ Start Queueable: SBQQ.ContractManipulationAPI.ContractAmender');
                            String qmodelJson = SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractAmender', objectId, null); 
                            system.debug('@@@*** qmodelJson debug: ' + qmodelJson);
    
                            JVCO_TempInvoiceProcessingData__c tempData = new JVCO_TempInvoiceProcessingData__c();
                            tempData.JVCO_Batch_Name__c = 'JVCO_LicAccGenerateAmendmentBatch';
                            tempData.JVCO_Id1__c = objectId;
                            tempData.JVCO_Id2__c = BC.getJobId();
                            tempDataList.add(tempData);
                        } 
                        catch(Exception e) {   
                            System.debug('Error(Contract API): ' + contList + ' ' + e.getMessage() + e.getCause() + e.getStackTraceString());
    
                            String ErrorLineNumber = e.getLineNumber() != Null ? String.ValueOf(e.getLineNumber()):'None';
                            String ErrorMessage = 'Cause: ' + e.getCause() + ' ErrMsg: ' + e.getMessage();
                            errLogList.add(JVCO_ErrorUtil.logError(contractRecord.Id, ErrorMessage , ErrorLineNumber , 'JVCO_LicAccGenerateAmendmentBatch:SBQQ.ContractManipulationAPI'));
                        }
                    }
                }
            }

            if(!errLogList.isEmpty()){
                insert errLogList;
            }

            if(!tempDataList.isEmpty()){
                insert tempDataList;
            }
        }
    }
    
    global void finish (Database.BatchableContext BC)
    {        
        JVCO_LicAccCompleteAmendmentBatch licAccCompleteAmendments = new JVCO_LicAccCompleteAmendmentBatch();
        String jobName = 'Run JVCO_LicAccCompleteAmendmentBatch ' + DateTime.now();
        String scheduleBatch = System.scheduleBatch(licAccCompleteAmendments, jobName, 5, 1);
    }
}