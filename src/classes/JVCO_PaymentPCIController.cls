/* ----------------------------------------------------------------------------------------------
    Name: JVCO_PaymentPCIController 
    Description: VF Page Controller for JVCO_PaymentPCI (NVM and PCI Integration) 
    
        Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    25-Apr-2020   0.1        joseph.g.barrameda  Intial creation
    30-Oct-2020   0.2        sean.g.perez        GREEN-35801 - Issue with Card Payment History not generating on a payment using PCI Pal
     
 */

public class JVCO_PaymentPCIController{

    public Account account;
    public Contact contact; 
        
    public Id accountId {get;set;}
    public Id contactId {get;set;}
    
    public Decimal amount {get;set;}
    
    public String accountName {get;set;}
    public String iframeUrl {get;set;}
    public String agentAccessToken {get;set;} 
    public String agentRefreshToken {get;set;}
    public String sessionGuid {get;set;}    
    public String sessionId {get;set;}
    public String defaultIframeURL {get;set;}
    
    public static JVCO_NVM_PCI_Settings__c config = JVCO_NVM_PCI_Settings__c.getInstance();
    
    //Constructor
    public JVCO_PaymentPCIController(){
        accountId = Apexpages.currentPage().getParameters().get('accountid');
        contactId = Apexpages.currentPage().getParameters().get('contactid'); 
        amount = Decimal.valueOf(Apexpages.currentPage().getParameters().get('amount'));
                 
    }
    

     /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used to authenticate NVM session
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    25-Apr-2020     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/    
    public void authenticateNVM(){

        String clientId= config.JVCO_Client_Id__c;
        String clientSecret=config.JVCO_Client_Secret__c;
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(config.JVCO_NVM_Endpoint__c);
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
    
        String payload = 'client_id='+EncodingUtil.urlEncode(clientId,'UTF-8')+'&client_secret='+EncodingUtil.urlEncode(clientSecret,'UTF-8')+'&grant_type='+EncodingUtil.urlEncode('client_credentials','UTF-8')+'&scope=globalpci';
        req.setBody(payload);
    
        HttpResponse response = http.send(req);
        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            system.debug('NVM Success');
            Map<String,Object> mapBody = (Map<String,Object>)JSON.deserializeUntyped(response.getBody());
    
            // get the Session ID from the response from the call to oauth2/token
            sessionId = (String)mapBody.get('access_token');
            //Request session to PCI             
            startPCISession(sessionId);
        } 
    }
    
     /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used to initiate PCI Session 
    Inputs:         SessionId
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    25-APR-2020     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    private void startPCISession (String sessionId){
        
        defaultIframeURL = 'https://euwest1.pcipalstaging.cloud';
        
        String agentId= [SELECT NVMContactWorld__NVM_Agent_Id__c from User WHERE ID=: userinfo.getUserid() LIMIT 1].NVMContactWorld__NVM_Agent_Id__c;
    
        account = [SELECT Id, Name, JVCO_Billing_Contact__c FROM Account WHERE Id =: accountId];
        accountName = account.Name;
        
        contact = [SELECT Id, Email, FirstName, LastName, MailingStreet, MailingCity, MailingPostalCode, Name 
                    FROM Contact WHERE Id = :account.JVCO_Billing_Contact__c];
         
        Http httpPCI = new Http();
        HttpRequest reqPCI = new HttpRequest();
        
        reqPCI.setMethod('POST');
        reqPCI.setEndpoint(config.JVCO_PCI_Endpoint__c);
        reqPCI.setHeader('Content-Type', 'application/json');
        reqPCI.setHeader('Authorization', ' Bearer ' + sessionId);
    
        reqPCI.setHeader('Accept','application/json');
            
        String jsonString = '{'+
            '\"AgentID\":\"' + agentId + '\",'+
            '\"accountconfigurationId\":'+ config.JVCO_accountconfigurationId__c + ','+
            '\"paymentData\": {'+
            '\"VendorTXCode\":\"' + config.JVCO_VendorTXCode__c + '\",'+
            '\"Amount\":\"' + amount + '\",'+
            '\"Description\":\"Payment\",'+
            '\"CardHolder\":\"'+ contact.Name +'\",'+
            '\"CustomerEmail\":\"' + contact.Email + '\",  '+
            '\"BillingFirstnames\":\"' + contact.FirstName + '\",'+
            '\"BillingSurname\":\"'+ contact.LastName +'\",'+
            '\"BillingAddress1\":\"'+ contact.MailingStreet +'\",'+
            '\"BillingAddress2\":\"'+'\",'+
            '\"BillingCity\":\"' + contact.MailingCity + '\",'+
            '\"BillingPostCode\":\"' + contact.MailingPostalCode +  '\",'+
            '\"BillingCountry\":\"GB\"'+
            '}'+
            '}';
    
        reqPCI.setBody(jsonString);
        
        system.debug('JSON String Payload' + jsonString);
        HttpResponse responsePCI = httpPCI.send(reqPCI);

        Map<String,Object> mapPCIBody = new  Map<String,Object>();
        // If the request is successful, parse the JSON response.
        
        if (responsePCI.getStatusCode() == 200) {
            
            mapPCIBody = (Map<String,Object>)system.JSON.deserializeUntyped(responsePCI.getBody());
            
            // get the IframeURL from the response from the call to
            iframeUrl= (String)mapPCIBody.get('iframeUrl');
            agentAccessToken = (String)mapPCIBody.get('agentAccessToken');
            agentRefreshToken = (String)mapPCIBody.get('agentRefreshToken');
            sessionGuid = (String)mapPCIBody.get('sessionGuid');
            
        } else{
            
            if (iFrameUrl == '' || iframeURL == null){
                iframeURL=defaultIframeUrl;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                    'There is no phone call in active condition assigned to agent (' + agentId + ') at the moment'));
                            
            } 
        
        }  
            
    }    
        
     /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used to get transaction details from PCI after a successful payment transaction 
    Inputs:         SessionGuid, SessionId
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    25-Apr-2020     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public pagereference getPCISessionStatus(String guid, String sessionId){
                    
        Http http = new Http();
        HttpRequest reqSession = new HttpRequest();
        
        String endPt = 'https://emea.api.newvoicemedia.com/globalpci/v1/session/';
        
        reqSession.setMethod('GET');
        reqSession.setEndpoint(endPt + guid);
        reqSession.setHeader('Authorization', 'Bearer ' + sessionId);
        
        HttpResponse responsePCI = http.send(reqSession);          
        
        system.debug(responsePCI);
        
        // If the request is successful, parse the JSON response.
        if (responsePCI.getStatusCode() == 200) {
        
            try{ 
                    
                JSON2ApexForPCI obj = JSON2ApexForPCI.parse(responsePCI.getBody());
                System.debug(obj);
                
                String invString=Apexpages.currentPage().getParameters().get('selectedsinv');
                String cardExpDate = obj.paymentResultData.OutputExpiryDate; 
                
                account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_DD_Payee_Contact__c, JVCO_Billing_Address__c,
                    c2g__CODABillingAddressIsValid__c, Billing_Email__c, Billing_Firstname__c, Billing_Surname__c, c2g__CODABillingMethod__c,
                    JVCO_Billing_Phone__c, BillingAddress, BillingStreet, BillingCity, BillingPostalCode FROM Account WHERE Id = :account.Id];

                contact = [SELECT Id, Email, FirstName, LastName, MailingStreet, MailingCity, MailingPostalCode FROM Contact WHERE Id = :account.JVCO_Billing_Contact__c];
                
                
                List<String> invIds = invString.split(',');
                List<c2g__codaInvoice__c> selectedInvList = [SELECT Id from c2g__codaInvoice__c WHERE ID IN: invIds];
                
                
                //Create Income Card Payment
                Income_Card_Payment__c icp = new Income_Card_Payment__c();
                icp.Account__c = accountId; 
                icp.Authorisation_Code__c = obj.paymentResultData.OutputTxAuthNo;
                icp.Authorisation_Date__c = (DateTime)JSON.deserialize('"' + obj.createdOn + '"', DateTime.class);
                icp.Card_Expiry_Date__c = cardExpDate.left( cardExpDate.length() -2);
                icp.Card_Type__c = obj.paymentResultData.CardType;
                icp.Contact__c = contactId; 
                icp.Immediate_Amount__c = (obj.paymentResultData.Amount/100); 
                icp.Name_On_Card__c = obj.paymentResultData.CardHolder;
                icp.Payment_Description__c ='PPL PRS Card Payment';
                icp.Payment_Reason__c = invString;
                icp.Payment_Status__c = 'Authorised'; 
                icp.Payment_Status_Details__c = obj.paymentResultData.OutputStatusDetails;
                icp.Sagepay_Vendor__c = obj.paymentResultData.Vendor;
                icp.Sagepay_Security_Key__c = obj.paymentResultData.OutputSecurityKey;
                icp.Sagepay_Transaction_ID__c =obj.paymentResultData.OutputVPSTxId;
                icp.Transaction_Type__c = 'Sale';
                icp.Token__c = guid; 
                insert icp; 
                
                //Create Income Card Payment History                       
                Income_Card_Payment_History__c icph = new Income_Card_Payment_History__c();
                icph.Income_Card_Payment__c = icp.id; 
                icph.Authorisation_Code__c = obj.paymentResultData.OutputTxAuthNo;
                icph.Authorisation_Date__c = (DateTime)JSON.deserialize('"' + obj.createdOn + '"', DateTime.class);
                icph.Amount__c = (obj.paymentResultData.Amount/100); 
                icph.Payment_Status__c = 'Authorised'; 
                icph.Payment_Status_Details__c = obj.paymentResultData.OutputStatusDetails;
                icph.SagePay_Vendor_Tx_Code__c = obj.paymentResultData.Vendor;
                icph.Sagepay_Security_Key__c = obj.paymentResultData.OutputSecurityKey;
                icph.Sagepay_Transaction_ID__c =obj.paymentResultData.OutputVPSTxId;
                icph.Transaction_Type__c = 'Sale';
                
                JVCO_FFUtil.stopCardPaymentSMPAfterInsert = TRUE;
                insert icph;                
                
                JVCO_FFUtil.stopCodaInvoiceHandlerAfterUpdate = TRUE;

                JVCO_Invoice_Group__c  invgrp =  SMP_PaymentPageController.createInvoiceGroup ('Debit/Credit Card', false, selectedInvList );
                invgrp.Income_Card_Payment__c = icp.id;
                update invgrp;
                
                          
            }catch(exception e){
                JVCO_ErrorUtil.logError(accountId, e.getMessage(), 'PCI-001', 'JVCO_PaymentPCIController');
                system.debug(e.getStackTraceString());
                system.debug(e.getMessage());            
            }
                
        }
        return null;
        
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used to invoke another method then redirect the page
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-Aug-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public Pagereference finishTransaction(){
    
        getPCISessionStatus(sessionGuid, sessionId);       
        
        Pagereference ref = new Pagereference('/' + accountId);
        ref.setRedirect(true);
        return ref;          
    }

    public Pagereference cancel(){
        
        Pagereference ref = new Pagereference('/' + accountId);
        return ref;
    }

}