@isTest 
private class JVCO_TaskTriggerHandlerTest{
    
    private static Account licAcc;
     
    @testSetup
    public static void setupData(){
        Profile p = [select id from Profile where Name = 'Licensing User' limit 1];
        User runningUser = new User();
        User systemUser = new User();
        UserRole uRole = new UserRole();

        uRole.DeveloperName = 'JVCO_Generic_Test_User'; 
        uRole.Name = 'Generic Test User';
        insert uRole;

        runningUser = new User(
            Alias = 'athr',
            Email='JVCO_AccountTriggerHandlerTest1@gmail.com', 
            EmailEncodingKey='UTF-8',
            FirstName = 'Generic', 
            LastName='User',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            UserRoleId = uRole.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='guuserstand3541987*@testorg.com',
            Division='Legal', Department='Legal'
        );
        
        insert runningUser;
        
        systemUser = new User(
            Alias = 'athr',
            Email='JVCO_systemcreation@gmail.com', 
            EmailEncodingKey='UTF-8',
            FirstName = 'Generic', 
            LastName='User',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            UserRoleId = uRole.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='guuserstand3541234822@testorg.com',
            Division='Legal', Department='Legal'
        );
        
        insert systemUser;
        
        PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors' limit 1];
        PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = ps.id, AssigneeId = systemUser.id);
        insert psa; 
    }
    
    static void prepareTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        Test.startTest();
        List<JVCO_BackgroundMatching__c> bgMatchingList = new List<JVCO_BackgroundMatching__c>();
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Trading Currency', JVCO_Default_Value__c = 'GBP'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Receivable Control', JVCO_Default_Value__c = '10040'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Output Vat Code', JVCO_Default_Value__c = 'GB-O-STD-LIC'));
        
        insert bgMatchingList;
        
        List<c2g__codaDimension3__c> dimensions = new List<c2g__codaDimension3__c>();
        dimensions.add(new c2g__codaDimension3__c(Name = 'Key', c2g__ReportingCode__c = 'Key'));
        dimensions.add(new c2g__codaDimension3__c(Name = 'Non Key', c2g__ReportingCode__c = 'Non Key'));
        insert dimensions;
        
        c2g__codaDimension1__c dim1 = new c2g__codaDimension1__c(Name = 'Live', c2g__ReportingCode__c = 'BC002', JVCO_Function__c = 'Budget');
        insert dim1;
        
        c2g__codaDimension1__c dim2 = new c2g__codaDimension1__c(Name = 'General Purpose', c2g__ReportingCode__c = 'BC004', JVCO_Function__c = 'Budget');
        insert dim2;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        insert glaList;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        
        
        User runUser = [SELECT Id FROM User WHERE Email = 'JVCO_systemcreation@gmail.com'];
        System.runAs(runUser){
            
            Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
            insert c;

            licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
            licAcc.JVCO_In_Enforcement__c = true;
            licAcc.JVCO_Credit_Status__c = 'Enforcement - Debt Recovery';
            insert licAcc;
        }
        
        Test.stopTest();
    }
    private static testMethod void testInsertTaskWithRestriction(){
        prepareTestData();
        
        User runUser = [SELECT Id FROM User WHERE Email = 'JVCO_AccountTriggerHandlerTest1@gmail.com'];
        System.assertNotEquals(runUser, null);
        Boolean errorExists = false;
        
        System.runAs(runUser){
            Task taskRec = new Task();
            taskRec.Subject = 'Send Quote';
            taskRec.WhatId = licAcc.Id;
            
            try{
                insert taskRec;
            }
            catch(Exception e){
                errorExists = true;
            }
        }
        
        //System.assertEquals(errorExists, True);
    }
    
    private static testMethod void testInsertTask(){
        prepareTestData();
        
        User runUser = [SELECT Id FROM User WHERE Email = 'JVCO_AccountTriggerHandlerTest1@gmail.com'];
        System.assertNotEquals(runUser, null);
        
        licAcc.JVCO_In_Enforcement__c = false;
        update licAcc;
        
        Boolean errorExists = false;
        
        System.runAs(runUser){
            Task taskRec = new Task();
            taskRec.Subject = 'Call';
            taskRec.WhatId = licAcc.Id;
            
            try{
                insert taskRec;
            }
            catch(Exception e){
                errorExists = true;
            }
        }
        
        System.assertEquals(errorExists, false);
    }
    
    private static testMethod void testEditTaskWithRestriction(){
        prepareTestData();
        
        User runUser = [SELECT Id FROM User WHERE Email = 'JVCO_AccountTriggerHandlerTest1@gmail.com'];
        System.assertNotEquals(runUser, null);
        Boolean errorExists = false;
        
        Task taskRec = new Task();
            taskRec.Subject = 'Call';
            taskRec.WhatId = licAcc.Id;
            insert taskRec;
            
        System.runAs(runUser){            
            
            try{
                taskRec.Subject = 'Call me now';
                update taskRec;
            }
            catch(Exception e){
                errorExists = true;
            }
        }
        
        System.assertEquals(errorExists, true);
    }
    private static testMethod void testDeleteTaskWithRestriction(){
        prepareTestData();
        
        User runUser = [SELECT Id FROM User WHERE Email = 'JVCO_AccountTriggerHandlerTest1@gmail.com'];
        System.assertNotEquals(runUser, null);
        Boolean errorExists = false;
        
        Task taskRec = new Task();
            taskRec.Subject = 'Call';
            taskRec.WhatId = licAcc.Id;
            insert taskRec;
                    
        System.runAs(runUser){
            
            try{
                delete taskRec;
            }
            catch(Exception e){
                errorExists = true;
                System.assertEquals(errorExists, true);
            }
        }
    }
    private static testMethod void testDeleteTaskWithoutRestriction(){
        prepareTestData();
        Boolean errorExists = false;
        
        Task taskRec = new Task();
        taskRec.Subject = 'Call';
        taskRec.WhatId = licAcc.Id;
        insert taskRec;
        try{
            delete taskRec;
        }
        catch(Exception e){
            errorExists = true;
            System.assertEquals(errorExists, true);
        }
    }
}