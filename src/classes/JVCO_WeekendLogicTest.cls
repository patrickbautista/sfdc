@isTest
private class JVCO_WeekendLogicTest
{
	@isTest
	static void testWeekendLogic()
	{
		List<c2g__codaTransaction__c> l_trans = new List<c2g__codaTransaction__c>();
		l_trans.add(new c2g__codaTransaction__c( c2g__TransactionDate__c = date.today()));

		JVCO_WeekendLogic.setWeekendWork(l_trans);

		for (c2g__codaTransaction__c l : l_trans)
		{
			//boolean test = );
			System.Assert(!String.IsEmpty(String.valueof(l.JVCO_Week_End__c)), 'Was not able to get weekend');
		}

	}
}