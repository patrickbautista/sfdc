/* ----------------------------------------------------------------------------------------------
   Name: JVCO_DeactivateOrderLogic.cls 
   Description: Business logic class for deactivating selected Order records

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  30-Mar-2017   0.1         ryan.i.r.limlingan  Intial creation
  ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_DeactivateOrderLogic
{
    private ApexPages.StandardSetController stdCtrl;
    private List<Order> selectedOrders;
    public Integer numOfSelectedOrders {get; set;}
    public List<Order> retrievedOrders {get; set;}

    // Constants used for page messages
    public final String NO_RECORDS_ERR = System.Label.JVCO_OrderNoRecords;
    public final String INVALID_RECORDS_ERR = System.Label.JVCO_OrderDeactivateInvalid;

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Class constructor
    Inputs: StandardSetController
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    30-Mar-2017 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public JVCO_DeactivateOrderLogic(ApexPages.StandardSetController ssc)
    {
        stdCtrl = ssc;
        // Retrieve selected Order records from related list
        selectedOrders = (List<Order>)ssc.getSelected();
        numOfSelectedOrders = selectedOrders.size();
    }

 /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: <TBD>
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    06-Mar-2017 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public PageReference deactivate()
    {
        if (numOfSelectedOrders > 0)
        {
            retrievedOrders = [SELECT Id, Status, AccountId,
                                  (SELECT Id FROM OrderItems)
                               FROM Order WHERE Id IN :selectedOrders];
            if (isListValid(retrievedOrders))
            {
                for (Order o : retrievedOrders)
                {
                    o.Status = 'Draft';
                }
                update retrievedOrders;
            } else {
                ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.Severity.ERROR, INVALID_RECORDS_ERR));
                numOfSelectedOrders = 0; // To simulate no selected invoice records
                return null;
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, NO_RECORDS_ERR));
            return null;
        }
        return new PageReference('/' + retrievedOrders.get(0).AccountId);
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Returns FALSE if there are Billing Invoice Lines already linked to Order Products
    Inputs: List<Order>
    Returns: Boolean
    <Date>      <Authors Name>      <Brief Description of Change> 
    26-Mar-2017 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public Boolean isListValid(List<Order> selectedOrders)
    {
        List<OrderItem> oiList = new List<OrderItem>();
        for (Order o : selectedOrders)
        {
            // Retrieve all Order Products from select Order records
            oiList.addAll(o.OrderItems);
        }

        List<blng__InvoiceLine__c> bInvLineList = [SELECT Id FROM blng__InvoiceLine__c
                                                   WHERE blng__OrderProduct__c IN :oiList];
        if (bInvLineList.isEmpty()) return TRUE;
        return FALSE;
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Returns to Account detail page
    Inputs: N/A
    Returns: PageReference
    <Date>      <Authors Name>      <Brief Description of Change> 
    30-Mar-2017 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public PageReference returnToAccount()
    {
        PageReference page = new PageReference(stdCtrl.cancel().getUrl());
        page.setRedirect(TRUE);
        return page;
    }
}