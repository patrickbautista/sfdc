@isTest
public class JVCO_LinkSubscriptionToProdFeatTest {
	@testSetup
	public static void setupData(){        
	    Id pbId = Test.getStandardPricebookId();

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;
	    
        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;

        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        SBQQ__ProductFeature__c pf = new SBQQ__ProductFeature__c(
        	SBQQ__ConfiguredSKU__c = prod.Id, 
        	SBQQ__Number__c = 1
        );
        insert pf;

        pf.JVCO_Temp_PF_Ext_ID__c = pf.Id;
        update pf;

        SBQQ__ProductOption__c po = new SBQQ__ProductOption__c(
        	SBQQ__ConfiguredSKU__c = prod.Id, 
        	SBQQ__Number__c = 1
        );
        insert po;

    	SBQQ__Quote__c q = new SBQQ__Quote__c(
    		SBQQ__Primary__c = true, 
    		SBQQ__StartDate__c = date.today(), 
    		Start_Date__c = date.today(), 
    		SBQQ__Status__c = 'Draft', 
    		SBQQ__Type__c = 'Quote', 
    		SBQQ__Account__c = acc2.Id, 
    		SBQQ__PriceBook__c = pbId, 
    		SBQQ__SubscriptionTerm__c = 12 
    	);
        insert q;
        
		SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c(
			SBQQ__Quote__c = q.Id, 
			SBQQ__Product__c = prod.Id
		);
        insert ql;
        
        
    	Contract cont = new Contract(
    		SBQQ__Quote__c = q.Id, 
    		AccountId = q.SBQQ__Account__c, 
    		StartDate = date.today()
    	);
        insert cont;
        
        SBQQ__Subscription__c sub = new SBQQ__Subscription__c(
			SBQQ__Contract__c = cont.Id, 
			SBQQ__QuoteLine__c = ql.Id, 
			SBQQ__Account__c = q.SBQQ__Account__c, 
			SBQQ__Product__c = ql.SBQQ__Product__c, 
			SBQQ__Quantity__c = 1
		);
        insert sub;

        sub.JVCO_Temp_DynamicOptionId__c = pf.Id; 
		sub.JVCO_Temp_RequiredById__c = sub.Id;
        sub.JVCO_Subscription_Temp_External_ID__c = sub.Id;
        update sub;
	}

	public static testMethod void testLinkSubToPF(){
		Test.startTest();

		JVCO_LinkSubscriptionToProdFeat lspf = new JVCO_LinkSubscriptionToProdFeat();
		Database.executeBatch(lspf);

		Test.stopTest();

        SBQQ__Subscription__c subChecker = [select Id, SBQQ__DynamicOptionId__c, SBQQ__RequiredById__c, JVCO_Temp_DynamicOptionId__c, JVCO_Temp_RequiredById__c, JVCO_Subscription_Temp_External_ID__c from SBQQ__Subscription__c order by CreatedDate DESC limit 1];
        SBQQ__ProductFeature__c pfChecker = [select Id from SBQQ__ProductFeature__c order by CreatedDate DESC limit 1];

        System.assertEquals(subChecker.JVCO_Temp_RequiredById__c, subChecker.Id);
        System.assertEquals(subChecker.JVCO_Temp_DynamicOptionId__c, pfChecker.Id);
	}
}