/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_QuoteLineTriggerHandlerTest.cls 
   Description:     Test class for JVCO_QuoteLineTriggerHandler.cls

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
  03-Jan-2017       0.1         Accenture-raus.k.b.ablaza         Initial version of the code      
  ---------------------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_QuoteLineTriggerHandlerTest {
    
    @testSetup
    private static void setupData(){        
        
        test.StartTest();

        List<JVCO_Primary_Tariff_Mapping__c> settings = new List<JVCO_Primary_Tariff_Mapping__c>();
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Aircraft', JVCO_Primary_Tariff__c = 'AC_PRS'));
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Hotel', JVCO_Primary_Tariff__c = 'HRH_PRS'));
        insert settings;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        PriceBook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.JVCO_Live__c = true;
        acc2.JVCO_Preferred_Contact_Method__c = 'Email';
        acc2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        insert acc2;
      
        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
		ven.JVCO_Primary_Tariff__c = 'AC_PRS';
        insert ven;

        JVCO_Venue__c ven2 = JVCO_TestClassObjectBuilder.createVenue();
        ven2.JVCO_Primary_Tariff__c = 'RS_PRS';
        ven2.Name__c = 'Test Ven2';
        ven.JVCO_Email__c = 'newemail@mail.com';
        ven2.JVCO_Street__c = '123 Testing Street'; 
        ven2.JVCO_City__c = 'New Testing City';
        ven2.JVCO_Country__c = 'NewCountry';
        ven2.JVCO_Postcode__c = 'B33 8TH';
        ven2.External_Id__c = null;
        insert ven2;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_End_Date__c = null;
        aff.JVCO_Closure_Reason__c = null;
        insert aff;

        JVCO_Affiliation__c aff2 = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff2.JVCO_End_Date__c = Date.today().addDays(365);
        aff2.JVCO_Closure_Reason__c = 'Deceased';
        aff2.name = 'Test Aff2';
        insert aff2;

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        insert opp;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
        insert q;
        
        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Primary_Tariff__c, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.id, prod.id);
        insert ql;

        SBQQ__Subscription__c sub = JVCO_TestClassObjectBuilder.createSubscription(acc2.id, prod.id);
        insert sub;      

        Test.StopTest();
    }

    private static testMethod void testStampAffiliation() {

        Test.startTest();
        
        Product2 prod = [select Id from Product2 limit 1];
        SBQQ__QuoteLineGroup__c qlg = [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        List<SBQQ__QuoteLine__c> assertChecker = new List<SBQQ__QuoteLine__c>();
        SBQQ__Subscription__c sub = [select id FROM SBQQ__Subscription__c limit 1];
        
        ql.Affiliation__c = qlg.JVCO_Affiliated_Venue__c;
        ql.SBQQ__Quote__c = qlg.SBQQ__Quote__c;
        ql.SBQQ__Group__c = qlg.Id;
        ql.SBQQ__Product__c = prod.Id; 
        
        insert ql;
  
        update ql;
    
        Test.stopTest();
        
        assertChecker = [select Id from SBQQ__QuoteLine__c];
        System.assert(!assertChecker.isEmpty());
    }

    private static testMethod void SetBudget() {

        Product2 prod = [select Id from Product2 limit 1];
        SBQQ__QuoteLineGroup__c qlg = [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        List<SBQQ__QuoteLine__c> assertChecker = new List<SBQQ__QuoteLine__c>();
        SBQQ__Subscription__c sub = [select id FROM SBQQ__Subscription__c limit 1];
        
        //ql.Affiliation__c = qlg.JVCO_Affiliated_Venue__c;
        ql.SBQQ__Quote__c = qlg.SBQQ__Quote__c;
        ql.SBQQ__Group__c = qlg.Id;
        ql.SBQQ__Product__c = prod.Id; 
        
        insert ql;
        
        Test.startTest();
        update ql;
    
        Test.stopTest();
        
        assertChecker = [select Id from SBQQ__QuoteLine__c];
        System.assert(!assertChecker.isEmpty());
    }

    private static testMethod void qlgAlreadyUpdatedQliPrimaryTariff_false() {
        
        Product2 prod = [select Id from Product2 limit 1];
        SBQQ__QuoteLineGroup__c qlg = [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        List<SBQQ__QuoteLine__c> assertChecker = new List<SBQQ__QuoteLine__c>();
        SBQQ__Subscription__c sub = [select id FROM SBQQ__Subscription__c limit 1];
        
        //ql.Affiliation__c = qlg.JVCO_Affiliated_Venue__c;
        ql.SBQQ__Quote__c = qlg.SBQQ__Quote__c;
        
        ql.SBQQ__Product__c = prod.Id; 
        
        insert ql;
        JVCO_QuoteLineGroupTriggerHandler.qlgAlreadyUpdatedQliPrimaryTariff = false;
        ql.SBQQ__Group__c = qlg.Id;
        Test.startTest();
        update ql;
    
        Test.stopTest();
        
        assertChecker = [select Id from SBQQ__QuoteLine__c];
        System.assert(!assertChecker.isEmpty());
    }

    private static testMethod void upSell() {

        
        
        Product2 prod = [select Id from Product2 limit 1];
        SBQQ__QuoteLineGroup__c qlg = [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        List<SBQQ__QuoteLine__c> assertChecker = new List<SBQQ__QuoteLine__c>();
        SBQQ__Subscription__c sub = [select id FROM SBQQ__Subscription__c limit 1];
        
        //ql.Affiliation__c = qlg.JVCO_Affiliated_Venue__c;
        ql.SBQQ__Quote__c = qlg.SBQQ__Quote__c;
        
        ql.SBQQ__Product__c = prod.Id; 
        ql.JVCO_Upsell__c = false;
        insert ql;
        JVCO_QuoteLineGroupTriggerHandler.qlgAlreadyUpdatedQliPrimaryTariff = false;
        ql.SBQQ__Group__c = qlg.Id;
        ql.JVCO_Revenue_Type__c = 'Review';
        ql.JVCO_Upsell__c = true;
        Test.startTest();
        update ql;
    
        Test.stopTest();
        
        assertChecker = [select Id from SBQQ__QuoteLine__c];
        System.assert(!assertChecker.isEmpty());
    }
    
    private static testMethod void stampQuoteLineGroup(){

        

        Product2 prod = [select Id from Product2 limit 1];
        SBQQ__QuoteLineGroup__c qlg = [select Id, JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c limit 1];
        SBQQ__Quote__c q = [select Id, SBQQ__Type__c from SBQQ__Quote__c limit 1];
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        List<SBQQ__QuoteLine__c> assertChecker = new List<SBQQ__QuoteLine__c>();
         SBQQ__Subscription__c sub = [select id FROM SBQQ__Subscription__c limit 1];

        ql.Affiliation__c = qlg.JVCO_Affiliated_Venue__c;
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Group__c = qlg.Id;
        ql.SBQQ__Product__c = prod.Id;
        ql.SBQQ__UpgradedSubscription__c =  sub.id;
        
        q.SBQQ__Type__c = 'Amendment';
        
     
        update q;
        insert ql;
        Test.startTest();
        update ql;
        
        Test.stopTest();
        
        assertChecker = [select Id from SBQQ__QuoteLine__c];
        System.assert(!assertChecker.isEmpty());
    }
    
    private static testMethod void matchingQuoteLineGroup(){
         

        Product2 prod = [select Id from Product2 limit 1];
        SBQQ__QuoteLineGroup__c qlg = [select Id, JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c limit 1];
        SBQQ__Quote__c q = [select Id, SBQQ__Type__c from SBQQ__Quote__c limit 1];
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        List<SBQQ__QuoteLine__c> assertChecker = new List<SBQQ__QuoteLine__c>();
        
        ql.Affiliation__c = qlg.JVCO_Affiliated_Venue__c;
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Group__c = qlg.Id;
        ql.SBQQ__Product__c = prod.Id;
        
        q.SBQQ__Type__c = 'Amendment';
       
        
       
        update q;
        insert ql;
        ql.Affiliation__c = null;
        ql.JVCO_Revenue_Type__c = 'Review'; 
        ql.JVCO_Upsell__c = true;
        Test.startTest();
        update ql;
        
        Test.stopTest();
        
        assertChecker = [select Id from SBQQ__QuoteLine__c];
        System.assert(!assertChecker.isEmpty());
    }

    private static testMethod void testDelete(){
        
        Product2 prod = [select Id from Product2 limit 1];
        SBQQ__QuoteLineGroup__c qlg = [select Id, JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c limit 1];
        SBQQ__Quote__c q = [select Id, SBQQ__Type__c, SBQQ__Account__c   from SBQQ__Quote__c limit 1];
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        List<SBQQ__QuoteLine__c> assertChecker = new List<SBQQ__QuoteLine__c>();
        SBQQ__Subscription__c sub = [select id FROM SBQQ__Subscription__c limit 1];

        //start added by edmundCua
        JVCO_Venue__c testVenue = new JVCO_Venue__c();
        testVenue = [select id from JVCO_Venue__c limit 1];

        JVCO_Artist__c testArtist = new JVCO_Artist__c();
        testArtist.name = 'Test Artist';
        insert testArtist;
        

        JVCO_Event__c EventRecord = new JVCO_Event__c();
        EventRecord.JVCO_Venue__c = testVenue.Id;                
        EventRecord.JVCO_Artist__c = testArtist.Id;
        EventRecord.JVCO_Event_Start_Date__c =  Date.Today();
        EventRecord.JVCO_Event_End_Date__c =Date.Today().addDays(25);
        EventRecord.JVCO_Invoice_Paid__c = false;
        EventRecord.JVCO_Event_Classification_Status__c = 'Classified';
        EventRecord.JVCO_Promoter__c = q.SBQQ__Account__c   ;
        EventRecord.JVCO_Tariff_Code__c = 'LC';
        EventRecord.JVCO_Event_Type__c = 'Concert - Qualifies for Old Rate';
        EventRecord.Headline_Type__c = 'No Headliner'; 
        EventRecord.JVCO_Number_of_Set_Lists_Expected__c = 2;
        insert EventRecord;
		

        List<SBQQ__QuoteLine__c> listQuoteLine = new List<SBQQ__QuoteLine__c>();

        SBQQ__QuoteLine__c ql2 = new SBQQ__QuoteLine__c();
        ql2.Affiliation__c = qlg.JVCO_Affiliated_Venue__c;
        ql2.SBQQ__Quote__c = q.Id;
        ql2.SBQQ__Group__c = qlg.Id;
        ql2.SBQQ__Product__c = prod.Id; 
        ql2.SBQQ__RenewedSubscription__c = sub.id;
        listQuoteLine.add(ql2);

        ql.JVCO_Event__c = EventRecord.id;
        //end added by edmundCua


        ql.Affiliation__c = qlg.JVCO_Affiliated_Venue__c;
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Group__c = qlg.Id;
        ql.SBQQ__Product__c = prod.Id; 
        ql.SBQQ__RenewedSubscription__c = sub.id;
        //start added by edmundCua
        listQuoteLine.add(ql);
        //end added by edmundCua
        
        q.SBQQ__Type__c = 'Renewal';
       
        

        update q;
		
        Test.startTest();
        //insert ql;
        //update ql;
        //delete ql;
        //start added byedmundCua
        insert listQuoteLine;
        //update listQuoteLine;
        delete listQuoteLine;
        //end added by edmundCua
        
        
        Test.stopTest();
        
        //assertChecker = [select Id from SBQQ__QuoteLine__c];
        //System.assert(assertChecker.isEmpty());
    }

    private static testMethod void testAddMinFee(){
        
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_OpportunityTrigger__c = true;
        insert dt;

        Product2 p = new Product2();
        p.Name = 'PPL Min fee';
        p.ProductCode = 'MRPPPL';
        p.IsActive = true;
        p.SBQQ__HidePriceInSearchResults__c = true;
        p.SBQQ__AssetConversion__c = 'One quote per line';
        p.SBQQ__DefaultQuantity__c = 1.0;
        p.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        p.SBQQ__SubscriptionTerm__c = 12;
        p.SBQQ__ChargeType__c = 'Recurring';
        p.SBQQ__BillingFrequency__c = 'Annual';
      //  p.blng__BillingRule__c = br.id;
        p.SBQQ__BillingType__c = 'Advance';
        insert p;

        Product2 p2 = new Product2();
        p2.Name = 'PPL Min fee';
        p2.ProductCode = 'MRPVPL';
        p2.IsActive = true;
        p2.SBQQ__HidePriceInSearchResults__c = true;
        p2.SBQQ__AssetConversion__c = 'One quote per line';
        p2.SBQQ__DefaultQuantity__c = 1.0;
        p2.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        p2.SBQQ__SubscriptionTerm__c = 12;
        p2.SBQQ__ChargeType__c = 'Recurring';
        p2.SBQQ__BillingFrequency__c = 'Annual';
      //  p.blng__BillingRule__c = br.id;
        p2.SBQQ__BillingType__c = 'Advance';
        insert p2;

        List<SBQQ__QuoteLine__c> testQLList = new List<SBQQ__QuoteLine__c>();


        Test.startTest();
        SBQQ__QuoteLineGroup__c qlg = [select Id, JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c limit 1];
        SBQQ__Quote__c q = [select Id, SBQQ__Type__c, SBQQ__Account__c, JVCO_PPLMinFeeAdded__c   from SBQQ__Quote__c limit 1];
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c(SBQQ__Product__c = p.id, SBQQ__Quote__c = q.id, SBQQ__Group__c = qlg.id);
        //insert ql;
        SBQQ__QuoteLine__c ql2 = new SBQQ__QuoteLine__c(SBQQ__Product__c = p2.id, SBQQ__Quote__c = q.id, SBQQ__Group__c = qlg.id);
        //insert ql2;
        testQLList.add(ql);
        testQLList.add(ql2);
        insert testQLList;

        Test.stopTest();

        SBQQ__Quote__c qRec = [select Id, SBQQ__Type__c, SBQQ__Account__c, JVCO_PPLMinFeeAdded__c, JVCO_VPLMinFeeAdded__c  from SBQQ__Quote__c Limit 1];
        system.assertEquals(qRec.JVCO_PPLMinFeeAdded__c, true);
        system.assertEquals(qRec.JVCO_VPLMinFeeAdded__c, true);

            
    }

    private static testMethod void testDeleteMinFee(){
        
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        insert dt;

        Product2 p = new Product2();
        p.Name = 'PPL Min fee';
        p.ProductCode = 'MRPPPL';
        p.IsActive = true;
        p.SBQQ__HidePriceInSearchResults__c = true;
        p.SBQQ__AssetConversion__c = 'One quote per line';
        p.SBQQ__DefaultQuantity__c = 1.0;
        p.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        p.SBQQ__SubscriptionTerm__c = 12;
        p.SBQQ__ChargeType__c = 'Recurring';
        p.SBQQ__BillingFrequency__c = 'Annual';
      //  p.blng__BillingRule__c = br.id;
        p.SBQQ__BillingType__c = 'Advance';
        insert p;

        Product2 p2 = new Product2();
        p2.Name = 'VPL Min fee';
        p2.ProductCode = 'MRPVPL';
        p2.IsActive = true;
        p2.SBQQ__HidePriceInSearchResults__c = true;
        p2.SBQQ__AssetConversion__c = 'One quote per line';
        p2.SBQQ__DefaultQuantity__c = 1.0;
        p2.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        p2.SBQQ__SubscriptionTerm__c = 12;
        p2.SBQQ__ChargeType__c = 'Recurring';
        p2.SBQQ__BillingFrequency__c = 'Annual';
      //  p.blng__BillingRule__c = br.id;
        p2.SBQQ__BillingType__c = 'Advance';
        insert p2;

        List<SBQQ__QuoteLine__c> testQLList2 = new List<SBQQ__QuoteLine__c>();
    
        SBQQ__QuoteLineGroup__c qlg = [select Id, JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c limit 1];
        SBQQ__Quote__c q = [select Id, SBQQ__Type__c, SBQQ__Account__c, JVCO_PPLMinFeeAdded__c   from SBQQ__Quote__c limit 1];
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c(SBQQ__Product__c = p.id, SBQQ__Quote__c = q.id, SBQQ__Group__c = qlg.id);
        //insert ql;
        SBQQ__QuoteLine__c ql2 = new SBQQ__QuoteLine__c(SBQQ__Product__c = p2.id, SBQQ__Quote__c = q.id, SBQQ__Group__c = qlg.id);
        //insert ql2;
        testQLList2.add(ql);
        testQLList2.add(ql2);
        
        Test.startTest();
        insert testQLList2;
        Test.stopTest();
        delete ql;
        delete ql2;
        
        SBQQ__Quote__c qRec = [select Id, SBQQ__Type__c, SBQQ__Account__c, JVCO_PPLMinFeeAdded__c, JVCO_VPLMinFeeAdded__c  from SBQQ__Quote__c Limit 1];
        system.assertEquals(qRec.JVCO_PPLMinFeeAdded__c, false);
        system.assertEquals(qRec.JVCO_VPLMinFeeAdded__c, false);
        
            
    }

    private static testMethod void testLookup(){

        Product2 p = new Product2();
        p.Name = 'PPL Min fee';
        p.ProductCode = 'MRPPPL';
        p.IsActive = true;
        p.SBQQ__HidePriceInSearchResults__c = true;
        p.SBQQ__AssetConversion__c = 'One quote per line';
        p.SBQQ__DefaultQuantity__c = 1.0;
        p.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        p.SBQQ__SubscriptionTerm__c = 12;
        p.SBQQ__ChargeType__c = 'Recurring';
        p.SBQQ__BillingFrequency__c = 'Annual';
      //  p.blng__BillingRule__c = br.id;
        p.SBQQ__BillingType__c = 'Advance';
        insert p;

        List<SBQQ__QuoteLine__c> testQLList3 = new List<SBQQ__QuoteLine__c>();
    
        SBQQ__QuoteLineGroup__c qlg = [select Id, JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c limit 1];
        JVCO_Venue__c ven = [select id from JVCO_Venue__c limit 1];
        JVCO_Affiliation__c af = [select id from JVCO_Affiliation__c limit 1];

        SBQQ__Quote__c q = [select Id, SBQQ__Type__c, SBQQ__Account__c, JVCO_PPLMinFeeAdded__c   from SBQQ__Quote__c limit 1];

        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c(SBQQ__Product__c = p.id, SBQQ__Quote__c = q.id, SBQQ__Group__c = qlg.id);
        ql.Affiliation__c = null;
        ql.SBQQ__Group__c = null;
        ql.JVCO_Venue__c = null;

        testQLList3.add(ql);
        insert testQLList3;

        Test.startTest();

        ql.JVCO_TempGroupLookup__c = qlg.id;
        ql.JVCO_TempVenueLookup__c = ven.id;
        ql.JVCO_TempAffLookup__c = af.id;
        update ql;
        Test.stopTest();

    }

    private static testMethod void testDeleteQuoteLineCancelledQuote()
    {
        User currentUser = JVCO_TestClassObjectBuilder.createUser('Licensing User');
        insert currentUser;

        SBQQ__Quote__c quoteRec = [SELECT Id, SBQQ__Type__c, JVCO_Cancelled__c FROM SBQQ__Quote__c LIMIT 1];
        quoteRec.JVCO_Cancelled__c = true;
        update quoteRec;

        SBQQ__QuoteLine__c quoteLineRec = [SELECT Id, SBQQ__Quote__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =: quoteRec.Id Limit 1];

        system.runAs(currentUser)
        {
            try
            {
                Test.startTest();
                delete quoteLineRec;
                Test.stopTest();

            }catch(Exception e)
            {
                Boolean expectedError = e.getMessage().contains('cannot delete Quote Line')? true : false;
                system.assert(expectedError);
            }
        }
    }

    private static testMethod void testDeleteQuoteLineAcceptedQuote()
    {
        User currentUser = JVCO_TestClassObjectBuilder.createUser('Licensing User');
        insert currentUser;

        SBQQ__Quote__c quoteRec = [SELECT Id, SBQQ__Type__c, SBQQ__Status__c FROM SBQQ__Quote__c LIMIT 1];
        quoteRec.SBQQ__Status__c = 'Accepted';
        update quoteRec;

        SBQQ__QuoteLine__c quoteLineRec = [SELECT Id, SBQQ__Quote__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =: quoteRec.Id Limit 1];

        system.runAs(currentUser)
        {
            try
            {
                Test.startTest();
                delete quoteLineRec;
                Test.stopTest();

            }catch(Exception e)
            {
                Boolean expectedError = e.getMessage().contains('cannot delete a Quote Line')? true : false;
                system.assert(expectedError);
            }
        }
    }

    private static testMethod void testDeleteQuoteLineSentForApprovalQuote()
    {
    
        SBQQ__Quote__c quoteRec = [SELECT Id, SBQQ__Type__c, Sent_for_Approval__c FROM SBQQ__Quote__c LIMIT 1];
        quoteRec.Sent_for_Approval__c = true;
        update quoteRec;

        SBQQ__QuoteLine__c quoteLineRec = [SELECT Id, SBQQ__Quote__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =: quoteRec.Id Limit 1];

        {
            try
            {
                Test.startTest();
                delete quoteLineRec;
                Test.stopTest();

            }catch(Exception e)
            {
                Boolean expectedError = e.getMessage().contains('cannot delete Quote Line')? true : false;
            }
        }
    }  
}