/* ----------------------------------------------------------------------------------------------
    Name: JVCO_NewEventExtensionTest 
    Description: Test Class for JVCO_NewEventExtension
                
    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    17-Nov-2017     0.1         Anna Gonzales       Initial Creation
	10-Feb-2021		0.2			Luke.Walker			Updated to make compatible with Lightning GREEN-36286
----------------------------------------------------------------------------------------------- */

@isTest
public class JVCO_NewEventExtensionTest {   
   
    /* Not required after updates GREEN-36286
    static TestMethod void eventTest() {
        
        //List<Account> cusAcc = TestDataFactory.createCustomerAccounts(1, TestDataFactory.CustomerAccountType.CUSTOMER);
        //List<Account> licAcc = TestDataFactory.createLicenceAccounts(1, cusAcc.get(0).id);

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account licAcc3 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        licAcc3.AccountNumber = '832022';
        LicAcc3.JVCO_Live__c = true;
        insert licAcc3; 

        List<JVCO_Venue__c> ven = TestDataFactory.createVenues(1);
        
        JVCO_Festival__c fest = new JVCO_Festival__c();
        fest.Name = 'Test Festival';
        fest.JVCO_Start_Date__c = date.today();
        fest.JVCO_End_Date__c = date.today();
        insert fest;


        //licAcc[0].JVCO_Live__c = true;
        //update licAcc[0];
        
        JVCO_Event__c eve = new JVCO_Event__c();
        eve.Name = 'Test Venue';
        eve.JVCO_Festival__c = fest.Id;
        eve.JVCO_Venue__c = ven.get(0).id;
        //eve.JVCO_Promoter__c = licAcc.get(0).id;
        eve.JVCO_Event_Type__c = 'Concert - Qualifies for Old Rate';
        eve.JVCO_Event_Start_Date__c = date.today();
        eve.JVCO_Event_End_Date__c = date.today();
        eve.JVCO_Tariff_Code__c = 'BF';
        eve.License_Account__c=licAcc3.id;
        eve.Headline_Type__c = 'No Headliner';
        eve.JVCO_Number_of_Set_Lists_Expected__c = 2;
        insert eve;
        
        ApexPages.StandardController sc = new ApexPages.standardController(eve);    
        JVCO_NewEventExtension newE = new JVCO_NewEventExtension(sc);
        
        
        //JVCO_Venue__c venue = new JVCO_Venue__c();
        //venue.Name = 'Test Venue1';
        //venue.JVCO_Field_Visit_Requested__c = true;
        //venue.JVCO_Street__c = 'testStreet';
        //venue.JVCO_City__c = 'testCoty';
        //venue.JVCO_Postcode__c ='AB1';
        //venue.JVCO_Country__c ='UK';
        
        test.startTest();
            //newE.getReturnEventCode();
            //newE.getReturnVenId();
            //newE.getReturnVenName();
            newE.getReturnFestivalName();
        test.stopTest();       
    }
	*/

    static testMethod void CreateEvent_Venue(){
    
		JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;
        
        JVCO_Festival__c fest = new JVCO_Festival__c();
        fest.Name = 'Festival Test';
        fest.JVCO_Start_Date__c = system.today();
        fest.JVCO_End_Date__c = system.today().addDays(1);
        insert fest;
        
        JVCO_Artist__c artist = new JVCO_Artist__c();
        artist.Name = 'Artist Test';
        insert artist;
        
        JVCO_Event__c evnt = new JVCO_Event__c();
        evnt.JVCO_Venue__c = ven.id;
        evnt.JVCO_Festival__c = fest.id;
        evnt.JVCO_Artist__c = artist.id;
        evnt.JVCO_Event_Start_Date__c = system.today();
        evnt.JVCO_Event_End_Date__c = system.today().addDays(1);
        evnt.Name = '.';
        evnt.JVCO_Number_of_Set_Lists_Expected__c = 1;
        insert evnt;
        
        Apexpages.currentpage().getparameters().put('id', evnt.Id); 
        ApexPages.standardController controller = new ApexPages.standardController(evnt);
        JVCO_NewEventExtension NewEventExtension =  new JVCO_NewEventExtension(controller);
        
        test.startTest();
        PageReference CreateEvent = NewEventExtension.CreateEvent();
        test.stopTest();

        System.assertEquals(ApexPages.currentPage().getParameters().get('id'), evnt.Id);
    }

}