/* ----------------------------------------------------------------------------------------------
   Name: JVCO_ResetAmendmentQuoteTest
   Description: Unit test JVCO_ResetAmendmentQuote

   Date         Version     Author               Summary of Changes 
   -----------  -------     -----------------    -----------------------------------------
  05-Jan-2018   0.1      Robert John B. Lacatan   Intial creation
  ----------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_ResetAmendmentQuoteTest{

    @testSetup
    private static void setupData(){
    
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        PriceBook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;
      
        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;
        /*
        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        insert aff;
        */

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        insert opp;
        
        
        
        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, 'Annually');
        q.SBQQ__Type__c = 'Amendment';
        insert q;
        
        
        SBQQ__Quote__c q2 = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, 'Annually');
        //q2.SBQQ__Type__c = 'Amendment';
        insert q2;
                
        
        //SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];
        
        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(null, q.id, null, prod.Id);
        ql.SBQQ__Quantity__c = 1;
        insert ql;
        /*
        Contract cont = JVCO_TestClassObjectBuilder.createContract(acc2.id, q2.id);
        insert cont;
        
        SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(acc2.id, prod.id);
        s.SBQQ__ListPrice__c = 1;
        insert s;
        */
              
            
        List<JVCO_ResetAmendQuote__c> settings = new List<JVCO_ResetAmendQuote__c>();
        settings.add(new JVCO_ResetAmendQuote__c(Name = 'Option1', Value__c = 'Set amendment quote price to £0.00'));
        settings.add(new JVCO_ResetAmendQuote__c(Name = 'Option2', Value__c = 'Return amendment quote to original state. (This will reverse the above option if previously done)'));
        settings.add(new JVCO_ResetAmendQuote__c(Name = 'ErrorMessage', Value__c = 'This functionality is only available for Amendment Quotes.'));
        insert settings;
        

        
    }
    
    private static testMethod void testQuoteNotAmendment(){
       
        SBQQ__Quote__c q = [SELECT id, SBQQ__Type__c from SBQQ__Quote__c WHERE SBQQ__Type__c != 'Amendment' limit 1];
        
            Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(q);
            JVCO_ResetAmendmentQuote cont = new JVCO_ResetAmendmentQuote(sc);
            cont.checkQuote();
           
        
            Test.stopTest();
            
        
           ApexPages.Message[] messages = ApexPages.getMessages();
           System.assertEquals('This functionality is only available for Amendment Quotes.', messages[0].getDetail()); 
            
     }
     
     
     
     private static testMethod void testAmendmentQuote(){
     
        SBQQ__Quoteline__c ql = [SELECT id, SBQQ__Quantity__c, SBQQ__NetTotal__c, SBQQ__Quote__r.Id, SBQQ__Quote__r.SBQQ__Type__c, SBQQ__PriorQuantity__c  from SBQQ__Quoteline__c where SBQQ__Quote__r.SBQQ__Type__c = 'Amendment' limit 1];
        SBQQ__Quote__c q = new SBQQ__Quote__c(id = ql.SBQQ__Quote__r.Id, SBQQ__Type__c = ql.SBQQ__Quote__r.SBQQ__Type__c);
           
        Test.startTest();

        JVCO_ResetAmendmentQuote testCls = new JVCO_ResetAmendmentQuote();
        List<SelectOption> options = testCls.getItems();

        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        JVCO_ResetAmendmentQuote cont = new JVCO_ResetAmendmentQuote(sc);
        cont.checkQuote();
        cont.action = 'Execute';
        cont.updateQuoteLine(); 
        Test.stopTest();

        ql = [SELECT id, SBQQ__Quantity__c, SBQQ__NetTotal__c, SBQQ__Quote__r.Id, SBQQ__Quote__r.SBQQ__Type__c, SBQQ__PriorQuantity__c  from SBQQ__Quoteline__c where SBQQ__Quote__r.SBQQ__Type__c = 'Amendment' limit 1];
   
        System.assertEquals(1, ql.SBQQ__PriorQuantity__c);
     }

     private static testMethod void testAmendmentQuote2(){
     
        SBQQ__Quoteline__c ql = [SELECT id, SBQQ__NetTotal__c, SBQQ__Quote__r.Id, SBQQ__Quote__r.SBQQ__Type__c, SBQQ__PriorQuantity__c  from SBQQ__Quoteline__c where SBQQ__Quote__r.SBQQ__Type__c = 'Amendment' limit 1];
        SBQQ__Quote__c q = new SBQQ__Quote__c(id = ql.SBQQ__Quote__r.Id, SBQQ__Type__c = ql.SBQQ__Quote__r.SBQQ__Type__c);
           
        Test.startTest();

        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        JVCO_ResetAmendmentQuote cont = new JVCO_ResetAmendmentQuote(sc);
        cont.checkQuote();
        cont.action = 'NotExecute';
        cont.updateQuoteLine(); 
        Test.stopTest();
   
        System.assertEquals(null, ql.SBQQ__PriorQuantity__c);
     }
}