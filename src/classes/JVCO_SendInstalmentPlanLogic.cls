/* ----------------------------------------------------------------------------------------------
   Name: JVCO_SendInstalmentPlanLogic.cls 
   Description: Controller class for JVCO_SendInstalmentPlan

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
   21-Dec-2016  0.1         kristoffer.d.martin Intial creation
----------------------------------------------------------------------------------------------- */
public with sharing class JVCO_SendInstalmentPlanLogic {
	
	private ApexPages.StandardSetController standardController;

	public JVCO_SendInstalmentPlanLogic(ApexPages.StandardSetController standardController)  {

		this.standardController = standardController;	
		
	}
}