/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_UpdateAccountBalanceLogic.cls 
   Description:     Logic class for updating account balance during after update and after insert of Transaction Line Item record.
   Test class:      

   Date            Version     Author                             Summary of Changes 
   -----------     -------     -----------------                  -----------------------------------
   30-Jun-2017     0.1         recuerdo.b.bregente                Intial draft
  ------------------------------------------------------------------------------------------------ */
public class JVCO_UpdateAccountBalanceLogic
{
    public static void filterRecordsForAccountBalanceUpdate(List<c2g__codaTransactionLineItem__c> transLineItemList){
        Set<Id> accountIdsToUpdateBalance = new Set<Id>();
        
        for(c2g__codaTransactionLineItem__c tliRec : transLineItemList){
            if(tliRec.c2g__Account__c <> null && 'Account'.equals(tliRec.c2g__LineType__c) && 'Available'.equals(tliRec.c2g__MatchingStatus__c)){
                accountIdsToUpdateBalance.add(tliRec.c2g__Account__c);
            }
        }
        
        if(accountIdsToUpdateBalance.size() > 0){
            updateAccountBalance(accountIdsToUpdateBalance);
        }
    }
    
    public static void updateAccountBalance(Set<Id> accountIdsToUpdateBalance){
        Map<Id, Account> accountsMap = new Map<Id, Account>([SELECT Id, ffps_accbal__Account_Balance__c FROM Account WHERE Id IN : accountIdsToUpdateBalance]);
        for(AggregateResult agg : [SELECT c2g__Account__c accountId, SUM(c2g__AccountOutstandingValue__c) accountBalance FROM c2g__codaTransactionLineItem__c WHERE (c2g__Account__c IN :accountIdsToUpdateBalance) GROUP BY c2g__Account__c]){
            
            if(accountsMap.containsKey(String.valueOf(agg.get('accountId')))){
                accountsMap.get(String.valueOf(agg.get('accountId'))).ffps_accbal__Account_Balance__c = Decimal.valueOf(agg.get('accountBalance') + '');
            }
        }
        
        Database.update(accountsMap.values());
    }
	
	@future
    public static void futureUpdateAccountBalance(Set<Id> accountIdsToUpdateBalance){
        Map<Id, Account> accountsMap = new Map<Id, Account>([SELECT Id, ffps_accbal__Account_Balance__c FROM Account WHERE Id IN : accountIdsToUpdateBalance]);
        for(AggregateResult agg : [SELECT c2g__Account__c accountId, SUM(c2g__AccountOutstandingValue__c) accountBalance FROM c2g__codaTransactionLineItem__c 
        WHERE (c2g__Account__c IN :accountIdsToUpdateBalance AND c2g__LineType__c = 'Account' AND c2g__MatchingStatus__c = 'Available') 
        GROUP BY c2g__Account__c]){
            
            if(accountsMap.containsKey(String.valueOf(agg.get('accountId')))){
                accountsMap.get(String.valueOf(agg.get('accountId'))).ffps_accbal__Account_Balance__c = Decimal.valueOf(agg.get('accountBalance') + '');
            }
        }
        
        Database.update(accountsMap.values());
    }
}