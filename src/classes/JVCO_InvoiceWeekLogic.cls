/* ----------------------------------------------------------------------------------------------
    Name: JVCO_InvoiceWeekLogic.cls 
    Description:        
    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    30-Aug-2017  0.2         kristoffer.d.martin   Changed the logic on how the week is being calculated
----------------------------------------------------------------------------------------------- */
public class JVCO_InvoiceWeekLogic {

    public static Date setWeekWork(blng__Invoice__c blgInvoice){
        //DateTime currentDate = blgInvoice.blng__InvoiceDate__c; 
        DateTime currentDate = Date.today();
        System.debug('@Blng Invoice: '+blgInvoice);
        
        Integer numberOfDays = Date.daysInMonth(currentDate.year(), currentDate.month());
        Date lastDayOfMonth = Date.newInstance(currentDate.year(), currentDate.month(), numberOfDays);    

        if(blgInvoice.blng__InvoiceDate__c != null)
        {       
            ////If Weekend
            //if(currentDate.format('E').equalsIgnoreCase('Sat') <> currentDate.format('E').equalsIgnoreCase('Sun'))
            //{
            //    while(!currentDate.format('E').equalsIgnoreCase('Mon'))
            //    {
            //        currentDate = Date.newInstance(currentDate.year(), currentDate.month(), currentDate.day()).addDays(1);
            //    }
            //}
            //else
            //{
            //    while(!currentDate.format('E').equalsIgnoreCase('Fri'))
            //    {
            //        currentDate = Date.newInstance(currentDate.year(), currentDate.month(), currentDate.day()).addDays(1);
            //    }
            //}
            while(!currentDate.format('E').equalsIgnoreCase('Sun'))
            {
                currentDate = Date.newInstance(currentDate.year(), currentDate.month(), currentDate.day()).addDays(1);
                if (currentDate.isSameDay(lastDayOfMonth)) {
                    break;
                }
            }
            // GREEN-22011 - kristoffer.d.martin - 30/08/2017 - END
        }    

        return Date.newInstance(currentDate.year(), currentDate.month(), currentDate.day());                 
    }
}