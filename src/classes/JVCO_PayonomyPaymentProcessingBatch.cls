/* ----------------------------------------------------------------------------------------------
    Name: JVCO_PayonomyPaymentProcessingBatch.cls 
    Description: Batch class that handles Cash Entry and Cash Entry Line Item creation of Payments

    Date         Version     Author              Summary of Changes 

    -----------  -------     -----------------   -----------------------------------------
    23-Dec-2016   0.1         ryan.i.r.limlingan  Intial creation
    30-Jan-2017   0.2         ryan.i.r.limlingan  Added Database.Stateful; and Cash Entry record
                                                creation and posting
    16-Jun-2017   0.3         Mel Andrei Santos   Added JVCO_ContractRenewQuotesBatch constructor to be used for scheduler and  Added call of next batch in the finish method as per Jira Ticket GREEN-17932
    04-Aug-2017   0.4         mel.andrei.b.santos Changed DML statement to database.insert as per Ticket GREEN-17932 - Mel Andrei Santos 03-08-2017
    22-Aug-2017   0.5         mel.andrei.b.santos Added try catch method for error handling in inserting error log records
    24-Aug-2017   0.6         mel.andrei.b.santos refactored inputting of fields
    14-Sep-2017   0.9         mel.andrei.b.Santos removed the if conditions as the null check in the error logs as per James
    29-Sep-2017   1.0         franz.g.a.dimaapi   Apply Matching Queueable after posting of Cash Entries
    09-Feb-2018   1.1         franz.g.a.dimaapi   Refactor Whole Class
    20-Apr-2018   1.2         mary.ann.a.ruelan   GREEN-31487. Transferred creating cash entry to payment shared logic to make sure no cash entries
                                                  were created without cash entry lines
    18-Jul-2018   1.3         franz.g.a.dimaapi   GREEN-32622 - Refactor Class 
    21-Aug-2018   1.4         mel.andrei.b.Santos GREEN-33186 - Updated class to orchestrate run for SagePay and Direct Debit
----------------------------------------------------------------------------------------------- */
public class JVCO_PayonomyPaymentProcessingBatch /*implements Database.Batchable<sObject>, Database.Stateful*/
{
    /*private static final Id sagepayRecTypeId = Schema.SObjectType.PAYBASE2__Payment__c.getRecordTypeInfosByName().get('SagePay').getRecordTypeId();
    private static final Id ddRecTypeId = Schema.SObjectType.PAYBASE2__Payment__c.getRecordTypeInfosByName().get('Direct Debit Collection').getRecordTypeId();

    private Set<Id> cashEntryIdSet = new Set<Id>();
    private Integer accumulatedAmount;
    private String recordType;
    private String paymentStatus;
    Boolean forSagePay = false; //21-Aug-2018  mel.andrei.b.Santos GREEN-33186

    public JVCO_PayonomyPaymentProcessingBatch(){}

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Constructor that initializes instance variables
    Inputs: String
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    30-Jan-2017 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */  
    /*public JVCO_PayonomyPaymentProcessingBatch(String recordType)
    {
        this.recordType = recordType;
        paymentStatus = (recordType.equals('SagePay')) ? 'AUTHORISED' : 'Submitted';
    }

    //start 21-Aug-2018 mel.andrei.b.Santos GREEN-33186
    public JVCO_PayonomyPaymentProcessingBatch(String recordType, Set<Id> cashEntryIdSet)
    {
        this.recordType = recordType;
        paymentStatus = (recordType.equals('SagePay')) ? 'AUTHORISED' : 'Submitted';
        this.cashEntryIdSet = cashEntryIdSet;
        system.debug('@@@ check Cash Entry ID Set for Direct Debit' + this.cashEntryIdSet);
    }
    //END
  
    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Queries Payment records
        Inputs: Database.BatchableContext
        Returns: Database.QueryLocator
        <Date>      <Authors Name>      <Brief Description of Change> 
        23-Dec-2016 ryan.i.r.limlingan  Initial version of function
        03-Feb-2017 ryan.i.r.limlingan  Narrowed down the query
    ------------------------------------------------------------------------------------------------ */  
    /*public Database.QueryLocator start(Database.BatchableContext BC)
    {
        //Credit Card
        if(recordType == 'SagePay')
        {
            forSagePay = true;  //21-Aug-2018  mel.andrei.b.Santos GREEN-33186
            // -- For Credit Card payments --
            // (1) Payment Status is AUTHORISED
            // (2) RecordType is SagePay
            // (3) Payment Description is not null and starts with GIN
            // (4) No existing Cash Entry Line Items
            return Database.getQueryLocator([SELECT Id, PAYBASE2__Pay_Date__c,
                                            JVCO_FF_Bank_Account_Name__c
                                            FROM PAYBASE2__Payment__c
                                            WHERE PAYBASE2__Status__c = :paymentStatus
                                            AND RecordTypeId = :sagepayRecTypeId
                                            AND PAYCP2__Payment_Description__c != null
                                            AND PAYCP2__Payment_Description__c LIKE 'GIN%'
                                            AND Id NOT IN 
                                                (SELECT JVCO_Payonomy_Payment__c
                                                FROM c2g__codaCashEntryLineItem__c
                                                WHERE JVCO_Payonomy_Payment__c != null)]);
        //Direct Debit
        }else if(recordType == 'Direct Debit Collection')
        {
            forSagePay = false; //21-Aug-2018  mel.andrei.b.Santos GREEN-33186
            // -- For Direct Debit --
            // (1) Payment STatus is Submitted
            // (2) RecordType is Direct Debit Collection
            // (3) Payment Description is not null and starts with GIN
            // (4) No existing Cash Entry Line Items
            return Database.getQueryLocator([SELECT Id, PAYBASE2__Pay_Date__c,
                                            JVCO_FF_Bank_Account_Name__c
                                            FROM PAYBASE2__Payment__c
                                            WHERE PAYBASE2__Status__c = :paymentStatus
                                            AND RecordTypeId = :ddRecTypeId
                                            AND PAYREC2__Payment_Agreement__r.PAYREC2__Description__c != null
                                            AND PAYREC2__Payment_Agreement__r.PAYREC2__Description__c LIKE 'GIN%'
                                            AND PAYFISH3__Type__c = 'Collection'
                                            AND (NOT CreatedBy.Name LIKE 'Data Migration%') 
                                            AND Id NOT IN 
                                                (SELECT JVCO_Payonomy_Payment__c
                                                FROM c2g__codaCashEntryLineItem__c
                                                WHERE JVCO_Payonomy_Payment__c != null)]);
        //Process all record payments if no rType - 16/06/2017 - Mel Andrei Santos    
        }else
        {
            return null;
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Creates a single Cash Entry record, and calls SharedLogic to process the payments
        Inputs: Database.BatchableContext, List<PAYBASE2__Payment__c>
        Returns: N/A
        <Date>      <Authors Name>      <Brief Description of Change> 
        23-Dec-2016 ryan.i.r.limlingan  Initial version of function
        30-Jan-2017 ryan.i.r.limlingan  Added Cash Entry creation
        28-Feb-2017 ryan.i.r.limlingan  Added variable to store payment method
        22-Aug-2017 mel.andrei.b.santos Added try catch method for error handling in inserting error log records
        20-Apr-2018 mary.ann.a.ruelan   updated method to create cash entries in processPayment. GREEN-31487
    ----------------------------------------------------------------------------------------------- */
    /*public void execute(Database.BatchableContext BC, List<PAYBASE2__Payment__c> scope) 
    {
        if(scope != null && !scope.isEmpty())
        {
            if(JVCO_PaymentSharedLogic.processPayment(recordType, scope))
            {                
                cashEntryIdSet.add(JVCO_PaymentSharedLogic.cashEntry.Id);
            }
        }
    }
  
    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Updates and posts the Cash Entry record
        Inputs: Database.BatchableContext
        Returns: N/A
        <Date>      <Authors Name>      <Brief Description of Change> 
        23-Dec-2016 ryan.i.r.limlingan  Initial version of function
        30-Jan-2017 ryan.i.r.limlingan  Added posting of Cash Entry
        29-Sep-2017 franz.g.a.dimaapi   Remove Matching Logic after Posting and changed it to run 
                                        Matching Queueable after Posting - GREEN-23428
        14-Dec-2017 jasper.j.figueroa   Changed the execute scope value to 1 - GREEN-26593
        21-Aug-2018 mel.andrei.b.Santos GREEN-33186 - Updated class to orchestrate run for SagePay and Direct Debit
    ----------------------------------------------------------------------------------------------- */
    /*public void finish(Database.BatchableContext BC)
    {
        //start 21-Aug-2018 mel.andrei.b.Santos GREEN-33186
        JVCO_General_Settings__c GENERAL_SETTINGS = JVCO_General_Settings__c.getInstance();
        Decimal PROCESSINGLIMIT = GENERAL_SETTINGS != null ? GENERAL_SETTINGS.JVCO_PayonomyPaymentProcessingBatchLimit__c : 20;

        if(forSagePay)
        {
            if(!cashEntryIdSet.isEmpty())
            {
                id batchjobidDD = Database.executebatch(new JVCO_PayonomyPaymentProcessingBatch('Direct Debit Collection',cashEntryIdSet), Integer.valueOf(PROCESSINGLIMIT));
            }
            else
            {
                id batchjobidDD = Database.executebatch(new JVCO_PayonomyPaymentProcessingBatch('Direct Debit Collection'), Integer.valueOf(PROCESSINGLIMIT));
            }
            
        }

        else
        {
            Integer paymentProcessingScope = (Integer)JVCO_General_Settings__c.getOrgDefaults().JVCO_Cash_Post_Queue_Rec_Per_Jobs__c; //GREEN-26593

            if(!cashEntryIdSet.isEmpty())
            {
                Database.executeBatch(new JVCO_CashEntriesPostingLogicBatch(cashEntryIdSet, true), paymentProcessingScope);          
            }   
        }

        //END
        
    }*/
}