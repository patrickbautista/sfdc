/* ----------------------------------------------------------------------------------------------
Name: JVCO_SmarterPayPaymentBatch.cls 
Description: Batch class that handles Cash Entry and Cash Entry Line Item creation of Payments

Date         Version     Author              Summary of Changes 

-----------  -------     -----------------   -----------------------------------------
27-Mar-2020   0.1         patrick.t.bautista  Intial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_SmarterPayPaymentBatch implements Database.Batchable<sObject>, Database.Stateful
{
    private String paymentStatus = '';
    private String paymentProcess = '';
    private Set<Id> cashEntryIdSet = new Set<Id>();
    public Boolean forSagePay = false; //21-Aug-2018  mel.andrei.b.Santos GREEN-33186
    public JVCO_SmarterPayPaymentBatch(){}
    
    public JVCO_SmarterPayPaymentBatch(String paymentProcess)
    {
        this.paymentProcess = paymentProcess;
        this.paymentStatus = (paymentProcess.equals('Direct Debit')) ? 'Submitted' : 'Authorised';
    }
    
    public JVCO_SmarterPayPaymentBatch(String paymentProcess, Set<Id> cashEntryIdSet)
    {
        this.paymentProcess = paymentProcess;
        this.paymentStatus = (paymentProcess.equals('Direct Debit')) ? 'Submitted' : 'Authorised';
        this.cashEntryIdSet = cashEntryIdSet;
        system.debug('@@@ check Cash Entry ID Set for Direct Debit' + this.cashEntryIdSet);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        //Direct Debit
        if(paymentProcess == 'Direct Debit')
        {
            forSagePay = false;
            //For Direct Debit History
            return Database.getQueryLocator([SELECT Id, Income_Direct_Debit__c, 
                                             Income_Direct_Debit__r.Account__c,
                                             Amount__c, DD_Status__c,
                                             DD_Collection_Date__c,
                                             DD_Code__c
                                             FROM Income_Debit_History__c 
                                             WHERE ((DD_Stage__c = : paymentStatus AND Amount__c > 0)
                                                    OR (DD_Stage__c = 'Successful' AND Amount__c < 0))
                                             AND Income_Direct_Debit__c != null
                                             AND Id NOT IN 
                                             (SELECT JVCO_Income_Debit_History__c
                                              FROM c2g__codaCashEntryLineItem__c
                                              WHERE JVCO_Income_Debit_History__c != null)]);
            
        //Credit Card
        }else if(paymentProcess == 'Credit Card')
        {
            forSagePay = true;
            return Database.getQueryLocator([SELECT id, Amount__c, 
                                             Income_Card_Payment__r.Account__c,
                                             Income_Card_Payment__c,
                                             Authorisation_Date__c,
                                             Sagepay_Transaction_ID__c
                                             FROM Income_Card_Payment_History__c
                                             WHERE Payment_Status__c = : paymentStatus
                                             AND Amount__c > 0
                                             AND Income_Card_Payment__c != null
                                             AND Id NOT IN 
                                             (SELECT JVCO_Income_Card_Payment_History__c
                                              FROM c2g__codaCashEntryLineItem__c
                                              WHERE JVCO_Income_Card_Payment_History__c != null)]);
        }else
        {
            return null;
        }
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope) 
    {
        if(scope != null && !scope.isEmpty() && JVCO_SmarterPayPaymentLogic.processPayment(paymentProcess, scope))
        {                
            cashEntryIdSet.add(JVCO_SmarterPayPaymentLogic.cashEntry.Id);
            cashEntryIdSet.addAll(JVCO_SmarterPayPaymentLogic.refundCashSet);
        }
    }
    
    public void finish(Database.BatchableContext BC)
    {
        //start 21-Aug-2018 mel.andrei.b.Santos GREEN-33186
        JVCO_General_Settings__c GENERAL_SETTINGS = JVCO_General_Settings__c.getInstance();
        Decimal PROCESSINGLIMIT = GENERAL_SETTINGS != null ? GENERAL_SETTINGS.JVCO_PayonomyPaymentProcessingBatchLimit__c : 20;

        if(forSagePay)
        {
            if(!cashEntryIdSet.isEmpty()){
                id batchjobidDD = Database.executebatch(new JVCO_SmarterPayPaymentBatch('Direct Debit',cashEntryIdSet), Integer.valueOf(PROCESSINGLIMIT));
            }
            else{
                id batchjobidDD = Database.executebatch(new JVCO_SmarterPayPaymentBatch('Direct Debit'), Integer.valueOf(PROCESSINGLIMIT));
            }
        }
        else{
            Integer paymentProcessingScope = (Integer)JVCO_General_Settings__c.getOrgDefaults().JVCO_Cash_Post_Queue_Rec_Per_Jobs__c; //GREEN-26593
            if(!cashEntryIdSet.isEmpty()){
                Database.executeBatch(new JVCO_CashEntriesPostingLogicBatch(true, cashEntryIdSet), paymentProcessingScope);          
            }   
        }
    }
}