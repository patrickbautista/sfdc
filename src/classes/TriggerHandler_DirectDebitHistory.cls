public with sharing class TriggerHandler_DirectDebitHistory {

  public static void OnBeforeInsert(List<Income_Debit_History__c> newRecords){
        
    }

    public static void OnAfterInsert(Map<Id, Income_Debit_History__c> newRecords){
        Map<Id, Income_Direct_Debit__c> parentDirectDebits = SMP_DirectDebitHelper.getParentDirectDebitsFromHistories(newRecords.values());
        List<Income_Debit_History__c> failureHistories = SMP_DirectDebitHelper.getHistoriesByReasonCode(newRecords.values(), null);
        List<Income_Debit_History__c> successfulNewInstructions = SMP_DirectDebitHelper.getNewInstructionsByStage(newRecords.values(), null, 'Submitted');
        System.debug('####ni' + successfulNewInstructions);
        List<Income_Direct_Debit__c> directDebitsToRecalculate = new List<Income_Direct_Debit__c>();
        for(Income_Debit_History__c ddh : successfulNewInstructions){
            if(ddh.Income_Direct_Debit__c != null){
                System.debug('####parentDirectDebits.get(ddh.Income_Direct_Debit__c)' + parentDirectDebits.get(ddh.Income_Direct_Debit__c));

                if(ddh.DD_Code__c != null){
                    if(ddh.DD_Code__c.toLowerCase() == 'addacs-c' || ddh.DD_Code__c.toLowerCase() == 'auddis-c'){
                        parentDirectDebits.get(ddh.Income_Direct_Debit__c).Send_DD_Documentation__c = 'Direct Debit Bank Amend';
                    }
                }
                if(ddh.DD_Status__c == 'New Instruction')
                {
                    System.debug('####HERE!!!');
                    parentDirectDebits.get(ddh.Income_Direct_Debit__c).Send_DD_Documentation__c = 'Direct Debit Confirmation';

                }


                directDebitsToRecalculate.add(parentDirectDebits.get(ddh.Income_Direct_Debit__c));
            }       
        }
        System.debug('####directDebitsToRecalculate' + directDebitsToRecalculate);
        
        if(directDebitsToRecalculate.Size() > 0){

            update directDebitsToRecalculate;

            update SMP_PaymentScheduleHelper.checkDirectDebit(directDebitsToRecalculate);
        }
        JVCO_SmarterPayFailureLogic.processAUDDISandADDACSCancellation(failureHistories);
    }

    public static void OnBeforeUpdate(Map<Id, Income_Debit_History__c> newRecords, Map<Id, Income_Debit_History__c> oldRecords){
    System.debug('#### newRecords: ' + newRecords);
    System.debug('#### oldRecords: ' + oldRecords);
    }
  
    public static void OnAfterUpdate(Map<Id, Income_Debit_History__c> newRecords, Map<Id, Income_Debit_History__c> oldRecords){
        Map<Id, Income_Direct_Debit__c> parentDirectDebits = new Map<Id, Income_Direct_Debit__c>([SELECT Id, Name, DD_Last_Collected_Date__c, DD_Ongoing_Collection_Amount__c,
                                                                DD_Account_Number_OK__c, DD_Sort_Code_OK__c, DD_Bank_Name__c, DD_Branch_Name__c,
                                                                DD_Bank_Account_Name__c, DD_Bank_Account_Number__c, DD_Bank_Sort_Code__c,
                                                                (SELECT Id, Invoice_Group__c
                                                                FROM Direct_Debit_Group_Invoice_s__r LIMIT 1) 
                                                                FROM Income_Direct_Debit__c WHERE Id 
                                                                IN :SMP_DirectDebitHelper.getParentDirectDebitsFromHistories(newRecords.values()).keySet()]);
        
        //sList<Income_Debit_History__c> successfulNewInstructions = SMP_DirectDebitHelper.getNewInstructionsByStage(newRecords.values(), oldRecords, 'Submitted');
        //System.debug('####' + successfulNewInstructions);
        List<Income_Debit_History__c> successfulHistories = SMP_DirectDebitHelper.getCollectionsByStage(newRecords.values(), oldRecords, 'Successful');
        List<Income_Debit_History__c> failedHistories = SMP_DirectDebitHelper.getCollectionsByStage(newRecords.values(), oldRecords, 'Failed');

        List<Income_Direct_Debit__c> directDebitsToRecalculate = new List<Income_Direct_Debit__c>();
        //List<c2g__codaInvoiceInstallmentLineItem__c> installmentLineItemsToUpdate = new List<c2g__codaInvoiceInstallmentLineItem__c>();

        for(Income_Debit_History__c ddh : successfulHistories){
            if(ddh.Income_Direct_Debit__c != null){
                if(ddh.DD_Code__c != null){
                    if(ddh.DD_Code__c.toLowerCase() == 'addacs-c' || ddh.DD_Code__c.toLowerCase() == 'auddis-c'){
                        parentDirectDebits.get(ddh.Income_Direct_Debit__c).Send_DD_Documentation__c = 'Direct Debit Bank Amend';
                    }
                }
                directDebitsToRecalculate.add(parentDirectDebits.get(ddh.Income_Direct_Debit__c));
            }       
        }

        // for(Income_Debit_History__c ddh : successfulNewInstructions){
        //     if(ddh.Income_Direct_Debit__c != null){
        //         if(ddh.DD_Code__c != null){
        //             if(ddh.DD_Code__c.toLowerCase() == 'addacs-c' || ddh.DD_Code__c.toLowerCase() == 'auddis-c'){
        //                 parentDirectDebits.get(ddh.Income_Direct_Debit__c).Send_DD_Documentation__c = 'Direct Debit Bank Amend';
        //             }
        //         }
        //         directDebitsToRecalculate.add(parentDirectDebits.get(ddh.Income_Direct_Debit__c));
        //     }       
        // }
    
        JVCO_SmarterPayFailureLogic.processPaymentFailure(failedHistories, true);
        
        /*for(Income_Debit_History__c ddh : successfulHistories){
            installmentLineItemsToUpdate.addAll(SMP_PaymentScheduleHelper.processSuccessfulDDHistories(ddh));   
        }
    
        for(Income_Debit_History__c ddh : failedHistories){
            installmentLineItemsToUpdate.addAll(SMP_PaymentScheduleHelper.processFailedDDHistories(ddh));
        }*/

        if(directDebitsToRecalculate.Size() > 0){
            update SMP_PaymentScheduleHelper.checkDirectDebit(directDebitsToRecalculate);
        }

        /*if(installmentLineItemsToUpdate.Size() > 0){
            upsert installmentLineItemsToUpdate;
        } */       

        if(failedHistories.Size() > 0){
            //MemberNotificationHelper.handleFailedDirectDebitPayment(failedHistories, parentDirectDebits);
        }
  }
}