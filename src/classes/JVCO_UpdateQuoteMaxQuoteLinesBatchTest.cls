@isTest
private class JVCO_UpdateQuoteMaxQuoteLinesBatchTest
{
	@testSetup static void setTestData(){
		
		JVCO_TestClassObjectBuilder.createBillingConfig();

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
    
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
            listQueue.add(q2);
            queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
            listQueue.add(q3);
            queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
            listQueue.add(q4);
            queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
            listQueue.add(q5);
            queuesobject q6 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankAccount__c'); 
            listQueue.add(q6);
            queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q7);
            queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatement__c'); 
            listQueue.add(q8);
            queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransaction__c'); 
            listQueue.add(q9);
            queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCreditNote__c'); 
            listQueue.add(q10);
            //queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntry__c'); 
            //listQueue.add(q9);
            //queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntryLineItem__c'); 
            //listQueue.add(q10);
            //queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatementLineItem__c'); 
            //listQueue.add(q10);
            //queuesobject q12 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatementLineItem__c'); 
            //listQueue.add(q12);           
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }

        c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
        testCompany.c2g__CashMatchingCurrencyMode__c = 'Account';
        testCompany.c2g__Country__c = 'GB';
        testCompany.c2g__ECCountryCode__c = 'GB';
        testCompany.c2g__UnitOfWork__c = 1.0;
        testCompany.Name = 'Test Company';
        testCompany.ownerid = testGroup.Id;
        insert testCompany;

        /*c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
        testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc.Dimension_1_Required__c = false;
        testGeneralLedgerAcc.Dimension_2_Required__c = false;
        testGeneralLedgerAcc.Dimension_3_Required__c = false;
        testGeneralLedgerAcc.Dimension_4_Required__c = false;
        testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
        testGeneralLedgerAcc.ownerid = testGroup.Id;*/

        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc10040 = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc10040.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc10040.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc10040.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc10040.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc10040.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc10040.c2g__ReportingCode__c = '10040';
        testGeneralLedgerAcc10040.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc10040.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc10040.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc10040.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc10040.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc10040.Dimension_1_Required__c = false;
        testGeneralLedgerAcc10040.Dimension_2_Required__c = false;
        testGeneralLedgerAcc10040.Dimension_3_Required__c = false;
        testGeneralLedgerAcc10040.Dimension_4_Required__c = false;
        testGeneralLedgerAcc10040.Name = '10040 - Accounts receivables control';
        testGeneralLedgerAcc10040.ownerid = testGroup.Id;
        insert testGeneralLedgerAcc10040;

        //Create General Ledger Account for Income Control
        c2g__codaGeneralLedgerAccount__c testIcGla = new c2g__codaGeneralLedgerAccount__c();
        testIcGla.c2g__ReportingCode__c = '30030';
        testIcGla.c2g__TrialBalance1__c = 'Liabilities';
        testIcGla.c2g__TrialBalance2__c = 'Income Control';
        testIcGla.c2g__TrialBalance3__c = 'Short-term income control account';
        testIcGla.c2g__Type__c = 'Balance Sheet';
        testIcGla.Name = '30030 - Income Control';
        testIcGla.ownerid = testGroup.Id;
        insert testIcGla;

        c2g__codaGeneralLedgerAccount__c testGeneralLedger = JVCO_TestClassObjectBuilder.createGeneralLedger();
        insert testGeneralLedger;

        c2g__codaTaxCode__c testTaxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedger.Id);
        insert testTaxCode;

        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        insert dt;
		
		Test.startTest();
        c2g__codaTaxRate__c testTaxRate = new c2g__codaTaxRate__c();
        testTaxRate.c2g__Rate__c = 20.0;
        testTaxRate.c2g__UnitOfWork__c = 1.0;
        testTaxRate.c2g__TaxCode__c = testTaxCode.id;
        insert testTaxRate;

        c2g__codaCashMatchingSettings__c settings = new c2g__codaCashMatchingSettings__c();
        settings.JVCO_Write_off_Limit__c = 1.00;
        insert settings;

        c2g__codaDimension2__c testDimension2 = new c2g__codaDimension2__c();
        testDimension2.name = 'Paid at PPL';
        testDimension2.c2g__ReportingCode__c = 'Paid at PPL';
        testDimension2.c2g__UnitOfWork__c = 1.0;
        insert testDimension2;

        c2g__codaDimension2__c testDimension3 = new c2g__codaDimension2__c();
        testDimension3.name = 'Non Key';
        testDimension3.c2g__ReportingCode__c = 'Non Key';
        testDimension3.c2g__UnitOfWork__c = 2.0;
        insert testDimension3;

        c2g__codaAccountingCurrency__c accCurrency = new c2g__codaAccountingCurrency__c();
        accCurrency.c2g__OwnerCompany__c = testCompany.id;
        accCurrency.c2g__DecimalPlaces__c = 2;
        accCurrency.Name = 'GBP';
        accCurrency.c2g__Dual__c = true ;
        accCurrency.ownerid = testGroup.Id;
        accCurrency.c2g__Home__c = true;
        accCurrency.c2g__UnitOfWork__c = 2.0;
        insert accCurrency;

        Product2 prod1 = new Product2();
        prod1.Name = 'PPL Product';
        prod1.SBQQ__ChargeType__c= 'One-Time';
        prod1.JVCO_Society__c = 'PPL';
        prod1.ProductCode = 'PPLPP085';
        prod1.IsActive = true;
        prod1.c2g__CODASalesRevenueAccount__c = testIcGla.Id;
        insert prod1;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 83202-C';
        a1.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc10040.id;
        a1.Type = 'Key Account';
        insert a1;

        //Create Contact
        Contact testContact = JVCO_TestClassObjectBuilder.createContact(a1.Id);
        testContact.Firstname = 'Test';
        testContact.LastName = 'TestContact';
        insert testContact;
        
        Account a2 = new Account();
        a2.RecordTypeId = licenceRT;
        a2.Name = 'Test Account 83202-L';

        a2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
        a2.JVCO_Customer_Account__c = a1.id;
        a2.c2g__CODAOutputVATCode__c = testTaxCode.id;
        a2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc10040.id;
        //a2.JVCO_Customer_Account__c = a1.Id;
        a2.JVCO_Preferred_Contact_Method__c = 'Email';
        a2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        a2.JVCO_Billing_Contact__c = testContact.id;
        a2.JVCO_Licence_Holder__c = testContact.id;
        a2.JVCO_Review_Contact__c = testContact.id;
        a2.Type = 'Key Account';
        insert a2;

        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();
        venueRecord.Name = 'test1';              
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = new JVCO_Affiliation__c();
        affilRecord.JVCO_Account__c = a2.Id;
        affilRecord.JVCO_Venue__c = venueRecord.id; 
        affilRecord.JVCO_Start_Date__c = system.today();     
        Insert  affilRecord;
        
        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 83202';
        o.AccountId = a2.Id;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o; 

        Pricebook2 testPB2 = new Pricebook2();
        testPB2.name = 'Standard Price Book';
        testPB2.IsActive = true;
        insert testPB2;        

        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Renewal';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        insert q;

        //SBQQ__QuoteLineGroup__c testQLG = [select id, JVCO_Affiliated_Venue__c, JVCO_Venue__c from SBQQ__QuoteLineGroup__c order by CreatedDate DESC limit 1];

        Contract contr = new Contract();
        contr.AccountId = a2.Id;
        contr.Status = 'Draft';
        contr.StartDate = Date.today();
        contr.ContractTerm = 12;
        contr.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
        contr.SBQQ__RenewalQuoted__c = FALSE;
        contr.SBQQ__Quote__c = q.id;
        contr.SBQQ__Opportunity__c = o.id;
        insert contr;
                
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        ql.SBQQ__ChargeType__c= 'One-Time';
        insert ql;

        PriceBookEntry testPB = new PriceBookEntry();
        testPB.Pricebook2Id = test.getStandardPriceBookId();
        testPB.product2id = prod1.id;
        testPB.IsActive = true;
        testPB.UnitPrice = 1000.0;
        insert testPB;

        Order testOrder = new Order();
        testorder.Accountid = a2.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = q.id;
        testOrder.blng__BillingAccount__c = a2.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        insert testOrder;  

        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 100.0;
        testOrderItem.UnitPrice = 10000.00;
        testOrderItem.EndDate = date.today().addMonths(5);
        //testOrderItem.blng__OverrideNextBillingDate__c = null;
        //testOrderItem.blng__BillThroughDateOverride__c = null;
        insert testOrderItem;
	
        //system.assertEquals(0.00, testOrderItem.SBQQ__TotalAmount__c,'Error');

        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
        testOrderItem.SBQQ__TotalAmount__c = 1000;
        update testOrderItem;
		Test.stopTest();
		
        c2g__codaYear__c yr= new c2g__codaYear__c();
        yr.Name ='2016';
        yr.c2g__AutomaticPeriodList__c =  true;
        yr.c2g__OwnerCompany__c = testCompany.id;
        yr.c2g__ExternalId__c = 'yzsd1234';
        yr.c2g__NumberOfPeriods__c =11;
        yr.c2g__StartDate__c =  system.today() - 10;
        yr.c2g__Status__c = 'Open';
        yr.c2g__PeriodCalculationBasis__c = '445';
        yr.c2g__YearEndMode__c = 'Full Accounting Code' ; 
        yr.c2g__UnitOfWork__c = 12;
        yr.ownerid = testGroup.Id;
        insert yr;

        c2g__codaPeriod__c testPeriod = new c2g__codaPeriod__c();
        testPeriod.c2g__OwnerCompany__c = testCompany.id;
        testPeriod.c2g__StartDate__c = System.today()-10;
        testPeriod.c2g__EndDate__c = System.today()+10;
        testPeriod.c2g__PeriodNumber__c ='123';
        testPeriod.c2g__Description__c ='test Desc';
        testPeriod.c2g__PeriodGroup__c = 'Q1';
        testPeriod.c2g__PeriodNumber__c = '1';
        testPeriod.c2g__YearName__c = yr.id;
        insert testPeriod;

        c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c();
        userCompany.c2g__Company__c = testCompany.id;
        userCompany.c2g__User__c = userInfo.getUserId();
        userCompany.c2g__ExternalId__c = 'ABCDE1234567876';
        userCompany.c2g__UnitOfWork__c = 111 ;
        insert userCompany;


    }

	@isTest
	static void itShould()
	{
		JVCO_UpdateQuoteMaxQuoteLinesBatch obj = new JVCO_UpdateQuoteMaxQuoteLinesBatch();
		Database.executebatch(obj);

	}
}