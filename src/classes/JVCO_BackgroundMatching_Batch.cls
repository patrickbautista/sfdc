/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_BackgroundMatching_Batch.cls 
   Description:     Batch Class for JVCO_BackgroundMatchingLogic.cls
   Test class:      

   Date            Version     Author                             Summary of Changes 
   -----------     -------     -----------------                  -----------------------------------
   26-Jul-2017     0.2         franz.g.a.dimaapi@accenture.com    Updated The Logic to be use in 
                                                                  Trigger Handler
   06-Dec-2017     0.3         csaba.feher@accenture.com          Setting isFromBatchProcess flag for GREEN-24996          
   23-Apr-2018     0.4         franz.g.a.dimaapi                  Updated Batch criteria for efficiency - GREEN-31602
   18-Jul-2018     0.5         franz.g.a.dimaapi                  GREEN-32622 - Refactor Class
   05-Oct-2018     0.6         franz.g.a.dimaapi                  GREEN-32854 - Updated Criteria for efficiency
   11-June-2019    0.7          mel.andrei.b.Santos               GREEN-34694 - updated finish method to call new bg matching batch
  ------------------------------------------------------------------------------------------------ */
public class JVCO_BackgroundMatching_Batch implements Database.Batchable<sObject>, Database.Stateful
{    
    public Map<Id, Id> licAndCustAccIdMap = new Map<Id, Id>();
    
    public JVCO_BackgroundMatching_Batch(){}

    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator([SELECT Id, Name, c2g__AccountOutstandingValue__c, c2g__LineType__c,
                                         c2g__MatchingStatus__c,
                                         c2g__Account__c, c2g__LineReference__c,
                                         c2g__Account__r.JVCO_Customer_Account__c,
                                         c2g__OwnerCompany__r.c2g__CustomerWriteOff__c,
                                         c2g__Transaction__r.c2g__Period__c,
                                         c2g__Transaction__r.c2g__Period__r.c2g__Closed__c,
                                         c2g__Transaction__r.c2g__TransactionType__c,
                                         c2g__Transaction__r.c2g__DocumentDescription__c,
                                         c2g__Transaction__r.c2g__CashEntry__r.c2g__Type__c,
                                         c2g__Transaction__r.c2g__SalesInvoice__c,
                                         c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Invoice_Legacy_Number__c,
                                         c2g__Transaction__r.c2g__SalesCreditNote__c,
                                         c2g__Transaction__r.c2g__SalesCreditNote__r.c2g__Invoice__c,
                                         c2g__Transaction__r.c2g__SalesCreditNote__r.c2g__Invoice__r.Name,
                                         c2g__Transaction__r.c2g__Journal__r.c2g__Type__c,
                                         c2g__Transaction__r.c2g__CashEntry__r.c2g__BankAccount__r.c2g__GeneralLedgerAccount__r.Name,
                                         c2g__LineDescription__c 
                                         FROM c2g__codaTransactionLineItem__c
                                         WHERE c2g__MatchingStatus__c = 'Available'
                                         AND c2g__LineType__c = 'Account'
                                         AND c2g__LineDescription__c != 'Not for Matching'
                                         AND c2g__AccountOutstandingValue__c != 0
                                         AND c2g__Transaction__r.c2g__TransactionType__c IN ('Cash')
                                         AND c2g__Transaction__r.c2g__CashEntry__r.c2g__Type__c IN ('Receipt')
                                         AND c2g__Account__r.JVCO_In_Infringement__c = FALSE
                                         AND c2g__Account__r.JVCO_In_Enforcement__c = FALSE
                                         AND c2g__Account__r.JVCO_Exclude_From_Background_Matching__c = FALSE]); 
    }
    
    public void execute(Database.BatchableContext bc, List<c2g__codaTransactionLineItem__c> scope)
    {
        JVCO_BackgroundMatchingLogic.matchingLogic(new Map<Id, c2g__codaTransactionLineItem__c>(scope), true);
        getAccountToRunQueable(JVCO_BackgroundMatchingLogic.licAndCustAccIdMap);
    }
    
    public void getAccountToRunQueable(Map<Id, Id> accountMapFromBgMatching){
        for(Id licAcc : accountMapFromBgMatching.keySet())
        {
            if(!licAndCustAccIdMap.containsKey(licAcc)){
                licAndCustAccIdMap.put(licAcc, accountMapFromBgMatching.get(licAcc));
            }
        }
    }
    
    public void finish(Database.BatchableContext bc)
    {
        JVCO_CreditNoteBackgroundMatchingBatch cnbgm = new JVCO_CreditNoteBackgroundMatchingBatch();
        cnbgm.licAndCustAccIdMap = licAndCustAccIdMap;
        Database.executeBatch(cnbgm, 1);
    }
}