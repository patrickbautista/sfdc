/*----------------------------------------------------------------------------------------------
Name: JVCO_DCA2StageBatchScheduler.cls 
Description: Scheduler to execute DCA 2nd Stage Batch - JVCO_DCA2StageBatchProcess
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
15-May-2019  0.1         patrick.t.bautista     Initial creation
----------------------------------------------------------------------------------------------- */
global class JVCO_DCA2StageBatchScheduler implements Schedulable 
{
    public static JVCO_DCA_2nd_Stage_Automation_Controller__c dcaController = [SELECT id, JVCO_DCA_2nd_Stage_Batch_size__c FROM JVCO_DCA_2nd_Stage_Automation_Controller__c
                                                                              LIMIT 1];
    public static integer BatchSize = Integer.valueOf(dcaController.JVCO_DCA_2nd_Stage_Batch_size__c == null ? 20 : dcaController.JVCO_DCA_2nd_Stage_Batch_size__c);
    global void execute(SchedulableContext sc) 
    {
		Database.executeBatch(new JVCO_DCA2StageBatchProcess(), BatchSize);
    }
    
    private static String getSchedulerExpression(DateTime dt) 
    {
        return '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();    
    }
    
    public static void CallExecuteMomentarily()  
    {
        System.schedule('JVCO_DCA2StageBatchScheduler', getSchedulerExpression(DateTime.Now().addSeconds(5)), new JVCO_DCA2StageBatchScheduler());
    }
}