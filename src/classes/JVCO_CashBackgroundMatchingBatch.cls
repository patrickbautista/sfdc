/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_CashBackgroundMatchingBatch.cls 
   Description:     Batch Class for JVCO_CashBackgroundMatchingBatch.cls
   Test class:      

   Date            Version     Author              Summary of Changes 
   -----------     -------     -----------------   -----------------------------------
   24-Apr-2019     0.1         franz.g.a.dimaapi   Intial creation
   11-Jun-2019     0.2         mel.andrei.b.santos GREEN-34694 - updated finish method to call overdue instalment batch
------------------------------------------------------------------------------------------------ */
public class JVCO_CashBackgroundMatchingBatch implements Database.Batchable<sObject>, Database.Stateful
{
    private static final JVCO_General_Settings__c generalSettings = JVCO_General_Settings__c.getInstance();
    private Boolean isBatch;
    private Boolean isQueryAll;
    private Map<Id, c2g__codaTransactionLineItem__c> tliMapToBGQueue;
    private Set<Id> cashEntryIdSet = new Set<Id>();
    public Map<Id, Id> licAndCustAccIdMap = new Map<Id, Id>();
    
    public JVCO_CashBackgroundMatchingBatch(){
        this.isQueryAll = true;
        this.isBatch = false;
        tliMapToBGQueue = new Map<Id, c2g__codaTransactionLineItem__c>();
    }
    
    public JVCO_CashBackgroundMatchingBatch (Set<Id> cashEntryIdSet, Boolean isBatch, Boolean useBatchAndIsSmarterProcess){
        JVCO_FFutil.stopCodaTransactionHandlerSMP = useBatchAndIsSmarterProcess;
        this.isQueryAll = false;
        this.cashEntryIdSet = cashEntryIdSet;
        this.isBatch = isBatch;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
        if(isQueryAll){
            return Database.getQueryLocator([SELECT Id,
                                             c2g__CashEntry__c,
                                             c2g__Account__c,
                                             c2g__CashEntryValue__c,
                                             c2g__AccountReference__c,
                                             JVCO_Reference_Document__c,
                                             JVCO_Reference_Document_Type__c,
                                             c2g__Account__r.JVCO_Customer_Account__c
                                             FROM c2g__codaCashEntryLineItem__c
                                             WHERE JVCO_Reference_Document_Type__c IN ('Invoice', 'Credit', 'Journal', 'Cash')
                                             AND c2g__Account__r.JVCO_In_Infringement__c = FALSE
                                             AND c2g__Account__r.JVCO_In_Enforcement__c = FALSE
                                             AND c2g__Account__r.JVCO_Exclude_From_Background_Matching__c = FALSE]);
        }else{
            return Database.getQueryLocator([SELECT Id, c2g__Account__c,
                                             Name, c2g__LineType__c, 
                                             c2g__MatchingStatus__c,
                                             c2g__AccountValue__c,
                                             c2g__Account__r.JVCO_Customer_Account__c,
                                             c2g__AccountOutstandingValue__c,
                                             c2g__LineReference__c,
                                             c2g__OwnerCompany__r.c2g__CustomerWriteOff__c,
                                             c2g__Transaction__r.c2g__Period__r.c2g__Closed__c
                                             FROM c2g__codaTransactionLineItem__c
                                             WHERE c2g__MatchingStatus__c = 'Available'
                                             AND c2g__LineType__c = 'Account'
                                             AND c2g__LineDescription__c != 'Not for Matching'
                                             AND c2g__AccountOutstandingValue__c != 0
                                             AND c2g__Transaction__r.c2g__TransactionType__c IN ('Cash')
                                             AND c2g__Transaction__r.c2g__CashEntry__r.c2g__Type__c IN ('Receipt')
                                             AND c2g__Transaction__r.c2g__CashEntry__c IN :cashEntryIdSet
                                             AND c2g__Account__r.JVCO_In_Infringement__c = FALSE
                                             AND c2g__Account__r.JVCO_In_Enforcement__c = FALSE
                                             AND c2g__Account__r.JVCO_Exclude_From_Background_Matching__c = FALSE]);
        }
    }

    public void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        if(!generalSettings.JVCO_Disable_Cash_Background_Matching__c)
        {
            if(isBatch || isQueryAll){
                if(isQueryAll){
                    setMatchingReferenceMapping((List<c2g__codaCashEntryLineItem__c>)scope);                    
                    getAccountToRunQueable((List<c2g__codaCashEntryLineItem__c>)scope, new List<c2g__codaTransactionLineItem__c>());
                    removeReferenceDocument(scope);
                }else{
                    setMatchingRefUsingTli((List<c2g__codaTransactionLineItem__c>)scope);
                    getAccountToRunQueable(new List<c2g__codaCashEntryLineItem__c>(), (List<c2g__codaTransactionLineItem__c>)scope);
                }
            }else{
                tliMapToBGQueue = new Map<Id, c2g__codaTransactionLineItem__c>([SELECT Id,
                                                                                c2g__Account__c,
                                                                                c2g__Account__r.JVCO_Customer_Account__c
                                                                                FROM c2g__codaTransactionLineItem__c
                                                                                WHERE c2g__MatchingStatus__c = 'Available'
                                                                                AND c2g__LineType__c = 'Account'
                                                                                AND c2g__HomeValue__c != 0
                                                                                AND c2g__Account__r.Name != :Label.JVCO_Account_Unidentified
                                                                                AND c2g__Transaction__r.c2g__CashEntry__c IN : cashEntryIdSet]);
            }
        }
    }
    
    private void setMatchingReferenceMapping(List<c2g__codaCashEntryLineItem__c> cashEntryLineList)
    {
        Map<Id, String> cashEntryTransLineIdToSInvNameMap = new Map<Id, String>();
        Map<Id, String> cashEntryTransLineIdToCNoteNameMap = new Map<Id, String>();
        Map<Id, String> cashEntryTransLineIdToJournalNameMap = new Map<Id, String>();
        Map<Id, String> cashEntryTransLineIdToReceiptLineNameMap = new Map<Id, String>();
        Map<Id, String> cashEntryTransLineIdToRefundLineNameMap = new Map<Id, String>();
        Map<Id, c2g__codaTransactionLineItem__c> transLineMap = new Map<Id, c2g__codaTransactionLineItem__c>();
        
        Set<Id> cashEntryIdToMatchSet = new Set<Id>();
        Map<Id, List<c2g__codaCashEntryLineItem__c>> accIdToCashEntryLineListMap = new Map<Id, List<c2g__codaCashEntryLineItem__c>>();
        for(c2g__codaCashEntryLineItem__c cel : cashEntryLineList)
        {
            cashEntryIdToMatchSet.add(cel.c2g__CashEntry__c);
            if(!accIdToCashEntryLineListMap.containsKey(cel.c2g__Account__c))
            {
                accIdToCashEntryLineListMap.put(cel.c2g__Account__c, new List<c2g__codaCashEntryLineItem__c>());
            }
            accIdToCashEntryLineListMap.get(cel.c2g__Account__c).add(cel);
        }
        
        for(c2g__codaTransactionLineItem__c tli : [SELECT Id, c2g__Account__c,
                                                   c2g__AccountValue__c,
                                                   c2g__AccountOutstandingValue__c,
                                                   c2g__LineReference__c,
                                                   c2g__OwnerCompany__r.c2g__CustomerWriteOff__c,
                                                   c2g__Transaction__r.c2g__Period__r.c2g__Closed__c
                                                   FROM c2g__codaTransactionLineItem__c
                                                   WHERE c2g__Transaction__r.c2g__CashEntry__c IN : cashEntryIdToMatchSet
                                                   AND c2g__AccountOutstandingValue__c != 0
                                                   AND c2g__LineType__c = 'Account'])
        {
            if(accIdToCashEntryLineListMap.containsKey(tli.c2g__Account__c))
            {
                for(c2g__codaCashEntryLineItem__c cel : accIdToCashEntryLineListMap.get(tli.c2g__Account__c))
                {
                    if(((tli.c2g__LineReference__c != null && tli.c2g__LineReference__c == cel.c2g__AccountReference__c)
                        || tli.c2g__LineReference__c == null)
                       && tli.c2g__Account__c == cel.c2g__Account__c
                       && Math.abs(tli.c2g__AccountValue__c) == cel.c2g__CashEntryValue__c)
                    {
                        String cashEntryLineReferenceDocument = cel.JVCO_Reference_Document__c;
                        if(cel.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Invoice'))
                        {
                            cashEntryTransLineIdToSInvNameMap.put(tli.Id, cashEntryLineReferenceDocument);
                        }else if(cel.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Credit'))
                        {
                            cashEntryTransLineIdToCNoteNameMap.put(tli.Id, cashEntryLineReferenceDocument);
                        }else if(cel.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Journal'))
                        {
                            cashEntryTransLineIdToJournalNameMap.put(tli.Id, cashEntryLineReferenceDocument);
                        }else if(cel.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Cash') && tli.c2g__AccountOutstandingValue__c > 0)
                        {
                            cashEntryTransLineIdToReceiptLineNameMap.put(tli.Id, cashEntryLineReferenceDocument);
                        }else
                        {
                            cashEntryTransLineIdToRefundLineNameMap.put(tli.Id, cashEntryLineReferenceDocument);
                        }
                        transLineMap.put(tli.Id, tli);
                    }
                }
            }
        }
        executeMatching(cashEntryTransLineIdToSInvNameMap, cashEntryTransLineIdToCNoteNameMap, cashEntryTransLineIdToJournalNameMap,
                       cashEntryTransLineIdToReceiptLineNameMap, cashEntryTransLineIdToRefundLineNameMap, transLineMap);
    }
    
    private void setMatchingRefUsingTli(List<c2g__codaTransactionLineItem__c> tliList)
    {
        Map<Id, String> cashEntryTransLineIdToSInvNameMap = new Map<Id, String>();
        Map<Id, String> cashEntryTransLineIdToCNoteNameMap = new Map<Id, String>();
        Map<Id, String> cashEntryTransLineIdToJournalNameMap = new Map<Id, String>();
        Map<Id, String> cashEntryTransLineIdToReceiptLineNameMap = new Map<Id, String>();
        Map<Id, String> cashEntryTransLineIdToRefundLineNameMap = new Map<Id, String>();
        Map<Id, c2g__codaTransactionLineItem__c> transLineMap = new Map<Id, c2g__codaTransactionLineItem__c>();
        
        for(c2g__codaTransactionLineItem__c tli : tliList){
            if(tli.c2g__LineReference__c != null)
            {
                transLineMap.put(tli.Id, tli);
                if(tli.c2g__LineReference__c.contains('SIN')){
                    cashEntryTransLineIdToSInvNameMap.put(tli.Id, tli.c2g__LineReference__c);
                }
                else if(tli.c2g__LineReference__c.contains('SCR')){
                    cashEntryTransLineIdToCNoteNameMap.put(tli.Id, tli.c2g__LineReference__c);
                }
                else if(tli.c2g__LineReference__c.contains('JNL')){
                    cashEntryTransLineIdToJournalNameMap.put(tli.Id, tli.c2g__LineReference__c);
                }
                else if(tli.c2g__LineReference__c.contains('CSH') && tli.c2g__AccountOutstandingValue__c > 0){
                    cashEntryTransLineIdToReceiptLineNameMap.put(tli.Id, tli.c2g__LineReference__c);
                }
                else if(tli.c2g__LineReference__c.contains('CSH') && tli.c2g__AccountOutstandingValue__c < 0){
                    cashEntryTransLineIdToRefundLineNameMap.put(tli.Id, tli.c2g__LineReference__c);
                }
            }
        }
        executeMatching(cashEntryTransLineIdToSInvNameMap, cashEntryTransLineIdToCNoteNameMap, cashEntryTransLineIdToJournalNameMap, 
                        cashEntryTransLineIdToReceiptLineNameMap, cashEntryTransLineIdToRefundLineNameMap, transLineMap);
    }
    
    private void executeMatching(Map<Id, String> cashEntryTransLineIdToSInvNameMap,
                                 Map<Id, String> cashEntryTransLineIdToCNoteNameMap,
                                 Map<Id, String> cashEntryTransLineIdToJournalNameMap,
                                 Map<Id, String> cashEntryTransLineIdToReceiptLineNameMap,
                                 Map<Id, String> cashEntryTransLineIdToRefundLineNameMap,
                                 Map<Id, c2g__codaTransactionLineItem__c> transLineMap)
    {
        JVCO_MatchingReferenceDocumentInterface mrdi = null;
        if(!cashEntryTransLineIdToSInvNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToInvoiceLogic(cashEntryTransLineIdToSInvNameMap, transLineMap);
        }else if(!cashEntryTransLineIdToCNoteNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToCreditNoteLogic(cashEntryTransLineIdToCNoteNameMap, transLineMap);
        }else if(!cashEntryTransLineIdToJournalNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToJournalLogic(cashEntryTransLineIdToJournalNameMap, transLineMap);
        }else if(!cashEntryTransLineIdToReceiptLineNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToCashReceiptLogic(cashEntryTransLineIdToReceiptLineNameMap, transLineMap);
        }else
        {
            mrdi = new JVCO_MatchToCashRefundLogic(cashEntryTransLineIdToRefundLineNameMap, transLineMap);
        }
        mrdi.execute();
    }

    private void removeReferenceDocument(List<c2g__codaCashEntryLineItem__c> cashEntryLineList)
    {
        for(c2g__codaCashEntryLineItem__c cashEntryLine : cashEntryLineList)
        {
            cashEntryLine.JVCO_Reference_Document__c = null;
        }
        update cashEntryLineList;
    }
    
    public void getAccountToRunQueable(List<c2g__codaCashEntryLineItem__c> cliList, List<c2g__codaTransactionLineItem__c> tliList){
        for(c2g__codaCashEntryLineItem__c cli : cliList)
        {
            if(!licAndCustAccIdMap.containsKey(cli.c2g__Account__c) && cli.c2g__Account__c != null && cli.c2g__Account__r.JVCO_Customer_Account__c != null){
                licAndCustAccIdMap.put(cli.c2g__Account__c, cli.c2g__Account__r.JVCO_Customer_Account__c);
            }
        }
        
        for(c2g__codaTransactionLineItem__c tli : tliList)
        {
            if(!licAndCustAccIdMap.containsKey(tli.c2g__Account__c) && tli.c2g__Account__c != null && tli.c2g__Account__r.JVCO_Customer_Account__c != null){
                licAndCustAccIdMap.put(tli.c2g__Account__c, tli.c2g__Account__r.JVCO_Customer_Account__c);
            }
        }
    }
    
    public void finish(Database.BatchableContext bc)
    {
        //run overdue an queueable if isqueryall is used
        if(!Test.isRunningTest() && isQueryAll)
        {
            JVCO_OverdueInstalment_Batch oib = new JVCO_OverdueInstalment_Batch();
            oib.licAndCustAccIdMap = licAndCustAccIdMap;
            Database.executeBatch(oib, 1);
        }//Call queueable if not manual used in posting
        else if(!Test.isRunningTest() && isBatch)
        {
            System.enqueueJob(new JVCO_UpdateSalesInvoiceQueueable(licAndCustAccIdMap));
        }else{//Run BG matching queueable if manual posting
            if(!isBatch && !isQueryAll){
                System.enqueueJob(new JVCO_BackgroundMatching_Queueable(tliMapToBGQueue));
            }
        }
    }
}