/**
* Created by: Accenture.
* Created Date: 16.10.2017
* Description: Controller class for SendRemittance List View button on Payment object 
*
**/
public class JVCO_SendRemittanceCtrl
{
    private final ApexPages.StandardSetController stdSetcontroller;

    public String finalURL{get;set;}
    public String errorMsg{get;set;}
    public String successMsg{get;set;}    

   /**
    *  Constructor 
    *
    */
   public JVCO_SendRemittanceCtrl(ApexPages.StandardSetController ctrl) 
   {
      stdSetcontroller= ctrl;
      String returnUrl = ApexPages.currentPage().getParameters().get('retUrl');
      finalURL = returnUrl ; //URL.getSalesforceBaseUrl().toExternalForm(); 
   }
   
    public void processSendRemittance()
    { 
        errorMsg = null;
        successMsg= null;
        Set<String> paymentIDset = new Set<String>();               
        // Check If user has selected any record 
        if (stdSetcontroller.getSelected().size() <= 0 )
        {
           errorMsg = System.label.JVCO_SendRemittance_Btn_No_Rows_Sel_Error_Msg ;  
           return;
        }
        for(sObject  sObj :stdSetcontroller.getSelected())
        {
            paymentIDset.add(sObj.id+'');    
        } 
        updatePayment(paymentIDset);
    }
           
    public void updatePayment(Set<String> paymentIDset)
    { 
        List<c2g__codaPayment__c> paymentLst = new List<c2g__codaPayment__c>();  
        List<c2g__codaPaymentMediaSummary__c> mediaSumaryLst = new   List<c2g__codaPaymentMediaSummary__c>(); 
        Set<String> paymentRecordSet = new Set<String>();
        
        for(c2g__codaPaymentMediaSummary__c  mediaSummaryObj : [Select id, c2g__PaymentMediaControl__r.c2g__Payment__c   from c2g__codaPaymentMediaSummary__c where JVCO_Generate_DQ_Record__c = false and  c2g__PaymentMediaControl__c  in (Select id from  c2g__codaPaymentMediaControl__c  where c2g__Payment__c  in:paymentIDset and c2g__Payment__r.c2g__Status__c = 'Matched'  and c2g__Payment__r.Send_Remittance__c = false ) ])
        {
            mediaSummaryObj.JVCO_Generate_DQ_Record__c =  true;
            mediaSumaryLst.add(mediaSummaryObj);
          //  paymentRecordSet.add(mediaSummaryObj.c2g__PaymentMediaControl__r.c2g__Payment__c ); 
        }
        try
        {
            if(mediaSumaryLst.size() > 0 )
            {
                update mediaSumaryLst;
                successMsg = 'Total number of records  processed : ' + mediaSumaryLst.size(); 
            }
            else
            {
                successMsg = System.label.JVCO_SendRemittance_Btn_SOQL_No_Rows_Error_Msg; 
            }            
            
            //successMsg = mediaSumaryLst.size() != paymentIDset.size() ? (successMsg +  ' Total number of records failed : '  + (paymentIDset.size() - mediaSumaryLst.size()  ))  : successMsg;
        }
        catch(Exception e)
        {
            errorMsg = System.label.JVCO_SendRemittance_Btn_Exception_Error_Msg;
        }            
    }      
}