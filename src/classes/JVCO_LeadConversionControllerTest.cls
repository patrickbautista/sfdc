/* ----------------------------------------------------------------------------------------------
   Name: JVCO_VenueAssignFieldAgentsPostLogicTest.cls 
   Description: Test Class for JVCO_LeadAssignFieldAgentsPostCodeLogic

   Date          Version     Author              Summary of Changes 
   -----------   -------     -----------------   -----------------------------------------
   26-Aug-2016   0.1         joseph.g.barrameda  Intial creation
   12-Jun-2017   0.2         raus.k.b.ablaza     Added data creation for Budget Category 
                                                 Dimension Mapping custom settings
  ----------------------------------------------------------------------------------------------- */
    // Scenarios: 
    //    1.   In the scenario that the Account, Contact and Venue do not already exist, new records will be created. 
    //    2.   If the Account and Contact exist, but the Venue doesn't, the Venue will be created and affiliated to the Account. 
    //    3.   If the Venue exists, but the Account and Contact don't, the Account and Contact will be created and affiliated with the Venue.
    //    4.   If the Account, Contact and Venue exist, but there is no relationship between them, an affiliation between the Account and Venue will be made.

@isTest
private class JVCO_LeadConversionControllerTest {
    
    public static Id cusAccountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
    public static Id licAccountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    
    @testSetup static void createQualifiedLead(){
        List<JVCO_Primary_Tariff_Mapping__c> settings = new List<JVCO_Primary_Tariff_Mapping__c>();
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Community Centre'));
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Aircraft'));
        insert settings;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        // Data creation for custom setting "Budget Category Dimension Mapping"
        JVCO_TestClassObjectBuilder.createCustomSettings();
      
        //Create Dimension 1
        c2g__codaDimension1__c Dim1 = new c2g__codaDimension1__c();
        Dim1.c2g__ReportingCode__c = 'BC004';
        Dim1.c2g__UnitOfWork__c = 4.0;
        Dim1.JVCO_Function__c = 'Budget';
        insert Dim1;


      
        //Create Pricebook
        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPricebook();
        insert pb;

        //Create New Lead
        Lead testLead = new Lead();
        testLead.status = 'Qualified';
        testLead.JVCO_Account_Name__c = 'PRS Test';
        testLead.Company        = 'XXX Inc';                        //New Venue Name
        testLead.JVCO_Venue_Street__c   = '41 Streatham High';
        testLead.JVCO_Venue_PostCode__c = 'SW16 1ER'; 
        testLead.JVCO_Venue_Type__c     = 'Aircraft';
        testLead.JVCO_Venue_Sub_Type__c  = 'Aircraft';
        
        testLead.Industry       = 'Music Systems';                //New Changes from Sector to Industry
        testLead.FirstName      = 'Chris';
        testLead.LastName       = 'Butler';
        testLead.Title          = 'Chairman';
        testLead.Email          = 'testLead@jvcolead.com';
        testLead.Phone          = '08000684828';
        testLead.MobilePhone    = '08000684828';
        testLead.Street         = '41 Streatham High';
        testLead.PostalCode     = 'SW16 1ER';
        testLead.City           = 'London';
        testLead.JVCO_Preferred_Communication_Method__c = 'Email';
        //testLead.JVCO_Marketing_Communications_Preference__c = 'Yes - send me marketing communications';
        testLead.JVCO_Start_Date__c = Date.today();
        testLead.JVCO_Business_Type_Lead__c = 'Ordinary Partnership';
        insert testLead;


        //Create New Activity
            Task t=new Task();
        t.Subject  = 'Follow up Status';
        t.WhoId    = testlead.id;
        t.Type     = 'Call';
        t.Status   = 'Open';
        t.Priority = 'Normal';
        insert t; 
        
        //Create New Event
        Event e=new Event();
        e.Subject      = 'Attend Concert';
        e.WhoId        = testLead.id;
        e.ActivityDateTime = System.Now();
        e.EndDateTime  = System.Now();
        insert e;

        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
        testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc.Dimension_1_Required__c = false;
        testGeneralLedgerAcc.Dimension_2_Required__c = false;
        testGeneralLedgerAcc.Dimension_3_Required__c = false;
        testGeneralLedgerAcc.Dimension_4_Required__c = false;
        testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
        insert testGeneralLedgerAcc;

        c2g__codaTaxCode__c taxCode = new c2g__codaTaxCode__c();
        taxCode.name = 'GB-O-STD-LIC';
        taxCode.c2g__GeneralLedgerAccount__c = testGeneralLedgerAcc.id;
        taxCode.ffvat__NetBox__c = 'Box 6';
        taxCode.ffvat__TaxBox__c = 'Box 1';
        insert taxCode;

        //added by edmundCUa
        /*JVCO_Budget_Category_Dimension_Mapping__c testBudgetDimension = new JVCO_Budget_Category_Dimension_Mapping__c();
        testBudgetDimension.JVCO_Budget_Category_Label__c = 'Transport & Terminals';
        testBudgetDimension.JVCO_Dimension_Label__c = 'Transport & Terminals';
        testBudgetDimension.JVCO_Dimension_Reporting_Code__c = 'BC017';
        testBudgetDimension.JVCO_Venue_SubType_Label__c = 'Aircraft';
        testBudgetDimension.JVCO_Venue_Type_Label__c = 'Aircraft';
        testBudgetDimension.Name = '50';
        insert testBudgetDimension;*/

        c2g__codaDimension1__c testDimension1 = new c2g__codaDimension1__c();
        testDimension1.c2g__ReportingCode__c = 'BC012';
        testDimension1.c2g__UnitOfWork__c = 24.0;
        testDimension1.JVCO_Function__c = 'Budget';
        testDimension1.Name = 'Transports & Terminals';
        insert testDimension1;

    }
    
    
    static testMethod void testConvertNotQualifiedLead(){
        
        //Create Lead 
        Lead newLead=new Lead();
        newLead.Status ='Open';
        newLead.FirstName ='Test';
        newLead.LastName = 'Record';
        newLead.JVCO_Account_Name__c = 'XYZ Inc';
        newLead.Company = 'Zinc 123';
        newlead.JVCO_Preferred_Communication_Method__c = 'Email';
        newlead.Email = 'test@test.com';
        //newLead.JVCO_Marketing_Communications_Preference__c = 'Yes - send me marketing communications';
        insert newLead;
        
        test.startTest();        
        ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
        JVCO_LeadConversionController leadConversion = new JVCO_LeadConversionController(sc);
        
        PageReference pageRef = Page.JVCO_LeadConversion;
        pageRef.getParameters().put('id', String.valueOf(newlead.Id));
        test.setCurrentPage(pageRef);
        test.stopTest();        
                
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean isError= false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains(Label.Lead_information_not_complete)) isError = true;
        }        
       // system.assertEquals(isError, true);
    }   

    // 1. In the scenario that the Account, Contact and Venue do not already exist, new records will be created.
    static testMethod void testConvertLeadNewAccountContact(){
        
        Lead newLead =[SELECT Id, Company, IsConverted, Name FROM Lead order by CreatedDate DESC limit 1];
       
        
        test.startTest();        
        ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
        ApexPages.currentPage().getHeaders();
        JVCO_LeadConversionController leadConversion = new JVCO_LeadConversionController(sc);
        
        PageReference pageRef = Page.JVCO_LeadConversion;
        pageRef.getParameters().put('id', String.valueOf(newlead.Id));
       
        
        Test.setCurrentPage(pageRef);
        leadConversion.checkDuplicates();
        leadConversion.clearDuplicateList();
        leadConversion.nextPage();
        leadConversion.isRedirectToOpp = true; 
        
        leadConversion.convertLead();
        test.stopTest();
        
        Lead checkLead =               [SELECT Id, Company, JVCO_Account_Name__c, IsConverted from Lead WHERE ID=:newLead.Id];
        //System.assertEquals(checklead.isConverted,true);                            //Validate if LeadConversion took place
        
        Account checkAccount =         [SELECT Id,Name FROM Account WHERE RecordTypeId=:cusAccountRecTypeId order by CreatedDate DESC limit 1];
        Account checkLicAccount =      [SELECT Id,Name FROM Account WHERE RecordTypeId=:licAccountRecTypeId order by CreatedDate DESC limit 1];
        Contact checkContact =         [SELECT Id,Name,AccountId FROM Contact order by CreatedDate DESC limit 1];
        JVCO_Venue__c checkVenue =     [SELECT Id,Name FROM JVCO_Venue__c order by CreatedDate DESC limit 1];
        JVCO_Affiliation__c checkAff=  [SELECT Id,JVCO_Account__c,JVCO_Venue__c FROM JVCO_Affiliation__c order by CreatedDate DESC limit 1];        
        
        System.assertEquals(checkAccount.Name, checkLead.JVCO_Account_Name__c);     //Validate if an Account was created
        System.assertEquals(checkContact.AccountId, checkAccount.Id);               //Validate if the Contact was associated to the Account 
        System.assertEquals(checkVenue.Name , newLead.Company);                     //Validate if Venue was created based from Lead.Venue Name
        System.assertEquals(checkAff.JVCO_Account__c , checkLicAccount.Id);         //Validate if an Affiliation was created based
        
        //Try to convert a Converted Lead
        ApexPages.StandardController newSC = new ApexPages.StandardController(newLead);
        JVCO_LeadConversionController newLeadConversion = new JVCO_LeadConversionController(newSC);
    
        PageReference newPageRef = Page.JVCO_LeadConversion;
        newPageRef.getParameters().put('id', String.valueOf(newlead.Id));
        Test.setCurrentPage(newPageRef);
        
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean isError = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains(Label.Lead_has_already_been_converted)) isError = true;
            System.debug('@@@*** error message: ' + msg);
        }
        
        System.assert(isError);                                                                                                                                                                                                                                                                                                                                                                           
   
    } 
    
    //2. If the Account and Contact exist, but the Venue doesn't, the Venue will be created and affiliated to the Account. 
    static testMethod void testConvertLeadExistingAccountContactVenue(){
        
        Lead newLead =[SELECT Id,IsConverted, Company,Street,City,Country, PostalCode,Name,Title,FirstName,LastName,
                      JVCO_Account_Name__c, JVCO_Venue_Street__c,JVCO_Venue_PostCode__c, Website from Lead order by CreatedDate DESC limit 1];

        c2g__codaTaxCode__c TC = [SELECT Id FROM c2g__codaTaxCode__c order by CreatedDate DESC limit 1];


        //Create an Account that will soon trigger the Account Duplicate Rule 
        Account acc= JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;
        
        //Create an Contact that will soon trigger the Contact Duplicate Rule
        Contact con = new Contact();
        con.AccountId     = acc.Id; 
        con.FirstName     = newLead.FirstName; 
        con.LastName      = newLead.LastName;
        con.Title         = newLead.Title;    
        insert con;

        test.startTest();        
        ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
        JVCO_LeadConversionController leadConversion = new JVCO_LeadConversionController(sc);
        
        PageReference pageRef = Page.JVCO_LeadConversion;
        pageRef.getParameters().put('id', String.valueOf(newlead.Id));
        Test.setCurrentPage(pageRef);        
        
        leadConversion.checkDuplicates();
    
        System.currentPageReference().getParameters().put('cusAccountIdParam', acc.Id );
        System.currentPageReference().getParameters().put('licAccountIdParam', acc2.Id );
        System.currentPageReference().getParameters().put('contactIdParam', con.Id);
        
        leadConversion.isRedirectToOpp = false; 
        leadConversion.getLicenceAccountId();
        leadConversion.getCustomerAccountId();
        leadConversion.getContactId();
        leadConversion.getVenueId();
        
      //  System.assertEquals(leadConversion.hasDuplicateAccountRecord , true);
      //  System.assertEquals(leadConversion.hasDuplicateContactRecord , true);
      //  System.assertEquals(leadConversion.hasDuplicateVenueRecord , false);               
     

        leadConversion.nextPage();         
        leadConversion.selectedLicenceAccountId = acc2.Id;
        leadConversion.selectedContactId = con.Id; 
        leadConversion.convertLead();   
        test.stopTest();  
 
        //Get the Licence Account created
        //JVCO_Venue__c checkVenue     = [SELECT Id,Name FROM JVCO_Venue__c WHERE Name =:newLead.Company order by CreatedDate DESC limit 1];
        
        //System.assertEquals(newLead.Company, checkVenue.Name);

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean isError = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('duplicate')) isError = true;
            System.debug('@@@*** error message: ' + msg);
        }
        
      //  System.assert(isError);  
        
    }
    
    
    //    3.   If the Venue exists, but the Account and Contact don't, the Account and Contact will be created and affiliated with the Venue.
    static testMethod void testConvertLeadExistingVenueNewAccountContact(){
        
        Lead newLead =[SELECT Id,IsConverted, Company,Street,City,Country, PostalCode,Name,Title,FirstName,LastName,
                      JVCO_Account_Name__c, JVCO_Venue_Street__c,JVCO_Venue_PostCode__c, Website from Lead order by CreatedDate DESC limit 1];
        
        //Create a Venue that will soon trigger the Venue Duplicate Rule
        JVCO_Venue__c ven = new JVCO_Venue__c();
        ven.Name                = newLead.Company;
        ven.JVCO_Street__c      = newLead.JVCO_Venue_Street__c;
        ven.JVCO_PostCode__c    = newLead.JVCO_Venue_PostCode__c;
        insert ven; 
        
        test.startTest();        
        ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
        JVCO_LeadConversionController leadConversion = new JVCO_LeadConversionController(sc);
        
        PageReference pageRef = Page.JVCO_LeadConversion;
        pageRef.getParameters().put('id', String.valueOf(newlead.Id));
        Test.setCurrentPage(pageRef);        
        
        leadConversion.checkDuplicates();
    
        System.currentPageReference().getParameters().put('venueIdparam', ven.Id );
         
        leadConversion.isRedirectToOpp = false; 
        leadConversion.getLicenceAccountId();
        leadConversion.getContactId();
        leadConversion.getVenueId();
        
        System.assertEquals(leadConversion.hasDuplicateAccountRecord , false);
        System.assertEquals(leadConversion.hasDuplicateContactRecord , false);
        //System.assertEquals(leadConversion.hasDuplicateVenueRecord , true);
        
        leadConversion.nextPage();         
        leadConversion.selectedvenueId = ven.Id; 
        leadConversion.convertLead();                  
        test.stopTest();        
        
        //Get the Licence Account created
        JVCO_Venue__c checkVenue     = [SELECT Id,Name FROM JVCO_Venue__c WHERE Name =:newLead.Company order by CreatedDate DESC limit 1];
        
        System.assertEquals(newLead.Company, checkVenue.Name);

    }
          

 
    //4. If the Account, Contact and Venue exist, but there is no relationship between them, an affiliation between the Account and Venue will be made.
    static testMethod void testConvertLeadExistingAccountContactNewVenue(){
        
        c2g__codaTaxCode__c TC = [SELECT Id FROM c2g__codaTaxCode__c order by CreatedDate DESC limit 1];


        Lead newLead =[SELECT Id,IsConverted, Company,Street,City,Country, PostalCode,Name,Title,FirstName,LastName,
                      JVCO_Account_Name__c, JVCO_Venue_Street__c,JVCO_Venue_PostCode__c, Website from Lead order by CreatedDate DESC limit 1];
        
        //Create an Account that will soon trigger the Account Duplicate Rule 
        Account acc= JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;
        
        //Create an Contact that will soon trigger the Contact Duplicate Rule
        Contact con = new Contact();
        con.AccountId     = acc.Id; 
        con.FirstName     = newLead.FirstName; 
        con.LastName      = newLead.LastName;
        con.Title         = newLead.Title;    
        insert con;

        //Create a Venue that will soon trigger the Venue Duplicate Rule
        JVCO_Venue__c ven = new JVCO_Venue__c();
        ven.Name                = newLead.company;
        ven.JVCO_Street__c      = newLead.JVCO_Venue_Street__c;
        ven.JVCO_PostCode__c    = newLead.JVCO_Venue_PostCode__c; 
        insert ven; 
        
        test.startTest();        
        ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
        JVCO_LeadConversionController leadConversion = new JVCO_LeadConversionController(sc);
        
        PageReference pageRef = Page.JVCO_LeadConversion;
        pageRef.getParameters().put('id', String.valueOf(newlead.Id));
        Test.setCurrentPage(pageRef);        
        //Set Account Id as the selectedAccountId
        leadConversion.checkDuplicates();
    
        System.currentPageReference().getParameters().put('cusAccountIdParam', acc.Id );
        System.currentPageReference().getParameters().put('licAccountIdParam', acc2.Id );
        System.currentPageReference().getParameters().put('contactIdparam', con.Id );
        System.currentPageReference().getParameters().put('venueIdparam', ven.Id );
        
        leadConversion.isRedirectToOpp = false; 
        leadConversion.getLicenceAccountId();
        leadConversion.getCustomerAccountId();
        leadConversion.getContactId();
        leadConversion.getVenueId();
        
      //  System.assertEquals(leadConversion.hasDuplicateAccountRecord , true);
      //  System.assertEquals(leadConversion.hasDuplicateContactRecord , true);
      // System.assertEquals(leadConversion.hasDuplicateVenueRecord , true);
        
        leadConversion.nextPage();         
        leadConversion.selectedLicenceAccountId = acc2.Id;
        leadConversion.selectedContactId = con.Id; 
        leadConversion.selectedVenueId   = ven.Id;        
        leadConversion.convertLead();                  
        test.stopTest();
        
        //Check if an affiliation between the Account and Venue was created.
        //Account checkLicence         = [SELECT Id FROM Account WHERE RecordTypeId=:licAccountRecTypeId order by CreatedDate DESC limit 1];
        //JVCO_Affiliation__c checkAff = [SELECT Id,Name FROM JVCO_Affiliation__c WHERE JVCO_Account__c =:checkLicence.Id AND JVCO_Venue__c=:ven.Id order by CreatedDate DESC limit 1];
        

        //An affiliation between the Licence Account and Venue was created. 
        //System.assertEquals(checkAff.id != null, true);        
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean isError = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('duplicate')) isError = true;
            System.debug('@@@*** error message: ' + msg);
        }
        
      //  System.assert(isError);  
        
    }   
      
    //    5.   If the Account exist and Contact dont. 
    static testMethod void testConvertLeadExistingAccountNewContact(){
        
         c2g__codaTaxCode__c TC = [SELECT Id FROM c2g__codaTaxCode__c order by CreatedDate DESC limit 1];

        Lead newLead =[SELECT Id,IsConverted, Company,Street,City,Country, PostalCode,Name,Title,FirstName,LastName,
                      JVCO_Account_Name__c, JVCO_Venue_Street__c,JVCO_Venue_PostCode__c, Website from Lead order by CreatedDate DESC limit 1];
        
         //Create an Customer Account that will soon trigger the Account Duplicate Rule 
         Account acc= JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;
       
        test.startTest();        
        ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
        JVCO_LeadConversionController leadConversion = new JVCO_LeadConversionController(sc);
        
        PageReference pageRef = Page.JVCO_LeadConversion;
        pageRef.getParameters().put('id', String.valueOf(newlead.Id));
        Test.setCurrentPage(pageRef);        
        
        leadConversion.checkDuplicates();
    
        System.currentPageReference().getParameters().put('cusAccountIdParam', acc.Id );
        System.currentPageReference().getParameters().put('licAccountIdParam', acc2.Id );
         
        leadConversion.isRedirectToOpp = false; 
        leadConversion.getLicenceAccountId();
        leadConversion.getContactId();
        leadConversion.getVenueId();
        
     //   System.assertEquals(leadConversion.hasDuplicateAccountRecord , true);
     //   System.assertEquals(leadConversion.hasDuplicateContactRecord , false);
     //   System.assertEquals(leadConversion.hasDuplicateVenueRecord , false);
        
        leadConversion.nextPage();         
        leadConversion.selectedCustomerAccountId = acc.Id; 
        leadConversion.selectedLicenceAccountId = acc2.Id; 
        leadConversion.selectedContactId = null; 
        
        leadConversion.convertLead();                  
        test.stopTest();        
        
        //Get the Licence Account created
        Contact checkContact     = [SELECT Id,AccountId from Contact order by CreatedDate DESC limit 1];  
      //  System.assertEquals(checkContact.AccountID, acc.Id);
    }

    
 
}