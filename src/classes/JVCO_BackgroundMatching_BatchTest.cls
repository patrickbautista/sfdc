@isTest
private class JVCO_BackgroundMatching_BatchTest
{
    @testSetup
    static void createTestData()
    {
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        List<JVCO_TransitionManualReviewNotifSettings__c> settings = new List<JVCO_TransitionManualReviewNotifSettings__c>();
        settings.add(new JVCO_TransitionManualReviewNotifSettings__c(Name = 'Limit', Limit__c = '3000'));
        insert settings;
    }

    @isTest
    static void testExecuteBatch()
    {
        JVCO_BackgroundMatching_Batch bm = new JVCO_BackgroundMatching_Batch();
        List<c2g__codaTransactionLineItem__c> tliList = new List<c2g__codaTransactionLineItem__c>();
        Database.QueryLocator ql = bm.start(null);
        bm.execute(null, tliList);
        bm.getAccountToRunQueable(new Map<Id, Id>());
        bm.finish(null);
    }
}