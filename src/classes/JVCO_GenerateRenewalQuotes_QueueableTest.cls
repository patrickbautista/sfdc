/* ----------------------------------------------------------------------------------------------
    Name: JVCO_GenerateRenewalQuotes_QueueableTest
    Description: Test Class for JVCO_GenerateRenewalQuotes_Queueable

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    08-Aug-2018     0.1         Jules Pablo         Intial creation
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_GenerateRenewalQuotes_QueueableTest {
	
	@testSetup static void setupTestData() 
    {
        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        insert dt;
        
        String cat;

        List<JVCO_Primary_Tariff_Mapping__c> settings = new List<JVCO_Primary_Tariff_Mapping__c>();
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Airport'));
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Aircraft')); 
        insert settings;

        JVCO_TestClassObjectBuilder.createBillingConfig();

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        Product2 prod1 = new Product2();
        prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 83202-C';
        a1.Type = 'Key Account';

        c2g__codaDimension1__c dim = new c2g__codaDimension1__c();
        dim.c2g__ReportingCode__c = 'BC008TEST';
        dim.Name = 'General Purpose';
        insert dim;

        a1.c2g__CODADimension1__c = dim.Id;

        insert a1;
        
        Account a2 = new Account();
        a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.Id);
        a2.JVCO_Preferred_Contact_Method__c = 'Letter';
        a2.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
        a2.Type = 'Key Account';
        insert a2;

        Pricebook2 testPB2 = new Pricebook2();
        testPB2.name = 'Standard Price Book';
        testPB2.IsActive = true;
        insert testPB2;
        
        PriceBookEntry pbe = new PriceBookEntry();
        pbe= JVCO_TestClassObjectBuilder.createPriceBookEntry(prod1.Id);
        insert pbe;
        
        
        
        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 83202';
        o.AccountId = a2.Id;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Renewal';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(2);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        insert q;
        
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(2);
        insert ql;
        
        Test.startTest();

        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();
        venueRecord.Name = 'test1';              
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = new JVCO_Affiliation__c();
        affilRecord.JVCO_Account__c = a2.Id;
        affilRecord.JVCO_Venue__c = venueRecord.id;   
        affilRecord.JVCO_Start_Date__c = system.today();   
        Insert  affilRecord;   
        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;   

        Opportunity opp = new Opportunity();
        opp.Id = o.Id;
        opp.Amount = 10;
        opp.StageName = 'Closed Won';
        opp.SBQQ__Contracted__c = true;
        update opp;
       
        
        cat = 'BB';
        JVCO_Venue__c nvenue3 = new JVCO_Venue__c();
        nvenue3.Name = 'Test Venue '+cat;
        nvenue3.JVCO_Lead_Source__c = 'Churn';
        nvenue3.JVCO_Venue_Type__c = 'Airport';
        nvenue3.JVCO_Venue_Sub_Type__c = 'Airport';
        nvenue3.JVCO_Budget_Category__c = 'Transports & Terminals';
        nvenue3.JVCO_City__c = 'City'+cat;
        nvenue3.JVCO_Country__c = 'United Kingdom';
        nvenue3.JVCO_Street__c = 'Street'+cat;
        nvenue3.JVCO_Postcode__c = 'N'+cat+' 7TH';
        insert nvenue3;

        JVCO_Affiliation__c  affilRecord3 = new JVCO_Affiliation__c();
        affilRecord3.JVCO_Account__c = a2.Id;
        affilRecord3.JVCO_Venue__c = nvenue3.id;   
        affilRecord3.JVCO_Start_Date__c = system.today().addMonths(13);
        insert affilRecord3;

        Contract c1 = new Contract();
        c1.SBQQ__RenewalQuoted__c = false;  
        c1.AccountID = a2.Id;
        c1.StartDate = System.today();
        c1.ContractTerm = 12;
        c1.SBQQ__RenewalQuoted__c = false;
        c1.JVCO_Renewal_Generated__c = false;
        c1.JVCO_RenewableQuantity__c = 1;
        c1.EndDate = System.today().addMonths(12);
        c1.Name = 'Test Contract 18023-L';
        c1.SBQQ__Quote__c = q.Id;
        c1.SBQQ__Opportunity__c = o.Id;
        c1.AccountId = a2.Id;
        c1.JVCO_Value__c = 1000;
        insert c1;

        SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(a2.id, prod1.id);
        s.Affiliation__c = affilRecord.Id;
        s.JVCO_Venue__c = venueRecord.Id;
        s.SBQQ__Contract__c = c1.Id;
        s.SBQQ__RenewalQuantity__c = 1;
        s.SBQQ__ListPrice__c = 1;
        s.SBQQ__NetPrice__c = 1;
        s.SBQQ__Quantity__c = 1;
        s.SBQQ__QuoteLine__c = ql.Id;
        s.End_Date__c = System.today().addMonths(12);
        s.Start_Date__c = System.today();
        insert s;

        
        c1.JVCO_RenewableQuantity__c = 1;
        c1.JVCO_Value__c = 1000;
        update c1;

        
        Test.stopTest();
    }

    private static testMethod void testMethodGenerateRenewalQuotesExecute() {
    	Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name, Licence_Account_Number__c from Account where RecordTypeId =: licenceRT limit 1];
        
        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        asCS.JVCO_Contract_Batch_Size__c = 1;

        upsert asCS JVCO_Array_Size__c.Id;
        
        Test.startTest();
        System.enqueueJob(new JVCO_GenerateRenewalQuotes_Queueable(a));
        Test.stopTest();
    }

    private static testMethod void testMethodGenerateRenewalQuotesExecuteError() {
    	Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name, Licence_Account_Number__c from Account where RecordTypeId =: licenceRT limit 1];
        
        JVCO_GenerateRenewalQuotesHelper.testError = true;
        
        Test.startTest();
        System.enqueueJob(new JVCO_GenerateRenewalQuotes_Queueable(a));
        Test.stopTest();
    }

    private static testMethod void testMethodGenerateRenewalQuotesReExecute() {
    	Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name, Licence_Account_Number__c from Account where RecordTypeId =: licenceRT limit 1];
        
        Map<Id, Contract> contractMap = new Map<Id, Contract>([select Id, AccountId, EndDate, ForceDeferred__c, Account.Type, SBQQ__AmendmentRenewalBehavior__c, SBQQ__RenewalQuoted__c, SBQQ__Opportunity__c  from Contract where SBQQ__RenewalQuoted__c = false and JVCO_Renewal_Generated__c = false and JVCO_RenewableQuantity__c > 0 and AccountId = :a.Id limit 1]);
        
        
        Set<Id> contractIds = new Set<Id>();
        for(Id oppId :contractMap.keySet()) {
        	contractIds.add(oppId);
        }

        Test.startTest();
        System.enqueueJob(new JVCO_GenerateRenewalQuotes_Queueable(contractMap, contractIds, a, '', 1, 0, 1));
        Test.stopTest();
    }
}