/* ----------------------------------------------------------------------------------------------
    Name: JVCO_RenewContractsExtension.cls 
    Description: Renews contracts

    Date         Version     Author                 Summary of Changes 
    -----------  -------     -----------------      -----------------------------------------
    ?             0.1         ?                     Intial creation (Added this section in version 0.2. Unsure who original created)
    21-Feb-2021   0.2         luke.walker           GREEN-36345 - Page was failing in lightning so updated to remove error
  ----------------------------------------------------------------------------------------------- */

public class JVCO_RenewContractsExtension  {

    public String masterContractId{get; set;}
    public final List<Contract> selectedContracts{get; set;}
    private String returnURL = null;
    private String jobId = null;
    private String uiThemeDisplayed;

    public JVCO_RenewContractsExtension(ApexPages.StandardSetController stdController) {
        Set<Id> contractIdSet = new Set<Id>();
        for(Contract c : (List<Contract>) stdController.getSelected())
        {
            contractIdSet.add(c.Id);
        }

        if (!contractIdSet.isEmpty())
        {
            selectedContracts = [Select Id, ContractNumber, StartDate, EndDate from Contract where Id in :contractIdSet];
        }

        returnURL = stdController.cancel().getUrl();
        uiThemeDisplayed = UserInfo.getUiThemeDisplayed();
    }

    public PageReference onRenew()
    {
        Boolean errorEncountered = false;

        // Check if am aster Contract has been selected
        if (masterContractId == null || masterContractId == '')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a Master Contract before Renewing'));
            errorEncountered = true;
        }
         //[Start] GREEN-32412 - refrain user to generate renewal quote if there's an existing job in progress
        JVCO_ApexJobController.retrieveJobs();
        if(JVCO_ApexJobController.lstApexJobs.size() > 0)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,JVCO_ApexJobController.RenewedInProgMsg));
            errorEncountered = true;
        }
        
        List<string> lstOfContracts = JVCO_ApexJobController.retrieveJobs(selectedContracts);
        if(lstOfContracts.size() > 0)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,JVCO_ApexJobController.RenewedMsg + lstOfContracts));
            errorEncountered = true;
        }
        //[End] GREEN-32412
        if (selectedContracts == null || selectedContracts.isEmpty())
        {
            errorEncountered = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No contracts have been selected for Renewal'));
        }

        if (!errorEncountered)
        {
            jobId = System.enqueueJob(new JVCO_RenewalContext(masterContractId, selectedContracts));
            
            Pagereference page = new PageReference(returnURL);
            
            //Checks for Lightning or Classic
            if(uiThemeDisplayed == 'Theme4d' || uiThemeDisplayed == 'Theme4u'){
                aura.redirect(page);
            } else {
                page.setRedirect(true);    
            }
            return page;
        }

        return null;

    }
}