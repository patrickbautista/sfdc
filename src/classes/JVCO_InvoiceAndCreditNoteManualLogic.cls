/* ----------------------------------------------------------------------------------------------
    Name: JVCO_InvoiceAndCreditNoteManualLogic
    Description: Create SalesInvoice And CreditNote for Manual Billing

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    06-Sep-2018  0.1         franz.g.a.dimaapi     Intial creation
    25-Nov-2019  0.9         franz.g.a.dimaapi     GREEN-33284 - Calculate Tax Value From Billing Parent Tax Total
----------------------------------------------------------------------------------------------- */
public class JVCO_InvoiceAndCreditNoteManualLogic
{   
    //Dim2,Prod, userCompany Mapping
    private static Map<String, Id> dim2Map;
    private static Map<String, Id> prodMap;
    private static c2g__codaUserCompany__c userCompany;
    private static Map<Id, String> accIdToInvoiceTypeMap;

    private static Map<Id, c2g__codaInvoice__c> accIdToSInvMap = new Map<Id, c2g__codaInvoice__c>();
    private static Map<Id, c2g__codaCreditNote__c> accIdToCNoteMap = new Map<Id, c2g__codaCreditNote__c>();
    private static Map<Id, Map<String, sObject>> accIdToDim2ToDocumentLineMap = new Map<Id, Map<String, sObject>>();

    private static Set<Id> sInvIdSet = new Set<Id>();
    private static Set<Id> cNoteIdSet = new Set<Id>();

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Credit Note
        Inputs: Account, Date, Date, String, Date
        Returns: c2g__codaCreditNote__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static void setupInvoiceAndCreditNoteHeader(List<Order> orderList)
    {
        setDefaultMappings();
        setAccIdToInvoiceTypeMap(orderList);
        for(AggregateResult ar : [SELECT AccountId accId,
                                  Account.c2g__CODADimension1__c dim1,
                                  Account.c2g__CODADimension3__c dim3,
                                  Account.c2g__CODAOutputVATCode__c taxCode,
                                  Account.JVCO_Dunning_Letter_Language__c dunningLanguage,
                                  SUM(JVCO_PRS_Total_Lines__c)prsTotalLines,
                                  SUM(JVCO_PPL_Total_Lines__c)pplTotalLines,
                                  SUM(JVCO_VPL_Total_Lines__c)vplTotalLines,
                                  SUM(JVCO_PRS_TotalAmount__c)prsTotalAmt,
                                  SUM(JVCO_PPL_TotalAmount__c)pplTotalAmt,
                                  SUM(JVCO_VPL_TotalAmount__c)vplTotalAmt,
                                  SUM(JVCO_PRS_Tax_Total_Amount__c)prsTotalTaxAmt,
                                  SUM(JVCO_PPL_Tax_Total_Amount__c)pplTotalTaxAmt,
                                  SUM(JVCO_VPL_Tax_Total_Amount__c)vplTotalTaxAmt,
                                  SUM(JVCO_Total_Amount__c)totalAmt,
                                  MIN(JVCO_Licence_Start_Date__c)minStartDate,
                                  MAX(JVCO_Licence_End_Date__c)maxEndDate,
                                  MIN(JVCO_DueDate__c)minDueDate,
                                  MIN(EffectiveDate)minInvDate,
                                  SUM(JVCO_Total_Surcharge_Amount__c)totalSurchargeAmt,
                                  SUM(JVCO_Surcharge_Tariffs__c)totalSurchargeTariffs
                                  FROM Order
                                  WHERE Id IN : orderList
                                  GROUP BY AccountId,
                                  Account.c2g__CODADimension1__c,
                                  Account.c2g__CODADimension3__c,
                                  Account.c2g__CODAOutputVATCode__c,
                                  Account.JVCO_Dunning_Letter_Language__c])
        {   
            Id accId = (Id)ar.get('accId');
            Date invDate = (Date)ar.get('minInvDate');
            Date dueDate = (Date)ar.get('minDueDate');
            Date licenceStartDate = (Date)ar.get('minStartDate');
            Date licenceEndDate = (Date)ar.get('maxEndDate');
            Decimal totalAmt = (Decimal)ar.get('totalAmt');
            //Create Sales Invoice Header
            if(totalAmt >= 0)
            {
                String customerType = (String)ar.get('dunningLanguage');
                Decimal totalSurchargeAmt = (Decimal)ar.get('totalSurchargeAmt');
                accIdToSInvMap.put(accId, createSalesInvoice(accId, customerType, invDate, dueDate, licenceStartDate, licenceEndDate, totalSurchargeAmt));
            //Create Sales Credit Note Header
            }else
            {
                accIdToCNoteMap.put(accId, createCreditNote(accId, invDate, dueDate, licenceStartDate, licenceEndDate));
            }

            Id dim1 = (Id)ar.get('dim1');
            Id dim3 = (Id)ar.get('dim3');
            Id taxCode = (Id)ar.get('taxCode');
            //Set Map Values
            accIdToDim2ToDocumentLineMap.put(accId, new Map<String, sObject>());
            //Create PRS Line
            if((Decimal)ar.get('prsTotalLines') > 0)
            {
                Decimal prsTotalAmt = (Decimal)ar.get('prsTotalAmt');
                Decimal prsTotalTaxAmt = (Decimal)ar.get('prsTotalTaxAmt');
                sObject docLine = createDocumentLine(dim1, dim3, taxCode, prsTotalAmt, prsTotalTaxAmt, 'PRS', totalAmt);
                accIdToDim2ToDocumentLineMap.get(accId).put('PRS', docLine);
            }
            //Create PPL Line
            if((Decimal)ar.get('pplTotalLines') > 0)
            {
                Decimal pplTotalAmt = (Decimal)ar.get('pplTotalAmt');
                Decimal pplTotalTaxAmt = (Decimal)ar.get('pplTotalTaxAmt');
                sObject docLine = createDocumentLine(dim1, dim3, taxCode, pplTotalAmt, pplTotalTaxAmt, 'PPL', totalAmt);
                accIdToDim2ToDocumentLineMap.get(accId).put('PPL', docLine);
            }
            //Create VPL Line
            if((Decimal)ar.get('vplTotalLines') > 0)
            {
                Decimal vplTotalAmt = (Decimal)ar.get('vplTotalAmt');
                Decimal vplTotalTaxAmt = (Decimal)ar.get('vplTotalTaxAmt');
                sObject docLine = createDocumentLine(dim1, dim3, taxCode, vplTotalAmt, vplTotalTaxAmt, 'VPL', totalAmt);
                accIdToDim2ToDocumentLineMap.get(accId).put('VPL', docLine);
            }   
        }

        insertDocumentHeaders();
        insertDocumentLines();
        linkDocumentToBInv(orderList);
        callPostBatch();
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Credit Note
        Inputs: Account, Date, Date, String, Date
        Returns: c2g__codaCreditNote__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static c2g__codaInvoice__c createSalesInvoice(Id accId, String customerType, Date invDate, Date dueDate, 
                                                            Date licenceStartDate, Date licenceEndDate, Decimal totalSurchargeAmt)
    {
        c2g__codaInvoice__c sInv = new c2g__codaInvoice__c();
        sInv.c2g__Account__c = accId;
        sInv.c2g__DeriveCurrency__c = true;
        sInv.c2g__DeriveDueDate__c = false;
        sInv.c2g__DerivePeriod__c = true;
        sInv.c2g__DueDate__c = dueDate;
        sInv.c2g__InvoiceDate__c = invDate;
        sInv.JVCO_Customer_Type__c = customerType;
        sInv.JVCO_Invoice_Type__c = accIdToInvoiceTypeMap.get(accId);
        sInv.JVCO_Licence_Start_Date__c = licenceStartDate;
        sInv.JVCO_Licence_End_Date__c = licenceEndDate;
        sInv.JVCO_Sales_Rep__c = UserInfo.getUserId();
        sInv.Total_Surchargeable_Amount__c = totalSurchargeAmt;
        return sInv;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Credit Note
        Inputs: Account, Date, Date, String, Date
        Returns: c2g__codaCreditNote__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static c2g__codaCreditNote__c createCreditNote(Id accId, Date invDate, Date dueDate, Date licenceStartDate, Date licenceEndDate)
    {
        c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
        cNote.c2g__Account__c = accId;
        cNote.c2g__CreditNoteDate__c = invDate;
        cNote.c2g__DeriveCurrency__c = true;
        cNote.c2g__DeriveDueDate__c = true;
        cNote.c2g__DerivePeriod__c = true;
        //cNote.c2g__DueDate__c = dueDate;
        cNote.JVCO_Credit_Reason__c = 'Invoice amendment'; 
        cNote.JVCO_Period_Start_Date__c = licenceStartDate;
        cNote.JVCO_Period_End_Date__c = licenceEndDate;
        cNote.JVCO_Sales_Rep__c = UserInfo.getUserId();
        cNote.JVCO_VAT_Registration_Number__c = userCompany.c2g__Company__r.c2g__VATRegistrationNumber__c;
        return cNote;
    }

    private static sObject createDocumentLine(Id dim1, Id dim3, Id taxCode, Decimal lineAmt, Decimal taxAmt, String type, Decimal totalAmt)
    {
        if(totalAmt >= 0)
        {
            return createSalesInvLineItem(dim1, dim3, taxCode, lineAmt, taxAmt, type);
        }else
        {
            return createCredNoteLineItem(dim1, dim3, taxCode, lineAmt, taxAmt, type);
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Sales Invoice
        Inputs: Id, Id, Decimal, String
        Returns: c2g__codaInvoiceLineItem__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static c2g__codaInvoiceLineItem__c createSalesInvLineItem(Id dim1, Id dim3, Id taxCode, Decimal amt, Decimal taxAmount, String type)
    {
        c2g__codaInvoiceLineItem__c sInvLine = new c2g__codaInvoiceLineItem__c();
        sInvLine.c2g__Dimension1__c = dim1;
        sInvLine.c2g__Dimension3__c = dim3;
        sInvLine.c2g__TaxCode1__c = taxCode;
        sInvLine.c2g__DeriveLineNumber__c = true;
        sInvLine.c2g__DeriveUnitPriceFromProduct__c = false;
        sInvLine.c2g__CalculateTaxValue1FromRate__c = false;
        sInvLine.c2g__TaxValue1__c = taxAmount.setScale(2);
        sInvLine.c2g__Quantity__c = 1;
        sInvLine.c2g__UnitPrice__c = amt;
        sInvLine.c2g__Dimension2__c = dim2Map.get(type);
        sInvLine.c2g__Product__c = prodMap.get(type);
        return sInvLine;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Credit Note Line
        Inputs: Id, Id, Decimal, String
        Returns: c2g__codaCreditNoteLineItem__c
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static c2g__codaCreditNoteLineItem__c createCredNoteLineItem(Id dim1, Id dim3, Id taxCode, Decimal amt, Decimal taxAmount, String type)
    {
        c2g__codaCreditNoteLineItem__c cNoteLine = new c2g__codaCreditNoteLineItem__c();
        cNoteLine.c2g__Dimension1__c = dim1;
        cNoteLine.c2g__Dimension3__c = dim3;
        cNoteLine.c2g__TaxCode1__c = taxCode;
        cNoteLine.c2g__DeriveLineNumber__c = true;
        cNoteLine.c2g__DeriveUnitPriceFromProduct__c = false;
        cNoteLine.c2g__CalculateTaxValueFromRate__c = false;
        cNoteLine.c2g__TaxValue1__c = (taxAmount * - 1).setScale(2);
        cNoteLine.c2g__Quantity__c = 1;
        cNoteLine.c2g__UnitPrice__c = amt * -1;
        cNoteLine.c2g__Dimension2__c = dim2Map.get(type);
        cNoteLine.c2g__Product__c = prodMap.get(type);
        return cNoteLine;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Computes for the Surcharge Generation Date
        Inputs: Decimal, String, Date, Date, Date, Decimal
        Returns: Date
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static void insertDocumentHeaders()
    {
        insert accIdToSInvMap.values();
        insert accIdToCNoteMap.values();
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Insert Document Lines
        Inputs: 
        Returns:
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static void insertDocumentLines()
    {
        List<sObject> docHeaderLineList = new List<sObject>();
        //Setup Document Line's Document Header's Id
        for(Id accId : accIdToDim2ToDocumentLineMap.keySet())
        {
            for(sObject sObj : accIdToDim2ToDocumentLineMap.get(accId).values())
            {
                //Sales Invoice Line
                if(sObj.getSObjectType() == Schema.c2g__codaInvoiceLineItem__c.SObjectType)
                {
                    ((c2g__codaInvoiceLineItem__c)sObj).c2g__Invoice__c = accIdToSInvMap.get(accId).Id;
                //Credit Note Line
                }else
                {
                    ((c2g__codaCreditNoteLineItem__c)sObj).c2g__CreditNote__c = accIdToCNoteMap.get(accId).Id;
                }
                docHeaderLineList.add(sObj);
            }
        }

        insert docHeaderLineList;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Link Document Header to Billing Header
        Inputs: List<blng__Invoice__c>
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static void linkDocumentToBInv(List<Order> orderList)
    {   
        List<OrderItem> orderItemList = [SELECT Id,
                                         JVCO_Sales_Invoice_Line_Item__c,
                                         JVCO_Sales_Credit_Note_Line_Item__c,
                                         Order.AccountId, 
                                         JVCO_Dimension_2__c
                                         FROM OrderItem
                                         WHERE OrderId IN : orderList];
        for(OrderItem orderLine : orderItemList)
        {
            
            sObject sObj = accIdToDim2ToDocumentLineMap.get(orderLine.Order.AccountId).get(orderLine.JVCO_Dimension_2__c);
            if(sObj.getSObjectType() == Schema.c2g__codaInvoiceLineItem__c.SObjectType)
            {
                orderLine.JVCO_Sales_Invoice_Line_Item__c = ((c2g__codaInvoiceLineItem__c)sObj).Id;
            }else
            {
                orderLine.JVCO_Sales_Credit_Note_Line_Item__c = ((c2g__codaCreditNoteLineItem__c)sObj).Id;
            }
        }

        update orderItemList;    
        
        for(Order ord : orderList)
        {
            if(accIdToSInvMap.containsKey(ord.AccountId))
            {
                Id sInvId = accIdToSInvMap.get(ord.AccountId).Id;
                ord.JVCO_Sales_Invoice__c = sInvId;
                sInvIdSet.add(sInvId);
            }else
            {
                Id cNoteId = accIdToCNoteMap.get(ord.AccountId).Id;
                ord.JVCO_Sales_Credit_Note__c = cNoteId;
                cNoteIdSet.add(cNoteId);
            }
        }
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Dimension2, Product, VatRegistration Mapping
        Inputs: 
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static void setDefaultMappings()
    {
        if(dim2Map == null)
        {
            dim2Map = new Map<String, Id>();
            for(c2g__codaDimension2__c dim2 : [SELECT Name, Id FROM c2g__codaDimension2__c
                                                WHERE Name IN ('PRS', 'PPL', 'VPL')])
            {
                dim2Map.put(dim2.Name, dim2.Id);
            }
        }
        
        if(prodMap == null)
        {
            prodMap = new Map<String, Id>();
            for(Product2 prod : [SELECT Id, Name FROM Product2 
                                    WHERE Name IN ('PPL Product', 'PRS Product', 'VPL Product')])
            {
                prodMap.put(prod.Name.substring(0,3), prod.Id);
            }
        }

        if(userCompany == null)
        {
            userCompany = [SELECT c2g__Company__c, c2g__Company__r.c2g__VATRegistrationNumber__c, 
                            c2g__User__c 
                            FROM c2g__codaUserCompany__c 
                            WHERE c2g__User__c =: UserInfo.getUserId()
                            LIMIT 1];
        }  
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Setup Sales Invoice Type Mapping
        Inputs: List<blng__Invoice__c>
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Sep-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static void setAccIdToInvoiceTypeMap(List<Order> orderList)
    {
        if(accIdToInvoiceTypeMap == null)
        {
            accIdToInvoiceTypeMap = new Map<Id, String>();
            for(Order ord : orderList)
            {
                if(!accIdToInvoiceTypeMap.containsKey(ord.AccountId))
                {
                    accIdToInvoiceTypeMap.put(ord.AccountId, ord.JVCO_Invoice_Type__c);
                }

                String invType = accIdToInvoiceTypeMap.get(ord.AccountId);
                if((invType == 'Pre-Contract' && ord.JVCO_Invoice_Type__c == 'Standard') ||
                    (invType == 'Supplementary' && (ord.JVCO_Invoice_Type__c == 'Standard' || ord.JVCO_Invoice_Type__c =='Pre-Contract')))
                {
                    accIdToInvoiceTypeMap.put(ord.AccountId, ord.JVCO_Invoice_Type__c);
                }
            }
        }
    }

    private static void callPostBatch()
    {
        //Run Post Batch
        if(!System.isBatch() && !System.isFuture() && !System.isQueueable())
        {
            JVCO_PostSalesInvoiceBatch pSIB = new JVCO_PostSalesInvoiceBatch(sInvIdSet, cNoteIdSet);
            psIB.stopMatching = false;
            Database.executeBatch(pSIB, 50);
        }    
    }
}