/* --------------------------------------------------------------------------------------------------
    Name:            JVCO_PaymentCollectionScheduler.cls 
    Description:     Class that handles 1.  JVCO_BackgroundMatching_Batch Schedulable APEX class as per JIRA ticket GREEN-17932
    Test class:      JVCO_PaymentCollectionScheduler.cls 

    Date                 Version     Author                          Summary of Changes 
    -----------          -------     -----------------               -------------------------------------------
    03-Aug-2017           0.1         mel.andrei.b.santos               Intial draft
------------------------------------------------------------------------------------------------ */
public class JVCO_PaymentCollectionScheduler implements Schedulable
{
    private final Decimal BGMATCHINGSIZE = JVCO_General_Settings__c.getInstance().JVCO_Background_Matching_Size__c;
    
    public void execute(SchedulableContext sc)
    {        
        Database.executeBatch(new JVCO_BackgroundMatching_Batch(), Integer.valueOf(BGMATCHINGSIZE));
    }
    
    private static String getSchedulerExpression(DateTime dt) 
    {
         return '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();    
    }

    public static void callExecuteMomentarily(String batchName)  
    {
        System.schedule(batchName, getSchedulerExpression(DateTime.Now().addSeconds(5)), new JVCO_PaymentCollectionScheduler());
    }
}