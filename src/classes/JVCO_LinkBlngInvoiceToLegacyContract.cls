/* ----------------------------------------------------------------------------------------------
Name: JVCO_LinkBlngInvoiceToLegacyContractVer.cls 
Description: Batch class that handles Legacy Contract in the Invoice to the Contract Number 
                This is another version to JVCO_LinkBlngInvoiceToLegacyContract.cls
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
03-Mar-2017  0.1         franz.g.a.dimaapi     Intial creation
27-Mar-2017  0.11        franz.g.a.dimaapi     Updated the legacy contracts to have a semicolon 
separation
24-Jul-2017  0.111       mary.ann.a.ruelan     Updated the init method to avoid duplicates in the updatedBlngInvoiceList
05-Sept-2017 0.2         desiree.m.quijada     Added logic to capture errors and log the record in the error log object
----------------------------------------------------------------------------------------------- */
global class JVCO_LinkBlngInvoiceToLegacyContract implements Database.Batchable<sObject>, Database.Stateful
{
    
    private static List<blng__Invoice__c> blngInvoiceList;
    private static List<blng__Invoice__c> updatedBlngInvoiceList = new List<blng__Invoice__c> ();
    
    
    private static void init()
    {
        
        Map<String, String> TempExtIdtoContractNoMap = new Map<String, String>();
        List<String> TempLegacyContractsList = new List<String> ();
        
        //creates a list of all temp legacy contracts in the JVCO_Temp_Legacy_Contracts__c field separated by a semicolon
        for(blng__Invoice__c blngInvoice : blngInvoiceList)
        {            
            for(String legacyContract : blngInvoice.JVCO_Temp_Legacy_Contracts__c.split(';'))
            {
                TempLegacyContractsList.add(legacyContract);
            }
            
        }   
        
        //gets all contact where JVCO_Contract_Temp_External_ID__c is found in TempLegacyContractsList
        List<Contract> contractList = [SELECT Id, Name, contractNumber, JVCO_Contract_Temp_External_ID__c FROM Contract
                                       WHERE JVCO_Contract_Temp_External_ID__c IN :TempLegacyContractsList];
        
        
        //creates a MAP of the JVCO_Contract_Temp_External_ID__c to the contract number
        for(Contract contract: contractList){
            TempExtIdtoContractNoMap.put(contract.JVCO_Contract_Temp_External_ID__c, contract.contractNumber);
        }               
        
        //
        for(blng__Invoice__c blngInvoice : blngInvoiceList)
        {
            string oldLegacyContract = blngInvoice.JVCO_Legacy_Contracts__c;
            for(String legacyContract : blngInvoice.JVCO_Temp_Legacy_Contracts__c.split(';'))
            {
                if(TempExtIdtoContractNoMap.containsKey(legacyContract)){
                    
                    if (String.ISBLANK(blngInvoice.JVCO_Legacy_Contracts__c)){
                        blngInvoice.JVCO_Legacy_Contracts__c=(TempExtIdtoContractNoMap.get(legacyContract)+';');
                    }
                    //only adds the legacy contract to the field if it is not there yet to avoid duplicates/ or unnecessary repopulating the field when this batch class is run several times
                    else if (!blngInvoice.JVCO_Legacy_Contracts__c.contains(TempExtIdtoContractNoMap.get(legacyContract))) {
                        blngInvoice.JVCO_Legacy_Contracts__c+=(TempExtIdtoContractNoMap.get(legacyContract)+';');
                    }
                }                
            }
             
            //only adds the invoice to the updatedlist if the JVCO_Legacy_Contracts__c is changed to reduce the number of records to include in UPDATE
            if (blngInvoice.JVCO_Legacy_Contracts__c!=oldLegacyContract){
                updatedBlngInvoiceList.add(blngInvoice);
            }
           
        }
       
          
    }
    
    //this method updates the invoices in the database
    private static void updateBlngInvoiceLegacyContract()
    {
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();

        if(updatedBlngInvoiceList.size() > 0)
        {            
            //update updatedBlngInvoiceList;
            List<Database.SaveResult> res = Database.update(updatedBlngInvoiceList,false);

            for(Integer i=0; i<updatedBlngInvoiceList.size();i++)
            {
                Database.SaveResult indRes = res[i];
                blng__Invoice__c indBlngInvoice = updatedBlngInvoiceList[i];

                if(!indRes.isSuccess())
                {
                    for(Database.Error err: indRes.getErrors())
                    {
                        ffps_custRem__Custom_Log__c newErr = new ffps_custRem__Custom_Log__c();
                        if(indBlngInvoice.ID != null || !String.isBlank(String.valueOf(indBlngInvoice.ID)))
                        {
                            if(err.getStatusCode() != null || !String.isBlank(String.valueOf(err.getStatusCode())))
                            {
                                if(err.getStatusCode() != null || !String.isBlank(String.valueOf(err.getStatusCode())))
                                {
                                    newErr.ffps_custRem__Running_User__c = UserInfo.getUserId();
                                    newErr.ffps_custRem__Related_Object_Key__c= String.valueOf(indBlngInvoice.ID);
                                    newErr.ffps_custRem__Detail__c = String.valueof(err.getMessage());
                                    newErr.ffps_custRem__Message__c = 'Link Billing Invoice To Legacy Contract';
                                    newErr.ffps_custRem__Grouping__c = String.valueof(err.getStatusCode());
                                }
                            }
                            errLogList.add(newErr);
                        }
                    }
                }
                else{
                    System.debug('Successfully updated');
                }

            }
        }        
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        return Database.getQueryLocator([SELECT Id, Name, JVCO_Temp_Legacy_Contracts__c, 
                                         JVCO_Legacy_Contracts__c
                                         FROM blng__Invoice__c
                                         WHERE JVCO_Temp_Legacy_Contracts__c 
                                         NOT IN ('', NULL)]);      
        
        
    }
    
    global void execute(Database.BatchableContext bc, List<blng__Invoice__c> scope)
    {        
        blngInvoiceList = scope;        
        if (blngInvoiceList.size()>0)
        {
            init();
            updateBlngInvoiceLegacyContract(); 
        }     
        
    }
    
    global void finish(Database.BatchableContext BC) 
    {                   
        System.debug('DONE');    
    }
}