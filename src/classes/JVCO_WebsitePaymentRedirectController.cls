/* ----------------------------------------------------------------------------------------------
   Name: JVCO_WebsitePaymentRedirectController.cls 
   Description: Handles the redirection of website payments

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  20-Mar-2018   0.1         jasper.j.figueroa  Intial creation - fix for GREEN-27737
  ----------------------------------------------------------------------------------------------- */
public class JVCO_WebsitePaymentRedirectController
{
    public string paymentRecordId{ get; set; }
    public string accountNumberValue{ get; set; }
    public string invoiceNumberValue{ get; set; }
    public string postCodeValue{ get; set; }
    public string referenceString{ get; set; }
    public static List<JVCO_Website_Payment_Setting__c> settings{ get; set; }

    public JVCO_WebsitePaymentRedirectController(ApexPages.StandardController ssc)
    {
        paymentRecordId = ApexPages.currentPage().getParameters().get('id');
        accountNumberValue= ApexPages.currentPage().getParameters().get('actNum');
        invoiceNumberValue= ApexPages.currentPage().getParameters().get('invn');
        postCodeValue= ApexPages.currentPage().getParameters().get('pCode');
    }
/* ----------------------------------------------------------------------------------------------
        Author: jasper.j.figueroa
        Company: Accenture
        Description: Sets the next page to be rendered
        Inputs: N/A
        Returns: N/A
        <Date>      <Authors Name>      <Brief Description of Change> 
        20-Mar-2018 jasper.j.figueroa     Initial version of function
----------------------------------------------------------------------------------------------- */ 
    public PageReference init()
    {
        Map<string, JVCO_Website_Payment_Setting__c> allData = JVCO_Website_Payment_Setting__c.getAll();
        settings = allData.values();
        PAYBASE2__Payment__c payment = [SELECT Id, PAYBASE2__Is_Paid__c, PAYSPCP1__Redirect_URL__c FROM PAYBASE2__Payment__c WHERE Id =: paymentRecordId];
        if(payment!=null)
        {
            if(payment.PAYBASE2__Is_Paid__c)
            {
                referenceString = settings[0].ThanksPageURL__c;
            }
            else
            {
                referenceString = 'https://'+ settings[0].SiteURL__c;
            }
        }
        system.debug('@@@@referenceString: ' + referenceString);
        PageReference pr = new  PageReference(referenceString);
        pr.getParameters().put('actNumber', accountNumberValue);
        pr.getParameters().put('invn', invoiceNumberValue);
        pr.getParameters().put('pCode', postCodeValue);
        return pr;
    }
}