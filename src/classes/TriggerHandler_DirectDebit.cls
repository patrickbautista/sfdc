public with sharing class TriggerHandler_DirectDebit {
    public static void OnBeforeInsert(List<Income_Direct_Debit__c> newRecords){
        
    }

    public static void OnAfterInsert(Map<Id, Income_Direct_Debit__c> newRecords){

       List<Id> ddsToUpdate = new List<Id>();

       for (Income_Direct_Debit__c DD : newRecords.values()) 
       {
            if(DD.DD_Collection_Reference__c == 'PPL PRS Web Direct Debit')
            {   
                ddsToUpdate.add(DD.Id); 
            }
       }
       List<c2g__codaInvoice__c> invoicesforUpdate = new List<c2g__codaInvoice__c>();

    //    List<SMP_DirectDebit_GroupInvoice__c> ddGroupInvoices = [SELECT Id, Income_Direct_Debit__c,Income_Direct_Debit__r.SalesInvoiceList__c, Invoice_Group__c FROM SMP_DirectDebit_GroupInvoice__c WHERE Income_Direct_Debit__c =: ddsToUpdate];
    //    for (SMP_DirectDebit_GroupInvoice__c groupInvoice : ddGroupInvoices) 
    //    {
    //        if(groupInvoice.Source__c == 'Web'){
    //             String[] split = groupInvoice.Income_Direct_Debit__r.SalesInvoiceList__c.split(',');

    //             for(Integer j = 0; j < split.size(); j++)
    //             {
    //                 c2g__codaInvoice__c invoice = new c2g__codaInvoice__c();

    //                 invoice.Id = split[j];
    //                 invoice.JVCO_Invoice_Group__c = groupInvoice.Invoice_Group__c;
    //                 invoice.JVCO_Payment_Method__c = 'Direct Debit';

    //                 invoicesforUpdate.add(invoice);

    //             }
    //         }

    //    }
       
    //        update invoicesforUpdate;
    }

    public static void OnBeforeUpdate(Map<Id, Income_Direct_Debit__c> newRecords, Map<Id, Income_Direct_Debit__c> oldRecords){
        system.debug('##### OnBeforeUpdate newRecords' + newRecords);
        List<SMP_DirectDebit_GroupInvoice__c> ddGroupInvoiceList = new List<SMP_DirectDebit_GroupInvoice__c>();
        ddGroupInvoiceList = [SELECT Id, Invoice_Group__c, Income_Direct_Debit__c FROM SMP_DirectDebit_GroupInvoice__c WHERE Income_Direct_Debit__c IN: newRecords.keySet()];
        List<Income_Debit_History__c> debitHistoryList = [SELECT Id, Income_Direct_Debit__c, DD_Collection_Date__c FROM Income_Debit_History__c WHERE Income_Direct_Debit__c IN: newRecords.keySet()];

        Set<Id> igSet = new Set<Id>();
        for(SMP_DirectDebit_GroupInvoice__c idd: ddGroupInvoiceList){
            igSet.add(idd.Invoice_Group__c);
        }

        List<c2g__codaInvoice__c> invoiceList = new List<c2g__codaInvoice__c>();
        invoiceList = [SELECT Id, c2g__Account__c, JVCO_Invoice_Group__c FROM c2g__codaInvoice__c WHERE JVCO_Invoice_Group__c IN :igSet];

        List<c2g__codaInvoiceInstallmentLineItem__c> iLIList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
        iLIList = [SELECT c2g__Amount__c, JVCO_Payment_Status__c, 
                   c2g__DueDate__c, c2g__Invoice__c, 
                   c2g__Invoice__r.JVCO_Invoice_Group__c,
                   JVCO_DD_Failed__c
                   FROM c2g__codaInvoiceInstallmentLineItem__c 
                   WHERE c2g__Invoice__r.JVCO_Invoice_Group__c IN: igSet
                   AND JVCO_Payment_Status__c = 'Unpaid' 
                   AND (c2g__DueDate__c > :Date.Today() 
                        OR JVCO_DD_Failed__c = true)
                  ORDER BY c2g__DueDate__c ASC];

                                                                                    
        List<Income_Direct_Debit__c> directDebitsToupdateList = new List<Income_Direct_Debit__c>();
        List<ddWrapper> ddWrapperList = new List<ddWrapper>();
        for(Income_Direct_Debit__c dd : newRecords.values()){
            Decimal leftToCollect = 0;
            ddWrapper wrapper = new ddWrapper();
            wrapper.directDebit = dd;
            if(dd.Send_DD_Documentation__c != null){
                wrapper.directDebit.Send_DD_Documentation__c = dd.Send_DD_Documentation__c;
            }
            List<JVCO_Invoice_Group__c> igList = new List<JVCO_Invoice_Group__c>();
            List<c2g__codaInvoiceInstallmentLineItem__c> liLIst = new List<c2g__codaInvoiceInstallmentLineItem__c>();

            for(SMP_DirectDebit_GroupInvoice__c gi : ddGroupInvoiceList){
                if(gi.Income_Direct_Debit__c == dd.Id){
                    JVCO_Invoice_Group__c ig = new JVCO_Invoice_Group__c();
                    ig.Id = gi.Invoice_Group__c;
                    igList.add(ig);
                    for(c2g__codaInvoiceInstallmentLineItem__c ili :iLIList){
                        System.debug('illi' + ili);
                        if(ili.c2g__Invoice__r.JVCO_Invoice_Group__c == gi.Invoice_Group__c){
                            leftToCollect += ili.c2g__Amount__c;
                            liLIst.add(ili);
                        }
                    }
                }
            }
            List<Income_Debit_History__c> idhList = New List<Income_Debit_History__c>();
            for(Income_Debit_History__c ddh :debitHistoryList){
                if(ddh.Income_Direct_Debit__c == dd.Id){
                    idhList.add(ddh);
                }
            }
            wrapper.debitHistoryList = idhList;
            wrapper.iliList = liLIst;
            wrapper.invoiceGroupList = igList;
            wrapper.amountLeftToCollect = leftToCollect;
            ddWrapperList.add(wrapper);
        }
        system.debug('##### OnBeforeUpdate');

        for(ddWrapper wrapper :ddWrapperList){
            Date lastCollectionDate = null;
            Date nextCollectionDate = null;
            Date ddhDate = null;
            Integer countOngoingCollection = 0;
            Decimal finalCollectionAmount = 0;
            Decimal firstCollectionAmount = 0;
           // System.debug('ddhDate' + wrapper.debitHistoryList);
           // System.debug('ddhDate' + wrapper.directDebit.DD_Next_Collection_Date__c);
            Integer i = 0;
            for(Income_Debit_History__c idh :wrapper.debitHistoryList){
                if(i == 0){
                    ddhDate = idh.DD_Collection_Date__c;
                    System.debug('ddhDate' + idh.DD_Collection_Date__c);
                    lastCollectionDate = idh.DD_Collection_Date__c;
                }
                if(ddhDate < idh.DD_Collection_Date__c){
                    ddhDate = idh.DD_Collection_Date__c;
                    System.debug('ddhDate' + idh.DD_Collection_Date__c);
                    lastCollectionDate = idh.DD_Collection_Date__c;
                }
                i++;
            }
            System.debug('ddhDatenext' + nextCollectionDate);
            i = 0;
            for(c2g__codaInvoiceInstallmentLineItem__c li :wrapper.iliList){
                System.debug('iliDate' + li.c2g__DueDate__c);
                // if(lastCollectionDate == null || (lastCollectionDate < li.c2g__DueDate__c && !li.JVCO_DD_Failed__c)){
                //     lastCollectionDate = li.c2g__DueDate__c;
                // }
                if((li.c2g__DueDate__c > lastCollectionDate) && i < 1 && !li.JVCO_DD_Failed__c){
                    nextCollectionDate = li.c2g__DueDate__c;
                    System.debug('nextddhDate' + nextCollectionDate);
                    countOngoingCollection++;
                    break;
                }
            }
            for(c2g__codaInvoiceInstallmentLineItem__c li :wrapper.iliList){
                if((li.c2g__DueDate__c == lastCollectionDate && !li.JVCO_DD_Failed__c)  
                   || (countOngoingCollection == 1 && li.JVCO_DD_Failed__c && wrapper.directDebit.DD_Collection_Period__c == 'Weekly')){
                    finalCollectionAmount += li.c2g__Amount__c;
                }
                if((li.c2g__DueDate__c == nextCollectionDate && !li.JVCO_DD_Failed__c)
                   || (countOngoingCollection > 0 && li.JVCO_DD_Failed__c && wrapper.directDebit.DD_Collection_Period__c == 'Weekly')){
                    firstCollectionAmount += li.c2g__Amount__c;
                }
            }
            wrapper.finalPaymentAmount = finalCollectionAmount;
            wrapper.nextCollectionAmount = firstCollectionAmount;
            wrapper.directDebit.DD_Final_Collection_Amount__c = finalCollectionAmount;
            wrapper.directDebit.DD_Ongoing_Collection_Amount__c = firstCollectionAmount;
            wrapper.directDebit.DD_First_Collection_Amount__c = firstCollectionAmount;
            wrapper.directDebit.Amount_Left_To_Collect__c = wrapper.amountLeftToCollect;
            wrapper.directDebit.DD_Next_Collection_Date__c = nextCollectionDate;
            if(wrapper.directDebit.DD_Status__c == 'New Instruction'){
                wrapper.directDebit.DD_First_Collection_Date__c = nextCollectionDate;
            }
            System.debug('ddupdate' + wrapper.directDebit.Send_DD_Documentation__c);

            directDebitsToupdateList.add(wrapper.directDebit);
        }
        System.debug('##directDebitsToupdateList' + directDebitsToupdateList);

    }
	
    public static void OnAfterUpdate(Map<Id, Income_Direct_Debit__c> newRecords, Map<Id, Income_Direct_Debit__c> oldRecords){
        system.debug(LoggingLevel.Error,'My Debug Statement');
        Map<string, Schema.RecordTypeInfo> recordTypes = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName();
        Map<Id, Contact> contactsToUpdateList = new Map<Id, Contact>();

        for(Income_Direct_Debit__c dd : newRecords.values()){
            if(dd.DD_Status__c == 'Cancelled by Payer' && oldRecords.get(dd.Id).DD_Status__c != 'Cancelled by Payer'
            || dd.DD_Status__c == 'Cancelled by Originator' && oldRecords.get(dd.Id).DD_Status__c != 'Cancelled by Originator'
            || dd.DD_Status__c == 'Cancelled' && oldRecords.get(dd.Id).DD_Status__c != 'Cancelled'){
                if(contactsToUpdateList.get(dd.Contact__c) == null){
                    contactsToUpdateList.put(dd.Contact__c, new Contact(Id = dd.Contact__c));
                }
            }
        }

        
    }
    
    public class ddWrapper{
        public Income_Direct_Debit__c directDebit {get; set;}
        public List<JVCO_Invoice_Group__c> invoiceGroupList {get; set;}
        public List<c2g__codaInvoiceInstallmentLineItem__c> iliList {get; set;}
        public Decimal finalPaymentAmount {get; set;}
        public Decimal nextCollectionAmount {get; set;}
        public Decimal amountLeftToCollect {get; set;}
        public List<Income_Debit_History__c> debitHistoryList {get; set;}
    }
}