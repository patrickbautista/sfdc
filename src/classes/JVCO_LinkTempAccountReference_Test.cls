/* ----------------------------------------------------------------------------------------------
Author: kristoffer.d.martin
Company: Accenture
Description: Test Class for JVCO_LinkTempAccountReference
<Date>      <Authors Name>       <Brief Description of Change> 
11-Apr-2017 kristoffer.d.martin  Initial version
----------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_LinkTempAccountReference_Test {
	
	/* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Sets up the needed records for testing
    Inputs: N/
    Returns: voidA
    <Date>      <Authors Name>       <Brief Description of Change> 
    11-Apr-2017 kristoffer.d.martin  Initial version of function
	----------------------------------------------------------------------------------------------- */
 	@testSetup static void createTestData() 
    {
		Group testGroup = new Group(Name='test group', Type='Queue');
		insert testGroup;
		QueuesObject testQueue ; 
		System.runAs(new User(Id=UserInfo.getUserId()))
		{
			List<queuesobject >  listQueue = new List<queuesobject >();
		   	queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
			listQueue.add(q1);
			queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
			listQueue.add(q2);
			queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
			listQueue.add(q3);
			queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
			listQueue.add(q4);
			queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
			listQueue.add(q5);
			queuesobject q6 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankAccount__c'); 
			listQueue.add(q6);
			queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
			listQueue.add(q7);
			queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatement__c'); 
			listQueue.add(q8);			
			queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntry__c'); 
			listQueue.add(q9);	
			insert listQueue;

			GroupMember GroupMemberObj = new GroupMember();
			GroupMemberObj.GroupId = testGroup.id;
			GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
			insert GroupMemberObj;
		}

		c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
		testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
		testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
		testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
		testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
		testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
		testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
		testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
		testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
		testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
		testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
		testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
		testGeneralLedgerAcc.Dimension_1_Required__c = false;
		testGeneralLedgerAcc.Dimension_2_Required__c = false;
		testGeneralLedgerAcc.Dimension_3_Required__c = false;
		testGeneralLedgerAcc.Dimension_4_Required__c = false;
		testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
		testGeneralLedgerAcc.ownerid = testGroup.Id;

		insert testGeneralLedgerAcc;
		//Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
		
        c2g__codaTaxCode__c taxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedgerAcc.id);
        insert taxCode;

		//Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }
		
		//Create Account
        Account testAccCust = new Account(Name='Test Customer Account');
        testAccCust.AccountNumber = '999999';
        testAccCust.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust.Type='Customer';
        testAccCust.RecordTypeId = accountRecordTypes.get('Customer Account');
        insert testAccCust;

         Account testAcc = JVCO_TestClassObjectBuilder.createLicenceAccount(testAccCust.id);
        testAcc.AccountNumber = '987654';
        testAcc.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAcc.c2g__CODAAllowDeleteInUse__c = false;
        testAcc.c2g__CODACreditLimitEnabled__c = false;
        testAcc.c2g__CODACreditStatus__c = 'Credit Allowed';
        testAcc.c2g__CODADaysOffset1__c = 0.0;
        testAcc.c2g__CODADiscount1__c = 0.0;
        testAcc.c2g__CODAFederallyReportable1099__c = false;
        testAcc.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testAcc.c2g__CODAIntercompanyAccount__c = false;
        testAcc.c2g__CODASalesTaxStatus__c = 'Taxable';
        insert testAcc;

        c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
        testCompany.c2g__CashMatchingCurrencyMode__c = 'Account';
        testCompany.c2g__Country__c = 'GB';
        testCompany.c2g__ECCountryCode__c = 'GB';
        testCompany.c2g__UnitOfWork__c = 1.0;
        testCompany.Name = 'Test Company';
        testCompany.ownerid = testGroup.Id;

        insert testCompany;

		c2g__codaAccountingCurrency__c accCurrency = new c2g__codaAccountingCurrency__c();
		accCurrency.c2g__OwnerCompany__c = testCompany.id;
		accCurrency.c2g__DecimalPlaces__c = 2;
		accCurrency.Name = 'GBP';
		accCurrency.c2g__Dual__c = true ;
		accCurrency.ownerid = testGroup.Id;
		accCurrency.c2g__Home__c = true;
		accCurrency.c2g__UnitOfWork__c = 2.0;
		insert accCurrency;

        c2g__codaBankAccount__c testBankAccount = new c2g__codaBankAccount__c();
        testBankAccount.c2g__AccountName__c = 'Test Bank Account';
        testBankAccount.c2g__AccountNumber__c = '987654';
        testBankAccount.c2g__BankName__c = 'Barclays Bank - Current';
        testBankAccount.c2g__GeneralLedgerAccount__c = testGeneralLedgerAcc.id;
        testBankAccount.Name = 'Test Bank Account 1';
        testBankAccount.c2g__OwnerCompany__c = testCompany.Id;
        testBankAccount.c2g__ReportingCode__c = 'BARC-CURR-JV';
        testBankAccount.c2g__BankAccountCurrency__c = accCurrency.id;
        insert testBankAccount;

        testCompany.c2g__BankAccount__c = testBankAccount.id;
        update testCompany;

		c2g__codaYear__c yr= new c2g__codaYear__c();
		yr.Name ='2016';
		yr.c2g__AutomaticPeriodList__c =  true;
		yr.c2g__OwnerCompany__c = testCompany.id;
		yr.c2g__ExternalId__c = 'yzsd1234';
		yr.c2g__NumberOfPeriods__c =11;
		yr.c2g__StartDate__c =  system.today() - 10;
		yr.c2g__Status__c = 'Open';
		yr.c2g__PeriodCalculationBasis__c = '445';
		yr.c2g__YearEndMode__c = 'Full Accounting Code' ; 
		yr.c2g__UnitOfWork__c = 12;
		yr.ownerid = testGroup.Id;
		insert yr;

        c2g__codaPeriod__c testPeriod = new c2g__codaPeriod__c();
        testPeriod.c2g__OwnerCompany__c = testCompany.id;
        testPeriod.c2g__StartDate__c = System.today()-10;
        testPeriod.c2g__EndDate__c = System.today()+10;
        testPeriod.c2g__PeriodNumber__c ='123';
		testPeriod.c2g__Description__c ='test Desc';
		testPeriod.c2g__PeriodGroup__c = 'Q1';
		testPeriod.c2g__PeriodNumber__c = '1';
		testPeriod.c2g__YearName__c = yr.id;
        insert testPeriod;

		c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c();
		userCompany.c2g__Company__c = testCompany.id;
		userCompany.c2g__User__c = userInfo.getUserId();
		userCompany.c2g__ExternalId__c = 'ABCDE1234567876';
		userCompany.c2g__UnitOfWork__c = 111 ;
		insert userCompany;

        //Create Sales Invoice
        c2g__codaInvoice__c testInvoice = new c2g__codaInvoice__c();
        testInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testInvoice.c2g__InvoiceDate__c = date.today().addDays(-7);
        testInvoice.c2g__DueDate__c = date.today().addDays(-7);
        testInvoice.c2g__Account__c = testAcc.Id;
        testInvoice.JVCO_Customer_Type__c = 'New Business';
        testInvoice.c2g__OwnerCompany__c = testCompany.id;
        testInvoice.ownerid = testGroup.Id;
        testInvoice.JVCO_Invoice_Legacy_Number__c = 'SIN00001';
        insert testInvoice;

        //Create Cash Entry
        c2g__codaCashEntry__c testCashEntry = new c2g__codaCashEntry__c();
        testCashEntry.c2g__PaymentMethod__c = 'Electronic';
        testCashEntry.c2g__OwnerCompany__c = testCompany.id;
        testCashEntry.ownerid = testGroup.Id;
        testCashEntry.c2g__Status__c = 'In Progress';
        testCashEntry.c2g__Date__c = Date.today();
        testCashEntry.c2g__Value__c = 0.00;
        insert testCashEntry;

        List<c2g__codaInvoice__c> salesInvoiceList = [SELECT Id, Name, c2g__Transaction__c, CreatedBy.name from c2g__codaInvoice__c];
        //system.assertEquals(null, salesInvoiceList);

        //Create Cash Entry Line Item
        c2g__codaCashEntryLineItem__c cashEntryLineItem1 = new c2g__codaCashEntryLineItem__c();
        cashEntryLineItem1.c2g__CashEntry__c = testCashEntry.id;
        cashEntryLineItem1.c2g__Account__c = testAccCust.id;
        cashEntryLineItem1.c2g__CashEntryValue__c = 15000;
        cashEntryLineItem1.c2g__LineNumber__c = 1;
        cashEntryLineItem1.c2g__AccountReference__c = salesInvoiceList[0].Name;
        cashEntryLineItem1.c2g__AccountPaymentMethod__c = 'Electronic';
        cashEntryLineItem1.c2g__OwnerCompany__c = testCompany.id;
        cashEntryLineItem1.JVCO_Temp_AccountReference__c = 'SIN00001';
        insert cashEntryLineItem1;

	}

	/* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Call the JVCO_LinkTempAccountReference Batch
    Inputs: N/
    Returns: voidA
    <Date>      <Authors Name>       <Brief Description of Change> 
    11-Apr-2017 kristoffer.d.martin  Initial version of function
	----------------------------------------------------------------------------------------------- */
	@isTest static void JVCO_LinkTempAccountReferenceBatch_test()
    {   
        Id LicenceRT =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
		Group testGroup = [SELECT Id, Name FROM Group order by CreatedDate DESC limit 1];
        Account testAcc = [SELECT Id, Name FROM Account WHERE RecordTypeId = :LicenceRT order by CreatedDate DESC limit 1];
        c2g__codaCompany__c testCompany = [SELECT Id, Name FROM c2g__codaCompany__c order by CreatedDate DESC limit 1];
        c2g__codaAccountingCurrency__c testAccountingCurrency = [SELECT Id, Name FROM c2g__codaAccountingCurrency__c order by CreatedDate DESC limit 1];
        c2g__codaBankAccount__c testBankAccount1 = [SELECT Id, Name FROM c2g__codaBankAccount__c order by CreatedDate DESC limit 1];
        c2g__codaPeriod__c testPeriod1 = [SELECT Id, Name FROM c2g__codaPeriod__c order by CreatedDate DESC limit 1];

        c2g.CODAAPICommon_7_0.Context contextCashEntry = new c2g.CODAAPICommon_7_0.Context();
		c2g.CODAAPICommon.Reference companyRef = c2g.CODAAPICommon.getRef(testCompany.Id, null); 
		c2g.CODAAPICommon.Reference ownerRef = c2g.CODAAPICommon.getRef(testGroup.Id, null); 
		c2g.CODAAPICommon.Reference testAccRef = c2g.CODAAPICommon.getRef(testAcc.Id, null); 
		c2g.CODAAPICommon.Reference accountingCurrencyRef = c2g.CODAAPICommon.getRef(testAccountingCurrency.Id, null); 
		c2g.CODAAPICommon.Reference testBankAccount1Ref = c2g.CODAAPICommon.getRef(testBankAccount1.Id, null); 
		c2g.CODAAPICommon.Reference testPeriod1Ref = c2g.CODAAPICommon.getRef(testPeriod1.Id, null); 

		//c2g.CODAAPICashEntryLineItemTypes_7_0.CashEntryLineItems lineItems = c2g.CODAAPICashEntryLineItemTypes_7_0.CashEntryLineItems>();

		//Create Cash Entry
        c2g.CODAAPICashEntryTypes_7_0.CashEntry cashEntryValue = new c2g.CODAAPICashEntryTypes_7_0.CashEntry();
        cashEntryValue.PaymentMethod = 'Electronic';
        cashEntryValue.OwnerCompany = companyRef;
        cashEntryValue.OwnerId = ownerRef;
        cashEntryValue.Status = c2g.CODAAPICashEntryTypes_7_0.enumStatus.InProgress;
        cashEntryValue.DateValue = Date.today();
        cashEntryValue.Value = 220.00;
        cashEntryValue.BankAccountCurrency = 'GBP';
        cashEntryValue.BankAccount = testBankAccount1Ref;
        cashEntryValue.CashEntryCurrency = accountingCurrencyRef;
        cashEntryValue.Period = testPeriod1Ref;
        cashEntryValue.TypeRef = c2g.CODAAPICashEntryTypes_7_0.enumType.Receipt;

        List<c2g__codaInvoice__c> salesInvoiceList = [SELECT Id, Name, c2g__Transaction__c from c2g__codaInvoice__c];

		c2g.CODAAPICommon.Reference cashEntryValueRef = c2g.CODAAPICashEntry_7_0.CreateCashEntry(contextCashEntry, cashEntryValue); 

        //Create Cash Entry Line Item
        c2g.CODAAPICashEntryLineItemTypes_7_0.CashEntryLineItem cashEntryLineItem = new c2g.CODAAPICashEntryLineItemTypes_7_0.CashEntryLineItem();
        cashEntryLineItem.CashEntry = cashEntryValueRef;
        cashEntryLineItem.Account = testAccRef;
        cashEntryLineItem.CashEntryValue = 220.00;
        cashEntryLineItem.PaymentValue = 220.00;
        cashEntryLineItem.AccountPaymentMethod = 'Electronic';
        cashEntryLineItem.PaymentCurrency = accountingCurrencyRef;
        cashEntryLineItem.OwnerCompany = companyRef;
        //cashEntryLineItem.JVCO_Temp_AccountReference__c = 'SIN00001';

        //cashEntryValue.LineItem = cashEntryLineItem;

		c2g.CODAAPICashEntry_7_0.UpdateCashEntry(contextCashEntry, cashEntryValue);

		//c2g__codaCashEntryLineItem__c testCashEntryLine = [SELECT Id, Name FROM c2g__codaCashEntryLineItem__c order by CreatedDate DESC limit 1];
		//testCashEntryLine.JVCO_Temp_AccountReference__c = 'SIN00001';
		//update testCashEntryLine;

		Test.startTest();

        
        
        
		JVCO_LinkTempAccountReference linkTempAccRef = new JVCO_LinkTempAccountReference();
		Database.executeBatch(linkTempAccRef);

		Test.stopTest();

        c2g__codaCashEntryLineItem__c cashEntryLineItem2 = [SELECT Id, Name, c2g__AccountReference__c
        												   FROM c2g__codaCashEntryLineItem__c
        												   order by CreatedDate DESC limit 1];

        c2g__codaInvoice__c invoice = [SELECT Id, Name 
        							   FROM c2g__codaInvoice__c 
        							   order by CreatedDate DESC limit 1];

        System.assertEquals(cashEntryLineItem2.c2g__AccountReference__c, invoice.Name);
    }
}