/* ----------------------------------------------------------------------------------------------
    Name: JVCO_LinkBlngLineToSInvLine
    Description: Batch for Linking Blng Line to Sales Line

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    28-Dec-2017  0.1         franz.g.a.dimaapi     Intial creation
    20-Jan-2018  0.2         franz.g.a.dimaapi     GREEN-27518 - Refactor Class
    19-Mar-2018  0.3         franz.g.a.dimaapi     GREEN-30762 - Fix Duedate issue for different payment terms
    27-Mar-2018  0.4         franz.g.a.dimaapi     GREEN-31118 - Add Order Groups for Scheduler
    22-May-2018  0.5         franz.g.a.dimaapi     GREEN-31946 - Fixed Sales Invoice Invoice Type
    06-Jul-2018  0.6         joseph.g.barrameda    GREEN-32626 - Remove Stateful
----------------------------------------------------------------------------------------------- */
public class JVCO_LinkBlngLineToSInvAndCnoteLineBatch implements Database.Batchable<sObject>{

    private final JVCO_General_Settings__c GeneralSettings = JVCO_General_Settings__c.getInstance();
    private final Decimal SALESINVOICESCOPE = GeneralSettings != null ? GeneralSettings.JVCO_Post_Sales_Invoice_Scope__c : 20;

    // Billing Invoice to Sales Invoice / Credit Note Map
    private Map<Id, Id> bInvIdToDocHeaderIdMap;
    // Sales Invoice Id to Invoice Type Map
    private Map<Id, String> sInvIdToInvoiceTypeMap = new Map<Id, String>();
    // Sales Invoice Id to Sales Invoice Line Item by Dimension 2 Map
    private Map<Id, Map<String, Id>> sInvIdToDim2SInvLineItemMap;
    // Credit Note Id to Credit Note Line Item by Dimension 2 Map
    private Map<Id, Map<String, Id>> cNoteIdToDim2CNoteLineItemMap;
    // Sales Invoice/Credit Note Id to Ordergroup Id Map
    private Map<Id,Id> docHeaderIdToOrderGroupIdMap = new Map<Id,Id>();
    //Id to be use for Temp Data
    private String invoiceRunId; 
    public Boolean stopMatching;

    private Set<Id> sInvIdStatefulSet = new Set<Id>();
    private Set<Id> cNoteIdStatefulSet = new Set<Id>();
    
    public JVCO_LinkBlngLineToSInvAndCnoteLineBatch(Map<Id, Id> bInvIdToDocHeaderIdMap, String invoiceRunId)
    {
        this.bInvIdToDocHeaderIdMap = bInvIdToDocHeaderIdMap;
        setSInvIdToInvoiceTypeMap();
        this.invoiceRunId = invoiceRunId; //GREEN-32626
    }

    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT Id,
                                         Account.JVCO_Renewal_Scenario__c,
                                         SBQQ__Quote__r.JVCO_Payment_Terms__c,
                                         SBQQ__Quote__r.JVCO_PPL_Gap__c,
                                         SBQQ__Quote__r.JVCO_PONumber__c,
                                         SBQQ__Quote__r.JVCO_Contact_Type__c,
                                         SBQQ__Quote__r.SBQQ__Opportunity2__c,
                                         SBQQ__Quote__r.SBQQ__MasterContract__c,
                                         SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_ContractScenario__c,
                                         SBQQ__Quote__r.SBQQ__Opportunity2__r.SBQQ__RenewedContract__c,
                                         SBQQ__Quote__r.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.JVCO_ContractScenario__c,
                                         JVCO_Order_Group__c,
                                         JVCO_Sales_Invoice__c,
                                         JVCO_Sales_Credit_Note__c
                                         FROM Order
                                         WHERE Id IN :bInvIdToDocHeaderIdMap.keySet()]);
    }

    public void execute(Database.BatchableContext bc, List<Order> scope)
    {   
        //Map Document Header Id to Order Group Id 
        setDocHeaderIdToOGIdMap(scope);
        linkDocHeaderIdToBInv(scope);
        //Get All Blng Invoice Line
        List<OrderItem> orderLineList = getBInvLineList(scope);
        system.debug('oit '+orderLineList);
        //Link Blng Invoice Line to Sales Invoice/Credit Note Line
        linkBInvLineToDocumentLIne(orderLineList);
        //Create Document Id Map Records   
        createTmpInvoiceProcessingDataRecords();        
    }

    private void linkDocHeaderIdToBInv(List<Order> orderList)
    {   
        Set<Order> updatedOrderSet = new Set<Order>();
        Set<SBQQ__Quote__c> updatedQuoteSet = new Set<SBQQ__Quote__c>();
        Set<Opportunity> updatedOppSet = new Set<Opportunity>();
        Map<Id, Id> oppIdToOrdIdMap = new Map<Id, Id>();

        Map<Id, c2g__codaInvoice__c> updatedSInvMap = new Map<Id, c2g__codaInvoice__c>();
        Set<Id> cNoteIdSet = new Set<Id>();
       
        for(Order ord : orderList)
        {
            Id docHeaderId = bInvIdToDocHeaderIdMap.get(ord.Id);
            //Link Sales Invoice to blng Invoice, Order, Quote
            if(docHeaderId.getSObjectType() == Schema.c2g__codaInvoice__c.SObjectType)
            {
                ord.JVCO_Sales_Invoice__c = docHeaderId;
                ord.JVCO_Order_Group__c = docHeaderIdToOrderGroupIdMap.get(docHeaderId);
                ord.JVCO_Order_Status__c = 'Posted';
                updatedOrderSet.add(ord);

                SBQQ__Quote__c q = new SBQQ__Quote__c();
                q.Id = ord.SBQQ__Quote__c;
                q.JVCO_Sales_Invoice__c = docHeaderId;
                updatedQuoteSet.add(q);

                //Sales Invoice
                c2g__codaInvoice__c sInv = new c2g__codaInvoice__c();
                sInv.Id = docHeaderId;
                sInv.JVCO_Invoice_Type__c = sInvIdToInvoiceTypeMap.get(docHeaderId);
                //Still true even if only 1 PPL Quote Gap
                if(ord.Account.JVCO_Renewal_Scenario__c == 'PPL First: Greater Than 60 Days' &&
                    ord.SBQQ__Quote__r.JVCO_PPL_Gap__c)
                {
                    sInv.PPL_Gap__c = true;
                }
                if(ord.SBQQ__Quote__r.JVCO_Contact_Type__c != null)
                {
                    sInv.JVCO_ContactType__c = ord.SBQQ__Quote__r.JVCO_Contact_Type__c;
                }
                if(ord.SBQQ__Quote__r.JVCO_PONumber__c != null)
                {
                    sInv.JVCO_PO_Number__c = ord.SBQQ__Quote__r.JVCO_PONumber__c;
                }
                if(ord.SBQQ__Quote__r.SBQQ__Opportunity2__c != null && ord.SBQQ__Quote__r.SBQQ__Opportunity2__r.SBQQ__RenewedContract__c != null){
                    sInv.JVCO_ContractScenario__c = ord.SBQQ__Quote__r.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.JVCO_ContractScenario__c;
                }
                if(ord.SBQQ__Quote__r.SBQQ__MasterContract__c != null){
                    sInv.JVCO_ContractScenario__c = ord.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_ContractScenario__c;
                }                
                updatedSInvMap.put(docHeaderId, sInv);
            //Link Credit Note to blng Invoice
            }else
            {
                ord.JVCO_Sales_Credit_Note__c = docHeaderId;
                ord.JVCO_Order_Status__c = 'Posted';
                cNoteIdSet.add(docHeaderId);
                updatedOrderSet.add(ord);
            }

            Opportunity opp = new Opportunity();
            opp.Id = ord.SBQQ__Quote__r.SBQQ__Opportunity2__c;
            opp.JVCO_Invoiced__c = true;
            updatedOppSet.add(opp);
            oppIdToOrdIdMap.put(opp.Id, ord.Id);
        }
        
        JVCO_FFUtil.stopBillNowFuction = true;
        update new List<Order>(updatedOrderSet);
        update new List<SBQQ__Quote__c>(updatedQuoteSet);
        update new List<Opportunity>(updatedOppSet);

        setSInvIdToDim2SInvLineItemMap(updatedSInvMap.keySet());
        setCNoteIdToDim2CNoteLineItemMap(cNoteIdSet);
        
        //Link Billing to Contract
        linkBInvToContract(oppIdToOrdIdMap);
        update updatedSInvMap.values();
    }

    private void linkBInvLineToDocumentLIne(List<OrderItem> orderLineList)
    {
        for(OrderItem orderLine : orderLineList)
        {
            if(orderLine.Order.JVCO_Sales_Invoice__c != null)
            {
                Id sInvLineId = sInvIdToDim2SInvLineItemMap.get(orderLine.Order.JVCO_Sales_Invoice__c).get(orderLine.JVCO_Dimension_2__c);
                orderLine.JVCO_Sales_Invoice_Line_Item__c = sInvLineId;
            }else
            {   
                Id cNoteLineId = cNoteIdToDim2CNoteLineItemMap.get(orderLine.Order.JVCO_Sales_Credit_Note__c).get(orderLine.JVCO_Dimension_2__c);
                orderLine.JVCO_Sales_Credit_Note_Line_Item__c = cNoteLineId;
            }
        }

        update orderLineList;
    }

    private List<OrderItem> getBInvLineList(List<Order> bInvList)
    {   
        return [SELECT Id, OrderId,
                Order.JVCO_Sales_Invoice__c,
                Order.JVCO_Sales_Credit_Note__c,
                JVCO_Sales_Credit_Note_Line_Item__c,
                JVCO_Sales_Invoice_Line_Item__c,
                JVCO_Dimension_2__c
                FROM OrderItem
                WHERE OrderId IN :bInvList];
    }

    private void linkBInvToContract(Map<Id, Id> oppIdToOrdIdMap)
    {   
        Set<Contract> contractSet = new Set<Contract>();
        for(Contract c : [SELECT Id, SBQQ__Opportunity__c, 
                          JVCO_Invoiced__c, JVCO_Billing_Invoice__c,
                          SBQQ__Order__c
                          FROM Contract
                          WHERE SBQQ__Opportunity__c IN :oppIdToOrdIdMap.keySet()])
        {
            // Link the Contract with the Billing Invoice
            c.SBQQ__Order__c = oppIdToOrdIdMap.get(c.SBQQ__Opportunity__c);
            c.JVCO_Invoiced__c = true;
            contractSet.add(c);
        }

        update new List<Contract>(contractSet);
    }

    private void setSInvIdToDim2SInvLineItemMap(Set<Id> sInvIdSet)
    {
        sInvIdToDim2SInvLineItemMap = new Map<Id, Map<String, Id>>();
        for(c2g__codaInvoiceLineItem__c sInvLine : [SELECT Id, c2g__Invoice__c, 
                                                    c2g__Dimension2__r.Name
                                                    FROM c2g__codaInvoiceLineItem__c
                                                    WHERE c2g__Invoice__c IN : sInvIdSet])
        {
            if(!sInvIdToDim2SInvLineItemMap.containsKey(sInvLine.c2g__Invoice__c))
            {
                sInvIdToDim2SInvLineItemMap.put(sInvLine.c2g__Invoice__c, new Map<String, Id>{});
            }
            sInvIdToDim2SInvLineItemMap.get(sInvLine.c2g__Invoice__c).put(sInvLine.c2g__Dimension2__r.Name, sInvLine.Id);
        }
        sInvIdStatefulSet.addAll(sInvIdSet);
    }

    private void setCNoteIdToDim2CNoteLineItemMap(Set<Id> cNoteIdSet)
    {
        cNoteIdToDim2CNoteLineItemMap = new Map<Id, Map<String, Id>>();
        for(c2g__codaCreditNoteLineItem__c cNoteLine : [SELECT Id, c2g__CreditNote__c, 
                                                        c2g__Dimension2__r.Name
                                                        FROM c2g__codaCreditNoteLineItem__c
                                                        WHERE c2g__CreditNote__c IN :cNoteIdSet])
        {
            if(!cNoteIdToDim2CNoteLineItemMap.containsKey(cNoteLine.c2g__CreditNote__c))
            {
                cNoteIdToDim2CNoteLineItemMap.put(cNoteLine.c2g__CreditNote__c, new Map<String, Id>{});
            }
            cNoteIdToDim2CNoteLineItemMap.get(cNoteLine.c2g__CreditNote__c).put(cNoteLine.c2g__Dimension2__r.Name, cNoteLine.Id);
        }
        cNoteIdStatefulSet.addAll(cNoteIdSet);
    }

    private void setDocHeaderIdToOGIdMap(List<Order> orderList)
    {
        //Create a mapping for Sales Invoice Id/Credit Note Id and OrderGroupId
        for(JVCO_TempInvoiceProcessingData__c tmp: [SELECT Id, JVCO_Id1__c, JVCO_Id2__c, JVCO_Type__c 
                                                    FROM JVCO_TempInvoiceProcessingData__c 
                                                    WHERE JVCO_Id1__c IN :bInvIdToDocHeaderIdMap.values() 
                                                    AND JVCO_Type__c = 'bInvIdToDocHeaderIdMap'])
        {
            docHeaderIdToOrderGroupIdMap.put(tmp.JVCO_Id1__c , tmp.JVCO_Id2__c);
        }

        // Sales Invoice/Credit Note Id to Ordergroup Id Map
        Map<Id, JVCO_Order_Group__c> tempDocHeaderIdToOGIdMap = new Map<Id, JVCO_Order_Group__c>();
        for(Order bInv : orderList)
        {
            Id docHeaderId = bInvIdToDocHeaderIdMap.get(bInv.Id);
            //Create Order Group per Document Header
            if(!docHeaderIdToOrderGroupIdMap.containskey(docHeaderId))
            {
                JVCO_Order_Group__c og = new JVCO_Order_Group__c();
                og.JVCO_Group_Order_Account__c = bInv.Account.Id;
                tempDocHeaderIdToOGIdMap.put(docHeaderId, og);
            }    
        }

        //Insert The Doc Id and Order Group Id Mapping in the Temp Processing Data
        if(!tempDocHeaderIdToOGIdMap.isEmpty())
        {
            insert tempDocHeaderIdToOGIdMap.values();
            
            List<JVCO_TempInvoiceProcessingData__c> tmpDocHeaderMappingList = new List<JVCO_TempInvoiceProcessingData__c>();
            for(Id docHeaderId : tempDocHeaderIdToOGIdMap.keyset())
            {
                JVCO_TempInvoiceProcessingData__c tmp= new JVCO_TempInvoiceProcessingData__c();
                tmp.JVCO_Id1__c = docHeaderId;
                tmp.JVCO_Id2__c = tempDocHeaderIdToOGIdMap.get(docHeaderId).Id;
                tmp.JVCO_Batch_Name__c = 'JVCO_LinkBlngLineToSInvAndCnoteLineBatch';
                tmp.JVCO_Job_Identifier__c = invoiceRunId;
                tmp.JVCO_Type__c = 'bInvIdToDocHeaderIdMap';
                tmpDocHeaderMappingList.add(tmp);
                
                docHeaderIdToOrderGroupIdMap.put(docHeaderId, tempDocHeaderIdToOGIdMap.get(docHeaderId).Id);
            }

            insert tmpDocHeaderMappingList; 
        }
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Create Mapping for Sales Invoice's Invoice Type
        Inputs: 
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        22-May-2018 franz.g.a.dimaapi    Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private void setSInvIdToInvoiceTypeMap()
    {
        for(Order ord : [SELECT Id, JVCO_Invoice_Type__c 
                                    FROM Order
                                    WHERE Id IN :bInvIdToDocHeaderIdMap.keySet()])
        {
            Id sInvId = bInvIdToDocHeaderIdMap.get(ord.Id);
            if(!sInvIdToInvoiceTypeMap.containsKey(sInvId))
            {
                sInvIdToInvoiceTypeMap.put(sInvId, ord.JVCO_Invoice_Type__c);
            }

            String invType = sInvIdToInvoiceTypeMap.get(sInvId);
            if((invType == 'Pre-Contract' && ord.JVCO_Invoice_Type__c == 'Standard') ||
                (invType == 'Supplementary' && 
                (ord.JVCO_Invoice_Type__c == 'Standard' || ord.JVCO_Invoice_Type__c =='Pre-Contract')))
            {
                sInvIdToInvoiceTypeMap.put(sInvId, ord.JVCO_Invoice_Type__c);
            }
        }
    }
                 
    public void finish(Database.BatchableContext bc)
    {
        //Query and Create Sales Id and Credit Note Id Map
        Set<Id> tmpsInvIdStatefulSet = new Set<Id>();
        Set<Id> tmpcNoteIdStatefulSet = new Set<Id>();
        
        for(JVCO_TempInvoiceProcessingData__c tmp: [SELECT Id, JVCO_Id1__c, JVCO_Type__c 
                                                    FROM JVCO_TempInvoiceProcessingData__c 
                                                    WHERE JVCO_Job_Identifier__c = :invoiceRunId 
                                                    AND JVCO_Batch_Name__c = 'JVCO_LinkBlngLineToSInvAndCnoteLineBatch'])
        {
            //Sales Invoice
            if(tmp.JVCO_Type__c == 'sInvIdStatefulSet')
            {
                tmpsInvIdStatefulSet.add(tmp.JVCO_Id1__c);
            //Credit Note
            }else if(tmp.JVCO_Type__c == 'cNoteIdStatefulSet')
            {
                tmpcNoteIdStatefulSet.add(tmp.JVCO_Id1__c);
            }
        } 

        //Run Post Batch
        JVCO_PostSalesInvoiceBatch pSIB = new JVCO_PostSalesInvoiceBatch(tmpsInvIdStatefulSet, tmpcNoteIdStatefulSet);
        psIB.stopMatching = stopMatching != null ? stopMatching : false;
        Database.executeBatch(pSIB, Integer.valueOf(SALESINVOICESCOPE));

        //Delete Batch Temp Data
        Database.executeBatch(new JVCO_DeleteTempInvoiceProcessingData(invoiceRunId), 2000);
    } 
    
    
    /* ----------------------------------------------------------------------------------------------------------
        Author:         joseph.g.barrameda
        Company:        Accenture
        Description:    This method is used to create records that will be used for mapping of sInvoice/cNote Ids. 
        Inputs:         None
        Returns:        None
        <Date>          <Authors Name>             <Brief Description of Change> 
        5-Jul-2018      joseph.g.barrameda         Initial version of the code - GREEN-32626
    ----------------------------------------------------------------------------------------------------------- */
    private void createTmpInvoiceProcessingDataRecords()
    {
        Set<Id> documentIdSet = new Set<Id>();        
        documentIdSet.addAll(sInvIdStatefulSet);
        documentIdSet.addAll(cNoteIdStatefulSet);   

        List<JVCO_TempInvoiceProcessingData__c> tmpInvoiceProcDataList = new List<JVCO_TempInvoiceProcessingData__c>();
        for(Id docId: documentIdSet)
        {
            JVCO_TempInvoiceProcessingData__c  tmpInvProcData = new JVCO_TempInvoiceProcessingData__c();
            tmpInvProcData.JVCO_Batch_Name__c = 'JVCO_LinkBlngLineToSInvAndCnoteLineBatch';                   
            tmpInvProcData.JVCO_Id1__c = docId; 
            tmpInvProcData.JVCO_Job_Identifier__c = invoiceRunId;           
            tmpInvProcData.JVCO_Type__c  = docId.getSObjectType() == Schema.c2g__codaInvoice__c.SObjectType ? 'sInvIdStatefulSet' : 'cNoteIdStatefulSet';
            tmpInvoiceProcDataList.add(tmpInvProcData);
        }
        insert tmpInvoiceProcDataList;
    }
   
}