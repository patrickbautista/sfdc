/* ----------------------------------------------------------------------------------------------------------
Name:            JVCO_OrderProductTrigger.cls 
Description:     Trigger handler for JVCO_OrderProductTrigger.trigger
Test class:      JVCO_OrderProductTriggerTest.cls 

Date             Version     Author                            Summary of Changes 
-----------      -------     -----------------                 -------------------------------------------
16-May-2017       0.1        robert.j.b.lacatan         Initial version of the code
04-Dec-2017       0.2        jules.osberg.a.pablo       Added fix for GREEN-26629, populates blng__NextBillingDate__c to allow the Order to be activated    
---------------------------------------------------------------------------------------------------------- */

public class JVCO_OrderProductTrigger {
    
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_OrderProductTrigger__c : false;
    
    public JVCO_OrderProductTrigger(){ }
    
    public static void onBeforeInsert(List<OrderItem> orderItemList){
        if(!skipTrigger) 
        { 
            populateDim2(orderItemList);
        }
    }
    
    public static void onAfterInsert(List<OrderItem> orderItemList){
        if(!skipTrigger) 
        {
            
        }
    }
    
    public static void onAfterUpdate(List<OrderItem> orderItemList, Map<Id, OrderItem> oldOrderItemMap)
    {
        if(!skipTrigger)
        {
            createPaidAmountTracker(orderItemList, oldOrderItemMap);
        }
    }
    
    public static void populateDim2(List<OrderItem> orderItemList){
        //populating dimension_2s __c  
        Set<Id> quoteLineIdSet = new Set<Id>();
        system.debug('aaaa');
        for(OrderItem oi : orderItemList)
        {
            quoteLineIdSet.add(oi.SBQQ__QuoteLine__c);
        }
        system.debug('quoteLineIdSet '+quoteLineIdSet);
        Map<Id, SBQQ__QuoteLine__c> quoteLineMap = new Map<Id, SBQQ__QuoteLine__c>([SELECT Id, Terminate__c,
                                                                                    SBQQ__StartDate__c,
                                                                                    SBQQ__EndDate__c
                                                                                    FROM SBQQ__QuoteLine__c 
                                                                                    WHERE Id IN : quoteLineIdSet]);
        
        for(OrderItem oi : orderItemList){
            
            oi.JVCO_Date_Override__c = true;
            if(oi.EndDate != null && oi.JVCO_Temp_End_Date__c == null)
                oi.JVCO_Temp_End_Date__c = oi.EndDate;
            if(oi.ServiceDate != null && oi.JVCO_Temp_Start_Date__c == null)
                oi.JVCO_Temp_Start_Date__c = oi.ServiceDate;
            if(oi.JVCO_Parent__c != null)
                oi.JVCO_Dimension_2__c = oi.JVCO_Parent__c;
            if(quoteLineMap.containsKey(oi.SBQQ__QuoteLine__c)){
                oi.JVCO_Terminated_Line__c = quoteLineMap.get(oi.SBQQ__QuoteLine__c).Terminate__c;
                oi.JVCO_Temp_Start_Date__c = quoteLineMap.get(oi.SBQQ__QuoteLine__c).SBQQ__StartDate__c;
                oi.JVCO_Temp_End_Date__c = quoteLineMap.get(oi.SBQQ__QuoteLine__c).SBQQ__EndDate__c;
            }
            if(oi.JVCO_Period_End_Date__c == null)
                oi.JVCO_Period_End_Date__c = oi.JVCO_Temp_End_Date__c != null ? oi.JVCO_Temp_End_Date__c : oi.JVCO_End_Date__c;
            if(oi.JVCO_Period_Start_Date__c == null)
                oi.JVCO_Period_Start_Date__c = oi.JVCO_Temp_End_Date__c != null ? oi.JVCO_Temp_Start_Date__c : oi.JVCO_Start_Date__c;
        }
    }
    
    public static void createPaidAmountTracker(List<OrderItem> newList, Map<id,OrderItem> oldMap)
    {
        JVCO_General_Settings__c GeneralSettings = JVCO_General_Settings__c.getInstance();
        
        List<JVCO_Invoice_Line_Paid_Amount_Tracker__c> invLinePATList = new List<JVCO_Invoice_Line_Paid_Amount_Tracker__c>();
        for(OrderItem ordLine : [SELECT id, JVCO_Paid_Amount__c, JVCO_Paid_Date__c,
                                 JVCO_Percentage_Paid__c, 
                                 Order.JVCO_Week__c,
                                 Order.JVCO_Distribution_Status__c, 
                                 JVCO_Dynamic_Paid_Amount__c,
                                 Order.JVCO_Payment_Status__c,
                                 Order.JVCO_Status_Change_Date__c,
                                 JVCO_Paid_Amount_Tracker__c,
                                 JVCO_Total_Amount__c
                                 FROM OrderItem
                                 WHERE id IN: newList])
        {
            if(ordLine.JVCO_Paid_Amount__c != oldMap.get(ordLine.id).JVCO_Paid_Amount__c)
            {
                JVCO_Invoice_Line_Paid_Amount_Tracker__c invPAT = insertInvoiceLineTrackerFields(ordLine);
                invLinePATList.add(invPAT);
            }
        }
        insert invLinePATList;
    }
    public static JVCO_Invoice_Line_Paid_Amount_Tracker__c insertInvoiceLineTrackerFields(OrderItem ordLine)
    {
        JVCO_Invoice_Line_Paid_Amount_Tracker__c invPAT = new JVCO_Invoice_Line_Paid_Amount_Tracker__c();
        invPAT.JVCO_Order_Product__c = ordLine.id;
        invPAT.JVCO_Paid_Date__c = ordLine.JVCO_Paid_Date__c;
        invPAT.JVCO_Percentage_Paid__c = ordLine.JVCO_Percentage_Paid__c;
        invPAT.JVCO_Week__c = ordLine.Order.JVCO_Week__c;
        invPAT.JVCO_Distribution_Status__c = ordLine.Order.JVCO_Distribution_Status__c;
        invPAT.JVCO_Dynamic_Paid_Amount__c = ordLine.JVCO_Paid_Amount_Tracker__c;
        invPAT.JVCO_Paid_Amount__c = ordLine.JVCO_Total_Amount__c * ordLine.JVCO_Percentage_Paid__c / 100;
        invPAT.JVCO_Total_Amount__c = ordLine.JVCO_Total_Amount__c;
        invPAT.JVCO_Payment_Status__c = ordLine.Order.JVCO_Payment_Status__c;
        invPAT.JVCO_Status_Change_Date__c = ordLine.Order.JVCO_Status_Change_Date__c;
        return invPAT;
    }
}