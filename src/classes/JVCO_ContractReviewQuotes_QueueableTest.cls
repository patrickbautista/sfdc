/* ----------------------------------------------------------------------------------------------
    Name: JVCO_ContractReviewQuotes_QueueableTest
    Description: Test Class for JVCO_ContractReviewQuotes_Queueable

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    08-Aug-2018     0.1         Jules Pablo         Intial creation
----------------------------------------------------------------------------------------------- */
@IsTest
private class JVCO_ContractReviewQuotes_QueueableTest {
    @testSetup static void setupTestData() {
        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Product2 prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        Account a1 = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert a1;

        Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
        insert a2;
        
        Opportunity o = JVCO_TestClassObjectBuilder.createOpportunity(a2);
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;
        
        SBQQ__QuoteProcess__c qpID = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qpId;

        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Amendment';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        q.SBQQ__QuoteProcessId__c = qpId.Id;
        insert q;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        qL.SPVAvailability__c = true;
        insert ql;
        
        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();           
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = JVCO_TestClassObjectBuilder.createAffiliation(a2.id, venueRecord.id);    
        Insert  affilRecord;   

        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;      

        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'ErrorNoSPV', Value__c = 'You can not contract the Quote since there are Quote Line(s) w/o SPV'));
        insert settings;
    }

    private static testMethod void testMethodContactReviewQuotesExecute() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Test.startTest();
        System.enqueueJob(new JVCO_ContractReviewQuotes_Queueable(accountRecord));
        Test.stopTest();
    }

    private static testMethod void testMethodContactReviewQuotesExecuteError() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        JVCO_ContractReviewQuotesHelper.testError = true;
        
        Test.startTest();
        System.enqueueJob(new JVCO_ContractReviewQuotes_Queueable(accountRecord));
        Test.stopTest();
    }

    private static testMethod void testMethodContactReviewQuotesReExecute() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id, AccountId, Name, CloseDate, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName, SBQQ__PrimaryQuote__c FROM Opportunity LIMIT 1]);

        Set<Id> opportunityIdSet = new Set<Id>();
        for(Id oppId :opportunityMap.keySet()) {
            opportunityIdSet.add(oppId);
        }
        Test.startTest();       
        System.enqueueJob(new JVCO_ContractReviewQuotes_Queueable(opportunityMap, accountRecord, 1, 0, '', opportunityIdSet, 1, 1));
        Test.stopTest();
    }

    private static testMethod void testMethodContactReviewQuotesReExecute2() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id, AccountId, Name, CloseDate, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName, SBQQ__PrimaryQuote__c FROM Opportunity LIMIT 1]);

        Test.startTest();       
        Opportunity testOpp = opportunityMap.values()[0].clone(false,false,false,false);
        insert testOpp;

        opportunityMap.put(testOpp.Id, testOpp);

        Set<Id> opportunityIdSet = new Set<Id>();
        for(Id oppId :opportunityMap.keySet()) {
            opportunityIdSet.add(oppId);
        }

        System.enqueueJob(new JVCO_ContractReviewQuotes_Queueable(opportunityMap, accountRecord, 1, 0, '', opportunityIdSet, 1, 4));
        Test.stopTest();
    }

    private static testMethod void testMethodContactReviewQuotesReExecute3() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id, Name, CloseDate, AccountId, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName, SBQQ__PrimaryQuote__c FROM Opportunity LIMIT 1]);

        JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
        kaTempRec.JVCO_AccountId__c = accountRecord.Id;
        kaTempRec.JVCO_OppId__c = opportunityMap.values()[0].Id;
        kaTempRec.JVCO_Contracted__c = true;
        insert kaTempRec;

        Test.startTest();       
        Opportunity testOpp = opportunityMap.values()[0].clone(false,false,false,false);
        insert testOpp;

        opportunityMap.put(testOpp.Id, testOpp);

        Set<Id> opportunityIdSet = new Set<Id>();
        for(Id oppId :opportunityMap.keySet()) {
            opportunityIdSet.add(oppId);
        }

        System.enqueueJob(new JVCO_ContractReviewQuotes_Queueable(opportunityMap, accountRecord, 1, 0, '', opportunityIdSet, 1, 1));
        Test.stopTest();
    }

    private static testMethod void testMethodContactReviewQuotesReExecute4() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id, Name, CloseDate, AccountId, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName, SBQQ__PrimaryQuote__c FROM Opportunity LIMIT 1]);

        JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
        kaTempRec.JVCO_AccountId__c = accountRecord.Id;
        kaTempRec.JVCO_OppId__c = opportunityMap.values()[0].Id;
        kaTempRec.JVCO_Contracted__c = true;
        insert kaTempRec;

        Test.startTest();       
        Opportunity testOpp = opportunityMap.values()[0].clone(false,false,false,false);
        testOpp.SBQQ__Contracted__c = true;
        insert testOpp;

        opportunityMap.values()[0].SBQQ__Contracted__c = true;
        update opportunityMap.values()[0];

        opportunityMap.put(testOpp.Id, testOpp);

        Set<Id> opportunityIdSet = new Set<Id>();
        for(Id oppId :opportunityMap.keySet()) {
            opportunityIdSet.add(oppId);
        }

        System.enqueueJob(new JVCO_ContractReviewQuotes_Queueable(opportunityMap, accountRecord, 1, 0, '', opportunityIdSet, 1, 1));
        Test.stopTest();
    }

    private static testMethod void testMethodContactReviewQuotesReExecute5() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c, JVCO_AbortRunningQueueable__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id, Name, CloseDate, AccountId, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName, SBQQ__PrimaryQuote__c FROM Opportunity LIMIT 1]);

        accountRecord.JVCO_AbortRunningQueueable__c = true;
        update accountRecord;

        JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
        kaTempRec.JVCO_AccountId__c = accountRecord.Id;
        kaTempRec.JVCO_OppId__c = opportunityMap.values()[0].Id;
        kaTempRec.JVCO_Contracted__c = true;
        insert kaTempRec;

        Test.startTest();       
        Opportunity testOpp = opportunityMap.values()[0].clone(false,false,false,false);
        testOpp.SBQQ__Contracted__c = true;
        insert testOpp;

        opportunityMap.values()[0].SBQQ__Contracted__c = true;
        update opportunityMap.values()[0];

        opportunityMap.put(testOpp.Id, testOpp);

        Set<Id> opportunityIdSet = new Set<Id>();
        for(Id oppId :opportunityMap.keySet()) {
            opportunityIdSet.add(oppId);
        }

        System.enqueueJob(new JVCO_ContractReviewQuotes_Queueable(opportunityMap, accountRecord, 1, 0, '', opportunityIdSet, 1, 1));
        Test.stopTest();
    }
}