public class JVCO_ApexJobProgressBarController {
    private Id batchClassId;
    final public String NOT_START = 'not_started';
    final public String PROCESSING = 'processing';
    final public String FINISHED = 'finished';
    public String batchStatus {get;set;}
    public Id batchId {get;set;}
    public String message {get;set;}
    public Integer errornum {get;set;}
    public String batchIdString {get;set;}
    
    public JVCO_ApexJobProgressBarController (){
        
        batchStatus = NOT_START;
        message = ''; 
        errornum =0; 
        
    }
    public boolean getShowProgressBar() {
        if(batchStatus == PROCESSING )
            return true;
        return false;
    }
    //this method will fetch apex jobs and will update status,JobItemsProcessed,NumberOfErrors,TotalJobItems
    public BatchJob[] getJobs() {
        List<AsyncApexJob> apexJobs = new List<AsyncApexJob>();

        if(batchIdString != null && batchIdString != ''){

            batchId = Id.valueOf(batchIdString);

            apexJobs = [Select TotalJobItems, Status, NumberOfErrors, ExtendedStatus, JobItemsProcessed, Id, JobType, ApexClassId,ApexClass.name, CreatedDate From AsyncApexJob Where Id =: batchId Order by CreatedDate DESC];
        }

        integer i =0;
        List<BatchJob> jobs = new List<BatchJob>();      
        for(AsyncApexJob job : apexJobs) {
            system.debug('i counter: ' + i);
            system.debug(job.id + ' ' + batchId);
            if(job.id != batchId)
                continue;
            
            BatchJob bj = new BatchJob();
            bj.isCompleted = false;
            
            bj.Job_Type = 'Process 1';          
            bj.aj = job;
            
            // NOT START YET
            if(job.jobItemsProcessed == 0) {
                bj.Percent= 0;
                jobs.add(bj);
                continue;
            }
            
            Decimal d = job.jobItemsProcessed;
            d = d.divide(job.TotalJobItems, 2)*100;
            bj.Percent= d.intValue();
            
            // PROCESSING
            if(bj.Percent != 100){
                jobs.add(bj);
                continue;
            }
            
            // FINISED
            batchStatus = FINISHED;

            errornum += job.NumberOfErrors;
            bj.isCompleted = true;
            jobs.add(bj);           
        }
        return jobs;
    }
    
    public PageReference updateProgress() {
        if(batchStatus == FINISHED) {
            message = 'COMPLETED';
        }
        return null;
    }
    public class BatchJob{
        public AsyncApexJob aj {get;set;}
        public Integer Percent {get;set;}
        public String Job_Type {get;set;}
        public Boolean isCompleted {get;set;}
        public BatchJob(){}     
    }
    
    //GREEN-33789
    public PageReference updateAcc(Id batchId, Id accountId){

        if(batchId != null){
            List<AsyncApexJob> apexJobs = 
            [Select TotalJobItems, Status, NumberOfErrors, ExtendedStatus, 
             JobItemsProcessed, Id, JobType, ApexClassId,ApexClass.name, CreatedDate 
             From AsyncApexJob Where Id =: batchId Order by CreatedDate DESC];

            List<Account> accOfJob = [SELECT Id, JVCO_ContractRenewQuotesApexJob__c, JVCO_ContractReviewQuotesApexJob__c, JVCO_GenerateRenewQuotesApexJob__c, JVCO_GenerateReviewQuotesApexJob__c, JVCO_OrderRenewQuotesApexJob__c, JVCO_OrderReviewQuotesApexJob__c FROM Account WHERE Id =: accountId];

            Account accToUpdate = new Account();

            if(!accOfJob.isEmpty()){
                if(!apexJobs.isEmpty()){
                    if(apexJobs[0].Status == 'Completed' || apexJobs[0].Status == 'Aborted'){
                        if(apexJobs[0].Id == accOfJob[0].JVCO_ContractRenewQuotesApexJob__c && accOfJob[0].JVCO_ContractRenewQuotesApexJob__c != null){
                            accToUpdate.Id = accOfJob[0].Id;
                            accToUpdate.JVCO_ContractRenewQuotesApexJob__c = '';
                        }
                        else if(apexJobs[0].Id == accOfJob[0].JVCO_ContractReviewQuotesApexJob__c && accOfJob[0].JVCO_ContractReviewQuotesApexJob__c != null){
                            accToUpdate.Id = accOfJob[0].Id;
                            accToUpdate.JVCO_ContractReviewQuotesApexJob__c = '';
                        }
                        else if(apexJobs[0].Id == accOfJob[0].JVCO_GenerateRenewQuotesApexJob__c && accOfJob[0].JVCO_GenerateRenewQuotesApexJob__c != null){
                            accToUpdate.Id = accOfJob[0].Id;
                            accToUpdate.JVCO_GenerateRenewQuotesApexJob__c = '';
                        }
                        else if(apexJobs[0].Id == accOfJob[0].JVCO_GenerateReviewQuotesApexJob__c && accOfJob[0].JVCO_GenerateReviewQuotesApexJob__c != null){
                            accToUpdate.Id = accOfJob[0].Id;
                            accToUpdate.JVCO_GenerateReviewQuotesApexJob__c = '';
                        }
                        else if(apexJobs[0].Id == accOfJob[0].JVCO_OrderRenewQuotesApexJob__c && accOfJob[0].JVCO_OrderRenewQuotesApexJob__c != null){
                            accToUpdate.Id = accOfJob[0].Id;
                            accToUpdate.JVCO_OrderRenewQuotesApexJob__c = '';
                        }
                        else if(apexJobs[0].Id == accOfJob[0].JVCO_OrderReviewQuotesApexJob__c && accOfJob[0].JVCO_OrderReviewQuotesApexJob__c != null){
                            accToUpdate.Id = accOfJob[0].Id;
                            accToUpdate.JVCO_OrderReviewQuotesApexJob__c = '';
                        }

                        if(accToUpdate.Id != null){
                            update accToUpdate;

                            PageReference tempPage = ApexPages.currentPage();
                            tempPage.setRedirect(true);
                            return tempPage;
                        }
                    }
                }
            }
        }

        return null;
}
    public PageReference updateAcc(Id accountId, String queueableType){

        String searchAcc = '%' + accountId + '%';
        String searchType = '%' + queueableType + '%';
        Id queueableJobId = null;

        List<JVCO_KATempHolder__c> kaTempList = [SELECT Id, Name, JVCO_JobId__c FROM JVCO_KATempHolder__c WHERE Name LIKE: searchAcc AND Name LIKE: searchType ORDER BY Id DESC LIMIT 1];

        if(!kaTempList.isEmpty()){

            queueableJobId = kaTempList[0].JVCO_JobId__c;
        }

        if(queueableJobId != null){

            List<AsyncApexJob> aJobList = [SELECT Id, Status, ApexClass.Name FROM AsyncApexJob WHERE Id =: queueableJobId LIMIT 1];

            if(!aJobList.isEmpty()){

                AsyncApexJob aJob = aJobList[0];

                if(aJob.Status == 'Aborted' || aJob.Status == 'Failed'){

                    Account accRec = new Account();

                    if(aJob.ApexClass.Name.containsIgnoreCase('ContractRenewQuotes_Queueable')){

                        accRec.Id = accountId;
                        accRec.JVCO_ContractRenewQuotesQueueable__c = null;
                    }
                    else if(aJob.ApexClass.Name.containsIgnoreCase('ContractReviewQuotes_Queueable')){

                        accRec.Id = accountId;
                        accRec.JVCO_ContractReviewQuotesQueueable__c = null;
                    }
                    else if((aJob.ApexClass.Name.containsIgnoreCase('OrderQuotes_Queueable'))){

                        accRec.Id = accountId;

                        if(queueableType == 'ContractRenew'){

                            accRec.JVCO_OrderRenewQuotesQueueable__c = null;
                        }
                        else if(queueableType == 'ContractReview'){

                            accRec.JVCO_OrderReviewQuotesQueueable__c = null;
                        }
                    }
                    else if(aJob.ApexClass.Name.containsIgnoreCase('GenerateRenewalQuotes_Queueable')){                        

                        accRec.Id = accountId;
                        accRec.JVCO_GenerateRenewQuotesQueueable__c = null;
                    }

                    if(accRec.Id != null){

                        update accRec;

                        PageReference tempPage = ApexPages.currentPage();
                        tempPage.setRedirect(true);
                        return tempPage;
                    }
                }
            }
        }

        return null;
    }
}