/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_MigratedEventsHandler.cls 

   Date            Version     Author                             Summary of Changes 
   -----------     -------     -----------------                  -----------------------------------
   07-DEC-2017     0.1         gypt.d.minierva                    Intial draft   
  ------------------------------------------------------------------------------------------------ */
public with sharing class JVCO_MigratedEventsHandler {

    public static void JVCO_MigratedEventsLogic(List<blng__Invoice__c> invoiceEvent) {
        
        List<Id> invoiceIds = new List<Id>();
        List<Id> tempPEventIds = new List<Id>();
        List<Id> tempUEventIds = new List<Id>();
        List<String> tempPContractIds = new List<String>();
        List<String> tempUContractIds = new List<String>();
        List<blng__Invoice__c> invoiceToFilter = new List<blng__Invoice__c>();
        List<Contract> paidContracts = new List<Contract>();
        List<Contract> unpaidContracts = new List<Contract>();
        List<JVCO_Event__c> paidEvents = new List<JVCO_Event__c>();
        List<JVCO_Event__c> unpaidEvents = new List<JVCO_Event__c>();
        List<JVCO_Event__c> eventsToUpdate = new List<JVCO_Event__c>();
        
        for (blng__Invoice__c ie : invoiceEvent){
            invoiceIds.add(ie.Id);
        }
        
        if(!invoiceIds.isEmpty()){

            invoiceToFilter = [SELECT Id,JVCO_Migrated__c, JVCO_Distribution_Status__c,
                                                         blng__Account__r.JVCO_Live__c,JVCO_Temp_Legacy_Contracts__c
                                                         FROM blng__Invoice__c WHERE Id in :invoiceIds];
                                                         
        }
        
        if(invoiceToFilter.size() > 0){
            
            for(blng__Invoice__c i : invoiceToFilter){

                if(i.JVCO_Distribution_Status__c == 'Fully Paid'){
                    tempPContractIds.add(i.JVCO_Temp_Legacy_Contracts__c);
                }
                
                if(i.JVCO_Distribution_Status__c == 'Unpaid' ||i.JVCO_Distribution_Status__c == 'Unmatched' || i.JVCO_Distribution_Status__c == 'Unmatched and Cancelled'){
                    tempUContractIds.add(i.JVCO_Temp_Legacy_Contracts__c);
                }
            }
        }
        
        if(!tempPContractIds.isEmpty()){
            paidContracts = [SELECT Id,JVCO_Contract_Temp_External_ID__c,(SELECT Id,JVCO_Event__c FROM SBQQ__Subscriptions__r) FROM Contract WHERE JVCO_Contract_Temp_External_ID__c in :tempPContractIds];
        }
        
        if(paidContracts.size() > 0){
            
            for(Contract pc : paidContracts){
                for(SBQQ__Subscription__c ps : pc.SBQQ__Subscriptions__r){
                    if(ps.JVCO_Event__c != null){
                        tempPEventIds.add(ps.JVCO_Event__c); 
                    }
                }
            }
        }
        
        if(!tempUContractIds.isEmpty()){

            unpaidContracts = [SELECT Id,JVCO_Contract_Temp_External_ID__c,(SELECT Id,JVCO_Event__c FROM SBQQ__Subscriptions__r) FROM Contract WHERE JVCO_Contract_Temp_External_ID__c in :tempUContractIds];
        }
        
        if(unpaidContracts.size() > 0){
            
            for(Contract uc : unpaidContracts){
                for(SBQQ__Subscription__c us : uc.SBQQ__Subscriptions__r){

                    if(us.JVCO_Event__c != null){

                        tempUEventIds.add(us.JVCO_Event__c);
                    }
                }
            }
        }
        
        if(!tempPEventIds.isEmpty()){
			
            paidEvents = [SELECT Id,JVCO_Invoice_Paid__c FROM JVCO_Event__c WHERE Id in :tempPEventIds];
            
            if(paidEvents.size() > 0){
                for(JVCO_Event__c pe : paidEvents){
                    pe.JVCO_Invoice_Paid__c = true;
                    eventsToUpdate.add(pe);
                }
            }
        }
        
        if(!tempUEventIds.isEmpty()){
			
            unpaidEvents = [SELECT Id,JVCO_Invoice_Paid__c FROM JVCO_Event__c WHERE Id in :tempUEventIds];
            
            if(unpaidEvents.size() > 0){
                for(JVCO_Event__c ue : unpaidEvents){
                    ue.JVCO_Invoice_Paid__c = false;
                    eventsToUpdate.add(ue);
                }
            }
        }
        if(!eventsToUpdate.isEmpty()){
            update eventsToUpdate;
        }
    }
}