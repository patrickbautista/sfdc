/* ----------------------------------------------------------------------------------------------------------
Name:            JVCO_SmarterPayPaymentScheduler.cls
Description:     websit payment schedular class
Test class:      JVCO_SmarterPayPaymentBatchTest.cls

Date             Version     Author                            Summary of Changes
-----------      -------     -----------------                 -------------------------------------------
21-May-2020      0.2         patrick.t.bautista                Initial version of the code - GREEN-35533
---------------------------------------------------------------------------------------------------------- */
public class JVCO_SmarterPayPaymentScheduler implements Schedulable
{
    private final JVCO_General_Settings__c GENERAL_SETTINGS = JVCO_General_Settings__c.getInstance();
    private final Decimal PROCESSINGLIMIT = GENERAL_SETTINGS != null ? GENERAL_SETTINGS.JVCO_PayonomyPaymentProcessingBatchLimit__c : 20;
    
    public void execute(SchedulableContext sc)
    {
        Database.executeBatch(new JVCO_SmarterPayPaymentBatch('Credit Card'),  Integer.valueOf(PROCESSINGLIMIT));
    }  
}