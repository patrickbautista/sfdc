/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CampaignHandler.cls 
   Description: Handler class for Campaign object

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  01-Sep-2016   0.1         ryan.i.r.limlingan  Intial creation
  19-Dec-2017	0.2			chun.yin.tang		Add disabling trigger to Campaign Handler
  ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_CampaignHandler
{
    
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_CampaignTrigger__c : false;   
    /* ------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: This method handles afterUpdate trigger events for Campaign
    Input: List<Campaign>, Map<Id, Campaign>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    01-Sep-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public static void afterUpdate(List<Campaign> campaigns, Map<Id, Campaign> oldCampaignsMap)
    {
        if(!skipTrigger) {
	        JVCO_CampaignUpdateMembersLogic.updateMembers(campaigns, oldCampaignsMap);
        }
    }
}