/**
* Created by: Flores,Emmanuel D.
* Created Date: 19.06.2017
* Description: APEX Batch class to update ‘Send NB/Review Document' and ‘Is Renew Contract’ check boxes
*  Date             Version     Author                            Summary of Changes 
*   -----------      -------     -----------------                 -------------------------------------------
*  01/03/2018         0.2         mel.andrei.b.santos               GREEN-30663 - Added Limit based on custom settings
*/
global class JVCO_GenerateReviewForm implements Database.batchable<SObject>,Database.AllowsCallouts,Database.Stateful, Schedulable{

    global List<Account> accountsList = new List<Account>();

    /**
    * This method retrieves retail and current contract records
    */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //Start mel andrei santos 28/02/2018 Added Limit based on custom settings
        String soqlLimit  = null;

        Map<String, JVCO_GenerateReviewFormSettings__c> settings = JVCO_GenerateReviewFormSettings__c.getAll();
        soqlLimit = string.valueof(settings.get('Limit').Limit__c); 

        //end mel andrei santos 28/02/2018

        String query = 'Select Id, JVCO_Licence_Period_End_Date__c, JVCO_Contract_EndDate__c, JVCO_Requested_Review_Date__c, JVCO_Review_Type__c, JVCO_No_of_Community_Building_Tariffs__c, JVCO_No_of_Discotheques_DH_Tariffs__c, JVCO_No_of_Holiday_Centres_Tariffs__c, AccountId, Account.JVCO_Send_NB_Review_Document__c, Account.JVCO_Review_Form__c from Contract';
        query += ' where Account.Type != \'Key Account\' AND Account.Type != \'Agency\' AND JVCO_Current_Period__c = true AND Generate_Review_Form__c = true';
        //query += ' and Id=\'8007E000000ORis\'';

        query += ' order by JVCO_Licence_Period_End_Date__c asc ';

        // mel andrei santos - added for soql Limit
        query = query + ' Limit ' + soqlLimit; 


        system.debug('QueryValue:'+ query);
        return Database.getQueryLocator(query);
    }
    
    /**
    * This method executes the retrieved contract records
    */
    global void execute(SchedulableContext sc){
        JVCO_GenerateReviewForm batch = new JVCO_GenerateReviewForm();
        Database.executeBatch(batch);
    }
    
    /**
    * This method executes the retrieved contract records
    */
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        System.debug('scope: '+ scope);

        List<Contract> contractsToUpdate = (List<Contract>)scope;

        if(!contractsToUpdate.isEmpty()){
            accountsList = checkContract(contractsToUpdate);
        }

        if(!accountsList.isEmpty()){
            try{
                database.update(accountsList);
            }catch(DmlException e){
                System.debug('Error: ' + e);
            }
        }
    }
    
    /**
    * This method will execute after all batched are processed
    */
    global void finish(Database.BatchableContext BC){

    }

    private static List<Account> checkContract(List<Contract> contractList){
        List<Account> accountListToUpdate = new List<Account>();
        Set<Id> accId = new Set<Id>();
        //Map<String,JVCO_Contract_Default_Review_Date__c> reviewDaysMap = JVCO_Contract_Default_Review_Date__c.getAll();

        for(Contract con : contractList){

            if(String.valueOf(con.Account.JVCO_Review_Form__c) != null){
                Account acc = new Account();
                acc.Id = con.AccountId;
                acc.JVCO_Is_Renew_Contract__c = true;
                acc.JVCO_Send_NB_Review_Document__c = true;
                
                if(!accId.contains(acc.Id)){
                    accountListToUpdate.add(acc);
                    accId.add(acc.Id);
                }
            }
        }

        return accountListToUpdate;
    }

}