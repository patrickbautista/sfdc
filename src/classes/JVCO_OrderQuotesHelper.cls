/* ------------------------------------------------------------------------------------------------
   Name:            JVCO_OrderQuotesHelper
   Description:     Helper class for JVCO_OrderQuotesBatch and JVCO_OrderQuotesHelper_Queueable

   Date            Version     Author                       Summary of Changes 
   -----------     -------     -----------------            ---------------------------------------
   09-Jul-2018     0.1         jules.osberg.a.pablo         Initial Version
   24-Apr-2019     0.2         rhys.j.c.dela.cruz           GREEN-34512 - Check if order is generated then revert ordered field if no order
   22-May-2019     0.2         rhys.j.c.dela.cruz           GREEN-33618 - Changes to queueable to accomodate new Temp Object
   26-Jun-2019     0.3         rhys.j.c.dela.cruz           GREEN-34698 - Adjusted handling for Opportunity that did not create an Order
------------------------------------------------------------------------------------------------ */
public with sharing class JVCO_OrderQuotesHelper {
    @testVisible
    private static Boolean testError = false;

    public JVCO_OrderQuotesHelper() {
        
    }

    public static string getQueryString(Account accountRecord, Set<Id> oIds) {
        string opportunityIds = '(';
        if(!oIds.isEmpty()) {
            for(Id oId : oIds) {
                opportunityIds = opportunityIds + '\'' + oId + '\',';
            }
            //opportunityIds.removeEnd(',');
            opportunityIds = opportunityIds.substring(0, opportunityIds.length() -1);
        } else {
            opportunityIds = opportunityIds + '\'\'';
        }
        opportunityIds = opportunityIds + ')';

        String query = 'select Id, AccountId, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName, SBQQ__PrimaryQuote__c from Opportunity where SBQQ__PrimaryQuote__r.SBQQ__NetAmount__c != 0 AND SBQQ__PrimaryQuote__r.SBQQ__NetAmount__c != null AND SBQQ__Contracted__c = true AND Id in ' + opportunityIds + ' and AccountId = ' + '\'' + accountRecord.Id + '\'';
        return query;
    }

    public static Map<String, String> executeOrderQuotes(List<Opportunity> opportunityList, Map<String, String> quoteStringMap) {
        //GREEN-31215 - Get custom settings to be the maximum number of tries in while loop
        Integer MaxOrderRetries;
        MaxOrderRetries = (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_MaxOrderRetries__c != null ? 
                          (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_MaxOrderRetries__c : 3;
        //Set<Id> oppIds = new Set<Id>();
        List<Opportunity> oppListToUpdate = new List<Opportunity>();
        Set<Id> quoteIdSet = new Set<Id>();
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        Integer j = 0;

        Map<String, String> mapOfQuotes = new Map<String, String>();
        Set<Id> oppIdSet = new Set<Id>();
 
        for (Opportunity opp : opportunityList) 
        {
            opp.SBQQ__Ordered__c = true;
            oppListToUpdate.add(opp);
            oppIdSet.add(opp.Id);
        }
        //GREEN-31215 - retry to save any failures
        do 
        { 
            if(!oppListToUpdate.isEmpty()) 
                {
                    //update oppListToUpdate;

                    //GREEN-28620 - start - 8Feb2018
                    if(Test.isRunningTest() && testError)
                    {  
                        oppListToUpdate.add(new Opportunity());
                    }

                    List<Database.SaveResult> res = Database.update(oppListToUpdate,false);
                    for(Integer i = 0; i < oppListToUpdate.size(); i++)
                    {
                        Database.SaveResult srOppList = res[i];
                        Opportunity origrecord = oppListToUpdate[i];
                        if(!srOppList.isSuccess())
                        {
                            System.debug('Order Generation fail: ');
                            for(Database.Error objErr:srOppList.getErrors())
                            {
                                ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                                errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                                errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(origrecord.ID);
                                errLog.ffps_custRem__Grouping__c = String.valueof(objErr.getStatusCode());
                                errLog.ffps_custRem__Detail__c = String.valueof(objErr.getMessage());

                                if(System.isQueueable()){
                                    errLog.ffps_custRem__Message__c = 'JVCO_OrderQuotesQueueable: Update Opportunity Record';    
                                }
                                else{
                                    errLog.ffps_custRem__Message__c = 'JVCO_OrderQuotesBatch: Update Opportunity Record';    
                                }
                                errLog.JVCO_Number_of_Tries__c = j + 1;
                                errLogList.add(errLog);
                                if(!mapOfQuotes.containsKey(String.valueOf(origrecord.SBQQ__PrimaryQuote__c))){
                                    mapOfQuotes.put(String.valueOf(origrecord.SBQQ__PrimaryQuote__c), '<li>Quote: ' + String.valueOf(origrecord.SBQQ__PrimaryQuote__c) + ' - ' + String.valueof(objErr.getStatusCode()) + '</li>');
                                }
                            }
                        }
                        //If Save Success, remove from list.
                        //else{
                        //    if(mapOfQuotes.containsKey(String.valueOf(origrecord.SBQQ__PrimaryQuote__c))){
                        //        mapOfQuotes.remove(String.valueOf(origrecord.SBQQ__PrimaryQuote__c));
                        //    }
                        //}
                    }   
                }

           System.debug('@@@Error Log List of quote: ' + errLogList);
            if(!errLogList.isempty())
            {
               try
               {
                  insert errLogList;
               }
               catch(Exception ex)
               {
                   System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                   
               }
            }
            j++;
        }
        //The loop is equal to custom settings JVCO_MaxOrderRetries__c and it varies to the size of errorloglist
        while (errLogList.size() > 0 && j < MaxOrderRetries);

        //GREEN-28620 - end - 8Feb2018    
        List<SBQQ__Quote__c> quoteList = [SELECT Id, SBQQ__Account__c, SBQQ__Opportunity2__c, (SELECT Id FROM SBQQ__Orders__r) FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c IN: oppIdSet AND SBQQ__Primary__c = true];

        List<Opportunity> oppListForUpdate = new List<Opportunity>();
        List<JVCO_KATempHolder__c> kaTempHolderForUpsert = new List<JVCO_KATempHolder__c>();

        for(SBQQ__Quote__c qRec : quoteList){

            if(qRec.SBQQ__Orders__r.isEmpty()){

                Opportunity opp = new Opportunity();
                opp.Id = qRec.SBQQ__Opportunity2__c;
                opp.SBQQ__Ordered__c = false;
                oppListForUpdate.add(opp);

                //21-May-2019 rhys.j.c.dela.cruz Upserting kaTempHolder record if Ordering Success
                if(System.isQueueable()){

                    JVCO_KATempHolder__c kaTempHolder = new JVCO_KATempHolder__c();
                    kaTempHolder.Name = qRec.SBQQ__Opportunity2__c;
                    kaTempHolder.JVCO_AccountId__c = qRec.SBQQ__Account__c;
                    kaTempHolder.JVCO_OppId__c = qRec.SBQQ__Opportunity2__c;
                    kaTempHolder.JVCO_Ordered__c = false;
                    kaTempHolder.JVCO_ErrorMessage__c =  'Quote: ' + qRec.Id + ' - ' + errLogList[0].ffps_custRem__Detail__c;
                    kaTempHolderForUpsert.add(kaTempHolder);
                }

                mapOfQuotes.remove(null);

                if(!mapOfQuotes.containsKey(String.valueOf(qRec.Id))){
                    mapOfQuotes.put(String.valueOf(qRec.Id), '<li>Quote: ' + String.valueOf(qRec.Id) + ' - ' + String.valueof(errLogList[0].ffps_custRem__Detail__c) + '</li>');
                }
            }
            else{

                //21-May-2019 rhys.j.c.dela.cruz Upserting kaTempHolder record if Ordering Success
                if(System.isQueueable()){

                    JVCO_KATempHolder__c kaTempHolder = new JVCO_KATempHolder__c();
                    kaTempHolder.Name = qRec.SBQQ__Opportunity2__c;
                    kaTempHolder.JVCO_AccountId__c = qRec.SBQQ__Account__c;
                    kaTempHolder.JVCO_OppId__c = qRec.SBQQ__Opportunity2__c;
                    kaTempHolder.JVCO_Ordered__c = true;
                    kaTempHolderForUpsert.add(kaTempHolder);
                }

                //Moved mapOfQuotes Map from Inner Loop
                if(mapOfQuotes.containsKey(String.valueOf(qRec.Id))){
                        mapOfQuotes.remove(String.valueOf(qRec.Id));
                }
            }
        }

        if(!oppListForUpdate.isEmpty()){
            update oppListForUpdate;
        }

        if(System.isQueueable()){

            if(!kaTempHolderForUpsert.isEmpty()){
                upsert kaTempHolderForUpsert Name;
            }
        }

        return mapOfQuotes;
    }

    //Method for sending email when ordering started/ended
    public static void sendOrderingEmail(Account accountRecord, Integer origRecSize, Boolean queueableProcess, Boolean isStarted, Map<String, String> quoteStringMap, Boolean isReview){

        Integer countOfOrdered = 0;
        String errorMessage = '';
        String errorQuoteIds = '';

        List<JVCO_KATempHolder__c> kaTempList = new List<JVCO_KATempHolder__c>();

        //Check the KA Temp Holder if queueable job
        if(System.isQueueable()){

            kaTempList = [SELECT Id, JVCO_AccountId__c, JVCO_OppId__c, JVCO_Contracted__c, JVCO_Ordered__c, JVCO_ProcessCompleted__c, JVCO_ErrorMessage__c FROM JVCO_KATempHolder__c WHERE JVCO_AccountId__c =: accountRecord.Id AND JVCO_OppId__c != null];

            if(!kaTempList.isEmpty()){

                for(JVCO_KATempHolder__c kaTempRec : kaTempList){

                    if(kaTempRec.JVCO_Ordered__c){
                        countOfOrdered++;
                    }
                    else{

                        errorMessage += '<li>' + kaTempRec.JVCO_ErrorMessage__c + '</li>';
                    }
                }

                errorQuoteIds = errorMessage;
                origRecSize = countOfOrdered;
            }
        }
        else{

            //Replace variables depending if queueable or not
            errorQuoteIds = String.join(quoteStringMap.values(), '');
            origRecSize = origRecSize - quoteStringMap.size();
        }

        //User currentUser = [SELECT Id, Email FROM USER WHERE Id = :UserInfo.getUserId() LIMIT 1];

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(UserInfo.getUserId());
        mail.setSaveAsActivity(false);

        mail.setReplyTo('no-reply@salesforce.com');
        if(queueableProcess && isStarted){
            mail.setSenderDisplayName('Queueable Processing');
            mail.setSubject('Order Quotes Queueable Process Started');
        }
        else if(!queueableProcess && isStarted){
            mail.setSenderDisplayName('Batch Processing');
            mail.setSubject('Order Quotes Batch Process Started');
        }
        else if(queueableProcess && !isStarted){
            mail.setSenderDisplayName('Queueable Processing');
            mail.setSubject('Order Quotes Queueable Process Completed');
        }
        else{
            mail.setSenderDisplayName('Batch Processing');
            mail.setSubject('Order Quotes Batch Process Completed');
        }
        
        String emailMsg;
        //Is started - Started Job
        if(isStarted){
            emailMsg = '<html><body>This is to notify you that the process to Order Quotes has now been started for ' + accountRecord.Name + ' ('+ System.now() + '). <br>Number of Renew Quotes to be Ordered: ' + origRecSize + ' records.';
            emailMsg += ' </body></html> ';
        }
        else{
            emailMsg = '<html><body>This is to notify you that the process to Order Quotes has now completed for ' + accountRecord.Name + ' ('+ System.now() + '). <br>Number of Renew Quotes Ordered: ' + origRecSize + ' records.';
            if(errorQuoteIds != ''){
                emailMsg += '<br>Please see errors below:<br>';
                emailMsg += '<ul>' + errorQuoteIds + '</ul>';
            }
            emailMsg += ' </body></html> ';
        }

        mail.setHTMLBody(emailMsg);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

        if(queueableProcess && !isStarted){
            if(isReview){
                JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_OrderReviewQuotesQueueable__c', '','JVCO_OrderReviewQuotesLastSubmitted__c');
            }
            else{
                JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_OrderRenewQuotesQueueable__c', '','JVCO_OrderRenewQuotesLastSubmitted__c');
            }    
        }
        
    }
}