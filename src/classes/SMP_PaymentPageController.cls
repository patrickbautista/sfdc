public with sharing class SMP_PaymentPageController {
    public Integer stepNo {get; set;}
    public Account account {get; set;}
    public Contact contact {get; set;}
    public Decimal inputtedAmount {get; set;}
    public Decimal maxAmount{get; set;}
    public List<salesInvoiceWrapper> salesInvoiceWrapperList {get; set;}
    public List<c2g__codaInvoice__c> salesInvoiceList {get; set;}
    public List<c2g__codaInvoice__c> selectedSalesInvoiceList {get; set;}
    public List<String> invoiceWithActiveIDDList {get;set;}
    public Set<String> selectedSInvSet;
    public Set<String> invoiceWithActiveDDList;
    public SMP_PaymentPageController(Apexpages.StandardController controller){
        inputtedAmount = 0;
        maxAmount = 0;
        stepNo = 1;
        invoiceWithActiveDDList = new Set<String>();
        account = (Account)controller.getRecord();
        
        account = [SELECT Id, Name, JVCO_Billing_Contact__c, JVCO_Invoice_Group_Id__c, JVCO_Billing_Address__c,
                   c2g__CODABillingAddressIsValid__c, Billing_Email__c, Billing_Firstname__c, Billing_Surname__c, c2g__CODABillingMethod__c,
                   JVCO_In_Infringement__c, JVCO_In_Enforcement__c, JVCO_Credit_Status__c,
                   JVCO_Billing_Phone__c, BillingAddress, BillingStreet, BillingCity, BillingPostalCode, Temp_Card_Email__c FROM Account WHERE Id = :account.Id];

        contact = [SELECT Id, Email, FirstName, LastName, MailingStreet, MailingCity, 
                 MailingPostalCode FROM Contact WHERE Id = :account.JVCO_Billing_Contact__c];

        salesInvoiceList = [SELECT Id, Name, c2g__Account__c, JVCO_Invoice_Group__c, c2g__OutstandingValue__c 
                        FROM c2g__codaInvoice__c WHERE c2g__Account__c = :account.Id];
        

        system.debug('##### salesinvoiceResults: '+ salesInvoiceList);
        
        checkInfringementEnforcement();
        
        invoiceWithActiveIDDList = checkActiveIDDMap(salesInvoiceList);
        
        salesInvoiceWrapperList = new List<salesInvoiceWrapper>();
        for(c2g__codaInvoice__c si :salesInvoiceList){
            salesInvoiceWrapper wrapper = new salesInvoiceWrapper();
            wrapper.Id = si.Id;
            wrapper.name = si.Name;
            wrapper.totalInvoiceValue = si.c2g__OutstandingValue__c;
            wrapper.selected = false;
            if(si.JVCO_Invoice_Group__c == null){
                wrapper.hasDD = false;
             }else{
                 wrapper.hasDD = true;
             }

            salesInvoiceWrapperList.add(wrapper);   
        }
        system.debug('##### salesInvoiceWrapperList: '+ salesInvoiceWrapperList);
    }
    
    public void recalculateTotals(){
        selectedSInvSet = new Set<String>();
        List<String> selectedSaleInvoiceIdList = new List<String>();
        inputtedAmount = 0;
        for(salesInvoiceWrapper si :salesInvoiceWrapperList){
            if(invoiceWithActiveIDDList.contains(si.Name) && si.selected){
                invoiceWithActiveDDList.add(si.Name);
            }
            else if(invoiceWithActiveIDDList.contains(si.Name) && !si.selected){
                invoiceWithActiveDDList.remove(si.Name);
            }
            else if(si.selected){
                inputtedAmount += si.totalInvoiceValue;
                maxAmount += si.totalInvoiceValue;
                selectedSInvSet.add(si.Id); // 01/05/2020 - joseph.g.barrameda  - get selected Sales Invoice that will created Invoice group in PCI page
                selectedSaleInvoiceIdList.add(si.Id);
            }
        }
        selectedSalesInvoiceList = [SELECT Id, c2g__Account__c, JVCO_Invoice_Group__c, c2g__OutstandingValue__c 
                                    FROM c2g__codaInvoice__c WHERE Id IN :selectedSaleInvoiceIdList];
    }
    
    public static List<String> checkActiveIDDMap(List<c2g__codaInvoice__c> invoiceList){
        List<String> invoiceNameList = new List<String>();
        //Get invoice name to get related CC Mapping record
        for(c2g__codaInvoice__c invoice : invoiceList){
            invoiceNameList.add(invoice.Name);
        }
        Map<Id, List<String>> invGrpIdToInvNameMap = new Map<Id, List<String>>();
        //Get related CC Mapping record and get invoice group
        for(JVCO_CC_Outstanding_Mapping__c ccMap : [SELECT JVCO_CC_Invoice_Group__c,
                                                    JVCO_Sales_Invoice_Name__c 
                                                    FROM JVCO_CC_Outstanding_Mapping__c
                                                    WHERE JVCO_Sales_Invoice_Name__c IN: invoiceNameList
                                                    AND JVCO_CC_Invoice_Group__c != null])
        {
            if(!invGrpIdToInvNameMap.containsKey(ccMap.JVCO_CC_Invoice_Group__c)){
                invGrpIdToInvNameMap.put(ccMap.JVCO_CC_Invoice_Group__c, new List<String>());
            }
            if(invGrpIdToInvNameMap.containsKey(ccMap.JVCO_CC_Invoice_Group__c)){
                invGrpIdToInvNameMap.get(ccMap.JVCO_CC_Invoice_Group__c).add(ccMap.JVCO_Sales_Invoice_Name__c);
            }
        }
        Map<Id, List<String>> IDDGrpIdToInvNameMap = new Map<Id, List<String>>();
        //Get related SMP invoice group and get Income DD
        for(SMP_DirectDebit_GroupInvoice__c iddInvGrp : [SELECT Income_Direct_Debit__c,
                                                         Invoice_Group__c
                                                         FROM SMP_DirectDebit_GroupInvoice__c 
                                                         WHERE Invoice_Group__c IN:invGrpIdToInvNameMap.keyset()])
        {
            if(!IDDGrpIdToInvNameMap.containsKey(iddInvGrp.Income_Direct_Debit__c)){
                IDDGrpIdToInvNameMap.put(iddInvGrp.Income_Direct_Debit__c, new List<String>());
            }
            if(IDDGrpIdToInvNameMap.containsKey(iddInvGrp.Income_Direct_Debit__c)){
                IDDGrpIdToInvNameMap.get(iddInvGrp.Income_Direct_Debit__c).addAll(invGrpIdToInvNameMap.get(iddInvGrp.Invoice_Group__c));
            }
        }
        List<String> invoiceCollectionWithActiveIDDList = new List<String>();
        //Get related IDD and if theres an active IDD put it to list
        for(Income_Direct_Debit__c IDD : [SELECT id
                                          FROM Income_Direct_Debit__c
                                          WHERE DD_Status__c NOT IN ('Cancelled', 'Cancelled by Originator', 'Cancelled by Payer', 'Expired')
                                          AND Id IN : IDDGrpIdToInvNameMap.keySet()])
        {
            invoiceCollectionWithActiveIDDList.addAll(IDDGrpIdToInvNameMap.get(IDD.Id));
        }
        return invoiceCollectionWithActiveIDDList;
    }
    public static JVCO_Invoice_Group__c createInvoiceGroup(String paymentMethod, Boolean isPaperDebit, List<c2g__codaInvoice__c> invoicesToProcess){
        // Construct map of selected invoice records with the needed fields
        Map<Id, c2g__codaInvoice__c> invoiceMap = new Map<Id, c2g__codaInvoice__c>(
                                                            [SELECT JVCO_Invoice_Group__c, 
                                                            JVCO_Payment_Method__c, Name, 
                                                            JVCO_Outstanding_Amount_to_Process__c, 
                                                            c2g__OutstandingValue__c
                                                            FROM c2g__codaInvoice__c 
                                                            WHERE Id IN :invoicesToProcess]);
        List<JVCO_CC_Outstanding_Mapping__c> ccOutsToUpdate = new List<JVCO_CC_Outstanding_Mapping__c>();
        Double totalAmount = 0.00;
        String slsInvName = '';
        for (Id invId : invoiceMap.keySet()){
            c2g__codaInvoice__c invoice = invoiceMap.get(invId);
            totalAmount += invoice.c2g__OutstandingValue__c;
            slsInvName += invoice.Name + ',';
            //Create a Record to used as mapping for two or more CC/DD payments to get their outstanding values
            JVCO_CC_Outstanding_Mapping__c ccOutsAmt = 
                new JVCO_CC_Outstanding_Mapping__c(JVCO_CC_Outstanding_Value__c = invoice.c2g__OutstandingValue__c,
                                                   JVCO_Sales_Invoice_Name__c = invoice.Name);
            ccOutsToUpdate.add(ccOutsAmt);
            // Direct Debit
            if(paymentMethod.contains('Direct Debit')){
                invoice.JVCO_Outstanding_Amount_to_Process__c = invoice.c2g__OutstandingValue__c;
            }
            invoice.JVCO_Payment_Method__c = paymentMethod;
            invoiceMap.put(invoice.id, invoice);
        }
        
        // Create Invoice Group record using the computed totalAmount
        JVCO_Invoice_Group__c invoiceGroup = new JVCO_Invoice_Group__c(
                                        JVCO_Total_Amount__c = totalAmount,
                                        JVCO_Paper_Debit__c = isPaperDebit,
                                        CC_SINs__c = slsInvName);
        insert invoiceGroup;

        for(c2g__codaInvoice__c inv : invoiceMap.values()){
            inv.JVCO_Invoice_Group__c = invoiceGroup.Id;
        }
        // Insert the list of mapping
        if(!ccOutsToUpdate.isEmpty()){
            for(JVCO_CC_Outstanding_Mapping__c ccOuts : ccOutsToUpdate){
                ccOuts.JVCO_CC_Invoice_Group__c = invoiceGroup.id;
            }
            insert ccOutsToUpdate;
        }
        try{
            update invoiceMap.values();
        }catch (Exception e){}
        
        return [SELECT Id, Name, JVCO_Total_Amount__c FROM JVCO_Invoice_Group__c WHERE Id = :invoiceGroup.Id];
    }

    public Pagereference constructSinglePaymentLink(){
        JVCO_Invoice_Group__c invoiceGroup = createInvoiceGroup('Debit/Credit Card', false, selectedSalesInvoiceList);//patrick change payment method
        String invoiceIdString = '';
        for(c2g__codaInvoice__c invoice :selectedSalesInvoiceList){
            invoiceIdString = invoiceIdString + invoice.Id + ',';
            invoice.JVCO_Invoice_Group__c = invoiceGroup.Id;
        }

        system.debug('##### invoiceGroup: '+ invoiceGroup);
        Decimal invoiceAmount = 0;
        if(Test.isRunningTest())
        {
            invoiceAmount = 10;
        }
        else
        {
            invoiceAmount = maxAmount/inputtedAmount;

        }

        // for(c2g__codaInvoice__c invoice :selectedSalesInvoiceList){
        //     invoice.c2g__OutstandingValue__c = invoiceAmount;
        // }
        // update selectedSalesInvoiceList;
        

        SMP_Payment_Config__c paymentConfig = SMP_Payment_Config__c.getInstance();

        if(!String.isEmpty(contact.Email)){
            account.Temp_Card_Email__c = contact.Email;
            update account;
        }
        
        CardPaymentHelper.MOTOPayment payment = new CardPaymentHelper.MOTOPayment();
        payment.billingAddress = contact.MailingStreet;
        payment.billingCity = contact.MailingCity;
        payment.billingCountryCode = 'GB';
        payment.billingEmail = contact.Email;
        payment.billingFirstName = contact.FirstName;
        payment.billingPostcode = contact.MailingPostalCode;
        payment.billingSurname = contact.LastName;
        payment.deliveryAddress = contact.MailingStreet;
        payment.deliveryCity = contact.MailingCity;
        payment.deliveryCountryCode = 'GB';
        payment.deliveryEmail = contact.Email;
        payment.deliveryFirstName = contact.FirstName;
        payment.deliveryPostCode = contact.MailingPostalCode;
        payment.deliverySurname = contact.LastName;
        payment.firstReferenceId = contact.Id;
        payment.firstReferenceName = 'Contact';
        payment.firstReferenceObject = 'Contact__c';
        payment.firstReferenceObjectName = 'Contact';
        payment.thirdReferenceId = selectedSalesInvoiceList[0].JVCO_Invoice_Group__c;
        payment.thirdReferenceObject = 'Invoice_Group__c';
        payment.thirdReferenceObjectName = 'Invoice Group';
        payment.secondReferenceId = account.Id;
        payment.secondReferenceObject = 'Account__c';
        payment.secondReferenceObjectName = 'Account';
        payment.incomeusername = paymentConfig.Income_User_name__c;
        payment.isMultiCurrency = false;
        payment.paymentDescription = 'PPL PRS Card Payment';
        payment.rpEnabled = 'false';
        payment.sagepayvendor = paymentConfig.SagePayVendor__c;
        payment.saleamount = string.valueOf(inputtedAmount);
        payment.salesCurrencyCode = 'GBP';
        payment.paymentReason = invoiceIdString;
        payment.softwareVersion = '3';

        return CardPaymentHelper.generateMOTOCardPaymentURL(payment);
    }
    
    public void next(){
        system.debug('##### inputted: ' + inputtedAmount);       
        system.debug('##### maxAmount: ' + maxAmount);
        if(invoiceWithActiveDDList.size() > 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.JVCO_PaymentCCButtonError));
        }
        else if(inputtedAmount == 0 && stepNo == 1){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                    'Please select a sales invoice with value.'));
        }
        else if((inputtedAmount < 1 || inputtedAmount > maxAmount) && stepNo == 1)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Amount value is invalid'));

        }
        else if(stepNo < 4){
            stepNo ++;
            
        }
    }

    public void back(){
        if(stepNo > 1){
            stepNo --;
        }
    }
    
    public void checkInfringementEnforcement(){
        //Getting Permission set of the user and see if it's has Enforcement Advisors
        String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
        Map<Id,PermissionSet> enAdPermSet = new Map<Id,PermissionSet>([SELECT Id FROM PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors']);
        List<PermissionSetAssignment> permissionSetList = [SELECT Id FROM PermissionSetAssignment WHERE 
                                                           AssigneeId = :UserInfo.getUserId() AND PermissionSetId IN :enAdPermSet.keySet()];
        
        if((account.JVCO_In_Infringement__c == TRUE || account.JVCO_In_Enforcement__c == TRUE) 
           && (account.JVCO_Credit_Status__c == 'Enforcement - Debt Recovery' ||
               account.JVCO_Credit_Status__c == 'Enforcement - Infringement') &&
           profileName <> 'System Administrator' && permissionSetList.isEmpty())
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.JVCO_CreditCardnfringementErrorMess));
            stepNo = 0;
        }
    }
    
    public Pagereference cancel(){
        Pagereference ref = new Pagereference('/' + account.Id);
        ref.setRedirect(true);
        return ref;
    }

    public class salesInvoiceWrapper{
        public String Id {get; set;}
        public c2g__codaInvoice__c salesInvoice {get; set;}
        public String name {get; set;}
        public Decimal totalInvoiceValue {get; set;}
        public Boolean selected {get; set;}
        public Boolean hasDD {get; set;}
    }
    
    public pagereference redirectToPCI(){
    
        String invoiceIdString = '';
        for(String invoice :selectedSInvSet){
            invoiceIdString = invoiceIdString + invoice + ',';
        }

        Pagereference ref = Page.JVCO_PaymentPCI; 
        ref.setRedirect(true);
        ref.getParameters().put('accountid',account.Id);
        ref.getParameters().put('contactid',contact.Id);
        ref.getParameters().put('amount',string.valueOf(inputtedAmount));
        ref.getParameters().put('selectedsinv',invoiceIdString);
        
        return ref; 
    
    }
  }