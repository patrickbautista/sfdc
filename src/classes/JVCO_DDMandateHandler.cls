/* ----------------------------------------------------------------------------------------------
    Author: patrick.t.bautista
    Company: Accenture
    Description: Creates Payment Agreement records for Accounts with existing Direct Debit
    Inputs: List<JVCO_DD_Mandate__c>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    21-Dec-2017 patrick.t.bautista   Initial version of function DD Mandate Checker
    03-Jan-2018 ranielle.j.c.tabora  Prevent DD Mandate Delete
    07-Jun-2018 patrick.t.bautista   GREEN-29920 - DD! Automatic DD Mandate Cancellations
    24-Oct-2018 patrick.t.bautista   GREEN-33740 - Refactor DD Cancellation
    11-Mar-2019 patrick.t.bautista   GREEN-34320 - Added condition when PPA = cancel, invoice group = Retired
----------------------------------------------------------------------------------------------- */
public class JVCO_DDMandateHandler 
{
    /*public static Boolean stopDDMandateHandlerBeforeUpdate = false;
    
    public static void onBeforeInsert(List<JVCO_DD_Mandate__c> newDDMandateList)
    {
        setDDmandateToTrue(newDDMandateList);
    }
    
    public static void onBeforeUpdate(List<JVCO_DD_Mandate__c> newDDMandateList)
    {
        if(!stopDDMandateHandlerBeforeUpdate)
        {
            setDDmandateToTrue(newDDMandateList);
        }
    }
    
    public static void onBeforeDelete(List<JVCO_DD_Mandate__c> newDDMandateList)
    {
        preventDDMandateDeletion(newDDMandateList);
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: patrick.t.bautista
        Company: Accenture
        Description: Create method setDDmandateToTrue to check if DD Mandate is Valid and for Deactivating of DD Mandate
        Inputs: List<JVCO_DD_Mandate__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        21-Dec-2017 patrick.t.bautista   Initial version of function DD Mandate Checker
        07-Jun-2018 patrick.t.bautista   GREEN-29920 - DD! Automatic DD Mandate Cancellations
    ----------------------------------------------------------------------------------------------- */
    /*private static void setDDmandateToTrue(List<JVCO_DD_Mandate__c> ddMandateList) 
    {
        Map<Id, List<JVCO_DD_Mandate__c>> accIdToDDMandateListMap = new Map<Id, List<JVCO_DD_Mandate__c>>();
        // Store DD Mandate Account Number and Sort Code
        Map<String, String> accountNumberToSortCodeMap = new Map<String, String>();
        // Loop to get DD Mandate Account number and Sort code
        for(JVCO_DD_Mandate__c ddMandate : ddMandateList)
        {
            if(ddMandate.JVCO_Deactivated__c)
            {
                if(!accIdToDDMandateListMap.containsKey(ddMandate.JVCO_Licence_Account__c))
                {
                    accIdToDDMandateListMap.put(ddMandate.JVCO_Licence_Account__c, new List<JVCO_DD_Mandate__c>());
                }
                accIdToDDMandateListMap.get(ddMandate.JVCO_Licence_Account__c).add(ddMandate);
            }
            accountNumberToSortCodeMap.put(ddMandate.JVCO_Account_Number__c, ddMandate.JVCO_Sort_Code__c);
        }
        
        if(!accIdToDDMandateListMap.isEmpty())
        {
            //Store PPA to cancel per DD Mandate
            Set<PAYREC2__Payment_Agreement__c> updatedAgreementList = new Set<PAYREC2__Payment_Agreement__c>();
            Set<Id> invoiceGroupIdSet = new Set<Id>();
            // Loop to get PPA to Cancel
            for(PAYREC2__Payment_Agreement__c agreement : [SELECT PAYREC2__Account__c, PAYREC2__Status__c, 
                                                           PAYFISH3__Current_Bank_Account__c,
                                                           PAYREC2__Payment_Schedule__r.PAYREC2__Day__c, 
                                                           PAYREC2__Payment_Schedule__r.PAYREC2__Max__c,
                                                           PAYFISH3__Current_Bank_Account__r.PAYACCVAL1__Account_Number__c,
                                                           PAYFISH3__Current_Bank_Account__r.PAYACCVAL1__Sort_Code__c,
                                                           JVCO_Invoice_Group__c
                                                           FROM PAYREC2__Payment_Agreement__c
                                                           WHERE PAYREC2__Account__c IN : accIdToDDMandateListMap.keySet()
                                                           AND PAYREC2__Status__c NOT IN ('Expired','Cancel','Errored')])
            {
                for(JVCO_DD_Mandate__c ddMandate : accIdToDDMandateListMap.get(agreement.PAYREC2__Account__c))
                {
                    if(agreement.PAYFISH3__Current_Bank_Account__r.PAYACCVAL1__Account_Number__c == ddMandate.JVCO_Account_Number__c &&
                       agreement.PAYFISH3__Current_Bank_Account__r.PAYACCVAL1__Sort_Code__c == ddMandate.JVCO_Sort_Code__c &&
                       agreement.PAYREC2__Payment_Schedule__r.PAYREC2__Max__c == ddMandate.JVCO_Number_of_Payments__c &&
                       agreement.PAYREC2__Payment_Schedule__r.PAYREC2__Day__c == String.valueOf(ddMandate.JVCO_Collection_Day__c))
                    {
                        agreement.PAYREC2__Status__c = 'Cancel';
                        updatedAgreementList.add(agreement);
                        invoiceGroupIdSet.add(agreement.JVCO_Invoice_Group__c);
                    }
                }
            }
            update new List<PAYREC2__Payment_Agreement__c>(updatedAgreementList);
            //Store and update Salesinvoice related to PPA
            if(!invoiceGroupIdSet.isEmpty())
            {
                List<c2g__codaInvoice__c> sInvList = [SELECT Id FROM c2g__codaInvoice__c WHERE JVCO_Invoice_Group__c IN :invoiceGroupIdSet];
                for(c2g__codaInvoice__c sInv : sInvList)
                {
                    sInv.JVCO_Invoice_Group__c = null;
                }
                update sInvList;
            }
        }
        
        // Store Account Number to update
        Set<string> payBankAccToDDmandate = new Set<string>();
        for(PAYACCVAL1__Bank_Account__c bankAcctoMap : [SELECT PAYACCVAL1__Account_Number__c, PAYACCVAL1__Sort_Code__c 
                                                        FROM PAYACCVAL1__Bank_Account__c 
                                                        WHERE PAYACCVAL1__Account_Number__c IN: accountNumberToSortCodeMap.keySet()
                                                        AND PAYACCVAL1__Sort_Code__c IN: accountNumberToSortCodeMap.values()])
        {
            payBankAccToDDmandate.add(bankAcctoMap.PAYACCVAL1__Account_Number__c);
        }
        
        // Store Accounts to be updated
        Set<Account> accountSet = new Set<Account>();
        // Sets the DD Mandate Checker field to true when DD Mandate is not valid
        for(JVCO_DD_Mandate__c ddMandate : ddMandateList)
        {
            Account acc = new Account();
            acc.id = ddMandate.JVCO_Licence_Account__c;
            acc.JVCO_DD_Mandate_Checker__c = !payBankAccToDDmandate.contains(ddMandate.JVCO_Account_Number__c);
            accountSet.add(acc);
        }
        update new List<Account>(accountSet);
    }*/

    /* ----------------------------------------------------------------------------------------------
        Author: ranielle.j.c.tabora
        Company: Accenture
        Description: Create method preventDDMandateDeletion to Prevent DD Mandate Delete
        Inputs: List<JVCO_DD_Mandate__c>
        Returns: void
        <Date>      <Authors Name>        <Brief Description of Change> 
        21-Dec-2017 ranielle.j.c.tabora   Initial version of function DD Mandate Delete
    ----------------------------------------------------------------------------------------------- */
    /*private static void preventDDMandateDeletion(List<JVCO_DD_Mandate__c> ddMandateList)
    {
        Map<Id, JVCO_DD_Mandate__c> ddMandateMap = new map<Id, JVCO_DD_Mandate__c>(ddMandateList);
        User u = [SELECT Profile.Name, 
                    (SELECT PermissionSet.Name 
                    FROM PermissionSetAssignments 
                    WHERE PermissionSet.Name = 'JVCO_Enforcement_Advisors')
                FROM User 
                WHERE Id = :UserInfo.getUserId()];

        for(JVCO_DD_Mandate__c ddMandate : [SELECT Id, 
                                            JVCO_Licence_Account__r.JVCO_In_Infringement__c,
                                            JVCO_Licence_Account__r.JVCO_In_Enforcement__c, 
                                            JVCO_Licence_Account__r.JVCO_Credit_Status__c
                                            FROM JVCO_DD_Mandate__c WHERE Id IN: ddMandateList])
        {
            if((ddMandate.JVCO_Licence_Account__r.JVCO_In_Infringement__c || ddMandate.JVCO_Licence_Account__r.JVCO_In_Enforcement__c) && 
                (ddMandate.JVCO_Licence_Account__r.JVCO_Credit_Status__c == 'Enforcement - Infringement' || 
                    ddMandate.JVCO_Licence_Account__r.JVCO_Credit_Status__c == 'Enforcement - Debt Recovery') &&
               u.Profile.Name != 'System Administrator' && u.PermissionSetAssignments.isEmpty())
            { 
                ddMandateMap.get(ddMandate.id).addError('Account Is In Infringement/In Enforcement, DD Mandate Can Not Be Deleted');
            }
        }
    }*/
}