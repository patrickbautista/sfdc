public class JVCO_CaseBeforeDeleteErrorLogic {

    private ApexPages.StandardController stdCtrl {get; set;}
    public Id CaseId;
    
    public String INVALID_CASE_ERR1 = 'You cannot delete a Case';
    
     public JVCO_CaseBeforeDeleteErrorLogic(ApexPages.StandardController ssc)
    {
        stdCtrl = ssc;
    }
    
    public PageReference init()
    {
        Case cases = (case)stdCtrl.getRecord();
        Case caseRecord = [SELECT Id, AccountId from Case where Id=: cases.Id]; 
        caseId = caseRecord.Id;
        Profile p = [select id, name from profile where name = 'System Administrator' limit 1];
        if(p.id == UserInfo.getProfileId()) {
            delete cases;
            pageReference page;
            if (caseRecord.accountId != null){
                page = new PageReference('/' + caseRecord.accountId);
            }
            else{
                page = new PageReference('/001/o');
            }
            page.setRedirect(TRUE);
            return page; 
            
        }
        else {   
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, INVALID_CASE_ERR1));
            return null;
        }
    }
    
    public PageReference returnToCase()
    {
        PageReference page = new PageReference('/'+caseId);
        page.setRedirect(TRUE);
        return page;
    }
    
}