public class JVCO_PostInvoiceQueueable Implements System.Queueable {

    public Integer batchSize;
    set<Id> invoiceIds;
    
    //how many queueables to launch in parallel (system limit 50)
    public static Integer defaultQueueableLimit = 50;
    public static Integer defaultBatchSize = 5;
    public static String defaultQuery = 'SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = \'Draft\' ORDER BY blng__Account__c LIMIT 50000';

    // method to launch a large number of queueable jobs to process posting of all cash entries
    public static void start() {
        start(defaultQuery,defaultBatchSize,defaultQueueableLimit);
    }

    public static void start(String queryString) {
        start(queryString,defaultBatchSize,defaultQueueableLimit);
    }
    
    public static void start(String queryString, Integer overrideBatchSize) {
        start(queryString,overrideBatchSize,defaultQueueableLimit);
    }
    
    public static void start(Integer overrideBatchSize) {
        start(defaultQuery,overrideBatchSize,defaultQueueableLimit);
    }
    
    public static void start(Integer overrideBatchSize, Integer overrideQueueableLimit) {
        start(defaultQuery,overrideBatchSize,overrideQueueableLimit);
    }
    
    public static void start(String queryString, Integer batchSize, Integer queueableLimit) {

        //select all invoice lines
        list<blng__Invoice__c> invoices = Database.query(queryString);

        //number of records to push into each queue. round up to ensure all records are processed even if some batches are of un
        Integer recordSplit = (Integer)((Decimal)invoices.size()/(Decimal)queueableLimit).round(System.RoundingMode.UP);

        //loop to create the correct number of queueable jobs whilst we have records
        for(Integer i =0; i < queueableLimit && !invoices.isEmpty(); i++) {

            JVCO_PostInvoiceQueueable piq = new JVCO_PostInvoiceQueueable();
            piq.invoiceIds = new set<Id>();
            piq.batchSize = batchSize;

            //put an appropriate number of records into the job
            for(Integer j =0; j < recordSplit && !invoices.isEmpty(); j++) {
                piq.invoiceIds.add(invoices.get(0).Id);
                invoices.remove(0);
            }

            System.enqueueJob(piq);
        }
    }

    //execute posting
    public void execute(QueueableContext ctx) {


        Integer linesAdded = 0;
        list<blng__Invoice__c> invoicesToPostInThisTransaction = new list<blng__Invoice__c>();
        
        
        list<blng__Invoice__c> invoicesToPost = [SELECT Id,blng__NumberOfInvoiceLines__c
        FROM blng__Invoice__c
        WHERE blng__InvoiceStatus__c = 'Draft' AND Id IN :invoiceIds];
        

        while(linesAdded < batchSize && !invoicesToPost.isEmpty()) {
            linesAdded += (Integer)invoicesToPost.get(0).blng__NumberOfInvoiceLines__c;
            invoicesToPostInThisTransaction.add(invoicesToPost.remove(0));
        }
        
        for(blng__Invoice__c inv : invoicesToPostInThisTransaction) {
            inv.blng__InvoiceStatus__c = 'Posted';
        }
        
        if(Test.isRunningTest()){
            update invoicesToPostInThisTransaction;
            invoicesToPostInThisTransaction = [SELECT Id,blng__NumberOfInvoiceLines__c, blng__InvoiceStatus__c
                                               FROM blng__Invoice__c
                                               WHERE blng__InvoiceStatus__c = 'Posted'];
            
            invoicesToPostInThisTransaction[0].blng__InvoiceStatus__c = 'Draft';
        }
       
        
        List<Database.SaveResult> saveResults = Database.update(invoicesToPostInThisTransaction,false);
        
        
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        
        for(Integer i = 0; i < invoicesToPostInThisTransaction.size(); i++) {

            if(!saveResults[i].isSuccess())
            {
                for(Database.Error objErr:saveResults[i].getErrors())
                {
                    ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                    errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                    errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(invoicesToPostInThisTransaction[i].Id);
                    errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                    errLog.ffps_custRem__Message__c = 'Posting of Invoice';
                    errLog.ffps_custRem__Grouping__c = 'PSQ-001';

                    errLogList.add(errLog);
                }
            }
        }

        insert errLogList;

        //launch next queueable
        if(!invoicesToPost.isEmpty()) {
            JVCO_PostInvoiceQueueable piq = new JVCO_PostInvoiceQueueable();
            piq.batchSize = batchSize;
            piq.invoiceIds = new set<Id>();
            
            for(blng__Invoice__c inv : invoicesToPost) {
                piq.invoiceIds.add(inv.Id);
            }
            if(!Test.isRunningTest()) {
                System.enqueueJob(piq);
            }
        }
    }
}