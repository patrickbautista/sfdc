@isTest
public class JVCO_salesRepBatchTest {
    
    /*@testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopOrderAfterUpdateHandler = true;
        JVCO_FFUtil.stopOrderBeforeUpdateHandler = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_FFUtil.stopContractHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopContractHandlerAfterUpdate = true;
        JVCO_FFUtil.stopCodaInvoiceHandlerAfterUpdate = true;
        JVCO_FFUtil.stopCodaCreditNoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopQuoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopQuoteHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopOpportunityHandlerBeforeUpdate = true;
        
        JVCO_FFUtil.stopContractDeletion = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        insert dt;
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim2('PRS');
        Test.startTest();
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        insert JVCO_TestClassHelper.setQuoteProcess();
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        Test.stopTest();

        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        blng__InvoiceRun__c invRun = new blng__InvoiceRun__c();
        invrun.blng__InvoiceDate__c = Date.today();
        invrun.blng__TargetDate__c = Date.today();
        insert invRun;

        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        bInv.blng__InvoiceRunCreatedBy__c = invRun.Id;
        insert bInv;
        
        blng__InvoiceLine__c bInvLine = JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);
        bInvLine.JVCO_Surcharge_Applicable__c = true;
        
        insert bInvLine;

        JVCO_General_Settings__c gs = new JVCO_General_Settings__c();
        gs.JVCO_Invoice_Schedule_Scope__c = 10;
        insert gs;
    }
    
    //Billing Invoice with Sales Invoice - Surcharge
    @isTest
    static void testCreateSingleOrderSurchargeInvoice()
    {   
        list<blng__Invoice__c> bInvList = new list<blng__Invoice__c>();
        blng__Invoice__c bInv = [SELECT Id FROM blng__Invoice__c LIMIT 1];
        
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        OrderItem oi = [SELECT Id FROM OrderItem LIMIT 1];  
        insert JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);
        bInvList.add(bInv);
        
        JVCO_InvoiceGenerationLogic.generateInvoiceFromSingleOrder(bInvList);
        Test.startTest();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        
        c2g__codaInvoice__c sInv = [SELECT Id, JVCO_Sales_Rep__c
                                    FROM c2g__codaInvoice__c LIMIT 1];
        sInv.JVCO_Sales_Rep__c = null;
        update sInv;
        bInv.JVCO_Invoice_Type__c = 'Surcharge';
        update bInv;
        id batchjobid = Database.executeBatch(new JVCO_salesRepBatch());
        Test.stopTest();
    }
    
    //Billing Invoice with Sales Invoice - Migrated
    @isTest
    static void testCreateSingleOrderMigratedInvoice()
    {   
        list<blng__Invoice__c> bInvList = new list<blng__Invoice__c>();
        blng__Invoice__c bInv = [SELECT Id FROM blng__Invoice__c LIMIT 1];
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        OrderItem oi = [SELECT Id FROM OrderItem LIMIT 1];  
        insert JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);
        bInvList.add(bInv);
        
        JVCO_InvoiceGenerationLogic.generateInvoiceFromSingleOrder(bInvList);
        Test.startTest();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        
        c2g__codaInvoice__c sInv = [SELECT Id, JVCO_Sales_Rep__c
                                    FROM c2g__codaInvoice__c LIMIT 1];
        sInv.JVCO_Sales_Rep__c = null;
        update sInv;
        bInv.JVCO_Migrated__c = true;
        update bInv;
        id batchjobid = Database.executeBatch(new JVCO_salesRepBatch());
        Test.stopTest();
    }
    
    //Billing Invoice with Sales Invoice - Order Not Null 
    @isTest
    static void testCreateSingleOrderInvoice()
    {   
        list<blng__Invoice__c> bInvList = new list<blng__Invoice__c>();
        blng__Invoice__c bInv = [SELECT Id FROM blng__Invoice__c LIMIT 1];
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        OrderItem oi = [SELECT Id FROM OrderItem LIMIT 1];  
        insert JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);
        bInvList.add(bInv);
        
        JVCO_InvoiceGenerationLogic.generateInvoiceFromSingleOrder(bInvList);
        Test.startTest();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        
        c2g__codaInvoice__c sInv = [SELECT Id, JVCO_Sales_Rep__c
                                    FROM c2g__codaInvoice__c LIMIT 1];
        sInv.JVCO_Sales_Rep__c = null;
        update sInv;
        id batchjobid = Database.executeBatch(new JVCO_salesRepBatch(bInv.Id));
        Test.stopTest();
    }
    
    //Billing Invoice with Sales Credit Note - Order Not Null
    @isTest
    static void testCreateCreditNoteFromSingleOrder()
    {
        list<blng__Invoice__c> bInvList = new list<blng__Invoice__c>();
        blng__Invoice__c bInv = [SELECT Id FROM blng__Invoice__c LIMIT 1];
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        OrderItem oi = [SELECT Id FROM OrderItem LIMIT 1];
        insert JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, -50, true);
        bInvList.add(bInv);
        
        Test.startTest();
        JVCO_CreditNoteGenerationLogic.generateCreditNote(bInvList);
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        c2g__codaCreditNote__c sCNote = [SELECT Id, JVCO_Sales_Rep__c
                                         FROM c2g__codaCreditNote__c LIMIT 1];
        sCNote.JVCO_Sales_Rep__c = null;
        update sCNote;
        id batchjobid = Database.executeBatch(new JVCO_salesRepBatch());
        Test.stopTest();
    }    

    //Billing Invoice with Sales Invoice and Sales Credit Note - Migrated
    @isTest
    static void testCreateMigratedInvoiceandCreditFromSingleOrder()
    {
        list<blng__Invoice__c> bInvList = new list<blng__Invoice__c>();
        blng__Invoice__c bInv = [SELECT Id FROM blng__Invoice__c LIMIT 1];
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        OrderItem oi = [SELECT Id FROM OrderItem LIMIT 1];
        insert JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, -50, true);
        bInvList.add(bInv);   
        
        JVCO_InvoiceGenerationLogic.generateInvoiceFromSingleOrder(bInvList);
        Test.startTest();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        
        c2g__codaInvoice__c sInv = [SELECT Id, JVCO_Sales_Rep__c
                                    FROM c2g__codaInvoice__c LIMIT 1];
        sInv.JVCO_Sales_Rep__c = null;
        update sInv;
        id batchjobid = Database.executeBatch(new JVCO_salesRepBatch());
        
        JVCO_CreditNoteGenerationLogic.generateCreditNote(bInvList);
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        c2g__codaCreditNote__c sCNote = [SELECT Id, JVCO_Sales_Rep__c
                                         FROM c2g__codaCreditNote__c LIMIT 1];
        sCNote.JVCO_Sales_Rep__c = null;
        update sCNote;
        bInv.JVCO_Migrated__c = true;
        update bInv;
        id batchjobid2 = Database.executeBatch(new JVCO_salesRepBatch());
        Test.stopTest();
    }
    
    //Billing Invoice with Sales Invoice and Sales Credit Note - Order Not Null
    @isTest
    static void testCreateInvoiceandCreditFromSingleOrder()
    {
        list<blng__Invoice__c> bInvList = new list<blng__Invoice__c>();
        blng__Invoice__c bInv = [SELECT Id FROM blng__Invoice__c LIMIT 1];
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        OrderItem oi = [SELECT Id FROM OrderItem LIMIT 1];
        insert JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, -50, true);
        bInvList.add(bInv);   
        
        JVCO_InvoiceGenerationLogic.generateInvoiceFromSingleOrder(bInvList);
        Test.startTest();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        
        c2g__codaInvoice__c sInv = [SELECT Id, JVCO_Sales_Rep__c
                                    FROM c2g__codaInvoice__c LIMIT 1];
        sInv.JVCO_Sales_Rep__c = null;
        update sInv;
        id batchjobid = Database.executeBatch(new JVCO_salesRepBatch());
        
        JVCO_CreditNoteGenerationLogic.generateCreditNote(bInvList);
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        c2g__codaCreditNote__c sCNote = [SELECT Id, JVCO_Sales_Rep__c
                                         FROM c2g__codaCreditNote__c LIMIT 1];
        sCNote.JVCO_Sales_Rep__c = null;
        update sCNote;
        id batchjobid2 = Database.executeBatch(new JVCO_salesRepBatch());
        Test.stopTest();
    }*/
        
}