public class JVCO_CashUnmatchingLogic
{
    public static Id openPeriodId;

    public static void undoCashMatching(c2g__codaCashMatchingHistory__c matchingHist)
    {
        JVCO_BackgroundMatchingLogic.stopMatching = TRUE;
        c2g.CODAAPICommon_8_0.Context context = new c2g.CODAAPICommon_8_0.Context();
        c2g.CODAAPICashMatchingTypes_8_0.Configuration configuration = new c2g.CODAAPICashMatchingTypes_8_0.Configuration();
        configuration.Account = c2g.CODAAPICommon.getRef(matchingHist.c2g__Account__c, null);
        configuration.MatchingCurrencyMode = c2g.CODAAPICashMatchingTypes_8_0.enumMatchingCurrencyMode.Account; 
        configuration.MatchingDate = System.Today();
        openPeriodId = openPeriodId == null ? matchingHist.c2g__Period__c : openPeriodId;
        configuration.MatchingPeriod = c2g.CODAAPICommon.getRef(openPeriodId, null);
        
        c2g.CODAAPICommon.Reference matchingReference = new c2g.CODAAPICommon.Reference();
        matchingReference.Id = matchingHist.c2g__MatchingReference__c;
        
        c2g.CODAAPICommon.Reference unmatchReference = c2g.CODAAPICashMatching_8_0.Unmatch(context, configuration, 
                                                        matchingReference, c2g.CODAAPICashMatchingTypes_8_0.enumUndoReason.MatchingError);
    }

    public static void rematchToRefund(c2g__codaCashEntry__c refundCashEntry, 
                                    List<c2g__codaTransactionLineItem__c> receiptCashTransLineList)
    {
        c2g__codaTransactionLineItem__c refundCashTransLine = [SELECT Id, c2g__Account__c, c2g__AccountOutstandingValue__c, 
                                                                c2g__Transaction__r.c2g__Period__c
                                                                FROM c2g__codaTransactionLineItem__c
                                                                WHERE c2g__Transaction__r.c2g__CashEntry__c = :refundCashEntry.Id
                                                                AND c2g__LineType__c = 'Account'];
        
        List<c2g__codaTransactionLineItem__c> matchToTransLineList = [SELECT ID, c2g__AccountOutstandingValue__c 
                                                                        FROM c2g__codaTransactionLineItem__c
                                                                        WHERE Id IN :receiptCashTransLineList];

        c2g.CODAAPICommon_8_0.Context context = new c2g.CODAAPICommon_8_0.Context();
        c2g.CODAAPICashMatchingTypes_8_0.Analysis analysisInfo = new c2g.CODAAPICashMatchingTypes_8_0.Analysis();

        c2g.CODAAPICashMatchingTypes_8_0.Configuration configuration = new c2g.CODAAPICashMatchingTypes_8_0.Configuration();
        configuration.MatchingDate = System.today();
        configuration.Account = c2g.CODAAPICommon.getRef(refundCashTransLine.c2g__Account__c, null);
        configuration.MatchingCurrencyMode = c2g.CODAAPICashMatchingTypes_8_0.enumMatchingCurrencyMode.Account; 
        configuration.MatchingPeriod = c2g.CODAAPICommon.getRef(refundCashTransLine.c2g__Transaction__r.c2g__Period__c, null);

        List<c2g.CODAAPICashMatchingTypes_8_0.Item> items = new List<c2g.CODAAPICashMatchingTypes_8_0.Item>();
        c2g.CODAAPICashMatchingTypes_8_0.Item itemRefund = new c2g.CODAAPICashMatchingTypes_8_0.Item();
        itemRefund.TransactionLineItem = c2g.CODAAPICommon.getRef(refundCashTransLine.Id, null);
        itemRefund.Paid = refundCashTransLine.c2g__AccountOutstandingValue__c;
        items.add(itemRefund);

        for(c2g__codaTransactionLineItem__c matchToTransLine: matchToTransLineList)
        {   
            c2g.CODAAPICashMatchingTypes_8_0.Item itemReceipt = new c2g.CODAAPICashMatchingTypes_8_0.Item();
            itemReceipt.TransactionLineItem = c2g.CODAAPICommon.getRef(matchToTransLine.Id, null);
            itemReceipt.Paid = matchToTransLine.c2g__AccountOutstandingValue__c;
            items.add(itemReceipt);
        }
        c2g.CODAAPICashMatching_8_0.Match(context, configuration, items, analysisInfo);      
    }
}