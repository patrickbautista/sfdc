/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_OverdueInstalment_Batch.cls 
   Description:     Batch Class for handler JVCO_OverdueInstalmentHandler

   Date            Version     Author                             Summary of Changes 
   -----------     -------     -----------------                  -----------------------------------
   02-Feb-2017     0.1         kristoffer.d.martin                Intial draft       
   26-Jul-2017     0.2         mel.andrei.b.santos                Added call of next batch in the finish method as per Jira Ticket GREEN-17932
   04-Aug-2017     0.3         mel.andrei.b.santos                provided batch size for calling the next batch
   28-Mar-2018     0.4         mary.ann.a.ruelan                  GREEN-31138. Updated batch to allow processing of installment lines related to a specific sales invoice
   13-Aug-2018     0.5         franz.g.a.dimaapi                  GREEN-33021 - Changed the starting query
------------------------------------------------------------------------------------------------ */
public class JVCO_OverdueInstalment_Batch implements Database.Batchable<sObject>, Database.Stateful
{
    private Boolean isSingleRecord = false;
    private Id sInvId;
    public Map<Id, Id> licAndCustAccIdMap = new Map<Id, Id>();
    
    public JVCO_OverdueInstalment_Batch(){}
    public JVCO_OverdueInstalment_Batch(Id sInvId)
    {
        this.sInvId = sInvId;
        isSingleRecord = true;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        
        if(isSingleRecord)
        {
            return Database.getQueryLocator([SELECT Id, c2g__Invoice__c
                                            FROM c2g__codaInvoiceInstallmentLineItem__c
                                            WHERE JVCO_DueDateExtension__c < :System.today() // GREEN-25022 10/31/2017 //GREEN-30931 15/03/2018
                                            AND JVCO_Payment_Status__c != 'Paid'
                                            AND c2g__Invoice__r.JVCO_Payment_Method__c != 'Direct Debit'
                                            AND c2g__Invoice__r.JVCO_Skip_OverdueInstallment_Batch__c = false
                                            AND c2g__Invoice__r.JVCO_Promise_to_Pay__c = null
                                            AND c2g__Invoice__c =: sInvId]);
        }else
        {
            return Database.getQueryLocator([SELECT Id, c2g__Invoice__c
                                            FROM c2g__codaInvoiceInstallmentLineItem__c
                                            WHERE JVCO_DueDateExtension__c < :System.today() // GREEN-25022 10/31/2017 //GREEN-30931 15/03/2018
                                            AND JVCO_Payment_Status__c != 'Paid'
                                            AND c2g__Invoice__r.JVCO_Payment_Method__c != 'Direct Debit'
                                            AND c2g__Invoice__r.JVCO_Skip_OverdueInstallment_Batch__c = false
                                            AND c2g__Invoice__r.JVCO_Promise_to_Pay__c = null]);
        }      
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        JVCO_OverdueInstalmentHandler.JVCO_OverdueInstalmentLogic(scope);
    }
    
    public void finish(Database.BatchableContext BC) 
    {
        //Calling next batch as per Ticket GREEN-17932 - Mel Andrei Santos 26-07-2017
        JVCO_SurchargeGeneration_Batch surchargeGenBatch = new JVCO_SurchargeGeneration_Batch();
        surchargeGenBatch.licAndCustAccIdMap = licAndCustAccIdMap;
        Id batchProcessId = Database.executeBatch(surchargeGenBatch,1); // provided batch size value = 1 Mel Andrei Santos 04/08/2017
    }
    
}