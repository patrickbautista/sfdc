/* -------------------------------------------------------------------------------------
    Name: JVCO_GenerateBlankQuotesBatch
    Description: Batch Class for creating blank quotes related to an Account's affiliation

    Date            Version     Author                Summary of Changes 
    -----------     -------     -----------------     ----------------------------------
    17-Nov-2017     0.1         jules.osberg.a.pablo  Intial creation
------------------------------------------------------------------------------------- */
global class JVCO_GenerateBlankQuotesBatch implements Database.Batchable<sObject>, Database.Stateful {

    global final Account accountRecord;
    global Date endDate;
    global Id pricebookId;
    global Integer amendQuoteRecCount;
    global Integer amendQuoteErrRecCount;
    global String amendQuoteErrRecMsg;
    public static Boolean isFromKAQuoteCreateBatch = false;
    global Integer generatedQuote;
    global Boolean isAmendment;

    global JVCO_GenerateBlankQuotesBatch(Account a, Date maxEndDate, Id pbId, Integer amendQuoteCount, Integer amendQuoteErrorCount, String amendQuoteErrorMsg, Boolean isAnAmendment) {
        accountRecord = a;
        generatedQuote = 0;
        endDate = maxEndDate;
        amendQuoteRecCount = amendQuoteCount;
        amendQuoteErrRecCount = amendQuoteErrorCount;
        amendQuoteErrRecMsg = amendQuoteErrorMsg; 
        isAmendment = isAnAmendment;
    }

    global Database.QueryLocator start (Database.BatchableContext BC) {
        System.debug('JVCO_KeyAccountQuoteCreationBatch START');
        String query = 'select Id, Name, JVCO_Venue__r.Name, JVCO_Account__c, JVCO_Venue__c, JVCO_Start_Date__c, JVCO_End_Date__c, JVCO_Omit_Quote_Line_Group__c from JVCO_Affiliation__c where JVCO_Omit_Quote_Line_Group__c = FALSE and JVCO_Account__c = '+ '\'' + accountRecord.Id + '\'' + ' and JVCO_End_Date__c = null AND Id NOT in (Select Affiliation__c from SBQQ__Subscription__c where SBQQ__Contract__r.JVCO_Renewal_Generated__c = false and SBQQ__Contract__r.AccountId = ' + '\'' + accountRecord.Id + '\'' + ') AND Id NOT in (Select JVCO_Affiliated_Venue__c from SBQQ__QuoteLineGroup__c where SBQQ__Quote__r.SBQQ__Status__c = \'Draft\' and SBQQ__Quote__r.SBQQ__Account__c = ' + '\'' + accountRecord.Id + '\'' + ') ';
        if(endDate != NULL) {
            query += 'and JVCO_Start_Date__c <= :endDate';
        }
        
        return Database.getQueryLocator(query);
    }

    global void execute (Database.BatchableContext BC, List<sObject> scope) {
        List<blng__ErrorLog__c> errLogList = new List<blng__ErrorLog__c>();
        if (scope.size() > 0) {
            isFromKAQuoteCreateBatch = true;    
            generatedQuote += JVCO_KeyAccountQuote.createQuote((List<JVCO_Affiliation__c>)scope, pricebookId, endDate, accountRecord);            
        }
    }

    global void finish (Database.BatchableContext BC) {
        AsyncApexJob aJob = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id = :BC.getJobId()];

        Integer totalQuotes = generatedQuote;
        Integer totalErrorQuotes = aJob.TotalJobItems - generatedQuote;
        String errorMessage = '<br>Amendment Quote Error: ' + amendQuoteErrRecMsg + 
                              '<br>Quote Error: ' + String.valueOf(aJob.ExtendedStatus) + '<br>';


        system.debug('sendEmailNotification totalQuotes: ' + totalQuotes);
        system.debug('sendEmailNotification totalErrorQuotes: ' + totalErrorQuotes);
        system.debug('sendEmailNotification errorMessage: ' + errorMessage);
        if(isAmendment){
        JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_GenerateReviewQuotesApexJob__c', '','JVCO_GenerateReviewQuotesLastSubmitted__c');
        }else{
            JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_GenerateRenewQuotesApexJob__c', '','JVCO_GenerateRenewQuotesLastSubmitted__c');
        }
        JVCO_KeyAccountQuote.sendEmailNotification(accountRecord, totalQuotes, totalErrorQuotes, errorMessage, amendQuoteRecCount, amendQuoteErrRecCount, aJob.Createdby.Email, isAmendment,null);
    }
}