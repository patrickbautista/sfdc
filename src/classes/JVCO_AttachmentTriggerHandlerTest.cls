@isTest
public class JVCO_AttachmentTriggerHandlerTest{
    
    @testSetup
    public static void setupData(){
        
        Account customerAcc = new Account();
        customerAcc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        customerAcc.TCs_Accepted__c = 'No';
        customerAcc.Type = 'Agency';
        customerAcc.c2g__CODADimension1__c = null;
        insert customerAcc;
    } 
    
    static testMethod void testAccountInsertAttachment()
    {                
        Account customerAcc=[SELECT Id from Account Limit 1];
        
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=customerAcc.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:customerAcc.id];
        System.assertEquals(1, attachments.size());      
    }
    
    static testMethod void testInEnforcementAccountInsertAttachment()
    {                
        Account customerAcc=[SELECT Id from Account Limit 1];
        customerAcc.JVCO_In_Enforcement__c= true;
        customerAcc.JVCO_Credit_Status__c = 'Enforcement - Debt Recovery';
        update customerAcc; 
                
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=customerAcc.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:customerAcc.id];
        System.assertEquals(1, attachments.size());      
    }
    
    static testMethod void testInEnforcementAccountUpdateAttachment()
    {                
        Account customerAcc=[SELECT Id from Account Limit 1];
        customerAcc.JVCO_In_Enforcement__c= true;
        customerAcc.JVCO_Credit_Status__c = 'Enforcement - Debt Recovery';
        update customerAcc; 
        
        System.assertEquals(true, customerAcc.JVCO_In_Enforcement__c);
                
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=customerAcc.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:customerAcc.id];
        System.assertEquals(1, attachments.size());     
        
        Blob newBodyBlob=Blob.valueOf('Unit Test Attachment Body - New Changes');
        attach.body = newBodyBlob;        
        update attach;  
        
        System.assertEquals(attach.body, newBodyBlob);     
    }
    
     static testMethod void testInEnforcementAccountDeleteAttachment()
    {                
        Account customerAcc=[SELECT Id from Account Limit 1];
        customerAcc.JVCO_In_Enforcement__c= true;
        customerAcc.JVCO_Credit_Status__c = 'Enforcement - Debt Recovery';
        update customerAcc; 
        
        System.assertEquals(true, customerAcc.JVCO_In_Enforcement__c);
                
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=customerAcc.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:customerAcc.id];
        System.assertEquals(1, attachments.size());     
        
        delete attach;  
    }
            
}