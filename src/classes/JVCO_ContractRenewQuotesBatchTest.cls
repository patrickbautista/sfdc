/* ----------------------------------------------------------------------------------------------
    Name: JVCO_ContractRenewQuotesBatchTest
    Description: Test Class for JVCO_ContractRenewQuotesBatch

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    29-Mar-2017     0.1         Jules Pablo         Intial creation
----------------------------------------------------------------------------------------------- */
@IsTest
private class JVCO_ContractRenewQuotesBatchTest 
{
    
    @testSetup static void setupTestData() 
    {
        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;
        
        Product2 prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;

        Opportunity o = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        o.SBQQ__Contracted__c = false;
        insert o;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Primary__c = true;
        q.SBQQ__Type__c = 'Renewal';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = acc2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        q.SBQQ__QuoteProcessId__c = qProcess.id;
        insert q;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        qL.SPVAvailability__c = true;
        insert ql;
        
        Contract cont = JVCO_TestClassObjectBuilder.createContract(acc2.id, q.id);
        insert cont;
        
        o.SBQQ__RenewedContract__c = cont.Id;
        update o;
        
        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();           
        Insert  venueRecord;   
        Test.startTest();
        JVCO_Affiliation__c  affilRecord = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, venueRecord.id);  
        Insert  affilRecord;   
        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;

        JVCO_Renewal_Settings__c rqcs = new JVCO_Renewal_Settings__c();
        rqcs.Name = 'Automatic Quote Completion';
        rqcs.days__c = 28;
        insert rqcs;

        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'ErrorNoSPV', Value__c = 'You can not contract the Quote since there are Quote Line(s) w/o SPV'));
        insert settings;
        Test.stopTest();
    }
    
    private static testMethod void testMethodContractRenewQuotesBatch() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name, JVCO_Contract_Batch_Size__c,JVCO_Account_Description__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        //  System.assertEquals(-1, a.JVCO_Contract_Batch_Size__c, 'NotMatch');
        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_ContractRenewQuotesBatch(a, 1), 1);
        Test.stopTest();
    }

    private static testMethod void testMethodContractRenewQuotesBatchTrue() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name, JVCO_Contract_Batch_Size__c,JVCO_Account_Description__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        //  System.assertEquals(-1, a.JVCO_Contract_Batch_Size__c, 'NotMatch');
        a.JVCO_Account_Description__c = 'Test';
        Update a;

        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_ContractRenewQuotesBatch(a, 1), 1);
        Test.stopTest();
    }

    private static testMethod void testMethodErrorSPV() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        SBQQ__Quote__c q = [select Id, SBQQ__Account__c from SBQQ__Quote__c where SBQQ__Account__c =: a.ID];
        SBQQ__QuoteLine__c qL = [select Id,SPVAvailability__c,ParentProduct__c from SBQQ__QuoteLine__c where SBQQ__Quote__c =: q.ID];
        qL.SPVAvailability__c = false;
        qL.ParentProduct__c = false;

        update qL;
        
        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_ContractRenewQuotesBatch(a, 1), 1);
        Test.stopTest();
    }

}