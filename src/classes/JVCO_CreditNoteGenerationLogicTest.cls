/**********************************************************************************
* @Author: 
* @Company: Accenture
* @Description: Test Class for JVCO_CreditNoteGenerationLogic
* @Created Date:
* @Revisions:
*      <Name>              <Date>          <Description>
*      jason.e.mactal      11.3.2017       Modified to avoid exceeding CPU Time Limit
*                                          Moved the creation of some test data inside test methods
*      franz.g.a.dimaapi   11.30.2017      Refactor Test Class to avoid cpu and soql 101
**********************************************************************************/
@isTest
public class JVCO_CreditNoteGenerationLogicTest
{
    /*@testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();        
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        //added
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_OpportunityTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_QuoteLineGroupTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        insert dt;
        
        JVCO_FFUtil.stopAccountDLRSHandler = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopCodaInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopContractHandlerAfterUpdate = true;
        JVCO_FFUtil.stopContractHandlerBeforeDelete = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_FFUtil.stopQuoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopQuoteHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopOrderAfterUpdateHandler = true;
        JVCO_FFUtil.stopOrderBeforeUpdateHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');

        //original //Test.startTest();
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        Test.startTest(); //added
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Test.stopTest(); //added
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;

        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        List<Order> orderList = new List<Order>();
        orderList.add(o);
        
        JVCO_General_Settings__c gs = new JVCO_General_Settings__c();
        gs.JVCO_Invoice_Schedule_Scope__c = 10;
        insert gs;
        
        //original //Test.stopTest();

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        insert bInv;
        blng__InvoiceLine__c bInvLine = JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, -50, true);
        bInvLine.JVCO_Surcharge_Applicable__c = true;
        bInvLine.Dimension_2s__c = 'PPL';
        JVCO_FFutil.stopBlngInvoiceLineHandlerBeforeInsert = true;
        insert bInvLine;
        Database.executeBatch(new JVCO_OrderMultipleBillNowBatch(orderList));
        
    }

    @isTest
    static void testCreateCreditNoteFromSingleOrder()
    {
        List<blng__Invoice__c> bInvList = [SELECT Id, blng__Account__c, blng__Order__c FROM blng__Invoice__c];
        Order o = new Order();
        o.Id = bInvList[0].blng__Order__c;
        o.JVCO_Order_Group__c = null;
        update o;
        Test.startTest();
        JVCO_CreditNoteGenerationLogic.generateCreditNote(bInvList);
        //added
        //c2g__codaCreditNote__c cNoteRes = [SELECT Id, c2g__NetTotal__c, JVCO_Credit_Reason__c FROM c2g__codaCreditNote__c order by CreatedDate DESC limit 1];
        //cNoteRes.JVCO_Credit_Reason__c = '';
        //update cNoteRes;
        //added
        Test.stopTest();
    }

    @isTest
    static void testCreateCreditNoteFromDataMig()
    {
        List<blng__Invoice__c> bInvList = [SELECT Id FROM blng__Invoice__c];
        Test.startTest();
        JVCO_CreditNoteGenerationLogic.generateCreditNoteFromDatamig(bInvList);
        Test.stopTest();
    }
	*/
}