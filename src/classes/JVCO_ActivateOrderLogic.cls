/* ----------------------------------------------------------------------------------------------
   Name: JVCO_ActivateOrderLogic.cls 
   Description: Business logic class for activating selected Order records

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  30-Mar-2017   0.1         ryan.i.r.limlingan  Intial creation
  18-Aug-2017   1.0         joseph.g.barrameda  Added criteria that activate only orders with Draft status and Completed price calculation status 
  ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_ActivateOrderLogic
{
    private ApexPages.StandardSetController stdCtrl;
    private List<Order> selectedOrders;
    public Integer numOfSelectedOrders {get; set;}
    public List<Order> retrievedOrders {get; set;}

    // Constants used for page messages
    public final String NO_RECORDS_ERR = System.Label.JVCO_OrderNoRecords;

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Class constructor
    Inputs: StandardSetController
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    30-Mar-2017 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public JVCO_ActivateOrderLogic(ApexPages.StandardSetController ssc)
    {
        stdCtrl = ssc;
        // Retrieve selected Order records from related list
        selectedOrders = (List<Order>)ssc.getSelected();
        numOfSelectedOrders = selectedOrders.size();
    }

 /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: <TBD>
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    06-Mar-2017 ryan.i.r.limlingan  Initial version of function
    18-Aug-2017 joseph.g.barrameda   Added criteria that filters only Draft status and Completed price calculation, and added error message
  ----------------------------------------------------------------------------------------------- */
    public PageReference activate()
    {
        List<Order> orderList = new List<Order>();
        if (numOfSelectedOrders > 0)
        {
            //Added by joseph.g.barrameda - 08/18/2017
            orderList = [SELECT Id, Status, AccountId, SBQQ__PriceCalcStatus__c  FROM Order WHERE Id IN :selectedOrders];
            List<Order> updateOrderList = new List<Order>();
            String errorMsgForPriceCalcStatusNotCompleted = 'You cannot activate the Orders until the Price Calculation Status is Completed. Please wait and Activate All once completed';
            
            //Added Not Needed status to criteria - rhys.j.c.dela.cruz - 3/26/2019
            for (Order o: orderList){
                if(o.Status == 'Draft' && o.SBQQ__PriceCalcStatus__c != 'Completed' && o.SBQQ__PriceCalcStatus__c != 'Not Needed'){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,errorMsgForPriceCalcStatusNotCompleted));
                    return null;
                }            
            }
            
            for (Order o:orderList)
            {
                if(o.Status == 'Draft' && (o.SBQQ__PriceCalcStatus__c == 'Completed' || o.SBQQ__PriceCalcStatus__c == 'Not Needed')){        //Filter only the Orders with Draft status, Completed and Not Needed priceCalcStatus 
                    o.Status = 'Activated';
                    updateOrderList.add(o);
                }
            }
            
            if (updateOrderList.size()>0){
                update updateOrderList;
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, NO_RECORDS_ERR));
            return null;
        }    
        return new PageReference('/' + orderList.get(0).AccountId);
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Returns to Account detail page
    Inputs: N/A
    Returns: PageReference
    <Date>      <Authors Name>      <Brief Description of Change> 
    30-Mar-2017 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public PageReference returnToAccount()
    {
        PageReference page = new PageReference(stdCtrl.cancel().getUrl());
        page.setRedirect(TRUE);
        return page;
    }
}