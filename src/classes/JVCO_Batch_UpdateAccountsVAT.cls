/*
Created by: Desiree Quijada
Date Created: 31-July-2017
Date Modified: 01-August-2017, 01-Sept-2017(Updated the query statement), 01-Sept-2017 (Catch errors ang create records for error logs)
Details: Update the Accounts with the correct VAT
Version: v2.0, v3.0
*/
global class JVCO_Batch_UpdateAccountsVAT implements Database.Batchable<sObject>{
    
    //query all accounts with c2g__CODAOutputVATCode__c equals to GB-O-CNV-STD or GB-O-CNV-EXMPT or GB-O-CNV-ZERO 
    public String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        query = 'SELECT ID, Name, c2g__CODAOutputVATCode__c, Account.c2g__CODAOutputVATCode__r.Name FROM Account where c2g__CODAOutputVATCode__R.Name = \'GB-O-CNV-STD\'' 
            +' OR c2g__CODAOutputVATCode__R.Name = \'GB-O-CNV-EXMPT\' OR c2g__CODAOutputVATCode__R.Name = \'GB-O-CNV-ZERO\'';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> accountList) {
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        c2g__codaTaxCode__c std = [SELECT ID FROM c2g__codaTaxCode__c WHERE NAME = 'GB-O-STD-LIC' ];
        c2g__codaTaxCode__c exmpt  = [SELECT ID FROM c2g__codaTaxCode__c WHERE NAME = 'GB-O-LIC-EXMPT' ];
        c2g__codaTaxCode__c zero  = [SELECT ID FROM c2g__codaTaxCode__c WHERE NAME = 'GB-O-LIC-ZERO' ];
        
        if(accountList!= null){
            for(Account a : accountList){
                if (a.c2g__CODAOutputVATCode__r.Name == 'GB-O-CNV-STD'){
                    a.c2g__CODAOutputVATCode__c = std.ID;
                }
                else if (a.c2g__CODAOutputVATCode__r.Name == 'GB-O-CNV-EXMPT'){
                    a.c2g__CODAOutputVATCode__c = exmpt.ID;
                }
                else if(a.c2g__CODAOutputVATCode__r.Name == 'GB-O-CNV-ZERO'){
                    a.c2g__CODAOutputVATCode__c = zero.ID;
                }
            }            
        }
        

        //update accountList;

        if(!accountList.isEmpty()){
          
          if(Test.isRunningTest()){
              accountList[0].JVCO_Customer_Account__c = null;
          }
          
          List<Database.SaveResult> saveResList = Database.update(accountList,false);

          for(Integer i=0; i < accountList.size(); i++){
            Database.SaveResult indRes = saveResList[i];
            Account acc = accountList[i];
            if(!indRes.isSuccess()){
              for(Database.Error err : indRes.getErrors()) {
                  ffps_custRem__Custom_Log__c newErr =  new ffps_custRem__Custom_Log__c();
                  if(acc.ID != null || !String.isBlank(String.valueOf(acc.ID))){
                    if(err.getMessage() != null || !String.isBlank(String.valueOf(err.getMessage()))){
                        if(err.getStatusCode() != null || !String.isBlank(String.valueOf(err.getStatusCode()))){
                            /*
                            newErr.JVCO_FailedRecordObjectReference__c = String.valueOf(acc.ID);
                            newErr.Name = String.valueOf(acc.ID);
                            newErr.blng__ErrorCode__c = String.valueof(err.getStatusCode());
                            newErr.blng__FullErrorLog__c = String.valueof(err.getMessage());
                            newErr.JVCO_ErrorBatchName__c = 'Update Accounts VAT';
                            */

                            newErr.ffps_custRem__Related_Object_Key__c = String.valueOf(acc.ID);
                            newErr.ffps_custRem__Grouping__c = String.valueof(err.getStatusCode());
                            newErr.ffps_custRem__Detail__c = String.valueof(err.getMessage());
                            newErr.ffps_custRem__Message__c = 'Update Accounts VAT';

                        } 
                    }
                    errLogList.add(newErr);
                  }
              }
            }
            else{
                System.debug('Successfully updated.');
            }
          }

          if(!errLogList.isEmpty()){
              insert errLogList; 
          }  
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}