@IsTest
private class JVCO_RecalculateQuotesBatchTest 
{
    @testSetup static void setupTestData() 
    {
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        PriceBook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        opp.CloseDate = System.today() - 5;
        insert opp;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_Closure_Reason__c = '';
        aff.JVCO_End_Date__c = null;
        insert aff;
        
        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
        q.Recalculate__c = false;
        insert q;
        
        SBQQ__Quote__c q2 = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
        q2.Recalculate__c = true;
        insert q2;
        
        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c order by CreatedDate DESC limit 1];

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, prod.Id);
        insert ql;
    }


    private static testMethod void testMethodGenerateRenewalQuotesBatchTrue() 
    {
        Set<Id> quoteIds = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        SBQQ__Quote__c qTrue = [select Id from SBQQ__Quote__c where Recalculate__c = true and SBQQ__Account__c =: a.Id order by CreatedDate DESC limit 1];
        
        quoteIds.add(qTrue.Id);

        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_RecalculateQuotesBatch(quoteIds, 1, true, null), 1);
        Test.stopTest();
    }
    
    private static testMethod void testMethodGenerateRenewalQuotesBatchFalse() 
    {
        Set<Id> quoteIds = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        SBQQ__Quote__c qFalse = [select Id from SBQQ__Quote__c where Recalculate__c = false and SBQQ__Account__c =: a.Id order by CreatedDate DESC limit 1];
        
        quoteIds.add(qFalse.Id);

        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_RecalculateQuotesBatch(quoteIds, 1, true, null), 1);
        Test.stopTest();
    }
    
    private static testMethod void testMethodFromRecalculateClass() 
    {
        Set<Id> quoteIds = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Name from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        SBQQ__Quote__c qFalse = [select Id from SBQQ__Quote__c where Recalculate__c = false and SBQQ__Account__c =: a.Id order by CreatedDate DESC limit 1];
        
        quoteIds.add(qFalse.Id);

        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_RecalculateQuotesBatch(quoteIds, 1, false, a), 1);
        Test.stopTest();
    }
}