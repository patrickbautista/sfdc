/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_CreateRenewalBatch.cls 
   Description:     Class that handles CreateRenewalQuote Schedulable APEX class as per JIRA ticket GREEN-9202
   Test class:    JVCO_CreateRenewalQuoteBatchTest.cls 

   Date                 Version       Author                             Summary of Changes 
   -----------          -------     -----------------                  -------------------------------------------
  20-Dec-2016           0.1           Accenture-Rolando Valencia       Intial draft
  27-Dec-2016           0.2           Accenture-Rolando Valencia         Added comments and method signatures.         
  04-May-2017           0.3           Accenture-Chuck Martin           Changed query 
  12-May-2017           0.4           Accenture-Raus Ablaza            Added constructors for adhoc testing
  08-Aug-2017           0.5          Accenture-Mel Andrei Santos       Added call of next batch and changed DML statement to database.update as per Ticket GREEN-17932
  22-Aug-2017           0.6          Accenture-Mel Andrei Santos       Added try catch method for error handling in inserting error log records
  24-Aug-2017           0.7          mel.andrei.b.santos                 refactored inputting of fields
  12-Sep-2017           0.8          Accenture - r.p.valencia.iii       GREEN-9202 v1.4 added static variable isAutoRenewal
  14-Sep-2017           0.9          mel.andrei.b.Santos                 removed the if conditions as the null check in the error logs as per James
  24-Oct-2017           1.0         Accenture- Mel Andrei Santos        Update the finish method to call the  complete renewal batch to run it after 5 mins as per GREEN-24872
  14-Nov-2017           1.1       Accenture - Mel Andrei  Santos        Updated query  "(EndDate = NEXT_N_DAYS:' +renewalDateNum+ ')" to  "(EndDate <= NEXT_N_DAYS:' +renewalDateNum+ ')" based on GREEN-20698
  27-Dec-2017           1.2         Accenture - r.p.valencia.iii        GREEN-27617 updated query to exclude contracts with JVCO_Do_Not_Renew__c = true
  28-Dec-2017           1.3         Accenture - r.p.valencia.iii        Added isRunningTest to create DML error
  10-Jan-2018           1.4         Accenture - r.p.valencia.iii        Removed the condition JVCO_Do_Not_Renew__c as it is a redundant filter with JVCO_RenewableQuantity__c > 0 and reverted the change of v1.1 as per GREEN-27617
  26-Jan-2018           1.5         Accenture - r.p.valencia.iii        GREEN-28346 Added JVCO_Renewal_Generated__c = FALSE and changed EndDate <= NEXT_N_DAYS
------------------------------------------------------------------------------------------------ */
global class JVCO_CreateRenewalQuoteBatch implements Database.Batchable<sObject>
{
    public Id contId;
    public Set<Id> cIdSet = new Set<Id>();
    //GREEN-9202 v1.4 update
    public static boolean isAutoRenewal = FALSE;

    //  START   raus.k.b.ablaza     04-May-2017     constructors for adhoc testing
    global JVCO_CreateRenewalQuoteBatch(){}
    
    // adhoc testing for single id
    global JVCO_CreateRenewalQuoteBatch(Id cId){
        contId = cId;
    }

    // adhoc testing for multiple ids
    global JVCO_CreateRenewalQuoteBatch(Set<Id> idSet){
        cIdSet = idSet;
    }
    //  END   raus.k.b.ablaza     04-May-2017     

    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        //String renewalDateNum; 
        String query;
               
        Integer renewalDateNum = Integer.valueOf(JVCO_Renewal_Settings__c.getValues('Automatic Quote Generation').days__c);
        
        /*for(JVCO_Renewal_Settings__c ren : JVCO_Renewal_Settings__c.getall().values())
        {
            if(ren.Name == 'Automatic Quote Generation')
            {
                renewalDateNum = String.valueOf(ren.days__c);
            }
        }*/
        
        query =  'SELECT id, SBQQ__RenewalQuoted__c ';
        query += 'FROM Contract ';
        query += 'WHERE (EndDate <= NEXT_N_DAYS:' +renewalDateNum+ ') '; //14-12-2017 mel.santos Updated query  "(EndDate = NEXT_N_DAYS:' +renewalDateNum+ ')" to  "(EndDate <= NEXT_N_DAYS:' +renewalDateNum+ ')" based on GREEN-20698
        //query += 'WHERE (EndDate = NEXT_N_DAYS:' +renewalDateNum+ ') '; //10-01-2018 r.p.valencia.iii reverted the query back to (EndDate = NEXT_N_DAYS:' +renewalDateNum+ ') based on GREEN-27617
        query += 'AND (JVCO_Renewal_Generated__c = FALSE) '; //26-01-2018 r.p.valencia.iii add this condition based on GREEN-28346
        query += 'AND (Account.JVCO_Review_Type__c = \'Automatic\') ';
        //GREEN-22239 - kristoffer.d.martin - 24-Aug-2017 - START
        //query += 'AND (id in (select sbqq__contract__c from sbqq__subscription__c)) ';
        query += 'AND (JVCO_RenewableQuantity__c > 0) '; 
        //GREEN-22239 - kristoffer.d.martin - 24-Aug-2017 - END
        // 04-May-2017 - GREEN-9202 - Kristoffer Chuck Martin - START
        query += 'AND (SBQQ__RenewalQuoted__c = false) ';
        // 04-May-2017 - GREEN-9202 - Kristoffer Chuck Martin - END
        //query += 'AND (JVCO_Do_Not_Renew__c = false) '; //27-12-2017 r.p.valencia.iii updated query to exclude contracts with JVCO_Do_Not_Renew__c = true - GREEN-27617
        //  START   raus.k.b.ablaza     04-May-2017     additional where statement for adhoc testing
        // single id adhoc
        if(!String.isBlank(String.valueOf(contId))){
            query += ' and id = \''+contId+'\' ';
        }

        // multiple ids adhoc
        if(!cIdSet.isEmpty()){
            String strIds = '';
            
            for(Id i : cIdSet){
                strIds += '\'' + i + '\',';
            }

            strIds = strIds.substring(0, strIds.length() - 1);

            query += ' and id in ('+strIds+') ';
        }
        
        //  END   raus.k.b.ablaza     04-May-2017  
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext BC, List<Contract> contractList)
    {
        List<Contract> contactsForUpdate = new List<Contract>();
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>(); //GREEN-17932 - Mel Andrei Santos 08-08-2017

        isAutoRenewal = TRUE; //GREEN-9202 v1.4   
        
        System.debug('@@contract '+contractList);
        for (Contract licenceContract: contractList)
        {
            licenceContract.SBQQ__RenewalQuoted__c = true;
            contactsForUpdate.add(licenceContract);
        }
        System.debug('@@contract2 '+contractList);   
        if (!contactsForUpdate.isEmpty())
        {
            if(Test.isRunningTest()){  //Added isRunningTest to create DML error - r.p.valencia.iii 28-Dec-2017
                contactsForUpdate.add(new Contract());
            }
            //update contactsForUpdate;   start Changed DML statement to database.update as per Ticket GREEN-17932 - Mel Andrei Santos 08-08-2017
            List<Database.SaveResult> res = Database.update(contactsForUpdate,false); // Start 24-08-2017 mel.andrei.b.santos
            //for(Database.SaveResult srConList:conUpdateList)
            for(Integer i = 0; i < contactsForUpdate.size(); i++)
            {
                Database.SaveResult srConList = res[i];
                Contract origrecord = contactsForUpdate[i];
                if(!srConList.isSuccess())
                {
                    System.debug('Update opportunity fail: ');
                    for(Database.Error objErr:srConList.getErrors())
                    {
                        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                        errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(origrecord.ID);
                        //errLog.Name = String.valueOf(origrecord.ID);
                        errLog.ffps_custRem__Grouping__c = String.valueof(objErr.getStatusCode());
                        errLog.ffps_custRem__Detail__c = String.valueof(objErr.getMessage());
                        errLog.ffps_custRem__Message__c = 'Quote Renewal';                                    

                            errLogList.add(errLog);
                    }
                }
            }
            System.debug('@@@Error Log List of quote: ' + errLogList);
            if(!errLogList.isempty())
            {
                System.debug('@@@Error Log List insert of quote?: ' + errLogList);
               // insert errLogList; Start 22-08-2017 mel.andrei.b.santos  Added try catch method for error handling in inserting error log records
               try
               {
                  insert errLogList;
               }
               catch(Exception ex)
               {
                   System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                   
               }
                //end
            }    
        } 
        System.debug('@@contract3 '+contractList);
    }
    
    global void finish(Database.BatchableContext BC)
    {
        JVCO_CompleteRenewalQuoteBatch completeRQBatch = new JVCO_CompleteRenewalQuoteBatch();
        string jobName = 'Run JVCO_CompleteRenewalQuoteBatch ' + Datetime.now();
        String scheduleBatch = System.scheduleBatch(completeRQBatch, jobName, 5 , 1);
    }
    
}