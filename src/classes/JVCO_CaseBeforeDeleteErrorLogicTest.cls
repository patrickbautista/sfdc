@isTest
public class JVCO_CaseBeforeDeleteErrorLogicTest {
    
    @testSetup
    private static void testSetup(){
        
        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;
        
        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;
        
        Entitlement ent = JVCO_TestClassObjectBuilder.createEntitlement(acc2.id);
        insert ent;
        
        JVCO_SLA_Constants__c slacon = JVCO_TestClassObjectBuilder.createSLAConstants();
        insert slacon;
        
        Case cas = JVCO_TestClassObjectBuilder.createCase(ent.id);
        insert cas;    
        
    }
    
    @isTest
    private static void initTest()
    {
        
        Case cases = new Case();
        cases = [SELECT Id FROM Case LIMIT 1];
        
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.JVCO_CaseBeforeDeleteError')); 
        System.currentPageReference().getParameters().put('retURL', 'test');
        ApexPages.StandardController sc = new ApexPages.StandardController(cases);
        JVCO_CaseBeforeDeleteErrorLogic caseBefore = new JVCO_CaseBeforeDeleteErrorLogic(sc);
        caseBefore.init();
        test.stoptest();
    }
    
    @isTest
    private static void returnToCaseTest()
    {
        
        Case cases = new Case();
        cases = [SELECT Id FROM Case LIMIT 1];
        
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.JVCO_CaseBeforeDeleteError')); 
        System.currentPageReference().getParameters().put('id', cases.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(cases);
        JVCO_CaseBeforeDeleteErrorLogic caseBefore = new JVCO_CaseBeforeDeleteErrorLogic(sc);
        PageReference pageRefReturnToCase = caseBefore.returnToCase();
        test.stoptest();
    }
    
    @isTest
    private static void licensingUser()
    {           
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Licensing User'].Id,
            LastName = 'last',
            Email = 'sample000@email.com',
            Username = 'puser000@email.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Division='Legal',
            Department='Legal'
        );
        Case cases = new case();
        cases.Origin = 'Email';
        cases.Type = 'Query';
        cases.Subject = 'Test';
        cases.JVCO_Case_Reason_Level_2__c = 'New Business';
        cases.JVCO_Case_Reason_Detail__c = 'Customer Details';  
        cases.Description = 'test';
        cases.Status = 'New';
        
        List<ApexPages.Message> msgList = ApexPages.getMessages();
        // or loop over the messages
        // 
        Test.startTest();
        System.runAs(u)
        {        
            insert cases;
            Test.setCurrentPageReference(new PageReference('Page.JVCO_CaseBeforeDeleteError')); 
            System.currentPageReference().getParameters().put('retURL', 'test');
            ApexPages.StandardController sc = new ApexPages.StandardController(cases);
            JVCO_CaseBeforeDeleteErrorLogic caseBefore = new JVCO_CaseBeforeDeleteErrorLogic(sc);
            caseBefore.init();
        }
        test.stoptest();
        
        for(ApexPages.Message msg :  ApexPages.getMessages()) 
        {
            System.assertEquals('You cannot delete a Case', msg.getSummary());
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
        }
        
    }
    
}