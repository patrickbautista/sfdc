@isTest
private class JVCO_OverdueInstalment_SchedTest
{
	@testSetup static void setupTestData(){
	
       QueuesObject testQueue ; 
          Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
       System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
            listQueue.add(q2);
            queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
            listQueue.add(q3);
            queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
            listQueue.add(q4);
            queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
            listQueue.add(q5);
            queuesobject q6 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankAccount__c'); 
            listQueue.add(q6);
            queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q7);
            queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatement__c'); 
            listQueue.add(q8);
            //queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntry__c'); 
            //listQueue.add(q9);
            //queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntryLineItem__c'); 
            //listQueue.add(q10);
            //queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatementLineItem__c'); 
            //listQueue.add(q10);
            //queuesobject q12 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatementLineItem__c'); 
            //listQueue.add(q12);           
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }

        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
        testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc.Dimension_1_Required__c = false;
        testGeneralLedgerAcc.Dimension_2_Required__c = false;
        testGeneralLedgerAcc.Dimension_3_Required__c = false;
        testGeneralLedgerAcc.Dimension_4_Required__c = false;
        testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
        testGeneralLedgerAcc.ownerid = testGroup.Id;

        c2g__codaGeneralLedgerAccount__c testGeneralLedger = JVCO_TestClassObjectBuilder.createGeneralLedger();
        insert testGeneralLedger;

        c2g__codaTaxCode__c testTaxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedger.Id);
        insert testTaxCode;

        c2g__codaTaxRate__c testTaxRate = new c2g__codaTaxRate__c();
        testTaxRate.c2g__Rate__c = 20.0;
        testTaxRate.c2g__UnitOfWork__c = 1.0;
        testTaxRate.c2g__TaxCode__c = testTaxCode.id;
        insert testTaxRate;

        insert testGeneralLedgerAcc;
        //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }
        
        //Create Account
        Account testAccCust = new Account(Name='Test Customer Account');
        testAccCust.AccountNumber = '999999';
        testAccCust.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust.Type='Customer';
        testAccCust.RecordTypeId = accountRecordTypes.get('Customer Account');
        testAccCust.c2g__CODAOutputVATCode__c = testTaxCode.id;
        insert testAccCust;

        //Create Contact
        Contact testContact = JVCO_TestClassObjectBuilder.createContact(testAccCust.Id);
        testContact.Firstname = 'Test';
        testContact.LastName = 'TestContact';
        insert testContact;

        Account testAcc = new Account(Name='Test Account');
        testAcc.AccountNumber = '987654';
        testAcc.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAcc.c2g__CODAAllowDeleteInUse__c = false;
        testAcc.c2g__CODACreditLimitEnabled__c = false;
        testAcc.c2g__CODACreditStatus__c = 'Credit Allowed';
        testAcc.c2g__CODADaysOffset1__c = 0.0;
        testAcc.c2g__CODADiscount1__c = 0.0;
        testAcc.c2g__CODAFederallyReportable1099__c = false;
        testAcc.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testAcc.c2g__CODAIntercompanyAccount__c = false;
        testAcc.c2g__CODASalesTaxStatus__c = 'Taxable';
        testAcc.JVCO_Customer_Account__c = testAccCust.id;
        testAcc.RecordTypeId = accountRecordTypes.get('Licence Account');
        testAcc.c2g__CODAOutputVATCode__c = testTaxCode.id;
        testAcc.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        testAcc.JVCO_Billing_Contact__c = testContact.id;
        testAcc.JVCO_Licence_Holder__c = testContact.id;
        testAcc.JVCO_Review_Contact__c = testContact.id;
        insert testAcc;

      /*  Account testAcc2 = new Account(Name='Unidentified');
        testAcc2.AccountNumber = '123456';
        testAcc2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAcc2.c2g__CODAAllowDeleteInUse__c = false;
        testAcc2.c2g__CODACreditLimitEnabled__c = false;
        testAcc2.c2g__CODACreditStatus__c = 'Credit Allowed';
        testAcc2.c2g__CODADaysOffset1__c = 0.0;
        testAcc2.c2g__CODADiscount1__c = 0.0;
        testAcc2.c2g__CODAFederallyReportable1099__c = false;
        testAcc2.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testAcc2.c2g__CODAIntercompanyAccount__c = false;
        testAcc2.c2g__CODASalesTaxStatus__c = 'Taxable';
        testAcc2.JVCO_Customer_Account__c = testAccCust.id;
        testAcc2.RecordTypeId = accountRecordTypes.get('Licence Account');
        insert testAcc2; */

        c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
        testCompany.c2g__CashMatchingCurrencyMode__c = 'Account';
        testCompany.c2g__Country__c = 'GB';
        testCompany.c2g__ECCountryCode__c = 'GB';
        testCompany.c2g__UnitOfWork__c = 1.0;
        testCompany.Name = 'PRS_PPL JV For Music';
        testCompany.ownerid = testGroup.Id;

        insert testCompany;

        c2g__codaAccountingCurrency__c accCurrency = new c2g__codaAccountingCurrency__c();
        accCurrency.c2g__OwnerCompany__c = testCompany.id;
        accCurrency.c2g__DecimalPlaces__c = 2;
        accCurrency.Name = 'GBP';
        accCurrency.c2g__Dual__c = true ;
        accCurrency.ownerid = testGroup.Id;
        accCurrency.c2g__Home__c = true;
        accCurrency.c2g__UnitOfWork__c = 2.0;
        insert accCurrency;

        c2g__codaBankAccount__c testBankAccount = new c2g__codaBankAccount__c();
        testBankAccount.c2g__AccountName__c = 'Test Bank Account';
        testBankAccount.c2g__AccountNumber__c = '987654';
        testBankAccount.c2g__BankName__c = 'Barclays Bank - Current';
        testBankAccount.c2g__GeneralLedgerAccount__c = testGeneralLedgerAcc.id;
        testBankAccount.Name = 'Test Bank Account 1';
        testBankAccount.c2g__OwnerCompany__c = testCompany.Id;
        testBankAccount.c2g__ReportingCode__c = 'BARC-CURR-JV';
        testBankAccount.c2g__BankAccountCurrency__c = accCurrency.id;
        insert testBankAccount;

        testCompany.c2g__BankAccount__c = testBankAccount.id;
        update testCompany;

        c2g__codaYear__c yr= new c2g__codaYear__c();
        yr.Name ='2016';
        yr.c2g__AutomaticPeriodList__c =  true;
        yr.c2g__OwnerCompany__c = testCompany.id;
        yr.c2g__ExternalId__c = 'yzsd1234';
        yr.c2g__NumberOfPeriods__c =11;
        yr.c2g__StartDate__c =  system.today() - 10;
        yr.c2g__Status__c = 'Open';
        yr.c2g__PeriodCalculationBasis__c = '445';
        yr.c2g__YearEndMode__c = 'Full Accounting Code' ; 
        yr.c2g__UnitOfWork__c = 12;
        yr.ownerid = testGroup.Id;
        insert yr;

        c2g__codaPeriod__c testPeriod = new c2g__codaPeriod__c();
        testPeriod.c2g__OwnerCompany__c = testCompany.id;
        testPeriod.c2g__StartDate__c = System.today()-10;
        testPeriod.c2g__EndDate__c = System.today()+10;
        testPeriod.c2g__PeriodNumber__c ='123';
        testPeriod.c2g__Description__c ='test Desc';
        testPeriod.c2g__PeriodGroup__c = 'Q1';
        testPeriod.c2g__PeriodNumber__c = '1';
        testPeriod.c2g__YearName__c = yr.id;
        insert testPeriod;

        c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c();
        userCompany.c2g__Company__c = testCompany.id;
        userCompany.c2g__User__c = userInfo.getUserId();
        userCompany.c2g__ExternalId__c = 'ABCDE1234567876';
        userCompany.c2g__UnitOfWork__c = 111 ;
        insert userCompany;

        //Create Sales Invoice
        c2g__codaInvoice__c testInvoice = new c2g__codaInvoice__c();
        testInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testInvoice.c2g__InvoiceDate__c = date.today().addDays(-7);
        testInvoice.c2g__DueDate__c = date.today().addDays(-7);
        testInvoice.c2g__Account__c = testAcc.Id;
        testInvoice.JVCO_Customer_Type__c = 'New Business';
        testInvoice.c2g__OwnerCompany__c = testCompany.id;
        testInvoice.ownerid = testGroup.Id;
        //testInvoice.Name = 'SIN000244';
        insert testInvoice;

        c2g__codaBankStatement__c bankStatement = new c2g__codaBankStatement__c();
        bankStatement.c2g__BankAccount__c = testBankAccount.id;
        bankStatement.c2g__OwnerCompany__c = testCompany.id;
        bankStatement.c2g__Reference__c = 'test0001';
        bankStatement.c2g__OpeningBalance__c = 10100;
        bankStatement.c2g__StatementDate__c = Date.today();
        insert bankStatement;

        List<JVCO_TransitionManualReviewNotifSettings__c> transManRevNotSettings = new List<JVCO_TransitionManualReviewNotifSettings__c>();
        transManRevNotSettings.add(new JVCO_TransitionManualReviewNotifSettings__c(Name = 'Limit', Limit__c = '3000'));
        insert transManRevNotSettings;
    }

    /*@isTest
    static void createSalesInvoice()
    {
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
                Map<String,String> accountRecordTypes = new Map<String,String>{};
                for(RecordType rt: rtypes)
                {
                  accountRecordTypes.put(rt.Name,rt.Id);
                }

                String RecordType =  accountRecordTypes.get('Licence Account');
                Account testAccount = [select id from Account WHERE RecordTypeId = :RecordType];
		c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c order by CreatedDate DESC limit 1];
                c2g__codaAccountingCurrency__c accCurrency = [select id from c2g__codaAccountingCurrency__c order by CreatedDate DESC limit 1];
                c2g__codaBankAccount__c testBankAccount = [select id from c2g__codaBankAccount__c order by CreatedDate DESC limit 1];
                Group testGroup = [select id from Group order by CreatedDate DESC limit 1];


		//Create Sales Invoice
                c2g__codaInvoice__c testInvoice = new c2g__codaInvoice__c();
                testInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
                testInvoice.c2g__InvoiceDate__c = date.today().addDays(-7);
                testInvoice.c2g__DueDate__c = date.today().addMonths(7);
                testInvoice.c2g__Account__c = testAccount.Id;
                testInvoice.JVCO_Customer_Type__c = 'New Business';
                testInvoice.c2g__OwnerCompany__c = testCompany.id;
                //testInvoice.ownerid = testGroup.Id;

                testInvoice.JVCO_Generate_Payment_Schedule__c = false;
                //testInvoice.JVCO_Number_of_Payments__c = 5.0 ;
                //testInvoice.JVCO_First_Due_Date__c = date.today().addDays(5);
                //testInvoice.Name = 'SIN000244';
                test.startTest();

                insert testInvoice;

                c2g__codaInvoice__c selectedInvoice = new c2g__codaInvoice__c();
                selectedInvoice = [select id from c2g__codaInvoice__c where id = :testInvoice.id order by CreatedDate DESC limit 1];
                system.assertEquals(testInvoice.id, selectedInvoice.id,'Error not equals');

                test.stopTest();

                c2g__codaBankStatement__c bankStatement = new c2g__codaBankStatement__c();
                bankStatement.c2g__BankAccount__c = testBankAccount.id;
                bankStatement.c2g__OwnerCompany__c = testCompany.id;
                bankStatement.c2g__Reference__c = 'test0001';
                bankStatement.c2g__OpeningBalance__c = 10100;
                bankStatement.c2g__StatementDate__c = Date.today();
                insert bankStatement;

                

	}*/

        @isTest
        static void updateSalesInvoiceAndCreate()
        {
                List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
                Map<String,String> accountRecordTypes = new Map<String,String>{};
                for(RecordType rt: rtypes)
                {
                  accountRecordTypes.put(rt.Name,rt.Id);
                }

                String RecordType =  accountRecordTypes.get('Licence Account');
                Account testAccount = [select id from Account WHERE RecordTypeId = :RecordType];
                c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c order by CreatedDate DESC limit 1];
                c2g__codaAccountingCurrency__c accCurrency = [select id from c2g__codaAccountingCurrency__c order by CreatedDate DESC limit 1];
                c2g__codaBankAccount__c testBankAccount = [select id from c2g__codaBankAccount__c order by CreatedDate DESC limit 1];
                Group testGroup = [select id from Group order by CreatedDate DESC limit 1];


                //Create Sales Invoice
                c2g__codaInvoice__c testInvoice = new c2g__codaInvoice__c();
                testInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
                testInvoice.c2g__InvoiceDate__c = date.today().addDays(-7);
                testInvoice.c2g__DueDate__c = date.today().addMonths(7);
                testInvoice.c2g__Account__c = testAccount.Id;
                testInvoice.JVCO_Customer_Type__c = 'New Business';
                testInvoice.c2g__OwnerCompany__c = testCompany.id;
                //testInvoice.ownerid = testGroup.Id;

                testInvoice.JVCO_Generate_Payment_Schedule__c = false;
                //testInvoice.JVCO_Number_of_Payments__c = 5.0 ;
                //testInvoice.JVCO_First_Due_Date__c = date.today().addDays(5);
                //testInvoice.Name = 'SIN000244';
                insert testInvoice;

                testInvoice.JVCO_Generate_Payment_Schedule__c = true;
                testInvoice.JVCO_Number_of_Payments__c = 5.0 ;
                testInvoice.JVCO_First_Due_Date__c = date.today().addDays(5); 
                testInvoice.JVCO_Payment_Method__c = 'Cheque';
                Test.startTest();

                update testInvoice;

                list<c2g__codaInvoiceInstallmentLineItem__c> listSelectedInvoiceLines = new list<c2g__codaInvoiceInstallmentLineItem__c>();
                listSelectedInvoiceLines = [select id, JVCO_Payment_Status__c from c2g__codaInvoiceInstallmentLineItem__c where c2g__codaInvoiceInstallmentLineItem__c.c2g__Invoice__c = :testInvoice.id order by CreatedDate DESC limit 1];
                //system.assertEquals(testInvoice.JVCO_Number_of_Payments__c, listSelectedInvoiceLines.size(), 'Error in Number of Lines Generated');
                //for(c2g__codaInvoiceInstallmentLineItem__c listItem : listSelectedInvoiceLines){
                //	system.assertEquals(null, listItem.JVCO_Payment_Status__c,'Error');
                //}
                Datetime dt = Datetime.now().addMinutes(1);
        		String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        		//JVCO_SurchargeGeneration_Sched.callExecuteMomentarily();
        		System.schedule('JVCO_OverdueInstalment_Sched', CRON_EXP, new JVCO_OverdueInstalment_Sched());

                Test.stopTest();

                c2g__codaBankStatement__c bankStatement = new c2g__codaBankStatement__c();
                bankStatement.c2g__BankAccount__c = testBankAccount.id;
                bankStatement.c2g__OwnerCompany__c = testCompany.id;
                bankStatement.c2g__Reference__c = 'test0001';
                bankStatement.c2g__OpeningBalance__c = 10100;
                bankStatement.c2g__StatementDate__c = Date.today();
                insert bankStatement;

                

        }
}