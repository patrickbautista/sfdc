/* ----------------------------------------------------------------------------------------------
    Name: JVCO_CancelDirectDebitController.cls 
    Description: Business logic class selecting Payment Agreements to be cancelled

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    05-Jan-2017   0.1         ryan.i.r.limlingan  Intial creation
    04-Nov-2019   0.2         franz.g.a.dimaapi   GREEN-35063 - Fixed Installment Issue
----------------------------------------------------------------------------------------------- */
public class JVCO_CancelDirectDebitController
{
    public ApexPages.StandardSetController stdCtrl;
    private List<PAYREC2__Payment_Agreement__c> selectedAgreements;
    public Integer numberOfAgreements {get; set;}
    public List<PAYREC2__Payment_Agreement__c> retrievedAgreements {get; set;}
    public String prompt {get; set;}
    public Boolean isProcessed {get; set;} // Flag to control which button is rendered

    // Constants used for page messages
    public final String NO_RECORDS_ERR = System.Label.JVCO_PaymentCancelAgreementNoRecords;
    public final String INV_RECORDS_ERR = System.Label.JVCO_PaymentCancelAgreementInvalidRecords;
    public final String PAGE_PROMPT = System.Label.JVCO_PaymentCancelAgreementPrompt;
    public final String OPERATION_SUCCESS = System.Label.JVCO_PaymentCancelAgreementSuccess;
    
/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Class constructor
    Inputs: StandardSetController
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    05-Jan-2017 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public JVCO_CancelDirectDebitController(ApexPages.StandardSetController ssc)
    {
        stdCtrl = ssc;
        // Retrieve selected Agreement records from related list
        selectedAgreements = (List<PAYREC2__Payment_Agreement__c>)ssc.getSelected();
        numberOfAgreements = selectedAgreements.size();
    }

 /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Checks if there are selected Agreement records
    Inputs: N/A
    Returns: PageReference
    <Date>      <Authors Name>      <Brief Description of Change> 
    05-Jan-2017 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public PageReference init()
    {
        if(numberOfAgreements > 0)
        {
            retrievedAgreements = [SELECT Id, Name, PAYREC2__Payment_Schedule__c,
                                   PAYREC2__Payment_Schedule__r.Name, PAYREC2__Status__c,
                                   PAYREC2__Description__c, PAYREC2__Total_Collected__c
                                   FROM PAYREC2__Payment_Agreement__c
                                   WHERE Id IN :selectedAgreements];

            for(PAYREC2__Payment_Agreement__c agreement :retrievedAgreements)
            {
                if(agreement.PAYREC2__Status__c.equals('Cancel'))
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, INV_RECORDS_ERR));
                    numberOfAgreements = 0; // To simulate no records selected
                }
            }
            prompt = PAGE_PROMPT;
            isProcessed = FALSE;
        }else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, NO_RECORDS_ERR));
        }
        return null;
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Cancels selected agreements, and deletes related Payment records
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    05-Jan-2017 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public void cancelAgreements()
    {
        for(PAYREC2__Payment_Agreement__c agreement : retrievedAgreements)
        {
            agreement.PAYREC2__Status__c = 'Cancel';
        }
        update retrievedAgreements;
        deletePayments(retrievedAgreements);
        prompt = OPERATION_SUCCESS;
        isProcessed = TRUE;
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Deletes Payment records based on the cancelled agreement
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    05-Jan-2017 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public void deletePayments(List<PAYREC2__Payment_Agreement__c> agreements)
    {
        
        /* ON HOLD: FURTHER INFO REQUIRED
        Set<PAYBASE2__Payment__c> paymentToDeleteSet = new Set<PAYBASE2__Payment__c>(
                                                        [SELECT Id, PAYBASE2__Pay_Date__c,
                                                         PAYCP2__Payment_Description__c
                                                         FROM PAYBASE2__Payment__c
                                                         WHERE PAYBASE2__Status__c = 'Submitted'
                                                         AND PAYREC2__Payment_Agreement__c
                                                         IN :agreements]);

        for (PAYBASE2__Payment__c payment : paymentToDeleteSet)
        {
            // Only retain Payment records submitted within the span of 4 days
            if (payment.PAYBASE2__Pay_Date__c.daysBetween(Date.today()) > 4)
            {
                paymentToDeleteSet.remove(payment);
            } else {
                // Retrieve the Group Invoice to be used to filter Sales Invoice records
                groupInvoiceNameSet.add(payment.PAYCP2__Payment_Description__c);
            }
        }

        if (!paymentToDeleteSet.isEmpty())
        {
            List<PAYBASE2__Payment__c> paymentToDeleteList =
                new List<PAYBASE2__Payment__c>(paymentToDeleteSet);
            delete paymentToDeleteList;
        }*/
        Set<String> groupInvoiceNameSet = new Set<String>();
        for(PAYREC2__Payment_Agreement__c a : agreements)
        {
            if(a.PAYREC2__Description__c != null) // john.patrick.valdez GREEN-32750 Direct Debit set up without GIN payment plan - customer therefore being chased for payment
            {
                groupInvoiceNameSet.add(a.PAYREC2__Description__c);
            } 
        }
        if(!groupInvoiceNameSet.isEmpty())
        {
            updateInvoiceRecords(groupInvoiceNameSet);
            updateInvoiceGroupToRetired(groupInvoiceNameSet);
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Deletes remaining instalment plans
        Inputs: N/A
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        09-Jan-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public void deleteInstalmentPlans(List<c2g__codaInvoice__c> invoiceList)
    {
        Set<c2g__codaInvoiceInstallmentLineItem__c> itemsToDeleteSet =
            new Set<c2g__codaInvoiceInstallmentLineItem__c>(
                [SELECT Id, c2g__DueDate__c FROM c2g__codaInvoiceInstallmentLineItem__c
                 WHERE c2g__Invoice__c IN :invoiceList]);

        for (c2g__codaInvoiceInstallmentLineItem__c item : itemsToDeleteSet)
        {
            System.debug(Date.today().daysBetween(item.c2g__DueDate__c));
            if (Date.today().daysBetween(item.c2g__DueDate__c) < 4)
            {
                // Don't delete payments that have already been collected
                itemsToDeleteSet.remove(item);
            }
        }

        if (!itemsToDeleteSet.isEmpty())
        {
            List<c2g__codaInvoiceInstallmentLineItem__c> itemsToDeleteList =
                new List<c2g__codaInvoiceInstallmentLineItem__c>(itemsToDeleteSet);
            System.debug(itemsToDeleteList);
            delete itemsToDeleteList;
        }
    }*/

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Updates related Sales Invoice records to update Stop Surcharge to FALSE
        Inputs: N/A
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Jan-2017 ryan.i.r.limlingan  Initial version of function
        29-Jun-2018 john.patrick.valdez Withhold surcharge flag wasn't cleared once DD was cancelled
    ----------------------------------------------------------------------------------------------- */
    public void updateInvoiceRecords(Set<String> groupInvoiceNameSet)
    {
        List<c2g__codaInvoice__c> invoiceToUpdateList = [SELECT JVCO_Stop_Surcharge__c,
                                                         JVCO_Invoice_Group__c, 
                                                         JVCO_Exempt_from_Surcharge__c,
                                                         ffps_custRem__Exclude_From_Reminder_Process__c,
                                                         JVCO_No_of_Unpaid_Installment_Line_Items__c                                                         
                                                         FROM c2g__codaInvoice__c
                                                         WHERE JVCO_Invoice_Group__r.Name
                                                         IN :groupInvoiceNameSet];

        for (c2g__codaInvoice__c invoice : invoiceToUpdateList)
        {
            invoice.JVCO_Stop_Surcharge__c = false;
                
            //GREEN 31938 john.patrick.valdez Withhold surcharge flag wasn't cleared once DD was cancelled
            /*if(invoice.JVCO_No_of_Unpaid_Installment_Line_Items__c == 0)
            {
                invoice.JVCO_Exempt_from_Surcharge__c = FALSE;
                invoice.ffps_custRem__Exclude_From_Reminder_Process__c = FALSE;
            }*/
            invoice.JVCO_Invoice_Group__c = null; // Remove relation to Invoice Group
        }

        if(!invoiceToUpdateList.isEmpty())
        {
            update invoiceToUpdateList;
            //deleteInstalmentPlans(invoiceToUpdateList);
        }
    }
    
    
/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Updates related Sales Invoice records to update Stop Surcharge to FALSE
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    06-Jan-2017 ryan.i.r.limlingan  Initial version of function
    06-Mar-2019 patrick.t.bautista  GREEN-34320 - Change functionality instead of deleting invoice
                                    group change status to retired
  ----------------------------------------------------------------------------------------------- */
    public void updateInvoiceGroupToRetired(Set<String> groupInvoiceNameSet)
    {
        List<JVCO_Invoice_Group__c> invGrpToUpdateList = new List<JVCO_Invoice_Group__c>();
        for(JVCO_Invoice_Group__c invGroup: [SELECT Id, JVCO_Status__c
                                             FROM JVCO_Invoice_Group__c
                                             WHERE Name IN :groupInvoiceNameSet
                                             AND JVCO_Status__c != 'Retired'])
        {
            invGroup.JVCO_Status__c = 'Retired';
            invGrpToUpdateList.add(invGroup);
        }
        update invGrpToUpdateList;
    }
    
/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Returns to Account detail page
    Inputs: N/A
    Returns: PageReference
    <Date>      <Authors Name>      <Brief Description of Change> 
    06-Jan-2017 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public PageReference returnToAccount()
    {
        PageReference page = new PageReference(stdCtrl.cancel().getUrl());
        page.setRedirect(TRUE);
        return page;
    }
}