@isTest
public class JVCO_CashTransferLogicTest
{

    @testSetup 
    static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        //JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        
        RenewalMonthLimit__c renewalCustomsetting = new RenewalMonthLimit__c();
        renewalCustomsetting.Amount__c = 3;
        insert renewalCustomsetting;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        List<c2g__codaDimension2__c> dim2List = new List<c2g__codaDimension2__c>();
        c2g__codaDimension2__c pplDim2 = JVCO_TestClassHelper.setDim2('PPL');
        dim2List.add(pplDim2);
        c2g__codaDimension2__c prsDim2 = JVCO_TestClassHelper.setDim2('PRS');
        dim2List.add(prsDim2);
        c2g__codaDimension2__c vplDim2 = JVCO_TestClassHelper.setDim2('VPL');
        dim2List.add(vplDim2);
        insert dim2List;

        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();
        insert dim3;

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;

        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_General_Settings__c GeneralSettings = new JVCO_General_Settings__c();
        GeneralSettings.JVCO_Post_Sales_Credit_Note_Scope__c = 1;
        insert GeneralSettings;
        
        Test.startTest();
        
        JVCO_Invoice_Group__c invoiceGroup = new JVCO_Invoice_Group__c();
        invoiceGroup.JVCO_Total_Amount__c = 12;
        insert invoiceGroup;
        
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        sInv.JVCO_Invoice_Group__c = invoiceGroup.Id;
        insert sInv;
        List<c2g__codaInvoiceLineItem__c> sInvLineList = new List<c2g__codaInvoiceLineItem__c>();
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, pplDim2.Id, p.Id));
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, prsDim2.Id, p.Id));
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, vplDim2.Id, p.Id));
        insert sInvLineList;
        
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        o.JVCO_Sales_Invoice__c = sInv.Id;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        
        Income_Direct_Debit__c incomeDD = new Income_Direct_Debit__c();
        incomeDD.Account__c = licAcc.Id;
        incomeDD.DD_Collection_Day__c = '1';
        incomeDD.DD_Collection_Period__c = 'Monthly';
        incomeDD.DD_Collection_Stretch__c = '1';
        incomeDD.DD_Collection_Type__c = 'Fixed';
        incomeDD.DD_Collection_Reference__c = 'Monthly Subscription';
        incomeDD.DD_Status__c = 'New Instruction';
        incomeDD.DD_Bank_Account_Number__c = '12345678';
        incomeDD.DD_Bank_Sort_Code__c = '123456';
        incomeDD.Number_Of_Payments__c = 1;
        insert incomeDD;
        
        SMP_DirectDebit_GroupInvoice__c ddInvoiceGrp = new SMP_DirectDebit_GroupInvoice__c();
        ddInvoiceGrp.Invoice_Group__c = invoiceGroup.Id;
        ddInvoiceGrp.Income_Direct_Debit__c = incomeDD.Id;
        insert ddInvoiceGrp;     
        
        c2g__codaInvoiceInstallmentLineItem__c invoiceInstalmentLine = new c2g__codaInvoiceInstallmentLineItem__c();
        invoiceInstalmentLine.c2g__Amount__c = 2;
        invoiceInstalmentLine.c2g__DueDate__c = Date.today() + 30;
        invoiceInstalmentLine.JVCO_Paid_Amount__c = 2;
        invoiceInstalmentLine.JVCO_Payment_Status__c = 'Unpaid';
        invoiceInstalmentLine.c2g__Invoice__c = sInv.id;
        insert invoiceInstalmentLine;
        
        c2g__codaCreditNote__c cred = JVCO_TestClassHelper.getCreditNote(licAcc.Id);
        insert cred;
        List<c2g__codaCreditNoteLineItem__c> credLineList = new List<c2g__codaCreditNoteLineItem__c>();
        credLineList.add(JVCO_TestClassHelper.getCreditNoteLine(cred.Id, dim1.id, dim3.Id, taxCode.Id, pplDim2.Id, p.Id, 100));
        insert credLineList;
        
        Order oCred = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        oCred.JVCO_Sales_Credit_Note__c = cred.Id;
        insert oCred;
        OrderItem oiCred = JVCO_TestClassHelper.setOrderItem(oCred.Id, pbe.Id, ql.Id);
        insert oiCred;
        
        Set<Id> sInvIdSet = new Set<Id>();
        Set<Id> creNoteSet = new Set<Id>();
        sInvIdSet.add(sInv.Id);
        creNoteSet.add(cred.Id);
        
        //Run Post Batch
        JVCO_PostSalesInvoiceBatch pSIB = new JVCO_PostSalesInvoiceBatch(sInvIdSet, creNoteSet);
        psIB.stopMatching = true;
        Database.executeBatch(pSIB);
        
        Test.stopTest();
    }
    @isTest
    static void testBAU()
    {
        Set<Id> cashEntryIdSet = new Set<Id>();
        c2g__codaCashEntry__c cashEntry = new c2g__codaCashEntry__c();
        cashEntry.c2g__PaymentMethod__c = 'Direct Debit';
        insert cashEntry;
        cashEntryIdSet.add(cashEntry.Id);

        c2g__codaInvoice__c sInv = [SELECT Id, Name, c2g__Account__c FROM c2g__codaInvoice__c LIMIT 1];
        c2g__codaCashEntryLineItem__c cashEntryLine = new c2g__codaCashEntryLineItem__c();
        cashEntryLine.c2g__Account__c = sInv.c2g__Account__c;
        cashEntryLine.c2g__AccountReference__c = sInv.Name;
        cashEntryLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cashEntryLine.c2g__CashEntryValue__c = 12000;
        cashEntryLine.c2g__CashEntry__c = cashEntry.Id;
        insert cashEntryLine;

        Test.startTest();
        Database.executeBatch(new JVCO_CashEntriesPostingLogicBatch(cashEntryIdSet));  
        Test.stopTest();

    }
    
    @isTest
    static void testNotDDCashEntry()
    {
        Set<Id> cashEntryIdSet = new Set<Id>();
        c2g__codaCashEntry__c cashEntry = new c2g__codaCashEntry__c();
        cashEntry.c2g__PaymentMethod__c = 'Bank Transfer';
        insert cashEntry;
        cashEntryIdSet.add(cashEntry.Id);

        c2g__codaInvoice__c sInv = [SELECT Id, Name, c2g__Account__c FROM c2g__codaInvoice__c LIMIT 1];
        c2g__codaCashEntryLineItem__c cashEntryLine = new c2g__codaCashEntryLineItem__c();
        cashEntryLine.c2g__Account__c = sInv.c2g__Account__c;
        cashEntryLine.c2g__AccountReference__c = sInv.Name;
        cashEntryLine.c2g__AccountPaymentMethod__c = 'Bank Transfer';
        cashEntryLine.c2g__CashEntryValue__c = 2;
        cashEntryLine.c2g__CashEntry__c = cashEntry.Id;
        insert cashEntryLine;

        Test.startTest();
        Database.executeBatch(new JVCO_CashEntriesPostingLogicBatch(cashEntryIdSet));  
        Test.stopTest();
    }

    @isTest
    static void testMigratedTax()
    {
        Set<Id> cashEntryIdSet = new Set<Id>();
        c2g__codaCashEntry__c cashEntry = new c2g__codaCashEntry__c();
        cashEntry.c2g__PaymentMethod__c = 'Direct Debit';
        insert cashEntry;
        cashEntryIdSet.add(cashEntry.Id);

        c2g__codaInvoice__c sInv = [SELECT Id, Name, c2g__Account__c FROM c2g__codaInvoice__c LIMIT 1];
        c2g__codaCashEntryLineItem__c cashEntryLine = new c2g__codaCashEntryLineItem__c();
        cashEntryLine.c2g__Account__c = sInv.c2g__Account__c;
        cashEntryLine.c2g__AccountReference__c = sInv.Name;
        cashEntryLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cashEntryLine.c2g__CashEntryValue__c = 12000;
        cashEntryLine.c2g__CashEntry__c = cashEntry.Id;
        insert cashEntryLine;

        c2g__codaGeneralLedgerAccount__c gla = [SELECT Id, Name FROM c2g__codaGeneralLedgerAccount__c
                                                WHERE Name = '10390 - VAT Output - Licensing'
                                                LIMIT 1];
        gla.name = '10390 - Tax Conversion';
        update gla;
        
        Test.startTest();
        Database.executeBatch(new JVCO_CashEntriesPostingLogicBatch(cashEntryIdSet));  
        Test.stopTest();

    }

    @isTest
    static void testMigratedCash()
    {
        Set<Id> cashEntryIdSet = new Set<Id>();
        c2g__codaCashEntry__c cashEntry = new c2g__codaCashEntry__c();
        cashEntry.c2g__PaymentMethod__c = 'Direct Debit';
        insert cashEntry;
        cashEntryIdSet.add(cashEntry.Id);

        c2g__codaInvoice__c sInv = [SELECT Id, Name, c2g__Account__c FROM c2g__codaInvoice__c LIMIT 1];
        c2g__codaCashEntryLineItem__c cashEntryLine = new c2g__codaCashEntryLineItem__c();
        cashEntryLine.c2g__Account__c = sInv.c2g__Account__c;
        cashEntryLine.c2g__AccountReference__c = sInv.Name;
        cashEntryLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cashEntryLine.c2g__CashEntryValue__c = 12000;
        cashEntryLine.c2g__CashEntry__c = cashEntry.Id;
        insert cashEntryLine;

        c2g__codaGeneralLedgerAccount__c gla = [SELECT Id, Name FROM c2g__codaGeneralLedgerAccount__c
                                                WHERE Name = '10100 - Bank Cash'
                                                LIMIT 1];
        gla.Name = '10130 - Bank Conversion';                                        
        update gla;

        Test.startTest();
        Database.executeBatch(new JVCO_CashEntriesPostingLogicBatch(cashEntryIdSet));  
        Test.stopTest();

    }
    
    @isTest
    static void testCashToCreditNoteMatching(){
        
        Set<Id> cashEntryIdSet = new Set<Id>();
        c2g__codaCashEntry__c cashEntry = new c2g__codaCashEntry__c();
        cashEntry.c2g__PaymentMethod__c = 'Bank Transfer';
        cashEntry.c2g__Type__c = 'Refund';
        insert cashEntry;
        cashEntryIdSet.add(cashEntry.Id);
        
        c2g__codaCreditNote__c cNote = [SELECT Id, Name, c2g__Account__c FROM c2g__codaCreditNote__c LIMIT 1];
        c2g__codaCashEntryLineItem__c cashEntryLine = new c2g__codaCashEntryLineItem__c();
        cashEntryLine.c2g__Account__c = cNote.c2g__Account__c;
        //cashEntryLine.c2g__AccountReference__c = cNote.Name;
        cashEntryLine.c2g__AccountPaymentMethod__c = 'Bank Transfer';
        cashEntryLine.c2g__CashEntryValue__c = 12000;
        cashEntryLine.c2g__CashEntry__c = cashEntry.Id;
        insert cashEntryLine;
        
        Test.startTest();
        
        cNote.JVCO_Reference_Document__c = [SELECT Name FROM c2g__codaCashEntryLineItem__c WHERE Id = : cashEntryLine.Id LIMIT 1].Name;
        update cNote;
        
        c2g.CODAAPICommon_7_0.Context contextObj = new c2g.CODAAPICommon_7_0.Context();
        c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
        ref.Id = cashEntry.Id;
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        c2g.CODAAPICashEntry_7_0.PostCashEntry(contextObj, ref);
        
        Database.executeBatch(new JVCO_CreditNoteBackgroundMatchingBatch());
        
        Test.stopTest();
    }
    
    @isTest
    static void invoiceToCreditNoteMatching(){
        
        c2g__codaInvoice__c sInv = [SELECT Id FROM c2g__codaInvoice__c LIMIT 1];
        c2g__codaCreditNote__c cNote = [SELECT Id, Name, c2g__Account__c FROM c2g__codaCreditNote__c LIMIT 1];
        
        sInv.JVCO_Reference_Document__c = cNote.Name;
        
        Test.startTest();
        
        update sInv;
        Database.executeBatch(new JVCO_SalesInvoiceBackgroundMatchingBatch());
        
        Test.stopTest();
    }
        
    @isTest
    static void testUpdateInvoiceLineQueueable()
    {
        Map<Id, Order> bInvMap = new Map<Id, Order>([SELECT Id FROM Order]);
        Map<Id, OrderItem> bInvLineMap = new Map<Id, OrderItem>();
        System.enqueueJob(new JVCO_UpdateInvoiceQueueable(bInvMap, bInvLineMap));
        bInvLineMap = new Map<Id, OrderItem>([SELECT Id FROM OrderItem]);
        system.debug(bInvLineMap.size());
        System.enqueueJob(new JVCO_UpdateInvoiceLineQueueable(bInvLineMap));
        JVCO_UpdateInvoiceQueueable uiq = new JVCO_UpdateInvoiceQueueable(null, null, null);
        JVCO_UpdateInvoiceLineQueueable uilq = new JVCO_UpdateInvoiceLineQueueable(null, null);
    }
    
    @isTest
    static void testJVCO_UpdateTCPfieldBatch()
    {
        List<c2g__codaMatchingReference__c> matchingRefList = new List<c2g__codaMatchingReference__c>();
        matchingRefList.add(new c2g__codaMatchingReference__c(c2g__Operation__c = 'Payments'));
        
        JVCO_UpdateTCPfieldBatch updtTCPbatch = new JVCO_UpdateTCPfieldBatch();
        set<id> cashMatchSet = new set<id>();
        cashMatchSet.add(matchingRefList[0].id);
        updtTCPbatch.execute(null, matchingRefList);
        Database.executeBatch(new JVCO_UpdateTCPfieldBatch(cashMatchSet),1);
    }
}