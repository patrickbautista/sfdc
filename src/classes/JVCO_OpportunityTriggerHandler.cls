/* ----------------------------------------------------------------------------------------------
  Name: JVCO_OpportunityTriggerHandler
  Description: Logic for Updating fields on Contract after Opportunity Update
 
  Date         Version     Author               Summary of Changes
  -----------  -------     -----------------    -----------------------------------------
  23-May-2017   0.1         Rhys Dela Cruz       Initial creation
  25-Jul-2017   0.2         Mel Andrei Santos    Added Logic to change Event Invoice field from event based on the Opportunity Invoice
  13-Sep-2017   0.3         Mel Andrei Santos    Updated updateEventInvoicedBasedonOpp, changed eventlist to map to prevent duplicate events
  18-Oct-2017   0.4         Raus Kenneth Ablaza  Refactored JVCO_Populate_Opportunity_Type_Field_2 GREEN-24928
  11-Nov-2017   0.5         Rhys Dela Cruz       Change status of Quote if Opp is Invoiced. GREEN-23355
  01-Feb-2018   0.6         Rolando Valencia     Added method setPrimaryQuote. GREEN-29698
  20-Feb-2018   0.7         reymark.j.l.arlos    GREEN-30099 Created new method to make sure invoiced is false upon creation of opportunity     
  11-Apr-2018   0.9         mel.andrei.b.Santos   GREEN-31233 - Remove the method call to setPrimaryQuote on onBeforeUpdate in the Opp trigger 
  23-Apr-2018   1.0         reymark.j.l.arlos    GREEN-31213 set invoiced false on the event if invoiced is set to false on opportunity
  14-Sep-2018   1.1         reymark.j.l.arlos    GREEN-GREEN-33458 Added conditions to set renewal generated to true or false
  19-Sept-2018  1.2         rhys.j.c.dela.cruz   GREEN-33476 - Adjusted method for Partner Users so Complete and Invoiced will not be overwritten
  27-Aug-2019   1.3         mel.andrei.b.santos   GREEN-34776 - Added method to update Opportunity if created from Auto Renewal Batch
  ----------------------------------------------------------------------------------------------- */

public class JVCO_OpportunityTriggerHandler {

    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_OpportunityTrigger__c : false;

    public static Map<Id, Contract> cMap = null;
    public static Map<Id, Account> accMap = null;

    public static void onBeforeInsert(List<Opportunity> oppList) {


        if (!skipTrigger)
        {
            system.debug('***JVCO_OpportunityTriggerHandler START onBeforeInsert***');
            populateOpportunityType(oppList);
            resetInvoicedFlag(oppList);
            system.debug('***getCpuTime()*** populateOpportunityType: ' + Limits.getCpuTime());        
            system.debug('***getCpuTime()*** populateOpportunity: ' + Limits.getCpuTime());
            system.debug('***JVCO_OpportunityTriggerHandler START onBeforeInsert***');
            assignIsRenewaltoAutoRenewed(oppList);
        }
        for (Opportunity o : oppList)
        {
          o.JVCO_Sales_Invoice_Lookup__c=o.SBQQ__PrimaryQuote__r.JVCO_Sales_Invoice__c;
        }
    }

    public static void onAfterInsert(List<Opportunity> oppList) {
        if (!skipTrigger)
        {
            system.debug('***JVCO_OpportunityTriggerHandler START onAfterInsert***');
            system.debug('***getCpuTime()*** updateContractRenewalGenerated: ' + Limits.getCpuTime());
            system.debug('***JVCO_OpportunityTriggerHandler START onAfterInsert***');
        }
    }

    public static void onBeforeUpdate(List<Opportunity> oppList, Map<Id, Opportunity> oldOppMap) {
        if (!skipTrigger)
        {
            system.debug(logginglevel.ERROR, '### JVCO_OpportunityTriggerHandler onBeforeUpdate START CPU Time: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime()); //jason.e.mactal added to monitor CPU Time   
            populateOpportunityType(oppList);
            //mel.andrei.b.santos GREEN-31233 11-04-2018
            if(Test.isRunningTest())
            {
                setPrimaryQuote(oppList);
            }

            system.debug(logginglevel.ERROR, '### JVCO_OpportunityTriggerHandler onBeforeUpdate START CPU Time: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime()); //jason.e.mactal added to monitor CPU Time  
            validateThatOnlyOneOrderInOpp(oppList,oldOppMap);
        }
        for (Opportunity o : oppList)
        {
          o.JVCO_Sales_Invoice_Lookup__c=o.SBQQ__PrimaryQuote__r.JVCO_Sales_Invoice__c;
        }
    }

    private static Id internalPricebookId = null;
    public static Id StandardPriceBookId {
        get {
            if (internalPricebookId == null)
            {
                internalPricebookId = [SELECT id FROM Pricebook2 WHERE name = 'Standard Price Book' LIMIT 1].Id;
            }

            return internalPricebookId;

        }
    }
    
  public static void onAfterUpdate(List<Opportunity> oppList, Map<Id, Opportunity> oldOppMap) {
    if (!skipTrigger)
    {
      system.debug('***FFUTIL***' + JVCO_FFUtil.stopOpportunityHandlerAfterUpdate);
        system.debug(logginglevel.ERROR, '### JVCO_OpportunityTriggerHandler onAfterUpdate START CPU Time: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime()); //jason.e.mactal added to monitor CPU Time   
        updateContractRenewalGenerated(opplist, oldOppMap);
        updateEventInvoicedBasedonOpp(opplist, oldOppMap); //mel.andrei.b.santos 25/07/2017 added line to call the method
        updateQuoteIfOppInvoiced(oppList, oldOppMap);
        system.debug(logginglevel.ERROR, '### JVCO_OpportunityTriggerHandler onAfterUpdate START CPU Time: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime()); //jason.e.mactal added to monitor CPU Time   
      }
    }

    /* ----------------------------------------------------------------------------------------------------------
      Author:         reymark.j.l.arlos
      Company:        Accenture
      Description:    Updates Invoiced to false
      Inputs:         Opportunity
      Returns:        n/a
      <Date>          <Authors Name>                      <Brief Description of Change>
      20-Feb-2018     reymark.j.l.arlos                   Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public static void resetInvoicedFlag(List<Opportunity> oppList)
    {
      
      for(Opportunity oppRec : oppList)
      {
        oppRec.JVCO_Invoiced__c = false;
      }
      
    }

    /* ----------------------------------------------------------------------------------------------------------
      Author:         rhys.j.c.dela.cruz
      Company:        Accenture
      Description:    Retrieves Opportunities that have Renewal and Contracted = True and updates related Contract
      Inputs:         Opportunity
      Returns:        n/a
      <Date>          <Authors Name>                      <Brief Description of Change>
      24-May-2017     rhys.j.c.dela.cruz                   Initial version of the code
      24-May-2017     mark.j.doggett                       Optimised data Query
      14-Sep-2018     reymark.j.l.arlos                    Added conditions to set renewal generated to true or false - GREEN-33458
      ----------------------------------------------------------------------------------------------------------*/
    public static void updateContractRenewalGenerated(List<Opportunity> oppList, Map<Id, Opportunity> oldOppMap) {

        Map<Id, Boolean> renewalGeneratedMap = new Map<Id, Boolean>();
        System.debug('@@@oppList: ' + oppList);

        if (cMap == null)
        {
            cMap = new Map<Id, Contract> ();
        }

        for (Opportunity opp : oppList) {
            //start 09-14-2018 reymark.j.l.arlos added conditions to set renewal generated to true or false based on the update made in opportunity
            if(opp.SBQQ__Renewal__c == true)
            {
                if(opp.SBQQ__Contracted__c && oldOppMap.get(opp.Id).SBQQ__Contracted__c == false)
                {
                    if(!renewalGeneratedMap.containsKey(opp.Id))
                    {
                        renewalGeneratedMap.put(opp.Id, true);
                    }
                }

                if(opp.SBQQ__Contracted__c == false && oldOppMap.get(opp.Id).SBQQ__Contracted__c == true)
                {
                    if(!renewalGeneratedMap.containsKey(opp.Id))
                    {
                        renewalGeneratedMap.put(opp.Id, false);
                    }
                }
            }
            //end 09-14-2018 reymark.j.l.arlos
        }
        
        //start 09-14-2018 reymark.j.l.arlos added conditions to set renewal generated to true or false based on the update made in opportunity
        if(!renewalGeneratedMap.isEmpty())
        {
            List<Contract> contractList = [SELECT Id, JVCO_Renewal_Generated__c, SBQQ__RenewalOpportunity__c
                                                                        FROM Contract
                                                                        WHERE SBQQ__RenewalOpportunity__c IN: renewalGeneratedMap.keySet()];

            if(!contractList.isEmpty())
            {
                for(Contract contRec : contractList)
                {
                    contRec.JVCO_Renewal_Generated__c = renewalGeneratedMap.get(contRec.SBQQ__RenewalOpportunity__c);
                }

                update contractList;
            }
        }
        //end 09-14-2018 reymark.j.l.arlos
    }

    /* ----------------------------------------------------------------------------------------------------------
      Author:         mel.andrei.b.santos
      Company:        Accenture
      Description:    Retrieves Opportunities that have Quotes and quote lines that looked up an event
     
      <Date>          <Authors Name>                      <Brief Description of Change>
      25-Jul-2017     mel.andrei.b.santos                 Initial version of the code
      13-Sep-2017     mel.andrei.b.Santos                 Changed eventlist to map to prevent duplicate events
      04-May-2018     reymark.j.l.arlos                   GREEN-31213 set invoiced false on the event if invoiced is set to false on opportunity
      ----------------------------------------------------------------------------------------------------------*/
    public static void updateEventInvoicedBasedonOpp(List<Opportunity> oppList, Map<Id, Opportunity> oldOppMap)
    {
        Map<Id, SBQQ__QuoteLine__c> qLMap = new Map<Id, SBQQ__QuoteLine__c> ();
        Map<Id, JVCO_Event__c> eventMap = new Map<Id, JVCO_Event__c> ();
        //List<JVCO_Event__c> eventList = new List<JVCO_Event__c>(); 13-Sep-2017 mel.andrei.b.santos
        Map<Id, JVCO_Event__c> eventToUpdate = new Map<Id, JVCO_Event__c> ();
        List<SBQQ__QuoteLine__c> qLList = new List<SBQQ__QuoteLine__c> ();
        //start 23-Apr-2018 reymark.j.l.arlos GREEN-31213
        List<SBQQ__QuoteLine__c> qLListFalse = new List<SBQQ__QuoteLine__c>();
        Map<Id, JVCO_Event__c> eventMapFalse = new Map<Id, JVCO_Event__c> ();
        //end 23-Apr-2018 reymark.j.l.arlos
        Set<Id> qIdSet = new Set<Id> ();
        Set<Id> eIdSet = new Set<Id> ();
        //start 23-Apr-2018 reymark.j.l.arlos GREEN-31213
        Set<Id> qIdSetFalse = new Set<Id>();
        Set<Id> eIdSetFalse = new Set<Id>();
        //end 23-Apr-2018 reymark.j.l.arlos
        System.debug('@@@ enter updateEventInvoicedBasedonOpp ');

        for (Opportunity opp : oppList)
        {
            Opportunity oldOpp = oldOppMap.get(opp.Id);
            if (opp.JVCO_Invoiced__c == true && opp.SBQQ__PrimaryQuote__c != null && oldOpp.JVCO_Invoiced__c == false)
            {
                qIdSet.add(opp.SBQQ__PrimaryQuote__c);
            }

            //start 23-Apr-2018 reymark.j.l.arlos GREEN-31213
            if(opp.JVCO_Invoiced__c == false && opp.SBQQ__PrimaryQuote__c != null && oldOpp.JVCO_Invoiced__c == true)
            {
                qIdSetFalse.add(opp.SBQQ__PrimaryQuote__c);
            }
            //end 23-Apr-2018 reymark.j.l.arlos
        }

        System.debug('@@@qIdSet: ' + qIdSet);
        if (!qIdSet.isEmpty())
        {
            qLList = new List<SBQQ__QuoteLine__c> ([Select Id, JVCO_Event__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c IN :qIdSet]);
            for (SBQQ__QuoteLine__c qL : qLList)
            {
                if (qL.JVCO_Event__c != null)
                {
                    if (!eIdSet.contains(qL.JVCO_Event__c))
                    {
                        eIdSet.add(qL.JVCO_Event__c);
                    }
                }
            }

            System.debug('@@@eIdSet: ' + eIdSet);
            if (!eIdSet.isEmpty())
            {
                eventMap = new Map<Id, JVCO_Event__c> ([Select Id, JVCO_Event_Invoiced__c FROM JVCO_Event__c WHERE Id IN :eIdSet]);
                System.debug('@@@eventMap: ' + eventMap);
                system.debug('###qLlist ' + qLList);
                for (SBQQ__QuoteLine__c qL : qLList)
                {
                    JVCO_Event__c event = new JVCO_Event__c();
                    if (eventMap.containskey(qL.JVCO_Event__c))
                    {
                        event = eventMap.get(qL.JVCO_Event__c);
                        event.JVCO_Event_Invoiced__c = true;
                        if (!eventToUpdate.containsKey(event.id))
                        {
                            eventToUpdate.put(event.id, event);
                        }

                    }
                }
                system.debug('###eventToUpdate ' + eventToUpdate);
            }
        }

        //start 23-Apr-2018 reymark.j.l.arlos GREEN-31213
        if(!qIdSetFalse.isEmpty())
        {
            qLListFalse = [Select Id , JVCO_Event__c From SBQQ__QuoteLine__c Where SBQQ__Quote__c in: qIdSetFalse];
            if(!qLListFalse.isEmpty())
            {
                for(SBQQ__QuoteLine__c qLRec : qLListFalse)
                {
                    if(qLRec.JVCO_Event__c != null)
                    {
                        if(!eIdSetFalse.contains(qLRec.JVCO_Event__c))
                        {
                            eIdSetFalse.add(qLRec.JVCO_Event__c);
                        }
                    }
                }

                if(!eIdSetFalse.isEmpty())
                {
                    eventMapFalse = new Map<Id, JVCO_Event__c> ([Select Id, JVCO_Event_Invoiced__c FROM JVCO_Event__c WHERE Id IN :eIdSetFalse]);

                    if(!eventMapFalse.isEmpty())
                    {
                        for(SBQQ__QuoteLine__c qLRec : qLListFalse)
                        {
                            JVCO_Event__c evntTemp = new JVCO_Event__c();
                            if(eventMapFalse.containsKey(qLRec.JVCO_Event__c))
                            {
                                evntTemp = eventMapFalse.get(qLRec.JVCO_Event__c);
                                evntTemp.JVCO_Event_Invoiced__c = false;
                                if(!eventToUpdate.containsKey(evntTemp.Id))
                                {
                                    eventToUpdate.put(evntTemp.Id, evntTemp);
                                }
                            }
                        }
                    }
                }
            }
        }

        if(!eventToUpdate.isEmpty())
        {
            update eventToUpdate.values();
        }
        //end 23-Apr-2018 reymark.j.l.arlos
    }

    /* ----------------------------------------------------------------------------------------------------------
      Author:         raus.k.b.ablaza
      Company:        Accenture
      Description:    Populates oppty type based on Dunning Letter Language if it is blank; refactored PB GREEN-24928
      Inputs:         List of Opportunity
      Returns:        n/a
      <Date>          <Authors Name>                      <Brief Description of Change>
      18-Oct-2017     raus.k.b.ablaza                     Initial version of the code 
      ----------------------------------------------------------------------------------------------------------*/
    public static void populateOpportunityType(List<Opportunity> oppList) {
        Set<Id> accIdSet = new Set<Id> ();

        if (accMap == null)
        {
            accMap = new Map<Id, Account> ();
        }

        for (Opportunity opp : oppList) {
            //25044
            if (opp.AccountId != null)
            {
                if (!accMap.containsKey(opp.AccountId))
                {
                    if(String.isBlank(opp.Type)){
                        accIdSet.add(opp.AccountId);    
                    }
                }
            }
        }

        if (!accIdSet.isEmpty()) {
            Map<Id, Account> tempMap = new Map<Id, Account> ([select Id, JVCO_Dunning_Letter_Language__c from Account where Id in :accIdSet]);
            accMap.putAll(tempMap);
        }

        if(!accMap.isEmpty()){
            for (Opportunity opp : oppList) {
                if(accMap.containsKey(opp.AccountId)){
                    opp.Type = accMap.get(opp.AccountId).JVCO_Dunning_Letter_Language__c;
                }
                
            }
        }
            
    }


    /* ----------------------------------------------------------------------------------------------------------
      Author:         rhys.j.c.dela.cruz
      Company:        Accenture
      Description:    Update Primary Quote's Status if Opportunity is invoiced.
      Inputs:         List of Opportunity
      Returns:        n/a
      <Date>          <Authors Name>                      <Brief Description of Change>
      11-Nov-2017     rhys.j.c.dela.cruz                  Initial version of the code 
      ----------------------------------------------------------------------------------------------------------*/
    public static void updateQuoteIfOppInvoiced(List<Opportunity> oppList, Map<Id, Opportunity> oldOppsMap) {

        Set<Id> quoteIds = new Set<Id> ();
        List<SBQQ__Quote__c> quoteList = new List<SBQQ__Quote__c> ();
        List<SBQQ__Quote__c> quotesToUpdate = new List<SBQQ__Quote__c> ();

        //GREEN-33476
        Id profileId = userinfo.getProfileId();
        String profileName = [SELECT Id, Name FROM Profile WHERE Id =: profileId].Name;

        for (Opportunity opp : oppList)
        {
            Opportunity oldOpp = oldOppsMap.get(opp.Id);

            if (opp.JVCO_Invoiced__c == true && opp.SBQQ__PrimaryQuote__c != null && oldOpp.JVCO_Invoiced__c == false)
            {
                quoteIds.add(opp.SBQQ__PrimaryQuote__c);
            }
        }

        if (!quoteIds.isEmpty()) {
            quoteList = new List<SBQQ__Quote__c> ([SELECT id, SBQQ__Status__c FROM SBQQ__Quote__c WHERE id IN :quoteIds]);

            for (SBQQ__Quote__c quotes : quoteList) {
                quotes.SBQQ__Status__c = 'Complete and Invoiced';
                //GREEN-33476
                if(profileName == 'JVCO Partner Community User'){
                    quotes.JVCO_QuoteComplete__c = true;
                }
                quotesToUpdate.add(quotes);
            }

            update quotesToUpdate;
        }
    }

  /* ----------------------------------------------------------------------------------------------------------
    Author:         r.p.valencia.iii
    Company:        Accenture
    Description:    Set the Primary quote of opportunity
    Inputs:         List of Opportunity, Map of old Opportunity
    Returns:        n/a
    <Date>          <Authors Name>               <Brief Description of Change>
    01-Feb-2018     r.p.valencia.iii             Initial version of the code 
    11-Apr-2018     mel.andrei.b.Santos           GREEN-31233 - Remove the method call to setPrimaryQuote on onBeforeUpdate in the Opp trigger and added Update DML
    ----------------------------------------------------------------------------------------------------------*/
  public static void setPrimaryQuote(List<Opportunity> oppList)
  {
    List<SBQQ__Quote__c> quoteList = new List<SBQQ__Quote__c>();
    Set<Id> oppIds = new Set<Id>();
    Map<Id, Id> quoteMap = new Map<Id, Id>();
    
    for(Opportunity opp: oppList)
    {
      if(string.isblank(opp.SBQQ__PrimaryQuote__c))
      {
        oppIds.add(opp.Id);
      }
    }

    if(!oppIds.isEmpty())
    {
      quoteList = [SELECT id, SBQQ__Opportunity2__c FROM SBQQ__Quote__c where SBQQ__Primary__c = TRUE AND SBQQ__Opportunity2__c IN :oppIds];

      if(!quoteList.isEmpty())
      {
        for(SBQQ__Quote__c qRec : quoteList)
        {
          quoteMap.put(qRec.SBQQ__Opportunity2__c, qRec.Id);
        }

        for(Opportunity opp: oppList)
        {
          if(quoteMap.containskey(opp.Id))
          {
            opp.SBQQ__PrimaryQuote__c = quoteMap.get(opp.Id);
          }
        }
        // START mel.andrei.b.santos 11-04-2018 GREEN-31233
        try
        {
          update oppList;
        }
        catch(Exception objErr)
        {
          String ErrorLineNumber = objErr.getLineNumber() != Null ? String.ValueOf(objErr.getLineNumber()):'None';
          String ErrorMessage = 'Cause: ' + objErr.getCause() + ' ErrMsg: ' + objErr.getMessage();
          system.debug('@@@ ' + ErrorLineNumber + ' ' + ErrorMessage);
        }
        // END
      }
    }
  }

  /* ----------------------------------------------------------------------------------------------------------
        Author:         robert.j.b.lacatan
        Company:        Accenture
        Description:    Ensure that only one order is associated to opportunity
        Inputs:         List<Opportunity> and old map of opportunity
        Returns:        n/a
        <Date>          <Authors Name>                      <Brief Description of Change>
        16-May-2019     robert.j.b.lacatan@accenture.com     Initial version of the code 
    ----------------------------------------------------------------------------------------------------------*/
    public static void validateThatOnlyOneOrderInOpp(List<Opportunity> oppList, Map<Id, Opportunity> oldOppMap){

        set<Id> oppToOrderSet = new set<Id>();
        Map<id,List<id>> oppOrderMap = new Map<id,List<id>>();

        for(Opportunity opp: oppList){
            if(opp.SBQQ__Ordered__c && !oldOppMap.get(opp.id).SBQQ__Ordered__c){
                oppToOrderSet.add(opp.id);
            }
        }

        if(!oppToOrderSet.isEmpty()){
            for(Order orderRec: [SELECT id, Opportunity.id from Order where OpportunityId IN: oppToOrderSet]){
                if(oppOrderMap.containsKey(orderRec.Opportunity.id)){
                    List<Id> orderIdList = new List<Id>(oppOrderMap.get(orderRec.Opportunity.id));
                    orderIdList.add(orderRec.id);
                    oppOrderMap.put(orderRec.Opportunity.id,orderIdList);
                }else{
                  oppOrderMap.put(orderRec.Opportunity.id, new List<id>{orderRec.id});
                }
            }

            for(Opportunity opp: oppList){
                if(oppOrderMap.containsKey(opp.id)){
                   opp.addError( 'There is already an order associated to ' + opp.id+'.');
                }
            }
        }

    }

    /* ----------------------------------------------------------------------------------------------------------
    Author:         mel.andrei.b.santos
    Company:        Accenture
    Description:    Places the value of isRenewal static variable to JVCO_Auto_Renewed__c ( GREEN-34776)
    Inputs:         List of Opportunities
    Returns:        n/a
    <Date>          <Authors Name>                      <Brief Description of Change> 
    27-Aug-2019     mel.andrei.b.santos                    Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public static void assignIsRenewaltoAutoRenewed(List<Opportunity> oppList)
    {
        
        for(Opportunity o : oppList)
        {
            o.JVCO_Auto_Renewed__c = JVCO_CreateRenewalQuoteBatch.isAutoRenewal;
        }
        
    }

}