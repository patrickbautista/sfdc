/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CampaignMemberHandler.cls 
   Description: Handler class for CampaignMember object

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  02-Sep-2016   0.1         ryan.i.r.limlingan  Intial creation
  19-Dec-2017	0.2		    chun.yin.tang		Add disabling trigger to Blng Invoice Handler
  ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_CampaignMemberHandler
{
   	public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_CampaignMemberTrigger__c : false;   
    /* ------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: This method handles afterInsert trigger events for CampaignMember
    Input: List<CampaignMember>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    02-Sep-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public static void afterInsert(List<CampaignMember> members)
    {
        if(!skipTrigger) {
	        JVCO_CampaignMemberReassignToQueueLogic.reassignMembers(members);
        }
    }
}