/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CreditCardRedirectLogic.cls 
   Description: Business logic class selecting invoices to be paid using Credit Card

   Date         Version     Author              Summary of Changes
   -----------  -------     -----------------   -----------------------------------------
  12-Dec-2016   0.1         ryan.i.r.limlingan  Intial creation
  09-Jan-2016   0.2         ryan.i.r.limlingan  Refactored code to call JVCO_PaymentSharedLogic
  07-Mar-2016   0.3         ryan.i.r.limlingan  Added Payment Method when creating an Invoice Group
  29-Nov-2018   0.5         patrick.t.bautista  GREEN-34038 - Refactor class and add the Error Message
  ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_CreditCardRedirectLogic
{
    /*private ApexPages.StandardSetController stdCtrl;
    private List<c2g__codaInvoice__c> selectedInvoices;
    public Integer numOfSelectedInv {get; set;}
    public List<c2g__codaInvoice__c> retrievedInvoices {get; set;}
    private Id accId;
    private JVCO_PaymentSharedLogic paymentLogic;

    // Constants used for page messages
    public final String NO_RECORDS_ERR = System.Label.JVCO_PaymentNoRecords;
    public final String INVALID_RECORDS_ERR = System.Label.JVCO_PaymentCCInvalidRecords;
    public final String INVALID_CC_ERROR = System.Label.JVCO_CreditCardnfringementErrorMess;
    public final String INVALID_CC_INVALID_RECORDS = System.Label.JVCO_PaymentCCButtonError;

    /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Class constructor
    Inputs: StandardSetController
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    12-Dec-2016 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public JVCO_CreditCardRedirectLogic(ApexPages.StandardSetController ssc)
    {
        stdCtrl = ssc;
        
        // Retrieve selected Sales Invoice records from related list
        selectedInvoices = (List<c2g__codaInvoice__c>)ssc.getSelected();
        numOfSelectedInv = selectedInvoices.size();
        
        // Initialize Payment Logic
        paymentLogic = new JVCO_PaymentSharedLogic(selectedInvoices,
            JVCO_PaymentSharedLogic.PaymentMode.Credit);
    }

    /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Prepares the page
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    12-Dec-2016 ryan.i.r.limlingan  Initial version of function
    09-Jan-2017 ryan.i.r.limlingan  Added calls to JVCO_PaymentSharedLogic
    07-Mar-2017 ryan.i.r.limlingan  Added Debit/Credit Card string to the passed parameter for
                                    createInvoiceGroup
    05-Apr-2018 patrick.t.bautista  Error Message for GREEN-31226 Credit Card
    ----------------------------------------------------------------------------------------------- */
    /*public PageReference init()
    {
        //Getting Permission set of the user and see if it's has Enforcement Advisors
        String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
        Map<Id,PermissionSet> enAdPermSet = new Map<Id,PermissionSet>([SELECT Id FROM PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors']);
        List<PermissionSetAssignment> permissionSetList = [SELECT Id FROM PermissionSetAssignment WHERE 
                                                           AssigneeId = :UserInfo.getUserId() AND PermissionSetId IN :enAdPermSet.keySet()];
        if (numOfSelectedInv > 0)
        {
            List<String> paymentStatus = new List<String>{'Errored','Expired','Cancel'};
            retrievedInvoices = [SELECT Name, c2g__InvoiceStatus__c, c2g__Account__r.JVCO_Credit_Status__c, 
                                 JVCO_Invoice_Group__c, c2g__PaymentStatus__c, JVCO_Payment_Method__c,
                                 c2g__Account__r.JVCO_In_Infringement__c, c2g__Account__r.JVCO_In_Enforcement__c,
                                 JVCO_Invoice_Group__r.JVCO_Payment_Agreement__r.PAYREC2__Status__c,
                                 JVCO_Invoice_Group__r.JVCO_Payment_Agreement__c 
                                 FROM c2g__codaInvoice__c WHERE Id IN :selectedInvoices];
            for(c2g__codaInvoice__c invoice : retrievedInvoices)
            {
                //Check if the sales invoice is in Direct Debit
                if(invoice.JVCO_Payment_Method__c == 'Direct Debit' && invoice.JVCO_Invoice_Group__c != NULL
                   && invoice.JVCO_Invoice_Group__r.JVCO_Payment_Agreement__c != NULL
                   && !paymentStatus.contains(invoice.JVCO_Invoice_Group__r.JVCO_Payment_Agreement__r.PAYREC2__Status__c))
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, INVALID_CC_INVALID_RECORDS));
                    numOfSelectedInv = 0;
                    return null;
                }
                //Check if Invoice Status is Not Complete or Paid
                else if(!invoice.c2g__InvoiceStatus__c.equals('Complete') || invoice.c2g__PaymentStatus__c.equals('Paid'))
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, INVALID_RECORDS_ERR));
                    numOfSelectedInv = 0;
                    return null;
                }
                //Check if the Account is in Infringement or Enforcement and System admin
                else if((invoice.c2g__Account__r.JVCO_In_Infringement__c == TRUE || invoice.c2g__Account__r.JVCO_In_Enforcement__c == TRUE) 
                        && (invoice.c2g__Account__r.JVCO_Credit_Status__c == 'Enforcement - Debt Recovery' ||
                            invoice.c2g__Account__r.JVCO_Credit_Status__c == 'Enforcement - Infringement') &&
                        profileName <> 'System Administrator' && permissionSetList.isEmpty())
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, INVALID_CC_ERROR));
                    numOfSelectedInv = 0;
                    return null;
                }
            }
            Account acct = [SELECT id, Name FROM Account WHERE id =: retrievedInvoices[0].c2g__Account__c];
            JVCO_Invoice_Group__c ig = paymentLogic.createInvoiceGroup('Debit/Credit Card', FALSE);
            paymentLogic.updateLicenceAccount(ig, acct);
            return paymentLogic.redirectToPayonomy(acct);
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, NO_RECORDS_ERR));
        }
        return null;
    }

    /* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Returns to Account detail page
    Inputs: N/A
    Returns: PageReference
    <Date>      <Authors Name>      <Brief Description of Change> 
    09-Jan-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    /*public PageReference returnToAccount()
    {
        PageReference page = new PageReference(stdCtrl.cancel().getUrl());
        page.setRedirect(TRUE);
        return page;
    }*/
}