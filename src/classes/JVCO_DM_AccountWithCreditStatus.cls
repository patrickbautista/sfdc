/* ----------------------------------------------------------------------------------------------
Name: JVCO_DM_AccountWithCreditStatus.cls 
Description: Batch class that updates the Last Reminder Severity Level field of the sales invoice basing on the credit status of the licence account related to it.
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
24-Oct-2017  0.1         desiree.m.quijada     Initial creation
04-Dec-2017  0.2         desiree.m.quijada     GREEN-26734 - Refactor the class
----------------------------------------------------------------------------------------------- */

public class JVCO_DM_AccountWithCreditStatus implements Database.Batchable<sObject>{

    public static List<c2g__codaInvoice__c> salesInvoiceList;


    public static void init(){

        List<c2g__codaInvoice__c> updateSalesInvoice = new List<c2g__codaInvoice__c>();
        
        
        //create a map for credit status and the number of days for the reminder level
        Map<String, Integer> credStatusLevelMap = new Map<String, Integer>();
        credStatusLevelMap.put('Debt - 1st stage', 2);
        credStatusLevelMap.put('Debt - 2nd stage', 3);
        credStatusLevelMap.put('Debt - 3rd stage', 4);
        credStatusLevelMap.put('Debt - Final Demand', 5);
        
        //for every sales invoice whose credit status of the account is equal to the key on the map then the reminder field will be updated
        if(!salesInvoiceList.isEmpty()){
            for(c2g__codaInvoice__c si : salesInvoiceList){
                if (credStatusLevelMap.containsKey(si.c2g__Account__r.JVCO_Credit_Status__c)){
                    si.ffps_custRem__Last_Reminder_Severity_Level__c = credStatusLevelMap.get(si.c2g__Account__r.JVCO_Credit_Status__c);
                }
                updateSalesInvoice.add(si);
            }
        }

        //update the salesinvoices
        if(!updateSalesInvoice.isEmpty()){
            database.update(updateSalesInvoice,false);
        }
    }

    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT ID, c2g__PaymentStatus__c, ffps_custRem__Last_Reminder_Severity_Level__c, c2g__Account__r.JVCO_Credit_Status__c
                                        FROM c2g__codaInvoice__c 
                                        WHERE c2g__PaymentStatus__c IN ('Part Paid', 'Unpaid') 
                                        AND c2g__Account__r.JVCO_Credit_Status__c IN ('Debt - 1st stage','Debt - 2nd stage', 'Debt - 3rd stage', 'Debt - Final Demand')
                                        AND ffps_custRem__Last_Reminder_Severity_Level__c = NULL]);
                                       
    }

    public void execute(Database.BatchableContext BC, List<c2g__codaInvoice__c> scope){
        salesInvoiceList = scope;
        init();

    }

    public void finish(Database.BatchableContext BC){
        System.debug('Done');
    }
}