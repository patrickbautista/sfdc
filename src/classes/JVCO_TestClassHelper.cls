@isTest
public class JVCO_TestClassHelper {

    public static List<QueuesObject> setFFQueueList(Id groupId)
    {
        List<QueuesObject> listQueue = new List<QueuesObject>();
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='Case'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaCompany__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sObjecttype='c2g__codaAccountingCurrency__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaYear__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaInvoice__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaJournal__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaGeneralLedgerAccount__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaTransaction__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaCreditNote__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaCashEntry__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaInvoice__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaBankAccount__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaMatchingReference__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaCashMatchingHistory__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaPurchaseInvoice__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='PAYBASE2__Payment__c'));
        listQueue.add(new QueuesObject (queueid=groupId, sobjecttype='c2g__codaBankStatement__c'));
        return listQueue;
    }

    public static Group setGroup()
    {
        return new Group(Name='test group', Type='Queue');
    }

    public static GroupMember setGroupMember(Id groupId)
    {
        GroupMember groupMember = new GroupMember();
        groupMember.GroupId = groupId;
        groupMember.UserOrGroupId = UserInfo.getUserId();
        return groupMember;
    }

    public static c2g__codaCompany__c setCompany(Id groupId)
    {
        c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
        testCompany.c2g__CashMatchingCurrencyMode__c = 'Account';
        testCompany.c2g__Country__c = 'GB';
        testCompany.c2g__ECCountryCode__c = 'GB';
        testCompany.Name = 'Test Company';
        testCompany.ownerid = groupId;
        return testCompany;
    }
    
    public static c2g__codaBankAccount__c setBankAcc(Id groupId, Id accCurrencyId, Id glaId)
    {
        c2g__codaBankAccount__c testBankAccount = new c2g__codaBankAccount__c();
        testBankAccount.c2g__AccountName__c = 'Test Bank Account';
        testBankAccount.c2g__AccountNumber__c = '987654';
        testBankAccount.c2g__BankName__c = 'Barclays Bank - Current';
        testBankAccount.c2g__GeneralLedgerAccount__c = glaId;
        testBankAccount.Name = 'Test Bank Account 1';
        testBankAccount.c2g__OwnerCompany__c = groupId;
        testBankAccount.c2g__ReportingCode__c = 'BARC-CURR-JV';
        testBankAccount.c2g__BankAccountCurrency__c = accCurrencyId;
        return testBankAccount;
    }
    
    public static c2g__codaAccountingCurrency__c setAccCurrency(Id testCompanyId, Id groupId)
    {
        c2g__codaAccountingCurrency__c accCurrency = new c2g__codaAccountingCurrency__c();
        accCurrency.c2g__OwnerCompany__c = testCompanyId;
        accCurrency.c2g__DecimalPlaces__c = 2;
        accCurrency.Name = 'GBP';
        accCurrency.c2g__Dual__c = true ;
        accCurrency.ownerid = groupId;
        accCurrency.c2g__Home__c = true;
        return accCurrency;
    }

    public static c2g__codaGeneralLedgerAccount__c setGLA(String glaType, Id groupId)
    {
        if(glaType == 'ARC')
        {
            return getAccReceivableGLA(groupId);
        }
        else if(glaType == 'IC')
        {
            return getIncControlGLA(groupId);
        }
        else if(glaType == 'VAT')
        {
            return getVATGLA(groupId);
        }
        else if(glaType == 'NONCONV')
        {
            return getNonConversionGLA(groupId);
        }
        else if(glaType == 'CONV')
        {
            return getConversionGLA(groupId);
        }
        else if(glaType == 'PPLCLEAR')
        {
            return getPPLClearingGLA(groupId);
        }
        else if(glaType == 'PRSCLEAR')
        {
            return getPRSClearingGLA(groupId);
        }
        else if(glaType == 'VPLCLEAR')
        {
            return getVPLClearingGLA(groupId);
        }
        else
        {
            return null;
        }
    }

    private static c2g__codaGeneralLedgerAccount__c getAccReceivableGLA(Id groupId)
    {   
        c2g__codaGeneralLedgerAccount__c getAccReceivableGLA = new c2g__codaGeneralLedgerAccount__c();
        getAccReceivableGLA.c2g__AdjustOperatingActivities__c = false;
        getAccReceivableGLA.c2g__AllowRevaluation__c = false;
        getAccReceivableGLA.c2g__CashFlowCategory__c = 'Operating Activities';
        getAccReceivableGLA.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        getAccReceivableGLA.c2g__GLAGroup__c = 'Accounts Receivable';
        getAccReceivableGLA.c2g__ReportingCode__c = '10040';
        getAccReceivableGLA.c2g__TrialBalance1__c = 'Assets';
        getAccReceivableGLA.c2g__TrialBalance2__c = 'Current Assets';
        getAccReceivableGLA.c2g__TrialBalance3__c = 'Accounts Receivables';
        getAccReceivableGLA.c2g__Type__c = 'Balance Sheet';
        getAccReceivableGLA.Name = '10040 - Accounts receivables control';
        getAccReceivableGLA.ownerid = groupId;
        return getAccReceivableGLA;
    }

    private static c2g__codaGeneralLedgerAccount__c getIncControlGLA(Id groupId)
    {
        c2g__codaGeneralLedgerAccount__c incomeControlGLA = new c2g__codaGeneralLedgerAccount__c();
        incomeControlGLA.c2g__ReportingCode__c = '30030';
        incomeControlGLA.c2g__TrialBalance1__c = 'Liabilities';
        incomeControlGLA.c2g__TrialBalance2__c = 'Income Control';
        incomeControlGLA.c2g__TrialBalance3__c = 'Short-term income control account';
        incomeControlGLA.c2g__Type__c = 'Balance Sheet';
        incomeControlGLA.Name = '30030 - Income Control';
        incomeControlGLA.ownerid = groupId;
        return incomeControlGLA;
    }

    private static c2g__codaGeneralLedgerAccount__c getVATGLA(Id groupId)
    {
        c2g__codaGeneralLedgerAccount__c incomeControlGLA = new c2g__codaGeneralLedgerAccount__c();
        incomeControlGLA.c2g__ReportingCode__c = '10390';
        incomeControlGLA.c2g__TrialBalance1__c = 'Liabilities';
        incomeControlGLA.c2g__TrialBalance2__c = 'Short-Term Liabilities';
        incomeControlGLA.c2g__TrialBalance3__c = 'Tax and Social Security Liabilities';
        incomeControlGLA.c2g__Type__c = 'Balance Sheet';
        incomeControlGLA.Name = '10390 - VAT Output - Licensing';
        incomeControlGLA.ownerid = groupId;
        return incomeControlGLA;
    }
    
    private static c2g__codaGeneralLedgerAccount__c getConversionGLA(Id groupId)
    {
        c2g__codaGeneralLedgerAccount__c incomeControlGLA = new c2g__codaGeneralLedgerAccount__c();
        incomeControlGLA.c2g__ReportingCode__c = '10130';
        incomeControlGLA.c2g__TrialBalance1__c = 'Liabilities';
        incomeControlGLA.c2g__TrialBalance2__c = 'Bank Conversion';
        incomeControlGLA.c2g__TrialBalance3__c = 'Short-term income control account';
        incomeControlGLA.c2g__Type__c = 'Balance Sheet';
        incomeControlGLA.Name = '10130 - Bank Conversion';
        incomeControlGLA.ownerid = groupId;
        return incomeControlGLA;
    }
    
    private static c2g__codaGeneralLedgerAccount__c getNonConversionGLA(Id groupId)
    {
        c2g__codaGeneralLedgerAccount__c incomeControlGLA = new c2g__codaGeneralLedgerAccount__c();
        incomeControlGLA.c2g__ReportingCode__c = '10100';
        incomeControlGLA.c2g__TrialBalance1__c = 'Liabilities';
        incomeControlGLA.c2g__TrialBalance2__c = 'Bank Cash';
        incomeControlGLA.c2g__TrialBalance3__c = 'Short-term income control account';
        incomeControlGLA.c2g__Type__c = 'Balance Sheet';
        incomeControlGLA.Name = '10100 - Bank Cash';
        incomeControlGLA.ownerid = groupId;
        return incomeControlGLA;
    }
    
    private static c2g__codaGeneralLedgerAccount__c getPPLClearingGLA(Id groupId)
    {
        c2g__codaGeneralLedgerAccount__c clearingGLA = new c2g__codaGeneralLedgerAccount__c();
        clearingGLA.c2g__ReportingCode__c = '30062';
        clearingGLA.c2g__TrialBalance1__c = 'Liabilities';
        clearingGLA.c2g__TrialBalance2__c = 'Income Control';
        clearingGLA.c2g__TrialBalance3__c = 'Trade and other payables';
        clearingGLA.c2g__Type__c = 'Balance Sheet';
        clearingGLA.Name = '30062 - PPL clearing account';
        clearingGLA.ownerid = groupId;
        return clearingGLA;
    }
    
    private static c2g__codaGeneralLedgerAccount__c getPRSClearingGLA(Id groupId)
    {
        c2g__codaGeneralLedgerAccount__c clearingGLA = new c2g__codaGeneralLedgerAccount__c();
        clearingGLA.c2g__ReportingCode__c = '30063';
        clearingGLA.c2g__TrialBalance1__c = 'Liabilities';
        clearingGLA.c2g__TrialBalance2__c = 'Income Control';
        clearingGLA.c2g__TrialBalance3__c = 'Trade and other payables';
        clearingGLA.c2g__Type__c = 'Balance Sheet';
        clearingGLA.Name = '30063 - PPL clearing account';
        clearingGLA.ownerid = groupId;
        return clearingGLA;
    }
    
    private static c2g__codaGeneralLedgerAccount__c getVPLClearingGLA(Id groupId)
    {
        c2g__codaGeneralLedgerAccount__c clearingGLA = new c2g__codaGeneralLedgerAccount__c();
        clearingGLA.c2g__ReportingCode__c = '30064';
        clearingGLA.c2g__TrialBalance1__c = 'Liabilities';
        clearingGLA.c2g__TrialBalance2__c = 'Income Control';
        clearingGLA.c2g__TrialBalance3__c = 'Trade and other payables';
        clearingGLA.c2g__Type__c = 'Balance Sheet';
        clearingGLA.Name = '30064 - PPL clearing account';
        clearingGLA.ownerid = groupId;
        return clearingGLA;
    }
    
    public static c2g__codaTaxCode__c setTaxCode(Id vatGLAId){
        c2g__codaTaxCode__c taxCode = new c2g__codaTaxCode__c();
        taxCode.name = 'GB-O-STD-LIC';
        taxCode.c2g__GeneralLedgerAccount__c = vatGLAId;
        taxCode.JVCO_Rate__c = 20;
        taxCode.ffvat__NetBox__c = 'Box 6';
        taxCode.ffvat__TaxBox__c = 'Box 1';
        return taxCode;
    }

    public static c2g__codaTaxRate__c setTaxRate(Id taxCodeId){
        c2g__codaTaxRate__c testTaxRate = new c2g__codaTaxRate__c();
        testTaxRate.c2g__Rate__c = 20.0;
        testTaxRate.c2g__TaxCode__c = taxCodeId;
        return testTaxRate;
    }

    public static c2g__codaDimension1__c setDim1()
    {
        c2g__codaDimension1__c dim1 = new c2g__codaDimension1__c();
        dim1.Name = 'Education';
        dim1.c2g__ReportingCode__c = 'BC001';
        dim1.JVCO_Function__c = 'Budget';
        return dim1;
    }

    public static c2g__codaDimension2__c setDim2()
    {
        c2g__codaDimension2__c dim2 = new c2g__codaDimension2__c();
        dim2.name = 'Paid at PPL';
        dim2.c2g__ReportingCode__c = 'Paid at PPL';
        return dim2;
    }
    
    public static c2g__codaDimension2__c setDim2(String name)
    {
        c2g__codaDimension2__c dim2 = new c2g__codaDimension2__c();
        dim2.name = name;
        dim2.c2g__ReportingCode__c = name;
        return dim2;
    }
    
    public static c2g__codaDimension3__c setDim3()
    {
        c2g__codaDimension3__c dim3 = new c2g__codaDimension3__c();
        dim3.name = 'Non Key';
        dim3.c2g__ReportingCode__c = 'Non Key';
        return dim3;
    }

    public static Product2 setProduct(Id icGLAId)
    {
        Product2 prod = new Product2();
        prod.Name = 'PPL Product';
        prod.SBQQ__ChargeType__c= 'One-Time';
        prod.JVCO_Society__c = 'PPL';
        prod.ProductCode = 'PPLPP085';
        prod.IsActive = true;
        prod.c2g__CODASalesRevenueAccount__c = icGLAId;
        prod.JVCO_Surcharge_Effective_Date__c = Date.newInstance(2016, 01, 01);
        prod.JVCO_Legacy_Joint_Tariff_Licence__c = true;
        return prod;
    }
    
    public static PriceBook2 setPriceBook2()
    {
        Pricebook2 pb = new Pricebook2();
        pb.name = 'Standard Price Book';
        pb.IsActive = true;
        return pb;
    }
    public static PriceBookEntry setPriceBookEntry(Id prodId)
    {
        PriceBookEntry pbe = new PriceBookEntry();
        pbe.Pricebook2Id = Test.getStandardPriceBookId();
        pbe.product2id = prodId;
        pbe.IsActive = true;
        pbe.UnitPrice = 1000.0;
        return pbe;
    }

    public static Account setCustAcc(Id arcGLAId, Id dim1Id)
    {
        Account acc = new Account();
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        acc.Name = 'Test Account 83202-C';
        acc.c2g__CODAAccountsReceivableControl__c = arcGLAId;
        acc.Type = 'Key Account';
        acc.c2g__CODADimension1__c = dim1Id;
        return acc;
    }

    public static Account setLicAcc(Id accId, Id taxCodeId, Id contactId)
    {
        Account acc = new Account();
        acc.Name = 'Test Account 832020-Lic';
        acc.AccountNumber = '832020';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
        acc.JVCO_Customer_Account__c = accId;
        acc.c2g__CODAOutputVATCode__c = taxCodeId;
        acc.JVCO_Preferred_Contact_Method__c = 'Email';
        acc.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        acc.JVCO_Billing_Contact__c = contactId;
        acc.JVCO_Licence_Holder__c = contactId;
        acc.JVCO_Review_Contact__c = contactId;
        acc.JVCO_DD_Payee_Contact__c = contactId;
        acc.JVCO_Collections_Agent__c = UserInfo.getUserId();
        acc.Type = 'Key Account';
        acc.JVCO_DD_Mandate_Checker__c = false;
        return acc;
    }
    
    public static Account setLicAcc(Id accId, Id taxCodeId, Id contactId, Id argLA)
    {
        Account acc = new Account();
        acc.Name = 'Test Account 83202-Lic';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
        acc.JVCO_Customer_Account__c = accId;
        acc.c2g__CODAOutputVATCode__c = taxCodeId;
        acc.JVCO_Preferred_Contact_Method__c = 'Email';
        acc.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        acc.JVCO_Billing_Contact__c = contactId;
        acc.JVCO_Licence_Holder__c = contactId;
        acc.JVCO_Review_Contact__c = contactId;
        acc.JVCO_DD_Payee_Contact__c = contactId;
        acc.JVCO_Collections_Agent__c = UserInfo.getUserId();
        acc.c2g__CODAAccountsReceivableControl__c = argLA;
        acc.c2g__CODAAccountTradingCurrency__c = 'GBP';
        acc.Type = 'Key Account';
        return acc;
    }

    public static Account setSuppAcc(Id taxCodeId, Id contactId, Id gla)
    {   
        Account acc = new Account();
        acc.Name = 'Test Account 83202-Lic';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        acc.c2g__CODAInputVATCode__c = taxCodeId;
        acc.JVCO_Preferred_Contact_Method__c = 'Email';
        acc.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        acc.JVCO_Supplier_Contact__c = contactId;
        acc.c2g__CODAFinanceContact__c = contactId;
        acc.c2g__CODAAccountTradingCurrency__c = 'GBP';
        acc.c2g__CODADescription1__c = 'test description 1';
        acc.c2g__CODABaseDate1__c = 'Invoice Date';
        acc.c2g__CODADaysOffset1__c = 0;
        acc.c2g__CODADiscount1__c = 0;
        acc.c2g__CODAAccountsPayableControl__c = gla;
        acc.c2g__CODABankAccountName__c = 'Test Bank AccName';
        acc.c2g__CODABankAccountNumber__c = '1234567890';
        acc.c2g__CODABankAccountReference__c = 'Test Bank AccRef';
        acc.c2g__CODABankSortCode__c = '1234';
        acc.Type = 'Key Account';
        acc.JVCO_Supplier_Category__c = 'Office Supplies';
        acc.JVCO_Email__c = 'test@test.com';
        return acc;
    }

    public static Contact setContact(Id custAcc)
    {
        Contact c = new Contact();
        c.LastName = 'Contact';
        c.FirstName = 'Test';
        c.Email = 'test@test.com';
        c.AccountId = custAcc; 
        c.MailingStreet = '30 Fenchurch St';
        c.MailingCity = 'London';
        c.MailingPostalCode = 'EC3M 3AD';
        c.MailingCountry = 'UK';       
        return c;
    }

    public static Opportunity setOpportunity(Id licAccId)
    {
        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 83202';
        o.AccountId = licAccId;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        return o;
    }
    
    public static SBQQ__QuoteProcess__c setQuoteProcess() 
    {
        SBQQ__QuoteProcess__c qProcess = new SBQQ__QuoteProcess__c();
        qProcess.SBQQ__Default__c = true;
        return qProcess;
    }
    
    public static SBQQ__Quote__c setQuote(Id accId, Id oppId)
    {
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Renewal';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = accId;
        q.SBQQ__Opportunity2__c = oppId;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        q.Accept_TCs__c  = 'Yes';
        q.JVCO_PONumber__c = '999999';
        return q;
    }

    public static SBQQ__QuoteLine__c setQuoteLine(Id qId, Id prodId)
    {
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = qId;
        ql.SBQQ__Product__c = prodId;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        ql.SBQQ__ChargeType__c= 'One-Time';
        ql.End_Date__c = Date.today();
        return ql;
    }
    public static Contract setContract(Id accId, Id oppId, Id qId)
    {
        Contract c = new Contract();
        c.AccountId = accId;
        c.Status = 'Draft';
        c.StartDate = Date.today();
        c.EndDate = Date.today().addDays(28);
        c.ContractTerm = 12;
        c.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
        c.SBQQ__Opportunity__c = oppId;
        c.SBQQ__Quote__c = qId;
        
        return c;
    }

    public static Order setOrder(Id accId, Id qId)
    {
        Order o = new Order();
        o.Accountid = accId;
        o.effectivedate = System.Today();
        o.SBQQ__Quote__c = qId;
        o.blng__BillingAccount__c = accId;
        o.SBQQ__PaymentTerm__c = 'Net 30';
        o.SBQQ__PriceCalcStatus__c = 'Completed';
        o.status = 'Draft';
        o.Pricebook2Id = Test.getStandardPriceBookId();
        return o;
    }


    public static OrderItem setOrderItem(Id oId, Id pbId, Id qlId)
    {
        OrderItem oi = new OrderItem();
        oi.OrderId = oId;
        oi.PriceBookEntryId = pbId;
        oi.SBQQ__QuoteLine__c = qlId;
        oi.SBQQ__ChargeType__c = 'One-Time';
        oi.Quantity = 100.0;
        oi.UnitPrice = 100.00;
        oi.EndDate = date.today().addMonths(5);
        oi.SBQQ__Activated__c = false;
        oi.blng__NextBillingDate__c = null;
        oi.blng__OverrideNextBillingDate__c = null;
        oi.blng__BillThroughDateOverride__c = null;
        return oi;
    }
    
    public static OrderItem setNegativeOrderItem(Id oId, Id pbId, Id qlId)
    {
        OrderItem oi = new OrderItem();
        oi.OrderId = oId;
        oi.PriceBookEntryId = pbId;
        oi.SBQQ__QuoteLine__c = qlId;
        oi.SBQQ__ChargeType__c = 'One-Time';
        oi.Quantity = -300.0;
        oi.UnitPrice = 100.00;
        oi.EndDate = date.today().addMonths(5);
        oi.SBQQ__Activated__c = false;
        return oi;
    }

    public static c2g__codaYear__c setYear(Id compId, Id groupId)
    {
        c2g__codaYear__c yr = new c2g__codaYear__c();
        yr.Name ='2016';
        yr.c2g__AutomaticPeriodList__c =  true;
        yr.c2g__OwnerCompany__c = compId;
        yr.c2g__ExternalId__c = 'yzsd1234';
        yr.c2g__NumberOfPeriods__c =11;
        yr.c2g__StartDate__c =  System.today() - 10;
        yr.c2g__Status__c = 'Open';
        yr.c2g__PeriodCalculationBasis__c = '445';
        yr.c2g__YearEndMode__c = 'Full Accounting Code' ; 
        yr.ownerid = groupId;
        return yr;
    }
    
    public static c2g__codaYear__c setYear2017(Id compId, Id groupId)
    {
        c2g__codaYear__c yr = new c2g__codaYear__c();
        yr.Name ='2017';
        yr.c2g__AutomaticPeriodList__c =  true;
        yr.c2g__OwnerCompany__c = compId;
        yr.c2g__ExternalId__c = 'yzsd2234';
        yr.c2g__NumberOfPeriods__c =11;
        yr.c2g__StartDate__c =  System.today() - 10;
        yr.c2g__Status__c = 'Open';
        yr.c2g__PeriodCalculationBasis__c = '445';
        yr.c2g__YearEndMode__c = 'Full Accounting Code' ; 
        yr.ownerid = groupId;
        return yr;
    }

    public static c2g__codaYear__c setYear(Id compId, Id groupId, String yearName)
    {
        c2g__codaYear__c yr = new c2g__codaYear__c();
        yr.Name = yearName;
        yr.c2g__AutomaticPeriodList__c =  true;
        yr.c2g__OwnerCompany__c = compId;
        yr.c2g__ExternalId__c = 'yzsd3234';
        yr.c2g__NumberOfPeriods__c = 11;
        yr.c2g__StartDate__c =  System.today() - 10;
        yr.c2g__Status__c = 'Open';
        yr.c2g__PeriodCalculationBasis__c = '445';
        yr.c2g__YearEndMode__c = 'Full Accounting Code' ; 
        yr.ownerid = groupId;
        return yr;
    }
    
    public static c2g__codaPeriod__c setPeriod(Id compId, Id yrId)
    {
        c2g__codaPeriod__c testPeriod = new c2g__codaPeriod__c();
        testPeriod.c2g__OwnerCompany__c = compId;
        testPeriod.c2g__StartDate__c = System.today()-30;
        testPeriod.c2g__EndDate__c = System.today()+30;
        testPeriod.c2g__PeriodNumber__c ='123';
        testPeriod.c2g__Description__c ='test Desc';
        testPeriod.c2g__PeriodGroup__c = 'Q1';
        testPeriod.c2g__PeriodNumber__c = '1';
        testPeriod.c2g__YearName__c = yrId;
        return testPeriod;
    }
    
    public static c2g__codaUserCompany__c setUserCompany(Id compId)
    {
        c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c();
        userCompany.c2g__Company__c = compId;
        userCompany.c2g__User__c = userInfo.getUserId();
        return userCompany;
    }
    
     public static JVCO_Venue__c createVenue()
    {
        JVCO_Venue__c testVenue = new JVCO_Venue__c();
        testVenue.Name = 'Sample Venue';
        testVenue.JVCO_Email__c = 'JVCO_QuoteLineTriggerHandlerTest@email.com';
        testVenue.External_Id__c = 'JVCO_QLTHTestsamplevenexid';
        testVenue.JVCO_Street__c = 'Test Street';
        testVenue.JVCO_City__c = 'Test City';
        testVenue.JVCO_Country__c = 'Test Country';
        testVenue.JVCO_Postcode__c = 'TN32 4SL';
       
        return testVenue;
    }

    public static JVCO_Affiliation__c createAffiliation(Id LicenceAccountId, Id VenueId)
    {
        JVCO_Affiliation__c testAffiliation = new JVCO_Affiliation__c();
        testAffiliation.JVCO_End_Date__c = date.today().addDays(20);
        testAffiliation.JVCO_Account__c = LicenceAccountId;
        testAffiliation.JVCO_Venue__c = VenueId;
        testAffiliation.JVCO_Closure_Reason__c = 'Dissolved';
        testAffiliation.JVCO_Start_Date__c = date.today();
        return testAffiliation;
    }
    

    public static void createBillingConfig() {
        List<sObject> blngConfigList = Test.loadData(blng__BillingConfig__c.sObjectType, 'JVCO_TestData_BillingConfig');
    }

    public static blng__Invoice__c setBInv(Id accId, Id oId)
    {
        blng__Invoice__c testInvoice = new blng__Invoice__c();
        testInvoice.blng__Account__c = accId;
        testInvoice.blng__Order__c = oId;
        testInvoice.blng__InvoiceDate__c = Date.today();
        testInvoice.blng__InvoiceStatus__c = 'Draft';
        return testInvoice;
    }

    public static blng__InvoiceLine__c setBInvLine(Id bInvId, Id pId, Id oiId, Decimal taxAmt, Boolean isCredit)
    {
        blng__InvoiceLine__c testInvoiceLine = new blng__InvoiceLine__c();
        testInvoiceLine.blng__Product__c = pId;
        testInvoiceLine.blng__StartDate__c = date.today();
        testInvoiceLine.blng__Invoice__c = bInvId;
        testInvoiceLine.blng__OrderProduct__c = oiId;
        testInvoiceLine.blng__UnitPrice__c = 172.99;
        if(isCredit)
        {
            testInvoiceLine.blng__Quantity__c = 234.00;
            testInvoiceLine.blng__TotalAmount__c = -1666.66;
            testInvoiceLine.blng__Subtotal__c = -3327.48;
        }else
        {
            testInvoiceLine.blng__Quantity__c = 1;
            testInvoiceLine.blng__TotalAmount__c = 10000.0;
            testInvoiceLine.blng__Subtotal__c = 10000.0;
        }
        
        testInvoiceLine.blng__TaxAmount__c = taxAmt;
        return testInvoiceLine;
    }

    public static void setCashMatchingCS()
    {
        c2g__codaCashMatchingSettings__c cm = new c2g__codaCashMatchingSettings__c();
        cm.JVCO_Write_off_Limit__c = 1;
        insert cm;
    }
    
    public static void setSurchargeCS()
    {
        JVCO_Invoice_Surcharge__c is = new JVCO_Invoice_Surcharge__c();
        is.Name = 'JVCO';
        is.JVCO_Surcharge_Multipiler__c = .5;
        insert is;
    }

    public static void setGeneralSettingsCS()
    {
        JVCO_General_Settings__c gs = new JVCO_General_Settings__c();
        gs.JVCO_Allow_Partial_Payment__c = true ;
        gs.JVCO_Background_Matching_Size__c = 1;
        gs.JVCO_Bank_Statement_Queue_Rec_For_Jobs__c = 1 ;
        gs.JVCO_Cash_Post_Queue_Rec_Per_Jobs__c = 1 ;
        gs.JVCO_Company_Name__c = 'Company Name';
        gs.JVCO_Currency_Code__c = 'GBP';
        gs.JVCO_Invoice_Schedule_Scope__c = 10;
        gs.JVCO_Non_Acceptable_GLAs__c = '10130,30030,30035,30062,30063,30064,30080';
        gs.JVCO_PayonomyPaymentProcessingBatchLimit__c = 5;
        gs.JVCO_PostCashEntriesQueueableLimit__c = 20;
        gs.JVCO_Queuable_Max_TransactionLine_Size__c = 1;
        gs.JVCO_Reporting_Code_JVCO__c = 'JVCO';
        gs.JVCO_Scope__c = 1;
        gs.JVCO_Sales_Invoice_Queue_Per_Jobs__c = 1;
        gs.JVCO_Cancel_Credit_Note_batch_size__c = 100;
        gs.JVCO_Post_Sales_Credit_Note_Scope__c = 1;
        gs.SetupOwnerId = Userinfo.getOrganizationId();
        insert gs;
    }

    public static Id getDDMandate(Id accId)
    {
        JVCO_DD_Mandate__C testDDMandate = new JVCO_DD_Mandate__C();
        testDDMandate.JVCO_Account_Number__c = '12345678';
        testDDMandate.JVCO_Sort_Code__c = '000001';
        testDDMandate.JVCO_Collection_Day__c = 1;
        testDDMandate.JVCO_Licence_Account__c = accId;
        testDDMandate.JVCO_Number_of_Payments__c = 4;
        testDDMandate.JVCO_AUDIS_Ref__c = '00000002';
        insert testDDMandate;
        return testDDMandate.Id;
    }
    
    public static Id getDDMandate(Id accId, Integer collectionDay, Integer noOfPayments)
    {
        JVCO_DD_Mandate__C testDDMandate = new JVCO_DD_Mandate__C();
        testDDMandate.JVCO_Account_Number__c = '12345678';
        testDDMandate.JVCO_Sort_Code__c = '000001';
        testDDMandate.JVCO_Collection_Day__c = collectionDay;
        testDDMandate.JVCO_Licence_Account__c = accId;
        testDDMandate.JVCO_Number_of_Payments__c = noOfPayments;
        testDDMandate.JVCO_AUDIS_Ref__c = '00000002';
        insert testDDMandate;
        return testDDMandate.Id;
    }
    
    public static Id getPaySched(Id baId)
    {
        PAYREC2__Payment_Schedule__c testPaySched = new PAYREC2__Payment_Schedule__c();
        testPaySched.name = '4 Instalments – 1st of the month';
        testPaySched.PAYFISH3__Bank_Account__c = baId;
        testPaySched.PAYFISH3__First_Collection_Interval__c = 10.0;
        testPaySched.PAYFISH3__Fund_Transfer_Agent__c = 'BACS';
        testPaySched.PAYREC2__Day__c = '1';
        testPaySched.PAYREC2__Frequency__c = 'month';
        testPaySched.PAYREC2__Interval__c = 1;
        testPaySched.PAYREC2__Max__c = 4;
        testPaySched.PAYREC2__Type__c = 'Fixed';
        insert testPaySched;
        return testPaySched.Id;
    }

    public static Id getPayBank()
    {
        PAYACCVAL1__Bank_Account__c testBankDebit = new PAYACCVAL1__Bank_Account__c();
        testBankDebit.PAYACCVAL1__Account_Number__c = '12345678';
        testBankDebit.PAYACCVAL1__Sort_Code__c = '000001';
        testBankDebit.PAYFISH3__Account_Name__c = 'Test Debit';
        insert testBankDebit;
        return testBankDebit.Id;
    }

    public static Id getPayBank(String name, String accountNumber, String sortCode)
    {
        PAYACCVAL1__Bank_Account__c testBankDebit = new PAYACCVAL1__Bank_Account__c();
        testBankDebit.PAYACCVAL1__Account_Number__c = accountNumber;
        testBankDebit.PAYACCVAL1__Sort_Code__c = sortCode;
        testBankDebit.PAYFISH3__Account_Name__c = name;
        insert testBankDebit;
        return testBankDebit.Id;
    }

    public static PAYBASE2__Payment__c getPayonomyPayment(String type)
    {
        PAYBASE2__Payment__c payment = new PAYBASE2__Payment__c();
        payment.PAYFISH3__Type__c = 'Collection';
        if(type == 'SagePay')
        {
            payment.RecordTypeId = Schema.SObjectType.PAYBASE2__Payment__c.getRecordTypeInfosByName().get('SagePay').getRecordTypeId();
            payment.PAYBASE2__Status__c = 'AUTHORISED';
        }else if(type == 'DirectDebit')
        {
            payment.RecordTypeId = Schema.SObjectType.PAYBASE2__Payment__c.getRecordTypeInfosByName().get('Direct Debit Collection').getRecordTypeId();
            payment.PAYBASE2__Status__c = 'Submitted';
        }
        payment.Name = 'Test Payment 01';
        payment.PAYBASE2__Is_Paid__c = true;
        payment.PAYBASE2__Amount_Refunded__c = 100;
        payment.PAYCP2__Is_Test__c = true;
        payment.PAYREC2__Is_Actual_Collection__c = true;
        payment.PAYSPCP1__Redirect_Parent__c= true;
        payment.PAYSPCP1__Gift_Aid__c= true;
        payment.PAYBASE2__Amount__c = 1250.00;

        return payment;   
    }

    public static c2g__codaInvoice__c getSalesInvoice(Id aId)
    {
        c2g__codaInvoice__c sInv = new c2g__codaInvoice__c();
        sInv.c2g__Account__c = aId;
        sInv.c2g__DueDate__c = System.today() + 30;
        sInv.c2g__InvoiceDate__c = System.today();
        sInv.c2g__DeriveCurrency__c = true;
        sInv.c2g__DerivePeriod__c = true;
        sInv.c2g__DeriveDueDate__c = false;
        return sInv;
    }

    public static c2g__codaInvoiceLineItem__c getSalesInvoiceLine(Id sInvId, Id dim1Id, Id dim3Id, Id taxId, Id dim2Id, Id prodId)
    {
        c2g__codaInvoiceLineItem__c sInvLine = new c2g__codaInvoiceLineItem__c();
        sInvLine.c2g__Invoice__c = sInvId;
        sInvLine.c2g__Dimension1__c = dim1Id;
        sInvLine.c2g__Dimension3__c = dim3Id;
        sInvLine.c2g__TaxCode1__c = taxId;
        sInvLine.c2g__DeriveLineNumber__c = TRUE;
        sInvLine.c2g__DeriveUnitPriceFromProduct__c = FALSE;
        sInvLine.c2g__Quantity__c = 1;
        sInvLine.c2g__UnitPrice__c = 1000;
        sInvLine.c2g__Dimension2__c = dim2Id;
        sInvLine.c2g__Product__c = prodId;
        return sInvLine;
    }

    public static c2g__codaCreditNote__c getCreditNote(Id licAccId)
    {
        c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
        cNote.c2g__Account__c = licAccId;
        cNote.c2g__DueDate__c = System.today() + 30;
        cNote.c2g__CreditNoteDate__c = System.today();
        cNote.c2g__DeriveCurrency__c = true;
        cNote.c2g__DerivePeriod__c = true;
        cNote.c2g__DeriveDueDate__c = true;
        return cNote;
    }

    public static c2g__codaCreditNoteLineItem__c getCreditNoteLine(Id cNoteId, Id dim1Id, Id dim3Id,
                                                                    Id taxCodeId, Id dim2Id, Id prodId, 
                                                                    Decimal unitPrice)
    {
        c2g__codaCreditNoteLineItem__c cNoteLine = new c2g__codaCreditNoteLineItem__c();
        cNoteLine.c2g__CreditNote__c = cNoteId;
        cNoteLine.c2g__Dimension1__c = dim1Id;
        cNoteLine.c2g__Dimension3__c = dim3Id;
        cNoteLine.c2g__TaxCode1__c = taxCodeId;
        cNoteLine.c2g__DeriveLineNumber__c = true;
        cNoteLine.c2g__DeriveUnitPriceFromProduct__c = false;
        cNoteLine.c2g__Quantity__c = 1;
        cNoteLine.c2g__UnitPrice__c = unitPrice;
        cNoteLine.c2g__Dimension2__c = dim2Id;
        cNoteLine.c2g__Product__c = prodId;
        return cNoteLine;
    }
    
    public static c2g__codaJournal__c getJournal()
    {
        c2g__codaJournal__c journal = new c2g__codaJournal__c();
        journal.c2g__JournalDate__c = System.today();
        journal.JVCO_Journal_Type__c = 'Standard';
        journal.c2g__Type__c = 'Manual Journal';
        journal.c2g__JournalDescription__c = 'Test';
        journal.c2g__DeriveCurrency__c = true;
        journal.c2g__DerivePeriod__c = true;
        return journal;
    }

    public static List<c2g__codaJournalLineItem__c> getJournalLineList(Id journalId, Id dim3Id, Id dim1Id, Id accId, Id accReceivableGLAId, Id glaId, Decimal amt)
    {
        List<c2g__codaJournalLineItem__c> journalLineList = new List<c2g__codaJournalLineItem__c>();
        c2g__codaJournalLineItem__c journalLine1 = new c2g__codaJournalLineItem__c();
        journalLine1.c2g__Value__c = amt;
        journalLine1.c2g__LineType__c = 'Account - Customer';
        journalLine1.c2g__Dimension3__c = dim3Id;
        journalLine1.c2g__GeneralLedgerAccount__c = accReceivableGLAId;
        journalLine1.c2g__Account__c = accId;
        journalLine1.c2g__Dimension1__c = dim1Id;
        journalLine1.c2g__Journal__c = journalId;
        journalLineList.add(journalLine1);

        c2g__codaJournalLineItem__c journalLine2 = new c2g__codaJournalLineItem__c();
        journalLine2.c2g__Value__c = -amt;
        journalLine2.c2g__LineType__c = 'General Ledger Account';
        journalLine2.c2g__Dimension3__c = dim3Id;
        journalLine2.c2g__GeneralLedgerAccount__c = glaId;
        journalLine2.c2g__Dimension1__c = dim1Id;
        journalLine2.c2g__Journal__c = journalId;
        journalLineList.add(journalLine2);
        
        return journalLineList;
    }

    public static c2g__codaCashEntry__c getCashEntry(String paymentMethod)
    {
        c2g__codaCashEntry__c cashEntry = new c2g__codaCashEntry__c();
        cashEntry.c2g__Date__c = System.today();
        cashEntry.ffcash__DerivePeriod__c = true;
        cashEntry.c2g__PaymentMethod__c = paymentMethod;
        return cashEntry;
    }

    public static c2g__codaCashEntryLineItem__c getCashEntryLine(Id cashEntryId, Id accId, Decimal amt, String paymentMethod)
    {
        c2g__codaCashEntryLineItem__c cashEntryLI = new c2g__codaCashEntryLineItem__c();
        cashEntryLI.c2g__CashEntry__c = cashEntryId;
        cashEntryLI.c2g__Account__c = accId;
        cashEntryLI.c2g__CashEntryValue__c = amt;
        cashEntryLI.c2g__AccountPaymentMethod__c = paymentMethod;
        cashEntryLI.ffcash__DeriveLineNumber__c = true;
        return cashEntryLI;
    }
}