/* --------------------------------------------------------------------------------------------------
Name:            JVCO_OrderRenewalScheduler.cls 
Description:     Class that handles OrderRenewalScheduler Schedulable APEX class
Test class:      

Date                 Version     Author                          Summary of Changes 
-----------          -------     -----------------               -------------------------------------------
05-Jul-2017           0.1         Accenture-Mel Andrei Santos      Intial draft    
------------------------------------------------------------------------------------------------ */

global class JVCO_OrderRenewalScheduler implements Schedulable{
    
    global void execute (SchedulableContext sc)
    { 

        JVCO_OrderRenewalBatch OrderRenewalBatch = new JVCO_OrderRenewalBatch();
        JVCO_OrderRenewalScheduler OrderRenewalSched = new  JVCO_OrderRenewalScheduler();
        //String cronStr = '0 0 23 1/1 * ? *';
       // String jobID = System.schedule('Process OrderRenewalScheduler', cronStr, OrderRenewalSched);
        Database.executeBatch(OrderRenewalBatch,1);
    }
    
    private static String getSchedulerExpression(DateTime dt) 
    {
         return '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();    
    }

    public static void callExecuteMomentarily()  
    {
        System.schedule('JVCO_OrderRenewalScheduler', getSchedulerExpression(DateTime.Now().addSeconds(5)), new JVCO_OrderRenewalScheduler());
    }
}