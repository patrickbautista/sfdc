@IsTest
private class JVCO_BatchJobHelperTest{

	@testSetup 
	static void setupTestData(){

		JVCO_BatchJobHelperSettings__c batchJHelper = JVCO_BatchJobHelperSettings__c.getOrgDefaults();
		batchJHelper.JVCO_BatchJobHelperActive__c = true;
		batchJHelper.JVCO_FlexQueueLimit__c = 0;
		batchJHelper.JVCO_NoOfJobItemsTreshold__c = 1;
		upsert batchJHelper;
	}

	private static testMethod void testBatchJobHelper(){

		Test.startTest();
		JVCO_BatchJobHelper.checkLargeJobs(1);
		Test.stopTest();
	}

	private static testMethod void testBatchJobHelperIsLargeJob(){

		Test.startTest();

		JVCO_BatchJobHelperSettings__c batchJHelper = [select id, JVCO_FlexQueueLimit__c from JVCO_BatchJobHelperSettings__c limit 1];
		batchJHelper.JVCO_FlexQueueLimit__c = 50;
		update batchJHelper;

		JVCO_BatchJobHelper.checkLargeJobs(2);
		Test.stopTest();
	}
}