@isTest
private class JVCO_OrderRenewalBatchTest
{
    @testSetup static void setupTestData() 
    {
        List<JVCO_Constants__c> custSetting = new List<JVCO_Constants__c>();
        custSetting.add(new JVCO_Constants__c(JVCO_Quote_Recalculation_Time_Buffer__c = 5));
        insert custSetting;
        
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();  
        insert qProcess;

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Account acc =  JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.JVCO_Renewal_Scenario__c = '';
        insert acc2;

        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_End_Date__c = null;
        aff.JVCO_Closure_Reason__c = null;
        insert aff;

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        opp.JVCO_OpportunityCancelled__c = false;
        opp.JVCO_Auto_Renewed__c = true;
        insert opp;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, 'Annually');
        q.JVCO_Quote_Auto_Renewed__c = true;
        q.SBQQ__Status__c = 'Draft';
        q.Recalculate__c = true;
        q.SBQQ__Type__c = 'Quote';
        insert q;
        
        SBQQ.TriggerControl.disable();
        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];
        
        q.SBQQ__Type__c = 'Renewal';
        update q;
        
        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, prod.Id);

        //ql.CreatedDate = Datetime.Now();
        //ql.JVCO_Salesforce_Last_Recalculated_Time__c = Datetime.Now().addDays(3);
        insert ql;
		SBQQ.TriggerControl.enable();
        //q.SBQQ__Type__c = 'Renewal';
        //q.JVCO_Quote_Auto_Renewed__c = true;
        //q.SBQQ__Status__c = 'Draft';
        //q.Recalculate__c = true;
        //ql.JVCO_Salesforce_Last_Recalculated_Time__c = Datetime.Now().addDays(3);
        //update q;

        //system.assertEquals(null, [select id, SBQQ__LineItemCount__c, JVCO_Recalculated__c from  sbqq__quote__c where id = :q.id]);

        List<JVCO_Renewal_Settings__c> settings = new List<JVCO_Renewal_Settings__c>();
        settings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Completion', Days__c = 0.0));
        settings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Generation', Days__c = 29));
        insert settings;

        


    }

    @isTest
    static void itShould()
    {

        SBQQ__Quote__c q = [select Id, JVCO_Quote_Auto_Renewed__c, JVCO_Recalculated__c  from SBQQ__Quote__c  limit 1];
        Opportunity o = [select id,JVCO_OpportunityCancelled__c,SBQQ__Contracted__c,SBQQ__Ordered__c, JVCO_Auto_Renewed__c  from Opportunity limit 1];

        q.JVCO_Quote_Auto_Renewed__c = true;
        q.Recalculate__c = true;
        update q;

        o.SBQQ__Contracted__c = TRUE;
        o.SBQQ__Ordered__c = FALSE;
        o.JVCO_Auto_Renewed__c  = TRUE;
        update o;

        system.assertEquals(TRUE, o.SBQQ__Contracted__c, 'Quote not Contracted');
        system.assertEquals(FALSE, o.SBQQ__Ordered__c, 'Opp is Ordered');
        system.assertEquals(False, o.JVCO_OpportunityCancelled__c, 'Opportunity cancelled');
        
        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_OrderRenewalBatch(), 1);
        Test.stopTest();

    }

    @isTest
    static void itShouldError()
    {
         SBQQ__Quote__c q = [select Id, JVCO_Quote_Auto_Renewed__c, JVCO_Recalculated__c  from SBQQ__Quote__c  limit 5];
        Opportunity o = [select id,JVCO_OpportunityCancelled__c,SBQQ__Contracted__c,SBQQ__Ordered__c, JVCO_Auto_Renewed__c  from Opportunity limit 5];
        SBQQ__QuoteLine__c qL = [Select id from SBQQ__QuoteLine__c ];

        q.JVCO_Quote_Auto_Renewed__c = true;
        q.Recalculate__c = true;
        update q;

        o.SBQQ__Contracted__c = TRUE;
        o.SBQQ__Ordered__c = false;
        o.JVCO_Auto_Renewed__c  = TRUE;
        update o;

        delete qL;

        system.assertEquals(TRUE, o.SBQQ__Contracted__c, 'Quote not Contracted');
        system.assertEquals(FALSE, o.SBQQ__Ordered__c, 'Opp is Ordered');
        system.assertEquals(False, o.JVCO_OpportunityCancelled__c, 'Opportunity cancelled');
        
        Test.startTest();

        Id batchId = database.executeBatch(new JVCO_OrderRenewalBatch(), 1);
        Test.stopTest();

    } 
}