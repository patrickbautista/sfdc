@isTest
private class JVCO_PopulateAccountExternalId_Test
{
	
	@testSetup
	static void setupTestData(){

		Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
            listQueue.add(q2);
            queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
            listQueue.add(q3);
            queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
            listQueue.add(q4);
            queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
            listQueue.add(q5);
            queuesobject q6 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankAccount__c'); 
            listQueue.add(q6);
            queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q7);
            queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatement__c'); 
            listQueue.add(q8);
            queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransaction__c'); 
            listQueue.add(q9);
            queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCreditNote__c'); 
            listQueue.add(q10);
            //queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntry__c'); 
            //listQueue.add(q9);
            //queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntryLineItem__c'); 
            //listQueue.add(q10);
            //queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatementLineItem__c'); 
            //listQueue.add(q10);
            //queuesobject q12 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatementLineItem__c'); 
            //listQueue.add(q12);           
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }

        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc10040 = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc10040.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc10040.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc10040.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc10040.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc10040.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc10040.c2g__ReportingCode__c = '10040';
        testGeneralLedgerAcc10040.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc10040.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc10040.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc10040.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc10040.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc10040.Dimension_1_Required__c = false;
        testGeneralLedgerAcc10040.Dimension_2_Required__c = false;
        testGeneralLedgerAcc10040.Dimension_3_Required__c = false;
        testGeneralLedgerAcc10040.Dimension_4_Required__c = false;
        testGeneralLedgerAcc10040.Name = '10040 - Accounts receivables control';
        testGeneralLedgerAcc10040.ownerid = testGroup.Id;
        insert testGeneralLedgerAcc10040;

        c2g__codaGeneralLedgerAccount__c testGeneralLedger = JVCO_TestClassObjectBuilder.createGeneralLedger();
        insert testGeneralLedger;

        c2g__codaTaxCode__c testTaxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedger.Id);
        insert testTaxCode;

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();

		Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 83202-C';
        a1.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc10040.id;
        a1.Type = 'Key Account';
        insert a1;

        //Create Contact
        Contact testContact = JVCO_TestClassObjectBuilder.createContact(a1.Id);
        testContact.Firstname = 'Test';
        testContact.LastName = 'TestContact';
        insert testContact;
        
        Account a2 = new Account();
        a2.RecordTypeId = licenceRT;
        a2.Name = 'Test Account 83202-L';

        a2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
        a2.JVCO_Customer_Account__c = a1.id;
        a2.c2g__CODAOutputVATCode__c = testTaxCode.id;
        a2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc10040.id;
        //a2.JVCO_Customer_Account__c = a1.Id;
        a2.JVCO_Preferred_Contact_Method__c = 'Email';
        a2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        a2.JVCO_Billing_Contact__c = testContact.id;
        a2.JVCO_Licence_Holder__c = testContact.id;
        a2.JVCO_Review_Contact__c = testContact.id;
        a2.AccountNumber = '00000007';
        a2.Type = 'Key Account';
        insert a2;
	}

	@isTest
	static void itShould()
	{
		test.startTest();

		JVCO_PopulateAccountExternalId obj = new JVCO_PopulateAccountExternalId();
		Database.executeBatch(obj);

		test.stopTest();

	}
}