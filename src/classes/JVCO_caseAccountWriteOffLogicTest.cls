@isTest
private class JVCO_caseAccountWriteOffLogicTest
{
    @testSetup static void createTestData() 
    {
        JVCO_TestClassHelper.createBillingConfig();        
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        //JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim2('PRS');
        insert JVCO_TestClassHelper.setDim2('VPL');
        Test.startTest();
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        p.Name = 'VPL Product';
        p.JVCO_Society__c = 'VPL';
        p.ProductCode = 'VPL Product';
        insert p;
        
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        //sq.JVCO_CreditReason__c = 'Credit to refund against PRS contract';
        q.JVCO_Quote_Id_to_Scheduler__c = q.id;
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;

        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        
        JVCO_Order_Group__c orderGroup2 = new JVCO_Order_Group__c();
        orderGroup2.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup2;
        
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        insert o;
        Order o2 = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o2.JVCO_Order_Group__c = orderGroup2.id;
        insert o2;
        
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        OrderItem oi2 = JVCO_TestClassHelper.setOrderItem(o2.Id, pbe.Id, ql.Id);
        oi2.UnitPrice = -100;
        insert oi2;
        
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_TestClassHelper.setGeneralSettingsCS();
        
        Set<Id> orderGroupIdSet = new Set<Id>();  
        orderGroupIdSet.add(orderGroup.Id);
        
        List<Order> orderList = new List<Order>();
        orderList.add(o2);
        
        JVCO_InvoiceAndCreditNoteManualLogic.setupInvoiceAndCreditNoteHeader(orderList);
        Test.stopTest();
        
        Database.executeBatch(new JVCO_OrderMultipleBillNowLogicBatch(orderGroupIdSet));
        insert JVCO_TestClassObjectBuilder.createSLAConstants();
    }
    
    @isTest
    static void testCaseAccWriteOff(){
        c2g__codaCompany__c testCompany = [SELECT id FROM c2g__codaCompany__c LIMIT 1]; 
        Account custAcc = [SELECT id, RecordType.Name FROM Account WHERE RecordType.Name = 'Customer Account' LIMIT 1];
        Account licAcc = [SELECT id, RecordType.Name FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];
        c2g__codaInvoice__c sInv = [SELECT id, Name FROM c2g__codaInvoice__c LIMIT 1];
        Test.startTest();
        //Create Cash Entry
        c2g__codaCashEntry__c testCashEntry = new c2g__codaCashEntry__c();
        testCashEntry.c2g__PaymentMethod__c = 'Direct Debit';
        testCashEntry.c2g__Account__c = licAcc.id;
        testCashEntry.c2g__OwnerCompany__c = testCompany.id;
        testCashEntry.ownerid = UserInfo.getUserId();
        testCashEntry.c2g__Status__c = 'In Progress';
        testCashEntry.c2g__Date__c = Date.today();
        testCashEntry.c2g__Value__c = 0.00;
        insert testCashEntry;

        List<c2g__codaInvoice__c> salesInvoiceList = [SELECT Id, Name, c2g__Transaction__c from c2g__codaInvoice__c];

        //Create Cash Entry Line Item
        c2g__codaCashEntryLineItem__c cashEntryLineItem = new c2g__codaCashEntryLineItem__c();
        cashEntryLineItem.c2g__CashEntry__c = testCashEntry.id;
        cashEntryLineItem.c2g__Account__c = licAcc.id;
        cashEntryLineItem.c2g__CashEntryValue__c = 100;
        cashEntryLineItem.c2g__LineNumber__c = 1;
        cashEntryLineItem.c2g__AccountReference__c = sInv.Name;
        cashEntryLineItem.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cashEntryLineItem.c2g__OwnerCompany__c = testCompany.id;
        insert cashEntryLineItem;
        
        c2g.CODAAPICommon_7_0.Context context = new c2g.CODAAPICommon_7_0.Context();
        List<c2g.CODAAPICommon.Reference> referenceList = new List<c2g.CODAAPICommon.Reference>();
        c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
        reference.Id = testCashEntry.Id;
        referenceList.add(reference);
        c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(context, referenceList);
        
        Entitlement e = JVCO_TestClassObjectBuilder.createEntitlement(licAcc.id);
        insert e;
        
        list<Case> caseList = new list<Case>();
        Case testCase = JVCO_TestClassObjectBuilder.createCase(e.id);
        testCase.Origin = 'Internal';
        testCase.RecordTypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Write Off').getRecordTypeId();
        testCase.Type = 'Write Off';
        testCase.AccountId = custAcc.id;
        testCase.ownerid = UserInfo.getUserId();
        insert testCase;  
        
        caseList.add(testCase);
        JVCO_caseAccountWriteOffLogic.updateCaseId(caseList);
        map<id, Case> mapCase = new map<id, Case>();
        mapCase.put(testCase.id, testCase);
        JVCO_caseAccountWriteOffLogic.updateCaseTotalWriteOffFields(mapCase);
        test.stopTest();
    }
    
}