public class TriggerHandler_DirectDebit_GroupInvoice {

    public static void OnBeforeInsert(List<SMP_DirectDebit_GroupInvoice__c> newRecords){
            
    }

    public static void OnAfterInsert(Map<Id, SMP_DirectDebit_GroupInvoice__c> newRecords){

       List<c2g__codaInvoice__c> invoicesforUpdate = new List<c2g__codaInvoice__c>();
       List<Income_Direct_Debit__c> ddToUpdateList = new List<Income_Direct_Debit__c>();
       
       List<SMP_DirectDebit_GroupInvoice__c> ddGroupInvoices = [SELECT Id, Income_Direct_Debit__c, Income_Direct_Debit__r.SalesInvoiceList__c, Invoice_Group__c, Source__c FROM SMP_DirectDebit_GroupInvoice__c WHERE Id =: newRecords.keySet()];
       for (SMP_DirectDebit_GroupInvoice__c groupInvoice : ddGroupInvoices) 
       {
           if(String.isNotBlank(groupInvoice.Income_Direct_Debit__r.SalesInvoiceList__c) && groupInvoice.Source__c == 'Web'){
                String[] split = groupInvoice.Income_Direct_Debit__r.SalesInvoiceList__c.split(',');

                for(Integer j = 0; j < split.size(); j++)
                {
                    c2g__codaInvoice__c invoice = new c2g__codaInvoice__c();

                    invoice.Id = split[j];
                    invoice.JVCO_Invoice_Group__c = groupInvoice.Invoice_Group__c;
                    invoice.JVCO_Payment_Method__c = 'Direct Debit';

                    invoicesforUpdate.add(invoice);

                }

            }
       }

       Map<Id, c2g__codaInvoice__c> invoiceMap = new Map<Id, c2g__codaInvoice__c>();
       for(c2g__codaInvoice__c inv :invoicesforUpdate){
           invoiceMap.put(inv.Id, inv);
       }

        update invoiceMap.values();
    }

    public static void OnBeforeUpdate(Map<Id, SMP_DirectDebit_GroupInvoice__c> newRecords, Map<Id, SMP_DirectDebit_GroupInvoice__c> oldRecords){

    }
	
    public static void OnAfterUpdate(Map<Id, SMP_DirectDebit_GroupInvoice__c> newRecords, Map<Id, SMP_DirectDebit_GroupInvoice__c> oldRecords){

        
    }
    
}