/* ----------------------------------------------------------------------------------------------
Name: JVCO_LinkPositiveInvoice.cls 
Description: Batch class that Link Positive Invoice    
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
11-Apr-2017  0.1         kristoffer.d.martin   Intial creation
23-Aug-2017  0.2         desiree.m.quijada     Added a list to avoid hitting the limits
04-Sept-2017 0.3         desiree.m.quijada     Catch dml exception error and create a record on the error log object
----------------------------------------------------------------------------------------------- */

global class JVCO_LinkPositiveInvoice implements Database.Batchable<sObject>, Database.Stateful {
    
    private static Map<String, ID> subscriptionIdMap;
    private static List<blng__Invoice__c> invoiceList;
    private static List<blng__Invoice__c> invoiceListWithTempLink;
    private static List<c2g__codaCreditNote__c> salesCreditNoteForUpdate;

    private static void init() {

        List<Id> salesCreditNoteIds = new List<Id>();
        salesCreditNoteForUpdate = new List<c2g__codaCreditNote__c>();
        List<String> legNumberList = new List<String>(); //23-Aug-2017 desiree.m.quijada
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>(); //04-Sept-2017 desiree.m.quijada

        if (!invoiceList.isEmpty())
        {
            for (blng__Invoice__c inv : invoiceList)
            {
                if (inv.JVCO_Sales_Credit_Note__r.JVCO_Temp_Link_Positive_Invoice__c != null)
                {
                    salesCreditNoteIds.add(inv.JVCO_Sales_Credit_Note__c);
                }
            }
        }

        List<c2g__codaCreditNote__c> salesCreditNoteList = [SELECT Id, Name, JVCO_Temp_Link_Positive_Invoice__c, c2g__Invoice__c
                                                            FROM c2g__codaCreditNote__c
                                                            WHERE Id in :salesCreditNoteIds];
        //23-Aug-2017                                                            
        for(c2g__codaCreditNote__c legNum : salesCreditNoteList){
            legNumberList.add(legNum.JVCO_Temp_Link_Positive_Invoice__c);
        }



        List<c2g__codaInvoice__c> codaInvoiceList = [SELECT Id, Name, JVCO_Invoice_Legacy_Number__c 
                                                     FROM c2g__codaInvoice__c
                                                     WHERE JVCO_Invoice_Legacy_Number__c != null
                                                     AND JVCO_Invoice_Legacy_Number__c IN :legNumberList];
        if (!codaInvoiceList.isEmpty())
        {
            for (c2g__codaInvoice__c cInvoice : codaInvoiceList)
            {
                if (!salesCreditNoteList.isEmpty())
                {
                    for (c2g__codaCreditNote__c creditNote : salesCreditNoteList)
                    {
                        if (creditNote.JVCO_Temp_Link_Positive_Invoice__c == cInvoice.JVCO_Invoice_Legacy_Number__c)
                        {
                            creditNote.c2g__Invoice__c = cInvoice.Id;
                            salesCreditNoteForUpdate.add(creditNote);
                        }
                    }
                }
            }
        }

        
            if (!salesCreditNoteForUpdate.isEmpty())
            {
                if(Test.isRunningTest()){
                    salesCreditNoteForUpdate[0].c2g__Account__c = null;
                }
                //update salesCreditNoteForUpdate;
                List<Database.SaveResult> saveResList = Database.update(salesCreditNoteForUpdate,false);

                //for every result, evaluate if is success or failed
                for(Integer i=0; i<salesCreditNoteForUpdate.size(); i++)
                {
                    Database.SaveResult indResult = saveResList[i];
                    c2g__codaCreditNote__c salesCredNote = salesCreditNoteForUpdate[i];
                    //for failed result, error will caught and logged to the error log object
                    if(!indResult.isSuccess())
                    {
                        for(Database.Error err : indResult.getErrors())
                        {
                            ffps_custRem__Custom_Log__c newErr =  new ffps_custRem__Custom_Log__c();
                              if(salesCredNote.ID != null || !String.isBlank(String.valueOf(salesCredNote.ID))){
                                if(err.getMessage() != null || !String.isBlank(String.valueOf(err.getMessage()))){
                                    if(err.getStatusCode() != null || !String.isBlank(String.valueOf(err.getStatusCode()))){
                                        
                                        newErr.ffps_custRem__Running_User__c = UserInfo.getUserId();
                                        newErr.ffps_custRem__Related_Object_Key__c= String.valueOf(salesCredNote.ID);
                                        newErr.ffps_custRem__Detail__c = String.valueof(err.getMessage());
                                        newErr.ffps_custRem__Message__c = 'Link Positive Invoice';
                                        newErr.ffps_custRem__Grouping__c = String.valueof(err.getStatusCode());
                                    } 
                                }
                                errLogList.add(newErr);
                              }
                        }
                    }
                    else{
                        System.debug('Successfully updated');
                    }
                }
                if(!errLogList.isEmpty())
                {
                   insert errLogList;
                }
            }
        
        
    }

    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id, Name, JVCO_Total_Amount__c, JVCO_Sales_Credit_Note__c, 
                                                JVCO_Sales_Credit_Note__r.JVCO_Temp_Link_Positive_Invoice__c
                                         FROM blng__Invoice__c
                                         WHERE JVCO_Total_Amount__c < 0]);

    }

    global void execute(Database.BatchableContext bc, List<blng__Invoice__c> scope)
    {
        invoiceList = scope;
        init();
                
    }

    global void finish(Database.BatchableContext BC) 
    {
        System.debug('DONE');
    
    }
    
}