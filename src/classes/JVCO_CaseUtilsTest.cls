@isTest
private class JVCO_CaseUtilsTest
{
	 @testSetup static void setUpTestData()
    {
        JVCO_SLA_Constants__c cs = JVCO_TestClassObjectBuilder.createSLAConstants();
        insert cs;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;

        SlaProcess ep = [SELECT Id FROM SlaProcess WHERE Name='standard support clone' order by CreatedDate DESC limit 1];

        Entitlement testEntitlement = JVCO_TestClassObjectBuilder.createEntitlement(acc2.id, ep.id);
        insert testEntitlement;
    }

	@isTest
	static void testMilestone()
	{
		Test.startTest();
        // Create Case records for every Case Origin with First Response SLA
        List<Case> cases = new List<Case>{
                                (new Case(Subject='Test--Email', Origin='Email')),
                                (new Case(Subject='Test--Webform', Origin='Webform')),
                                (new Case(Subject='Test--RC', Origin='Request Callback'))
                            };
        insert cases;

        List<Case> casesToTestList = [SELECT Id, Status FROM Case WHERE Subject LIKE 'Test--%'];
        String criteria = JVCO_CaseUtils.slaConstants.JVCO_First_Milestone_Criteria__c;
        List<Id> caseIdList = new List<Id>();
        for (Case c : casesToTestList)
        {
            caseIdList.add(c.Id);
            c.Status = criteria; // Responded
        }
        //update casesToTestList;

        String milestoneName = JVCO_CaseUtils.slaConstants.JVCO_First_Milestone__c + '%';

        // Retrieve CaseMilestone records of newly created Case records
        List<CaseMilestone> cmToTest = [SELECT CaseId, Case.Status, CompletionDate
                                        FROM CaseMilestone
                                        WHERE CaseMilestone.MilestoneType.Name LIKE :milestoneName
                                        AND CaseId IN :caseIdList];

        for (CaseMilestone cm : cmToTest)
        {
            cm.Case.Status = 'Responded';
            //System.assert(cm.CompletionDate != null);
            System.assertEquals(criteria, cm.Case.Status);
        }
        
        Test.stopTest();

	}
}