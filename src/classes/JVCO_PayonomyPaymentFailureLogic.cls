/* ----------------------------------------------------------------------------------------------
    Name: JVCO_PayonomyPaymentFailureLogic.cls 
    Description: Business logic class for <FOR UPDATE>

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    27-Dec-2016   0.1        kristoffer.d.martin Intial creation
    24-Oct-2017   0.2        franz.g.a.dimaapi   Refactor Class
    30-May-2018   0.3        mary.ann.a.ruelan   GREEN-32111 - Added error logs in DML statements
    16-Aug-2018   0.4        patrick.t.bautista  GREEN-32995 - Added DD Mandate Cancelation for Deliberative
    24-Oct-2018   0.5        mary.ann.a.ruelan   GREEN-33665 - Make error codes case insensitive
    09-Jan-2019   0.6        franz.g.a.dimaapi   GREEN-34214 - Refactor and Fix Deliberate for Installments
    18-Feb-2019   0.7        franz.g.a.dimaapi   GREEN-34334 - Refactor Class and Fix Deliberate Installment Logic
----------------------------------------------------------------------------------------------- */
public class JVCO_PayonomyPaymentFailureLogic
{
    /*private static final Map<String, JVCO_Payonomy_Error_Codes__c> PAYMENT_ERROR_CODE = JVCO_Payonomy_Error_Codes__c.getAll();
    private static final Map<String, JVCO_Payonomy_Error_Codes__c> errorCodesMap = new Map<String, JVCO_Payonomy_Error_Codes__c>();
    private static final Id ddRecordTypeId = Schema.SObjectType.PAYBASE2__Payment__c.getRecordTypeInfosByName().get('Direct Debit Collection').getRecordTypeId();

    //Allowable Payment Id for failure logic
    private static Set<Id> paymentIdSet = new Set<Id>();
    //Set of Cash Receipt Transaction - Use in getting Transaction Line List
    private static Set<Id> transIdSet = new Set<Id>();
    //Set of Account Reference in Cash Entry Line Item
    private static Set<String> accReferenceSet = new Set<String>();
    //Set of Account to DD Mandate in Payonomy Payments
    private static Set<Id> accToDDMandateSet = new Set<Id>();
    //PayonomyPaymentId to Refund Cash Map
    private static Map<Id, c2g__codaCashEntry__c> ppIdToRefundMap = new Map<Id, c2g__codaCashEntry__c>();
    //PayonomyPaymentId to Total Amount Map
    private static Map<Id, Double> ppIdToCLITotalAmtMap = new Map<Id, Double>();

    //All Account Reference Map
    private static Map<String, Id> acctReferenceToInvIdMap = new Map<String, Id>();
    private static Map<Id, List<c2g__codaTransactionLineItem__c>> accIdToReceiptTransLineListMap = new Map<Id, List<c2g__codaTransactionLineItem__c>>();
    private static Map<String, List<c2g__codaInvoiceInstallmentLineItem__c>> accRefToPaidInstallmentLineListMap = new Map<String, List<c2g__codaInvoiceInstallmentLineItem__c>>();
    private static Map<String, Date> accRefToCollectionDateMap = new Map<String, Date>();

    private static List<c2g__codaCashEntryLineItem__c> cashEntryLineItemList = new List<c2g__codaCashEntryLineItem__c>();
    //List of Receipt Cash Transaction Line - Use in getting Matching Histories
    private static List<c2g__codaTransactionLineItem__c> receiptTransLineList = new List<c2g__codaTransactionLineItem__c>();
    //List of Matching Histories for Receipt - Use in getting Matching Reference
    private static List<c2g__codaCashMatchingHistory__c> matchingHistList = new List<c2g__codaCashMatchingHistory__c>();
    
    //List of records to be updated
    private static List<c2g__codaCashEntryLineItem__c> updatedCashEntryLineItemList = new List<c2g__codaCashEntryLineItem__c>();
    private static Set<c2g__codaInvoiceInstallmentLineItem__c> updatedInvInstallmentSet = new Set<c2g__codaInvoiceInstallmentLineItem__c>();
    private static Set<PAYREC2__Payment_Agreement__c> updatedPymtAgreementSet = new Set<PAYREC2__Payment_Agreement__c>();
    private static Set<c2g__codaInvoice__c> sInvSet = new Set<c2g__codaInvoice__c>();

    /* ----------------------------------------------------------------------------------------------
        Author: kristoffer.d.martin
        Company: Accenture
        Description: 
        Inputs: List<PAYBASE2__Payment__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        27-Dec-2016 kristoffer.d.martin Initial version of function
        24-Oct-2017 patrick.t.bautista  Refactor Method
    ----------------------------------------------------------------------------------------------- */
    /*public static void processPaymentFailure(List<PAYBASE2__Payment__c> paymentList)
    {   
        setCapsErrorCodes();
        stopperMethod();
        setErrorDDPaymentIdSet(paymentList);
        if(!paymentIdSet.isEmpty()){
            setCashEntryLineMap();
            setInvoiceMap();
            setInvoiceInstallmentMap();
            updateReceiptCashEntryLineItemReason();
            setTransactionLineList();
            setMatchingHistList();
        }
        if(!matchingHistList.isEmpty())
        {   
            setInvoiceMap();
            JVCO_CashUnmatchingLogic.openPeriodId = JVCO_FFUtil.getOpenPeriodId();
            //Call FF Unmatch API
            for(c2g__codaCashMatchingHistory__c matchingHist : matchingHistList)
            {
                JVCO_CashUnmatchingLogic.undoCashMatching(matchingHist);
            }
            insertRefundCash();
        }
    }
    
    private static void setErrorDDPaymentIdSet(List<PAYBASE2__Payment__c> paymentList)
    {
        for(PAYBASE2__Payment__c payment : paymentList) 
        {
            if((payment.PAYBASE2__Status__c == 'Error' || payment.PAYBASE2__Status__c == 'Agent Exception') && 
                payment.RecordTypeId == ddRecordTypeId && payment.PAYBASE2__Status_Text__c != null &&
                payment.PAYREC2__Payment_Agreement__c != null)
            {
                paymentIdSet.add(payment.Id);
                createRefundCashEntry(payment);
            }
        }
    }
    
    private static void setCashEntryLineMap()
    {   
        for(c2g__codaCashEntryLineItem__c cli : [SELECT Id, JVCO_Payonomy_Payment__c,
                                                JVCO_Payonomy_Payment__r.PAYREC2__Payment_Agreement__r.PAYREC2__Payment_Schedule__c,
                                                JVCO_Payonomy_Payment__r.PAYREC2__Payment_Agreement__r.PAYREC2__Payment_Schedule__r.PAYREC2__Day__c,
                                                JVCO_Payonomy_Payment__r.PAYREC2__Payment_Agreement__c,
                                                JVCO_Payonomy_Payment__r.PAYBASE2__Status_Text__c,
                                                JVCO_Payonomy_Payment__r.PAYBASE2__Pay_Date__c,
                                                c2g__CashEntry__r.Name, c2g__CashEntryValue__c,
                                                c2g__CashEntry__r.c2g__Transaction__c,
                                                c2g__CashEntry__c,JVCO_Payment_Status__c,
                                                c2g__AccountReference__c,
                                                c2g__Account__c
                                                FROM c2g__codaCashEntryLineItem__c
                                                WHERE JVCO_Payonomy_Payment__c IN :paymentIdSet])
        {
            if(!ppIdToCLITotalAmtMap.containsKey(cli.JVCO_Payonomy_Payment__c))
            {
                ppIdToCLITotalAmtMap.put(cli.JVCO_Payonomy_Payment__c, 0);
            }
            //aggregated cash val = summation of all related payonomy payment cash entry lines
            Double aggCashVal = ppIdToCLITotalAmtMap.get(cli.JVCO_Payonomy_Payment__c) + cli.c2g__CashEntryValue__c;
            ppIdToCLITotalAmtMap.put(cli.JVCO_Payonomy_Payment__c, aggCashVal);
            
            if(!String.isblank(cli.c2g__CashEntry__r.c2g__Transaction__c))
            {
                transIdSet.add(cli.c2g__CashEntry__r.c2g__Transaction__c);
            }
            if(!String.isblank(cli.c2g__AccountReference__c))
            {
                accReferenceSet.add(cli.c2g__AccountReference__c);
            }
            
            cashEntryLineItemList.add(cli);
        }
    }

    private static void updateReceiptCashEntryLineItemReason()
    {
        for(c2g__codaCashEntryLineItem__c cli : cashEntryLineItemList)
        {   
            //Update all the Cash Entry Lines and add to List
            cli.JVCO_Payment_Status__c  = 'Failed';
            String errorCode = cli.JVCO_Payonomy_Payment__r.PAYBASE2__Status_Text__c.toLowerCase();
            if(errorCodesMap.containsKey(errorCode))
            {
                String deliberateCode = errorCodesMap.get(errorCode).JVCO_Deliberate__c;
                cli.JVCO_Payment_Failure_Reason__c = getPaymentFailureReason(deliberateCode);
                //Delete Installment Line and Cancel Payonomy Agreement
                setInstallmentAndAgreement(cli, deliberateCode);
            }
            updatedCashEntryLineItemList.add(cli);
        }
    }

    private static void setInvoiceInstallmentMap()
    {
        for(c2g__codaInvoiceInstallmentLineItem__c installmentLine : [SELECT Id, c2g__Invoice__r.Name,
                                                                        c2g__LineNumber__c, JVCO_Payment_Status__c
                                                                        FROM c2g__codaInvoiceInstallmentLineItem__c
                                                                        WHERE c2g__Invoice__r.Name IN :accReferenceSet
                                                                        ORDER BY c2g__LineNumber__c DESC])
        {
            if(installmentLine.JVCO_Payment_Status__c != 'Unpaid')
            {
                if(!accRefToPaidInstallmentLineListMap.containsKey(installmentLine.c2g__Invoice__r.Name))
                {
                    accRefToPaidInstallmentLineListMap.put(installmentLine.c2g__Invoice__r.Name, new List<c2g__codaInvoiceInstallmentLineItem__c>{});
                }
                accRefToPaidInstallmentLineListMap.get(installmentLine.c2g__Invoice__r.Name).add(installmentLine);
            }
        }
    }

    private static void setTransactionLineList()
    {
        for(c2g__codaTransactionLineItem__c tli : [SELECT Id, c2g__Transaction__c, c2g__LineReference__c,
                                                    c2g__AccountValue__c, c2g__Account__c,
                                                    c2g__AccountOutstandingValue__c
                                                    FROM c2g__codaTransactionLineItem__c
                                                    WHERE c2g__Transaction__c IN :transIdSet
                                                    AND c2g__LineReference__c IN :accReferenceSet
                                                    AND c2g__LineType__c = 'Account'])
        {
            if(!accIdToReceiptTransLineListMap.containsKey(tli.c2g__Account__c))
            {
                accIdToReceiptTransLineListMap.put(tli.c2g__Account__c, new List<c2g__codaTransactionLineItem__c>{});
            }
            accIdToReceiptTransLineListMap.get(tli.c2g__Account__c).add(tli);
            receiptTransLineList.add(tli);
        }
    }

    private static void setMatchingHistList()
    {
        matchingHistList = [SELECT Id, c2g__MatchingReference__c, c2g__TransactionLineItem__c,
                            c2g__Account__c, c2g__Period__c
                            FROM c2g__codaCashMatchingHistory__c
                            WHERE c2g__TransactionLineItem__c IN :receiptTransLineList
                            AND c2g__Action__c = 'Match'
                            AND c2g__UndoMatchingReference__c=NULL];                                                     
    }

    private static void createRefundCashEntry(PAYBASE2__Payment__c payment)
    {
        c2g__codaCashEntry__c refundCash = new c2g__codaCashEntry__c();
        refundCash.c2g__Account__c = payment.JVCO_AccountId__c;
        refundCash.c2g__Date__c = Date.today();
        refundCash.c2g__BankAccount__c = payment.JVCO_FF_Bank_Account_Name__c;
        refundCash.c2g__Description__c = payment.PAYCP2__Vendor_Transaction_Id__c;
        refundCash.JVCO_Payonomy_Payment__c = payment.Id;
        refundCash.ffcash__DerivePeriod__c = true;
        refundCash.c2g__Type__c = 'Refund';
        refundCash.JVCO_Receipt_reversal_failed__c = true;
        refundCash.c2g__PaymentMethod__c = 'Direct Debit';
        
        ppIdToRefundMap.put(payment.Id, refundCash);
    }

    private static void postAndMatchRefundCash()
    {
        c2g.CODAAPICommon_7_0.Context context = new c2g.CODAAPICommon_7_0.Context();
        List<c2g.CODAAPICommon.Reference> referenceList = new List<c2g.CODAAPICommon.Reference>();
        for (c2g__codaCashEntry__c cashRefund : ppIdToRefundMap.values())
        {
            c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = cashRefund.Id;
            referenceList.add(reference);
        }
        c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(context, referenceList);

        for(c2g__codaCashEntry__c cashRefund : ppIdToRefundMap.values())
        {
            if(accIdToReceiptTransLineListMap.containsKey(cashRefund.c2g__Account__c))
            {
                List<c2g__codaTransactionLineItem__c> transLineList = accIdToReceiptTransLineListMap.get(cashRefund.c2g__Account__c);
                if(!transLineList.isEmpty())
                {
                    Id jobId = System.enqueueJob(new JVCO_PayonomyMatchRefundQueueable(cashRefund, transLineList));
                }
            }
        }
    }

    @TestVisible
    private static void insertRefundCash()
    {   
        if(!ppIdToRefundMap.isEmpty())
        {
            try
            {
                for(Id ppId : ppIdToRefundMap.keySet())
                {
                    ppIdToRefundMap.get(ppId).c2g__NetValue__c = ppIdToCLITotalAmtMap.get(ppId);
                }
                insert ppIdToRefundMap.values();

                //Create Refund Line Item
                List<c2g__codaCashEntryLineItem__c> refundCashEntryLineItemList = new List<c2g__codaCashEntryLineItem__c>();
                for(c2g__codaCashEntry__c c : ppIdToRefundMap.values())
                {
                    c2g__codaCashEntryLineItem__c cashEntryLineItem = new c2g__codaCashEntryLineItem__c();
                    // Populate Cash Entry Line Item fields
                    cashEntryLineItem.c2g__CashEntry__c = c.Id;
                    cashEntryLineItem.c2g__Account__c = c.c2g__Account__c;
                    cashEntryLineItem.c2g__CashEntryValue__c = c.c2g__NetValue__c;
                    refundCashEntryLineItemList.add(cashEntryLineItem);
                }
                insert refundCashEntryLineItemList;

                update updatedCashEntryLineItemList;

                update new List<PAYREC2__Payment_Agreement__c>(updatedPymtAgreementSet);

                //Change latest Installment to Unpaid
                if(!updatedInvInstallmentSet.isEmpty())
                {
                    for(c2g__codaInvoiceInstallmentLineItem__c invInstallmentLine : updatedInvInstallmentSet)
                    {
                        invInstallmentLine.JVCO_Payment_Status__c = 'Unpaid';
                        invInstallmentLine.JVCO_Paid_Amount__c = 0;
                    }
                    update new List<c2g__codaInvoiceInstallmentLineItem__c>(updatedInvInstallmentSet);
                }
                update new List<c2g__codaInvoice__c>(sInvSet);
                cancelDDMandate();
                //Match Refund to Receipt Cash
                postAndMatchRefundCash();
            }catch(Exception e)
            {
                catchErrorLogs(e);
            }
        }
    }

    private static String getPaymentFailureReason(String deliberateCode)
    {
        String paymentFailureReason = '';
        //Deliberate
        if(deliberateCode == 'Deliberate')
        {   
            paymentFailureReason = 'Deliberative Failed Direct Debit Collection';
        //Non Deliberate
        }else if(deliberateCode == 'Non-Deliberate')
        {
            paymentFailureReason = 'Non-Deliberative Failed Direct Debit Collection';
        }

        return paymentFailureReason;
    }

    private static void setInstallmentAndAgreement(c2g__codaCashEntryLineItem__c cli, String deliberateCode)
    {   
        PAYREC2__Payment_Agreement__c pymtAgreement = new PAYREC2__Payment_Agreement__c();
        pymtAgreement.Id = cli.JVCO_Payonomy_Payment__r.PAYREC2__Payment_Agreement__c;
        
        String acctReference = cli.c2g__AccountReference__c;
        c2g__codaInvoice__c sInv = new c2g__codaInvoice__c();
        
        if(deliberateCode == 'Deliberate')
        {   
            pymtAgreement.PAYREC2__Status__c = 'Cancel';
            accToDDMandateSet.add(cli.c2g__Account__c);
            sInv.JVCO_Invoice_Group__c = null;
            
        }else
        {   
            pymtAgreement.PAYREC2__Status__c = 'First re-present';
            //Compute Collection Date
            String scheduleDay = cli.JVCO_Payonomy_Payment__r.PAYREC2__Payment_Agreement__r.PAYREC2__Payment_Schedule__r.PAYREC2__Day__c;
            Integer day = Integer.valueOf(scheduleDay);
            Date payDate = cli.JVCO_Payonomy_Payment__r.PAYBASE2__Pay_Date__c;
            Date newdate = Date.newInstance(payDate.year(), payDate.month(), day);
            if(payDate.day() >= day)
            {
                newdate = newdate.addMonths(1);
            }
            pymtAgreement.PAYREC2__Next_Collection__c = newdate;
            sInv.JVCO_skipDateVR__c = true;
            sInv.JVCO_Promise_to_Pay__c = newdate.addDays(1);
            sInv.JVCO_Stop_Surcharge__c = false;
        }

        if(acctReferenceToInvIdMap.containsKey(acctReference))
        {
            sInv.Id = acctReferenceToInvIdMap.get(acctReference);
            sInvSet.add(sInv);
        }
        
        if(accRefToPaidInstallmentLineListMap.containsKey(acctReference))
        {
            updatedInvInstallmentSet.add(accRefToPaidInstallmentLineListMap.get(acctReference)[0]);
        }
        updatedPymtAgreementSet.add(pymtAgreement);
    }

    @TestVisible
    private static void setInvoiceMap()
    {
        for(c2g__codaInvoice__c inv : [SELECT Id, Name 
                                        FROM c2g__codaInvoice__c 
                                        WHERE Name IN :accReferenceSet])
        {
            acctReferenceToInvIdMap.put(inv.Name, inv.Id);
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: mary.ann.a.ruelan
        Company: Accenture
        Description: Generate Company Vat Registration Number
        Inputs: Set<Id>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        30-May-2018 mary.ann.a.ruelan    GREEN-32111. Created to catch error logs
    ----------------------------------------------------------------------------------------------- */
    /*private static void catchErrorLogs(Exception e)
    {              
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
        errLog.ffps_custRem__Detail__c = string.valueof(e.getMessage());
        errLog.ffps_custRem__Message__c = 'Payonomy failure';
        errLog.ffps_custRem__Grouping__c = 'PP-001';
        
        errLogList.add(errLog); 
        insert errLogList;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: patrick.t.bautista
        Company: Accenture
        Description: Generate Company Vat Registration Number
        Inputs: Set<Id>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        7-June-2018 patrick.t.bautista  GREEN-29920 Automatic DD mandate Cancellation  
    ----------------------------------------------------------------------------------------------- */
    /*private static void cancelDDMandate()
    {
        Set<JVCO_DD_Mandate__c> cancelToDDSet = new Set<JVCO_DD_Mandate__c>();
        for(JVCO_DD_Mandate__c cancelDDMandate : [SELECT id, JVCO_Deactivated__c, JVCO_Licence_Account__c 
                                                  FROM JVCO_DD_Mandate__c 
                                                  WHERE JVCO_Licence_Account__c IN: accToDDMandateSet])
        {
            cancelDDMandate.JVCO_Deactivated__c = true;
            cancelToDDSet.add(cancelDDMandate);
        }
        update new List<JVCO_DD_Mandate__c>(cancelToDDSet);
    }
    
    private static void setCapsErrorCodes()
    {
        for(String errorCode : PAYMENT_ERROR_CODE.keySet())
        {
            errorCodesMap.put(errorCode.toLowerCase(), PAYMENT_ERROR_CODE.get(errorCode));            
        }        
    }

    private static void stopperMethod()
    {
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        JVCO_FFUtil.stopAccountDLRSHandler = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_DDMandateHandler.stopDDMandateHandlerBeforeUpdate = true;
    }*/
}