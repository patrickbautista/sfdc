@isTest
public class JVCO_DCA2StageBatchProcessTest {
    @testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim2('PRS');
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        JVCO_DCA_2nd_Stage_Automation_Controller__c DCAAutomation = new JVCO_DCA_2nd_Stage_Automation_Controller__c();
        DCAAutomation.Name = 'Test';
        DCAAutomation.JVCO_Incoming_DCA_Reason_Code_1st_Stage__c = '100 - Paid in Full';
        DCAAutomation.JVCO_Number_of_Days_Send_to_Letter__c = 0;
        DCAAutomation.JVCO_Number_of_Days_to_Start_DCA2ndStage__c = 0;
        insert DCAAutomation;
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
    }
    @isTest
    static void testManualTickDCA2ndstage10Days()
    {
        List<Account> acct = [Select id, JVCO_Incoming_Reason_Code__c,
                              ffps_accbal__Account_Balance__c,
                              JVCO_DCA_Send_to_Letter_Date__c,
                              JVCO_DCA_Send_to_Letter__c,
                              JVCO_DCA_Type_2nd_Stage__c,
                              JVCO_In_Enforcement__c,
                              JVCO_In_Infringement__c,
                              JVCO_Credit_Status__c,
                              RecordType.Name,
                              JVCO_DCA_Type__c
                              FROM Account 
                              WHERE RecordType.Name = 'Licence Account' 
                              limit 1
                             ];
        acct[0].JVCO_DCA_Send_to_Letter__c = TRUE;
        acct[0].JVCO_DCA_Type__c = 'CSL';
        acct[0].JVCO_Credit_Status__c = 'Pre DCA 2nd Stage';
        acct[0].ffps_accbal__Account_Balance__c = 1000;
        acct[0].Date_to_be_Passed_to_DCA_2nd_Stage__c = system.today();
        acct[0].JVCO_In_Enforcement__c = FALSE;
        acct[0].JVCO_In_Infringement__c = FALSE;
        acct[0].JVCO_DCA_Type_2nd_Stage__c = 'Oriel';
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        update acct;
        
        Test.startTest();
        Database.executeBatch(new JVCO_DCA2StageBatchProcess());
        Test.stopTest();
    }
    @isTest
    static void testSendLetter4days()
    {
        List<Account> acct = [Select id, JVCO_Incoming_Reason_Code__c,
                              ffps_accbal__Account_Balance__c,
                              DCA_2nd_Stage_Letter_to_be_Sent__c,
                              JVCO_In_Enforcement__c,
                              JVCO_In_Infringement__c,
                              RecordType.Name,
                              JVCO_Credit_Status__c,
                              JVCO_DCA_Type__c
                              FROM Account 
                              WHERE RecordType.Name = 'Licence Account' 
                              limit 1
                             ];
        acct[0].JVCO_DCA_Type__c = 'CSL';
        acct[0].JVCO_Credit_Status__c = 'Pre DCA 2nd Stage';
        acct[0].JVCO_Incoming_Status_to_DCA__c = 'Withdrawal';
        acct[0].JVCO_Incoming_Reason_Code__c = '100 - Paid in Full';
        acct[0].ffps_accbal__Account_Balance__c = 1000;
        acct[0].DCA_2nd_Stage_Letter_to_be_Sent__c = system.today();
        acct[0].JVCO_In_Enforcement__c = FALSE;
        acct[0].JVCO_In_Infringement__c = FALSE;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        update acct;
        
        Test.startTest();
        JVCO_DCA2StageBatchScheduler.CallExecuteMomentarily();
        Test.stopTest();
    }
    @isTest
    static void testErrorLogs()
    {
        JVCO_DCA2StageBatchProcess dcab = new JVCO_DCA2StageBatchProcess();
        dcab.createErrorLog('test', 'test', 'test');
    }
}