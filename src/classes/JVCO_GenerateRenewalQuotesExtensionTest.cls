/* ----------------------------------------------------------------------------------------------
    Name: JVCO_GenerateRenewalQuotesExtensionTest
    Description: Test Class for JVCO_GenerateRenewalQuotesExtension

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    03-Mar-2017     0.1         Marlon Ocillos      Intial creation
----------------------------------------------------------------------------------------------- */
@isTest
public class JVCO_GenerateRenewalQuotesExtensionTest {
	
    @testSetup static void setup() 
    {
        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        insert dt;
        
        Pricebook2 pb = JVCO_TestClassObjectBuilder.CreatePriceBook();
        insert pb;

        Product2 prod = JVCO_TestClassObjectBuilder.CreateProduct();
        insert prod;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.CreateQuoteProcess();
        insert qProcess;

        Account acc = JVCO_TestClassObjectBuilder.CreateCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.CreateLicenceAccount(acc.id);
        acc2.JVCO_Preferred_Contact_Method__c = 'Letter';
        acc2.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
        insert acc2;

        

        Opportunity opp = JVCO_TestClassObjectBuilder.CreateOpportunity(acc2);
        opp.CloseDate = Date.today().addDays(-5);
        insert opp;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.CreateVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.CreateAffiliation(acc2.id, ven.id);
        aff.JVCO_Closure_Reason__c = '';
        aff.JVCO_End_Date__c = null;
        insert aff;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.CreateQuote(acc2.id, qProcess.id, opp.id, null, null);
        insert q;
        
        SBQQ__QuoteLineGroup__c  qlg = new SBQQ__QuoteLineGroup__c();
        qlg.JVCO_Affiliated_Venue__c = aff.id;
        qlg.JVCO_IsComplete__c = true;    
        qlg.SBQQ__Quote__c = q.Id;
        Insert qlg;   
        
        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.CreateQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, prod.Id);
        insert ql;
        
        Test.startTest();
        Contract contr = new Contract();
        contr.AccountId = acc2.Id;
        contr.Status = 'Draft';
        contr.StartDate = Date.today();
        contr.ContractTerm = 12;
        contr.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
        contr.SBQQ__RenewalQuoted__c = FALSE;
        contr.SBQQ__Quote__c = q.id;
        contr.SBQQ__Opportunity__c = opp.id;
        contr.SBQQ__RenewalQuoted__c = false;
        contr.JVCO_Renewal_Generated__c = false;
        contr.JVCO_RenewableQuantity__c = 1;
        //contr.JVCO_Current_Period__c = true;
        contr.JVCO_Value__c = 1000;
        contr.EndDate = Date.today().addMonths(12);
        insert contr;

        SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(acc2.id, prod.id);
        s.Affiliation__c = aff.Id;
        s.JVCO_Venue__c = ven.Id;
        s.SBQQ__Contract__c = contr.Id;
        s.SBQQ__RenewalQuantity__c = 1;
        s.SBQQ__ListPrice__c = 1;
        s.SBQQ__NetPrice__c = 1;
        s.SBQQ__Quantity__c = 1;
        s.SBQQ__QuoteLine__c = ql.Id;
        s.End_Date__c = System.today().addMonths(12);
        s.Start_Date__c = System.today();
        s.SBQQ__Number__c = 1;
        insert s;

        contr.JVCO_RenewableQuantity__c = 1;
        contr.JVCO_Value__c = 1000;
        update contr;

        JVCO_KABatchSetting__c kaBS = JVCO_KABatchSetting__c.getOrgDefaults();
        kaBS.JVCO_ContractRenewQuotesQueueable__c = false;
        kaBS.JVCO_ContractReviewQuotesQueueable__c = false;
        kaBS.JVCO_GenerateAmendmentQuotesQueueable__c = false;
        kaBS.JVCO_GenerateRenewalQuotesQueueable__c = false;
        upsert kaBS JVCO_KABatchSetting__c.Id;

        Test.stopTest();
    }
    
    static testMethod void testForGenerateRenewalQuotes()
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
		
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        JVCO_GenerateRenewalQuotesExtension ctrl = new JVCO_GenerateRenewalQuotesExtension(sc);
        
        Test.startTest();
        ctrl.queueGenerateRenewalQuotes();
        ctrl.backToRecord();
        Test.stopTest();

        //List<SBQQ__Quote__c> generatedRenewalQuoteList = [SELECT Id FROM SBQQ__Quote__c WHERE ];
    }
    
    static testMethod void testForGenerateRenewalQuotes2()
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
		
        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        asCS.JVCO_Renew_Batch_Size__c = 2;
        upsert asCS JVCO_Array_Size__c.Id;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        JVCO_GenerateRenewalQuotesExtension ctrl = new JVCO_GenerateRenewalQuotesExtension(sc);
        
        Test.startTest();
        ctrl.queueGenerateRenewalQuotes();
        ctrl.backToRecord();
        Test.stopTest();
    }

    static testMethod void testForGenerateRenewalQuotesQueueable()
    {
        JVCO_KABatchSetting__c kaBS = JVCO_KABatchSetting__c.getOrgDefaults();
        kaBS.JVCO_ContractRenewQuotesQueueable__c = true;
        kaBS.JVCO_ContractReviewQuotesQueueable__c = true;
        kaBS.JVCO_GenerateAmendmentQuotesQueueable__c = true;
        kaBS.JVCO_GenerateRenewalQuotesQueueable__c = true;
        upsert kaBS JVCO_KABatchSetting__c.Id;

        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [select Id, Type from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];

        a.Type = 'Key Account';
        update a;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        JVCO_GenerateRenewalQuotesExtension ctrl = new JVCO_GenerateRenewalQuotesExtension(sc);
        
        Test.startTest();
        ctrl.queueGenerateRenewalQuotes();
        ctrl.backToRecord();
        Test.stopTest();
    }

    static testMethod void testForGenerateRenewalQuotesKeyAccount()
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

        Account accList = [Select Id, Type from Account where RecordTypeId = :licenceRT];

        accList.Type = 'Key Account';
        update accList;
        
        Account a = [select Id from Account where id =: accList.id order by CreatedDate DESC limit 1];
        
        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        asCS.JVCO_Renew_Batch_Size__c = 2;
        upsert asCS JVCO_Array_Size__c.Id;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        JVCO_GenerateRenewalQuotesExtension ctrl = new JVCO_GenerateRenewalQuotesExtension(sc);
        
        Test.startTest();
        ctrl.queueGenerateRenewalQuotes();
        ctrl.backToRecord();
        Test.stopTest();
    }
}