/* ----------------------------------------------------------------------------------------------
    Name: JVCO_salesRepStamp
    Description: Batch Class for updating Stamped Sales Rep fields in Sales Invoice and Sales Credit Note

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    14-Aug-2018     0.1         john.patrick.valdez      Intial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_salesRepStampedBatch /*implements Database.Batchable<sObject>*/{

    /*private id bInvoice;
    public JVCO_salesRepStampedBatch(){}
    public JVCO_salesRepStampedBatch(id bInvoice)
    {

        this.bInvoice = bInvoice;

    }
    public Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator([SELECT Id, CreatedDate, JVCO_Sales_Invoice__c,
                                        JVCO_Sales_Invoice__r.JVCO_Sales_Rep__c,
                                        JVCO_Sales_Invoice__r.SalesRep_Department__c, 
                                        JVCO_Sales_Invoice__r.SalesRep_Role__c, 
                                        JVCO_Sales_Invoice__r.SalesRep_Profile__c, 
                                        JVCO_Sales_Invoice__r.SalesRep_Manager__c,
										JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Department__c,
                                        JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Role__c,
                                        JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Profile__c,
                                        JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Manager__c,
                                        JVCO_Sales_Invoice__r.JVCO_Sales_Rep__r.Department,
                                        JVCO_Sales_Invoice__r.JVCO_Sales_Rep__r.UserRole.Name,
                                        JVCO_Sales_Invoice__r.JVCO_Sales_Rep__r.Profile.Name,
                                        JVCO_Sales_Invoice__r.JVCO_Sales_Rep__r.Manager.Name, 
                                        JVCO_Sales_Credit_Note__c, 
                                        JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__c,
                                        JVCO_Sales_Credit_Note__r.SalesRep_Department__c, 
                                        JVCO_Sales_Credit_Note__r.SalesRep_Role__c, 
                                        JVCO_Sales_Credit_Note__r.SalesRep_Profile__c, 
                                        JVCO_Sales_Credit_Note__r.SalesRep_Manager__c,
                                        JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Department__c,
                                        JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Role__c,
                                        JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Profile__c,
                                        JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Manager__c,
                                        JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__r.Department,
                                        JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__r.UserRole.Name,
                                        JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__r.Profile.Name,
                                        JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__r.Manager.Name
                                         FROM blng__Invoice__c
                                         WHERE 
                                        (JVCO_Sales_Invoice__c != NULL AND ((JVCO_Sales_Invoice__r.SalesRep_Department__c = NULL AND JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Department__c != NULL)
                                         OR 
                                        (JVCO_Sales_Invoice__r.SalesRep_Role__c = NULL AND JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Role__c != NULL) 
                                        OR
                                        (JVCO_Sales_Invoice__r.SalesRep_Profile__c = NULL AND JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Profile__c != NULL) 
                                        OR
                                        (JVCO_Sales_Invoice__r.SalesRep_Manager__c = NULL AND JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Manager__c != ' ')))
                                         OR
                                        (JVCO_Sales_Credit_Note__c != NULL AND ((JVCO_Sales_Credit_Note__r.SalesRep_Department__c = '' AND JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Department__c != NULL) 
                                         OR 
                                        (JVCO_Sales_Credit_Note__r.SalesRep_Role__c = NULL AND JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Role__c != NULL) 
                                        OR
                                        (JVCO_Sales_Credit_Note__r.SalesRep_Profile__c = NULL AND JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Profile__c != NULL) 
                                        OR
                                        (JVCO_Sales_Credit_Note__r.SalesRep_Manager__c = NULL AND JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Manager__c != ' ')))
                                         ]);
     }
    public void execute(Database.BatchableContext BC, List<blng__Invoice__c> scope)
    {
        Map<Id, c2g__codaInvoice__c> updatedSInvMap = new Map<Id, c2g__codaInvoice__c>();
        Map<Id, c2g__codaCreditNote__c> updatedSCNoteMap = new Map<Id, c2g__codaCreditNote__c>();
        
        for(blng__Invoice__c bInv : scope)
        {
            //Billing Invoice with Sales Invoice
            if(bInv.JVCO_Sales_Invoice__c != null && bInv.JVCO_Sales_Credit_Note__c == null)
            {
                c2g__codaInvoice__c newInvoice = new c2g__codaInvoice__c();
                newInvoice.Id = bInv.JVCO_Sales_Invoice__r.id;
                if (bInv.JVCO_Sales_Invoice__r.SalesRep_Department__c == NULL && bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Department__c != NULL)
                {
                    newInvoice.SalesRep_Department__c = bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep__r.Department;
                }     
                if (bInv.JVCO_Sales_Invoice__r.SalesRep_Role__c == NULL && bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Role__c != NULL)
                {
                    newInvoice.SalesRep_Role__c = bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep__r.UserRole.Name;
                }
                if (bInv.JVCO_Sales_Invoice__r.SalesRep_Profile__c == NULL && bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Profile__c != NULL)
                {	
                    newInvoice.SalesRep_Profile__c = bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep__r.Profile.Name;
                }
                if (bInv.JVCO_Sales_Invoice__r.SalesRep_Manager__c == NULL && bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Manager__c != ' ')
                {
                    newInvoice.SalesRep_Manager__c = bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep__r.Manager.Name; 
                }
                updatedSInvMap.put(newInvoice.Id, newInvoice);
            }
            //Billing Invoice with Sales Credit Note
            if(bInv.JVCO_Sales_Credit_Note__c != null && bInv.JVCO_Sales_Invoice__c == null)
            {
                c2g__codaCreditNote__c newCreditNote = new c2g__codaCreditNote__c();
                newCreditNote.Id = bInv.JVCO_Sales_Credit_Note__r.id;
                if (bInv.JVCO_Sales_Credit_Note__r.SalesRep_Department__c == NULL && bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Department__c != NULL)
                {
                    newCreditNote.SalesRep_Department__c = bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__r.Department;
                }     
                if (bInv.JVCO_Sales_Credit_Note__r.SalesRep_Role__c == NULL && bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Role__c != NULL)
                {
                    newCreditNote.SalesRep_Role__c = bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__r.UserRole.Name;
                }
                if (bInv.JVCO_Sales_Credit_Note__r.SalesRep_Profile__c == NULL && bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Profile__c != NULL)
                {	
                    newCreditNote.SalesRep_Profile__c = bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__r.Profile.Name;
                }
                if (bInv.JVCO_Sales_Credit_Note__r.SalesRep_Manager__c == NULL && bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Manager__c !=  ' ')
                {
                    newCreditNote.SalesRep_Manager__c = bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__r.Manager.Name;  
                }
                updatedSCnoteMap.put(newCreditNote.Id, newCreditNote);
            }

            //Billing Invoice with Sales Invoice and Sales Credit Note
            if(bInv.JVCO_Sales_Invoice__c != null && bInv.JVCO_Sales_Credit_Note__c != null)
            {
                c2g__codaInvoice__c newInvoice = new c2g__codaInvoice__c();
                newInvoice.Id = bInv.JVCO_Sales_Invoice__r.id;
                if (bInv.JVCO_Sales_Invoice__r.SalesRep_Department__c == NULL && bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Department__c != NULL)
                {
                    newInvoice.SalesRep_Department__c = bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep__r.Department;
                }     
                if (bInv.JVCO_Sales_Invoice__r.SalesRep_Role__c == NULL && bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Role__c != NULL)
                {
                    newInvoice.SalesRep_Role__c = bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep__r.UserRole.Name;
                }
                if (bInv.JVCO_Sales_Invoice__r.SalesRep_Profile__c == NULL && bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Profile__c != NULL)
                {	
                    newInvoice.SalesRep_Profile__c = bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep__r.Profile.Name;
                }
                if (bInv.JVCO_Sales_Invoice__r.SalesRep_Manager__c == NULL && bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep_Manager__c != ' ')
                {
                    newInvoice.SalesRep_Manager__c = bInv.JVCO_Sales_Invoice__r.JVCO_Sales_Rep__r.Manager.Name; 
                }
                updatedSInvMap.put(newInvoice.Id, newInvoice);
                
                c2g__codaCreditNote__c newCreditNote = new c2g__codaCreditNote__c();
                newCreditNote.Id = bInv.JVCO_Sales_Credit_Note__r.id;
                if (bInv.JVCO_Sales_Credit_Note__r.SalesRep_Department__c == NULL && bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Department__c != NULL)
                {
                    newCreditNote.SalesRep_Department__c = bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__r.Department;
                }     
                if (bInv.JVCO_Sales_Credit_Note__r.SalesRep_Role__c == NULL && bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Role__c != NULL)
                {
                    newCreditNote.SalesRep_Role__c = bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__r.UserRole.Name;
                }
                if (bInv.JVCO_Sales_Credit_Note__r.SalesRep_Profile__c == NULL && bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Profile__c != NULL)
                {	
                    newCreditNote.SalesRep_Profile__c = bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__r.Profile.Name;
                }
                if (bInv.JVCO_Sales_Credit_Note__r.SalesRep_Manager__c == NULL && bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep_Manager__c != ' ')
                {
                    newCreditNote.SalesRep_Manager__c = bInv.JVCO_Sales_Credit_Note__r.JVCO_Sales_Rep__r.Manager.Name;  
                }
                updatedSCnoteMap.put(newCreditNote.Id, newCreditNote);  
            }
        }
        if(!updatedSInvMap.isEmpty())
        {
            update updatedSInvMap.values();
        }
        if(!updatedSCNoteMap.isEmpty())
        {
            update updatedSCNoteMap.values();
        }
    }
    public void finish(Database.BatchableContext BC){}*/
}