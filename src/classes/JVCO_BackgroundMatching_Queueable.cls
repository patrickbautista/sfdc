/* ------------------------------------------------------------------------------------------------
   Name:            JVCO_OverdueInstalment_Batch.cls 
   Description:     Batch Class for handler JVCO_OverdueInstalmentHandler

   Date            Version     Author                   Summary of Changes 
   -----------     -------     -----------------        ---------------------------------------
   09-Jul-2018     0.1         franz.g.a.dimaapi        GREEN-32622 - Improve Performance Issue
------------------------------------------------------------------------------------------------ */
public class JVCO_BackgroundMatching_Queueable implements Queueable
{
    private Map<Id, c2g__codaTransactionLineItem__c> transLineMap;
    private Map<Id, c2g__codaTransactionLineItem__c> tliToNextQueueMap;
    
    public JVCO_BackgroundMatching_Queueable(Map<Id, c2g__codaTransactionLineItem__c> transLineMap)
    {
        this.transLineMap = transLineMap;
        this.tliToNextQueueMap = transLineMap;
    }
    
    public JVCO_BackgroundMatching_Queueable(Map<Id, c2g__codaTransactionLineItem__c> transLineMap, Map<Id, c2g__codaTransactionLineItem__c> tliToNextQueueMap)
    {
        this.transLineMap = transLineMap;
        this.tliToNextQueueMap = tliToNextQueueMap;
    }

    public void execute(QueueableContext context) 
    {
        Map<Id, c2g__codaTransactionLineItem__c> tempTliToUpdateMap = new Map<Id, c2g__codaTransactionLineItem__c>();
        Map<id, c2g__codaTransactionLineItem__c> tempTliNextQueueMap = new Map<Id, c2g__codaTransactionLineItem__c>();
        
        if(!transLineMap.isEmpty())
        {
            Boolean isFirst = true;
            //Construct Single Map using First Record
            for(c2g__codaTransactionLineItem__c transLine : transLineMap.values()){
                if(isFirst){
                    isFirst = false;
                    tempTliToUpdateMap.put(transLine.Id, transLine);
                }else{
                    tempTliNextQueueMap.put(transLine.Id, transLine);
                }
            }
            //Background Matching Logic
            JVCO_BackgroundMatchingLogic.matchingLogic(tempTliToUpdateMap, false);
        }
        system.debug('tliToNextQueueMap'+tliToNextQueueMap);
        //Recursion until the map is empty
        if(!tempTliNextQueueMap.isEmpty() && !Test.isRunningTest())
        {
            System.enqueueJob(new JVCO_BackgroundMatching_Queueable(tempTliNextQueueMap, tliToNextQueueMap));
        }else if(!Test.isRunningTest()){
            System.enqueueJob(new JVCO_UpdateSalesInvoiceQueueable(tliToNextQueueMap));
        }
    }
}