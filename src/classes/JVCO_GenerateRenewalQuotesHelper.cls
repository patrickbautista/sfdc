/* ------------------------------------------------------------------------------------------------
   Name:            JVCO_GenerateRenewalQuotesHelper
   Description:     Helper class for JVCO_GenerateRenewalQuotesBatch and JVCO_GenerateRenewalQuotes_Queueable

   Date            Version     Author                       Summary of Changes 
   -----------     -------     -----------------            ---------------------------------------
   09-Jul-2018     0.1         jules.osberg.a.pablo         Initial Version
   18-Aug-2018     0.2         mel.andrei.b.santos          GREEN-32273 - Set SBQQ__AmendmentRenewalBehavior__c  to Earliest End Date if Key Accounts
------------------------------------------------------------------------------------------------ */
public class JVCO_GenerateRenewalQuotesHelper {
    @testVisible
    private static Boolean testError = false;

    public JVCO_GenerateRenewalQuotesHelper() {
        
    }

    public static string getQueryString(Account accountRecord) {
        //String query = 'select Id, EndDate, SBQQ__RenewalQuoted__c, ForceDeferred__c from Contract where EndDate > TODAY and AccountId = ' + '\'' + accountRecord.Id + '\'';
        //String query = 'select Id, EndDate, SBQQ__RenewalQuoted__c, ForceDeferred__c, JVCO_Active__c from Contract where JVCO_Active__c = true and AccountId = ' + '\'' + accountRecord.Id + '\'';
        String query = 'select Id, EndDate, ForceDeferred__c, Account.Type, SBQQ__AmendmentRenewalBehavior__c, SBQQ__RenewalQuoted__c, SBQQ__Opportunity__c from Contract where SBQQ__RenewalQuoted__c = false and JVCO_Renewal_Generated__c = false and JVCO_RenewableQuantity__c > 0 and AccountId = ' + '\'' + accountRecord.Id + '\'';   
        return query;
    }

    public static string executeRenewalQuotes(List<Contract> processContractList, Account accountRecord) {
        List<Contract> contractListToUpdate = new List<Contract> ();
        String returnErrorMessage = '';

        //START 18-08-2018 mel.andrei.b.santos GREEN-32273 - Set SBQQ__AmendmentRenewalBehavior__c  to Earliest End Date if Key Accounts
        List<Contract> conUpdate = new List<Contract>();
        //END

        //START 03-Apr-2018 GREEN-30531 louis.a.del.rosario
        Set<Id> setIdContract = new Set<Id>();
        Set<Id> setIdProcessContract = new Set<Id>();

        //for(Contract c:(List<Contract>) scope){
        for(Contract c: processContractList){
            setIdContract.add(c.Id);

            //START 18-08-2018 mel.andrei.b.santos GREEN-32273 - Set SBQQ__AmendmentRenewalBehavior__c  to Earliest End Date if Key Accounts
            if(c.Account.type == 'Key Account')
            {
                system.debug('Test if Key Accounts  ' + c.Account.Type);
                c.SBQQ__AmendmentRenewalBehavior__c = 'Earliest End Date';
                conUpdate.add(c);
            } 
            //END
        }

        //START 18-08-2018 mel.andrei.b.santos GREEN-32273 - Set SBQQ__AmendmentRenewalBehavior__c  to Earliest End Date if Key Accounts
        if(!conUpdate.isEmpty())
        {
            update conUpdate;
        }
        //END

        Map<Id,SBQQ__Subscription__c> mapIdSubscription = new Map<Id,SBQQ__Subscription__c>([SELECT Id,SBQQ__RenewalQuantity__c,SBQQ__Contract__c,SBQQ__Contract__r.JVCO_Contract_EndDate__c,Affiliation__c,Affiliation__r.JVCO_End_Date__c FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c =: setIdContract]);

        for(SBQQ__Subscription__c s:mapIdSubscription.values()){
            if(s.Affiliation__r.JVCO_End_Date__c == null){
                setIdProcessContract.add(s.SBQQ__Contract__c);
            }else if(s.SBQQ__Contract__r.JVCO_Contract_EndDate__c > s.Affiliation__r.JVCO_End_Date__c){
                setIdProcessContract.add(s.SBQQ__Contract__c);
            }
        }

        Map<Id,Contract> mapIdContract = new Map<Id,Contract>([select Id, EndDate, ForceDeferred__c from Contract where SBQQ__RenewalQuoted__c = false and JVCO_Renewal_Generated__c = false and JVCO_RenewableQuantity__c > 0 and AccountId =: accountRecord.Id and Id =: setIdProcessContract]);

        System.debug('ContractMap: ' + mapIdContract.values()); 
        System.debug('ContractMapSize: ' + mapIdContract.size()); 

        for (Contract contractRecord : mapIdContract.values())
        {
            //contractIds.add(contractRecord.Id); //02-Nov-2017 GREEN-25156 jules.osberg.a.pablo

            // contractRecord.SBQQ__RenewalQuoted__c = true;
            // contractRecord.ForceDeferred__c = 1;
            // contractListToUpdate.add(contractRecord);

            if(System.isQueueable()){

                RenewalContracts rc = new RenewalContracts();
                
                List<Contract> contractsList = new List<Contract>();
                contractsList.add(contractRecord);

                rc.masterContractId = contractRecord.Id;
                rc.renewedContracts = contractsList;

                SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractRenewer', rc.masterContractId, JSON.serialize(rc)); 
            }
            else{

                contractRecord.SBQQ__RenewalQuoted__c = true;
                contractRecord.ForceDeferred__c = 1;
                contractListToUpdate.add(contractRecord);
            }
        }
        //END 03-Apr-2018 GREEN-30531 louis.a.del.rosario
        try{
            
            Database.update(contractListToUpdate);

            if(Test.isRunningTest() && testError){ 
                 Database.update(new Opportunity());                             
            }

            if(System.isQueueable()){

                JVCO_KATempHolder__c kaTempHolder = new JVCO_KATempHolder__c();
                kaTempHolder.Name = processContractList[0].Id;
                kaTempHolder.JVCO_AccountId__c = accountRecord.Id;
                kaTempHolder.JVCO_OppId__c = processContractList[0].SBQQ__Opportunity__c;
                kaTempHolder.JVCO_ContId__c = processContractList[0].Id;
                kaTempHolder.JVCO_GeneratedRenewal__c = true;

                upsert kaTempHolder Name;
            }
        } 
        catch(Exception e) {

            String ErrorMessage = 'Cause: ' + e.getCause() + ' ErrMsg: ' + e.getMessage();
            returnErrorMessage += '<br>Line Number: ' + e.getLineNumber() != Null ? String.ValueOf(e.getLineNumber()):'None' + ' ' + ErrorMessage;
            
            if(System.isQueueable()){

                JVCO_KATempHolder__c kaTempHolder = new JVCO_KATempHolder__c();
                kaTempHolder.Name = processContractList[0].Id;
                kaTempHolder.JVCO_AccountId__c = accountRecord.Id;
                kaTempHolder.JVCO_OppId__c = processContractList[0].SBQQ__Opportunity__c;
                kaTempHolder.JVCO_ContId__c = processContractList[0].Id;
                kaTempHolder.JVCO_GeneratedRenewal__c = false;

                upsert kaTempHolder Name;
            }

            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
            //errLog.Name = String.valueOf(processContractList[0].Id);
            errLog.ffps_custRem__Related_Object_Key__c = processContractList[0].Id;
            errLog.ffps_custRem__Detail__c = String.valueOf(e.getMessage());
            errLog.ffps_custRem__Grouping__c = String.valueOf(e.getCause()) != null && String.valueOf(e.getCause()) != '' ? String.valueOf(e.getCause()) : 'N/A';
            errLog.ffps_custRem__Message__c = 'JVCO_GenerateRenewalQuote';

            insert errLog;
        }
            
        return returnErrorMessage;
    }
    
    private class RenewalContracts{

        public Id masterContractId; 
        public Contract[] renewedContracts; 
    }
    
    //Method for sending Email of Started Job
    public static void sendBeginJobEmail(Account accountRecord, Integer sizeOfJob, Boolean queueableProcess){

        //User currentUser = [SELECT Id, Email FROM USER WHERE Id = :UserInfo.getUserId() LIMIT 1];

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(UserInfo.getUserId());
        mail.setSaveAsActivity(false);

        mail.setReplyTo('no-reply@salesforce.com');
        if(queueableProcess){
            mail.setSenderDisplayName('Queueable Processing');
            mail.setSubject('Generate Renewal Quotes Queueable Process Started');
        }else{
            mail.setSenderDisplayName('Batch Processing');
            mail.setSubject('Generate Renewal Quotes Batch Process Started');
        }
        
        String emailMsg = '<html><body>This is to notify you that the process to Generate Renewal Quotes has now been started for ' + accountRecord.Name + ' ('+ System.now() + '). <br><br>Number of Renew Quotes to be Contracted: ' + sizeOfJob + ' records.';
        emailMsg += ' </body></html> ';

        mail.setHTMLBody(emailMsg);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}