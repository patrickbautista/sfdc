@isTest
public class JVCO_PayonomyPaymentAgreementHandlerTest
{

    @testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        JVCO_Payonomy_Error_Codes__c payErrCode = new JVCO_Payonomy_Error_Codes__c();
        payErrCode.name = 'AUDDIS 1';
        payErrCode.JVCO_Deliberate__c = 'Deliberate';
        payErrCode.JVCO_Description__c = 'Error object in testClass';
        payErrCode.JVCO_Cancel_DD_Mandate__c = true;
        insert payErrCode;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        insert glaList;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim2('PRS');
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Test.startTest();
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        insert JVCO_TestClassHelper.setQuoteProcess();
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        Test.stopTest();

        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        JVCO_TestClassHelper.setGeneralSettingsCS();
        JVCO_TestClassHelper.setCashMatchingCS();

        blng__InvoiceRun__c invRun = new blng__InvoiceRun__c();
        invrun.blng__InvoiceDate__c = Date.today();
        invrun.blng__TargetDate__c = Date.today();
        insert invRun;

        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        bInv.blng__InvoiceRunCreatedBy__c = invRun.Id;
        insert bInv;
    }
    
    @isTest
    static void testPayonomyPaymentAgreementHandler()
    {   
        //before Insert && after Insert
        blng__Invoice__c bInv = [SELECT Id, blng__Account__c FROM blng__Invoice__c LIMIT 1];
        Id payBankId = JVCO_TestClassHelper.getPayBank();
        Id ddMandateId = JVCO_TestClassHelper.getDDMandate(bInv.blng__Account__c);
        Id paySchedId = JVCO_TestClassHelper.getPaySched(payBankId);
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        OrderItem oi = [SELECT Id FROM OrderItem LIMIT 1];  
        insert JVCO_TestClassHelper.setBInvLine(bInv.Id, p.Id, oi.Id, 0, false);
        blng__InvoiceRun__c bInvRun = [SELECT Id FROM blng__InvoiceRun__c LIMIT 1];
        Test.startTest();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        bInvRun.blng__Status__c = 'Completed';
        SBQQ.TriggerControl.disable();
        update bInvRun;
        Test.stopTest();
        c2g__codaInvoice__c invoice = [SELECT Id, Name FROM c2g__codaInvoice__c LIMIT 1];
        
        JVCO_Invoice_Group__c invGrp = new JVCO_Invoice_Group__c();
        invGrp.JVCO_Total_Amount__c = 400;
        invGrp.CC_SINs__c = invoice.Name + ',';
        insert invGrp;
        
        JVCO_Invoice_Group__c invGrpQuery = [SELECT Id, JVCO_Total_Amount__c, Name, CC_SINs__c FROM JVCO_Invoice_Group__c LIMIT 1];
        
        PAYREC2__Payment_Agreement__c agreement = new PAYREC2__Payment_Agreement__c();
        agreement.PAYREC2__Account__c = bInv.blng__Account__c;
        agreement.PAYREC2__Ongoing_Collection_Amount__c = 100.0;
        agreement.PAYREC2__Description__c = invGrpQuery.Name;
        agreement.PAYFISH3__Current_Bank_Account__c = payBankId;
        agreement.PAYFISH3__Payee_Agreed__c = true;
        agreement.PAYREC2__Payment_Schedule__c = paySchedId;
        agreement.PAYREC2__Status__c = 'New Instruction';
        agreement.RecordTypeId = Schema.SObjectType.PAYREC2__Payment_Agreement__c.getRecordTypeInfosByName().get('Direct Debit').getRecordTypeId();
        insert agreement;
        
        //before Update
        agreement.PAYREC2__Status__c = 'On Going';
        Id payBankId2 = JVCO_TestClassHelper.getPayBank('test direct debit2','11111111','000002');
        agreement.PAYFISH3__Current_Bank_Account__c = payBankId2;
        update agreement;
        agreement.PAYREC2__Status__c = 'New Instruction';
        update agreement;

        //after Update
        agreement.PAYREC2__Status__c = 'Errored';
        agreement.JVCO_Status_Message__c = 'AUDDIS:1:hehe:hotdog';
        update agreement;

        //before Delete
        JVCO_PayonomyPaymentAgreementHandler.stopAgreementDeletion = true;
        delete agreement;
    }

}