@isTest
private class JVCO_OrderProductTriggerTest
{
    @testSetup static void setupTestData() 
    {
        JVCO_TestClassObjectBuilder.createBillingConfig();
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        Product2 prod1 = new Product2();
        prod1.Name = 'Test Product 83202-1';
        prod1.ProductCode = 'TP 83202-1';
        prod1.SBQQ__ChargeType__c= 'One-Time';
        insert prod1;

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 83202-C';
        a1.Type = 'Key Account';
        insert a1;
        
        Account a2 = JVCO_TestClassOBjectBuilder.createLicenceAccount(a1.id);
        a2.RecordTypeId = licenceRT;
        a2.Name = 'Test Account 83202-License';
        a2.JVCO_Customer_Account__c = a1.Id;
        insert a2;
        
        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 83202';
        o.AccountId = a2.Id;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o; 

        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Renewal';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        insert q;

        Contract contr = new Contract();
        contr.AccountId = a2.Id;
        contr.Status = 'Draft';
        contr.StartDate = Date.today();
        contr.ContractTerm = 12;
        contr.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
        contr.SBQQ__RenewalQuoted__c = FALSE;
        contr.SBQQ__Quote__c = q.id;
        contr.SBQQ__Opportunity__c = o.id;
        insert contr;

        /*Order testOrder = new Order();
        testorder.Accountid = a2.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = q.id;
        testOrder.blng__BillingAccount__c = a2.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        insert testOrder;*/
        Test.startTest();
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        ql.SBQQ__ChargeType__c= 'One-Time';
        insert ql;
        
        JVCO_Venue__c  venueRecord = JVCO_TestClassOBjectBuilder.createVenue();
        venueRecord.Name = 'test1';              
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = new JVCO_Affiliation__c();
        affilRecord.JVCO_Account__c = a2.Id;
        affilRecord.JVCO_Venue__c = venueRecord.id;   
        affilRecord.JVCO_Start_Date__c = system.today();  
        Insert  affilRecord;  

        PriceBookEntry testPB = new PriceBookEntry();
        testPB.Pricebook2Id = test.getStandardPriceBookId();
        testPB.product2id = prod1.id;
        testPB.IsActive = true;
        testPB.UnitPrice = 100.0;
        insert testPB; 
        
        /*SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;*/

       /*PriceBookEntry testPB = new PriceBookEntry();
        testPB.Pricebook2Id = test.getStandardPriceBookId();
        testPB.product2id = prod1.id;
        testPB.IsActive = true;
        testPB.UnitPrice = 100.0;
        insert testPB;

        OpportunityLineItem testOppLineItem = new OpportunityLineItem();
        testOppLineItem.OpportunityId = o.id;
        testOppLineItem.Quantity = 1.0;
        testOppLineItem.UnitPrice = 100;
        testOppLineItem.PricebookEntryId = testPB.id;
        insert testOppLineItem;*/

       /* o.SBQQ__Contracted__c = true;
        o.SBQQ__Ordered__c = true;

        update o;*/
        Test.stopTest();
    }

    @isTest
    static void OrderProductTest()
    {
        Test.startTest();

        Account testAccount = [select id from account limit 1];
        system.debug('TestAccount' + testAccount);

        Product2 testProd = [select id from product2 limit 1];

        Opportunity testOpp = [select id, StageName, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity limit 1];

        SBQQ__Quote__c testQuote = [select id, SBQQ__NetAmount__c, Recalculate__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c limit 1];
        
        SBQQ__QuoteLine__c ql = [select id, 
                                    SBQQ__SpecialPriceType__c, 
                                    SBQQ__ListPrice__c, 
                                    SBQQ__CustomerPrice__c,
                                    SBQQ__NetPrice__c,
                                    SBQQ__SpecialPrice__c,
                                    SBQQ__RegularPrice__c,
                                    SBQQ__UnitCost__c,
                                    SBQQ__ProratedListPrice__c
                                 from SBQQ__QuoteLine__c limit 1];

        Order testOrder = new Order();
        testorder.Accountid = testAccount.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAccount.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        insert testOrder;  



        PriceBookEntry testPB = [select id from PriceBookEntry limit 1];

        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = 100.00;
        testOrderItem.EndDate = date.today().addMonths(5);
        insert testOrderItem;

        

        Order selectOrder = [select id from Order where id = :testOrder.id];

        //system.assertEquals(testOrder.id,selectOrder.Id, 'ERROR');

                
        OrderItem selectOrderItem = [select id, blng__OverrideNextBillingDate__c,   blng__BillThroughDateOverride__c,
            blng__NextBillingDate__c, 
            blng__NextChargeDate__c from OrderItem limit 1 ];
        //system.assertEquals('0', testOrderItem.id,'Error in id');
        //System.assertEquals(date.today(),selectOrderItem.blng__OverrideNextBillingDate__c,'Error in blng__OverrideNextBillingDate__c');
        //System.assertEquals(testOrderItem.EndDate, selectOrderItem.blng__BillThroughDateOverride__c,'Error in blng__BillThroughDateOverride__c');
        System.assertEquals(testOrderItem.JVCO_Start_Date__c, selectOrderItem.blng__NextBillingDate__c);
        System.assertEquals(testOrderItem.JVCO_Start_Date__c, selectOrderItem.blng__NextChargeDate__c);
        Test.stopTest();

        /*Test.startTest();

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(sc);
        actOrder.activate();

        test.stoptest();*/  
    }

    @isTest
    static void OrderActivate()
    {

        Account testAccount = [select id from account limit 1];
        system.debug('TestAccount' + testAccount);

        Product2 testProd = [select id from product2 limit 1];

        Opportunity testOpp = [select id, StageName, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity limit 1];

        SBQQ__Quote__c testQuote = [select id, SBQQ__NetAmount__c, Recalculate__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c limit 1];
        
        SBQQ__QuoteLine__c ql = [select id, 
                                    SBQQ__SpecialPriceType__c, 
                                    SBQQ__ListPrice__c, 
                                    SBQQ__CustomerPrice__c,
                                    SBQQ__NetPrice__c,
                                    SBQQ__SpecialPrice__c,
                                    SBQQ__RegularPrice__c,
                                    SBQQ__UnitCost__c,
                                    SBQQ__ProratedListPrice__c
                                 from SBQQ__QuoteLine__c limit 1];

        Order testOrder = new Order();
        testorder.Accountid = testAccount.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAccount.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        insert testOrder;  



        PriceBookEntry testPB = [select id from PriceBookEntry limit 1];

        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = 100.00;
        testOrderItem.EndDate = date.today().addMonths(5);

        Order selectOrder = [select id from Order where id = :testOrder.id];

        //system.assertEquals(testOrder.id,selectOrder.Id, 'ERROR');

       // Test.startTest();
        insert testOrderItem;

        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
        testOrderItem.JVCO_Percentage_Paid__c = 1;
        testOrderItem.JVCO_Paid_Amount__c = 100;
        update testOrderItem;
        //OrderItem selectOrderItem = [select id, blng__OverrideNextBillingDate__c, blng__BillThroughDateOverride__c from OrderItem limit 1 ];
        //system.assertEquals('0', testOrderItem.id,'Error in id');
        //system.assertEquals(date.today(),testOrderItem.blng__OverrideNextBillingDate__c,'Error in blng__OverrideNextBillingDate__c');
        //system.assertEquals(testOrderItem.EndDate, testOrderItem.blng__BillThroughDateOverride__c,'Error in blng__BillThroughDateOverride__c');

        //Test.stopTest();

        List<Order> listOrder = new list<Order>();
        listOrder.add(testOrder);

        Test.startTest();

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(sc);
        actOrder.activate();

        test.stoptest();
    }
}