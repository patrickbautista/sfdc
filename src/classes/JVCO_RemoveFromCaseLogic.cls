/* -----------------------------------------------------------------------------------------------
   Name: JVCO_RemoveFromCaseLogic.cls 
   Description: Business logic class for removing cash entry lines from Case

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   --------------------------------------------------
   02-Feb-2021   0.1         Luke.Walker      Intial creation
   18-Feb-2021	 o.2		 Luke.Walker		GREEN-36320 - Added update to Case to recalculate amounts
   ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_RemoveFromCaseLogic 
{
  	private ApexPages.StandardSetController stdCtrl;
    private List<c2g__codaCashEntryLineItem__c> selectedCashLines;
    private id caseID;
    public Integer numOfSelectedCashLines {get; set;}
    public final String NO_RECORDS_ERR = System.Label.JVCO_NoCashLines;

    //constructor
    public JVCO_RemoveFromCaseLogic(ApexPages.StandardSetController ssc){
      stdCtrl = ssc;
        selectedCashLines = (List<c2g__codaCashEntryLineItem__c>)ssc.getSelected();
        numOfSelectedCashLines = selectedCashLines.size();
        caseID = ApexPages.currentPage().getParameters().get('id');
    }
    
    public PageReference remove()
    {  
        List<c2g__codaCashEntryLineItem__c> cashList = new List<c2g__codaCashEntryLineItem__c>();
        Case caseRecord;
        
        if(numOfSelectedCashLines > 0){
            cashList = [SELECT ID, JVCO_Case_Id__c FROM c2g__codaCashEntryLineItem__c WHERE ID IN :selectedCashLines];
            caseRecord = [SELECT ID FROM Case WHERE ID = :caseID];

            List<c2g__codaCashEntryLineItem__c> updateCashList = new List<c2g__codaCashEntryLineItem__c>();
            
            for(c2g__codaCashEntryLineItem__c c: cashList){
                c.JVCO_Case_Id__c = NULL;
                c.JVCO_Write_Off_Status__c = NULL;
                updateCashList.add(c);
            }
            
            if(updateCashList.size() > 0){
                update updateCashList;
                update caseRecord;
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, NO_RECORDS_ERR));
            return null;
        }
        
        String uiThemeDisplayed = UserInfo.getUiThemeDisplayed();
        PageReference pageRef = new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
        
        //Create URL for Lightning or Classic
        if(uiThemeDisplayed == 'Theme4d' || uiThemeDisplayed == 'Theme4u'){
            aura.redirect(pageRef);
      return pageRef;
        }
        pageRef.setRedirect(true);    
        return pageRef;
    }
    
    public PageReference returnToCase()
    {
        PageReference page = new PageReference(stdCtrl.cancel().getUrl());
        page.setRedirect(TRUE);
        return page;
    }
    
}