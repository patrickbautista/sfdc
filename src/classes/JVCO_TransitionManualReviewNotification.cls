/**
* Created by: Accenture
* Created Date: 05.09.2017
* Description: APEX Batch class to update ‘Send NB/Review Document' and ‘Is Renew Contract’ check boxes 
*
*  Change Description 
*  
*  Enterprise ID                 Date                   Desc
*  Ashok                         14\11\2017             GREEN-25802 - Changes made to fix the issue.
*  mel.andrei.b.santos           28/02/2018             GREEN-30663 - Added Limit based on custom settings
**/
global class JVCO_TransitionManualReviewNotification  implements Database.batchable<Sobject>,Database.AllowsCallouts, Schedulable, Database.Stateful{

    public Id accId;    
    public Set<Id> accIdSet = null;
    public Map<Id, Id> licAndCustAccIdMap = new Map<Id, Id>();
    /**
    * This method retrieves retail and current contract records
    */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String soqlLimit  = null;
        //Start mel andrei santos 28/02/2018 Added Limit based on custom settings
        /*
        List<JVCO_PPL_PPRS_App_Config__mdt>  appConfigLst = [Select DeveloperName,MasterLabel, Value__c from JVCO_PPL_PPRS_App_Config__mdt where  DeveloperName = :'Transition_Manual_Review_Query_Limit'];
        if(appConfigLst.size() > 0)
        {
            soqlLimt = appConfigLst.get(0).Value__c;        
        }
        */

         Map<String, JVCO_TransitionManualReviewNotifSettings__c> settings = JVCO_TransitionManualReviewNotifSettings__c.getAll();

        //end mel andrei santos 28/02/2018

        RecordType recType = [Select id from RecordType where DeveloperName = 'JVCO_Licence_Account'];
        String query= 'Select JVCO_Sent_Manual_Review_Notifi_Email__c from Account where recordtypeID = \'' + recType.ID+ '\' and JVCO_Send_Email_Manual_Review_Notifi__c = true  ';
        // Add the soql limit to the SOQL
      
        if(!String.isBlank(String.valueOf(accId)))
        {
            query += ' and id = \''+accId+'\' ';
        }
        
        if(accIdSet != null)
        {
            query += ' and id  in :accIdSet ';
        }
                      
        query= query + ' order by JVCO_Review_Date__c ';     

        //Start mel andrei santos 28/02/2018 Added Limit based on custom settings

        /*
        if(soqlLimt != null)
        {
            query = query + ' Limit ' + soqlLimt;
        }*/

        soqlLimit = string.valueof(settings.get('Limit').Limit__c); 

        query = query + ' Limit ' + soqlLimit;

        //end mel andrei santos

        //query  = query +  ' and  id = \'0017E00000cnvHf\'';
        system.debug('query--' + query)  ;  
        return Database.getQueryLocator(query);
    }
    // adhoc testing for single id
    global JVCO_TransitionManualReviewNotification(Id aId)
    {
        accId = aId;
    }
    //constructors for adhoc testing
    global JVCO_TransitionManualReviewNotification(){}

    // adhoc testing for multiple ids
    global JVCO_TransitionManualReviewNotification(Set<Id> idSet)
    {
        accIdSet = new Set<Id>();
        if(idSet!= null)
        {
            accIdSet= idSet;
        }               
    }

    
    /**
    * This method executes the retrieved contract records
    */
    global void execute(SchedulableContext sc)
    {
        Map<String, JVCO_TransitionManualReviewNotifSettings__c> settingsbatchsize = JVCO_TransitionManualReviewNotifSettings__c.getAll();
        
        Decimal batchsize = settingsbatchsize.get('Limit').Batch_Size__c; 

        if(batchsize != null)
        {
            JVCO_TransitionManualReviewNotification  batch = new JVCO_TransitionManualReviewNotification();
            Database.executeBatch(batch, Integer.valueof(batchsize) );
        }
        else
        {
            JVCO_TransitionManualReviewNotification  batch = new JVCO_TransitionManualReviewNotification();
            Database.executeBatch(batch, 50 );
        }
    }
    
    /**
    * This method executes the retrieved contract records
    */
    global void execute(Database.BatchableContext BC, List<Sobject> scope)
    {
        System.debug('scope: '+ scope);
        List<Account> accLst = new List<Account>();
        for(Sobject sobj: scope)
        {
            Account accObj  = (Account)sobj;
            accObj.JVCO_Sent_Manual_Review_Notifi_Email__c  = true;   
            accLst.add(accObj);   
        }
        try
        {
             List<Database.SaveResult> uResults = Database.update(accLst,false);
             String errMessage = '';
          /*   for (Database.SaveResult sr : uResults ) 
             {
                if (sr.isSuccess()) 
                {
                    // Operation was successful, so get the ID of the record that was processed
                  //  System.debug('Successfully inserted account. Account ID: ' + sr.getId());
                }
                else 
                {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) 
                    {
                      //  System.debug('The following error has occurred.');                    
                      //  System.debug(err.getStatusCode() + ': ' + err.getMessage());
                     //   System.debug('Account fields that affected this error: ' + err.getFields());
                    }
                }
            }  */              
        }
        catch(Exception e)
        {
            System.debug('Error: ' + e);
        }
    }    
    /**
    * This method will execute after all batched are processed
    */
    global void finish(Database.BatchableContext BC)
    {
        if(!licAndCustAccIdMap.isEmpty() && !Test.isRunningTest()){
            System.enqueueJob(new JVCO_UpdateSalesInvoiceQueueable(licAndCustAccIdMap));
        }
    }
}