/* ----------------------------------------------------------------------------------------------
Name: JVCO_DCA2StageBatchProcess
Description: Batch Class for updating Account for DCA Passed to 2nd Stage

Date            Version     Author              Summary of Changes 
-----------     -------     -----------------   -----------------------------------------
12-Apr-2018     0.1         patrick.t.bautista      Intial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_DCA2StageBatchProcess  implements Database.Batchable<sObject>{
    
    public JVCO_DCA2StageBatchProcess(){}
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([SELECT id, 
                                         ffps_accbal__Account_Balance__c,
                                         DCA_2nd_Stage_Letter_to_be_Sent__c,
                                         JVCO_DCA_Send_to_Letter__c,
                                         Date_to_be_Passed_to_DCA_2nd_Stage__c,
                                         JVCO_Manual_Passed_to_DCA_2nd_Stage__c,
                                         JVCO_DCA_Send_to_Letter_Date__c
                                         FROM Account
                                         WHERE ffps_accbal__Account_Balance__c > 0
                                         AND JVCO_Credit_Status__c = 'Pre DCA 2nd Stage'
                                         AND JVCO_Manual_Passed_to_DCA_2nd_Stage__c = false
                                         AND JVCO_Passed_to_DCA_Date_2nd_Stage__c = null
                                         AND (JVCO_Promise_to_Pay__c <= today OR JVCO_Promise_to_Pay__c = null)
                                         AND JVCO_In_Enforcement__c = false
                                         AND JVCO_In_Infringement__c = false
                                         AND Pause_DCA_2nd_Stage__c = false
                                         AND Id NOT IN (SELECT c2g__Account__c 
                                                        FROM c2g__codaInvoice__c 
                                                        WHERE ffps_custRem__Exclude_From_Reminder_Process__c = true 
                                                        AND c2g__PaymentStatus__c IN ('Unpaid', 'Part Paid'))
                                         AND ((JVCO_DCA_Send_to_Letter__c = false AND DCA_2nd_Stage_Letter_to_be_Sent__c != null)
                                              OR (JVCO_DCA_Send_to_Letter__c = true AND Date_to_be_Passed_to_DCA_2nd_Stage__c != null))
                                        ]);
    }
    public void execute(Database.BatchableContext BC, List<Account > scope)
    {
        Set<Account> accToUpdateSet = new Set<Account>();
        JVCO_DCA_2nd_Stage_Automation_Controller__c dca2ndStgCont = [select id, JVCO_Incoming_DCA_Reason_Code_1st_Stage__c,
                                                                     JVCO_Number_of_Days_Send_to_Letter__c,
                                                                     JVCO_Number_of_Days_to_Start_DCA2ndStage__c
                                                                     FROM JVCO_DCA_2nd_Stage_Automation_Controller__c
                                                                     LIMIT 1];
        
        for(Account accToProcess: scope)
        {
            //DCA Send to Letter process after 4 days
            if(accToProcess.JVCO_DCA_Send_to_Letter__c == false && accToProcess.DCA_2nd_Stage_Letter_to_be_Sent__c <= system.today())
            {
                //For the PB to trigger
                accToProcess.JVCO_DCA_Send_to_Letter__c = true;
                accToProcess.Date_to_be_Passed_to_DCA_2nd_Stage__c = system.today() + Integer.valueOf(dca2ndStgCont.JVCO_Number_of_Days_to_Start_DCA2ndStage__c);
            }
            //DCA Send to DCA 2nd Stage process 10 days part
            else if(accToProcess.JVCO_DCA_Send_to_Letter__c == true
                    && accToProcess.Date_to_be_Passed_to_DCA_2nd_Stage__c <= system.today())
            {
                accToProcess.JVCO_Manual_Passed_to_DCA_2nd_Stage__c = true;
            }
            accToUpdateSet.add(accToProcess);
        }
        
        if(!accToUpdateSet.isEmpty())
        {
            try{
                update new List<Account>(accToUpdateSet);
            }
            catch(Exception e){
                insert createErrorLog('DCA', e.getMessage(), 'DCA 2nd Stage Batch');
            }
        }
    }
    @TestVisible
    private ffps_custRem__Custom_Log__c createErrorLog(String errCode, String errMessage, String batchName)
    {
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
        errLog.ffps_custRem__Detail__c = string.valueof(errMessage);
        errLog.ffps_custRem__Message__c = batchName;
        errLog.ffps_custRem__Grouping__c = errCode;
        return errLog;
    }
    public void finish(Database.BatchableContext BC){}
}