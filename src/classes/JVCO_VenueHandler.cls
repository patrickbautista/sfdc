/* ----------------------------------------------------------------------------------------------
   Name: JVCO_LeadHandler.cls 
   Description: Handler class for Venue object
   
   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
   06-Sep-2016   0.1        mark.glenn.b.ayson  Intial creation
   03-Apr-2017   0.2        samuel.a.d.oberes   added updatePrimaryTariffIfTypeChanged method that runs in the before-insert and before-update DML context
   04-May-2020   0.3        rhys.j.c.dela.cruz  GREEN-35591 - added filter in updatePrimaryTariffIfTypeChanged method to only query Draft Quotes
   ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_VenueHandler 
{

  public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
  public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_VenueTrigger__c : false;

  public static void onBeforeUpdate(List<JVCO_Venue__c> venueList, Map<id, JVCO_Venue__c> oldVenueMap) {
    if(!skipTrigger) 
        {
          updateBudgetCategory(venueList);
          updateVenueOwnerID(venueList , oldVenueMap);
          updatePrimaryTariffIfTypeChanged(venueList , oldVenueMap);
        }
  }
  public static void onBeforeInsert(List<JVCO_Venue__c> venueList, Map<id, JVCO_Venue__c> oldVenueMap) {
    if(!skipTrigger) 
        {
          updateBudgetCategory(venueList);
          updateVenueOwnerID(venueList);
          updatePrimaryTariffIfTypeChanged(venueList, null);
        }
  }
  public static void onAfterInsert(List<JVCO_Venue__c> venueList, Map<id, JVCO_Venue__c> oldVenueMap) {
    if(!skipTrigger) 
        {
          createTaskAfterInsert(venueList);
        }
  }
      /* ------------------------------------------------------------------------------------------
       Author: kristoffer.d.martin
       Company: Accenture
       Description: This method will call the JVCO_AffiliationMerge
       Input: List<JVCO_Venue__c>
       Returns: void
       <Date>      <Authors Name>      <Brief Description of Change> 
       02-Jun-2017 kristoffer.d.martin Initial version of function
       ----------------------------------------------------------------------------------------------- */
  public static void onAfterDelete(Map<Id,JVCO_Venue__c> oldVenueMap)
  {
    if(!skipTrigger) 
    {
        
      
      List<Id> venueIdList = new List<Id>();

      for (JVCO_Venue__c ven : oldVenueMap.values()) 
      {
          venueIdList.add(ven.JVCO_MasterRecordId__c);
      }
      
      if(!System.isBatch() && !System.isFuture()){
          JVCO_AffiliationMerge.mergeVenueAffiliation(venueIdList);
      }
    }
  }

    /* ------------------------------------------------------------------------------------------
       Author: mark.glenn.b.ayson
       Company: Accenture
       Description: This method handles beforeUpdate and beforeInsert trigger events for venue
       Input: List<JVCO_Venue__c>
       Returns: void
       <Date>      <Authors Name>      <Brief Description of Change> 
       07-Sep-2016 mark.glenn.b.ayson  Initial version of function
       ----------------------------------------------------------------------------------------------- */
    public static void updateVenueOwnerID(List<JVCO_Venue__c> newValue, Map<Id, JVCO_Venue__c> oldValue)
    {
        List<JVCO_Venue__c> venueRecordList = new List<JVCO_Venue__c>();

        // Check if the postcode or field visit checkbox is changed to trigger the handler
        for(JVCO_Venue__c venueRecord : newValue)
        {
            // Get the values from the old map
            JVCO_Venue__c oldVenue = oldValue.get(venueRecord.Id);

            if(!String.isBlank(venueRecord.JVCO_Postcode__c) && venueRecord.JVCO_Field_Visit_Requested__c == true)
            {
                // Check if either one of the fields has changed
                if(!venueRecord.JVCO_Postcode__c.equals(oldVenue.JVCO_Postcode__c) || !(venueRecord.JVCO_Field_Visit_Requested__c == oldVenue.JVCO_Field_Visit_Requested__c))
                {
                    // If either changes, fire the trigger handler
                    System.debug('There is a change.');
                    venueRecordList.add(venueRecord);
                }
            }
        }

        if(venueRecordList.size() > 0)
        {
            // This will load the lead handler method (List<Lead>, Boolean if the trigger is an update)
            JVCO_VenueAssignFieldAgentsPostCodeLogic.updateVenueOwnerID(venueRecordList, true);
        }
    } 
  

    /* ------------------------------------------------------------------------------------------
       Author: mark.glenn.b.ayson
       Company: Accenture
       Description: This method handles beforeUpdate and beforeInsert trigger events for venue
       Input: List<JVCO_Venue__c>
       Returns: void
       <Date>      <Authors Name>      <Brief Description of Change> 
       21-Sep-2016 mark.glenn.b.ayson  Initial version of function
       ----------------------------------------------------------------------------------------------- */
    public static void updateVenueOwnerID(List<JVCO_Venue__c> newValue)
    {
        List<JVCO_Venue__c> venueRecordList = new List<JVCO_Venue__c>();
        
        for(JVCO_Venue__c venueRecord : newValue)
        {
            if(!String.isBlank(venueRecord.JVCO_Postcode__c) && venueRecord.JVCO_Field_Visit_Requested__c == true)
            {
                venueRecordList.add(venueRecord);
            }
        }
        
        if(venueRecordList.size() > 0)
        {
            // This will load the lead handler method (List<Lead>, Boolean if the trigger is an update)
            JVCO_VenueAssignFieldAgentsPostCodeLogic.updateVenueOwnerID(venueRecordList, false);
        }
    }
  

    /* ------------------------------------------------------------------------------------------
       Author: mark.glenn.b.ayson
       Company: Accenture
       Description: This method handles beforeUpdate and beforeInsert trigger events for Lead
       Input: List<JVCO_Venue__c>
       Returns: void
       <Date>      <Authors Name>      <Brief Description of Change> 
       21-Sep-2016 mark.glenn.b.ayson  Initial version of function
       ----------------------------------------------------------------------------------------------- */
    public static void createTaskAfterInsert(List<JVCO_Venue__c> newValue)
    {
        // Create a task after the record is inserted
        List<JVCO_AssignFieldAgentWrapper> taskRecordList = new List<JVCO_AssignFieldAgentWrapper>();
        JVCO_AssignFieldAgentWrapper wrapper = new JVCO_AssignFieldAgentWrapper(); 

        // Get the detail of the new record
        for(JVCO_Venue__c venue : newValue)
        {
            if(venue.JVCO_Field_Visit_Requested__c == true && !String.isBlank(venue.JVCO_Postcode__c))
            {

                wrapper = new JVCO_AssignFieldAgentWrapper(); 
                // Add the venue name
                wrapper.venueName = venue.Name;

                // Check if the owner ID is a queue or not, if it is a queue, save the current user else, save the owner ID
                if(venue.ownerID != JVCO_AssignFieldAgentsPostCodeUtil.queueID)
                {
                    wrapper.ownerID = venue.ownerID;
                }
                else
                {
                    wrapper.ownerID = UserInfo.getUserId();
                }

                // Add the record ID
                wrapper.recordID = venue.Id;

                // Add to list
                taskRecordList.add(wrapper);
            }
        }

        if(taskRecordList.size() > 0)
        {
            // Call the create task methid in the util class (List<String>, a boolean value if the originating object is a Lead)
            JVCO_AssignFieldAgentsPostCodeUtil.createNewTask(taskRecordList, false);
        }
    }

    /* ------------------------------------------------------------------------------------------
       Author:      samuel.a.d.oberes
       Company:     Accenture
       Description: Stamps the JVCO_Primary_Tariff__c from JVCO_Primary_Tariff_Mapping__c custom setting. This happens in the before-update and before-insert context.
       Input:       List<JVCO_Venue__c>, Map<Id, JVCO_Venue__c>
       Returns:     void
       <Date>       <Authors Name>      <Brief Description of Change> 
       03-Apr-2017  samuel.a.d.oberes   Initial version
       04-May-2020  rhys.j.c.dela.cruz  GREEN-35591 - added filter in updatePrimaryTariffIfTypeChanged method to only query Draft Quotes
       ----------------------------------------------------------------------------------------------- */
    public static void updatePrimaryTariffIfTypeChanged(List<JVCO_Venue__c> newVenuesList, Map<Id, JVCO_Venue__c> oldVenuesMap) {
        
        // NOTE: Below code can still be refactored/optimized.

        Map<String, JVCO_Primary_Tariff_Mapping__c> settings = JVCO_Primary_Tariff_Mapping__c.getAll();
        Map<Id, String> venueIdsToPrimaryTariffMap = new Map<Id, String>();
        for (JVCO_Venue__c v : newVenuesList) {
            if (oldVenuesMap != null) {
                if (v.JVCO_Venue_Type__c != null && v.JVCO_Venue_Type__c != oldVenuesMap.get(v.Id).JVCO_Venue_Type__c) {
                    if (settings.containsKey(v.JVCO_Venue_Type__c)) {
                        v.JVCO_Primary_Tariff__c = settings.get(v.JVCO_Venue_Type__c).JVCO_Primary_Tariff__c;
                        venueIdsToPrimaryTariffMap.put(v.Id, v.JVCO_Primary_Tariff__c);
                    } else {
                        v.addError('Primary tariff failed to update because the venue type does not exist in the \'Primary Tariff Mapping\'. Kindly report this incident to your administrator.');
                    }
                }
            } else {
                if (v.JVCO_Venue_Type__c != null) {
                    if (settings.containsKey(v.JVCO_Venue_Type__c)) {
                        v.JVCO_Primary_Tariff__c = settings.get(v.JVCO_Venue_Type__c).JVCO_Primary_Tariff__c;
                        venueIdsToPrimaryTariffMap.put(v.Id, v.JVCO_Primary_Tariff__c);
                    } else {
                        v.addError('Primary tariff failed to update because the venue type does not exist in the \'Primary Tariff Mapping\'. Kindly report this incident to your administrator.');
                    }
                }
            }
        }

        if (venueIdsToPrimaryTariffMap.size() > 0) {

            // Needed to separate the query for QLG and QLI because deep traversals (more than 2 levels) results to "Nesting of semi join sub-selects is not supported". 
            // This is a limitation. SEE: https://success.salesforce.com/ideaView?id=08730000000HSxSAAW
            List<SBQQ__QuoteLineGroup__c> tmpQlgList = [SELECT Id 
                                                          FROM SBQQ__QuoteLineGroup__c 
                                                         WHERE JVCO_Affiliated_Venue__c 
                                                            IN (SELECT Id 
                                                                  FROM JVCO_Affiliation__c 
                                                                 WHERE JVCO_Venue__c 
                                                                    IN :venueIdsToPrimaryTariffMap.keySet())];

            if (tmpQlgList.size() > 0) {

                List<SBQQ__QuoteLine__c> quoteLinesToUpdateList = [SELECT Id, SBQQ__Group__c, SBQQ__Group__r.JVCO_Affiliated_Venue__c, SBQQ__Group__r.JVCO_Affiliated_Venue__r.JVCO_Venue__c
                                                                     FROM SBQQ__QuoteLine__c 
                                                                    WHERE SBQQ__Group__c 
                                                                       IN :tmpQlgList
                                                                       AND SBQQ__Quote__r.SBQQ__Status__c = 'Draft']; //2020-04-30 rhys.j.c.dela.cruz GREEN-35591 - Query only Draft Quotes

                if (quoteLinesToUpdateList.size() > 0) {
                    for (SBQQ__QuoteLine__c ql : quoteLinesToUpdateList) {
                        ql.JVCO_Primary_Tariff__c = venueIdsToPrimaryTariffMap.get(ql.SBQQ__Group__r.JVCO_Affiliated_Venue__r.JVCO_Venue__c);
                    }

                    try {
                        update quoteLinesToUpdateList;
                    } catch (DmlException de) {
                        newVenuesList.get(0).addError('Primary tariff failed to update because of the ff. error(s) from the quote lines: ' + de.getMessage());
                    }
                }
            }
        }
    }


    
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    Populates the Budget Category Field based from the Venue Type against the Budget Category Dimension Mapping list
    Inputs:         Venue record list
    Returns:        Boolean
    <Date>          <Authors Name>                    <Brief Description of Change> 
    11-Aug-2017     joseph.g.barrameda                 Initial version of the code
    21-Sep-2017     joseph.g.barrameda                 Change the argument from Venue Subtype to Venue Type
    ----------------------------------------------------------------------------------------------------------*/    
    public static void updateBudgetCategory(List<JVCO_Venue__c> venueList)
    {
    
        List<JVCO_Budget_Category_Dimension_Mapping__c> budgetCategoryList =JVCO_Budget_Category_Dimension_Mapping__c.getall().values(); 
        Map<String,String> getBudgetCategoryByVenueType = new Map<String,String>();
        
        for(JVCO_Budget_Category_Dimension_Mapping__c bcdm: budgetCategoryList ){
            getBudgetCategoryByVenueType.put(bcdm.JVCO_Venue_Type_Label__c, bcdm.JVCO_Budget_Category_Label__c);
        }
        
        for(JVCO_Venue__c venueRec: venueList){
            venueRec.JVCO_Budget_Category__c = getBudgetCategoryByVenueType.get(venueRec.JVCO_Venue_Type__c);
        }

    }

}