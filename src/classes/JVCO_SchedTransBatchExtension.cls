public class JVCO_SchedTransBatchExtension{
    public String reportLink {get;set;}
    public JVCO_SchedTransBatchExtension() {
        
        reportLink  = (String)JVCO_Constants__c.getOrgDefaults().AccountTransformationReportLink__c;
    }

    public PageReference redirectToSched(){

        
        PageReference schedJobPage = new PageReference('/ui/setup/apex/batch/ScheduleBatchApexPage?job_name=Account+Transformation+Batch+' + Date.today().day() + '/' + Date.today().month() + '/' + Date.today().year() +'&ac=JVCO_TransOfAccTypeScheduler');

        return schedJobPage;
    } 
}