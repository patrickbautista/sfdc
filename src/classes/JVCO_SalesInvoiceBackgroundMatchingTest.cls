@isTest
public class JVCO_SalesInvoiceBackgroundMatchingTest 
{
    @testSetup static void createTestData() 
    {
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        //Settings
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_codaCreditNoteTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        c2g__codaDimension2__c dim2 = JVCO_TestClassHelper.setDim2('PPL');
        insert dim2;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;

        //Credit Note
        c2g__codaCreditNote__c posCNote = getCreditNote(licAcc.Id);
        insert posCNote;
        c2g__codaCreditNoteLineItem__c posCNoteLine = getCreditNoteLine(posCNote.Id, dim1.Id, dim3.Id, taxCode.Id, dim2.Id, p.Id, 12000);
        insert posCNoteLine;
        c2g.CODAAPICommon.Reference cNoteRef = new c2g.CODAAPICommon.Reference();
        cNoteRef.Id = posCNote.Id;
       
        //Sales Invoice
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        insert sInv;
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);
        c2g.CODAAPICommon.Reference sInvRef = new c2g.CODAAPICommon.Reference();
        sInvRef.Id = sInv.Id;

        Test.startTest();
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(context, sInvRef);
        c2g.CODAAPISalesCreditNote_10_0.PostCreditNote(context, cNoteRef);
        Test.stopTest();
    }

    @isTest
    static void matchToCreditNoteLogic()
    {

        c2g__codaCreditNote__c cNote = [SELECT Id, Name
                                        FROM c2g__codaCreditNote__c LIMIT 1];
        c2g__codaInvoice__c sInv = [SELECT Id, Name
                                    FROM c2g__codaInvoice__c LIMIT 1];

        sInv.JVCO_Reference_Document__c = cNote.Name;
        update sInv;
        Test.startTest();
        Database.executeBatch(new JVCO_SalesInvoiceBackgroundMatchingBatch(), 1);
        Test.stopTest();
    }

    @isTest
    static void testMatchToCashReceiptLogic()
    {
        Set<Id> cashEntryIdSet = new Set<Id>();
        c2g__codaCashEntry__c cashEntry = new c2g__codaCashEntry__c();
        cashEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cashEntry.c2g__Type__c = 'Receipt';
        insert cashEntry;
        cashEntryIdSet.add(cashEntry.Id);
        c2g__codaInvoice__c sInv = [SELECT Id, c2g__Account__c, Name
                                    FROM c2g__codaInvoice__c LIMIT 1];
        c2g__codaCashEntryLineItem__c cashEntryLine = new c2g__codaCashEntryLineItem__c();
        cashEntryLine.c2g__Account__c = sInv.c2g__Account__c;
        cashEntryLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cashEntryLine.c2g__CashEntryValue__c = 1199;
        cashEntryLine.c2g__CashEntry__c = cashEntry.Id;
        insert cashEntryLine;

        c2g__codaCashEntryLineItem__c cashEntryLineRef = [SELECT Name FROM c2g__codaCashEntryLineItem__c LIMIT 1];
        sInv.JVCO_Reference_Document__c = cashEntryLineRef.Name;
        update sInv;
        c2g.CODAAPICommon_7_0.Context context = new c2g.CODAAPICommon_7_0.Context();
        c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
        ref.Id = cashEntry.Id;
        Test.startTest();
        c2g.CODAAPICashEntry_7_0.PostCashEntry(context, ref);
        Database.executeBatch(new JVCO_SalesInvoiceBackgroundMatchingBatch(), 1);
        Test.stopTest();
    }

    private static c2g__codaCreditNote__c getCreditNote(Id licAccId)
    {
        c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
        cNote.c2g__Account__c = licAccId;
        cNote.c2g__DueDate__c = System.today() + 30;
        cNote.c2g__CreditNoteDate__c = System.today();
        cNote.c2g__DeriveCurrency__c = true;
        cNote.c2g__DerivePeriod__c = true;
        cNote.c2g__DeriveDueDate__c = true;
        return cNote;
    }

    private static c2g__codaCreditNoteLineItem__c getCreditNoteLine(Id cNoteId, Id dim1Id, Id dim3Id,
                                                                    Id taxCodeId, Id dim2Id, Id prodId, 
                                                                    Decimal unitPrice)
    {
        c2g__codaCreditNoteLineItem__c cNoteLine = new c2g__codaCreditNoteLineItem__c();
        cNoteLine.c2g__CreditNote__c = cNoteId;
        cNoteLine.c2g__Dimension1__c = dim1Id;
        cNoteLine.c2g__Dimension3__c = dim3Id;
        cNoteLine.c2g__TaxCode1__c = taxCodeId;
        cNoteLine.c2g__DeriveLineNumber__c = true;
        cNoteLine.c2g__DeriveUnitPriceFromProduct__c = false;
        cNoteLine.c2g__Quantity__c = 1;
        cNoteLine.c2g__UnitPrice__c = unitPrice;
        cNoteLine.c2g__Dimension2__c = dim2Id;
        cNoteLine.c2g__Product__c = prodId;
        return cNoteLine;
    }
    
}