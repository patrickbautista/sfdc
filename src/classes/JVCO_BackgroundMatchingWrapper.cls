/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_BackgroundMatchingWrapper.cls 
   Description:     Wrapper Class for JVCO_BackgroundMatchingLogic
   Test class:      

   Date            Version     Author                             Summary of Changes 
   -----------     -------     -----------------                  -----------------------------------
   28-Apr-2017     0.1         franz.g.a.dimaapi@accenture.com    Intial draft
   23-Jul-2017     0.2         franz.g.a.dimaapi@accenture.com    Updated Overpaid Logic
   24-Aug-2017     0.3         franz.g.a.dimaapi@accenture.com    Refactor runMatchingAPI() method
   30-Nov-2017     0.4         filip.bezak@accenture.com          Added functionality for partialy paid matching of Journals
   06-Dec-2017     0.5         csaba.feher@accenture.com          Added paid values as for GREEN-24996
   07-Feb-2018     0.6         filip.bezak@accenture.com          Logic for preventing the creation of Journal when matching against Sales Invoice without Billing Invoice is performed
   11-Mar-2019     0.7         franz.g.a.dimaapi                  GREEN-34114 - Clean Class
  ------------------------------------------------------------------------------------------------ */
public class JVCO_BackgroundMatchingWrapper{
    
    public c2g.CODAAPICommon_8_0.Context context = new c2g.CODAAPICommon_8_0.Context();
    public String matchFromType;
    public Boolean isFromAccount = false;
    public Boolean isMatchByAccRef = false;
    public Boolean isInstLineRecalc = false;
    public c2g__codaTransactionLineItem__c matchFromTransLine = new c2g__codaTransactionLineItem__c();
    public c2g__codaTransactionLineItem__c matchToTransLine;
    public List<c2g__codaInvoiceInstallmentLineItem__c> installmentLineList = null;
    public List<c2g__codaTransactionLineItem__c> matchToTransLineByAccList = new List<c2g__codaTransactionLineItem__c>();
    public Double writeOffLimit = c2g__codaCashMatchingSettings__c.getInstance().JVCO_Write_off_Limit__c;
    private Id openPeriodId;
    public c2g__codaTransactionLineItem__c getMatchToTransLineByAcc()
    {
        Double matchFromAmount = matchFromTransLine.c2g__AccountOutstandingValue__c;
        c2g__codaTransactionLineItem__c tli = null;
        if(matchFromType == 'Receipt'){
            //Return Invoice Transaction Line
            tli = getInvoiceTLI(matchFromAmount);
        }else if(matchFromType == 'Refund'){
            //Return CreditNote Transaction Line
            tli = getCreditNoteTLI(matchFromAmount);
            if(tli == null)
            {
                //Return Cash Receipt Transaction Line
                tli = getReceiptCashTLI(matchFromAmount);
            }
        }else if(matchFromType == 'Invoice')
        {
            //Return Cash Receipt Transaction Line
            tli = getReceiptCashTLI(matchFromAmount);
            if(tli == null)
            {
                tli = getJournalTLI(matchFromAmount);
            }
        }else if(matchFromType == 'Journal')
        {
            //Return Invoice Transaction Line
            tli = getInvoiceTLI(matchFromAmount);
        }
        return tli;
    }

    public Map<Id, Id> runMatchingAPI(){
        Map<Id, Id> licAndCustAccIdMap = new Map<Id, Id>();
        if(matchToTransLine != null){
            setOpenPeriod();
            //context.CompanyName = 'PRS_PPL JV For Music';
            c2g.CODAAPICashMatchingTypes_8_0.Configuration configuration = new c2g.CODAAPICashMatchingTypes_8_0.Configuration();
            configuration.Account = c2g.CODAAPICommon.getRef(matchFromTransLine.c2g__Account__c, null);
            configuration.MatchingDate = System.today();
            configuration.MatchingCurrencyMode = c2g.CODAAPICashMatchingTypes_8_0.enumMatchingCurrencyMode.Account; 
            
            //Matching Period
            Id periodId = matchFromTransLine.c2g__Transaction__r.c2g__Period__r.c2g__Closed__c ? openPeriodId : matchFromTransLine.c2g__Transaction__r.c2g__Period__c;
            configuration.MatchingPeriod = c2g.CODAAPICommon.getRef(periodId, null);
            
            List<c2g.CODAAPICashMatchingTypes_8_0.Item> items = new List<c2g.CODAAPICashMatchingTypes_8_0.Item>();
            c2g.CODAAPICashMatchingTypes_8_0.Analysis analysisInfo = new c2g.CODAAPICashMatchingTypes_8_0.Analysis();
            
            c2g.CODAAPICashMatchingTypes_8_0.Item item1 = new c2g.CODAAPICashMatchingTypes_8_0.Item();
            c2g.CODAAPICashMatchingTypes_8_0.Item item2 = new c2g.CODAAPICashMatchingTypes_8_0.Item();
            
            item1.TransactionLineItem = c2g.CODAAPICommon.getRef(matchFromTransLine.Id, null);
            item2.TransactionLineItem = c2g.CODAAPICommon.getRef(matchToTransLine.Id, null);
            //By Account Reference - Overpay - Receipt to Sales Invoice
            if(isMatchByAccRef)
            {
                Double difference = matchToTransLine.c2g__AccountOutstandingValue__c - Math.abs(matchFromTransLine.c2g__AccountOutstandingValue__c);
                // Check if valid Write-Off
                if(difference != 0 && Math.abs(difference) <= writeOffLimit)
                {
                    item1.Paid = matchFromTransLine.c2g__AccountOutstandingValue__c;
                    item2.Paid = matchFromTransLine.c2g__AccountOutstandingValue__c * -1;
                    item2.WriteOff = difference;
                    analysisInfo.WriteOffGla = c2g.CODAAPICommon.getRef(matchFromTransLine.c2g__OwnerCompany__r.c2g__CustomerWriteOff__c, null);
                    //If Overpay
                }else if(difference < 0)
                {
                    item1.Paid = matchToTransLine.c2g__AccountOutstandingValue__c * -1;
                    item2.Paid = matchToTransLine.c2g__AccountOutstandingValue__c;
                    //If Underpay
                }else if(difference > 0)
                {
                    item1.Paid = matchFromTransLine.c2g__AccountOutstandingValue__c;
                    item2.Paid = matchFromTransLine.c2g__AccountOutstandingValue__c * -1;
                    //Equal 
                }else
                {
                    item1.Paid = matchFromTransLine.c2g__AccountOutstandingValue__c;
                    item2.Paid = matchToTransLine.c2g__AccountOutstandingValue__c;
                }
                
                //By Account
            }else{
                //Match From Cash Receipt
                if(matchFromType == 'Receipt'){
                    System.debug('@'+'Receipt');
                    Double difference = matchToTransLine.c2g__AccountOutstandingValue__c - Math.abs(matchFromTransLine.c2g__AccountOutstandingValue__c);
                    //Check if Overpay/Underpay and Applicable to Write-Off
                    if(difference != 0 && Math.abs(difference) <= writeOffLimit)
                    {
                        item1.Paid = matchFromTransLine.c2g__AccountOutstandingValue__c;
                        item2.Paid = matchFromTransLine.c2g__AccountOutstandingValue__c * -1;
                        item2.WriteOff = difference;
                        analysisInfo.WriteOffGla = c2g.CODAAPICommon.getRef(matchFromTransLine.c2g__OwnerCompany__r.c2g__CustomerWriteOff__c, null);
                        //Check if equal
                    }else if(difference == 0)
                    {
                        item1.Paid = matchFromTransLine.c2g__AccountOutstandingValue__c;
                        item2.Paid = matchToTransLine.c2g__AccountOutstandingValue__c;
                    }else
                    { // implemented by Csaba Feher 06-Dec-2017 GREEN-24996
                        item1.Paid = matchToTransLine.c2g__AccountOutstandingValue__c * -1;
                        item2.Paid = matchToTransLine.c2g__AccountOutstandingValue__c;
                    }
                    System.debug('Receipt writeOffLimit: ' + writeOffLimit);
                    System.debug('@'+difference);
                    //Match From CreditNote
                    /*}else if(matchFromType == 'Credit Note')
                    {
                    Double difference = matchToTransLine.c2g__AccountOutstandingValue__c - Math.abs(matchFromTransLine.c2g__AccountOutstandingValue__c);
                    //Check if Underpay
                    if(difference > 0)
                    {
                        item1.Paid = matchFromTransLine.c2g__AccountOutstandingValue__c;
                        item2.Paid = matchFromTransLine.c2g__AccountOutstandingValue__c * -1;
                    //Overpay || Equal
                    }else{
                        item1.Paid = matchToTransLine.c2g__AccountOutstandingValue__c * -1;
                        item2.Paid = matchToTransLine.c2g__AccountOutstandingValue__c;
                    }*/
                    //Match From Journal
                }else if(matchFromType == 'Journal')
                {
                    Double difference = matchToTransLine.c2g__AccountOutstandingValue__c - Math.abs(matchFromTransLine.c2g__AccountOutstandingValue__c);
                    //Check if Underpay
                    if(difference > 0)
                    {
                        item1.Paid = matchFromTransLine.c2g__AccountOutstandingValue__c;
                        item2.Paid = matchFromTransLine.c2g__AccountOutstandingValue__c * -1;                    
                    }
                    //Overpay || Equal
                    else
                    {
                        item1.Paid = matchToTransLine.c2g__AccountOutstandingValue__c * -1;
                        item2.Paid = matchToTransLine.c2g__AccountOutstandingValue__c;
                    }
                }
                //Match From any except Cash Receipt and Credit Note
                else{
                    item1.Paid = matchFromTransLine.c2g__AccountOutstandingValue__c;
                    item2.Paid = matchToTransLine.c2g__AccountOutstandingValue__c;  
                }
            }
            
            System.debug('@Item1: '+ item1);
            items.add(item1);
            System.debug('@Item2: '+ item2);
            items.add(item2);
            
            //Call FF Matching API
            try //temporary exception handling to avoid runtime error during batch (c2g.CODAAPIException: Referenced object not found.) - Csaba Feher 07-Dec-2017 GREEN-24996
            {   //this is implemented in QA already
                c2g.CODAAPICashMatching_8_0.Match(context, configuration, items, analysisInfo);
                licAndCustAccIdMap.put(matchFromTransLine.c2g__Account__c, matchFromTransLine.c2g__Account__r.JVCO_Customer_Account__c);
            }
            catch(Exception fe)
            {
                System.debug('Exception during financialforce matching: ' + fe.getMessage() + fe.getStackTraceString() + fe.getCause());
                List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
                
                ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(item1.TransactionLineItem.Id);
                errLog.ffps_custRem__Detail__c = string.valueof(fe.getMessage());
                errLog.ffps_custRem__Message__c = 'Background Auto Matching';
                errLog.ffps_custRem__Grouping__c = 'BM-001';
                errLogList.add(errLog);
                
                insert errLogList;
            }
        }
        return licAndCustAccIdMap; 
    }

    private c2g__codaTransactionLineItem__c getReceiptCashTLI(Double matchFromAmount){
        for(c2g__codaTransactionLineItem__c tli : matchToTransLineByAccList){
            if(tli.c2g__Transaction__r.c2g__TransactionType__c == 'Cash'){
                if(matchFromAmount == tli.c2g__AccountOutstandingValue__c * -1){
                    return tli;
                }
            }   
        }

        return null;
    }

    private c2g__codaTransactionLineItem__c getCreditNoteTLI(Double matchFromAmount){
        for(c2g__codaTransactionLineItem__c tli : matchToTransLineByAccList){
            if(tli.c2g__Transaction__r.c2g__TransactionType__c == 'Credit Note'){
                if(matchFromAmount == tli.c2g__AccountOutstandingValue__c * -1){
                    return tli;
                }
            }   
        }

        return null;
    }
    
    private c2g__codaTransactionLineItem__c getInvoiceTLI(Double matchFromAmount){
        for(c2g__codaTransactionLineItem__c tli : matchToTransLineByAccList){
            if(tli.c2g__Transaction__r.c2g__TransactionType__c == 'Invoice'){
                if(matchFromType == 'Receipt'){
                    if((tli.c2g__AccountOutstandingValue__c * -1) <= (matchFromAmount + writeOffLimit) && 
                        (tli.c2g__AccountOutstandingValue__c * -1) >= (matchFromAmount - writeOffLimit)){ 
                        return tli;
                    }
                }
                if(matchFromAmount == tli.c2g__AccountOutstandingValue__c * -1){
                    return tli;
                }
            }   
        }

        return null;
    }

    private c2g__codaTransactionLineItem__c getJournalTLI(Double matchFromAmount){
        for(c2g__codaTransactionLineItem__c tli : matchToTransLineByAccList){
            if(tli.c2g__Transaction__r.c2g__TransactionType__c == 'Journal' &&
                tli.c2g__Transaction__r.c2g__Journal__r.c2g__Type__c == 'Manual Journal' &&
                tli.c2g__Transaction__r.c2g__Journal__r.c2g__JournalDescription__c != 'Transfer Cash to Parent'){
                if(matchFromAmount == tli.c2g__AccountOutstandingValue__c * -1){
                    return tli;
                }
            }   
        }

        return null;
    }
    
    private void setOpenPeriod()
	{
        try
        {
            this.openPeriodId = [SELECT ID
								FROM c2g__codaPeriod__c 
								WHERE c2g__Closed__c = FALSE
								AND c2g__AR__c = FALSE
								AND c2g__Cash__c = FALSE
								AND c2g__EndDate__c >= TODAY
								AND c2g__OwnerCompany__r.Name = 'PPL PRS Ltd.'
								ORDER BY NAME ASC
								LIMIT 1].Id;
        }catch(Exception e){}
	}
}