/**
* Created by: Accenture
* Created Date: 05.11.2017
* Description: Custom VF for Resend Document.
*
*  Change History :
*  Date             Name            Version No       Description 
*  22/12/2017     Ashok Kumar        1.0              GREEN-27738  'Resend Document' Button - Enhancement  Description
   04/05/2018     mary.ann.a.ruelan  1.1              GREEN-31701   updated to disable inclusions picklist when transmission=email 
*
*
**/
public class JVCO_ResendDocumentBundleCtrl 
{
    public JVCO_Document_Queue__c documentObj {get;set;}
    private JVCO_Document_Queue__c documentMasterCopy;
    private Boolean firstLoad;
    private Map<String,List<String>> picklistValuesMap  = new Map<String,List<String>>();
    public List<SelectOption> formatPicklistValues{get;set;}
    public List<SelectOption> transmissionPicklistValues{get;set;}
    public List<SelectOption> printStatusPicklistValues{get;set;}
    public List<SelectOption> inclusionsPicklistValues{get;set;}    
    /**
     * Constructor 
     *
     */
    public JVCO_ResendDocumentBundleCtrl(ApexPages.StandardController controller) 
    {
         String masterCopyID = Apexpages.currentpage().getparameters().get('cloneID');        
         RecordType recObj = [Select id from RecordType where DeveloperName = 'Send_Document_Clone'];         
         String soql = getCreatableFieldsSOQL('JVCO_Document_Queue__c' , (' id = \'' + masterCopyID   + '\'' ));        
         documentMasterCopy = Database.query(soql);
         documentObj= documentMasterCopy.clone(false); 
         documentObj.RecordTypeID= recObj.id;          
         documentObj.JVCO_Generation_Status__c = 'To be Generated';
         documentObj.JVCO_Clone_DQ__c = true;          
         firstLoad =  true;
         
         for(JVCO_PPL_PPRS_App_Config__mdt configObj : [Select id, Controlling_Field_Value__c,MasterLabel, Value__c, Field_API_Name__c from JVCO_PPL_PPRS_App_Config__mdt where Field_API_Name__c in ('JVCO_Format__c','JVCO_Scenario__c', 'JVCO_Transmission__c' , 'JVCO_Inclusions__c' , 'JVCO_Print_Status__c')])
         {
             if(configObj.Value__c != null )
             {
                  picklistValuesMap.put( configObj.Controlling_Field_Value__c, configObj.Value__c.split(';'));     
             }           
         }         
         getFormatPicklistValues();
         transmissionPicklistValues();
         printStatusPicklistValues();
         inclusionsPicklistValues();
         firstLoad  = false;  
    }
    /**
     * Returns a dynamic SOQL statement for the whole object, includes only creatable fields since we will be inserting a cloned result of this query 
     *
     */
    public string getCreatableFieldsSOQL(String objectName, String whereClause)
    {           
        String selects = '';           
        if (whereClause == null || whereClause == ''){ return null; }           
        // Get a map of field name and field token 
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap(); 
        list<string> selectFields = new list<string>(); 
        //selectFields.add('RecordTypeID');   
        if (fMap != null){ 
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft) 
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd) 
                if (fd.isCreateable()){ // field is creatable 
                    selectFields.add(fd.getName()); 
                } 
            } 
        } 
          
        if (!selectFields.isEmpty()){ 
            for (string s:selectFields){ 
                selects += s + ','; 
            } 
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));} 
              
        } 
          
        return 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;           
    } 
    /**
     *
     * insert recod to the database.
     * 
     */
    public Pagereference saveRecord()
    {
        Pagereference  pageRef = null;
        String errmsg = validateDQ(); 
        
        try
        {
            if(errmsg != null)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,errmsg ));            
                return null;
            }
            upsert documentObj;
            pageRef = new Pagereference('/' + documentObj.id);
        }
        catch(DMLException e )
        {
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0)));  
        }
        catch(Exception e )
        {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));  
             System.debug('e.getMessage()--' + e.getMessage());      
        }
        return pageRef;
    }
    /**
     * Validate DQ record is updated.
     *
     */    
    private String validateDQ()
    {
        String errMsg = null;                
        // if(documentObj.Scenario_Category__c != documentMasterCopy.Scenario_Category__c || documentObj.JVCO_Scenario__c!= documentMasterCopy.JVCO_Scenario__c ||documentObj.JVCO_Sub_Scenario__c!= documentMasterCopy.JVCO_Sub_Scenario__c)
        if(documentObj.JVCO_Format__c == '' || documentObj.JVCO_Format__c == null || documentObj.JVCO_Transmission__c == '' || documentObj.JVCO_Transmission__c ==  null ) 
        {
           
            errMsg   = System.label.JVCO_Resend_DQ_Cannot_update_Err_msg;
        }
        else if( documentObj.JVCO_Transmission__c == 'Letter' && (documentObj.JVCO_Print_Status__c == null || documentObj.JVCO_Print_Status__c  ==''))
        {
            errMsg   =System.label.JVCO_Resend_DQ_Cannot_update_Err_msg;
        }
        
        
        return errMsg;                
    }
    
    public void  getFormatPicklistValues()
    {        
        formatPicklistValues  = new List<SelectOption>();              
        formatPicklistValues.add(new SelectOption('','--None--'));        
            
        if(picklistValuesMap.get(documentObj.JVCO_Scenario__c) !=null)
        {               
            for(String str : picklistValuesMap.get(documentObj.JVCO_Scenario__c))
            {
                 formatPicklistValues.add(new SelectOption(str,str));         
            }
        }                    
    }
    
    
    public void transmissionPicklistValues()
    {          
        transmissionPicklistValues = new List<SelectOption>();
        transmissionPicklistValues.add(new SelectOption('','--None--')); 
        if(picklistValuesMap.get(documentObj.JVCO_Format__c) != null && picklistValuesMap.get(documentObj.JVCO_Format__c).size() > 0)
        {        
            for(String str : picklistValuesMap.get(documentObj.JVCO_Format__c))
            {
                 transmissionPicklistValues.add(new SelectOption(str,str));         
            }            
        }
        if(!firstLoad)
        {            
            documentObj.JVCO_Inclusions__c = '';
            documentObj.JVCO_Print_Status__c= '';           
            if(documentObj.JVCO_Format__c == '' || documentObj.JVCO_Format__c == null)
            {
                documentObj.JVCO_Transmission__c = '';
            }   
             printStatusPicklistValues();
             inclusionsPicklistValues();        
        } 
    }
    
    public void printStatusPicklistValues()
    {        
        printStatusPicklistValues = new List<SelectOption>();         
        printStatusPicklistValues.add(new SelectOption('','--None--'));         
        if(picklistValuesMap.get(documentObj.JVCO_Transmission__c)!=null)
        {           
            for(String str : picklistValuesMap.get(documentObj.JVCO_Transmission__c))
            {
                 printStatusPicklistValues.add(new SelectOption(str,str));         
            }
         }
         if(!firstLoad)
        {
            if(String.isBlank(documentObj.JVCO_Transmission__c) || documentObj.JVCO_Transmission__c == 'Email' )
            {
                documentObj.JVCO_Print_Status__c = '';
            }
            documentObj.JVCO_Inclusions__c= '';           
            inclusionsPicklistValues();
        }   
    }
    
    public void  inclusionsPicklistValues()
    {
        inclusionsPicklistValues = new List<SelectOption>();
        inclusionsPicklistValues.add(new SelectOption('','--None--'));
        if(picklistValuesMap.get(documentObj.JVCO_Print_Status__c)!=null)
        {           
            for(String str : picklistValuesMap.get(documentObj.JVCO_Print_Status__c))
            {
                 InclusionsPicklistValues.add(new SelectOption(str,str));         
            }
         }   
    }            
}