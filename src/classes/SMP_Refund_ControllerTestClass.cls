@isTest
private class SMP_Refund_ControllerTestClass{
    static testMethod void testRefund() {

        Contact contact = new Contact(
            //AccountId = '0011X00000Tre06',               // Account Name
            FirstName = 'TestName1',
            LastName = 'TestName2',
            Email = 'adriel.nikko.h.reyes@accenture.com',  // Email
            JVCO_Contact_Status__c = 'Active',             // Contact Status
            Title = 'Test Manager',                        // Job Title
            Phone = '312370101',                           // Phone
            MobilePhone = '312370101',                     // Mobile
            JVCO_Email_Marketing_Opt_in__c = false,        // Email Marketing Opt-in
            JVCO_Telephone_Marketing_Opt_in__c = false,    // Telephone Marketing Opt-in
            JVCO_Postal_Marketing_Opt_in__c = false,       // Postal Marketing Opt-in
            Use_Main_Customer_Address__c = false,          // Use Customer Account Address
            pi__pardot_hard_bounced__c = false,            // Pardot Hard Bounced
            Pardot_Contact__c = false,                     // Pardot
            HasOptedOutOfEmail = false                    // Email Opt Out
          );
          insert contact;


        Income_Card_Payment__c cp = new Income_Card_Payment__c(
            Authorisation_Code__c = '5052854',                                     // Authorisation Code
            Authorisation_Date__c = Date.Parse('10/03/2020'),                 // Authorisation Date
            Contact__c = contact.Id,                                               // Contact
            Card_Type__c = 'VISA',                                                   // Card Type
            Immediate_Amount__c = 3339.37,                                           // Immediate Amount
            //Total_Amount_Taken__c = 3339.37,
            Card_Number_Ending__c = '0006',                                          // Card Number Ending
            Payment_Description__c = 'PPL First Card Payment',                       // Payment Description
            Transaction_Type__c = 'Sale',                                            // Transaction Type
            Payment_Reason__c = 'nulla3H1X000000OcqJUAS,',                           // Payment Reason
            Payment_Status__c = 'Authorised',                                        // Payment Status
            Payment_Status_Details__c = '0000 : The Authorisation was Successful.',  // Payment Status Details
            Sagepay_Transaction_ID__c = '{001F2AFB-8D31-CFD7-FF3B-7C32C1711A8D}',    // Sagepay Transaction ID
            Sagepay_Security_Key__c = 'JV7O5RKUJZ',                                  // Sagepay Security Key
            RP_Enabled__c = false                                                   // RP Enabled
          );
          insert cp;


        ApexPages.StandardController sc = new ApexPages.StandardController(cp);

        SMP_Refund_Controller controller = new SMP_Refund_Controller(sc);

        SMP_Refund_Controller.getConnectionURL();
        SMP_Refund_Controller.fetchUserSessionId();

    }

}