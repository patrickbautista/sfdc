@isTest
private class JVCO_DM_AccountWithCreditStatusTest
{
    @testSetup
    static void createTestData()
    {
        JVCO_TestClassObjectBuilder.createBillingConfig();

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
            listQueue.add(q2);
            queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
            listQueue.add(q3);
            queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
            listQueue.add(q4);
            queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
            listQueue.add(q5);
            queuesobject q6 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankAccount__c'); 
            listQueue.add(q6);
            queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q7);
            queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatement__c'); 
            listQueue.add(q8);          
            queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntry__c'); 
            listQueue.add(q9);  
            queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='PAYBASE2__Payment__c'); 
            listQueue.add(q10);
            queuesobject q11 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransaction__c'); 
            listQueue.add(q11);
            //queuesobject q12 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransactionLineItem__c'); 
            //listQueue.add(q12);
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }
        
         //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }

        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc= JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert testGeneralLedgerAcc;
      
        c2g__codaCompany__c testCompany= JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert testCompany;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(testGeneralLedgerAcc.Id);
        insert taxCode;
        
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_AccountTrigger__c = true;
        insert dt;  

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;

        //Create Account               
        Account testAccCust = JVCO_TestClassHelper.setCustAcc(testGeneralLedgerAcc.Id, dim1.Id);
        insert testAccCust ;
        Contact c = JVCO_TestClassHelper.setContact(testAccCust.id);
        insert c;
        Account testLicAcc = JVCO_TestClassHelper.setLicAcc(testAccCust.Id, taxCode.Id, c.Id,testGeneralLedgerAcc.Id);
        testLicAcc.JVCO_Credit_Status__c = 'Debt - 1st stage';
        insert testLicAcc;
        

        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(testCompany.Id, testGroup.Id);
        insert accCurrency;
        

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(testCompany.Id, testGroup.Id);
        insert yr;
        
        insert JVCO_TestClassHelper.setPeriod(testCompany.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(testCompany.Id);
    
        
        Product2 p = JVCO_TestClassHelper.setProduct(testGeneralLedgerAcc.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(testLicAcc.Id);
        insert opp;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(testLicAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;

        Contract contr= new Contract();
        contr.AccountId = testLicAcc.Id;
        contr.Status = 'Draft';
        contr.StartDate = Date.today();
        contr.ContractTerm = 12;
        contr.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
        insert contr;
        
        Test.startTest();
        
        Order o = JVCO_TestClassHelper.setOrder(testLicAcc.Id, q.Id);
        insert o;
        
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        OrderItem oi1 = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi1;

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(testLicAcc.Id, o.Id);
        insert bInv;

        
        //Create Sales Invoice
        c2g__codaInvoice__c testInvoice1 = new c2g__codaInvoice__c();
        testInvoice1.c2g__InvoiceCurrency__c = accCurrency.Id;
        testInvoice1.c2g__InvoiceDate__c = date.today().addDays(-7);
        testInvoice1.c2g__DueDate__c = date.today().addDays(-7);
        testInvoice1.c2g__Account__c = testLicAcc.Id;
        testInvoice1.JVCO_Customer_Type__c = 'New Business';
        testInvoice1.c2g__OwnerCompany__c = testCompany.id;
        testInvoice1.JVCO_Invoice_Legacy_Number__c = 'SIN00001';
        testInvoice1.JVCO_Original_Invoice__c = null;
        insert testInvoice1;
        
        
        Test.stopTest();
        
    }
    
    @isTest
    static void statusFirstStage()
    
    {
        c2g__codaInvoice__c testInvoice1 = [SELECT ID FROM c2g__codaInvoice__c LIMIT 1];
        Product2 p = [SELECT ID FROM Product2 LIMIT 1];
        
        Test.startTest();
        c2g__codaInvoiceLineItem__c salesInvoiceLine = new c2g__codaInvoiceLineItem__c();
        salesInvoiceLine.c2g__Invoice__c = testInvoice1.id;
        salesInvoiceLine.c2g__Product__c = p.id;
        salesInvoiceLine.c2g__Quantity__c = 1.000000;
        salesInvoiceLine.c2g__UnitPrice__c= 45.000000000;
        salesInvoiceLine.c2g__DeriveUnitPriceFromProduct__c = true;
        salesInvoiceLine.c2g__DeriveTaxRate1FromCode__c = true;
        salesInvoiceLine.c2g__CalculateTaxValue1FromRate__c = true;
        salesInvoiceLine.c2g__TaxValue1__c = 0;
        insert salesInvoiceLine;
        
        
        //post the sales invoice to create a transaction 
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = testInvoice1.Id;
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(context, reference);
        Test.stopTest();
        
        JVCO_DM_AccountWithCreditStatus b = new JVCO_DM_AccountWithCreditStatus();
        Database.executeBatch(b);
         
        
    }
    
    @isTest
    static void statusSecondStage()
    {
        Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        List<Account> a = [SELECT ID, JVCO_Credit_Status__c FROM Account WHERE RecordTypeId =: recId LIMIT 1];
        system.assertNotEquals(null, a, 'ACCOUNT IS NOT NULL');
        a[0].JVCO_Credit_Status__c = 'Debt - 2nd stage';
        update a;
        
        c2g__codaInvoice__c testInvoice1 = [SELECT ID FROM c2g__codaInvoice__c LIMIT 1];
        Product2 p = [SELECT ID FROM Product2 LIMIT 1];
        
        Test.startTest();
        c2g__codaInvoiceLineItem__c salesInvoiceLine = new c2g__codaInvoiceLineItem__c();
        salesInvoiceLine.c2g__Invoice__c = testInvoice1.id;
        salesInvoiceLine.c2g__Product__c = p.id;
        salesInvoiceLine.c2g__Quantity__c = 1.000000;
        salesInvoiceLine.c2g__UnitPrice__c= 45.000000000;
        salesInvoiceLine.c2g__DeriveUnitPriceFromProduct__c = true;
        salesInvoiceLine.c2g__DeriveTaxRate1FromCode__c = true;
        salesInvoiceLine.c2g__CalculateTaxValue1FromRate__c = true;
        salesInvoiceLine.c2g__TaxValue1__c = 0;
        insert salesInvoiceLine;
        
        
        //post the sales invoice to create a transaction 
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = testInvoice1.Id;
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(context, reference);
        Test.stopTest();
        
        JVCO_DM_AccountWithCreditStatus b = new JVCO_DM_AccountWithCreditStatus();
        Database.executeBatch(b);
         
       
    }
    
    @isTest
    static void statusThirdStage()
    {
        Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        List<Account> a = [SELECT ID, JVCO_Credit_Status__c FROM Account WHERE RecordTypeId =: recId LIMIT 1];
        system.assertNotEquals(null, a, 'ACCOUNT IS NOT NULL');
        a[0].JVCO_Credit_Status__c = 'Debt - 3rd stage';
        update a;
        
        c2g__codaInvoice__c testInvoice1 = [SELECT ID FROM c2g__codaInvoice__c LIMIT 1];
        Product2 p = [SELECT ID FROM Product2 LIMIT 1];
        
        Test.startTest();
        c2g__codaInvoiceLineItem__c salesInvoiceLine = new c2g__codaInvoiceLineItem__c();
        salesInvoiceLine.c2g__Invoice__c = testInvoice1.id;
        salesInvoiceLine.c2g__Product__c = p.id;
        salesInvoiceLine.c2g__Quantity__c = 1.000000;
        salesInvoiceLine.c2g__UnitPrice__c= 45.000000000;
        salesInvoiceLine.c2g__DeriveUnitPriceFromProduct__c = true;
        salesInvoiceLine.c2g__DeriveTaxRate1FromCode__c = true;
        salesInvoiceLine.c2g__CalculateTaxValue1FromRate__c = true;
        salesInvoiceLine.c2g__TaxValue1__c = 0;
        insert salesInvoiceLine;
        
        
        //post the sales invoice to create a transaction 
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = testInvoice1.Id;
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(context, reference);
        Test.stopTest();
        
        JVCO_DM_AccountWithCreditStatus b = new JVCO_DM_AccountWithCreditStatus();
        Database.executeBatch(b);
         
        
    }
    
    
    @isTest
    static void statusFinalStage()
    {
        Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        List<Account> a = [SELECT ID, JVCO_Credit_Status__c FROM Account WHERE RecordTypeId =: recId LIMIT 1];
        system.assertNotEquals(null, a, 'ACCOUNT IS NOT NULL');
        a[0].JVCO_Credit_Status__c = 'Debt - Final Demand';
        update a;
        
        c2g__codaInvoice__c testInvoice1 = [SELECT ID FROM c2g__codaInvoice__c LIMIT 1];
        Product2 p = [SELECT ID FROM Product2 LIMIT 1];
        
        Test.startTest();
        c2g__codaInvoiceLineItem__c salesInvoiceLine = new c2g__codaInvoiceLineItem__c();
        salesInvoiceLine.c2g__Invoice__c = testInvoice1.id;
        salesInvoiceLine.c2g__Product__c = p.id;
        salesInvoiceLine.c2g__Quantity__c = 1.000000;
        salesInvoiceLine.c2g__UnitPrice__c= 45.000000000;
        salesInvoiceLine.c2g__DeriveUnitPriceFromProduct__c = true;
        salesInvoiceLine.c2g__DeriveTaxRate1FromCode__c = true;
        salesInvoiceLine.c2g__CalculateTaxValue1FromRate__c = true;
        salesInvoiceLine.c2g__TaxValue1__c = 0;
        insert salesInvoiceLine;
        
        
        //post the sales invoice to create a transaction 
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = testInvoice1.Id;
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(context, reference);
        Test.stopTest();
        
        JVCO_DM_AccountWithCreditStatus b = new JVCO_DM_AccountWithCreditStatus();
        Database.executeBatch(b);
         
        
    }
}