public class JVCO_DisposeVenueQueueable implements Queueable 
{
    public Account accountRecord;
    public Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsDisplayMap;
    public Map<String, Date> mapTempIdJVCOAffiliationDate;
    public Map<String, String> mapTempIdJVCOAffiliationCloseReason;
    public Map<String, String> mapTempIdJVCOAffiliationAccId;
    public Integer processSize;


    public JVCO_DisposeVenueQueueable( Account accountRecordHolder, Map<String, Date> mapTempIdJVCOAffiliationDateHolder , Map<String, String> mapTempIdJVCOAffiliationCloseReasonHolder, Map<String, String> mapTempIdJVCOAffiliationAccIdHolder, Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsDisplayMapHolder, Integer processSizeHolder )
    {
        accountRecord = accountRecordHolder;
        mapTempIdJVCOAffiliationDate = mapTempIdJVCOAffiliationDateHolder;
        mapTempIdJVCOAffiliationCloseReason = mapTempIdJVCOAffiliationCloseReasonHolder;
        mapTempIdJVCOAffiliationAccId = mapTempIdJVCOAffiliationAccIdHolder;
        transferSelectionsDisplayMap = transferSelectionsDisplayMapHolder;
        processSize = processSizeHolder;
    }

    public void execute(QueueableContext context) 
    {
        Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsCheckerMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();

        //Start LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
        if(!mapTempIdJVCOAffiliationDate.isEmpty()) {
            for(String kAffli: mapTempIdJVCOAffiliationDate.keySet()){
                transferSelectionsDisplayMap.get(kAffli).affiliationRecord.JVCO_End_Date__c = Date.valueOf(mapTempIdJVCOAffiliationDate.get(kAffli));
            }
        }

        if(!mapTempIdJVCOAffiliationCloseReason.isEmpty()) {
            for(String kAffli: mapTempIdJVCOAffiliationCloseReason.keySet()){
                transferSelectionsDisplayMap.get(kAffli).affiliationRecord.JVCO_Closure_Reason__c = String.valueOf(mapTempIdJVCOAffiliationCloseReason.get(kAffli));
            }
        }

        if(!mapTempIdJVCOAffiliationAccId.isEmpty()) {
            for(String kAffli: mapTempIdJVCOAffiliationAccId.keySet()){
                transferSelectionsDisplayMap.get(kAffli).accountId = String.valueOf(mapTempIdJVCOAffiliationAccId.get(kAffli));
            }
        }
        //End LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values

        if(transferSelectionsDisplayMap.size() > 0) {
            // START jules.osberg.a.pablo     23/10/2017      GREEN-25128
            Integer ctr2 = 0;

            for(String tsdmKey : transferSelectionsDisplayMap.keySet()) {
                if(ctr2 < processSize){
                    transferSelectionsCheckerMap.put(tsdmKey, transferSelectionsDisplayMap.remove(tsdmKey));
                    ctr2++;
                }
            }
            //Start LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
            mapTempIdJVCOAffiliationDate.clear();
            if(transferSelectionsDisplayMap.size() > 0) {  
                for(String affKey: transferSelectionsDisplayMap.keySet()){
                    mapTempIdJVCOAffiliationDate.put(affKey,Date.valueOf(transferSelectionsDisplayMap.get(affKey).affiliationRecord.JVCO_End_Date__c));
                }
            }

            mapTempIdJVCOAffiliationCloseReason.clear();
            if(transferSelectionsDisplayMap.size() > 0) {  
                for(String affKey: transferSelectionsDisplayMap.keySet()){
                    mapTempIdJVCOAffiliationCloseReason.put(affKey,String.valueOf(transferSelectionsDisplayMap.get(affKey).affiliationRecord.JVCO_Closure_Reason__c));
                }
            }

            mapTempIdJVCOAffiliationAccId.clear();
            if(transferSelectionsDisplayMap.size() > 0) {  
                for(String affKey: transferSelectionsDisplayMap.keySet()){
                    mapTempIdJVCOAffiliationAccId.put(affKey,String.valueOf(transferSelectionsDisplayMap.get(affKey).accountId));
                }
            }

            JVCO_VenueTransfer venuetransfer = new JVCO_VenueTransfer();
            Boolean retAcquisition =  venuetransfer.confirmDisposals(transferSelectionsCheckerMap, true);
 
            if(!transferSelectionsCheckerMap.isEmpty()) 
            {    
                if(retAcquisition) 
                {
                    if(!transferSelectionsDisplayMap.isEmpty()) 
                    {
                        // repeat 
                        if(!Test.isRunningTest())
                        {

                            System.enqueueJob(new JVCO_DisposeVenueQueueable( accountRecord, mapTempIdJVCOAffiliationDate , mapTempIdJVCOAffiliationCloseReason, mapTempIdJVCOAffiliationAccId, transferSelectionsDisplayMap,  processSize));
                        }
                    } 
                    else 
                    {
                        // end 
                        sendEmailCompletion(accountRecord);
                        accountRecord.JVCO_VenDisposalQueueFlag__c = false;
                        update accountRecord;
                    }
                } 
                else 
                {
                    // repeat stepNumber = 1;
                    if(!Test.isRunningTest())
                    {
                        System.enqueueJob(new JVCO_DisposeVenueQueueable( accountRecord, mapTempIdJVCOAffiliationDate , mapTempIdJVCOAffiliationCloseReason, mapTempIdJVCOAffiliationAccId, transferSelectionsDisplayMap,  processSize));
                    }

                }   
            }
        } 
        else 
        {
            //end
            sendEmailCompletion(accountRecord);
            accountRecord.JVCO_VenDisposalQueueFlag__c = false;
            update accountRecord;
        }

    }

    public static void sendEmailCompletion(Account accountRecord) 
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setTargetObjectId(userInfo.getUserID());
        mail.setSaveAsActivity(false);

        mail.setReplyTo('no-reply@salesforce.com');
        mail.setSenderDisplayName('Venue Disposal Queueable Processing');
        mail.setSubject('Venue Disposal Queueable Completed');

        String emailMsg = '';

        
            emailMsg += '<html><body>This is to notify you that the process to Dispose Venue  for ' + accountRecord.Name + ' (' +System.now() + ') has been completed' ;
        

        emailMsg += ' </body></html> ';

        //End
        
        mail.setHTMLBody(emailMsg);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    
}
