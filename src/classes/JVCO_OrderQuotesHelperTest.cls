/* ----------------------------------------------------------------------------------------------
    Name: JVCO_OrderQuotesHelperTest
    Description: Test Class for JVCO_OrderQuotesHelper

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    08-Aug-2018     0.1         Jules Pablo         Intial creation
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_OrderQuotesHelperTest {

    @testSetup static void setupTestData() {
        Product2 prod1 = new Product2();
        prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 83202-C';
        a1.Type = 'Key Account';
        insert a1;
        
        Account a2 = new Account();
        a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.Id);
        insert a2;

        Pricebook2 testPB2 = new Pricebook2();
        testPB2.name = 'Standard Price Book';
        testPB2.IsActive = true;
        insert testPB2;
        
        PriceBookEntry pbe = new PriceBookEntry();
        pbe= JVCO_TestClassObjectBuilder.createPriceBookEntry(prod1.Id);
        insert pbe;
        
        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 83202';
        o.AccountId = a2.Id;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Renewal';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        insert q;
        
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        insert ql;
        
        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();
        venueRecord.Name = 'test1';              
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = new JVCO_Affiliation__c();
        affilRecord.JVCO_Account__c = a2.Id;
        affilRecord.JVCO_Venue__c = venueRecord.id;   
        affilRecord.JVCO_Start_Date__c = system.today();   
        Insert  affilRecord;   
        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;   

        Opportunity opp = new Opportunity();
        opp.Id = o.Id;
        opp.Amount = 10;
        opp.StageName = 'Closed Won';
        opp.SBQQ__Contracted__c = true;
        update opp;
    }

    private static testMethod void testMethodOrderQuotesBatch() 
    {  
        Set<Id> idsToPass = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [SELECT Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT limit 1];
        Opportunity o = [SELECT Id, Amount, SBQQ__Ordered__c FROM Opportunity where Name = 'Test opportunity 83202' and AccountId =: a.Id];
        idsToPass.add(o.Id);

        Map<String, String> quoteStringMap = new Map<String, String>();

        List<Opportunity> opportunityList = Database.query(JVCO_OrderQuotesHelper.getQueryString(a, idsToPass));
        Test.startTest();
        JVCO_OrderQuotesHelper.executeOrderQuotes(opportunityList, quoteStringMap);

        //Opportunity updatedOpportunity = [SELECT Id, SBQQ__Ordered__c FROM Opportunity where Name = 'Test opportunity 83202' and Id = :o.Id];
        //System.assertEquals(updatedOpportunity.SBQQ__Ordered__c, true);
        Test.stopTest();
        
    }

    private static testMethod void testSendOrderingEmail() 
    {  
        Set<Id> idsToPass = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [SELECT Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT limit 1];
        Opportunity o = [SELECT Id, Amount, SBQQ__Ordered__c FROM Opportunity where Name = 'Test opportunity 83202' and AccountId =: a.Id];
        idsToPass.add(o.Id);

        Map<String, String> quoteStringMap = new Map<String, String>();

        //List<Opportunity> opportunityList = Database.query(JVCO_OrderQuotesHelper.getQueryString(a, idsToPass));

        Test.startTest();
        JVCO_OrderQuotesHelper.sendOrderingEmail(a, 1, true, true, quoteStringMap, true);
        //Opportunity updatedOpportunity = [SELECT Id, SBQQ__Ordered__c FROM Opportunity where Name = 'Test opportunity 83202' and Id = :o.Id];
        //System.assertEquals(updatedOpportunity.SBQQ__Ordered__c, true);
        Test.stopTest();
        
    }

    private static testMethod void testSendOrderingEmail2() 
    {  
        Set<Id> idsToPass = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [SELECT Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT limit 1];
        Opportunity o = [SELECT Id, Amount, SBQQ__Ordered__c FROM Opportunity where Name = 'Test opportunity 83202' and AccountId =: a.Id];
        idsToPass.add(o.Id);

        Map<String, String> quoteStringMap = new Map<String, String>();

        //List<Opportunity> opportunityList = Database.query(JVCO_OrderQuotesHelper.getQueryString(a, idsToPass));

        Test.startTest();
        JVCO_OrderQuotesHelper.sendOrderingEmail(a, 1, false, true, quoteStringMap, true);
        //Opportunity updatedOpportunity = [SELECT Id, SBQQ__Ordered__c FROM Opportunity where Name = 'Test opportunity 83202' and Id = :o.Id];
        //System.assertEquals(updatedOpportunity.SBQQ__Ordered__c, true);
        Test.stopTest();
        
    }

    private static testMethod void testSendOrderingEmail3() 
    {  
        Set<Id> idsToPass = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [SELECT Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT limit 1];
        Opportunity o = [SELECT Id, Amount, SBQQ__Ordered__c FROM Opportunity where Name = 'Test opportunity 83202' and AccountId =: a.Id];
        idsToPass.add(o.Id);

        Map<String, String> quoteStringMap = new Map<String, String>();

        //List<Opportunity> opportunityList = Database.query(JVCO_OrderQuotesHelper.getQueryString(a, idsToPass));

        Test.startTest();
        JVCO_OrderQuotesHelper.sendOrderingEmail(a, 1, true, false, quoteStringMap, true);
        //Opportunity updatedOpportunity = [SELECT Id, SBQQ__Ordered__c FROM Opportunity where Name = 'Test opportunity 83202' and Id = :o.Id];
        //System.assertEquals(updatedOpportunity.SBQQ__Ordered__c, true);
        Test.stopTest();
        
    }

    private static testMethod void testSendOrderingEmail4() 
    {  
        Set<Id> idsToPass = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [SELECT Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT limit 1];
        Opportunity o = [SELECT Id, Amount, SBQQ__Ordered__c FROM Opportunity where Name = 'Test opportunity 83202' and AccountId =: a.Id];
        idsToPass.add(o.Id);

        Map<String, String> quoteStringMap = new Map<String, String>();

        //List<Opportunity> opportunityList = Database.query(JVCO_OrderQuotesHelper.getQueryString(a, idsToPass));

        Test.startTest();
        JVCO_OrderQuotesHelper.sendOrderingEmail(a, 1, false, false, quoteStringMap, true);
        //Opportunity updatedOpportunity = [SELECT Id, SBQQ__Ordered__c FROM Opportunity where Name = 'Test opportunity 83202' and Id = :o.Id];
        //System.assertEquals(updatedOpportunity.SBQQ__Ordered__c, true);
        Test.stopTest();
        
    }

    private static testMethod void testMethodOrderQuotesQueueable() 
    {  
        Set<Id> idsToPass = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [SELECT Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT limit 1];
        Opportunity o = [SELECT Id, Amount, SBQQ__Ordered__c FROM Opportunity where Name = 'Test opportunity 83202' and AccountId =: a.Id];
        idsToPass.add(o.Id);

        Map<String, String> quoteStringMap = new Map<String, String>();

        List<Opportunity> opportunityList = Database.query(JVCO_OrderQuotesHelper.getQueryString(a, idsToPass));
        Test.startTest();
        System.enqueueJob(new JVCO_OrderQuotes_Queueable(a, new Map<Id, Opportunity>(opportunityList).keySet(), true));

        //Opportunity updatedOpportunity = [SELECT Id, SBQQ__Ordered__c FROM Opportunity where Name = 'Test opportunity 83202' and Id = :o.Id];
        //System.assertEquals(updatedOpportunity.SBQQ__Ordered__c, true);
        Test.stopTest();
        
    }
}