/* ----------------------------------------------------------------------------------------------
Name: JVCO_SmarterPayFailureLogic
Test Class: JVCO_SmarterPayFailureLogicTest
Description: Logic for failure of Income Direct Debit History(SmarterPay)

Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
04-May-2020  0.1         patrick.t.bautista    Intial creation - GREEN-35543
----------------------------------------------------------------------------------------------- */
public class JVCO_SmarterPayFailureLogic
{
    private static JVCO_SmarterPaymentFailureQueueable.JVCO_SmarterPaymentFailureWrapper spWrapper;
    /* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: 
    Inputs: List<PAYBASE2__Payment__c>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    27-Dec-2016 kristoffer.d.martin Initial version of function
    24-Oct-2017 patrick.t.bautista  Refactor Method
    ----------------------------------------------------------------------------------------------- */
    public static void processPaymentFailure(List<Income_Debit_History__c> paymentList, Boolean ifValidForRefund)
    {   
        spWrapper = new JVCO_SmarterPaymentFailureQueueable.JVCO_SmarterPaymentFailureWrapper();
        setCapsErrorCodes(ifValidForRefund);
        stopperMethod();
        setErrorDDPaymentIdSet(paymentList);
        if(!spWrapper.paymentIdSet.isEmpty()){
            setCashEntryLineMap();
            setInvoiceMap();
            setInvoiceInstallmentMap();
            updateReceiptCashEntryLineItemReason();
            if(!spWrapper.installmentlineAdd5daysSet.isEmpty()){
                for(c2g__codaInvoiceInstallmentLineItem__c ili : spWrapper.installmentlineAdd5daysSet){
                    ili.c2g__DueDate__c = update5bussinessdayWoutHolliday(ili.c2g__DueDate__c);
                    spWrapper.updatedInvInstallmentSet.add(ili);
                }
            }
            setTransactionLineList();
            setMatchingHistList();
        }
        if(!spWrapper.matchingHistList.isEmpty())
        {   
            //Run queueable job for unmatching of cash to invoice and match cash to refund
            System.enqueueJob(new JVCO_SmarterPaymentFailureQueueable(spWrapper.matchingHistList, spWrapper));
        }
    }
    
    private static void setErrorDDPaymentIdSet(List<Income_Debit_History__c> paymentList)
    {
        for(Income_Debit_History__c payment : [SELECT id, DD_Stage__c,
                                               DD_Code__c, DD_Failed__c,
                                               Income_Direct_Debit__c,
                                               Income_Direct_Debit__r.Account__c
                                               from Income_Debit_History__c
                                               WHERE Id IN: paymentList]) 
        {
            if(payment.DD_Stage__c == 'Failed' && payment.DD_Code__c != null && 
               payment.Income_Direct_Debit__c != null && payment.DD_Failed__c
               && spWrapper.errorCodesMap.containsKey(payment.DD_Code__c.toLowerCase()))
            {
                spWrapper.paymentIdSet.add(payment.Id);
                createRefundCashEntry(payment);
            }
        }
    }
    
    private static void setCashEntryLineMap()
    {   
        for(c2g__codaCashEntryLineItem__c cli : [SELECT Id, JVCO_Income_Debit_History__c,
                                                 JVCO_Income_Debit_History__r.DD_Stage__c,
                                                 JVCO_Income_Debit_History__r.DD_Code__c,
                                                 JVCO_Income_Debit_History__r.Income_Direct_Debit__c,
                                                 JVCO_Income_Debit_History__r.DD_Status__c,
                                                 JVCO_Income_Debit_History__r.DD_Collection_Date__c,
                                                 c2g__CashEntryValue__c,
                                                 c2g__CashEntry__r.c2g__Transaction__c,
                                                 c2g__CashEntry__c,JVCO_Payment_Status__c,
                                                 c2g__AccountReference__c,
                                                 c2g__Account__c
                                                 FROM c2g__codaCashEntryLineItem__c
                                                 WHERE JVCO_Income_Debit_History__c IN :spWrapper.paymentIdSet])
        {
            if(!spWrapper.ppIdToCLITotalAmtMap.containsKey(cli.JVCO_Income_Debit_History__c))
            {
                spWrapper.ppIdToCLITotalAmtMap.put(cli.JVCO_Income_Debit_History__c, 0);
            }
            //aggregated cash val = summation of all related payonomy payment cash entry lines
            Double aggCashVal = spWrapper.ppIdToCLITotalAmtMap.get(cli.JVCO_Income_Debit_History__c) + cli.c2g__CashEntryValue__c;
            spWrapper.ppIdToCLITotalAmtMap.put(cli.JVCO_Income_Debit_History__c, aggCashVal);
            
            if(!String.isblank(cli.c2g__CashEntry__r.c2g__Transaction__c))
            {
                spWrapper.transIdSet.add(cli.c2g__CashEntry__r.c2g__Transaction__c);
            }
            if(!String.isblank(cli.c2g__AccountReference__c))
            {
                spWrapper.accReferenceSet.add(cli.c2g__AccountReference__c);
            }
            spWrapper.cashEntryLineItemList.add(cli);
        }
    }
    
    private static void updateReceiptCashEntryLineItemReason()
    {
        for(c2g__codaCashEntryLineItem__c cli : spWrapper.cashEntryLineItemList)
        {   
            //Update all the Cash Entry Lines and add to List
            cli.JVCO_Payment_Status__c  = 'Failed';
            String errorCode = cli.JVCO_Income_Debit_History__r.DD_Code__c.toLowerCase();
            //Third Represent the move to deliberate
            Boolean checkIf3rdRepToDeliberate = cli.JVCO_Income_Debit_History__r.DD_Status__c == 'Third Represent' 
                && errorCode.contains('arudd-0') ? true : false;
            //Sets DD failed to true of instalment line if it's not in Third Represent
            Boolean ifDDFailed = cli.JVCO_Income_Debit_History__r.DD_Status__c != 'Third Represent' 
                && errorCode.contains('arudd-0') ? true : false;
            if(spWrapper.errorCodesMap.containsKey(errorCode))
            {
                String deliberateCode = spWrapper.errorCodesMap.get(errorCode).JVCO_Deliberate__c;
                //Boolean checkIfDDICA = spWrapper.errorCodesMap.get(errorCode).JVCO_REASON_CODE__c == 'DDICA';
                
                cli.JVCO_Payment_Failure_Reason__c = getPaymentFailureReason(deliberateCode, /*checkIfDDICA, */checkIf3rdRepToDeliberate);
                //Delete Installment Line and Cancel Payonomy Agreement
                setInstallmentAndAgreement(cli, deliberateCode, checkIf3rdRepToDeliberate, /*checkIfDDICA, */ifDDFailed);
            }
            spWrapper.updatedCashEntryLineItemList.add(cli);
        }
    }
    
    private static void setInvoiceInstallmentMap()
    {
        for(c2g__codaInvoiceInstallmentLineItem__c installmentLine : [SELECT Id, c2g__Invoice__r.Name,
                                                                      c2g__LineNumber__c, JVCO_Payment_Status__c,
                                                                      JVCO_DD_Failed__c, c2g__DueDate__c
                                                                      FROM c2g__codaInvoiceInstallmentLineItem__c
                                                                      WHERE c2g__Invoice__r.Name IN :spWrapper.accReferenceSet
                                                                      ORDER BY c2g__LineNumber__c DESC])
        {
            if(installmentLine.JVCO_Payment_Status__c != 'Unpaid')
            {
                if(!spWrapper.accRefToPaidInstallmentLineListMap.containsKey(installmentLine.c2g__Invoice__r.Name))
                {
                    spWrapper.accRefToPaidInstallmentLineListMap.put(installmentLine.c2g__Invoice__r.Name, new List<c2g__codaInvoiceInstallmentLineItem__c>{});
                }
                spWrapper.accRefToPaidInstallmentLineListMap.get(installmentLine.c2g__Invoice__r.Name).add(installmentLine);
            }
        }
    }
    
    private static void setTransactionLineList()
    {
        for(c2g__codaTransactionLineItem__c tli : [SELECT Id, c2g__Transaction__c, c2g__LineReference__c,
                                                   c2g__AccountValue__c, c2g__Account__c,
                                                   c2g__AccountOutstandingValue__c
                                                   FROM c2g__codaTransactionLineItem__c
                                                   WHERE c2g__Transaction__c IN :spWrapper.transIdSet
                                                   AND c2g__LineReference__c IN :spWrapper.accReferenceSet
                                                   AND c2g__LineType__c = 'Account'])
        {
            if(!spWrapper.accIdToReceiptTransLineListMap.containsKey(tli.c2g__Account__c))
            {
                spWrapper.accIdToReceiptTransLineListMap.put(tli.c2g__Account__c, new List<c2g__codaTransactionLineItem__c>{});
            }
            spWrapper.accIdToReceiptTransLineListMap.get(tli.c2g__Account__c).add(tli);
            spWrapper.receiptTransLineList.add(tli);
        }
    }
    
    private static void setMatchingHistList()
    {
        spWrapper.matchingHistList = [SELECT Id, c2g__MatchingReference__c, c2g__TransactionLineItem__c,
                                      c2g__Account__c, c2g__Period__c
                                      FROM c2g__codaCashMatchingHistory__c
                                      WHERE c2g__TransactionLineItem__c IN :spWrapper.receiptTransLineList
                                      AND c2g__Action__c = 'Match'
                                      AND c2g__UndoMatchingReference__c=NULL];                                                     
    }
    
    private static void createRefundCashEntry(Income_Debit_History__c payment)
    {
        c2g__codaCashEntry__c refundCash = new c2g__codaCashEntry__c();
        refundCash.c2g__Account__c = payment.Income_Direct_Debit__r.Account__c;
        refundCash.c2g__Date__c = Date.today();
        refundCash.JVCO_Income_Debit_History__c = payment.Id;
        refundCash.ffcash__DerivePeriod__c = true;
        refundCash.c2g__Type__c = 'Refund';
        refundCash.JVCO_Receipt_reversal_failed__c = true;
        refundCash.c2g__PaymentMethod__c = 'Direct Debit';
        
        spWrapper.ppIdToRefundMap.put(payment.Id, refundCash);
    }
    
    private static String getPaymentFailureReason(String deliberateCode, /*Boolean checkIfDDICA, */Boolean checkIf3rdRepToDeliberate)
    {
        String paymentFailureReason = '';
        //Deliberate
        if(deliberateCode == 'Deliberate' || checkIf3rdRepToDeliberate)
        {   
            paymentFailureReason = /*checkIfDDICA ? 'Direct Debit Indemnity Claim' : */'Deliberative Failed Direct Debit Collection';
            //Non Deliberate
        }else if(deliberateCode == 'Non-Deliberate')
        {
            paymentFailureReason = 'Non-Deliberative Failed Direct Debit Collection';
        }
        
        return paymentFailureReason;
    }
    
    private static void setInstallmentAndAgreement(c2g__codaCashEntryLineItem__c cli, String deliberateCode, Boolean checkIf3rdRepToDeliberate, /*Boolean checkIfDDICA,*/ Boolean ifDDFailed)
    {   
        Income_Direct_Debit__c incomeDD = new Income_Direct_Debit__c();
        incomeDD.Id = cli.JVCO_Income_Debit_History__r.Income_Direct_Debit__c;
        spWrapper.updatedIncomeDDSet.add(incomeDD);
        
        if(deliberateCode == 'Deliberate' || checkIf3rdRepToDeliberate)
        {   
            incomeDD.DD_Status__c = 'Cancelled';
            //if(!checkIfDDICA){
                incomeDD.Send_DD_Documentation__c = 'Direct Debit Default';
            //}
            if(spWrapper.acctReferenceToInvIdMap.containsKey(cli.c2g__AccountReference__c)){
                c2g__codaInvoice__c sInv = spWrapper.acctReferenceToInvIdMap.get(cli.c2g__AccountReference__c);
                sInv.JVCO_Invoice_Group__c = null;
                sInv.JVCO_Payment_Method__c = null;
                spWrapper.sInvToUpdateSet.add(sInv);
            }
        }
        
        if(spWrapper.accRefToPaidInstallmentLineListMap.containsKey(cli.c2g__AccountReference__c))
        {
            for(c2g__codaInvoiceInstallmentLineItem__c instalmentLine : spWrapper.accRefToPaidInstallmentLineListMap.get(cli.c2g__AccountReference__c)){
                if(cli.JVCO_Income_Debit_History__r.DD_Collection_Date__c == instalmentLine.c2g__DueDate__c){
                    if(ifDDFailed){
                        spWrapper.installmentlineAdd5daysSet.add(instalmentLine);
                    }
                    else{
                        spWrapper.updatedInvInstallmentSet.add(instalmentLine);
                    }
                }
            }
        }
    }
    
    public static Date update5bussinessdayWoutHolliday(Date dueDate)
    {
        Set<Date> gbHolidayDateSet = new Set<Date>();
        for(JVCO_GBHoliday__c gbHoliday : [SELECT Id, JVCO_HolidayDate__c
                                           FROM JVCO_GBHoliday__c 
                                           WHERE JVCO_HolidayDate__c >= : dueDate
                                           AND JVCO_HolidayDate__c <= : dueDate.addMonths(1)
                                           AND JVCO_Is_Weekend__c = false])
        {
            gbHolidayDateSet.add(gbHoliday.JVCO_HolidayDate__c);
        }
        
        Date computedCollectionDate = dueDate;
        Date firstMonday = Date.newInstance(1990, 1, 1);
        Integer workingDays = 0;
        integer collectionInterval = 5;
        while(workingDays < collectionInterval)
        {
            computedCollectionDate = computedCollectionDate.addDays(1);
            if(Math.mod(firstMonday.daysBetween(computedCollectionDate), 7) < 5)
            {  
                if(!gbHolidayDateSet.contains(computedCollectionDate) || 
                   (gbHolidayDateSet.contains(computedCollectionDate) && 
                    workingDays == (collectionInterval - 1)))
                {
                    workingDays++;
                }
            }
        }
        return computedCollectionDate;
    }
    
    @TestVisible
    private static void setInvoiceMap()
    {
        for(c2g__codaInvoice__c inv : [SELECT Id, Name,
                                       JVCO_Payment_Method__c,
                                       JVCO_Invoice_Group__c
                                       FROM c2g__codaInvoice__c 
                                       WHERE Name IN :spWrapper.accReferenceSet])
        {
            spWrapper.acctReferenceToInvIdMap.put(inv.Name, inv);
        }
    }
    
    private static void setCapsErrorCodes(Boolean ifValidForRefund)
    {
        for(JVCO_Payonomy_Error_Codes__c errorCode : JVCO_Payonomy_Error_Codes__c.getall().values()){
            if(!spWrapper.errorCodesMap.containsKey(errorCode.JVCO_REASON_CODE__c) && errorCode.JVCO_Cash_Refund__c == ifValidForRefund){
                spWrapper.errorCodesMap.put(errorCode.JVCO_REASON_CODE__c.toLowerCase(), errorCode);
            }
        }
    }
    
    private static void stopperMethod()
    {
        SBQQ.TriggerControl.disable();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        JVCO_FFUtil.stopAccountDLRSHandler = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        //JVCO_DDMandateHandler.stopDDMandateHandlerBeforeUpdate = true;
    }
    
    public static void processAUDDISandADDACSCancellation(List<Income_Debit_History__c> incomeDDHistories){
        stopperMethod();
        //AUDDIS and ADDACS
        Set<Income_Direct_Debit__c> iddToUpdateSet = new Set<Income_Direct_Debit__c>();
        for(Income_Direct_Debit__c incomeDD : [SELECT Id,
                                               DD_Status__c
                                               FROM Income_Direct_Debit__c
                                               WHERE Id IN (SELECT Income_Direct_Debit__c 
                                                            FROM Income_Debit_History__c
                                                            WHERE Id IN : incomeDDHistories)])
        {
            incomeDD.DD_Status__c = 'Cancelled';
            iddToUpdateSet.add(incomeDD);
        }
        Set<c2g__codaInvoice__c> invoiceToUpdateSet = new Set<c2g__codaInvoice__c>();
        for(c2g__codaInvoice__c invoice : [SELECT Id,
                                           JVCO_Invoice_Group__c,
                                           JVCO_Payment_Method__c
                                           FROM c2g__codaInvoice__c
                                           WHERE JVCO_Invoice_Group__c IN (SELECT Invoice_Group__c
                                                                           FROM SMP_DirectDebit_GroupInvoice__c
                                                                           WHERE Income_Direct_Debit__c IN : iddToUpdateSet
                                                                           AND Invoice_Group__c != null)])
        {
            invoice.JVCO_Invoice_Group__c = null;
            invoice.JVCO_Payment_Method__c = null;
            invoiceToUpdateSet.add(invoice);
        }
        try
        {
            update new List<c2g__codaInvoice__c>(invoiceToUpdateSet);
            update new List<Income_Direct_Debit__c>(iddToUpdateSet);
        }catch(Exception e)
        {
            //catchErrorLogs(e);
        }
    }
}