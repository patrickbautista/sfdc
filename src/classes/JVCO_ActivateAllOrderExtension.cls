/* ------------------------------------------------------------------------------------------------
   Name:            JVCO_ActivateAllOrderExtension
   Description:     Extension class for JVCO_ActivateAllOrder.page

   Date            Version     Author                    Summary of Changes 
   -----------     -------     -----------------            ---------------------------------------
   19-Sep-2018     0.1         jules.osberg.a.pablo     Initial Version
   06-Dec-2018     0.2         rhys.j.c.dela.cruz           GREEN-33362 - Edited criteria to accept Not Needed value
------------------------------------------------------------------------------------------------ */
public class JVCO_ActivateAllOrderExtension
{
    private ApexPages.StandardSetController stdCtrl;
    private Account accountRecord;
    public boolean isAllCompleted {get; set;} 
    private Integer initialProcessSize;

    @TestVisible private static boolean runAsQueueable = false;
  @testVisible private static Boolean testError = false;

    public JVCO_ActivateAllOrderExtension(ApexPages.StandardSetController ssc) {
        stdCtrl = ssc;
        isAllCompleted = true;
        Id accountId = Id.valueOf(ApexPages.currentPage().getParameters().get('id'));
        accountRecord = [SELECT Id, Type FROM Account WHERE Id = :accountId];   

       initialProcessSize = 20;
    if((Integer)JVCO_Constants__c.getOrgDefaults().JVCO_Initial_Activate_Order_Batch_Size__c != null) {
          initialProcessSize = (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_Initial_Activate_Order_Batch_Size__c;
      }
 
    }

    public PageReference activate() {
      List<Opportunity> relatedOppList = [SELECT Id, SBQQ__PrimaryQuote__c, SBQQ__PrimaryQuote__r.Name FROM Opportunity WHERE SBQQ__Contracted__c = TRUE AND SBQQ__Ordered__c = FALSE AND Amount != 0 AND Amount != NULL AND Account.Id = :accountRecord.Id AND SBQQ__PrimaryQuote__c != null];

      if(!relatedOppList.isEmpty() && relatedOppList != null) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You cannot activate orders as the following ' + relatedOppList.size() + ' quotes have been contracted but have not generated an order. To rectify, please go to the Opportunity related to these quotes, check \'Ordered\' and save the record.'));
        String errorOppRecords = '';
        for(Opportunity oppRecord :relatedOppList) {
          errorOppRecords += oppRecord.SBQQ__PrimaryQuote__r.Name + '<br/>';
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorOppRecords));
        isAllCompleted = false;
      }  

      if(isAllCompleted) {
        Map<Id, Order> orderMap = new Map<Id, Order>([SELECT Id, Status, AccountId, SBQQ__PriceCalcStatus__c FROM Order WHERE AccountId = :accountRecord.Id AND Status = 'Draft']);  
        for(Order orderRecord :orderMap.values()) {
          if(orderRecord.SBQQ__PriceCalcStatus__c != 'Completed' && orderRecord.SBQQ__PriceCalcStatus__c != 'Not Needed') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You cannot activate the Orders until the Price Calculation Status is Completed. Please wait and Activate All once completed.'));
            isAllCompleted = false;
            //break;
            return null;
          }
        }

        if(isAllCompleted) {
 
          if((orderMap.size() > initialProcessSize) || (runAsQueueable && Test.isRunningTest())) {
            System.enqueueJob(new JVCO_ActivateAllOrderQueueable(orderMap, true));
          } else {
            List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
            Savepoint sp = Database.setSavepoint();
            List<sObject> updateOrderList = new List<sObject>();
          for(Order orderRecord :orderMap.values()) {
            if(orderRecord.SBQQ__PriceCalcStatus__c == 'Completed' || orderRecord.SBQQ__PriceCalcStatus__c == 'Not Needed') {
                  orderRecord.Status = 'Activated';
                  updateOrderList.add(orderRecord);
              }  
          }
          if(Test.isRunningTest() && testError){ 
                        List<sObject> lstObject = new List<sObject>();
                        updateOrderList.add(new Opportunity());                               
                    }
          List<Database.SaveResult> res = Database.update(updateOrderList,false);
                    for(Integer i = 0; i < updateOrderList.size(); i++) {
                        Database.SaveResult srOppList = res[i];
                        sObject origrecord = updateOrderList[i];
                        if(!srOppList.isSuccess()) {
                            Database.rollback(sp);
                            System.debug('Update sObject fail: ');
                            for(Database.Error objErr:srOppList.getErrors()) {
                                ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                                /*
                                errLog.Name = String.valueOf(origrecord.ID);
                                errLog.JVCO_FailedRecordObjectReference__c = String.valueOf(origrecord.ID);
                                errLog.blng__FullErrorLog__c = string.valueof(objErr.getMessage());
                                errLog.blng__ErrorCode__c = string.valueof(objErr.getStatusCode());
                                errLog.JVCO_ErrorBatchName__c = 'JVCO_ActivateAllOrderExtension: Activate All Orders';
                                errLogList.add(errLog);   
                                */

                                errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(origrecord.ID);
                                errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                                errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                                errLog.ffps_custRem__Message__c = 'JVCO_ActivateAllOrderExtension: Activate All Orders';
                                errLogList.add(errLog);   
                            }
                        }
                    }

                    if(!errLogList.isempty()) {
                      try {
                          insert errLogList;
                      } catch(Exception ex) {
                          System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());                           
                      }   
                    }     
          }
        }
        return new PageReference('/' + accountRecord.Id);
    } else {
      return null;
    }   
    }

    public PageReference deactivate() {
      Map<Id, Order> orderMap = new Map<Id, Order>([SELECT Id, Account.Id, JVCO_Invoiced__c, Status FROM Order WHERE Account.Id =:accountRecord.Id AND Status = 'Activated' AND JVCO_Invoiced__c = false]);  

      if((orderMap.size() > initialProcessSize) || (runAsQueueable && Test.isRunningTest()))  {
        System.enqueueJob(new JVCO_ActivateAllOrderQueueable(orderMap, false));
      } else {
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        Savepoint sp = Database.setSavepoint();
        List<sObject> updateOrderList = new List<sObject>();
        for(Order orderRecord :orderMap.values()) {
           orderRecord.Status = 'Draft';
           updateOrderList.add(orderRecord);
        }
        if(Test.isRunningTest() && testError){ 
                List<sObject> lstObject = new List<sObject>();
                updateOrderList.add(new Opportunity());                               
            }
        List<Database.SaveResult> res = Database.update(updateOrderList,false);
          for(Integer i = 0; i < updateOrderList.size(); i++) {
              Database.SaveResult srOppList = res[i];
              sObject origrecord = updateOrderList[i];
              if(!srOppList.isSuccess()) {
                  Database.rollback(sp);
                  System.debug('Update sObject fail: ');
                  for(Database.Error objErr:srOppList.getErrors()) {
                      ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                      /*
                      errLog.Name = String.valueOf(origrecord.ID);
                      errLog.JVCO_FailedRecordObjectReference__c = String.valueOf(origrecord.ID);
                      errLog.blng__FullErrorLog__c = string.valueof(objErr.getMessage());
                      errLog.blng__ErrorCode__c = string.valueof(objErr.getStatusCode());
                      errLog.JVCO_ErrorBatchName__c = 'JVCO_ActivateAllOrderExtension: Deactivate All Orders';
                      errLogList.add(errLog);   
                      */

                      errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(origrecord.ID);
                      errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                      errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                      errLog.ffps_custRem__Message__c = 'JVCO_ActivateAllOrderExtension: Deactivate All Orders';
                      errLogList.add(errLog);  
                  }
              }
          }

          if(!errLogList.isempty()) {
              try {
                  insert errLogList;
              } catch(Exception ex) {
                  System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());           
              }   
          }
      }
      return new PageReference('/' + accountRecord.Id);
    }

    public PageReference returnToAccount() {
        return new PageReference('/' + accountRecord.Id);
    }
}