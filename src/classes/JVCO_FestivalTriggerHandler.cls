/* ----------------------------------------------------------------------------------------------------------
Name:            JVCO_FestivalTriggerHandler.cls
Description:     Trigger handler for JVCO_FestivalTrigger.trigger
     

Date             Version     Author                            Summary of Changes 
-----------      -------     -----------------           -------------------------------------------
06-Apr-2018      0.1        robert.j.b.lacatan              Initial version of code
---------------------------------------------------------------------------------------------------------- */

public class JVCO_FestivalTriggerHandler{
    
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_FestivalTrigger__c : false;

    public static void onBeforeInsert(List<JVCO_Festival__c> festivalList){
         if(!skipTrigger) 
        {
            copyStartDate(festivalList);
        }
        
    }
    
    public static void onBeforeUpdate(List<JVCO_Festival__c> festivalList){
    
         if(!skipTrigger) 
        {
            copyStartDate(festivalList);
        }
        
    }
    
    
    //GREEN-30795
    public static void copyStartDate(List<JVCO_Festival__c> festivalList){
    
        if(!festivalList.isEmpty()){
            for(JVCO_Festival__c festival: festivalList){
                festival.JVCO_StartDate_Text__c = string.valueof(festival.JVCO_Start_Date__c.Day()) + '/' + string.valueof(festival.JVCO_Start_Date__c.Month()) + '/' + string.valueof(festival.JVCO_Start_Date__c.Year());
            }
        }
    }
    
    
        
        
}