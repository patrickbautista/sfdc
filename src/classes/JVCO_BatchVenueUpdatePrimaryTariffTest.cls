@isTest
private class JVCO_BatchVenueUpdatePrimaryTariffTest
{
    @testSetup
    static void dataSetup() {
        
        List<JVCO_Venue__c> venueList = new List<JVCO_Venue__c>();
        JVCO_Venue__c venue = new JVCO_Venue__c();
        Integer i = 0;
        List<JVCO_Primary_Tariff_Mapping__c> allCats2Insert = new List<JVCO_Primary_Tariff_Mapping__c>();
        
        JVCO_Primary_Tariff_Mapping__c cat1 = new JVCO_Primary_Tariff_Mapping__c();
        cat1.Name = 'Activity Centre';
        cat1.JVCO_Primary_Tariff__c  = 'GP_PRS';
        allCats2Insert.add(cat1);
        
        JVCO_Primary_Tariff_Mapping__c cat2 = new JVCO_Primary_Tariff_Mapping__c();
        cat2.Name = 'Aircraft';
        cat2.JVCO_Primary_Tariff__c  = 'AC_PRS';
        allCats2Insert.add(cat2);
        
        JVCO_Primary_Tariff_Mapping__c cat3 = new JVCO_Primary_Tariff_Mapping__c();
        cat3.Name = 'Airport';
        cat3.JVCO_Primary_Tariff__c  = 'GP_PRS';
        allCats2Insert.add(cat3);
        
        insert allCats2Insert;
        
        Set<String> allCats = JVCO_Primary_Tariff_Mapping__c.getAll().keySet();
        Set<String> notAllowedCats = new Set<String>{'Amateur Theatrical Performances', 'Concert Venues', 'Conference Centre', 'Fashion Shows', 'Gymnastics Clubs', 'Mobile DJs', 'Puppet/Marionette/Magic Shows', 'Single Events', 'Theatrical Productions', 'Election Campaigns', 'Waiting Rooms', 'Gymnastic Club'};
            for(String catName : allCats)
        {
            if(notAllowedCats.contains(catName))
                continue;
            venue = new JVCO_Venue__c();
            venue.JVCO_City__c = 'EDINBURGH';
            venue.JVCO_County__c = 'Midlothian';
            venue.Name = 'Zoo Southside Bar';
            venue.JVCO_Merge_Id__c = 'JVCO_V_NM_zzzzz' + i;
            //venue.JVCO_Phone__c = '1305784287';
            venue.JVCO_Postcode__c = 'EH8 9ER';
            //venue.JVCO_PPL_Id__c = '1720486';
            venue.JVCO_PRS_Id__c = '6666666' + i;
            venue.JVCO_Status__c = 'Active';
            venue.JVCO_Street__c = 'Southside Community Centre, 117';
            venue.JVCO_Venue_Type__c = catName;
            venue.JVCO_Internal_Id__c = 'Venue-7777777';
            venueList.add(venue);
            i++;
        }
        insert venueList;
        for(JVCO_Venue__c venueRec : venueList)
        {
            venueRec.JVCO_Primary_Tariff__c = null;
        }
        update venueList;
    }
    static testMethod void batchTest()
    {
        Set<String> allCats = JVCO_Primary_Tariff_Mapping__c.getAll().keySet();
        Integer insertedVenuesCount = allCats.size();
        System.debug('Data Setup: ' + [SELECT Id,JVCO_Primary_Tariff__c , JVCO_Venue_Type__c FROM JVCO_Venue__c WHERE JVCO_Primary_Tariff__c = null AND JVCO_Venue_Type__c != null]);
        Test.startTest();
        Id batchJobId = Database.executeBatch(new JVCO_BatchVenueUpdatePrimaryTariff(), 200);
        Test.stopTest();
        List<JVCO_Venue__c> venuesWithPopulatedTariff = [SELECT Id 
                                                         FROM JVCO_Venue__c
                                                         WHERE JVCO_Primary_Tariff__c != NULL];
        //System.assertEquals(insertedVenuesCount, venuesWithPopulatedTariff.size());
    }
}