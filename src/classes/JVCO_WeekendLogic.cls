public class JVCO_WeekendLogic {

    public static void setWeekendWork(List<c2g__codaTransaction__c> transactionList){
        
        
        for(c2g__codaTransaction__c trans : transactionList)
        {
            if(trans.c2g__TransactionDate__c != null)
            {
                //Get the current date
                DateTime currentDate = trans.c2g__TransactionDate__c;
                
                Integer numberOfDays = Date.daysInMonth(currentDate.year(), currentDate.month());
                Date lastDayOfMonth = Date.newInstance(currentDate.year(), currentDate.month(), numberOfDays); 
                //Start GREEN-24707 Week should be calculated by Sunday of that CreatedDate patrick.t.bautista 11/6/2017
                while(!currentDate.format('E').equalsIgnoreCase('Sun'))
                {
                    currentDate = Date.newInstance(currentDate.year(), currentDate.month(), currentDate.day()).addDays(1);
                    if (currentDate.isSameDay(lastDayOfMonth)) {
                    break;
                    }
                }//End 
                trans.JVCO_Week_End__c = date.newInstance(currentDate.year(), currentDate.month(), currentDate.day());
            }       
        }
    }
}