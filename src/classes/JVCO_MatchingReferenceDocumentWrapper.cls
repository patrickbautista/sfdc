public virtual class JVCO_MatchingReferenceDocumentWrapper
{
	protected Map<Id, c2g__codaTransactionLineItem__c> transLineMap;
	protected Map<Id, String> transLineIdToRefDocMap = new Map<Id, String>();
	protected Map<String, c2g__codaTransactionLineItem__c> refDocToMatchingRefTransLineMap = new Map<String, c2g__codaTransactionLineItem__c>();
	private Double writeOffLimit = c2g__codaCashMatchingSettings__c.getInstance().JVCO_Write_off_Limit__c;
	private List<ffps_custRem__Custom_Log__c> errorLogList = new List<ffps_custRem__Custom_Log__c>();
	private Id openPeriodId;
	
    public JVCO_MatchingReferenceDocumentWrapper()
    {
        setOpenPeriod();
    }
    
	protected void callMatchingAPI()
	{
		c2g.CODAAPICommon_8_0.Context context = new c2g.CODAAPICommon_8_0.Context();
		for(Id transLineId : transLineIdToRefDocMap.keySet())
		{
			String referenceDocument = transLineIdToRefDocMap.get(transLineId);
			if(refDocToMatchingRefTransLineMap.containsKey(referenceDocument))
			{
				c2g__codaTransactionLineItem__c matchFromTransLine = transLineMap.get(transLineId);
				c2g__codaTransactionLineItem__c matchToTransLine = refDocToMatchingRefTransLineMap.get(referenceDocument);

				c2g.CODAAPICashMatchingTypes_8_0.Configuration configuration = new c2g.CODAAPICashMatchingTypes_8_0.Configuration();
		        configuration.Account = c2g.CODAAPICommon.getRef(matchFromTransLine.c2g__Account__c, null);
		        configuration.MatchingDate = System.today();
		        configuration.MatchingCurrencyMode = c2g.CODAAPICashMatchingTypes_8_0.enumMatchingCurrencyMode.Account;
		        //Matching Period
		        Id periodId = matchFromTransLine.c2g__Transaction__r.c2g__Period__r.c2g__Closed__c ? openPeriodId : matchFromTransLine.c2g__Transaction__r.c2g__Period__c;
		        configuration.MatchingPeriod = c2g.CODAAPICommon.getRef(periodId, null);

		        List<c2g.CODAAPICashMatchingTypes_8_0.Item> items = new List<c2g.CODAAPICashMatchingTypes_8_0.Item>();
		        c2g.CODAAPICashMatchingTypes_8_0.Analysis analysisInfo = new c2g.CODAAPICashMatchingTypes_8_0.Analysis();

		        c2g.CODAAPICashMatchingTypes_8_0.Item item1 = new c2g.CODAAPICashMatchingTypes_8_0.Item();
		        c2g.CODAAPICashMatchingTypes_8_0.Item item2 = new c2g.CODAAPICashMatchingTypes_8_0.Item();

		        item1.TransactionLineItem = c2g.CODAAPICommon.getRef(matchFromTransLine.Id, null);
		        item2.TransactionLineItem = c2g.CODAAPICommon.getRef(matchToTransLine.Id, null);

		        Double item1Amount = matchFromTransLine.c2g__AccountOutstandingValue__c;
		        Double item2Amount = matchToTransLine.c2g__AccountOutstandingValue__c;
		        Decimal difference = item1Amount + item2Amount;
		        Boolean overPay = Math.abs(item1Amount) > Math.abs(item2Amount);
	            //Write-Off
	            if(difference != 0 && Math.abs(difference) <= writeOffLimit)
	            {
	            	item1.Paid = item1Amount;
	            	item2.Paid = item1Amount * -1;
	            	item2.WriteOff = difference.setScale(2);
	            	System.debug(difference);
	            	analysisInfo.WriteOffGla = c2g.CODAAPICommon.getRef(matchFromTransLine.c2g__OwnerCompany__r.c2g__CustomerWriteOff__c, null);
	            }else
	            {
	            	item1.Paid = overPay ? item2Amount * -1: item1Amount;
	            	item2.Paid = overPay ? item2Amount : item1Amount * -1;
	            }
	            items.add(item1);
	            items.add(item2);
	            try
	            {
	            	c2g.CODAAPICashMatching_8_0.Match(context, configuration, items, analysisInfo);
	            }catch(Exception e)
	            {
	            	errorLogList.add(createErrorLog(transLineId, 'BM-001', e.getMessage(), 'Background Auto Matching'));
	            }
			}
		}
        insert errorLogList;
	}

	@TestVisible
	private ffps_custRem__Custom_Log__c createErrorLog(Id refId, String errCode, String errMessage, String batchName)
	{
		ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
        errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(refId);
        errLog.ffps_custRem__Detail__c = errMessage;
        errLog.ffps_custRem__Message__c = batchName;
        errLog.ffps_custRem__Grouping__c = errCode;

        return errLog;
	}

	private void setOpenPeriod()
	{
        try
        {
            this.openPeriodId = [SELECT ID
								FROM c2g__codaPeriod__c 
								WHERE c2g__Closed__c = FALSE
								AND c2g__AR__c = FALSE
								AND c2g__Cash__c = FALSE
								AND c2g__EndDate__c >= TODAY
								AND c2g__OwnerCompany__r.Name = 'PPL PRS Ltd.'
								ORDER BY NAME ASC
								LIMIT 1].Id;
        }catch(Exception e){}
	}
}