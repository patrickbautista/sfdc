public with sharing class TriggerHandler_CardPaymentHistory {

    public static void OnBeforeInsert(List<Income_Card_Payment_History__c> newRecords){
        
    }
    
    public static void OnAfterInsert(Map<Id, Income_Card_Payment_History__c> newRecords){
        System.debug('####newRecords ' + newRecords);
        List<Income_Card_Payment_History__c> cardPaymentHistoryList = new List<Income_Card_Payment_History__c>();
        cardPaymentHistoryList = [SELECT Id, Income_Card_Payment__r.Account__r.Temp_Card_Email__c, Income_Card_Payment__r.Contact__r.Email, Income_Card_Payment__r.Payment_Reason__c, Income_Card_Payment__c, Payment_Status__c, Transaction_Type__c, Income_Card_Payment__r.Supplied_Email__c, Income_Card_Payment__r.Payment_Status__c, Income_Card_Payment__r.RP_Enabled__c, Income_Card_Payment__r.Invoice_Group__c FROM Income_Card_Payment_History__c
                                                                                                    WHERE Id IN: newRecords.keyset()];
        List<String> cardPaymentHistorysToLink = new List<String>();
        List<CardPaymentSalesInvoiceWrapper> wrapperList = new List<CardPaymentSalesInvoiceWrapper>();
        for(Income_Card_Payment_History__c cardPaymentHistory : cardPaymentHistoryList){
            System.debug('#### Payment_Status__c' + cardPaymentHistory.Payment_Status__c);
            System.debug('#### Payment_Reason__c' + cardPaymentHistory.Income_Card_Payment__r.Payment_Reason__c);
            if(cardPaymentHistory.Payment_Status__c == 'Authorised' || cardPaymentHistory.Payment_Status__c == 'Cancelled'){
                if(String.isNotBlank(cardPaymentHistory.Income_Card_Payment__r.Payment_Reason__c)){
                    CardPaymentSalesInvoiceWrapper wrapper = new CardPaymentSalesInvoiceWrapper();
                    List<Id> sInvIdList = new List<Id>();
                    string[] split = cardPaymentHistory.Income_Card_Payment__r.Payment_Reason__c.split(',');
                    for(string element : split)
                    {
                        cardPaymentHistorysToLink.add(element.replace(',', ''));
                        sInvIdList.add(element.replace(',', ''));
                    }
                    if(cardPaymentHistorysToLink.size() == 0){
                        cardPaymentHistorysToLink.add(cardPaymentHistory.Income_Card_Payment__r.Payment_Reason__c.replace(',', ''));
                    }

                    //cardPaymentHistory.Income_Card_Payment__r.Sales_Invoice_Numbers__c = 
                    wrapper.cardPaymentHistory = cardPaymentHistory;
                    wrapper.cardPayment = cardPaymentHistory.Income_Card_Payment__r;
                    wrapper.cardPaymentHistory.Invoices_Paid__c = cardPaymentHistory.Income_Card_Payment__r.Payment_Reason__c;
                    wrapper.transactionType = cardPaymentHistory.Transaction_Type__c;
                    wrapper.salesInvoiceIdList = sInvIdList;

                    wrapperList.add(wrapper);
                }
            }
        }

        wrapperList = SMP_PaymentScheduleHelper.checkCardPaymentHistory(wrapperList);

        List<c2g__codaInvoice__c> salesInvoiceResultsList = [SELECT Id, Name, c2g__OutstandingValue__c, JVCO_Invoice_Group__c, JVCO_Invoice_Group__r.Income_Card_Payment__c, JVCO_Invoice_Group__r.JVCO_invoiceNumbers__c, JVCO_Outstanding_Amount_to_Process__c 
                                                FROM c2g__codaInvoice__c 
                                                WHERE Id IN :cardPaymentHistorysToLink];
        for(string s :cardPaymentHistorysToLink){
            System.debug('##### cardPaymentHistorysToLink' + s);
        }
        
        System.debug('##### salesInvoiceResultsList' + salesInvoiceResultsList);
        Decimal totalAmount = 0;
        //JVCO_Invoice_Group__c invoiceGroup = createInvoiceGroup('Card Payment', false, salesInvoiceResultsList);
        for (c2g__codaInvoice__c salesInvoice :salesInvoiceResultsList) {
            totalAmount += salesInvoice.c2g__OutstandingValue__c;
        }

        totalAmount = totalAmount/100;

        List<JVCO_Invoice_Group__c> invoiceGroupList = new List<JVCO_Invoice_Group__c>();
        List<Income_Card_Payment_History__c> cphToUpdate = new List<Income_Card_Payment_History__c>();
        List<Income_Card_Payment__c> cpListToUpdate = new List<Income_Card_Payment__c>();
        Map<Id, c2g__codaInvoice__c> salesInvoiceResultsMap = new Map<Id, c2g__codaInvoice__c>();
        for(c2g__codaInvoice__c salesInvoice :salesInvoiceResultsList){
            salesInvoiceResultsMap.put(salesInvoice.Id, salesInvoice);
        }
        
      
        //salesInvoice.c2g__OutstandingValue__c  = salesInvoice.c2g__OutstandingValue__c * totalAmount;     

        for(CardPaymentSalesInvoiceWrapper wrapper :wrapperList){
            Income_Card_Payment__c cardPaymentToUpdate = new Income_Card_Payment__c();
            string salesInvoiceNumbers = '';
            cardPaymentToUpdate = wrapper.cardPayment;
            
            cphToUpdate.add(wrapper.cardPaymentHistory);
            List<c2g__codaInvoice__c> salesInvoiceListToAdd = new List<c2g__codaInvoice__c>();
            Decimal numberOfInvoiceToSplit = 0;
            numberOfInvoiceToSplit = wrapper.salesInvoiceIdList.Size();
            for(Id salesInvoiceId :wrapper.salesInvoiceIdList){
                system.debug('#### wrapper' +wrapper);

                System.debug('invoiceNum' + salesInvoiceResultsMap.get(salesInvoiceId).Name);
                salesInvoiceNumbers += salesInvoiceResultsMap.get(salesInvoiceId).Name + ',';
                if(string.isNotEmpty(salesInvoiceResultsMap.get(salesInvoiceId).JVCO_Invoice_Group__r.JVCO_invoiceNumbers__c)){
                    salesInvoiceResultsMap.get(salesInvoiceId).JVCO_Invoice_Group__r.JVCO_invoiceNumbers__c = salesInvoiceResultsMap.get(salesInvoiceId).JVCO_Invoice_Group__r.JVCO_invoiceNumbers__c +', '+ salesInvoiceResultsMap.get(salesInvoiceId).Name;
                }else{
                    System.debug('####' + salesInvoiceResultsMap.get(salesInvoiceId).Name);
                    salesInvoiceResultsMap.get(salesInvoiceId).JVCO_Invoice_Group__r.JVCO_invoiceNumbers__c = salesInvoiceResultsMap.get(salesInvoiceId).Name;
                }
                if(wrapper.transactionType == 'Refund' || wrapper.transactionType == 'Void'){
                    //salesInvoice.c2g__OutstandingValue__c = salesInvoice.c2g__OutstandingValue__c - (wrapper.cardPaymentHistory.Credit_Amount__c / numberOfInvoiceToSplit);
                    JVCO_Invoice_Group__c invoiceGroup = new JVCO_Invoice_Group__c();
                    invoiceGroup.Id = salesInvoiceResultsMap.get(salesInvoiceId).JVCO_Invoice_Group__c;
                    invoiceGroup.JVCO_invoiceNumbers__c = salesInvoiceNumbers;
                    invoiceGroup.Income_Card_Payment__c = wrapper.cardPayment.Id;
                    invoiceGroupList.add(invoiceGroup);
                    //wrapper.salesInvoiceList.add(salesInvoice);
                }
                else if(wrapper.transactionType == 'Cancelled'){
                    for(c2g__codaInvoice__c invoice :salesInvoiceResultsList){
                        if(salesInvoiceResultsMap.get(salesInvoiceId).Id == invoice.Id){
                            invoice.JVCO_Invoice_Group__c = null;
                        }
                    }
                }
                else{
                    //salesInvoice.c2g__OutstandingValue__c = salesInvoice.c2g__OutstandingValue__c - (wrapper.cardPaymentHistory.Amount__c / numberOfInvoiceToSplit);
                    JVCO_Invoice_Group__c invoiceGroup = new JVCO_Invoice_Group__c();
                    invoiceGroup.Id = salesInvoiceResultsMap.get(salesInvoiceId).JVCO_Invoice_Group__c;
                    invoiceGroup.Income_Card_Payment__c = wrapper.cardPayment.Id;
                    invoiceGroupList.add(invoiceGroup);
                    //wrapper.salesInvoiceList.add(salesInvoice);
                }
                
            }
            cardPaymentToUpdate.Sales_Invoice_Numbers__c = salesInvoiceNumbers;
            cpListToUpdate.add(cardPaymentToUpdate);
        }

        system.debug('#### about to update ');

        // Set<Income_Card_Payment__c> mysetILI = new Set<Income_Card_Payment__c>();
        // List<Income_Card_Payment__c> resultILI = new List<Income_Card_Payment__c>();
        // mysetILI.addAll(cpListToUpdate);
        // resultILI.addAll(mysetILI);
        Map<Id, Income_Card_Payment__c> cardPaymentMap = new Map<Id, Income_Card_Payment__c>();
        for(Income_Card_Payment__c cp :cpListToUpdate){
            cardPaymentMap.put(cp.Id, cp);
        }

        Map<Id, JVCO_Invoice_Group__c> igMap = new Map<Id, JVCO_Invoice_Group__c>();
        for(JVCO_Invoice_Group__c ig :invoiceGroupList){
            igMap.put(ig.Id, Ig);
        }

        Map<Id, Income_Card_Payment_History__c> cphMap = new Map<Id, Income_Card_Payment_History__c>();
        for(Income_Card_Payment_History__c icph :cphToUpdate){
            cphMap.put(icph.Id, icph);
        }

        update cardPaymentMap.values();
        update igMap.values();
        update salesInvoiceResultsList;
        update cphMap.values();

        
        List<Income_Card_Payment__c> cardPayments = new List<Income_Card_Payment__c>();
        List<Account> accounts = new List<Account>();
        List<JVCO_Invoice_Group__c> groupsForDeletion = new List<JVCO_Invoice_Group__c>();
        List<id> salesInvoiceIdsTemp = new List<id>();
        List<Income_Card_Payment__c> cardPaymentsToBeUpdated = new List<Income_Card_Payment__c>();
        
        system.debug('##### cardPaymentHistoryList '+cardPaymentHistoryList);


        for (Income_Card_Payment_History__c history : cardPaymentHistoryList) {
            Income_Card_Payment__c tempCard = new Income_Card_Payment__c();
            tempCard.Id = history.Income_Card_Payment__c;
            tempCard.Supplied_Email__c = history.Income_Card_Payment__r.Contact__r.Email;
            cardPayments.add(tempCard);
            Account acc = new Account();
            acc.Id = history.Income_Card_Payment__r.Account__c;
            acc.Temp_Card_Email__c = '';            
            system.debug('##### Payment_Reason__c '+history.Income_Card_Payment__r.Payment_Reason__c);
            system.debug('##### Payment_Status__c '+history.Payment_Status__c);

            if(history.Payment_Status__c == 'Cancelled')
            {
                if(history.Income_Card_Payment__r.Payment_Reason__c != null)
                {
                    string[] split = history.Income_Card_Payment__r.Payment_Reason__c.split(',');
                    system.debug('##### split '+split);

                    for(string element : split)
                    {
                        salesInvoiceIdsTemp.add(element.replace(',', ''));
                    }            
                    system.debug('##### salesInvoiceIdsTemp '+salesInvoiceIdsTemp);
                    Income_Card_Payment__c tempCardPayment = new Income_Card_Payment__c();
                    tempCardPayment.Id = history.Income_Card_Payment__c;
                    tempCardPayment.Payment_Reason__c = history.Income_Card_Payment__r.Payment_Reason__c;
                    tempCardPayment.Payment_Reason__c = '';

                    cardPaymentsToBeUpdated.add(tempCardPayment);   
                    for(JVCO_Invoice_Group__c ig :invoiceGroupList){
                        if(ig.Id == history.Income_Card_Payment__r.Invoice_Group__c){
                            JVCO_Invoice_Group__c groupInvoice = new JVCO_Invoice_Group__c();
                            groupInvoice.Id = ig.Id;

                            groupsForDeletion.add(groupInvoice);     
                        }
                    }
                }
                
            }
            accounts.add(acc);
            
        }
        system.debug('##### salesInvoiceIdsTemp '+salesInvoiceIdsTemp);

        system.debug('#### cardPayments' + cardPayments);

        system.debug('##### groupsForDeletion '+groupsForDeletion);

        Set<JVCO_Invoice_Group__c> deleteSET = new Set<JVCO_Invoice_Group__c>();
        deleteSET.addAll(groupsForDeletion);
        groupsForDeletion.addAll(deleteSET);
        
        List<JVCO_Invoice_Group__c> deleteList = new List<JVCO_Invoice_Group__c>();
        deleteList.addAll(deleteSET);
        // groupsForDeletion = [SELECT Id FROM JVCO_Invoice_Group__c WHERE Income_Card_Payment__c IN : salesInvoiceIdsTemp];
        system.debug('#### deleteList' + deleteList);
        system.debug('#### deleteSET' + deleteSET);

        if(deleteList.size() > 0)
        {
            delete deleteList;
            if(cardPaymentsToBeUpdated.size() > 0)
            {
                update cardPaymentsToBeUpdated;

            }
        }

        system.debug('#### history after edit = ' + cardPaymentHistoryList);
        update cardPayments;
        update accounts;
    }

    public static void OnBeforeUpdate(Map<Id, Income_Card_Payment_History__c> newRecords, Map<Id, Income_Card_Payment_History__c> oldRecords){
        
    }
    
    public static void OnAfterUpdate(Map<Id, Income_Card_Payment_History__c> newRecords, Map<Id, Income_Card_Payment_History__c> oldRecords){
        // System.debug('####OnBeforeUpdate ');    
        // List<Income_Card_Payment_History__c> cardPaymentHistoryList = new List<Income_Card_Payment_History__c>();
        // cardPaymentHistoryList = [SELECT Id, Income_Card_Payment__r.Account__r.Temp_Card_Email__c, Income_Card_Payment__r.Payment_Reason__c, Income_Card_Payment__c, Payment_Status__c, Transaction_Type__c, Income_Card_Payment__r.Supplied_Email__c, Income_Card_Payment__r.Payment_Status__c, Income_Card_Payment__r.RP_Enabled__c FROM Income_Card_Payment_History__c
        //                                                                                             WHERE Id IN: newRecords.keyset()];
        // List<Income_Card_Payment__c> cardPayments = new List<Income_Card_Payment__c>();
        // List<Account> accounts = new List<Account>();
        // List<JVCO_Invoice_Group__c> groupsForDeletion = new List<JVCO_Invoice_Group__c>();
        // List<id> salesInvoiceIdsTemp = new List<id>();
        // List<Income_Card_Payment__c> cardPaymentsToBeUpdated = new List<Income_Card_Payment__c>();

        // for (Income_Card_Payment_History__c history : cardPaymentHistoryList) {
           
        //     system.debug('##### Payment_Reason__c '+history.Income_Card_Payment__r.Payment_Reason__c);
        //     system.debug('##### Payment_Status__c '+history.Income_Card_Payment__r.Payment_Status__c);

        //     if(history.Income_Card_Payment__r.Payment_Status__c == 'Cancelled')
        //     {
        //         string[] split = history.Income_Card_Payment__r.Payment_Reason__c.split(',');
        //         system.debug('##### split '+split);

        //         for(string element : split)
        //         {
        //             salesInvoiceIdsTemp.add(element.replace(',', ''));
        //         }            
        //         system.debug('##### salesInvoiceIdsTemp '+salesInvoiceIdsTemp);
        //         Income_Card_Payment__c tempCardPayment = new Income_Card_Payment__c();
        //         tempCardPayment.Id = history.Income_Card_Payment__c;
        //         tempCardPayment.Payment_Reason__c = history.Income_Card_Payment__r.Payment_Reason__c;
        //         tempCardPayment.Payment_Reason__c = '';

        //         cardPaymentsToBeUpdated.add(tempCardPayment);
        //     }
            
        // }
        // system.debug('##### salesInvoiceIdsTemp '+salesInvoiceIdsTemp);

        // update cardPaymentsToBeUpdated;

        // system.debug('#### cardPayments' + cardPayments);

        // List<c2g__codaInvoice__c> salesInvoices =  [SELECT Id, JVCO_Invoice_Group__c FROM c2g__codaInvoice__c WHERE Id IN : salesInvoiceIdsTemp];
        
        // system.debug('#### salesInvoices' + salesInvoices);

        // for (c2g__codaInvoice__c temp : salesInvoices) 
        // {
        //     JVCO_Invoice_Group__c groupInvoice = new JVCO_Invoice_Group__c();
        //     groupInvoice.Id = temp.JVCO_Invoice_Group__c;

        //     groupsForDeletion.add(groupInvoice);        
        // }
        // system.debug('##### groupsForDeletion '+groupsForDeletion);

        // Set<JVCO_Invoice_Group__c> deleteSET = new Set<JVCO_Invoice_Group__c>();
        // deleteSET.addAll(groupsForDeletion);
        // groupsForDeletion.addAll(deleteSET);
        
        // List<JVCO_Invoice_Group__c> deleteList = new List<JVCO_Invoice_Group__c>();
        // deleteList.addAll(deleteSET);
        // // groupsForDeletion = [SELECT Id FROM JVCO_Invoice_Group__c WHERE Income_Card_Payment__c IN : salesInvoiceIdsTemp];
        // system.debug('#### deleteList' + deleteList);
        // system.debug('#### deleteSET' + deleteSET);

        // delete deleteList;

    }

    public class CardPaymentSalesInvoiceWrapper{
        public Income_Card_Payment_History__c cardPaymentHistory {get; set;}
        public Income_Card_Payment__c cardPayment {get; set;}
        public String transactionType {get; set;}
        public List<Id> salesInvoiceIdList {get; set;}
        public List<c2g__codaInvoice__c> salesInvoiceList {get; set;}
        public List<c2g__codaInvoiceInstallmentLineItem__c> installmentLineItemList {get; set;}
    }
}