/* ----------------------------------------------------------------------------------------------
Author: Recuerdo Bregente
Company: Accenture
Description: Test Class for JVCO_MergeController
<Date>      <Authors Name>       <Brief Description of Change> 
21-Aug-2017 Recuerdo Bregente  Initial version
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_MergeControllerTest {
   private static List<Account> licAccounts;
  
  @testSetup static void setUpTestData(){
        
    Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='JVCO_Venue__c'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q2);
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }

        JVCO_Venue__c venue = new JVCO_Venue__c();
        venue.Name = 'test venue 1';
        venue.JVCO_Field_Visit_Requested__c = false;
        venue.JVCO_Street__c = 'testStreet';
        venue.JVCO_City__c = 'testCoty';
        venue.JVCO_Postcode__c ='TN32 4SL';
        venue.JVCO_Country__c ='UK';

        insert venue;
        
        JVCO_Venue__c venue2 = new JVCO_Venue__c();
        venue2.Name = 'update venue';
        venue2.JVCO_Field_Visit_Requested__c = false;
        venue2.JVCO_Street__c = 'updateStreet';
        venue2.JVCO_City__c = 'updateCoty';
        venue2.JVCO_Postcode__c ='TN32 4SL';
        venue2.JVCO_Country__c ='UK';

        insert venue2;
        
        //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
        testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc.Dimension_1_Required__c = false;
        testGeneralLedgerAcc.Dimension_2_Required__c = true;
        testGeneralLedgerAcc.Dimension_3_Required__c = false;
        testGeneralLedgerAcc.Dimension_4_Required__c = false;
        testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
        testGeneralLedgerAcc.ownerid = testGroup.Id;
        testGeneralLedgerAcc.c2g__ReportingCode__c = '30030';
        insert testGeneralLedgerAcc;
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedgerAcc.id);
        insert taxCode;

        //Create Account
        Account testAccCust = new Account(Name='Test Customer Account');
        testAccCust.AccountNumber = '999999';
        testAccCust.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust.Type='Customer';
        testAccCust.RecordTypeId = accountRecordTypes.get('Customer Account');
        insert testAccCust;
        
        Account testAccCust2 = new Account(Name='Test Customer Account 2');
        testAccCust2.AccountNumber = '2222222';
        testAccCust2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust2.Type='Customer';
        testAccCust2.RecordTypeId = accountRecordTypes.get('Customer Account');
        insert testAccCust2;

         Contact c = JVCO_TestClassObjectBuilder.createContact(testAccCust.id);
        insert c;

        Contact con = JVCO_TestClassObjectBuilder.createContact(testAccCust2.id);
        con.LastName = 'LastName';
        con.FirstName = 'FirstName';
        con.Email = 'test@hotmail.com';
        insert con;

        
        Account testLicAcc1 = new Account(Name='Test Lic Account 1');
        testLicAcc1.AccountNumber = '987654';
        testLicAcc1.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testLicAcc1.c2g__CODAAllowDeleteInUse__c = false;
        testLicAcc1.c2g__CODACreditLimitEnabled__c = false;
        testLicAcc1.c2g__CODACreditStatus__c = 'Credit Allowed';
        testLicAcc1.c2g__CODADaysOffset1__c = 0.0;
        testLicAcc1.c2g__CODADiscount1__c = 0.0;
        testLicAcc1.c2g__CODAFederallyReportable1099__c = false;
        testLicAcc1.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testLicAcc1.c2g__CODAIntercompanyAccount__c = false;
        testLicAcc1.c2g__CODASalesTaxStatus__c = 'Taxable';
        testLicAcc1.JVCO_Customer_Account__c = testAccCust.id;
        testLicAcc1.RecordTypeId = accountRecordTypes.get('Licence Account');
        testLicAcc1.c2g__CODAOutputVATCode__c = taxCode.id;
        testLicAcc1.JVCO_Preferred_Contact_Method__c = 'Email';
        testLicAcc1.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        testLicAcc1.JVCO_Billing_Contact__c = c.id;
        testLicAcc1.JVCO_Licence_Holder__c = c.id;
        testLicAcc1.JVCO_Review_Contact__c = c.id;
        insert testLicAcc1;

        Account testLicAcc2 = new Account(Name='Test License Account 2');
        testLicAcc2.AccountNumber = '987623';
        testLicAcc2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testLicAcc2.c2g__CODAAllowDeleteInUse__c = false;
        testLicAcc2.c2g__CODACreditLimitEnabled__c = false;
        testLicAcc2.c2g__CODACreditStatus__c = 'Credit Allowed';
        testLicAcc2.c2g__CODADaysOffset1__c = 0.0;
        testLicAcc2.c2g__CODADiscount1__c = 0.0;
        testLicAcc2.c2g__CODAFederallyReportable1099__c = false;
        testLicAcc2.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testLicAcc2.c2g__CODAIntercompanyAccount__c = false;
        testLicAcc2.c2g__CODASalesTaxStatus__c = 'Taxable';
        testLicAcc2.JVCO_Customer_Account__c = testAccCust.id;
        testLicAcc2.RecordTypeId = accountRecordTypes.get('Licence Account');
        testLicAcc2.c2g__CODAOutputVATCode__c = taxCode.id;
        testLicAcc2.JVCO_Preferred_Contact_Method__c = 'Email';
        testLicAcc2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        testLicAcc2.JVCO_Billing_Contact__c = c.id;
        testLicAcc2.JVCO_Licence_Holder__c = c.id;
        testLicAcc2.JVCO_Review_Contact__c = c.id;
        insert testLicAcc2;
        
        licAccounts = new List<Account>();
        licAccounts.add(testLicAcc1);
        licAccounts.add(testLicAcc2);
        
        JVCO_Affiliation__c aff = new JVCO_Affiliation__c();
        aff.JVCO_Account__c = testLicAcc1.Id;
        aff.JVCO_Venue__c = venue.Id;
        aff.JVCO_Start_Date__c = Date.Today();
        insert aff;
        
        Opportunity opp = new Opportunity();
        opp.AccountId = testLicAcc2.Id;
        opp.Name = 'Opp test';
        opp.StageName = 'Quoting';
        opp.CloseDate = System.Today().addDays(5);
        insert opp;
        
        List<JVCO_BackgroundMatching__c> bgMatchingList = new List<JVCO_BackgroundMatching__c>();
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Trading Currency', JVCO_Default_Value__c = 'GBP'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Receivable Control', JVCO_Default_Value__c = '10040'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Output Vat Code', JVCO_Default_Value__c = 'GB-O-STD-LIC'));
        
        insert bgMatchingList;
        
        List<c2g__codaDimension3__c> dimensions = new List<c2g__codaDimension3__c>();
        dimensions.add(new c2g__codaDimension3__c(Name = 'Key', c2g__ReportingCode__c = 'Key'));
        dimensions.add(new c2g__codaDimension3__c(Name = 'Non Key', c2g__ReportingCode__c = 'Non Key'));
        insert dimensions;
    }
  
  /* ----------------------------------------------------------------------------------------------
    Author: Recuerdo Bregente
    Company: Accenture
    Description: Test JVCO_MergeController constructor
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>       <Brief Description of Change> 
    21-Aug-2017 Recuerdo Bregente  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    @isTest static void testConstructor()
    {
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.JVCO_AccountMerge')); 
        System.currentPageReference().getParameters().put('find', 'true');
        System.currentPageReference().getParameters().put('object', 'Account');
        System.currentPageReference().getParameters().put('hideFilter', '1');
        System.currentPageReference().getParameters().put('limit', '3');
        System.currentPageReference().getParameters().put('field1', 'RecordTypeId');
        System.currentPageReference().getParameters().put('op1', 'equals');
        System.currentPageReference().getParameters().put('value1', 'Licence Account');
        JVCO_MergeController mergeController = new JVCO_MergeController();
        mergeController.hideHeader = false;
        Boolean isHideHeader = mergeController.hideHeader;
        
        mergeController.hideFilter = false;
        Boolean isHideFilter = mergeController.hideFilter;
        
        mergeController.hideSidebar = false;
        Boolean isHideSidebar = mergeController.hideSidebar;
        
        mergeController.getShowQuery();
        mergeController.getError();
        
        System.assert(mergeController.getResults().size() > 0);
        mergeController.getResults()[0].setSelected(true);
        
        mergeController.selectMerge();
        
        Test.stopTest();
    }
    
    @isTest static void testResetQueryOnVenue()
    {
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.JVCO_AccountMerge')); 
        System.currentPageReference().getParameters().put('find', 'true');
        System.currentPageReference().getParameters().put('object', 'JVCO_Venue__c');
        System.currentPageReference().getParameters().put('hideFilter', '1');
        System.currentPageReference().getParameters().put('limit', '3');
        JVCO_MergeController mergeController = new JVCO_MergeController();
        mergeController.resetQuery();
        mergeController.previous();
        mergeController.cancel();
        
        Test.stopTest();
    }
    
    @isTest static void testSelectMergeNoSelected()
    {
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.JVCO_AccountMerge')); 
        System.currentPageReference().getParameters().put('find', 'true');
        System.currentPageReference().getParameters().put('object', 'Account');
        System.currentPageReference().getParameters().put('hideFilter', '1');
        System.currentPageReference().getParameters().put('limit', '3');
        System.currentPageReference().getParameters().put('field1', 'RecordTypeId');
        System.currentPageReference().getParameters().put('op1', 'equals');
        System.currentPageReference().getParameters().put('value1', 'Licence Account');
        JVCO_MergeController mergeController = new JVCO_MergeController();
        
        System.assert(mergeController.getResults().size() > 0);
        
        mergeController.selectMerge();
        mergeController.getMergeRecord1();
        mergeController.getMergeRecord2();
        mergeController.getMergeRecord3();
        
        Test.stopTest();
    }
    
    @isTest static void testSelectMergeSelectedInfrigement()
    {
        List<Account> licAccounts = [SELECT Id, JVCO_In_Infringement__c FROM Account WHERE RecordType.Name = 'Licence Account'];
        System.assert(licAccounts.size() > 0);
        for(Account acc : licAccounts){
            acc.JVCO_In_Infringement__c = true;
        }
        Database.update(licAccounts);
        
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.JVCO_AccountMerge')); 
        System.currentPageReference().getParameters().put('find', 'true');
        System.currentPageReference().getParameters().put('object', 'Account');
        System.currentPageReference().getParameters().put('hideFilter', '1');
        System.currentPageReference().getParameters().put('limit', '3');
        System.currentPageReference().getParameters().put('field1', 'RecordTypeId');
        System.currentPageReference().getParameters().put('op1', 'equals');
        System.currentPageReference().getParameters().put('value1', 'Licence Account');
        JVCO_MergeController mergeController = new JVCO_MergeController();
        
        System.assert(mergeController.getResults().size() > 0);
        for(Integer index = 0; index < mergeController.getResults().size(); index++){
            mergeController.getResults()[index].setSelected(true);
        }
        
        mergeController.selectMerge();
        
        
        Test.stopTest();
    }
    
    @isTest static void testSelectMerge()
    {
        
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.JVCO_AccountMerge')); 
        System.currentPageReference().getParameters().put('find', 'true');
        System.currentPageReference().getParameters().put('object', 'Account');
        System.currentPageReference().getParameters().put('hideFilter', '1');
        System.currentPageReference().getParameters().put('limit', '3');
        System.currentPageReference().getParameters().put('field1', 'RecordTypeId');
        System.currentPageReference().getParameters().put('op1', 'equals');
        System.currentPageReference().getParameters().put('value1', 'Licence Account');
        JVCO_MergeController mergeController = new JVCO_MergeController();
        
        System.assert(mergeController.getResults().size() > 0);
        for(Integer index = 0; index < mergeController.getResults().size(); index++){
            mergeController.getResults()[index].setSelected(true);
        }
        
        mergeController.selectMerge();
        mergeController.getFieldLabels();
        mergeController.selectedField = 'Id';
        mergeController.doSelectField();
        mergeController.getChildRelationships();
        mergeController.setChildRelationships(new List<String>{'JVCO_Affiliation__r'});
        mergeController.getChildRelationshipSelection();
        mergeController.clearChildSelection();
        
        
        Test.stopTest();
    }
    
    @isTest static void testDoMerge()
    {
        
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.JVCO_AccountMerge')); 
        System.currentPageReference().getParameters().put('find', 'true');
        System.currentPageReference().getParameters().put('object', 'Account');
        System.currentPageReference().getParameters().put('hideFilter', '1');
        System.currentPageReference().getParameters().put('limit', '3');
        System.currentPageReference().getParameters().put('field1', 'RecordTypeId');
        System.currentPageReference().getParameters().put('op1', 'equals');
        System.currentPageReference().getParameters().put('value1', 'Licence Account');
        JVCO_MergeController mergeController = new JVCO_MergeController();
        
        System.assert(mergeController.getResults().size() > 0);
        for(Integer index = 0; index < mergeController.getResults().size(); index++){
            mergeController.getResults()[index].setSelected(true);
        }
        
        mergeController.selectMerge();
        mergeController.doMerge();
        
        
        Test.stopTest();
    }
    
    @isTest static void testDoMergeCustomerAccount()
    {
        
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.JVCO_AccountMerge')); 
        System.currentPageReference().getParameters().put('find', 'true');
        System.currentPageReference().getParameters().put('object', 'Account');
        System.currentPageReference().getParameters().put('hideFilter', '1');
        System.currentPageReference().getParameters().put('limit', '3');
        System.currentPageReference().getParameters().put('field1', 'RecordTypeId');
        System.currentPageReference().getParameters().put('op1', 'equals');
        System.currentPageReference().getParameters().put('value1', 'Customer Account');
        JVCO_MergeController mergeController = new JVCO_MergeController();
        
        System.assert(mergeController.getResults().size() > 0);
        for(Integer index = 0; index < mergeController.getResults().size(); index++){
            mergeController.getResults()[index].setSelected(true);
        }
        
        mergeController.selectMerge();
        mergeController.doMerge();
        
        
        Test.stopTest();
    }
    
    @isTest static void testDoMergeVenue()
    {
        
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.JVCO_AccountMerge')); 
        System.currentPageReference().getParameters().put('find', 'true');
        System.currentPageReference().getParameters().put('object', 'JVCO_Venue__c');
        System.currentPageReference().getParameters().put('hideFilter', '1');
        System.currentPageReference().getParameters().put('limit', '3');
        System.currentPageReference().getParameters().put('field1', 'Name');
        System.currentPageReference().getParameters().put('op1', 'contains');
        System.currentPageReference().getParameters().put('value1', 'venue');
        JVCO_MergeController mergeController = new JVCO_MergeController();
        mergeController.showMasterAfterMerge = false;
        System.assert(mergeController.getResults().size() > 0);
        for(Integer index = 0; index < mergeController.getResults().size(); index++){
            mergeController.getResults()[index].setSelected(true);
        }
        
        mergeController.selectMerge();
        mergeController.doMerge();
        
        
        Test.stopTest();
    }
}