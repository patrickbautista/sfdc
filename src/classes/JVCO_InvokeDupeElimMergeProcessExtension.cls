public class JVCO_InvokeDupeElimMergeProcessExtension {
 
  public class DupeElimRedirectException extends Exception {}

  private List<Account> accountsList { get; set; }

  public String recordTypeName { private get; public set; }

  public JVCO_InvokeDupeElimMergeProcessExtension(ApexPages.StandardSetController stdSetController){
      accountsList = (List<Account>)stdSetController.getSelected();
  }

  public PageReference redirectLicAcctToDupeElimPage() {

    Profile profile = [Select Name from Profile Where Id = :UserInfo.getProfileId()];

    if(!'Licensing User'.equals(profile.Name) && !'System Administrator'.equals(profile.Name) && !'Live Music User'.equals(profile.Name) 
        && !'Key Accounts Manager'.equals(profile.Name) && !'Recovery & Collection'.equals(profile.Name) && !'Standard Accounts Agent'.equals(profile.Name) ){
        return new PageReference('/apex/InsufficientPrivileges');
    }
        
    List<String> accountNamesList = new List<String>();
    List<Account> acctList = queryAccounts(accountsList); 
    
    if (!accountsList.isEmpty()) {
      for (Account a : acctList) {
        if (a.RecordType.Name != 'Licence Account') {
          throw new DupeElimRedirectException('Invalid search: A non licence account was included. Kindly refine then try again.');
          //return null; GREEN-32978 - Commented due to unreachable statement error
        } else {
          accountNamesList.add(a.AccountNumber);
        }

        if(a.JVCO_Credit_Status__c == 'Passed to DCA - 1st Stage' || a.JVCO_Credit_Status__c == 'Passed to DCA – 2nd Stage'){
          System.debug('Has DCA');
          //throw new DupeElimRedirectException('There is a Licence Account that is in DCA Stage 1 or DCA Stage 2. Please use an Account that is not in DCA.');
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'There is a Licence Account that is in DCA Stage 1 or DCA Stage 2. Please use an Account that is not in DCA.'));
          return null;
        }
      }
      
    //start - mel.andrei.b.santos 23-10-2018 - GREEN-33536 - updated code to consider DD Payee Contact
    List<JVCO_DD_Mandate__c> ddMandateCheck = [Select Id,JVCO_Licence_Account__c, JVCO_Licence_Account__r.JVCO_DD_Payee_Contact__c from JVCO_DD_Mandate__c where JVCO_Licence_Account__c  in :acctList and JVCO_Licence_Account__r.JVCO_DD_Payee_Contact__c = null AND JVCO_Deactivated__c = false] ;

    if(!ddMandateCheck.isEmpty())
    {
      //throw new DupeElimRedirectException('There is a Licence Account that has an active DD Mandate with no valid DD Payee Contact. Please add a valid DD Payee Contact or deactivate the DD Mandate.');
       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'There is a Licence Account that has an active DD Mandate with no valid DD Payee Contact. Please add a valid DD Payee Contact or deactivate the DD Mandate.'));
      return null;
    }

    //end - mel.andrei.b.santos 

    }

    
    //Add validation rule when no licence accounts were selected - GREEN-31191 - joseph.g.barrameda
    if (acctList.size() == 0 ){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Please select Licence Accounts to merge.'));
                        return null;
    }
    
    if (acctList.size() > 3 ){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'You may only select up to three records to merge.'));
                        return null;
    }                   
    return new PageReference(getDestinationUrl('Licence+Account', accountNamesList));
  }

  private List<Account> queryAccounts(List<Account> acctsList) {
      List<Account> acctList = [SELECT Id, Name, RecordType.Name, JVCO_In_Enforcement__c, JVCO_In_Infringement__c, AccountNumber, JVCO_Credit_Status__c FROM Account WHERE Id IN :acctsList];  //
      return acctList;
  }

  private String getDestinationUrl(String rtype, List<String> accountNamesList) {

    String names = '';
    String destination = '/apex/JVCO_AccountMerge?find=true&object={0}&field1={1}&op1={2}&value1={3}&field3={4}&field4={5}&limit={6}';
    
	if (!accountNamesList.isEmpty()) {
      names = String.join(accountNamesList, ', ');
      destination = destination + '&op3={7}&value3={8}&hideFilter=1';
    }

    String[] arguments = new String[] { 'Account', 'RecordTypeId', 'equals', rtype, 'AccountNumber', 'JVCO_Registered_Address_Postcode__c', '3', 'contains', names };
    return String.format(destination, arguments);
    
  }
}