global with sharing class SMP_Refund_Controller{

    public String paymentURL {get; set;}
    public Income_Card_Payment__c cp;

    public SMP_Refund_Controller(ApexPages.StandardController controller)
    {       
        this.cp= (Income_Card_Payment__c)Controller.getRecord();

        cp = [SELECT Id, Name, Card_Number_Ending__c, Payment_Description__c, Total_Amount_Taken__c, Sagepay_Transaction_ID__c, Sagepay_Security_Key__c, Authorisation_Code__c,
              Authorisation_Date__c FROM Income_Card_Payment__c WHERE Id =:cp.Id];

        System.debug('#######card payment record '+ cp);
    
        CardPaymentHelper.Refund refund = new CardPaymentHelper.Refund();
        refund.cardPaymentRecordId = cp.Id;
        refund.cardPaymentRecordName = cp.Name;
        refund.cardNumber = cp.Card_Number_Ending__c;
        refund.paymentDescription = cp.Payment_Description__c;
        refund.originalAmount = String.valueOf(cp.Total_Amount_Taken__c);
        refund.relatedVPSTxId = cp.Sagepay_Transaction_ID__c;
        refund.relatedVendorTXCode = cp.Name;
        refund.relatedSecurityKey = cp.Sagepay_Security_Key__c;
        refund.relatedTxAuthNo = cp.Authorisation_Code__c;
        refund.authorisationDate = String.valueOf(cp.Authorisation_Date__c);
        refund.cardNumber = cp.Card_Number_Ending__c;
        
        PageReference url = CardPaymentHelper.generateECommerceRefundPaymentURL(refund);
        System.debug('#######refund URL '+ url.getURL());

        paymentURL =  url.getURL();

    }
    public static string getConnectionURL(){
        return String.valueof(url.getSalesforceBaseUrl().toExternalForm()).replace('-api.salesforce.com','') + '/services/Soap/u/37.0/' + userinfo.getOrganizationId();
    }
    public static String fetchUserSessionId() {
        if (Test.isRunningTest()) {
            return '';
        }
        
        String sessionId = '';
        // Refer to the Page
        PageReference reportPage = Page.currentUserInfoCtrl;
        // Get the content of the VF page
        String vfContent = reportPage.getContent().toString();
        System.debug('vfContent '+vfContent);
        // Find the position of Start_Of_Session_Id and End_Of_Session_Id
        Integer startP = vfContent.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
        endP = vfContent.indexOf('End_Of_Session_Id');
        // Get the Session Id
        sessionId = vfContent.substring(startP, endP);
        System.debug('sessionId '+sessionId);
        // Return Session Id
        return sessionId;
   }
}