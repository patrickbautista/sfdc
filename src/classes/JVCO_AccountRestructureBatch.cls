/* -------------------------------------------------------------------------------------
    Name: JVCO_AccountRestructureBatch
    Description: 

    Date            Version     Author                Summary of Changes 
    -----------     -------     -----------------     ----------------------------------
    10-May-2017     0.1         jules.osberg.a.pablo  Intial creation
------------------------------------------------------------------------------------- */
global class JVCO_AccountRestructureBatch implements Database.Batchable<sObject>, Database.Stateful {

    private final Account customerAccount;
    private final JVCO_AccountRestructureHelper accountRestructureHelper;
    private static List<String> exceptions;
    public static Map<String, String> invalidAccountIdMap = new Map<String, String>();
    global Map<String, String> invalidAccountIdMap2 = new Map<String, String>();
    private static String errorMessage = '';
    global String errorMessage2 = '';

    global JVCO_AccountRestructureBatch(Account a) {
        customerAccount = a;
        accountRestructureHelper = new JVCO_AccountRestructureHelper();
    }

    global Database.QueryLocator start (Database.BatchableContext BC) {
        System.debug('JVCO_AccountRestructureBatch START');
        
        return Database.getQueryLocator(accountRestructureHelper.getQueryString(customerAccount.Id));
    }

    global void execute (Database.BatchableContext BC, List<sObject> scope) {
        
        if (scope.size() > 0) {
            List<JVCO_Affiliation__c> validAffiliations = accountRestructureHelper.runValidationsOnAffiliations((List<JVCO_Affiliation__c>)scope, customerAccount.Id);

            errorMessage += accountRestructureHelper.processValidAffiliations(validAffiliations, customerAccount.Id);
            System.debug('## errorMessage BA:' + errorMessage);
            System.debug('## invalidAccountIdMap BA:' + invalidAccountIdMap);
            invalidAccountIdMap2.putAll(invalidAccountIdMap);

            errorMessage2 += errorMessage;
            System.debug('## errorMessage2 BA:' + errorMessage2);
        }
    }

    global void finish (Database.BatchableContext BC) {
        AsyncApexJob aJob = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id = :BC.getJobId()];

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {aJob.CreatedBy.Email});
        mail.setReplyTo('no-reply@salesforce.com');
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject(Label.AccountRestructureEmailMessagePart1 + ' ' + customerAccount.Name + ' ' + Label.AccountRestructureEmailMessagePart2);

        String emailMsg = Label.AccountRestructureEmailMessagePart1 + ' ' + customerAccount.Name + ' ' + Label.AccountRestructureEmailMessagePart2;
        
        emailMsg += '<br><br>';
        if(errorMessage2 != '' && errorMessage2 != null) {     
            emailMsg += Label.AccountRestructureEmailMessagePart3 + '<ul>' + errorMessage2 + '</ul>';
        }     

        System.debug('## invalidAccountIdMap2:' + invalidAccountIdMap2);
        if(!invalidAccountIdMap2.isEmpty())  {
            emailMsg += 'Invalid Id: <ul> ';
            for(String invalidId: invalidAccountIdMap2.keySet()){
                emailMsg += '<li>' + invalidAccountIdMap2.get(invalidId) + ' for Affiliation ' + invalidId + ' is not a valid Licence Account Id within this Customer Account or is a Live Account Id.</li>';
            }
            emailMsg += '</ul>';
        }

        if(aJob.NumberOfErrors > 0) {
            emailMsg += '<br><br>The batch Apex job processed ' + aJob.TotalJobItems + ' batches with '+ aJob.NumberOfErrors + ' failures. ExtendedStatus: ' + aJob.ExtendedStatus;
        }

        emailMsg += '</body></html>';

        mail.setHTMLBody(emailMsg);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}