/* ----------------------------------------------------------------------------------------------
    Name: JVCO_UpdateSalesInvoiceQueueable.cls 
    Description: Queueable class for sales invoice update by triggerring JVCO_codaCashEntryLineItemHandler
    Test class: JVCO_UpdateSalesInvoiceQueueableTest

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    18-Nov-2020  0.1         patrick.t.bautista    Intial creation - GREEN-36082
----------------------------------------------------------------------------------------------- */
public class JVCO_UpdateSalesInvoiceQueueable implements Queueable{
    
    //Custom settings
    private static final JVCO_General_Settings__c GeneralSettings = JVCO_General_Settings__c.getInstance();
    private static final Decimal salesInvoiceLimit = GeneralSettings != null ? GeneralSettings.JVCO_Sales_Invoice_Queue_Per_Jobs__c : 85;
    private Map<Id, Order> updatedOrderMap = new Map<Id, Order>();
    private Map<Id, OrderItem> updatedOrderItemMap = new Map<Id, OrderItem>();
    private Map<Id, c2g__codaTransactionLineItem__c> tliMap;
    private Map<Id, Id> licAndCustAccIdMap;
    //enqueue all except posting of invoice
    public JVCO_UpdateSalesInvoiceQueueable(Map<Id, c2g__codaTransactionLineItem__c> tliMap){
        this.tliMap = tliMap;
        this.licAndCustAccIdMap = new Map<Id, Id>();
    }
    //Enqueue for posting of invoice
    public JVCO_UpdateSalesInvoiceQueueable(Map<Id, Id> licAndCustAccIdMap){
        this.licAndCustAccIdMap = licAndCustAccIdMap;
        this.tliMap = new Map<Id, c2g__codaTransactionLineItem__c>();
    }
    //Enqueue for > 10k invoicelines
    public JVCO_UpdateSalesInvoiceQueueable(Map<Id, Id> licAndCustAccIdMap, Map<Id, Order> updatedOrderMap, Map<Id, OrderItem> updatedOrderItemMap){
        this.licAndCustAccIdMap = licAndCustAccIdMap;
        this.tliMap = new Map<Id, c2g__codaTransactionLineItem__c>();
        this.updatedOrderMap = updatedOrderMap;
        this.updatedOrderItemMap = updatedOrderItemMap;
    }
    
    public void execute(QueueableContext context)
    {
        //Account to update this run
        Map<Id, Id> accountToUpdateMap = new Map<Id, Id>();
        //Tli to update next run
        Map<Id, c2g__codaTransactionLineItem__c> tliNextToQueueMap = new Map<Id, c2g__codaTransactionLineItem__c>();
        //Queue account to next run
        Map<Id, Id> accountQueueMap = new Map<Id, Id>();
        
        Integer queueCounter = 0;
        if(!tliMap.isEmpty()){
            for(c2g__codaTransactionLineItem__c tli : tliMap.values())
            {
                if(queueCounter != salesInvoiceLimit){
                    queueCounter ++;
                    if(!accountToUpdateMap.containsKey(tli.c2g__Account__c) && tli.c2g__Account__r.JVCO_Customer_Account__c != null && tli.c2g__Account__c != null){
                        accountToUpdateMap.put(tli.c2g__Account__c, tli.c2g__Account__r.JVCO_Customer_Account__c);
                    }
                }
                else{
                    tliNextToQueueMap.put(tli.Id, tli);
                }
            }
        }else{
        system.debug('licAndCustAccIdMap'+licAndCustAccIdMap);
            for(Id accId : licAndCustAccIdMap.keySet()){
                if(queueCounter != salesInvoiceLimit){
                    queueCounter ++;
                    accountToUpdateMap.put(accId, licAndCustAccIdMap.get(accId));
                    system.debug('accountToUpdateMap'+accountToUpdateMap);
                }else{
                    accountQueueMap.put(accId, licAndCustAccIdMap.get(accId));
                    system.debug('accountQueueMap'+accountQueueMap);
                }
            }
        }
        try{
            if(!accountToUpdateMap.isEmpty()){
                List<Account> accountToUpdateList = new List<Account>();
                accountToUpdateList.addAll(JVCO_AccountUtil.updateCustomerAccountUponLicenceAccountUpdate(new Set<Id>(accountToUpdateMap.values())));
                accountToUpdateList.addAll(JVCO_AccountUtil.getTheTotalNumberOfInvoices(accountToUpdateMap.keyset()));
                JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
                JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
                update accountToUpdateList;
                
                if(!tliNextToQueueMap.isEmpty() && !Test.isRunningTest() && updatedOrderMap.isEmpty() && updatedOrderItemMap.isEmpty()){
                    Id queueId = System.enqueueJob(new JVCO_UpdateSalesInvoiceQueueable(tliNextToQueueMap));
                }
                if(!accountQueueMap.isEmpty() && !Test.isRunningTest() && updatedOrderMap.isEmpty() && updatedOrderItemMap.isEmpty()){
                    Id queueId = System.enqueueJob(new JVCO_UpdateSalesInvoiceQueueable(accountQueueMap));
                }
                if(!accountQueueMap.isEmpty() && !Test.isRunningTest() && !updatedOrderMap.isEmpty() && !updatedOrderItemMap.isEmpty()){
                    System.enqueueJob(new JVCO_UpdateSalesInvoiceQueueable(accountQueueMap, updatedOrderMap, updatedOrderItemMap));
                }
                if(accountQueueMap.isEmpty() && !Test.isRunningTest() && !updatedOrderMap.isEmpty() && !updatedOrderItemMap.isEmpty()){
                    System.enqueueJob(new JVCO_UpdateInvoiceQueueable(updatedOrderMap, updatedOrderItemMap));
                }
            }
        }
        catch(exception ex){
            insert createErrorLog('UPT-002', ex.getMessage(), 'JVCO_UpdateSalesInvoiceQueueable');
        }
    }
    
    @TestVisible
    private ffps_custRem__Custom_Log__c createErrorLog(String errCode, String errMessage, String batchName)
    {
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
        errLog.ffps_custRem__Detail__c = errMessage;
        errLog.ffps_custRem__Message__c = batchName;
        errLog.ffps_custRem__Grouping__c = errCode;
        
        return errLog;
    }
}