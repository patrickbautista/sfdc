@isTest
public class JVCO_EndDateAffiliationsTest {
    
    @testSetup
    public static void setupData(){
        
        JVCO_Array_Size__c asCS = new JVCO_Array_Size__c();
        asCS.JVCO_MaxAffiliationsForSyncProcess__c = 0;
        asCs.JVCO_AffiliationBatchScopeSize__c = 100;
        insert asCS;
        
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc.JVCO_Review_Type__c = 'Manual';
        licAcc.JVCO_Active__c = true;
        system.debug('@@@*** before insert acc debug: ' + licAcc);
        insert licAcc;
        system.debug('@@@*** after insert acc debug: ' + licAcc);
        

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
		
		JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
		insert ven;
		
		JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(licAcc.Id, ven.Id);
		aff.JVCO_Closure_Reason__c = null;
        aff.JVCO_End_Date__c = null;
		insert aff;
		
    }
    
    public static testMethod void testEndDateAffiliationBatch(){
        Account acc = [select Id, JVCO_Closure_Reason__c, JVCO_Closure_Date__c, JVCO_Review_Type__c, JVCO_Active__c from Account where RecordType.Name = 'Licence Account'];
        
        acc.JVCO_Closure_Date__c = date.today().addDays(20);
        acc.JVCO_Closure_Reason__c = 'Dissolved';
        system.debug('@@@*** test acc debug: ' + acc);
        Test.startTest();
        update acc;
        Test.stopTest();
        
        JVCO_Affiliation__c aff = [select Id, JVCO_End_Date__c, JVCO_Closure_Reason__c from JVCO_Affiliation__c where JVCO_Account__c = :acc.Id];
        
        System.AssertEquals(acc.JVCO_Closure_Date__c, aff.JVCO_End_Date__c);
        System.AssertEquals(acc.JVCO_Closure_Reason__c, aff.JVCO_Closure_Reason__c);
    }

}