/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_OpportunityTriggerHandlerTest.cls 
   Description:     Test class for  JVCO_OpportunityTriggerHandler.cls 

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
  24-May-2017       0.1         Rhys Dela Cruz                    Initial version of the code     
  16-Oct-2017       0.2         Jasper Figueroa                   Addressed codaTaxCode error
  01-Feb-2018       0.3         Rolando Valencia                  Created testmethod for setPrimaryQuote
  11-Apr-2018       0.4         Mel Andrei Santos                 GREEN-31233 addressed soql issue
  ---------------------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_OpportunityTriggerHandlerTest{

    @testSetup
    private static void testSetup(){
        Test.startTest();

        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        PriceBook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;

        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.Type = 'Key Account';
        insert acc2;

        Account licAcc3 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        licAcc3.AccountNumber = '832022';
        LicAcc3.JVCO_Live__c = true;
        insert licAcc3; 

        //Account acc3 = JVCO_TestClassObjectBuilder.createCustomerAccount();
        //acc3.Name = 'TestCust2';
        //insert acc3;

        Account acc4 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc4.Type = 'Customer';
        insert acc4;

        acc4.JVCO_Customer_Account__c = acc.Id;
        update acc4;

      
        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_Closure_Reason__c = '';
        aff.JVCO_End_Date__c = null;
        insert aff;

        Test.stopTest();

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        insert opp;
        

        List<sObject> artistList = Test.loadData(JVCO_Artist__c.SObjectType, 'JVCO_TestData_Artist');

        JVCO_Event__c evt = new JVCO_Event__c();
        evt.Name = 'evtTest';
        evt.JVCO_Artist__c = artistList.get(0).Id;
        evt.JVCO_Venue__c = ven.Id;
        evt.JVCO_Event_Type__C = 'Concert – Qualifies for Old Rate';
        evt.JVCO_Event_Start_Date__c = date.today();
        evt.JVCO_Event_End_Date__c = date.today().addDays(30);
        evt.JVCO_Event_Classification_Status__c = null;
        evt.JVCO_Classification_Required_By_Date__c = null;
        evt.License_Account__c=licAcc3.id;
        evt.Headline_Type__c = 'No Headliner';
        evt.JVCO_Number_of_Set_Lists_Expected__c = 2;
        insert evt;

        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(acc2.Id, opp.Id);
        insert q;

        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c =true;
        //dt.JVCO_OpportunityTrigger__c = true;

        insert dt;

        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, prod.Id);
        ql.JVCO_Event__c = evt.id;
        ql.JVCO_Venue__c = ven.id;
        insert ql;

        Contract contr = JVCO_TestClassHelper.setContract(acc2.Id, opp.Id, q.Id);
        insert contr;

        Order o = JVCO_TestClassHelper.setOrder(acc2.Id, q.Id);
        o.status = 'Draft';
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

       /* Order o = new Order();
        o.Accountid = acc2.id;
        o.effectivedate = System.Today();
        o.SBQQ__Quote__c = q.id;
        o.blng__BillingAccount__c = acc2.id;
        o.SBQQ__PaymentTerm__c = 'Net 30';
        o.SBQQ__PriceCalcStatus__c = 'Completed';
        o.status = 'Draft';
        o.Pricebook2Id = Test.getStandardPriceBookId();
        o.OpportunityId = opp.id;
        insert o;
        Test.stopTest();
        */
    }

    
    
    private static testMethod void testUpdateContractRenewalGenerated()
    {
        Opportunity oppRec = [Select Id, SBQQ__Contracted__c, StageName, Probability, SBQQ__RenewedContract__c From Opportunity Limit 1 ];
        oppRec.SBQQ__Contracted__c = false;
        oppRec.StageName = 'Quoting';
        oppRec.Probability = 50;
        oppRec.JVCO_Invoiced__c = true;
        update oppRec;

        oppRec.JVCO_Invoiced__c = false;
        update oppRec;

        oppRec.JVCO_Invoiced__c = true;
        oppRec.SBQQ__Renewal__c = true;
        update oppRec;

        Contract cRec = [Select Id, SBQQ__RenewalQuoted__c, JVCO_Renewal_Generated__c From Contract Limit 1];
        cRec.SBQQ__RenewalQuoted__c = true;
        cRec.SBQQ__RenewalOpportunity__c = oppRec.Id;
        Database.SaveResult saveR = database.update(cRec);

        Test.startTest();
        if(saveR.isSuccess())
        {
            oppRec.SBQQ__Contracted__c = true;
            oppRec.StageName = 'Closed Won';
            oppRec.Probability = 100;
            Database.SaveResult sR = database.update(oppRec);

            if(sR.isSuccess())
            {
                Contract finalCont = [Select Id, JVCO_Renewal_Generated__c From Contract where SBQQ__RenewalOpportunity__c =: oppRec.Id];
                system.assert(finalCont.JVCO_Renewal_Generated__c, 'Renewal Generated Error');
            }
            else
            {
                system.assert(false,'Contracting Quote Error');
            }

        }
        else
        {
            system.assert(false,saveR.getErrors());
        }
        Test.stopTest();
    }

    

    private static testMethod void testSetPrimaryQuote()
    {
        Test.startTest();
        Opportunity o = [SELECT Id, SBQQ__PrimaryQuote__c FROM Opportunity LIMIT 1];

        SBQQ__Quote__c q = [SELECT Id FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c != NULL AND SBQQ__Primary__c = TRUE LIMIT 1];

        
        o.SBQQ__PrimaryQuote__c = NULL;
        update o;
        Test.stopTest();
        
        Opportunity opp = [SELECT Id, SBQQ__PrimaryQuote__c FROM Opportunity LIMIT 1];

        System.assertEquals(q.id, opp.SBQQ__PrimaryQuote__c, 'ERROR:Primary Quote is not equal to the Quote ID');
    

    }
    

    public static testMethod void testInvoiced()
    {
        //SBQQ__Quote__c qtRec = [Select Id, SBQQ__Primary__c, Start_Date__c , End_Date__c, SBQQ__SubscriptionTerm__c, SBQQ__Opportunity2__c, Accept_TCs__c,SBQQ__Status__c From SBQQ__Quote__c Limit 1];
        //JVCO_Venue__c venRec = [Select Id from JVCO_Venue__c Limit 1];
        //SBQQ__QuoteLine__c qtlRec = [Select Id, SBQQ__Quote__c, JVCO_Event__c From SBQQ__QuoteLine__c where SBQQ__Quote__c =: qtRec.Id Limit 1 ];
        Test.startTest();

        Opportunity opp = [SELECT Id, SBQQ__PrimaryQuote__c, JVCO_Invoiced__c FROM Opportunity LIMIT 1];
        
        opp.JVCO_Invoiced__c = false;
        update opp;

        Test.stopTest();

        JVCO_Event__c evntRec = [Select Id, JVCO_Event_Quoted__c, JVCO_CancelledEvent__c,JVCO_Event_Invoiced__c From JVCO_Event__c Limit 1];
        system.assertEquals(false, evntRec.JVCO_Event_Invoiced__c);

        opp.JVCO_Invoiced__c = true;
        update opp;

        JVCO_Event__c evntRec1 = [Select Id, JVCO_Event_Quoted__c, JVCO_CancelledEvent__c,JVCO_Event_Invoiced__c From JVCO_Event__c Limit 1];
        system.assertEquals(true, evntRec1.JVCO_Event_Invoiced__c);
        

    }

    public static testMethod void oppValidatioMultiplePrder()
    {
        //Account licAcc = [SELECT id from Account Where RecordType.Name = 'Licence Account' AND JVCO_Live__c = false limit 1];
        //SBQQ__Quote__c quoteRec = [SELECT id from SBQQ__Quote__c limit 1];
        Opportunity oppRec = [SELECT id, SBQQ__Ordered__c from Opportunity limit 1];


        

        Test.startTest();
        try{
            oppRec.SBQQ__Ordered__c = true;
            update oppRec;
        }catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('There is already an order associated to') ? true : false;
            system.assertEquals(expectedExceptionThrown,true);
        }

        Test.stopTest();   
    }
    
}