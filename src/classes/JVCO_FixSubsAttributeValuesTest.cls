@isTest
private class JVCO_FixSubsAttributeValuesTest {

    @testSetup 
    static void setup() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        User u = new User(Alias = 'utest', Email='unit.test@unit.test.com',
                          EmailEncodingKey='UTF-8', LastName='Unit Test', 
                          LanguageLocaleKey='en_US', LocaleSIdKey='en_GB', ProfileId = p.Id,
                          TimezoneSIdKey='Europe/London', Username='test.test@unit.test.com',
                          Division='Legal', Department='Legal');

        insert u;

        system.runAs(u){
            Account acc =  JVCO_TestClassObjectBuilder.createCustomerAccount();
            insert acc;

            Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
            acc2.JVCO_Renewal_Scenario__c = '';
            acc2.JVCO_Live__c = true;
            insert acc2;

            List<sObject> lProd = Test.loadData(Product2.SObjectType, 'JVCO_Products');
            
            Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
            prod.proratable__c = false;
            insert prod;

            Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
            insert pb;

            PriceBookEntry pEntry = JVCO_TestClassObjectBuilder.createPriceBookEntry(prod.id);
            insert pEntry;

            Contract cont = JVCO_TestClassObjectBuilder.createContract(acc2.id, null);
            insert cont;

            JVCO_Venue__c venueRec = JVCO_TestClassObjectBuilder.createVenue();
            insert venueRec;

            //create affiliation
            JVCO_Affiliation__c affiliationRec = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, venueRec.id);
            affiliationRec.JVCO_Closure_Reason__c = '';
            affiliationRec.JVCO_End_Date__c = null;
            insert affiliationRec;

            //load artist test data
            List<sObject> artistList = Test.loadData(JVCO_Artist__c.SObjectType, 'JVCO_TestData_Artist');

            //create event configured for GP_PRS
            JVCO_Event__c eventRec = new JVCO_Event__c();
            eventRec.JVCO_Venue__c = venueRec.Id;                
            eventRec.JVCO_Artist__c = artistList.get(0).Id;
            eventRec.JVCO_Event_Start_Date__c = Date.Today();
            eventRec.JVCO_Event_End_Date__c = Date.Today().addDays(25);
            eventRec.JVCO_Invoice_Paid__c = false;
            eventRec.JVCO_Event_Classification_Status__c = 'Classified';
            //eventRec.JVCO_Promoter__c = accLicence.Id;
            eventRec.JVCO_Tariff_Code__c = 'V';
            eventRec.JVCO_Event_Type__c = 'Concert - Qualifies for Old Rate';
            eventRec.JVCO_Event_Quoted__c = false;
            eventRec.JVCO_CancelledEvent__c = false;
            eventRec.JVCO_Event_Invoiced__c = false;
            eventRec.JVCO_Percentage_Controlled__c = 75.0;
            eventRec.JVCO_Percentage_Controlled_Required__c = true;
            eventRec.JVCO_Room_Names__c ='Test';
            eventRec.License_Account__c = acc2.id;
            eventRec.Headline_Type__c = 'No Headliner';
            eventRec.JVCO_Number_of_Set_Lists_Expected__c = 2;
            insert eventRec;
            
            List<SBQQ__Subscription__c> lSubs = new List<SBQQ__Subscription__c>();

            for(integer i= 0; i < lProd.size(); i++){
                Product2 prodRec =  (Product2)lProd[i];
                lSubs.add(new SBQQ__Subscription__c(Accommodation__c = 2,AdmissionCapacity__c = 2,AdmissionChargePremises__c = TRUE, AdmissionMorethan20__c = TRUE, Admissions__c = 2, AggAnnualAttndnce__c = 12, SBQQ__Product__c = prodRec.id, SBQQ__ListPrice__c = 1,SBQQ__NetPrice__c = 1, SBQQ__ProrateMultiplier__c = 1, SBQQ__Quantity__c = 3, SBQQ__PartnerDiscount__c = 10, FurtherInfo__c = 'test test', AircraftType__c = 'test', 
                    AnnualIncome__c = 1000, ApplyReducedRate__c = TRUE, ApprovedCCDisc__c = TRUE, AreaName__c = 'Test', AreaOfExbtn__c = 10, AreaOrRoomDetails__c = 'Test', Attendance__c = 100, AudibleArea__c = 100, AudienceCapacity__c = 125, AvgAttendance__c = 150, AvgAttndncePerSession__c = 100, AvgWeeklyCongregation__c = 10,
                    BarArea__c = 10, BethCapacityExcEmployees__c = 10, BethCapacityIncEmployees__c = 10, BoxOfficeReceipts__c = 10000, CandidateNames__c = 'test', Capacity__c = 10, CCLI__c = TRUE, CD__c = TRUE, ChargeAmount__c = 1000, ClassMorethan30__c = TRUE, CustomerUKActive__c = TRUE,  CustSuppliedInfo__c = TRUE, DefinedAnnualIncome__c = 10000,
                    DemoOnly__c = TRUE, DurationHour__c = 2, DurationMins__c = 13, EducExemption__c = 12, EventNameOrType__c = 'Test', EventNameText__c = 'Test', EvtsWithFeaturedFilmVideo__c = 10, EvtsWithFeaturedMusic__c = 10, ExbtnNameOrType__c = 'Test', ExbtnOrTradeShowOrganiser__c = TRUE, ExhibitionName__c = 'Test', ExpPerformersGtrThan20__c = 10, ExpPerformersLessThan20__c = 10,
                   ExtrnlLinesPerSwitchboard__c = 10, FeatutedMusicEvtCapacity__c = 10, FilmVideoEventCapacity__c = 123, FleetNameOrOther__c = 'Test', FloorArea__c = 100, GrossReceipts__c = 10000, HrsPerOccasion__c = 2, HrsPerShow__c = 2, HrsPerWeek__c = 2, JKBoxSupplierRented__c = TRUE, JVCO_MinimumFeeApplied__c = TRUE, LicenceFee__c = 100, LicensingDuration__c = 10, LicensingDurationMin__c = 30, LicensingDurationSec__c = 13,
                   LiveGrossReceipts__c = 100, LiveMusicExpenditure__c = 1000, MaxAttendance__c = 1000, MinsPerPerf__c = 12, MultiRateApply__c = TRUE, MusicDuration__c = 12, MusicDurationMin__c = 30, MusicDurationSec__c = 34, MusicOnlyForGuests__c = TRUE, NameGeneric__c = 'Test', NameOfPerf__c = 'test', NameOrTypeOfPermExbtns__c = 'Test', NameOrTypeOfTempExbtns__c = 'Test', NoOfAerobicClasses__c = 100, NoOfAircraft__c = 10, 
                   NoOfAreas__c = 10, NoOfBedNights__c = 10, NoOfBedroomsApplicable__c = 11, NoOfBedroomsCentralRelay__c = 11, NoOfBedroomsRecMusic__c = 10, NoOfBeds__c = 10, NoOfBoxes__c = 10, NoOfCandidates__c = 12, NoOfCarriages__c = 10 , NoOfChairsOrTrtmntTables__c = 10, NoOfClasses__c = 12, NoOfClassesPerAnnum__c = 10, NoOfClubsOrInstrctrs__c = 10, NoOfConcourse__c = 10, NoOfDays__c = 10, NoOfDaysExbtnOpen__c = 10, NoOfDaysPerformed__c = 10,
                   NoOfDaysPerWeek__c = 10, NoOfDaysWithLiveMusic__c = 2, NoOfDJs__c = 3, NoOfDressComptOrEvts__c = 6, NoOfEvents__c = 13, NoOfExternalLines__c = 4, NoOfExtnSpkrsOrRelayPnts__c = 13, NoOfFullTimeEquivalent__c = 5, NoOfFunctions__c = 7, NoOfIndvStndsPlayMusic__c = 7, NoOfInstruments__c = 15, NoOfLessonsPerAnnum__c = 4, NoOfLstningPostsOrKiosks__c = 5, NoOfMembers__c = 7, NoOfMusicians__c = 4, NoOfMusicSystmsInPark__c = 5,
                   NoOfMusicSystmsSpecfRides__c = 8, NoOfOccasions__c = 5, NoOfOperators__c = 3, NoOfPassengers__c = 5, NoOfPassengersInflight__c = 5, NoOfPerf__c = 2, NoOfPerformances__c = 5, NoOfPermExbtnsOrDsply__c = 4, NoOfPitches__c = 3, NoOfPlasmaScreens__c = 4, NoOfPlasmaScreensBValue__c = 5, NoOfPlatforms__c = 5, NoOfPltfrmOrConcrseArea__c = 6, NoOfPremises__c = 5, NoOfPsgrCabBoth__c =3, NoOfPsgrCabCDMP3__c = 3, NoOfPsgrCabTVRadioDVD__c = 2,
                   NoOfPupilsSchoolRoll__c = '1', NoOfPuppetrsOrMagicns__c = 2, NoOfQuizPerAnnum__c = 4, NoOfRides__c = 2, NoOfRinks__c = 4, NoOfScreenings__c = 4, NoOfScreens__c =3, NoOfSeats__c = 2, NoOfSeparateAreas__c = 4, NoOfSessionPerWeek__c = 12, NoOfSessions__c = 2, NoOfSetsEquipments__c = 3, NoOfSFEEvents__c = 5, NoOfShows__c = 3, NoOfSpeakers__c = 3, NoOfStaff__c = 3, NoOfStands__c = 3, NoOfStudiosUsed__c = 3, NoOfStylChairTrtmtTable__c = 12,
                   NoOfStylistTrtmtChairs__c = 2, NoOfSwitchboards__c = 4, NoOfTempExbtnsWithBGM__c = 4, NoOfTrainers__c = 12, NoOfTvMonitors__c = 2, NoOfTvMonitorsBValue__c = 4, NoOfTvVideoWallProjScreen__c = 5, NoOfUnits__c = 5, NoOfVehicles__c = 8, NoOfVessels__c = 4, NoOfWeeks__c = 6, NoOfWksPerAnnum__c = 2, NoStaffMusicAvailable__c = 4, NumberOfBedrooms__c = 3, NumberOfEmployees__c = 4, NumberofTempExhibitsBGM__c = 3, OnlyRehearsals__c = TRUE,
                   Other__c = TRUE, PeopleAdmitted__c = 2, PercOfContWorks__c = 4, PerfPoint__c = 5, PermitApplicable__c = TRUE, PersonsAdmitted__c = 3, PriorApplicationMade__c = TRUE, ProdCostRecouped__c = TRUE, ProdRunMorethan26week__c = TRUE, PssgrCapacity__c = 3, PupilsOnSchoolRoll__c = 4, QuantityPrs__c = 2, Radio__c = TRUE, RadioTvViaCentralRelaySys__c = TRUE, RecMusicViaPlayerDevices__c = TRUE, ReportReqFulfilled__c = TRUE, RideType__c = 'Test',
                   Room__c = 'Test', RoomCapacity__c = 2, RoomName__c = 'Test', RoomsWithBoth__c = 2, RoomsWithRecCdTape__c = 3, RoomsWithTvRadVid__c = 3, SchoolYouthBand__c = TRUE, SeparateCounterTakeOut__c = TRUE, ShiftHours__c = 2, ShiftMinutes__c = 13, ShiftName__c = 'Test', JVCO_Event__c = eventRec.id, CustomMinMaxCalcType__c = 'Min', TV__c = TRUE, TotalAnnualExpenditure__c = 1000, SBQQ__Contract__c = cont.id  ));


            }
            insert lSubs;

            SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(acc2.id, prod.id);
            s.SBQQ__ListPrice__c = 1;
            s.SBQQ__NetPrice__c = 0.33;
            s.SBQQ__ProrateMultiplier__c = 0.33;
            s.ChargeYear__c = 'Current Year';
            s.NoOfQuizPerAnnum__c = 7;

            insert s;

        }

        JVCO_FixProRateValuesCustomSetting__c setting = new JVCO_FixProRateValuesCustomSetting__c(Name = 'FixProrate', Min__c = 'Min', MAx__c = 'Max',MRPPPL__c = 'MRPPPL', MRPVPL__c = 'MRPVPL', DataMigId__c= u.Id);
        insert setting;
        


    }

    @isTest 
    static void testUpdateSubs() {        
        Test.startTest();
        JVCO_FixSubsAttributeValues fixAttValue = new JVCO_FixSubsAttributeValues ();
        Id batchId = Database.executeBatch(fixAttValue,900);
        Test.stopTest();

        SBQQ__Subscription__c checkSubs = [SELECT id, AttributeValues__c, JVCO_NetTotal__c,SBQQ__NetPrice__c,SBQQ__ListPrice__c,SBQQ__ProrateMultiplier__c,SBQQ__Quantity__c,JVCO_NOT_Renewable__c,ChargeYear__c ,SBQQ__RenewalQuantity__c FROM SBQQ__Subscription__c limit 1];


        System.assert(checkSubs.AttributeValues__c!=null || checkSubs.AttributeValues__c != '');
        //System.assertEquals(checkSubs.SBQQ__ProrateMultiplier__c,1);
    }

    @isTest
    static void testWithSubsId() {
        list <SBQQ__Subscription__c> lSubs = [SELECT id, AttributeValues__c, JVCO_NetTotal__c,SBQQ__NetPrice__c,SBQQ__ListPrice__c,SBQQ__ProrateMultiplier__c,SBQQ__Quantity__c,JVCO_NOT_Renewable__c,ChargeYear__c ,SBQQ__RenewalQuantity__c FROM SBQQ__Subscription__c];
        //Set <SBQQ__Subscription__c> sSubs = new Set <SBQQ__Subscription__c>();
        Set<id> s1 = new Set<id>();
        for(SBQQ__Subscription__c subsRec: lSubs){
            s1.add(subsRec.id);
        }
        
        Test.startTest();

        JVCO_FixSubsAttributeValues bo = new JVCO_FixSubsAttributeValues(s1); 
        database.executeBatch(bo,900);
        Test.stopTest();

        SBQQ__Subscription__c checkSubs = [SELECT id, AttributeValues__c, JVCO_NetTotal__c,SBQQ__NetPrice__c,SBQQ__ListPrice__c,SBQQ__ProrateMultiplier__c,SBQQ__Quantity__c,JVCO_NOT_Renewable__c,ChargeYear__c ,SBQQ__RenewalQuantity__c FROM SBQQ__Subscription__c limit 1];


        System.assert(checkSubs.AttributeValues__c!=null || checkSubs.AttributeValues__c != '');

    }

}