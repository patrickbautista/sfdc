/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_SubscriptionTriggerHandlerTest.cls 
   Description:     Test class for  JVCO_SubscriptionTriggerHandler.cls 

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
  11-May-2017       0.1        Accenture-mel.andrei.b.santos         Initial version of the code     
  24-Oct-2017       0.2        Accenture-jason.e.mactal             Modified Test Class to avoid errors
  ---------------------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_SubscriptionTriggerHandlerTest {

@testSetup
  private static void testSetup() {

    JVCO_TestClassObjectBuilder.createBillingConfig(); //jason

    Group testGroup = new Group(Name='test group', Type='Queue');
    insert testGroup;

    System.runAs(new User(Id=UserInfo.getUserId()))
    {
        List<queuesobject >  listQueue = new List<queuesobject >();
        queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
        listQueue.add(q1);
        queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
        listQueue.add(q2);
        queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
        listQueue.add(q3);
        queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
        listQueue.add(q4);
        queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
        listQueue.add(q5);
        queuesobject q6 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankAccount__c'); 
        listQueue.add(q6);
        queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
        listQueue.add(q7);
        queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatement__c'); 
        listQueue.add(q8);
        queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransaction__c'); 
        listQueue.add(q9);
        queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCreditNote__c'); 
        listQueue.add(q10);
        //queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntry__c'); 
        //listQueue.add(q9);
        //queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntryLineItem__c'); 
        //listQueue.add(q10);
        //queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatementLineItem__c'); 
        //listQueue.add(q10);
        //queuesobject q12 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatementLineItem__c'); 
        //listQueue.add(q12);           
        insert listQueue;

        GroupMember GroupMemberObj = new GroupMember();
        GroupMemberObj.GroupId = testGroup.id;
        GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
        insert GroupMemberObj;
    }

    c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
    testCompany.c2g__CashMatchingCurrencyMode__c = 'Account';
    testCompany.c2g__Country__c = 'GB';
    testCompany.c2g__ECCountryCode__c = 'GB';
    testCompany.c2g__UnitOfWork__c = 1.0;
    testCompany.Name = 'Test Company';
    testCompany.ownerid = testGroup.Id;
    insert testCompany;
    
    Test.startTest();
    c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc10040 = new c2g__codaGeneralLedgerAccount__c();
    testGeneralLedgerAcc10040.c2g__AdjustOperatingActivities__c = false;
    testGeneralLedgerAcc10040.c2g__AllowRevaluation__c = false;
    testGeneralLedgerAcc10040.c2g__CashFlowCategory__c = 'Operating Activities';
    testGeneralLedgerAcc10040.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
    testGeneralLedgerAcc10040.c2g__GLAGroup__c = 'Accounts Receivable';
    testGeneralLedgerAcc10040.c2g__ReportingCode__c = '10040';
    testGeneralLedgerAcc10040.c2g__TrialBalance1__c = 'Assets';
    testGeneralLedgerAcc10040.c2g__TrialBalance2__c = 'Current Assets';
    testGeneralLedgerAcc10040.c2g__TrialBalance3__c = 'Accounts Receivables';
    testGeneralLedgerAcc10040.c2g__Type__c = 'Balance Sheet';
    testGeneralLedgerAcc10040.c2g__UnitOfWork__c = 1.0;
    testGeneralLedgerAcc10040.Dimension_1_Required__c = false;
    testGeneralLedgerAcc10040.Dimension_2_Required__c = false;
    testGeneralLedgerAcc10040.Dimension_3_Required__c = false;
    testGeneralLedgerAcc10040.Dimension_4_Required__c = false;
    testGeneralLedgerAcc10040.Name = '10040 - Accounts receivables control';
    testGeneralLedgerAcc10040.ownerid = testGroup.Id;
    insert testGeneralLedgerAcc10040;

    //Create General Ledger Account for Income Control
    c2g__codaGeneralLedgerAccount__c testIcGla = new c2g__codaGeneralLedgerAccount__c();
    testIcGla.c2g__ReportingCode__c = '30030';
    testIcGla.c2g__TrialBalance1__c = 'Liabilities';
    testIcGla.c2g__TrialBalance2__c = 'Income Control';
    testIcGla.c2g__TrialBalance3__c = 'Short-term income control account';
    testIcGla.c2g__Type__c = 'Balance Sheet';
    testIcGla.Name = '30030 - Income Control';
    testIcGla.ownerid = testGroup.Id;
    insert testIcGla;

    c2g__codaGeneralLedgerAccount__c testGeneralLedger = JVCO_TestClassObjectBuilder.createGeneralLedger();
    insert testGeneralLedger;

    c2g__codaTaxCode__c testTaxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedger.Id);
    insert testTaxCode;

    c2g__codaTaxRate__c testTaxRate = new c2g__codaTaxRate__c();
    testTaxRate.c2g__Rate__c = 20.0;
    testTaxRate.c2g__UnitOfWork__c = 1.0;
    testTaxRate.c2g__TaxCode__c = testTaxCode.id;
    insert testTaxRate;

    c2g__codaCashMatchingSettings__c settings = new c2g__codaCashMatchingSettings__c();
    settings.JVCO_Write_off_Limit__c = 1.00;
    insert settings;

    c2g__codaDimension2__c testDimension2 = new c2g__codaDimension2__c();
    testDimension2.name = 'Paid at PPL';
    testDimension2.c2g__ReportingCode__c = 'Paid at PPL';
    testDimension2.c2g__UnitOfWork__c = 1.0;
    insert testDimension2;

    c2g__codaDimension2__c testDimension3 = new c2g__codaDimension2__c();
    testDimension3.name = 'Non Key';
    testDimension3.c2g__ReportingCode__c = 'Non Key';
    testDimension3.c2g__UnitOfWork__c = 2.0;
    insert testDimension3;

    c2g__codaAccountingCurrency__c accCurrency = new c2g__codaAccountingCurrency__c();
    accCurrency.c2g__OwnerCompany__c = testCompany.id;
    accCurrency.c2g__DecimalPlaces__c = 2;
    accCurrency.Name = 'GBP';
    accCurrency.c2g__Dual__c = true ;
    accCurrency.ownerid = testGroup.Id;
    accCurrency.c2g__Home__c = true;
    accCurrency.c2g__UnitOfWork__c = 2.0;
    insert accCurrency;

    Product2 prod1 = new Product2();
    prod1.Name = 'PPL Product';
    prod1.SBQQ__ChargeType__c= 'One-Time';
    prod1.JVCO_Society__c = 'PPL';
    prod1.ProductCode = 'PPLPP085';
    prod1.IsActive = true;
    prod1.JVCO_NOT_Renewable__c = true;
    prod1.c2g__CODASalesRevenueAccount__c = testIcGla.Id;
    insert prod1;
    
    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
    
    Account a1 = new Account();
    a1.RecordTypeId = customerRT;
    a1.Name = 'Test Account 83202-C';
    a1.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc10040.id;
    a1.Type = 'Key Account';
    insert a1;

    //Create Contact
    Contact testContact = JVCO_TestClassObjectBuilder.createContact(a1.Id);
    testContact.Firstname = 'Test';
    testContact.LastName = 'TestContact';
    insert testContact;
    
    Account a2 = new Account();
    a2.RecordTypeId = licenceRT;
    a2.Name = 'Test Account 83202-License';

    a2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    a2.JVCO_Customer_Account__c = a1.id;
    a2.c2g__CODAOutputVATCode__c = testTaxCode.id;
    a2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc10040.id;
    //a2.JVCO_Customer_Account__c = a1.Id;
    a2.JVCO_Preferred_Contact_Method__c = 'Email';
    a2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
    a2.JVCO_Billing_Contact__c = testContact.id;
    a2.JVCO_Licence_Holder__c = testContact.id;
    a2.JVCO_Review_Contact__c = testContact.id;
    a2.JVCO_Live__c = true;
    a2.Type = 'Key Account';
    insert a2;

    JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();
    venueRecord.Name = 'test1';              
    Insert  venueRecord;   

    JVCO_Affiliation__c  affilRecord = JVCO_TestClassObjectBuilder.createAffiliation(a2.id, venueRecord.id); 
    Insert  affilRecord;
    
    Opportunity o = new Opportunity();
    o.Name = 'Test opportunity 83202';
    o.AccountId = a2.Id;
    o.CloseDate = System.today() - 5;
    o.StageName = 'Draft';
    insert o; 

    Pricebook2 testPB2 = new Pricebook2();
    testPB2.name = 'Standard Price Book';
    testPB2.IsActive = true;
    insert testPB2;        

    SBQQ__QuoteProcess__c qId = JVCO_TestClassObjectBuilder.createQuoteProcess();
    insert qId;

    SBQQ__Quote__c q = new SBQQ__Quote__c();
    q.SBQQ__Type__c = 'Renewal';
    q.SBQQ__Status__c = 'Draft';
    q.SBQQ__Primary__c = true;
    q.SBQQ__Account__c = a2.Id;
    q.SBQQ__Opportunity2__c = o.Id;
    q.SBQQ__StartDate__c = System.today() - 5;
    q.SBQQ__EndDate__c = System.today().addMonths(12);
    q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
    q.SBQQ__QuoteProcessId__c = qId.Id;
    insert q;

    //SBQQ__QuoteLineGroup__c testQLG = [select id, JVCO_Affiliated_Venue__c, JVCO_Venue__c from SBQQ__QuoteLineGroup__c limit 1];

    Contract contr = new Contract();
    contr.AccountId = a2.Id;
    contr.Status = 'Draft';
    contr.StartDate = Date.today();
    contr.ContractTerm = 12;
    contr.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
    contr.SBQQ__RenewalQuoted__c = FALSE;
    contr.SBQQ__Quote__c = q.id;
    contr.SBQQ__Opportunity__c = o.id;
    insert contr;
    
    Test.stopTest();
    
    SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
    ql.SBQQ__Quote__c = q.Id;
    ql.SBQQ__Product__c = prod1.Id;
    ql.SBQQ__Quantity__c = 1;
    ql.SBQQ__Number__c = 1;
    ql.SBQQ__PricingMethod__c = 'List';
    ql.SBQQ__ListPrice__c = 10;
    ql.SBQQ__CustomerPrice__c = 10;
    ql.SBQQ__NetPrice__c = 10;
    ql.SBQQ__SpecialPrice__c = 10;
    ql.SBQQ__RegularPrice__c = 10;
    ql.SBQQ__UnitCost__c = 10;
    ql.SBQQ__ProratedListPrice__c = 10;
    ql.SBQQ__SpecialPriceType__c = 'Custom';
    ql.SBQQ__ChargeType__c= 'One-Time';
    insert ql;

    PriceBookEntry testPB = new PriceBookEntry();
    testPB.Pricebook2Id = test.getStandardPriceBookId();
    testPB.product2id = prod1.id;
    testPB.IsActive = true;
    testPB.UnitPrice = 1000.0;
    insert testPB;
     //start added by edmundCua
    JVCO_Artist__c testArtist = new JVCO_Artist__c();
    testArtist.name = 'Test Artist';
    insert testArtist;

    Account a3 = new Account();
    a3.RecordTypeId = licenceRT;
    a3.Name = 'Test Account 83202-License Promoter';

    a3.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    a3.JVCO_Customer_Account__c = a1.id;
    a3.c2g__CODAOutputVATCode__c = testTaxCode.id;
    a3.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc10040.id;
    //a2.JVCO_Customer_Account__c = a1.Id;
    a3.JVCO_Preferred_Contact_Method__c = 'Email';
    a3.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
    a3.JVCO_Billing_Contact__c = testContact.id;
    a3.JVCO_Licence_Holder__c = testContact.id;
    a3.JVCO_Review_Contact__c = testContact.id;
    a3.JVCO_Live__c = true;
    a3.Type = 'Promoter';
    insert a3;
        
    JVCO_Event__c EventRecord = new JVCO_Event__c();
    EventRecord.JVCO_Venue__c = venueRecord.Id;                
    EventRecord.JVCO_Artist__c = testArtist.Id;
    EventRecord.JVCO_Event_Start_Date__c =  Date.Today();
    EventRecord.JVCO_Event_End_Date__c =Date.Today().addDays(25);
    EventRecord.JVCO_Invoice_Paid__c = false;
    EventRecord.JVCO_Event_Classification_Status__c = 'Classified';
    EventRecord.JVCO_Promoter__c = a3.id   ;
    EventRecord.JVCO_Tariff_Code__c = 'LC';
    EventRecord.JVCO_Event_Type__c = 'Concert - Qualifies for Old Rate';
    EventRecord.Headline_Type__c = 'No Headliner';
    EventRecord.JVCO_Number_of_Set_Lists_Expected__c = 2;
    insert EventRecord;

  }

  private static testMethod void setRenewalQuantityTest (){
    
    test.startTest();
    Account testAcc = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c!=null LIMIT 1];
    
    Product2 testProd = [SELECT Id FROM Product2 LIMIT 1];
    JVCO_Event__c testEvent = [SELECT Id FROM JVCO_Event__c LIMIT 1];

    Contract testCont = [SELECT Id FROM Contract LIMIT 1];

   
    testAcc.JVCO_Live__c = true;
    update testAcc;
    SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(testAcc.id, testProd.id);
    s.SBQQ__Contract__c = testCont.Id;
    insert s;
    test.stopTest();
    
    SBQQ__Subscription__c sub = [SELECT Id, SBQQ__RenewalQuantity__c FROM SBQQ__Subscription__c LIMIT 1];
    system.assertEquals(0,sub.SBQQ__RenewalQuantity__c);
  }

    private static testMethod void setRenewalQuantityTestSecondRenewalQuantity(){

        test.startTest();
            Account testAcc = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c!=null LIMIT 1];

            Product2 testProd = [SELECT Id FROM Product2 LIMIT 1];
            JVCO_Event__c testEvent = [SELECT Id FROM JVCO_Event__c LIMIT 1];

            Contract testCont = [SELECT Id FROM Contract LIMIT 1];
            JVCO_Affiliation__c testAffi = [SELECT Id FROM JVCO_Affiliation__c LIMIT 1];

            testAffi.JVCO_End_Date__c = Date.today();

            testAcc.JVCO_Live__c = true;
            update testAcc;
            SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(testAcc.id, testProd.id);
            s.SBQQ__Contract__c = testCont.Id;
            s.End_Date__c = Date.today();
            s.Affiliation__c = testAffi.Id;
            insert s;
        test.stopTest();

        SBQQ__Subscription__c sub = [SELECT Id, SBQQ__RenewalQuantity__c FROM SBQQ__Subscription__c LIMIT 1];
        system.assertEquals(0,sub.SBQQ__RenewalQuantity__c);
    }

    private static testMethod void setRenewalQuantityTestSecondRenewalQuantityUpdate(){

        test.startTest();
            Account testAcc = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c!=null LIMIT 1];

            Product2 testProd = [SELECT Id FROM Product2 LIMIT 1];
            JVCO_Event__c testEvent = [SELECT Id FROM JVCO_Event__c LIMIT 1];

            Contract testCont = [SELECT Id FROM Contract LIMIT 1];
            JVCO_Affiliation__c testAffi = [SELECT Id FROM JVCO_Affiliation__c LIMIT 1];

            testAffi.JVCO_End_Date__c = Date.today();

            testAcc.JVCO_Live__c = true;
            update testAcc;
            SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(testAcc.id, testProd.id);
            s.SBQQ__Contract__c = testCont.Id;
            s.End_Date__c = Date.today();
            s.Affiliation__c = testAffi.Id;
            insert s;
            s.End_Date__c = Date.today()+1;
            update s;
        test.stopTest();

        SBQQ__Subscription__c sub = [SELECT Id, SBQQ__RenewalQuantity__c FROM SBQQ__Subscription__c LIMIT 1];
        system.assertEquals(0,sub.SBQQ__RenewalQuantity__c);
    }

  private static testMethod void updateParentImportRecordTest (){
    
    test.startTest();
    Account testAcc = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c!=null LIMIT 1];
    SBQQ__QuoteLine__c parentQuoteLine = [SELECT Id, SBQQ__Quote__c, SBQQ__Product__c FROM SBQQ__QuoteLine__c WHERE SBQQ__RequiredBy__c=null LIMIT 1];
    Product2 testProd = [SELECT Id FROM Product2 LIMIT 1];
    JVCO_Event__c testEvent = [SELECT Id FROM JVCO_Event__c LIMIT 1];
    list<SBQQ__Subscription__c> subscriptionList = new list<SBQQ__Subscription__c>();

    SBQQ__QuoteLine__c quoteLine = parentQuoteLine.clone(false, true, false, false);
    quoteLine.SBQQ__Quote__c = parentQuoteLine.SBQQ__Quote__c;
    quoteLine.SBQQ__Product__c = parentQuoteLine.SBQQ__Product__c;
    quoteLine.SBQQ__RequiredBy__c = parentQuoteLine.id;
    insert quoteLine;

    
    SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(testAcc.id, testProd.id);
    s.JVCO_Event__c = testEvent.id;
    s.SBQQ__QuoteLine__c = parentQuoteLine.id;
    subscriptionList.add(s);

    SBQQ__Subscription__c s2 = JVCO_TestClassObjectBuilder.createSubscription(testAcc.id, testProd.id);
    s2.JVCO_Event__c = testEvent.id;
    s2.SBQQ__QuoteLine__c = quoteLine.id;
    subscriptionList.add(s2);

    insert subscriptionList;
    test.stopTest();

    //system.assertEquals(0,s.SBQQ__RenewalQuantity__c);
    //system.assertEquals(0,s2.SBQQ__RenewalQuantity__c);
  }

  private static testMethod void stopSubscriptionDeletionTestSysAd (){

    test.startTest();
    Account testAcc = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c!=null LIMIT 1];
    SBQQ__QuoteLine__c parentQuoteLine = [SELECT Id FROM SBQQ__QuoteLine__c WHERE SBQQ__RequiredBy__c=null LIMIT 1];
    Product2 testProd = [SELECT Id FROM Product2 LIMIT 1];
    JVCO_Event__c testEvent = [SELECT Id FROM JVCO_Event__c LIMIT 1];

    

    User systemAdmin = JVCO_TestClassObjectBuilder.createUser('System Administrator');
    insert systemAdmin;

    SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(testAcc.id, testProd.id);
    insert s;

    system.runAs(systemAdmin){
        delete s;
    }

    test.stopTest();

    system.assertEquals(0, [SELECT Id FROM SBQQ__Subscription__c].size());
  }
    private static testMethod void stopSubscriptionDeletionTestKeyAccMngr (){

    test.startTest();
    Account testAcc = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c!=null LIMIT 1];
    SBQQ__QuoteLine__c parentQuoteLine = [SELECT Id FROM SBQQ__QuoteLine__c WHERE SBQQ__RequiredBy__c=null LIMIT 1];
    Product2 testProd = [SELECT Id FROM Product2 LIMIT 1];
    JVCO_Event__c testEvent = [SELECT Id FROM JVCO_Event__c LIMIT 1];
    boolean expectedExceptionThrown;

    

    User keyAccMngr = JVCO_TestClassObjectBuilder.createUser('Key Accounts Manager');
    insert keyAccMngr;

    system.runAs(keyAccMngr){
    SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(testAcc.id, testProd.id);
    insert s;
    
    try{
        delete s;
        }catch(exception e){
            expectedExceptionThrown =  e.getMessage().contains('You cannot delete a subscription')? true:false;
        }
    }

    test.stopTest();
    System.Assert(expectedExceptionThrown);
  }

    @isTest
    static void testStampCurrentValues()
    {
        String LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Account accLicence = [Select Id From Account where RecordTypeId = :LicenceRT limit 1];
        accLicence.JVCO_Review_Frequency__c = 'Bi-annually';
        update accLicence;
        
        List<Contract> contList = [SELECT Id, AccountId, StartDate, EndDate FROM Contract WHERE AccountId =:accLicence.Id order by CreatedDate DESC limit 1];

        Integer startDate = -364;
        Integer endDate = 1;
        for(Contract cont : contList)
        {
            date sd = date.today().addDays(startDate);
            date ed = date.today().addDays(endDate);
            cont.StartDate = sd;
            cont.EndDate = ed;
            cont.AccountId = accLicence.Id;
            cont.JVCO_RenewableQuantity__c = 60;
            startDate--;
            endDate++;
        }

        update contList;

        Product2 proRecPRS = JVCO_TestClassObjectBuilder.createProduct();
        proRecPRS.JVCO_Society__c = 'PRS';
        insert proRecPRS; 

        Product2 proRecPPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecPPL.JVCO_Society__c = 'PPL';
        insert proRecPPL;

        Product2 proRecVPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecVPL.JVCO_Society__c = 'VPL';
        insert proRecVPL;

        List<SBQQ__Subscription__c> subList = new List<SBQQ__Subscription__c>();

        for(Contract cont : contList)
        {
            SBQQ__Subscription__c subsPRS = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecPRS.Id);
            subsPRS.SBQQ__Contract__c = cont.Id;
            subsPRS.SBQQ__ListPrice__c = 10;
            subsPRS.SBQQ__RenewalQuantity__c = 10;
            subsPRS.SBQQ__Quantity__c = 10;
            subsPRS.SBQQ__NetPrice__c = 10;
            subsPRS.SBQQ__TerminatedDate__c = null;
            subsPRS.Start_Date__c = cont.StartDate;
            subsPRS.End_Date__c = cont.EndDate;
            subsPRS.SBQQ__SubscriptionStartDate__c = cont.StartDate;
            subsPRS.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsPRS.ChargeYear__c = 'Current Year';
            subList.add(subsPRS);

            SBQQ__Subscription__c subsPPL = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecPPL.Id);
            subsPPL.SBQQ__Contract__c = cont.Id;
            subsPPL.SBQQ__ListPrice__c = 10;
            subsPPL.SBQQ__RenewalQuantity__c = 20;
            subsPPL.SBQQ__Quantity__c = 20;
            subsPPL.SBQQ__NetPrice__c = 10;
            subsPPL.SBQQ__TerminatedDate__c = null;
            subsPPL.Start_Date__c = cont.StartDate;
            subsPPL.End_Date__c = cont.EndDate;
            subsPPL.SBQQ__SubscriptionStartDate__c =cont.StartDate;
            subsPPL.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsPPL.ChargeYear__c = 'Current Year';
            subList.add(subsPPL);

             SBQQ__Subscription__c subsVPL = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecVPL.Id);
            subsVPL.SBQQ__Contract__c = cont.Id;
            subsVPL.SBQQ__ListPrice__c = 10;
            subsVPL.SBQQ__RenewalQuantity__c = 30;
            subsVPL.SBQQ__Quantity__c = 30;
            subsVPL.SBQQ__NetPrice__c = 10;
            subsVPL.SBQQ__TerminatedDate__c = null;
            subsVPL.Start_Date__c = cont.StartDate;
            subsVPL.End_Date__c = cont.EndDate;
            subsVPL.SBQQ__SubscriptionStartDate__c =cont.StartDate;
            subsVPL.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsVPL.ChargeYear__c = 'Current Year';
            subList.add(subsVPL);
        }

        Test.startTest();
        
        insert subList;

        Test.stopTest();


        Account accLicence1 = [Select Id, JVCO_PRS_Current_Value_Total__c, JVCO_PPL_Current_Value_Total__c, JVCO_VPL_Current_Value_Total__c From Account where RecordTypeId = :LicenceRT limit 1];

        system.assertEquals(100,accLicence1.JVCO_PRS_Current_Value_Total__c);
        system.assertEquals(200,accLicence1.JVCO_PPL_Current_Value_Total__c);
        system.assertEquals(300,accLicence1.JVCO_VPL_Current_Value_Total__c);

    }

    @isTest
    static void testStampMigratedCurrentValues()
    {
        String LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Account accLicence = [Select Id From Account where RecordTypeId = :LicenceRT limit 1];
        accLicence.JVCO_Review_Frequency__c = 'Bi-annually';
        update accLicence;
        
        List<Contract> contList = [SELECT Id, AccountId, StartDate, EndDate FROM Contract WHERE AccountId =:accLicence.Id order by CreatedDate DESC limit 1];

        Integer startDate = -364;
        Integer endDate = 1;
        for(Contract cont : contList)
        {
            date sd = date.today().addDays(startDate);
            date ed = date.today().addDays(endDate);
            cont.StartDate = sd;
            cont.EndDate = ed;
            cont.AccountId = accLicence.Id;
            cont.JVCO_RenewableQuantity__c = 60;
            cont.JVCO_Contract_Temp_External_ID__c = 'Cont: ' + startDate;
            startDate--;
            endDate++;
        }

        update contList;

        Product2 proRecPRS = JVCO_TestClassObjectBuilder.createProduct();
        proRecPRS.JVCO_Society__c = 'PRS';
        insert proRecPRS; 

        Product2 proRecPPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecPPL.JVCO_Society__c = 'PPL';
        insert proRecPPL;

        Product2 proRecVPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecVPL.JVCO_Society__c = 'VPL';
        insert proRecVPL;

        List<SBQQ__Subscription__c> subList = new List<SBQQ__Subscription__c>();

        for(Contract cont : contList)
        {
            SBQQ__Subscription__c subsPRS = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecPRS.Id);
            subsPRS.SBQQ__Contract__c = cont.Id;
            subsPRS.SBQQ__ListPrice__c = 10;
            subsPRS.SBQQ__RenewalQuantity__c = 10;
            subsPRS.SBQQ__Quantity__c = 10;
            subsPRS.SBQQ__NetPrice__c = 10;
            subsPRS.SBQQ__TerminatedDate__c = null;
            subsPRS.Start_Date__c = cont.StartDate;
            subsPRS.End_Date__c = cont.EndDate;
            subsPRS.SBQQ__SubscriptionStartDate__c = cont.StartDate;
            subsPRS.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsPRS.ChargeYear__c = 'Current Year';
            subsPRS.JVCO_Subscription_Temp_External_ID__c = 'PRSProduct: ' + subsPRS.Start_Date__c;
            subList.add(subsPRS);

            SBQQ__Subscription__c subsPPL = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecPPL.Id);
            subsPPL.SBQQ__Contract__c = cont.Id;
            subsPPL.SBQQ__ListPrice__c = 10;
            subsPPL.SBQQ__RenewalQuantity__c = 20;
            subsPPL.SBQQ__Quantity__c = 20;
            subsPPL.SBQQ__NetPrice__c = 10;
            subsPPL.SBQQ__TerminatedDate__c = null;
            subsPPL.Start_Date__c = cont.StartDate;
            subsPPL.End_Date__c = cont.EndDate;
            subsPPL.SBQQ__SubscriptionStartDate__c =cont.StartDate;
            subsPPL.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsPPL.ChargeYear__c = 'Current Year';
            subsPPL.JVCO_Subscription_Temp_External_ID__c = 'PPLProduct: ' + subsPPL.Start_Date__c;
            subList.add(subsPPL);

             SBQQ__Subscription__c subsVPL = JVCO_TestClassObjectBuilder.createSubscription(accLicence.Id,proRecVPL.Id);
            subsVPL.SBQQ__Contract__c = cont.Id;
            subsVPL.SBQQ__ListPrice__c = 10;
            subsVPL.SBQQ__RenewalQuantity__c = 30;
            subsVPL.SBQQ__Quantity__c = 30;
            subsVPL.SBQQ__NetPrice__c = 10;
            subsVPL.SBQQ__TerminatedDate__c = null;
            subsVPL.Start_Date__c = cont.StartDate;
            subsVPL.End_Date__c = cont.EndDate;
            subsVPL.SBQQ__SubscriptionStartDate__c =cont.StartDate;
            subsVPL.SBQQ__SubscriptionEndDate__c = cont.EndDate;
            subsVPL.ChargeYear__c = 'Current Year';
            subsVPL.JVCO_Subscription_Temp_External_ID__c = 'VPLProduct: ' + subsVPL.Start_Date__c;
            subList.add(subsVPL);
        }

        Test.startTest();
        
        insert subList;

        Test.stopTest();


        Account accLicence1 = [Select Id, JVCO_PRS_Current_Value_Total__c, JVCO_PPL_Current_Value_Total__c, JVCO_VPL_Current_Value_Total__c From Account where RecordTypeId = :LicenceRT limit 1];

        system.assertEquals(100,accLicence1.JVCO_PRS_Current_Value_Total__c);
        system.assertEquals(200,accLicence1.JVCO_PPL_Current_Value_Total__c);
        system.assertEquals(300,accLicence1.JVCO_VPL_Current_Value_Total__c);

    }

    private static testMethod void testSumTerminatedSubs(){
    
        test.startTest();
        Account testAcc = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c!=null LIMIT 1];
        
        Product2 testProd = [SELECT Id FROM Product2 LIMIT 1];
        
        Contract testCont = [SELECT Id FROM Contract WHERE AccountId =:testAcc.id LIMIT 1];

        JVCO_Affiliation__c testAff = [SElECT id, JVCO_Start_Date__c, JVCO_End_Date__c, JVCO_Closure_Reason__c FROM JVCO_Affiliation__c WHERE JVCO_Account__c =:testAcc.id LIMIT 1];
        testAff.JVCO_End_Date__c = System.today()-5;
        testAff.JVCO_Start_Date__c = System.today()-60;
        testAff.JVCO_Closure_Reason__c = 'Venue no longer exists';
        update testAff;

        
        SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(testAcc.id, testProd.id);
        s.SBQQ__Contract__c = testCont.Id;
        s.Terminate__c = true;
        s.Affiliation__c = testAff.id;
        s.SBQQ__ListPrice__c = 1;
        s.SBQQ__NetPrice__c =1;

        insert s;
        test.stopTest();

        SBQQ__Subscription__c sRec = [SELECT id, Affiliation_Venue_Closed__c, Terminate__c from SBQQ__Subscription__c where id =:s.id];
        System.assertEquals(sRec.Affiliation_Venue_Closed__c, true);
        Account testAcc1 = [SELECT Id, Total_Canceled_Subscription_Value__c FROM Account WHERE id =:testAcc.id];
        System.assert(testAcc1.Total_Canceled_Subscription_Value__c>0);

    }

    private static testMethod void testSubLookup(){

        test.startTest();

        Account testAcc = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c!=null LIMIT 1];

        Product2 testProd = [SELECT Id FROM Product2 LIMIT 1];
        JVCO_Event__c testEvent = [SELECT Id FROM JVCO_Event__c LIMIT 1];

        Contract testCont = [SELECT Id FROM Contract LIMIT 1];
        JVCO_Affiliation__c testAffi = [SELECT Id FROM JVCO_Affiliation__c LIMIT 1];
        JVCO_Venue__c testVen = [SELECT Id FROM JVCO_Venue__c LIMIT 1];

        testAffi.JVCO_End_Date__c = Date.today();

        SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(testAcc.id, testProd.id);
        s.SBQQ__Contract__c = testCont.Id;
        insert s;

        s.JVCO_TempSubAffLookup__c = testAffi.id;
        s.JVCO_TempSubVenueLookup__c = testVen.id;

        update s;

        test.stopTest();
    }
}
