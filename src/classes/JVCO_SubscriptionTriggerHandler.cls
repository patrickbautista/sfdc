/* ----------------------------------------------------------------------------------------------------------
    Name:            JVCO_SubscriptionTriggerHandler.cls 
    Description:     Trigger handler for JVCO_SubscriptionTrigger.trigger
    Test class:      

    Date             Version     Author                            Summary of Changes 
    -----------      -------     -----------------                 -------------------------------------------
    10-May-2017       0.1        Accenture-mel.andrei.b.santos         Initial version of the code 
    11-May-2017       0.2        Accenture-mel.andrei.b.santos       Changed to onBeforeUpdate   
    24-Sep-2017       0.3        franz.g.a.dimaapi                  Add Before Delete implementation  
    20-Oct-2017       0.4        jules.osberg.a.pablo               Added GREEN-25066 fix code that allows system admin to delete contracts
    04-Apr-2018       0.5        Accenture-rhys.j.c.dela.cruz       Created aggregate method for GREEN-31270
    13-Jun-2018       0.6        Accenture-reymark.j.l.arlos        Updated setContractValuesBySociety to handle the rollup of historic and current values GREEN-31270
    20-Jun-2018       0.7        rhys.j.c.dela.cruz                 GREEN-32427 - Added new method to call logic that will sum the totals of the Subscriptions to the related Event record
    04-July-2018      0.8        reymark.j.l.arlos                  Updated setContractValuesBySociety to exclude amendment generated subscription for migrated contracts to be calculated as current GREEN-32402
    24-Jul-2018       0.9        Accenture-rhys.j.c.dela.cruz       GREEN-32704 - temp lookup fields to copy to original fields for permission set
    19-Sep-2018       1.0        Accenture-reymark.j.l.arlos        Updated setContractValuesBySociety Removed codes that handle calculation of historic values and updated query for current values based on GREEN-33361 code update requirements
    04-Oct-2018       1.1        Accenture-reymark.j.l.arlos        Updated on how the current value is calculated based on the update of requirement on GREEN-33361
    03-Apr-2019       1.2        rhys.j.c.dela.cruz                 GREEN-34465 - Adjusted Stock Value method to Accomodate Past Year Values
 ---------------------------------------------------------------------------------------------------------- */

public class JVCO_SubscriptionTriggerHandler {
  
    
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_SubscriptionTrigger__c : false;


    public static void onBeforeInsert(List<SBQQ__Subscription__c> subscriptionList){
        if(!skipTrigger) {
            setRenewalQuantity(subscriptionList); 
            //system.debug('@@@@@@ onBeforeInsert :' + subscriptionList );         
            setRenewalQuantityOnBeforeInsert(subscriptionList);
        }
    }

    public static void onBeforeUpdate(List<SBQQ__Subscription__c> SubscriptionList, Map<Id, SBQQ__Subscription__c> oldSubscriptionMap){
        if(!skipTrigger) {
            setRenewalQuantity(subscriptionList);
            //system.debug('@@@@@@ onBeforeUpdate :' + subscriptionList );
            setRenewalQuantityBeforeUpdate(subscriptionList);
            copySubscriptionLookupValue(subscriptionList);
        }
    }
    
    public static void onBeforeDelete(Map<Id, SBQQ__Subscription__c> oldSubscriptionMap){
        //START 20-Oct-2017  jules.osberg.a.pablo GREEN-25066
        if(!skipTrigger) {
            //system.debug('@@@@@@ onBeforeDelete :' + oldSubscriptionMap ); 
            Profile p = [select id, name from profile where name = 'System Administrator' limit 1];
            if(p.id != UserInfo.getProfileId()) {
                if(!JVCO_FFUtil.stopSubscriptionDeletion)
                {
                    stopSubscriptionDeletion(oldSubscriptionMap);
                }
            }
            //END 20-Oct-2017  jules.osberg.a.pablo GREEN-25066 
        }
    }

    public static void onAfterDelete(Map<Id, SBQQ__Subscription__c> oldSubscriptionMap){
        //START 21-Jun-2018 rhys.j.c.dela.cruz
        if(!skipTrigger){
            updateEventRecordDetails(null, oldSubscriptionMap.values());
        }
        //END 21-Jun-2018 rhys.j.c.dela.cruz
    }

    public static void onAfterInsert(List<SBQQ__Subscription__c> subscriptionList){
        if(!skipTrigger) {
            //system.debug('@@@@@@ onAfterInsert :' + subscriptionList );
            setContractValuesBySociety(subscriptionList);
            updateEventRecordDetails(subscriptionList, null);
            rollupNetTotalOfTerminatedSubscription(subscriptionList);
            populateContractFieldsAggregate(subscriptionList);
        }
    }
    
    public static void onAfterUpdate(List<SBQQ__Subscription__c> SubscriptionList, Map<Id, SBQQ__Subscription__c> oldSubscriptionMap){ 
            if(!skipTrigger) {
            //system.debug('@@@@@@ onAfterInsert :' + subscriptionList );
            setContractValuesBySociety(subscriptionList);
            updateEventRecordDetails(subscriptionList, null);
            rollupNetTotalOfTerminatedSubscription(subscriptionList);
            populateContractFieldsAggregate(subscriptionList);
        }   
    }

/* ----------------------------------------------------------------------------------------------------------
    Method:            populateQuote 
    Description:   Setting of Renewal Quantity to 0 if Charge year is not equal to "Current Year"  
    Date             Version     Author                            Summary of Changes 
    -----------      -------     -----------------                 -------------------------------------------
    10-May-2017       0.1        Accenture-mel.andrei.b.santos         Initial version of the code 
    11-May-2017       0.2        Accenture-mel.andrei.b.santos       Changed to onBeforeUpdate    
    29-May-2017       0.3        Accenture-eu.rey.t.cadag              Renewal Contract for import event related tariff should also assign 0 to Renewal Quantity
    13-Nov-2017       0.4        Accenture-reymark.j.l.arlos        Updated to set renewal quantity to zero if quote is live. GREEN-25141
    22-Jan-2018       0.5        Accenture-mariel.m.buena           Remove updates made for GREEN-25141. Added update for GREEN-27596 If product is NOT Renewable set Renewal Qty = 0 
    08-May-2020		  0.6		 Accenture-rhys.j.c.dela.cruz		GREEN-35025 - Adjusted method to add filter to override certain Products their Renewal Quantity
 ---------------------------------------------------------------------------------------------------------- */  
    public static void setRenewalQuantity(List<SBQQ__Subscription__c> subscriptionList)
    {   
        Set<Id> prodSet = new Set<Id>();
        List<String> prodCodes = new List<String>();

        if(JVCO_Constants__c.getOrgDefaults().JVCO_ProductRenewQtyOverride__c != null && JVCO_Constants__c.getOrgDefaults().JVCO_ProductRenewQtyOverride__c != ''){

            prodCodes = JVCO_Constants__c.getOrgDefaults().JVCO_ProductRenewQtyOverride__c.split(';');
        }

        for(SBQQ__Subscription__c s : subscriptionList){

            prodSet.add(s.SBQQ__Product__c);
        }

        Map<Id, Product2> prodMap = new Map<Id, Product2>([SELECT Id, ProductCode FROM Product2 WHERE Id IN: prodSet]);

        for(SBQQ__Subscription__c s : subscriptionList){ //22-Jan-2017 mariel.m.buena Added update for GREEN-27596 If product is NOT Renewable set Renewal Qty = 0
            if(s.ChargeYear__c != 'Current Year' || s.JVCO_NOT_Renewable__c){
                s.SBQQ__RenewalQuantity__c = 0;           
            }
            else if(!prodCodes.isEmpty()){

                if(prodCodes.contains(prodMap.get(s.SBQQ__Product__c).ProductCode)){

                    s.SBQQ__RenewalQuantity__c = s.JVCO_NetTotal__c;
                }
            }
            
            // if(s.JVCO_NOT_Renewable__c)
            // {
            //     s.SBQQ__RenewalQuantity__c = 0;
            // }
        }

         
    }

    /* ----------------------------------------------------------------------------------------------------------
        Author:         franz.g.a.dimaapi
        Company:        Accenture
        Description:    Stop User from deleting the subscription
        Inputs:         Map<Id, SBQQ__Subscription__c>
        <Date>          <Authors Name>                      <Brief Description of Change> 
        24-Sep-2017     franz.g.a.dimaapi                    Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/  
    private static void stopSubscriptionDeletion(Map<Id, SBQQ__Subscription__c> oldSubscriptionMap)
    {
        for(Id cId : oldSubscriptionMap.keySet())
        {
            oldSubscriptionMap.get(cId).addError('You cannot delete a subscription');
        }         
    }

    /* ----------------------------------------------------------------------------------------------------------
        Author:         louis.a.g.del.rosario
        Company:        Accenture
        Description:    Set the renewal quantity to Zero Before Insert Context 
        Inputs:         Map<Id, SBQQ__Subscription__c>
        <Date>          <Authors Name>                      <Brief Description of Change> 
        06-Mar-2018     louis.a.g.del.rosario                    Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/  
    private static void setRenewalQuantityOnBeforeInsert(List<SBQQ__Subscription__c> subscriptionList)
    {
        Set<Id> setIdAffliation = new Set<Id>();
        Map<Id,JVCO_Affiliation__c> mapIdAffliation = new Map<Id,JVCO_Affiliation__c>();

        for(SBQQ__Subscription__c s : subscriptionList){
            if(s.Affiliation__c !=null){
                setIdAffliation.add(s.Affiliation__c);    
            }
        }
        if(!setIdAffliation.isEmpty()){
            Map<Id,JVCO_Affiliation__c> tempMap = new Map<Id,JVCO_Affiliation__c>([SELECT Id, JVCO_End_Date__c FROM JVCO_Affiliation__c where Id in :setIdAffliation]);
            mapIdAffliation.putAll(tempMap);
        }


        for(SBQQ__Subscription__c s : subscriptionList){
            if(!mapIdAffliation.isEmpty()){
                if(mapIdAffliation.containsKey(s.Affiliation__c)){
                    if(mapIdAffliation.get(s.Affiliation__c).JVCO_End_Date__c !=null && mapIdAffliation.get(s.Affiliation__c).JVCO_End_Date__c <= s.End_Date__c){
                        s.SBQQ__RenewalQuantity__c = 0;
                    }   
                }
            }
        }

    }

    /* ----------------------------------------------------------------------------------------------------------
        Author:         louis.a.g.del.rosario
        Company:        Accenture
        Description:    Set the renewal quantity to Zero BeforeUpdate Context
        Inputs:         Map<Id, SBQQ__Subscription__c>
        <Date>          <Authors Name>                      <Brief Description of Change> 
        06-Mar-2018     louis.a.g.del.rosario                   Initial version of the code
        19-Oct-2018     rhys.j.c.dela.cruz                      GREEN-32311 - Adjusted method
    ----------------------------------------------------------------------------------------------------------*/  
    private static void setRenewalQuantityBeforeUpdate(List<SBQQ__Subscription__c> subscriptionList)
    {
        Set<Id> affIdSet = new Set<Id>();
        Map<Id, JVCO_Affiliation__c> mapIdAffliation = new Map<Id, JVCO_Affiliation__c>();

        for(SBQQ__Subscription__c s : subscriptionList){
            if(s.Affiliation__c != null && s.SBQQ__RenewalQuantity__c != 0){
                affIdSet.add(s.Affiliation__c);
            }
        }

        if(!affIdSet.isEmpty()){
            Map<Id,JVCO_Affiliation__c> tempMap = new Map<Id,JVCO_Affiliation__c>([SELECT Id, JVCO_End_Date__c, JVCO_Closure_Reason__c FROM JVCO_Affiliation__c WHERE Id in :affIdSet]);
            mapIdAffliation.putAll(tempMap);
            System.debug('THE BUG affmap: ' + mapIdAffliation);
        }

        if(!mapIdAffliation.isEmpty()){
            for(SBQQ__Subscription__c s : subscriptionList){
                if(mapIdAffliation.containsKey(s.Affiliation__c)){
                    if(mapIdAffliation.get(s.Affiliation__c).JVCO_End_Date__c != null && mapIdAffliation.get(s.Affiliation__c).JVCO_End_Date__c <= s.End_Date__c && mapIdAffliation.get(s.Affiliation__c).JVCO_Closure_Reason__c != null){
                        s.SBQQ__RenewalQuantity__c = 0;
                    }
                }
            }
        }
    }
    
     /* ----------------------------------------------------------------------------------------------------------
        Author:         rhys.j.c.dela.cruz
        Company:        Accenture
        Description:    Aggregate method to set values on Contract fields for total by Product Society.
        Inputs:         Map<Id, SBQQ__Subscription__c>
        <Date>          <Authors Name>                      <Brief Description of Change> 
        06-Mar-2018     rhys.j.c.dela.cruz                   Initial version of the code
        18-Apr-2018     mariel.m.buena                       Assigning totalValue to Contract fields for GREEN-31270.
        13-Jun-2018     reymark.j.l.arlos                    Updated to handle the rollup of historic and current values GREEN-31270
        04-July-2018    reymark.j.l.arlos                    Updated to exclude amendment generated subscription for migrated contracts to be calculated as current GREEN-32402
        19-Sep-2018     reymark.j.l.arlos                    Removed codes that handle calculation of historic values and updated query for current values based on GREEN-33361 code update requirements
        04-Oct-2018     reymark.j.l.arlos                    Updated on how the current value is calculated based on the update on GREEN-33361
        11-Oct-2018     reymark.j.l.arlos                    Added additional for loop to set the current values to zero in order to update it to zero if the sum didn't return any value
        03-Apr-2019     rhys.j.c.dela.cruz                   GREEN-34465 - Adjusted method for Past Year Values
    ----------------------------------------------------------------------------------------------------------*/
    public static void setContractValuesBySociety(List<SBQQ__Subscription__c> subscriptionList)
    {
        Set<Id> contId = new Set<Id>();
        Set<Id> contIdLastYearSet = new Set<Id>();
        
        Decimal prsTotal = 0;
        Decimal pplTotal = 0;
        Decimal vplTotal = 0;

        //start 01-Oct-2018 reymark.j.l.arlos
        Set<Id> nonMigratedContract = new Set<Id>();
        Set<Id> migratedContract = new Set<Id>();
        //end 01-Oct-2018 reymark.j.l.arlos
        Integer lastYear = Date.today().year() - 1;

        for(SBQQ__Subscription__c sub : subscriptionList)
        {
            if(sub.SBQQ__Contract__c != null)
            {
                contId.add(sub.SBQQ__Contract__c);
            }

            if(sub.Start_Date__c != null){

                if(sub.SBQQ__Contract__c != null && sub.Start_Date__c.year() == lastYear){
                    contIdLastYearSet.add(sub.SBQQ__Contract__c);
                }
            }
        }

        if(!contId.isEmpty())
        {
            //start 04-Oct-2018 reymark.j.l.arlos updated on how the current value is calculated based on the update on GREEN-33361
            Map<Id,Contract> contMap = new Map<Id,Contract> ([SELECT Id, JVCO_PRS_Current_Value_Total__c, JVCO_PPL_Current_Value_Total__c, JVCO_VPL_Current_Value_Total__c, JVCO_PRS_Last_Year_ValueContract__c, JVCO_PPL_Last_Year_ValueContract__c, JVCO_VPL_Last_Year_ValueContract__c FROM Contract WHERE Id IN: contId ]); //JVCO_PRS_Historic_Value_Total__c, JVCO_PPL_Historic_Value_Total__c, JVCO_VPL_Historic_Value_Total__c

            //start 11-Oct-2018 reymark.j.l.arlos added additional for loop to set the current values to zero in order to update it to zero if the sum didn't return any value
            for(Contract contRec : contMap.values())
            {
                contRec.JVCO_PRS_Current_Value_Total__c = 0;
                contRec.JVCO_PPL_Current_Value_Total__c = 0;
                contRec.JVCO_VPL_Current_Value_Total__c = 0;

                //Last Year Values
                contRec.JVCO_PRS_Last_Year_ValueContract__c = 0;
                contRec.JVCO_PPL_Last_Year_ValueContract__c = 0;
                contRec.JVCO_VPL_Last_Year_ValueContract__c = 0;
            }
            //end 11-Oct-2018 reymark.j.l.arlos

            List<AggregateResult> currentSubValues = [SELECT SBQQ__Contract__c, SBQQ__Product__r.JVCO_Society__c aggSociety, SUM(JVCO_NetTotal__c) totalValue from SBQQ__Subscription__c WHERE SBQQ__RenewalQuantity__c != 0 AND SBQQ__TerminatedDate__c = null AND SBQQ__Contract__c IN: contId GROUP BY SBQQ__Contract__c, SBQQ__Product__r.JVCO_Society__c];

            //calculation for current value
            if(!currentSubValues.isEmpty())
            {
                for(AggregateResult currentAggResult : currentSubValues)
                {
                    //Total for PRS value
                    if(currentAggResult.get('aggSociety') == 'PRS')
                    {
                        prsTotal = ((decimal)currentAggResult.get('totalValue'));
                        
                        if(contMap.containsKey((Id)currentAggResult.get('SBQQ__Contract__c')))
                        {
                            contMap.get((Id)currentAggResult.get('SBQQ__Contract__c')).JVCO_PRS_Current_Value_Total__c = prsTotal;
                        }
                        
                    }

                    //Total for PPL value
                    if(currentAggResult.get('aggSociety') == 'PPL')
                    {
                        pplTotal = ((decimal)currentAggResult.get('totalValue'));
                        
                        if(contMap.containsKey((Id)currentAggResult.get('SBQQ__Contract__c')))
                        {
                            contMap.get((Id)currentAggResult.get('SBQQ__Contract__c')).JVCO_PPL_Current_Value_Total__c = pplTotal;
                        }
                        
                    }

                    //Total for VPL value
                    if(currentAggResult.get('aggSociety') == 'VPL')
                    {
                        vplTotal = ((decimal)currentAggResult.get('totalValue'));
                        
                        if(contMap.containsKey((Id)currentAggResult.get('SBQQ__Contract__c')))
                        {
                            contMap.get((Id)currentAggResult.get('SBQQ__Contract__c')).JVCO_VPL_Current_Value_Total__c = vplTotal;
                        }
                    }
                }
            }
            //end 04-Oct-2018 reymark.j.l.arlos GREEN-33361

            if(!contIdLastYearSet.isEmpty()){

                //Query for last year Sub Values
                List<SBQQ__Subscription__c> lastYearSubList = [SELECT Id, SBQQ__Contract__c, SBQQ__Product__r.JVCO_Society__c, JVCO_NetTotal__c FROM SBQQ__Subscription__c WHERE SBQQ__RenewalQuantity__c != 0 AND SBQQ__TerminatedDate__c = null AND CALENDAR_YEAR(Start_Date__c) =: lastYear AND JVCO_NetTotal__c != 0 AND SBQQ__Contract__c IN: contIdLastYearSet];
                
                if(!lastYearSubList.isEmpty()){

                    System.debug('For Loop CPU Test 2 Start: ' + Limits.getCpuTime());
                    for(SBQQ__Subscription__c sub : lastYearSubList){

                        String subSociety = sub.SBQQ__Product__r.JVCO_Society__c;

                        if(subSociety == 'PRS'){

                            contMap.get(sub.SBQQ__Contract__c).JVCO_PRS_Last_Year_ValueContract__c += sub.JVCO_NetTotal__c;
                        }
                        else if(subSociety == 'PPL'){

                            contMap.get(sub.SBQQ__Contract__c).JVCO_PPL_Last_Year_ValueContract__c += sub.JVCO_NetTotal__c;
                        }
                        else if(subSociety == 'VPL'){

                            contMap.get(sub.SBQQ__Contract__c).JVCO_VPL_Last_Year_ValueContract__c += sub.JVCO_NetTotal__c;
                        }
                    }
                    System.debug('For Loop CPU Test 2 End: ' + Limits.getCpuTime());
                }
            }
            
            update contMap.values();
        }        
    }


    /* ----------------------------------------------------------------------------------------------------------
        Author:         rhys.j.c.dela.cruz
        Company:        Accenture
        Description:    Method to call logic that would update related Event record. GREEN-32427
        Inputs:         Map<Id, SBQQ__Subscription__c>
        <Date>          <Authors Name>                      <Brief Description of Change> 
        20-Jun-2018     rhys.j.c.dela.cruz                   Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public static void updateEventRecordDetails(List<SBQQ__Subscription__c> subscriptionList, List<SBQQ__Subscription__c> subscriptionListOnDelete){

        Set<Id> evtIdSet = new Set<Id>();
        Set<Id> evtIdSetOnDelete = new Set<Id>();

        if(subscriptionList != null && subscriptionListOnDelete == null){
            for(SBQQ__Subscription__c subRec : subscriptionList){
                if(subRec.JVCO_Event__c != null){
                    evtIdSet.add(subRec.JVCO_Event__c);
                }
            }
        }else if(subscriptionListOnDelete != null && subscriptionList == null){
            for(SBQQ__Subscription__c subRec : subscriptionListOnDelete){
                if(subRec.JVCO_Event__c != null){
                    evtIdSetOnDelete.add(subRec.JVCO_Event__c);
                }
            }
        }

        if(!evtIdSet.isEmpty()){
            JVCO_ImportEventsLogic_enhancement.updateEventQuoteDetails(null, null, evtIdSet, null);
        }else if(!evtIdSetOnDelete.isEmpty()){
            JVCO_ImportEventsLogic_enhancement.updateEventQuoteDetails(null, null, null, evtIdSetOnDelete);
        }
    }

    /* ----------------------------------------------------------------------------------------------------------
        Author:         robert.j.b.lacatan
        Company:        Accenture
        Description:    Method to sum up net total of all terminated subscription
        Inputs:         
        <Date>          <Authors Name>                      <Brief Description of Change> 
        13-July-2018     robert.j.b.lacatan                   Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public static void rollupNetTotalOfTerminatedSubscription(List<SBQQ__Subscription__c> subscriptionList){

        Set<id> licAccSet = new Set<id>();
        List<Account> accListToUPdate = new List<Account>();
        
        for(SBQQ__Subscription__c subs: subscriptionList)
        {
            if(!licAccSet.contains(subs.SBQQ__Account__c))
            {
                if(subs.Affiliation_Venue_Closed__c && subs.Terminate__c)
                {
                    licAccSet.add(subs.SBQQ__Account__c);
                }
            }
        }


        if(!licAccSet.isEmpty())
        {
            for(AggregateResult ar: [SELECT SBQQ__Account__c, SUM(JVCO_NetTotal__c) sumNetTotal FROM SBQQ__Subscription__c WHERE SBQQ__Account__c IN :licAccSet AND Affiliation_Venue_Closed__c = TRUE AND Terminate__c=True GROUP BY SBQQ__Account__c])
            {
                Account upAcc = new Account();
                upAcc.id = ((Id)ar.get('SBQQ__Account__c'));
                upAcc.Total_Canceled_Subscription_Value__c = ((Decimal)ar.get('sumNetTotal'));
                accListToUPdate.add(upAcc);
            }
        }

        if(!accListToUPdate.isEmpty())
        {
            update accListToUPdate;
        }

    }

    /* ----------------------------------------------------------------------------------------------------------
      Author:         rhys.j.c.dela.cruz
      Company:        Accenture
      Description:    GREEN-32704 - temp lookup fields to copy to original fields for permission set
      Inputs:         List of Quote Lines
      Returns:        n/a
      <Date>          <Authors Name>                      <Brief Description of Change> 
      24-Jul-2018     rhys.j.c.dela.cruz                   Initial version
      ----------------------------------------------------------------------------------------------------------*/
    public static void copySubscriptionLookupValue(List<SBQQ__Subscription__c> subList){

        if(!subList.isEmpty()){
            for(SBQQ__Subscription__c sub : subList){
                if(sub.JVCO_TempSubAffLookup__c != null){
                    sub.Affiliation__c = sub.JVCO_TempSubAffLookup__c;
                    sub.JVCO_TempSubAffLookup__c = null;
                }
                if(sub.JVCO_TempSubVenueLookup__c != null){
                    sub.JVCO_Venue__c = sub.JVCO_TempSubVenueLookup__c;
                    sub.JVCO_TempSubVenueLookup__c = null;
                }
            }
        }
    }
    
    /* ----------------------------------------------------------------------------------------------------------
      Author:         rhys.j.c.dela.cruz
      Company:        Accenture
      Description:    GREEN-35686 - update fields on Contract
      Inputs:         List of Quote Lines
      Returns:        n/a
      <Date>          <Authors Name>                      <Brief Description of Change> 
      10-Jul-2020     rhys.j.c.dela.cruz                   Initial version
      ----------------------------------------------------------------------------------------------------------*/
    public static void populateContractFieldsAggregate(List<SBQQ__Subscription__c> subscriptionList){

        Set<Id> contIdSet = new Set<Id>();
        Map<Id, Contract> contMap = new Map<Id, Contract>();
        
        for(SBQQ__Subscription__c sub : subscriptionList){

            if(sub.SBQQ__Contract__c != null){
                contIdSet.add(sub.SBQQ__Contract__c);
            }
        }

        if(!contIdSet.isEmpty()){

            List<AggregateResult> contAggreList = [SELECT SBQQ__Contract__c, SBQQ__Product__r.Proratable__c isProratable, Count(Id) countOfProducts FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c IN: contIdSet GROUP BY SBQQ__Contract__c, SBQQ__Product__r.Proratable__c ORDER BY SBQQ__Contract__c ASC];

            if(!contAggreList.isEmpty()){

                for(AggregateResult aggResult : contAggreList){

                    if(contMap.containsKey((Id)aggResult.get('SBQQ__Contract__c'))){

                        if((Boolean)aggResult.get('isProratable')){

                            contMap.get((Id)aggResult.get('SBQQ__Contract__c')).JVCO_Number_of_Proratable_Products__c = (Integer)aggResult.get('countOfProducts');                         
                        }
                        else{

                            contMap.get((Id)aggResult.get('SBQQ__Contract__c')).JVCO_Number_of_Non_Proratable_Products__c = (Integer)aggResult.get('countOfProducts');
                        }

                        //contMap.get((Id)aggResult.get('SBQQ__Contract__c')).JVCO_Number_of_Products__c = contMap.get((Id)aggResult.get('SBQQ__Contract__c')).JVCO_Number_of_Proratable_Products__c + contMap.get((Id)aggResult.get('SBQQ__Contract__c')).JVCO_Number_of_Non_Proratable_Products__c;
                    }
                    else{

                        Contract contRec = new Contract();

                        contRec.Id = (Id)aggResult.get('SBQQ__Contract__c');
                        contRec.JVCO_Number_of_Proratable_Products__c = 0;
                        contRec.JVCO_Number_of_Non_Proratable_Products__c = 0;
                        //contRec.JVCO_Number_of_Products__c = 0;

                        if((Boolean)aggResult.get('isProratable')){

                            contRec.JVCO_Number_of_Proratable_Products__c = (Integer)aggResult.get('countOfProducts');
                        }
                        else{

                            contRec.JVCO_Number_of_Non_Proratable_Products__c = (Integer)aggResult.get('countOfProducts');
                        }

                        //contRec.JVCO_Number_of_Products__c = contRec.JVCO_Number_of_Proratable_Products__c + contRec.JVCO_Number_of_Non_Proratable_Products__c;

                        contMap.put((Id)aggResult.get('SBQQ__Contract__c'), contRec);
                    }
                }
            }
        }

        if(!contMap.isEmpty()){

            update contMap.values();
        }
    }
}