@isTest
public class JVCO_DCAQueryCasesBatchProcessTest {
@testSetup static void createTestData() 
    {   
        JVCO_SLA_Constants__c cs = JVCO_TestClassObjectBuilder.createSLAConstants();
        insert cs;
    
        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.JVCO_Incoming_Status_to_DCA__c = 'Withdrawal';
        acc2.JVCO_Incoming_Reason_Code__c = '100 - PAID IN FULL';
        insert acc2;
        
        SlaProcess ep = [SELECT Id FROM SlaProcess WHERE Name='standard support clone' order by CreatedDate DESC limit 1];

        Entitlement testEntitlement = JVCO_TestClassObjectBuilder.createEntitlement(acc2.id, ep.id);
        insert testEntitlement;

        Case caseRec = new Case();
        caseRec.AccountId = acc2.id;
        caseRec.JVCO_DCA_Status__c = null;
        caseRec.JVCO_Status_Reason_Code__c = null;
        caseRec.Description = null;
        caseRec.Status = 'Open';
        caseRec.Subject='Test--Email';
        caseRec.Origin='DCA';
        caseRec.Reason = 'DCA Query';
        caseRec.Type = 'Query';
        insert caseRec;

        JVCO_DisableTriggers__c caseHandler = new JVCO_DisableTriggers__c();
        caseHandler.JVCO_CaseTrigger__c = true;
        caseHandler.JVCO_AccountTrigger__c = true;
        insert caseHandler;
       
    }
    
    @isTest
    static void CaseWithdrawal1()
    {
        List<Case> caseVar = [SELECT id, Status,Type,
                             Description, JVCO_DCA_Status__c,
                             JVCO_Status_Reason_Code__c
                             FROM Case];
        
        Test.startTest();
        id batchjobid = Database.executeBatch(new JVCO_DCAQueryCasesBatchProcess());
        Test.stopTest();
        Case cas = [SELECT id, Status From Case WHERE id = :caseVar[0].id];
        System.assertEquals('Closed', cas.Status);
    }
    @isTest
    static void CaseWithdrawal2()
    {
        Account lic = [SELECT id, JVCO_Incoming_Status_to_DCA__c, JVCO_Incoming_Reason_Code__c FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];
        lic.JVCO_Incoming_Status_to_DCA__c = 'Withdrawal';
        lic.JVCO_Incoming_Reason_Code__c = '220 - RECALLED TO JVCO';
        update lic;
        
        List<Case> caseVar = [SELECT id, Status,Type,
                             Description, JVCO_DCA_Status__c,
                             JVCO_Status_Reason_Code__c
                             FROM Case];
        
        Test.startTest();
        id batchjobid = Database.executeBatch(new JVCO_DCAQueryCasesBatchProcess());
        Test.stopTest();
        Case cas = [SELECT id, Status From Case WHERE id = :caseVar[0].id];
        System.assertEquals('Closed', cas.Status);
    }
    @isTest
    static void CaseWithdrawal3()
    {
        Account lic = [SELECT id, JVCO_Incoming_Status_to_DCA__c, JVCO_Incoming_Reason_Code__c FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];
        lic.JVCO_Incoming_Status_to_DCA__c = 'Withdrawal';
        lic.JVCO_Incoming_Reason_Code__c = '230 - ACCOUNT CLOSED BY JVCO';
        update lic;
        
        List<Case> caseVar = [SELECT id, Status,Type,
                             Description, JVCO_DCA_Status__c,
                             JVCO_Status_Reason_Code__c
                             FROM Case];
        
        Test.startTest();
        id batchjobid = Database.executeBatch(new JVCO_DCAQueryCasesBatchProcess());
        Test.stopTest();
        Case cas = [SELECT id, Status From Case WHERE id = :caseVar[0].id];
        System.assertEquals('Closed', cas.Status);
    }
    @isTest
    static void CaseWithdrawal4()
    {
        Account lic = [SELECT id, JVCO_Incoming_Status_to_DCA__c, JVCO_Incoming_Reason_Code__c FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];
        lic.JVCO_Incoming_Status_to_DCA__c = 'On Hold';
        lic.JVCO_Incoming_Reason_Code__c = '370 - REQUEST TO PAY';
        update lic;
        
        List<Case> caseVar = [SELECT id, Status,Type,
                             Description, JVCO_DCA_Status__c,
                             JVCO_Status_Reason_Code__c
                             FROM Case];
        
        Test.startTest();
        id batchjobid = Database.executeBatch(new JVCO_DCAQueryCasesBatchProcess());
        Test.stopTest();
        Case cas = [SELECT id, Status From Case WHERE id = :caseVar[0].id];
        System.assertEquals('Closed', cas.Status);
    }
    @isTest
    static void CaseWithdrawal5()
    {
        Account lic = [SELECT id, JVCO_Incoming_Status_to_DCA__c, JVCO_Incoming_Reason_Code__c FROM Account WHERE RecordType.Name = 'Licence Account' LIMIT 1];
        lic.JVCO_Incoming_Status_to_DCA__c = 'On Hold';
        //lic.JVCO_Incoming_Reason_Code__c = '380 - PROMISE TO PAY';
        update lic;
        
        List<Case> caseVar = [SELECT id, Status,Type,
                             Description, JVCO_DCA_Status__c,
                             JVCO_Status_Reason_Code__c
                             FROM Case];
        
        Test.startTest();
        id batchjobid = Database.executeBatch(new JVCO_DCAQueryCasesBatchProcess());
        Test.stopTest();
        Case cas = [SELECT id, Status From Case WHERE id = :caseVar[0].id];
        //System.assertEquals('Closed', cas.Status);
    }
}