/* --------------------------------------------------------------------------------------------------
Name:            JVCO_codaCashMatchingHistoryHandler
Description:     Handler for JVCO_codaCashMatchingHistory Trigger
Test class:      JVCO_codaCashMatchingHistoryLogicTest

Date            Version     Author                      Summary of Changes 
-----------     -------     -----------------           -----------------------------------
22-Nov-2017     0.1         jason.e.mactal              Initial Creation
19-Dec-2017     0.2         chun.yin.tang               Adding disabling trigger to the cashmathcing history handler
20-Dec-2017     0.3         filip.bezak@accenture.com   GREEN-26914 - Update field Payment Made if new cash entry to invoice mapping is created
18-Jan-2018     0.4         filip.bezak@accenture.com   GREEN-28083 - TCP - AP Bank Account Used
02-Feb-2018     0.5         filip.bezak@accenture.com   GREEN-28502 - TC2P - Cash file PPL and PRS not showing expected results for part payment by Credit note and Cash entry
25-Feb-2018     0.6         mary.ann.a.ruelan           GREEN-30568 - Finance reconciliation cash matching task
08-Jun-2018     0.7         john.patrick.valdez         GREEN-32018 - Cash matching document(Sales Invoice Reference) number required on cash line
11-Jul-2018     0.8         franz.g.a.dimaapi           GREEN-31903 - Payment Received from Oriel, then after cash matching. 
Send Payment File back to Oriel repeating payment instruction.
09-Sep-2018     0.8         john.patrick.valdez         GREEN-31903 - Payment Received from Oriel, then after cash matching. 
Send Payment File back to Oriel repeating payment instruction.
------------------------------------------------------------------------------------------------ */
public class JVCO_codaCashMatchingHistoryHandler
{
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_codaCashMatchingHistoryTrigger__c : false;
    public static Boolean forceFutureRun = false;
    
    public static void beforeInsert(List<c2g__codaCashMatchingHistory__c > cashMatchingHistoryList)
    {
        if(!skipTrigger)
        {
            updateSalesInvoiceMatchingHistoryFields(cashMatchingHistoryList); // goods
            JVCO_codaCashMatchingHistoryLogic.checkIfMultipleMatching(cashMatchingHistoryList, forceFutureRun);
        }
    }
    
    public static void afterInsert(Map<Id,c2g__codaCashMatchingHistory__c> cashMatchingHistoryMap)
    {
        if(!skipTrigger)
        {
            Map<Id, c2g__codaInvoice__c> invoiceMap = new Map<Id, c2g__codaInvoice__c>();
            invoiceMap = JVCO_codaCashMatchingHistoryLogic.setSalesInvoicePaymentMadeFlag(cashMatchingHistoryMap, invoiceMap);
            invoiceMap = JVCO_codaCashMatchingHistoryLogic.setInvoiceCashPaidDate(cashMatchingHistoryMap, invoiceMap);
            JVCO_codaCashMatchingHistoryLogic.updateMatchedValues(cashMatchingHistoryMap, null, invoiceMap);
            
            //GREEN-28083 - not all matched references should call transfer to cash
            List<c2g__codaMatchingReference__c> filteredMatchingRef = JVCO_codaCashMatchingHistoryLogic.filterListForTransferToCash(cashMatchingHistoryMap);
            
            if(!JVCO_FFUtil.stopCashTransferLogic && !filteredMatchingRef.isEmpty())
            {
                //Run Transfer To Cash Functionality
                if(!forceFutureRun && !checkIfUserPassesValidation() && (System.isBatch() || System.isQueueable() || System.isScheduled() || System.isFuture()))
                {
                    new JVCO_CashTransferLogic().createJournalUsingBatch(filteredMatchingRef);
                }else
                {
                    JVCO_CashTransferLogic.createJournal(JVCO_CashTransferLogic.prepare(filteredMatchingRef), forceFutureRun);
                }
            }//transactionline update
            JVCO_codaCashMatchingHistoryLogic.setTransLineCashMatchingTotal(cashMatchingHistoryMap.values()); //mary.ann.a.ruelan 25-02-2018 GREEN-30568
            
            stampingOfCaseInfoWhenMatchingAndUnmatching(cashMatchingHistoryMap);
        }
    }
    
    public static void afterUpdate(Map<Id,c2g__codaCashMatchingHistory__c> cashMatchingHistoryMap, Map<Id,c2g__codaCashMatchingHistory__c> oldCashMatchingHistoryMap)
    {
        if(!skipTrigger)
        {
            JVCO_codaCashMatchingHistoryLogic.updateMatchedValues(cashMatchingHistoryMap, oldCashMatchingHistoryMap, new Map<Id, c2g__codaInvoice__c>());
        }
    }
    
    private static Boolean checkIfUserPassesValidation()
    {
        for(AsyncApexJob asynchApex : [SELECT Id,
                                       ApexClass.Name
                                       FROM AsyncApexJob
                                       WHERE ApexClass.Name = 'CashMatchingQueuedMatchService'
                                       AND CreatedById = :UserInfo.getUserId()
                                       AND Status IN ('Holding','Queued','Preparing','Processing')])
        {
            return true;
        }
        return false;
    }
    
    private static void updateSalesInvoiceMatchingHistoryFields(List<c2g__codaCashMatchingHistory__c > cashMatchingHistoryList)
    {   
        Map<Id, Set<Id>> matchRefIdToTranasctionLineIdSetMap = new Map<Id, Set<Id>>();
        Set<Id> transLineIdSet = new Set<Id>();
        
        Map<Id,List<Id>> matchRefIdToTransactionLineIdListMap = new Map<Id,List<Id>>();
        for(c2g__codaCashMatchingHistory__c cashMatchingHistory : cashMatchingHistoryList)
        {
            if(cashMatchingHistory.c2g__Action__c == 'Match' || cashMatchingHistory.c2g__Action__c == 'Undo Match')
            {
                transLineIdSet.add(cashMatchingHistory.c2g__TransactionLineItem__c);
                if(!matchRefIdToTranasctionLineIdSetMap.containsKey(cashMatchingHistory.c2g__MatchingReference__c))
                {
                    matchRefIdToTranasctionLineIdSetMap.put(cashMatchingHistory.c2g__MatchingReference__c, new Set<Id>());
                    matchRefIdToTransactionLineIdListMap.put(cashMatchingHistory.c2g__MatchingReference__c, new List<Id>());
                }
                matchRefIdToTranasctionLineIdSetMap.get(cashMatchingHistory.c2g__MatchingReference__c).add(cashMatchingHistory.c2g__TransactionLineItem__c);
                matchRefIdToTransactionLineIdListMap.get(cashMatchingHistory.c2g__MatchingReference__c).add(cashMatchingHistory.c2g__TransactionLineItem__c);
            }
            
            
            
        }
        
        Map<Id, c2g__codaTransactionLineItem__c> transactionLineMap = new Map<Id, c2g__codaTransactionLineItem__c>(
            [SELECT Id, c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Cash_Payment_Method__c,
             c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Cash_Payment_Method__c,
             c2g__Transaction__r.c2g__TransactionType__c
             FROM c2g__codaTransactionLineItem__c
             WHERE Id IN : transLineIdSet
            ]);
        
        
        
        for(c2g__codaCashMatchingHistory__c cashMatchingHistory : cashMatchingHistoryList)
        {
            
            if(cashMatchingHistory.c2g__Action__c == 'Match' || cashMatchingHistory.c2g__Action__c == 'Undo Match' &&
               transactionLineMap.containsKey(cashMatchingHistory.c2g__TransactionLineItem__c))
            {
                List<Id> tliIdList = matchRefIdToTransactionLineIdListMap.get(cashMatchingHistory.c2g__MatchingReference__c);
                
                if(cashMatchingHistory.c2g__TransactionLineItem__c == tliIdList[0])
                {
                    cashMatchingHistory.JVCO_Corresponding_Transaction_Line_Item__c = tliIdList[1];
                }
                else if(cashMatchingHistory.c2g__TransactionLineItem__c == tliIdList[1])
                {
                    cashMatchingHistory.JVCO_Corresponding_Transaction_Line_Item__c = tliIdList[0];
                }
                c2g__codaTransactionLineItem__c tli = transactionLineMap.get(cashMatchingHistory.c2g__TransactionLineItem__c);
                
                if(tli.c2g__Transaction__r.c2g__TransactionType__c == 'Invoice')
                {
                    if(tli.c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Cash_Payment_Method__c != null)
                    {
                        cashMatchingHistory.JVCO_Payment_Method__c = tli.c2g__Transaction__r.c2g__SalesInvoice__r.JVCO_Cash_Payment_Method__c;
                    }   
                }
                if(tli.c2g__Transaction__r.c2g__TransactionType__c == 'Credit Note')
                {
                    if(tli.c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Cash_Payment_Method__c != null)
                    {
                        cashMatchingHistory.JVCO_Payment_Method__c = tli.c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Cash_Payment_Method__c; 
                    }
                }
                
            }   
        }
    }
    
    
    public static void stampingOfCaseInfoWhenMatchingAndUnmatching(Map<Id,c2g__codaCashMatchingHistory__c> cashMatchingHistoryMap)
    {
        set<id> cashEntryIdSet = new set<id>();
        set<id> accountIdSet = new set<id>();
        set<Id> caseId = new set<Id>();
        set<String> salesInvoiceReferenceSet = new set<String>();
        List<c2g__codaCashEntryLineItem__c> cashEntryLineItemRecList = new List<c2g__codaCashEntryLineItem__c>();
        set<id> transactionLineItemIdSet = new set<id>(); 
        map<id,List<Case>> accountCaseMap = new  map<id,List<Case>>();
        List<JVCO_CashEntryLineItemHistory__c> cashEntryLineItemHistoryList = new List<JVCO_CashEntryLineItemHistory__c>();
        Map<id,JVCO_CashEntryLineItemHistory__c> customerAccountCashEntryLineHistoryMap = new Map<id,JVCO_CashEntryLineItemHistory__c>();
        List<c2g__codaCashEntryLineItem__c> updateCashEntryLineItemList = new List<c2g__codaCashEntryLineItem__c>();
        Map<id, Case> customerAccountAndInsolvencyCaseMap = new Map<id,Case>();
        Map<id, Case> customerAccountAndInfringmentCaseMap = new Map<id,Case>();
        Map<id, Case> customerAccountAndDebtRecoveryCaseMap = new Map<id,Case>();
        Map<id,String> transItemTypeOfMatchMap = new Map<id,String>();
        Map<String,String> transItemSalesInvoiceTypeOfMatchMap = new Map<String,String>();
        Map<String, CashEntryLineItemHistory_CS__c> recordTypeCustomSetting = CashEntryLineItemHistory_CS__c.getAll();
        Map<id,Map<String,id>> caseIdOpenInsolvency = new Map<id,Map<String,id>>();
        Map<id,Map<String,id>> caseIdOpenInfringement = new Map<id,Map<String,id>>();
        Map<id,Map<String,id>> caseIdOpenDebt = new Map<id,Map<String,id>>();
        Map<id,Map<String,id>> caseIdClosedInsolvency = new Map<id,Map<String,id>>();
        Map<id,Map<String,id>> caseIdClosedInfringement = new Map<id,Map<String,id>>();
        Map<id,Map<String,id>> caseIdClosedDebt = new Map<id,Map<String,id>>();
        
        for(c2g__codaCashMatchingHistory__c codaMatchingHistoryRec :cashMatchingHistoryMap.values())
        {
            if(codaMatchingHistoryRec.c2g__Action__c == 'Match' || codaMatchingHistoryRec.c2g__Action__c == 'Undo Match')
            {
                transactionLineItemIdSet.add(codaMatchingHistoryRec.c2g__TransactionLineItem__c);
                transItemTypeOfMatchMap.put(codaMatchingHistoryRec.c2g__TransactionLineItem__c,codaMatchingHistoryRec.c2g__Action__c);
            }
        }
        if(!transactionLineItemIdSet.isEmpty())
        {
            for(c2g__codaTransactionLineItem__c tLIRec: [SELECT id, c2g__Transaction__r.c2g__CashEntry__c, c2g__LineReference__c 
                                                         FROM c2g__codaTransactionLineItem__c 
                                                         WHERE Id IN :transactionLineItemIdSet 
                                                         AND c2g__Transaction__r.c2g__CashEntry__c != null
                                                         AND c2g__LineReference__c != null])
            {
                cashEntryIdSet.add(tLIRec.c2g__Transaction__r.c2g__CashEntry__c);
                salesInvoiceReferenceSet.add(tLIRec.c2g__LineReference__c);
                transItemSalesInvoiceTypeOfMatchMap.put(tLIRec.c2g__LineReference__c,transItemTypeOfMatchMap.get(tLIRec.id));
            }
        }
        
        if(!cashEntryIdSet.isEmpty())
        {
            //add field needed to be updated in this query
            cashEntryLineItemRecList = [SELECT id, c2g__Account__c, c2g__Account__r.JVCO_Customer_Account__c, JVCO_ExcludeFromDunningCycle__c,JVCO_Exclude_Reason__c,JVCO_Reminder_Level__c,JVCO_Collection_Agent__c,DCA_Type__c,DCA_Stage__c,JVCO_DCA_Status__c,
                                        JVCO_Status_Reason_Code__c,JVCO_Outgoing_Status_to_DCA_2nd_Stage__c,JVCO_Outgoing_Reason_Code_DCA_2nd_Stage__c,JVCO_Incoming_Status_to_DCA__c,JVCO_Incoming_Reason_Code__c,JVCO_Incoming_Status_from_DCA_2nd_Stage__c, JVCO_Debt_Recovery_Case_Number__c, 
                                        JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c,JVCO_CaseRecordType_Insolvency__c,JVCO_CaseStatus_Insolvency__c,JVCO_Enforcement_Status_Insolvency__c,JVCO_Insolvency_Case_Type__c, c2g__AccountReference__c, 
                                        JVCO_Insolvency_Case__c,JVCO_CaseRecordType_Infringement__c,JVCO_CaseStatus_Infringement__c,JVCO_Enforcement_Status_Infringement__c,JVCO_Enforcement_Infringement_case_Type__c, JVCO_Insolvency_Case_Number__c, 
                                        JVCO_Enforcement_Infringement_case__c,JVCO_CaseRecordType_Debt__c,JVCO_CaseStatus_Debt__c,JVCO_Enforcement_Status_Debt__c,JVCO_Enforcement_Debt_Recovery_Case_Type__c,JVCO_Enforcement_Debt_Recovery_Case__c, JVCO_Infringement_Case_Number__c, 
                                        c2g__Account__r.RecordType.Name, c2g__Account__r.JVCO_Credit_Status__c,c2g__Account__r.ffps_custRem__Exclude_From_Reminder_Process__c,c2g__Account__r.JVCO_Exclude_Reason__c,c2g__Account__r.ffps_custRem__Last_Reminder_Severity_Level__c,
                                        c2g__Account__r.JVCO_Collections_Agent__c,c2g__Account__r.JVCO_Collections_Agent__r.Name, c2g__Account__r.JVCO_Collections_Agent__r.FirstName, c2g__Account__r.JVCO_Collections_Agent__r.LastName, c2g__Account__r.JVCO_DCA_Type__c,c2g__Account__r.JVCO_DCA_Type_2nd_Stage__c,c2g__Account__r.JVCO_DCA_Status__c,c2g__Account__r.JVCO_Status_Reason_Code__c,c2g__Account__r.JVCO_Outgoing_Status_to_DCA_2nd_Stage__c,
                                        c2g__Account__r.JVCO_Outgoing_Reason_Code_DCA_2nd_Stage__c,c2g__Account__r.JVCO_Incoming_Status_to_DCA__c,c2g__Account__r.JVCO_Incoming_Reason_Code__c,c2g__Account__r.JVCO_Incoming_Status_from_DCA_2nd_Stage__c,c2g__Account__r.JVCO_Incoming_DCA_Reason_Code_2nd_Stage__c 
                                        FROM c2g__codaCashEntryLineItem__c 
                                        WHERE c2g__CashEntry__c IN:cashEntryIdSet
                                        AND c2g__AccountReference__c IN:salesInvoiceReferenceSet];
            
            for(c2g__codaCashEntryLineItem__c rec: cashEntryLineItemRecList)
            {
                if(rec.c2g__Account__r.JVCO_Customer_Account__c!=null)
                {
                    accountIdSet.add(rec.c2g__Account__r.JVCO_Customer_Account__c);
                }else
                {
                    accountIdSet.add(rec.c2g__Account__c);
                }
            }
        }
        
        
        
        if(!accountIdSet.isEmpty())
        {
            for(AggregateResult ar: [SELECT max(id) caseId, max(CreatedDate),
                                     RecordType.Name recType, AccountId acc
                                     FROM case 
                                     WHERE AccountId IN:accountIdSet 
                                     AND (RecordType.Name = 'Enforcement - Debt Recovery' 
                                          OR RecordType.Name = 'Enforcement - Infringement' 
                                          OR RecordType.Name = 'Insolvency') 
                                     AND Status !='Closed'
                                     GROUP BY AccountId, RecordType.Name 
                                     ORDER BY accountid ASC])
            {
                if(ar.get('recType') == 'Insolvency'){
                    Map<String,Id> tmpMap = new Map<String, Id>();
                    tmpMap.put('Insolvency', (Id)ar.get('caseId'));
                    caseIdOpenInsolvency.put((Id)ar.get('acc'),tmpMap);
                }
                if(ar.get('recType') == 'Enforcement - Debt Recovery'){
                    Map<String,Id> tmpMap = new Map<String, Id>();
                    tmpMap.put('Enforcement - Debt Recovery', (Id)ar.get('caseId'));
                    caseIdOpenDebt.put((Id)ar.get('acc'),tmpMap);
                    
                }
                if(ar.get('recType') == 'Enforcement - Infringement'){
                    Map<String,Id> tmpMap = new Map<String, Id>();
                    tmpMap.put('Enforcement - Infringement', (Id)ar.get('caseId'));
                    caseIdOpenInfringement.put((Id)ar.get('acc'),tmpMap);
                }
            }
            
            for(AggregateResult ar: [SELECT max(id) caseId, max(ClosedDate),
                                     RecordType.Name recType, AccountId acc
                                     FROM case 
                                     WHERE AccountId IN:accountIdSet 
                                     AND (RecordType.Name = 'Enforcement - Debt Recovery'
                                          OR RecordType.Name = 'Enforcement - Infringement' 
                                          OR RecordType.Name = 'Insolvency')
                                     AND Status ='Closed'
                                     GROUP BY AccountId, RecordType.Name 
                                     ORDER BY accountid asc])
            {
                if(ar.get('recType') == 'Insolvency'){
                    Map<String,Id> tmpMap = new Map<String, Id>();
                    tmpMap.put('Insolvency', (Id)ar.get('caseId'));
                    caseIdClosedInsolvency.put((Id)ar.get('acc'),tmpMap);
                    
                }
                if(ar.get('recType') == 'Enforcement - Debt Recovery'){
                    Map<String,Id> tmpMap = new Map<String, Id>();
                    tmpMap.put('Enforcement - Debt Recovery', (Id)ar.get('caseId'));
                    caseIdClosedDebt.put((Id)ar.get('acc'),tmpMap);
                    
                }
                if(ar.get('recType') == 'Enforcement - Infringement'){
                    Map<String,Id> tmpMap = new Map<String, Id>();
                    tmpMap.put('Enforcement - Infringement', (Id)ar.get('caseId'));
                    caseIdClosedInfringement.put((Id)ar.get('acc'),tmpMap);
                }
            }
            
            for(id acc: accountIdSet)
            {
                if(caseIdOpenInsolvency.containsKey(acc) && caseIdOpenInsolvency.get(acc).containsKey('Insolvency'))
                {
                    caseId.add(caseIdOpenInsolvency.get(acc).get('Insolvency'));
                }else
                {
                    if(caseIdClosedInsolvency.containsKey(acc))
                    {
                        caseId.add(caseIdClosedInsolvency.get(acc).get('Insolvency'));  
                    }
                }
                
                if(caseIdOpenDebt.containsKey(acc) && caseIdOpenDebt.get(acc).containsKey('Enforcement - Debt Recovery'))
                {
                    caseId.add(caseIdOpenDebt.get(acc).get('Enforcement - Debt Recovery'));
                }else
                {
                    if(caseIdClosedDebt.containsKey(acc))
                    {
                        caseId.add(caseIdClosedDebt.get(acc).get('Enforcement - Debt Recovery'));
                    }
                }
                
                if(caseIdOpenInfringement.containsKey(acc) && caseIdOpenInfringement.get(acc).containsKey('Enforcement - Infringement'))
                {
                    caseId.add(caseIdOpenInfringement.get(acc).get('Enforcement - Infringement'));
                }else
                {
                    if(caseIdClosedInfringement.containsKey(acc))
                    {
                        caseId.add(caseIdClosedInfringement.get(acc).get('Enforcement - Infringement'));
                    }
                }
            }
        }
        
        if(!caseId.isEmpty())
        {
            for(Case rec : [SELECT id, RecordType.Name, Status, 
                            JVCO_Enforcement_Status__c, JVCO_DCA_Type__c, 
                            Type, AccountId, Account.JVCO_Customer_Account__c, 
                            Account.RecordType.Name
                            FROM Case WHERE id IN: caseId])
            {
                if(rec.Account.RecordType.Name == 'Customer Account')
                {   
                    if(rec.RecordType.Name == 'Insolvency')
                    {
                        if(!customerAccountAndInsolvencyCaseMap.containsKey(rec.AccountId))
                        {
                            customerAccountAndInsolvencyCaseMap.put(rec.AccountId, rec);
                        }
                        
                    }else if(rec.RecordType.Name == 'Enforcement - Infringement')
                    {
                        if(!customerAccountAndInfringmentCaseMap.containsKey(rec.AccountId))
                        {
                            customerAccountAndInfringmentCaseMap.put(rec.AccountId, rec);
                        }
                        
                    }else if(rec.RecordType.Name == 'Enforcement - Debt Recovery')
                    {
                        if(!customerAccountAndDebtRecoveryCaseMap.containsKey(rec.AccountId))
                        {
                            customerAccountAndDebtRecoveryCaseMap.put(rec.AccountId, rec);
                        }
                    }
                    
                    if(accountCaseMap.containsKey(rec.AccountId))
                    {
                        List<Case> cRecList = accountCaseMap.get(rec.AccountId);
                        cRecList.add(rec);
                        accountCaseMap.put(rec.AccountId,cRecList);
                    }else
                    {
                        accountCaseMap.put(rec.AccountId, new List<Case>{rec});
                    }
                }
            }
        }
        
        if(!accountCaseMap.isEmpty())
        {
            for(Id rec: accountCaseMap.keyset())
            {
                JVCO_CashEntryLineItemHistory__c cashEntryLineItemHistoryRec = new JVCO_CashEntryLineItemHistory__c();
                cashEntryLineItemHistoryRec = JVCO_codaCashMatchingHistoryLogic.prepareCashEntryLineItemHistory(customerAccountAndInsolvencyCaseMap,customerAccountAndInfringmentCaseMap,customerAccountAndDebtRecoveryCaseMap,rec,cashEntryLineItemHistoryRec);
                if(!customerAccountCashEntryLineHistoryMap.containsKey(rec))
                {
                    customerAccountCashEntryLineHistoryMap.put(rec, cashEntryLineItemHistoryRec);
                }
            }
        }
        
        if(!cashEntryLineItemRecList.isEmpty())
        {
            for(c2g__codaCashEntryLineItem__c rec:cashEntryLineItemRecList)
            {
                if(customerAccountCashEntryLineHistoryMap.containsKey(rec.c2g__Account__r.JVCO_Customer_Account__c))
                {
                    rec = JVCO_codaCashMatchingHistoryLogic.stampValuesToCashEntryLineItemCaseValues(customerAccountCashEntryLineHistoryMap,rec,cashEntryLineItemHistoryList);
                    JVCO_CashEntryLineItemHistory__c cELIHRec = new JVCO_CashEntryLineItemHistory__c();
                    cELIHRec = JVCO_codaCashMatchingHistoryLogic.prepareCashEntryLineItemHistory(customerAccountAndInsolvencyCaseMap,customerAccountAndInfringmentCaseMap,customerAccountAndDebtRecoveryCaseMap,rec.c2g__Account__r.JVCO_Customer_Account__c,cELIHRec);
                    cELIHRec.CashEntryLineItem__c = rec.id;
                    
                    if(transItemSalesInvoiceTypeOfMatchMap.get(rec.c2g__AccountReference__c)=='Match')
                    {
                        cELIHRec.RecordTypeId = recordTypeCustomSetting.get('Matched').Value__c;
                    }
                    
                    if(transItemSalesInvoiceTypeOfMatchMap.get(rec.c2g__AccountReference__c)=='Undo Match')
                    {
                        cELIHRec.RecordTypeId = recordTypeCustomSetting.get('Reversed').Value__c;
                    }
                    cashEntryLineItemHistoryList.add(cELIHRec);
                }
                rec = JVCO_codaCashMatchingHistoryLogic.stampValuesToCashEntryLineItemAccountValues (rec);
                updateCashEntryLineItemList.add(rec);
            }
        }
        
        insert cashEntryLineItemHistoryList;
        update updateCashEntryLineItemList;
    }
}