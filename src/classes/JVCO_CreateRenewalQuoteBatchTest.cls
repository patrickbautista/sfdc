/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CreateRenewalQuoteBatchTest
   Description: Test Class of Create Renewal Quote Batch

   Date         Version     Author                          Summary of Changes 
   -----------  -------     -----------------               -----------------------------------------
  05-Jul-2017   0.1         Accenture-erica.jane.w.matias   Intial creation
  28-Dec-2017   0.2         Accenture-r.p.valencia.iii      Updated testSetup to accommodate change on GREEN-25234
  29-Dec-2017   0.3         Accenture-r.p.valencia.iii      Added another test method for error logs on JVCO_CreateRenewalQuotesBatch
  
  ----------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_CreateRenewalQuoteBatchTest
{
    @testSetup static void insertValues()
    {
        List<JVCO_Constants__c> custSetting = new List<JVCO_Constants__c>();
        custSetting.add(new JVCO_Constants__c(JVCO_Quote_Recalculation_Time_Buffer__c = 5));
        insert custSetting;
        
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        JVCO_FFUtil.stopCodaInvoiceHandlerAfterUpdate = true;
        JVCO_FFUtil.stopOrderAfterUpdateHandler = true;
        JVCO_FFUtil.stopOrderBeforeUpdateHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();  
        insert qProcess;

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Account acc =  JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.JVCO_Review_Type__c = 'Automatic';
        acc2.JVCO_DD_Payee_Contact__c = acc2.JVCO_Billing_Contact__c;
        //acc2.JVCO_Renewal_Scenario__c = 'PPL First: Greater Than 60 Days';
        insert acc2;

        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;
        Test.startTest();
        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_Closure_Reason__c = '';
        aff.JVCO_End_Date__c = null;
        insert aff;

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        insert opp;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, 'Quarterly');
        q.SBQQ__Status__c = 'Draft';
        q.Recalculate__c = true;
        q.End_Date__c = date.today()-1;
        q.Start_Date__c = date.today()-35;
        q.SBQQ__EndDate__c = date.today()-1;
        q.SBQQ__StartDate__c = date.today()-35;
        q.SBQQ__Type__c = 'Quote';
        insert q;
        
        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c order by CreatedDate DESC limit 1];

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, prod.Id);
        insert ql;

        q.SBQQ__Type__c = 'Renewal';
        q.JVCO_Quote_Auto_Renewed__c = false;

        update q;

        Contract c = JVCO_TestClassObjectBuilder.createContract(acc2.id, q.id);
        c.JVCO_Licence_Period_End_Date__c = date.today() -5 ;
        c.JVCO_Sum_Subsc_Renewal_Qty__c = 50;
        c.StartDate = date.today()-35;
        c.EndDate = c.StartDate + 34;
        c.SBQQ__RenewalQuoted__c = false;
        c.JVCO_RenewableQuantity__c = 50;
        insert c;

        SBQQ__Subscription__c sub = JVCO_TestClassObjectBuilder.createSubscription(acc2.id, prod.id);
        sub.sbqq__contract__c = c.id;
        sub.SBQQ__RenewalQuantity__c = 50;
        insert sub;

        Contract c2 = new Contract();
        //System.assertEquals(c, c2, 'error');

        List<JVCO_Renewal_Settings__c> settings = new List<JVCO_Renewal_Settings__c>();
        settings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Completion', Days__c = 1.0));
        settings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Generation', Days__c = 1.0));
        insert settings;
        Test.stopTest();    
    }

    @isTest
    static void testCreateRenewalQuoteBatch()
    {      
        Contract c = [SELECT id, SBQQ__RenewalQuoted__c,StartDate,EndDate,JVCO_RenewableQuantity__c  FROM Contract order by CreatedDate DESC limit 1];
        c.JVCO_RenewableQuantity__c = 50;
        System.assertEquals(false, c.SBQQ__RenewalQuoted__c, 'Contract was not setup correctly');
        System.assertEquals(50, c.JVCO_RenewableQuantity__c, 'Contract JVCO_RenewableQuantity__c  is wrong');
        
        Test.startTest();
        update c;
        database.executeBatch(new JVCO_CreateRenewalQuoteBatch(),5);
        Test.stopTest();       

        Contract c2 = [SELECT id, SBQQ__RenewalQuoted__c,StartDate,EndDate FROM Contract order by CreatedDate DESC limit 1];
        
        System.assertEquals(date.today()-35, c2.StartDate, 'Contract StartDate is wrong');
        System.assertEquals(date.today()-1, c2.EndDate, 'Contract EndDate is wrong');
        //System.assertEquals(true, c2.SBQQ__RenewalQuoted__c, 'Contract was not updated');

    }

    @isTest
    static void testCreateRenewalQuoteBatchString()
    {      
        Contract c = [SELECT id, SBQQ__RenewalQuoted__c FROM Contract order by CreatedDate DESC limit 1];
        
        Test.startTest();   
        database.executeBatch(new JVCO_CreateRenewalQuoteBatch(c.id));
        Test.stopTest();

        //Contract c2 = [SELECT id, SBQQ__RenewalQuoted__c FROM Contract order by CreatedDate DESC limit 1];

        //System.assertEquals(true, c2.SBQQ__RenewalQuoted__c, 'Contract was not updated');

    }

    @isTest
    static void testCreateRenewalQuoteBatchSet()
    {       
        List<Contract> c = [SELECT id, SBQQ__RenewalQuoted__c FROM Contract order by CreatedDate DESC limit 1];
        Set<id> set_contr = new Set<id>();

        for (Contract co : c)
        {
            set_contr.add(co.id);
        }

        Test.startTest();
        database.executeBatch(new JVCO_CreateRenewalQuoteBatch(set_contr));
        Test.stopTest();

        //Contract c2 = [SELECT id, SBQQ__RenewalQuoted__c FROM Contract order by CreatedDate DESC limit 1];

        //System.assertEquals(true, c2.SBQQ__RenewalQuoted__c, 'Contract was not updated');

    }

    @isTest
    static void testErrorLogs()
    {      
        Contract c = [SELECT id, SBQQ__RenewalQuoted__c,StartDate,EndDate,JVCO_RenewableQuantity__c  FROM Contract order by CreatedDate DESC limit 1];
        c.JVCO_RenewableQuantity__c = 50;
        
        Test.startTest();
        update c;
        database.executeBatch(new JVCO_CreateRenewalQuoteBatch(),5);
        Test.stopTest();

        //if(test.isRunningTest()){
        //    Database.executeBatch(new JVCO_CreateRenewalQuoteBatch(), 5);
        //}
        
        //System.assertEquals(date.today()-40, s.SBQQ__SubscriptionEndDate__c , 'Subscription EndDate is wrong');
        //System.assertEquals(date.today()-35, s.SBQQ__SubscriptionStartDate__c , 'Subscription StartDate is wrong');
        
    }
}