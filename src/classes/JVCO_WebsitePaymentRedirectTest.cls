@isTest
private class JVCO_WebsitePaymentRedirectTest {
    
    @TestSetup static void createTestData(){
        
        JVCO_TestClassHelper.setGeneralSettingsCS();
        JVCO_TestClassHelper.setCashMatchingCS();
        
        JVCO_Invoice_Group__c invGrp = new JVCO_Invoice_Group__c();
        invGrp.JVCO_Status__c = 'Active';
        insert invGrp;
        
        PAYBASE2__Payment__c testPayment = JVCO_TestClassHelper.getPayonomyPayment('SagePay');
        testPayment.PAYCP2__Payment_Description__c = invGrp.Name;
        insert testPayment;
        
        JVCO_Website_Payment_Setting__c st = new  JVCO_Website_Payment_Setting__c();
        st.reCaptchaPublicKey__c = '6Lc4B7sUAAAAAOCIV0yMRPJtMVn60Ew98fr7NRMU';
        st.reCaptchaPrivateKey__c = '6Lc4B7sUAAAAAEJjWV0yiv37wr3GNZlsDvCIo7LA';
        st.name = 'WebsitePaymentSetting';
        st.ThanksPageURL__c = 'https://www.google.com/';
        st.EnvURL__c = 'www.google.com';
        st.CardMaxLimit__c = 100000;
        st.ProductName__c = 'Payonomy Validator';
        st.Version__c = '1.1';
        st.Invalid_Account__c = 'test';
        st.Long_Billing_Street__c = 'test';
        st.Invalid_Postcode__c = 'test';
        st.PaymentLimtMsg__c = 'test';
        st.Recaptcha_Incomplete__c = 'test';
        st.Unpaid_Credit_Note__c = 'test';
        st.reCaptchaPublicKey__c = 'test';
        st.No_Billing_Address__c = 'test';
        st.Invalid_Invoice__c = 'test';
        st.Active_Direct_Debit__c = 'test';
        st.In_Infringement__c = 'test';
        st.OneInvMustBeSelected__c = 'test';
        st.Missing_Invoice_Or_Postcode__c = 'test';
        st.Invalid_Email__c = 'test';
        st.In_Enforcement__c = 'test';
        st.All_Invoices_Paid__c = 'test';
        Insert st;
        
    }
    
    static testmethod void testWebPayRedirectDone()
    {
        
        PAYBASE2__Payment__c testPayment = [SELECT Id, PAYBASE2__Is_Paid__c FROM PAYBASE2__Payment__c Limit 1];
        PageReference pageRef = Page.JVCO_WebsitePaymentRedirect;
        test.setCurrentPage(pageRef);
        apexPages.Currentpage().getParameters().put('Id',testPayment.id);

         ApexPages.StandardController sc = new ApexPages.StandardController(testPayment);
        JVCO_WebsitePaymentRedirectController webPayRedContr = new JVCO_WebsitePaymentRedirectController(sc);
        webPayRedContr.init();
    }
    static testmethod void testWebPayRedirectCancel()
    {
        PAYBASE2__Payment__c testPayment = [SELECT Id, PAYBASE2__Is_Paid__c FROM PAYBASE2__Payment__c Limit 1];
        testPayment.PAYBASE2__Is_Paid__c = false;
        PageReference pageRef = Page.JVCO_WebsitePaymentRedirect;
        test.setCurrentPage(pageRef);
        apexPages.Currentpage().getParameters().put('Id',testPayment.id);

         ApexPages.StandardController sc = new ApexPages.StandardController(testPayment);
        JVCO_WebsitePaymentRedirectController webPayRedContr = new JVCO_WebsitePaymentRedirectController(sc);
        webPayRedContr.init();
    }
}