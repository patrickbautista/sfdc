/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_NotesTriggerHandler.cls 
   Description:     Handler Class for JVCO_NotesTrigger
   Test class:      JVCO_NotesTriggerHandlerTest.cls 

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
  05-Apr-2018       0.1         Accenture-recuerdo.b.bregente     Initial draft           
  ---------------------------------------------------------------------------------------------------------- */
public class JVCO_NotesTriggerHandler {
	public static void onBeforeInsert(List<Note> notes){
        List<Note> noteToProcessForRestriction = new List<Note>();
        Set<Id> accountIdsRestriction = new Set<Id>();
        
        for(Note noteRec : notes){
            if(noteRec.ParentId <> null){
                accountIdsRestriction.add(noteRec.ParentId);
                noteToProcessForRestriction.add(noteRec);
            }
        }
        restrictOnInfringementOrEnforcement(noteToProcessForRestriction, accountIdsRestriction);
    }
    
    public static void onBeforeUpdate(List<Note> notes){
        List<Note> noteToProcessForRestriction = new List<Note>();
        Set<Id> accountIdsRestriction = new Set<Id>();
        
        for(Note noteRec : notes){
            if(noteRec.ParentId <> null){
                accountIdsRestriction.add(noteRec.ParentId);
                noteToProcessForRestriction.add(noteRec);
            }
        }
        restrictUpdateAndDelete(noteToProcessForRestriction, accountIdsRestriction, 'Updating');
    }
    
    public static void onBeforeDelete(List<Note> notes){
        List<Note> noteToProcessForRestriction = new List<Note>();
        Set<Id> accountIdsRestriction = new Set<Id>();
        
        for(Note noteRec : notes){
            if(noteRec.ParentId <> null){
                accountIdsRestriction.add(noteRec.ParentId);
                noteToProcessForRestriction.add(noteRec);
            }
        }
        restrictUpdateAndDelete(noteToProcessForRestriction, accountIdsRestriction, 'Deleting');
    }
    
    private static void restrictOnInfringementOrEnforcement(List<Note> notes, Set<Id> accountIdsRestriction){
        Map<Id, Account> restrictedAccountsMap = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN :accountIdsRestriction AND 
                                                    (JVCO_In_Enforcement__c = true OR JVCO_In_Infringement__c = true) AND 
                                                    (JVCO_Credit_Status__c = 'Enforcement - Debt Recovery' OR JVCO_Credit_Status__c = 'Enforcement - Infringement')]);
        
                                                            
        Boolean isRestricted = (restrictedAccountsMap.size() > 0 && checkUserIfRestricted());
       
        for(Note noteRec : notes){
            if(isRestricted && restrictedAccountsMap.containsKey(noteRec.ParentId)){
                noteRec.addError('Creating Notes on Accounts marked "In Enforcement" or "In Infringement" is not allowed.');
            }
        }
    }
    
    private static void restrictUpdateAndDelete(List<Note> notes, Set<Id> accountIdsRestriction, String operation){
        Map<Id, Account> restrictedAccountsMap = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN :accountIdsRestriction AND 
                                                    (JVCO_In_Enforcement__c = true OR JVCO_In_Infringement__c = true) AND 
                                                    (JVCO_Credit_Status__c = 'Enforcement - Debt Recovery' OR JVCO_Credit_Status__c = 'Enforcement - Infringement')]);
        
                                                            
        Boolean isRestricted = (restrictedAccountsMap.size() > 0 && checkUserIfRestricted());
       
        for(Note noteRec : notes){
            if(isRestricted && restrictedAccountsMap.containsKey(noteRec.ParentId)){
                noteRec.addError(operation + ' Notes on Accounts marked "In Enforcement" or "In Infringement" is not allowed.');
            }
        }
    }
    
    
    private static Boolean checkUserIfRestricted(){
        String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
        Map<Id,PermissionSet> enAdPermSet = new Map<Id,PermissionSet>([SELECT Id FROM PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors']);
        List<PermissionSetAssignment> permissionSetList = [SELECT Id FROM PermissionSetAssignment WHERE 
                                                            AssigneeId = :UserInfo.getUserId() AND PermissionSetId IN :enAdPermSet.keySet()];
                                                            
        return (profileName <> 'System Administrator' && permissionSetList.isEmpty());
    }
}