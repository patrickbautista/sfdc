public class JVCO_MatchToCreditNoteLogic extends JVCO_MatchingReferenceDocumentWrapper implements JVCO_MatchingReferenceDocumentInterface
{
	private JVCO_MatchToCreditNoteLogic(){}

	public JVCO_MatchToCreditNoteLogic(Map<Id, String> transLineIdToRefDocMap, Map<Id, c2g__codaTransactionLineItem__c> transLineMap)
	{
		super.transLineIdToRefDocMap = transLineIdToRefDocMap;
		super.transLineMap = transLineMap;
	}

	public void execute()
	{
		stopperMethod();
		setMatchingReferenceTransactionLine();
		callMatchingAPI();
	}

	private void setMatchingReferenceTransactionLine()
	{
		//Get all Transaction Line account with the given Reference Document
		//Map it to Reference Document - refDocToMatchingRefTransLineMap
		for(c2g__codaTransactionLineItem__c referenceTransLine : [SELECT Id, Name,
																	c2g__Transaction__r.c2g__TransactionType__c,
																	c2g__Transaction__r.c2g__SalesInvoice__r.Name,
																	c2g__Transaction__r.c2g__SalesCreditNote__r.Name,
																	c2g__Transaction__r.c2g__Journal__r.Name,
																	c2g__AccountOutstandingValue__c
																	FROM c2g__codaTransactionLineItem__c
																	WHERE c2g__MatchingStatus__c = 'Available'
						                                            AND c2g__LineType__c = 'Account'
						                                            AND c2g__LineDescription__c != 'Not for Matching'
						                                            AND c2g__Transaction__r.c2g__SalesCreditNote__r.Name IN : transLineIdToRefDocMap.values()])
		{
			refDocToMatchingRefTransLineMap.put(referenceTransLine.c2g__Transaction__r.c2g__SalesCreditNote__r.Name, referenceTransLine);
		}
	}

	

	private void stopperMethod()
	{
		//JVCO_FFUtil.stopCashTransferLogic = true;
	}
}