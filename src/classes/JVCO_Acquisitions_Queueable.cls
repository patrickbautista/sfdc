/* ----------------------------------------------------------------------------------------------
Name: JVCO_Acquisitions_Queueable
Description: Queueable Class for JVCO_AcquisitionsExtension

Date            Version     Author              Summary of Changes 
-----------     -------     -----------------   -----------------------------------------
19-Jun 2019 	0.1	 		mel.andrei.b.santos	Intial creation
GREEN-35188
----------------------------------------------------------------------------------------------- */

public class JVCO_Acquisitions_Queueable implements Queueable  
{
	public Account accountRecord;
	public Boolean isAgency;
    public Map<String, Date> mapTempIdJVCOAffiliationDate;
    public Map<String, Boolean> mapTempIdJVCOAffiliationCheckBox;
    public Map<String, String> mapTempIdJVCOAffiliationCustRefNum;
    public Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsDisplayMap;
    public Integer processSize;

	public JVCO_Acquisitions_Queueable( Account accountRecordHolder, Boolean isAgencyHolder, Map<String, Date> mapTempIdJVCOAffiliationDateHolder , Map<String, Boolean> mapTempIdJVCOAffiliationCheckBoxHolder, Map<String, String> mapTempIdJVCOAffiliationCustRefNumHolder, Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsDisplayMapHolder, Integer processSizeHolder) 
	{
		accountRecord = accountRecordHolder;
		isAgency = isAgencyHolder;
		mapTempIdJVCOAffiliationDate = mapTempIdJVCOAffiliationDateHolder;
		mapTempIdJVCOAffiliationCheckBox = mapTempIdJVCOAffiliationCheckBoxHolder;
        mapTempIdJVCOAffiliationCustRefNum = mapTempIdJVCOAffiliationCustRefNumHolder;
		transferSelectionsDisplayMap = transferSelectionsDisplayMapHolder;
		processSize = processSizeHolder;
		
	}


	public void execute(QueueableContext context)
	{
		Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsCheckerMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();

        //Start LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
        if(!mapTempIdJVCOAffiliationDate.isEmpty()) 
        {
        	system.debug('@@@ transferSelectionsDisplayMap' + transferSelectionsDisplayMap);
        	system.debug('@@@ mapTempIdJVCOAffiliationDate' + mapTempIdJVCOAffiliationDate);
            for(String kAffli: mapTempIdJVCOAffiliationDate.keySet())
            {
              	//if(transferSelectionsDisplayMap.get(kAffli) != null)
            	//{
            		system.debug('@@@ transferSelectionsDisplayMap' + transferSelectionsDisplayMap);
            		system.debug('@@@ transferSelectionsDisplayMap.get(kAffli)' + transferSelectionsDisplayMap.get(kAffli));
            		transferSelectionsDisplayMap.get(kAffli).affiliationRecord.JVCO_Start_Date__c = Date.valueOf(mapTempIdJVCOAffiliationDate.get(kAffli));
            	//}
                
            }
        }
        
        if(!mapTempIdJVCOAffiliationCheckBox.isEmpty()) 
        {
        	system.debug('@@@ mapTempIdJVCOAffiliationCheckBox' + mapTempIdJVCOAffiliationCheckBox);
            for(String kAffli: mapTempIdJVCOAffiliationCheckBox.keySet())
            {
            	//if(transferSelectionsDisplayMap.get(kAffli) != null)
            	//{
            		transferSelectionsDisplayMap.get(kAffli).affiliationRecord.JVCO_Omit_Quote_Line_Group__c = Boolean.valueOf(mapTempIdJVCOAffiliationCheckBox.get(kAffli));
            	//}
                
            }
        }

        if(!mapTempIdJVCOAffiliationCustRefNum.isEmpty())
        {
            for(String kAffli: mapTempIdJVCOAffiliationCheckBox.keySet())
            {
                //if(transferSelectionsDisplayMap.get(kAffli) != null)
                //{
                    transferSelectionsDisplayMap.get(kAffli).affiliationRecord.JVCO_Customer_Reference_Number__c = String.valueOf(mapTempIdJVCOAffiliationCustRefNum.get(kAffli));
                //}
                
            }
        }
        //End LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values

        if(transferSelectionsDisplayMap.size() > 0) 
        {    
            // START    jules.osberg.a.pablo     19/10/2017      GREEN-24918 Fixed issue with affiliation start dates incorrectly cascading to all affilations
            Integer ctr2 = 0;
            
            for(String tsdmKey : transferSelectionsDisplayMap.keySet()) 
            {
                if(ctr2 < processSize)
                {
                    transferSelectionsCheckerMap.put(tsdmKey, transferSelectionsDisplayMap.remove(tsdmKey));
                    ctr2++;
                }
            }

            //Start LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
            mapTempIdJVCOAffiliationDate.clear();
            if(transferSelectionsDisplayMap.size() > 0) {  
                for(String affKey: transferSelectionsDisplayMap.keySet()){
                    mapTempIdJVCOAffiliationDate.put(affKey,Date.valueOf(transferSelectionsDisplayMap.get(affKey).affiliationRecord.JVCO_Start_Date__c));
                }
            }

            mapTempIdJVCOAffiliationCheckBox.clear();
            if(transferSelectionsDisplayMap.size() > 0) {  
                for(String affKey: transferSelectionsDisplayMap.keySet()){
                    mapTempIdJVCOAffiliationCheckBox.put(affKey,Boolean.valueOf(transferSelectionsDisplayMap.get(affKey).affiliationRecord.JVCO_Omit_Quote_Line_Group__c));
                }
            }

            mapTempIdJVCOAffiliationCustRefNum.clear();
            if(transferSelectionsDisplayMap.size() > 0) {  
                for(String affKey: transferSelectionsDisplayMap.keySet()){
                    mapTempIdJVCOAffiliationCustRefNum.put(affKey,String.valueOf(transferSelectionsDisplayMap.get(affKey).affiliationRecord.JVCO_Customer_Reference_Number__c));
                }
            }
            
             //End LADR | 15/11/2017 | GREEN-25128,GREEN-25739 | Fix for the incorrect values
            JVCO_VenueTransfer venuetransfer = new JVCO_VenueTransfer();
            Boolean retAcquisition =  venuetransfer.confirmAcquisition(accountRecord, isAgency, transferSelectionsCheckerMap, false);
            
            //if(!transferSelectionsToPassMap.isEmpty()) { // Commented jules.osberg.a.pablo     19/10/2017      GREEN-24918
            if(!transferSelectionsCheckerMap.isEmpty()) {
                if(retAcquisition) 
                {
                    if(!transferSelectionsDisplayMap.isEmpty()) 
                    {
                        if(!Test.isRunningTest())
                        {

                            System.enqueueJob(new JVCO_Acquisitions_Queueable(accountRecord, isAgency, mapTempIdJVCOAffiliationDate, mapTempIdJVCOAffiliationCheckBox, mapTempIdJVCOAffiliationCustRefNum, transferSelectionsDisplayMap,  processSize));
                        }
                    } 
                    else 
                    {
                        sendEmailCompletion(accountRecord);
                        accountRecord.JVCO_VenAcquisitionQueueFlag__c = false;
            			update accountRecord;
                    }
                } 
                else 
                {
                    if(!Test.isRunningTest())
                    {
                        System.enqueueJob(new JVCO_Acquisitions_Queueable(accountRecord, isAgency, mapTempIdJVCOAffiliationDate, mapTempIdJVCOAffiliationCheckBox, mapTempIdJVCOAffiliationCustRefNum, transferSelectionsDisplayMap,  processSize));
                    }
                    

                }   
            }
        } 
        else 
        {
            accountRecord.JVCO_VenAcquisitionQueueFlag__c = false;
            update accountRecord;
            sendEmailCompletion(accountRecord);
        }

	}

	public static void sendEmailCompletion(Account accountRecord) 
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setTargetObjectId(userInfo.getUserID());
        mail.setSaveAsActivity(false);

        mail.setReplyTo('no-reply@salesforce.com');
        mail.setSenderDisplayName('Venue Acquisition Queueable Processing');
        mail.setSubject('Venue Acquisition Queueable Completed');

        String emailMsg = '';

        
            emailMsg += '<html><body>This is to notify you that the process to Acquire Venue  for ' + accountRecord.Name + ' (' +System.now() + ') has been completed' ;
        

        emailMsg += ' </body></html> ';

        //End
        
        mail.setHTMLBody(emailMsg);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }


}