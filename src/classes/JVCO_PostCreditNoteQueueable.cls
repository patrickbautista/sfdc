public class JVCO_PostCreditNoteQueueable Implements System.Queueable {
    public Integer batchSize;
    set<Id> creditNoteIds;
    Boolean stopMatching = JVCO_BackgroundMatchingLogic.stopMatching;
    
    //how many queueables to launch in parallel (system limit 50)
    public static Integer defaultQueueableLimit = 50;
    public static Integer defaultBatchSize = 5;
    public static String defaultQuery = 'SELECT Id FROM c2g__codaCreditNote__c WHERE c2g__CreditNoteStatus__c = \'In Progress\' AND ((JVCO_Temp_Link_Positive_Invoice__c != NULL AND c2g__Invoice__c != NULL) OR (JVCO_Temp_Link_Positive_Invoice__c = NULL AND c2g__Invoice__c = NULL)) ORDER BY c2g__Account__c LIMIT 50000';
    
    // method to launch a large number of queueable jobs to process posting of all cash entries
    public static void start() {
        start(defaultQuery,defaultBatchSize,defaultQueueableLimit);
    }

    public static void start(String queryString) {
        start(queryString,defaultBatchSize,defaultQueueableLimit);
    }
    
    public static void start(String queryString, Integer overrideBatchSize) {
        start(queryString,overrideBatchSize,defaultQueueableLimit);
    }
    
    public static void start(Integer overrideBatchSize) {
        start(defaultQuery,overrideBatchSize,defaultQueueableLimit);
    }
    
    public static void start(Integer overrideBatchSize, Integer overrideQueueableLimit) {
        start(defaultQuery,overrideBatchSize,overrideQueueableLimit);
    }
    
    public static void start(String queryString, Integer batchSize, Integer queueableLimit) {
        
        //select all cash entries which have lines which are linked to accounts
        list<c2g__codaCreditNote__c> creditNotes = Database.query(queryString);

        //number of records to push into each queue. round up to ensure all records are processed even if some batches are of un
        Integer recordSplit = (Integer)((Decimal)creditNotes.size()/(Decimal)queueableLimit).round(System.RoundingMode.UP);
        
        //loop to create the correct number of queueable jobs whilst we have records
        for(Integer i =0; i < queueableLimit && !creditNotes.isEmpty(); i++) {
            
            JVCO_PostCreditNoteQueueable pcnq = new JVCO_PostCreditNoteQueueable();
            pcnq.batchSize = batchSize;
            pcnq.creditNoteIds = new set<Id>();
            
            //put an appropriate number of records into the job
            for(Integer j =0; j < recordSplit && !creditNotes.isEmpty(); j++) {
                pcnq.creditNoteIds.add(creditNotes.get(0).Id);
                creditNotes.remove(0);
            }
            
            System.enqueueJob(pcnq); 
        }
    }
    /*
     * Author: Filip Bezak
     * Starts parallel processing of Sales Credit Notes entered as parameter. If matching is disabled only posting is done.
     * parameters: creditNotes - List of Sales Credit Notes that needs to be posted
     */
    public static void start(List<c2g__codaCreditNote__c> creditNotes, Integer overrideQueueableLimit, Integer overrideBatchSize, Boolean stopMatching)
    {
        System.debug('creditNotes in class: ' + creditNotes);
        if(overrideQueueableLimit > 50)
        {
            System.debug('JVCO_PostCreditNoteQueueable.start: overrideQueueableLimit was set to more than 50 so it was set to 50');
            overrideQueueableLimit = 50;
        }
        if(overrideBatchSize > 200)
        {
            System.debug('JVCO_PostCreditNoteQueueable.start: overrideBatchSize was set to more than 200 so it was set to 200');
            overrideBatchSize = 200;
        }
        //number of records to push into each queue. round up to ensure all records are processed even if some batches are of un
        Integer recordSplit = (Integer)((Decimal)creditNotes.size()/(Decimal)overrideQueueableLimit).round(System.RoundingMode.UP);
        
        //loop to create the correct number of queueable jobs whilst we have records
        for(Integer i = 0; i < overrideQueueableLimit && !creditNotes.isEmpty(); i++)
        {
            JVCO_PostCreditNoteQueueable pcnq = new JVCO_PostCreditNoteQueueable();
            pcnq.batchSize = overrideBatchSize;
            pcnq.creditNoteIds = new set<Id>();
            pcnq.stopMatching = stopMatching;
            
            //put an appropriate number of records into the job
            for(Integer j = 0; j < recordSplit && !creditNotes.isEmpty(); j++)
            {
                pcnq.creditNoteIds.add(creditNotes.get(0).Id);
                creditNotes.remove(0);
            }
            System.enqueueJob(pcnq);
        }
    }
    
    //execute posting
    public void execute(QueueableContext ctx) {
        JVCO_BackgroundMatchingLogic.stopMatching = this.stopMatching;
        Integer linesAdded = 0;
        list<c2g__codaCreditNote__c> creditNotesToPost = new list<c2g__codaCreditNote__c>();

        list<c2g__codaCreditNote__c> creditNotes = [SELECT Id, (SELECT Id FROM c2g__CreditNoteLineItems__r)
                                                    FROM c2g__codaCreditNote__c WHERE c2g__CreditNoteStatus__c = 'In Progress' 
                                                    AND Id IN :creditNoteIds ORDER BY c2g__Account__c];
        while(linesAdded < batchSize && !creditNotes.isEmpty()) {
            linesAdded += creditNotes.get(0).c2g__CreditNoteLineItems__r.size();
            creditNotesToPost.add(creditNotes.remove(0));
        }
        //create ff objects
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();

        List<c2g.CODAAPICommon.Reference> referenceList = new List<c2g.CODAAPICommon.Reference>();

        for (c2g__codaCreditNote__c creditNote : creditNotesToPost) {
            // Moved the reference from outside the loop to inside the loop - franz.g.a.dimaapi
            c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = creditNote.Id;
            referenceList.add(reference);
        }

        if(!referenceList.isEmpty()) {
            try {
                //Call Bulk Posting for CreditNote API
                c2g.CODAAPISalesCreditNote_10_0.BulkPostCreditNote(context, referenceList);
            }
            //error logging
            catch (Exception e) {
                list<ffps_custRem__Custom_Log__c> errorLogs = new list<ffps_custRem__Custom_Log__c>();


                for (c2g__codaCreditNote__c creditNote : creditNotesToPost) {

                    ffps_custRem__Custom_Log__c newErr = new ffps_custRem__Custom_Log__c();
                    newErr.ffps_custRem__Running_User__c = UserInfo.getUserId();
                    newErr.ffps_custRem__Related_Object_Key__c= String.valueOf(creditNote.Id);
                    newErr.ffps_custRem__Detail__c = string.valueof(e.getMessage());
                    newErr.ffps_custRem__Message__c = 'Posting of Credit Note';
                    newErr.ffps_custRem__Grouping__c = 'PCQ-001';

                    errorLogs.add(newErr);
                }

                insert errorLogs;
            }
        }
        
        //launch next queueable
        if(!creditNotes.isEmpty()) {
            JVCO_PostCreditNoteQueueable pcnq = new JVCO_PostCreditNoteQueueable();
            pcnq.batchSize = batchSize;
            pcnq.creditNoteIds = new set<Id>();
            pcnq.stopMatching = stopMatching;
            
            for(c2g__codaCreditNote__c creditNote : creditNotes) {
                pcnq.creditNoteIds.add(creditNote.Id);
            }
            if(!Test.isRunningTest())
                System.enqueueJob(pcnq);
        }
    }
}