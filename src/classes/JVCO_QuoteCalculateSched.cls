/* ------------------------------------------------------------------------------------------------
   Name:            JVCO_QuoteCalculateSched
   Description:     Schedule Calculate quote for GREEN-34969

   Date            Version     Author                       Summary of Changes 
   -----------     -------     -----------------            ---------------------------------------
   20-Mar-2020     0.1         rhys.j.c.dela.cruz           Initial Version
------------------------------------------------------------------------------------------------ */
public class JVCO_QuoteCalculateSched implements Schedulable {

    List<SBQQ__Quote__c> quoteList = new List<SBQQ__Quote__c>();

    public JVCO_QuoteCalculateSched(List<SBQQ__Quote__c> quoteList){

        this.quoteList = quoteList;
    }

    public void execute(SchedulableContext sc){

        List<SBQQ__Quote__c> qListForUpdate = new List<SBQQ__Quote__c>();

        for(SBQQ__Quote__c qRec : quoteList){

            SBQQ__Quote__c q = new SBQQ__Quote__c();
            q.Id = qRec.Id;
            //q.Recalculate__c = !qRec.Recalculate__c;
            if(qRec.Recalculate__c){
                q.Recalculate__c = false;
            }
            else{
                q.Recalculate__c = true;
            }
            qListForUpdate.add(q);
        }        

        if(!qListForUpdate.isEmpty()){

            update qListForUpdate;
            System.abortJob(sc.getTriggerId());
        }
    }
}