@isTest
private class JVCO_ReIndexQuoteLineControllerTest
{
	private static Boolean wasMessageAdded(String message) 
	{
        List<ApexPages.Message> pageMessages = ApexPages.getMessages();
        System.assertNotEquals(0, pageMessages.size());

        Boolean messageFound = false;

        for(ApexPages.Message msg : pageMessages) {
            if(msg.getDetail().contains(message)) {
                messageFound = true;        
            }
        }

        return messageFound;
    }

	@testSetup
	static void testSetup()
	{
		Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
		insert acc;

		Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
		acc2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
		insert acc2;

		Account acc3 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
		acc3.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
		acc3.Type = 'Promoter';
		insert acc3;
	}

	@isTest
	static void noQuoteLineTest()
	{
		Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
		insert pb;

		SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
		insert qProcess;

		Account acc2 = [Select Id, Name From Account where RecordType.Name = 'Licence Account' AND Type != 'Promoter'];

		JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
		insert ven;

		Test.startTest();
		JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
		aff.JVCO_End_Date__c = null;
		aff.JVCO_Closure_Reason__c = null;
		insert aff;

		Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
		insert opp;

		SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
		q.Start_Date__c  = date.today();
		q.End_Date__c  = date.today().addDays(180);
		q.SBQQ__EndDate__c = date.today().addDays(180);
		q.SBQQ__SubscriptionTerm__c  = 12;
		q.Recalculate__c = true;
		q.SBQQ__Status__c  = 'Draft';
		q.JVCO_Contact_Type__c = 'Email';
		q.Accept_TCs__c = null;
		insert q;

		SBQQ__Quote__c qRec = [select Id, Name, SBQQ__NetAmount__c, JVCO_Contact_Type__c, Recalculate__c, SBQQ__Opportunity2__c, JVCO_QuoteComplete__c, SBQQ__Status__c from SBQQ__Quote__c limit 1];

		Test.setCurrentPage(Page.JVCO_ReIndexQuoteLine);
		ApexPages.StandardController standardController = new ApexPages.StandardController(qRec);
        JVCO_ReIndexQuoteLineController controller = new JVCO_ReIndexQuoteLineController(standardController);
        Test.stopTest();

        System.assert(wasMessageAdded('No available Quote Line records for '));

        controller.returnToQuote();

	}

	@isTest
	static void oneQuoteLineTest()
	{
		Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
		insert pb;

		SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
		insert qProcess;

		Account acc2 = [Select Id, Name From Account where RecordType.Name = 'Licence Account' AND Type != 'Promoter'];

		JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
		insert ven;

		Test.startTest();
		JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
		aff.JVCO_End_Date__c = null;
		aff.JVCO_Closure_Reason__c = null;
		insert aff;

		Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
		insert opp;

		SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
		q.Start_Date__c  = date.today();
		q.End_Date__c  = date.today().addDays(180);
		q.SBQQ__EndDate__c = date.today().addDays(180);
		q.SBQQ__SubscriptionTerm__c  = 12;
		q.Recalculate__c = true;
		q.SBQQ__Status__c  = 'Draft';
		q.JVCO_Contact_Type__c = 'Email';
		q.Accept_TCs__c = null;
		insert q;

		SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];

		Product2 proRecPRS = JVCO_TestClassObjectBuilder.createProduct();
        proRecPRS.JVCO_Society__c = 'PRS';
        insert proRecPRS;

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, proRecPRS.Id);
		ql.SBQQ__StartDate__c = q.Start_Date__c;
		ql.SBQQ__EndDate__c = q.End_Date__c;
		ql.Start_Date__c = q.Start_Date__c;
		ql.End_Date__c = q.End_Date__c;
		ql.SPVAvailability__c = true;
		insert ql;

		SBQQ__Quote__c qRec = [select Id, Name, SBQQ__NetAmount__c, JVCO_Contact_Type__c, Recalculate__c, SBQQ__Opportunity2__c, JVCO_QuoteComplete__c, SBQQ__Status__c from SBQQ__Quote__c limit 1];

		Test.setCurrentPage(Page.JVCO_ReIndexQuoteLine);
		ApexPages.StandardController standardController = new ApexPages.StandardController(qRec);
        JVCO_ReIndexQuoteLineController controller = new JVCO_ReIndexQuoteLineController(standardController);
        Test.stopTest();

        System.assert(wasMessageAdded('Only 1 Quote Line record available for '));

        controller.returnToQuote();

	}

	@isTest
	static void nonPromoterQuote()
	{
		Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
		insert pb;

		SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
		insert qProcess;

		Account acc2 = [Select Id, Name From Account where RecordType.Name = 'Licence Account' AND Type != 'Promoter'];

		JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
		insert ven;

		Test.startTest();
		JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
		aff.JVCO_End_Date__c = null;
		aff.JVCO_Closure_Reason__c = null;
		insert aff;

		Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
		insert opp;

		SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
		q.Start_Date__c  = date.today();
		q.End_Date__c  = date.today().addDays(180);
		q.SBQQ__EndDate__c = date.today().addDays(180);
		q.SBQQ__SubscriptionTerm__c  = 12;
		q.Recalculate__c = true;
		q.SBQQ__Status__c  = 'Draft';
		q.JVCO_Contact_Type__c = 'Email';
		q.Accept_TCs__c = null;
		insert q;

		SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];

		//
		List<Product2> prodList = new List<Product2>();

		Product2 proRecPRS = JVCO_TestClassObjectBuilder.createProduct();
        proRecPRS.JVCO_Society__c = 'PRS';
        prodList.add(proRecPRS);

        //
        Product2 proRecPPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecPPL.JVCO_Society__c = 'PPL';
        prodList.add(proRecPPL);
        //
        Product2 proRecVPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecVPL.JVCO_Society__c = 'VPL';
        prodList.add(proRecVPL);
        insert prodList;

        //
        List<SBQQ__QuoteLine__c> qlList = new List<SBQQ__QuoteLine__c>();

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, proRecPRS.Id);
		ql.SBQQ__StartDate__c = q.Start_Date__c;
		ql.SBQQ__EndDate__c = q.End_Date__c;
		ql.Start_Date__c = q.Start_Date__c;
		ql.End_Date__c = q.End_Date__c;
		ql.SPVAvailability__c = true;
		//insert ql;
		qlList.add(ql);

		SBQQ__QuoteLine__c ql2 = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, proRecPRS.Id);
		ql2.SBQQ__StartDate__c = q.Start_Date__c;
		ql2.SBQQ__EndDate__c = q.End_Date__c;
		ql2.Start_Date__c = q.Start_Date__c;
		ql2.End_Date__c = q.End_Date__c;
		ql2.SPVAvailability__c = true;
		qlList.add(ql2);

		insert qlList;

		//
		SBQQ__QuoteLine__c ql3 = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, proRecPRS.Id);
		ql3.SBQQ__StartDate__c = q.Start_Date__c;
		ql3.SBQQ__EndDate__c = q.End_Date__c;
		ql3.Start_Date__c = q.Start_Date__c;
		ql3.End_Date__c = q.End_Date__c;
		ql3.SPVAvailability__c = true;
		ql3.SBQQ__RequiredBy__c = ql.Id;
		insert ql3;

		SBQQ__Quote__c qRec = [select Id, Name, SBQQ__NetAmount__c, JVCO_Contact_Type__c, Recalculate__c, SBQQ__Opportunity2__c, JVCO_QuoteComplete__c, SBQQ__Status__c from SBQQ__Quote__c limit 1];

		Test.setCurrentPage(Page.JVCO_ReIndexQuoteLine);
		ApexPages.StandardController standardController = new ApexPages.StandardController(qRec);
        JVCO_ReIndexQuoteLineController controller = new JVCO_ReIndexQuoteLineController(standardController);
        controller.reIndex();
        Test.stopTest();
	}

	@isTest
	static void promoterQuote()
	{
		Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
		insert pb;

		SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
		insert qProcess;

		Account acc2 = [Select Id, Name From Account where RecordType.Name = 'Licence Account' AND Type != 'Promoter'];

		JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
		insert ven;

		Test.startTest();
		JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
		aff.JVCO_End_Date__c = null;
		aff.JVCO_Closure_Reason__c = null;
		insert aff;

		Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
		insert opp;

		SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
		q.Start_Date__c  = date.today();
		q.End_Date__c  = date.today().addDays(180);
		q.SBQQ__EndDate__c = date.today().addDays(180);
		q.SBQQ__SubscriptionTerm__c  = 12;
		q.Recalculate__c = true;
		q.SBQQ__Status__c  = 'Draft';
		q.JVCO_Contact_Type__c = 'Email';
		q.Accept_TCs__c = null;
		insert q;

		SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];

		//
		List<Product2> prodList = new List<Product2>();

		Product2 proRecPRS = JVCO_TestClassObjectBuilder.createProduct();
        proRecPRS.JVCO_Society__c = 'PRS';
        prodList.add(proRecPRS);

        //
        Product2 proRecPPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecPPL.JVCO_Society__c = 'PPL';
        prodList.add(proRecPPL);
        //
        Product2 proRecVPL = JVCO_TestClassObjectBuilder.createProduct();
        proRecVPL.JVCO_Society__c = 'VPL';
        prodList.add(proRecVPL);
        insert prodList;

        //
        List<SBQQ__QuoteLine__c> qlList = new List<SBQQ__QuoteLine__c>();

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, proRecPRS.Id);
		ql.SBQQ__StartDate__c = q.Start_Date__c;
		ql.SBQQ__EndDate__c = q.End_Date__c;
		ql.Start_Date__c = q.Start_Date__c;
		ql.End_Date__c = q.End_Date__c;
		ql.SPVAvailability__c = true;
		//insert ql;
		qlList.add(ql);

		SBQQ__QuoteLine__c ql2 = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, proRecPRS.Id);
		ql2.SBQQ__StartDate__c = q.Start_Date__c;
		ql2.SBQQ__EndDate__c = q.End_Date__c;
		ql2.Start_Date__c = q.Start_Date__c;
		ql2.End_Date__c = q.End_Date__c;
		ql2.SPVAvailability__c = true;
		qlList.add(ql2);

		insert qlList;

		//
		SBQQ__QuoteLine__c ql3 = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, proRecPRS.Id);
		ql3.SBQQ__StartDate__c = q.Start_Date__c;
		ql3.SBQQ__EndDate__c = q.End_Date__c;
		ql3.Start_Date__c = q.Start_Date__c;
		ql3.End_Date__c = q.End_Date__c;
		ql3.SPVAvailability__c = true;
		ql3.SBQQ__RequiredBy__c = ql.Id;
		insert ql3;

		SBQQ__Quote__c qRec = [select Id, Name, SBQQ__NetAmount__c, JVCO_Contact_Type__c, Recalculate__c, SBQQ__Opportunity2__c, JVCO_QuoteComplete__c, SBQQ__Status__c from SBQQ__Quote__c limit 1];

		Test.setCurrentPage(Page.JVCO_ReIndexQuoteLine);
		ApexPages.StandardController standardController = new ApexPages.StandardController(qRec);
        JVCO_ReIndexQuoteLineController controller = new JVCO_ReIndexQuoteLineController(standardController);
        controller.reIndex();
        Test.stopTest();
	}
}