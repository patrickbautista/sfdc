/* ----------------------------------------------------------------------------------------------
Name: JVCO_LinkOriginatorBankAccount.cls 
Description: Batch class that handles Bank Accounts update
             the account reference             
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
12-Apr-2017  0.1         kristoffer.d.martin   Intial creation
03-Nov-2017  0.2         desiree.m.quijada     Added filter criteria to query records with 
                                               PAYFISH3__Originator_Bank_Account__c with null values
----------------------------------------------------------------------------------------------- */
global class JVCO_LinkOriginatorBankAccount implements Database.Batchable<sObject> {
    
    private static Map<String, ID> subscriptionIdMap;
    private static List<PAYBASE2__Payment__c> paymentListForUpdate;
    private static List<PAYBASE2__Payment__c> paymentList;

    private static void init(){

        PAYACCVAL1__Bank_Account__c originatorBank = [SELECT Id, Name, PAYFISH3__Originator__c
                                                      FROM PAYACCVAL1__Bank_Account__c
                                                      WHERE PAYFISH3__Originator__c = true
                                                      LIMIT 1];

        paymentListForUpdate = new List<PAYBASE2__Payment__c>();

        for(PAYBASE2__Payment__c payment : paymentList)
        {
            if (originatorBank != null)
            {
                payment.PAYFISH3__Originator_Bank_Account__c = originatorBank.Id;
                paymentListForUpdate.add(payment);
            }
        }
        update paymentListForUpdate;
        
    }

    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id, Name, PAYFISH3__Payee_Bank_Account__c, PAYFISH3__Originator_Bank_Account__c,
                                                PAYREC2__Payment_Agreement__c, PAYREC2__Payment_Agreement__r.PAYFISH3__Current_Bank_Account__c
                                         FROM PAYBASE2__Payment__c
                                         //11-03-17 -- add filter criteria
                                         WHERE (CreatedBy.Name = 'Data Migration' OR CreatedBy.ID = :UserInfo.getUserID())
                                         AND PAYFISH3__Originator_Bank_Account__c = null]);

    }

    global void execute(Database.BatchableContext bc, List<PAYBASE2__Payment__c> scope)
    {
        paymentList = scope;
        init();
                
    }

    global void finish(Database.BatchableContext BC) 
    {
        
        System.debug('DONE');
    
    }
    
}