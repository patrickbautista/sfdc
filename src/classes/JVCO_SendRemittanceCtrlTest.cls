/**
* Created by: Accenture.
* Created Date: 16.10.2017
* Description: Test class for JVCO_SendRemittanceCtrl class
*
**/
@isTest
public class JVCO_SendRemittanceCtrlTest
{
   /**
    *  Test method to test JVCO_SendRemittanceCtrl  controller 
    *
    */
    @isTest
    static void testController()
    {
        List<c2g__codaPayment__c> paymentLst = new list<c2g__codaPayment__c>();    
        paymentLst.add(new c2g__codaPayment__c());   
        Test.startTest();
        ApexPages.StandardSetController stdCtrl = new ApexPages.StandardSetController(paymentLst );
        JVCO_SendRemittanceCtrl ctrlObj = new JVCO_SendRemittanceCtrl(stdCtrl);
        ctrlObj.processSendRemittance();
        stdCtrl.setSelected(paymentLst);
        ctrlObj.processSendRemittance();
        Test.stopTest();
    }
}