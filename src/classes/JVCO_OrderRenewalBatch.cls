/* ----------------------------------------------------------------------------------------------
   Name: JVCO_OrderRenewalBatch.cls 
   Description: Batch class that handles Order generation of renewed Quotes

   Date         Version     Author                              Summary of Changes 
   -----------  -------     -----------------               -----------------------------------------
  26-Mar-2017   0.1         ryan.i.r.limlingan              Intial creation
  27-JUl-2017   0.2         mel.andrei.b.santos             Changed DML statement to database.update as per Ticket GREEN-17932
  04-Aug-2017   0.3         mel.andrei.b.santos             Update the error logs for the database.update
  24-Aug-2017   0.4         mel.andrei.b.santos             refactored inputting of fields
  25-Aug-2017   0.5         ryan.i.r.limlingan              Changed query as described in GREEN-22244;
                                                            Also removed unnecessary Opportunity field updates
  14-Sep-2017   0.6         mel.andrei.b.Santos              removed the if conditions as the null check in the error logs as per James
  30-Jan-2018   0.7         mel.andrei.b.Santos             added JVCO_OpportunityCancelled__c as a condition for query as per GREEN-29724
  24-Apr-2019   0.8         rhys.j.c.dela.cruz              GREEN-34512 - Check if order is generated then revert ordered field if no order
  26-Jun-2019   0.9         rhys.j.c.dela.cruz              GREEN-34698 - Adjusted handling for Opportunity that did not create an Order
  27-Aug-2019   0.9         mel.andrei.b.santos             GREEN-34776 - Updated Query to filter Auto Renewed = TRUE from Opportunity instead of from Primary Quote
----------------------------------------------------------------------------------------------- */
global class JVCO_OrderRenewalBatch implements Database.Batchable<sObject> {

    global JVCO_OrderRenewalBatch() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([SELECT Id, SBQQ__Ordered__c FROM Opportunity
                                         WHERE SBQQ__Contracted__c = TRUE
                                         AND SBQQ__Ordered__c = FALSE
                                         AND JVCO_Auto_Renewed__c = TRUE
                                         AND JVCO_OpportunityCancelled__c = FALSE]);
    }

    global void execute(Database.BatchableContext BC, List<Opportunity> oppToUpdateList )
    {
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>(); // mel.andrei.b.santos 27-07-2017 GREEN-17932
    
        //GREEN-31215 - Get custom settings to be the maximum number of tries in while loop
        Integer MaxOrderRetries;
        MaxOrderRetries = (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_MaxOrderRetries__c != null ? 
                    (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_MaxOrderRetries__c : 3;
        //Number of increments or tries used in while loop
        Integer j = 0;

        Set<Id> oppIdSet = new Set<Id>();

        for (Opportunity opp : oppToUpdateList)
        {
            /** ryan.i.r.limlingan 25-08-2017 GREEN-22244 
             *  No longer needed, already updated in CompleteRenewal batch
            opp.StageName = 'Closed Won';
            opp.Probability = 100;
            opp.SBQQ__Contracted__c = TRUE;
            */
            opp.SBQQ__Ordered__c = TRUE;
            oppIdSet.add(opp.Id);
        }
        //GREEN-31215 - retry to save any failures
    do 
        { 
            
            if (!oppToUpdateList.isEmpty())
            {
                System.debug('oppToUpdateList: ' + oppToUpdateList);
                //update oppToUpdateList; start Changed DML statement to database.update as per Ticket GREEN-17932 - Mel Andrei Santos 27-07-2017
                List<Database.SaveResult> res = Database.update(oppToUpdateList,false);  // Start 24-08-2017 mel.andrei.b.santos
                for(Integer i = 0; i < oppToUpdateList.size(); i++)
                {
                    Database.SaveResult srOppList = res[i];
                    Opportunity origrecord = oppToUpdateList[i];
                    if(!srOppList.isSuccess())
                    {
                        System.debug('Update opportunity fail: ');
                        for(Database.Error objErr:srOppList.getErrors())
                        {
                            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c(); 
    
                            errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                            errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                            errLog.ffps_custRem__Message__c = 'Order Renewal';     
                            errLog.ffps_custRem__Related_Object_Key__c = string.valueof(origrecord.ID);
                            errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                            //errLog.Name = string.valueof(origrecord.ID);
                            errLog.JVCO_Number_of_Tries__c = j + 1;
                            errLogList.add(errLog);
       
                        }
                    }
                }
                System.debug('@@@Error Log List of quote: ' + errLogList);
                if(!errLogList.isempty())
                {
                    System.debug('@@@Error Log List insert of quote?: ' + errLogList);
                    // insert errLogList; Start 22-08-2017 mel.andrei.b.santos  Added try catch method for error handling in inserting error log records
                    try
                    {
                        insert errLogList;
                    }
                    catch(Exception ex)
                    {
                        System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                        
                    }//end
                }  
                    //end
                j++;
            }
        }
        //The loop is equal to custom settings JVCO_MaxOrderRetries__c and it varies to the size of errorloglist
        while(errLogList.size() > 0 && j < MaxOrderRetries);

        List<SBQQ__Quote__c> quoteList = [SELECT Id, SBQQ__Opportunity2__c, (SELECT Id FROM SBQQ__Orders__r) FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c IN: oppIdSet AND SBQQ__Primary__c = true];

        List<Opportunity> oppListForUpdate = new List<Opportunity>();        

        for(SBQQ__Quote__c qRec : quoteList){

            if(qRec.SBQQ__Orders__r.isEmpty()){

                Opportunity opp = new Opportunity();
                opp.Id = qRec.SBQQ__Opportunity2__c;
                opp.SBQQ__Ordered__c = false;
                oppListForUpdate.add(opp);
            }
        }

        if(!oppListForUpdate.isEmpty()){
            update oppListForUpdate;
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
        //GREEN-35667
        JVCO_LicAccGenerateAmendmentBatch genLicAccAmendmentsBatch = new JVCO_LicAccGenerateAmendmentBatch();
        Database.executeBatch(genLicAccAmendmentsBatch, 1);
    }
    
}