@isTest
private class JVCO_codaBankStmtLineItemHandlerTest2
{
    
/* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Sets up the needed records for testing
    Inputs: N/
    Returns: voidA
    <Date>      <Authors Name>       <Brief Description of Change> 
    07-Feb-2017 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    @testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId(), Email = 'test@gmail.com'))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        insert glaList;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        List<c2g__codaDimension2__c> dim2List = new List<c2g__codaDimension2__c>();
        c2g__codaDimension2__c pplDim2 = JVCO_TestClassHelper.setDim2('PPL');
        dim2List.add(pplDim2);
        c2g__codaDimension2__c prsDim2 = JVCO_TestClassHelper.setDim2('PRS');
        dim2List.add(prsDim2);
        c2g__codaDimension2__c vplDim2 = JVCO_TestClassHelper.setDim2('VPL');
        dim2List.add(vplDim2);
        insert dim2List;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();
        insert dim3;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        custAcc.Name=Label.JVCO_Account_Unidentified;
        update custAcc;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Test.startTest();
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        insert JVCO_TestClassHelper.setQuoteProcess();
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        Test.stopTest();

        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        JVCO_TestClassHelper.setGeneralSettingsCS();
        JVCO_TestClassHelper.setCashMatchingCS();
        
        
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        insert sInv;
        List<c2g__codaInvoiceLineItem__c> sInvLineList = new List<c2g__codaInvoiceLineItem__c>();
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, pplDim2.Id, p.Id));
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, prsDim2.Id, p.Id));
        sInvLineList.add(JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, vplDim2.Id, p.Id));
        insert sInvLineList;
        Set<Id> sInvIdSet = new Set<Id>();
        sInvIdSet.add(sInv.Id);

        //Run Post Batch
        JVCO_PostSalesInvoiceBatch pSIB = new JVCO_PostSalesInvoiceBatch(sInvIdSet, new Set<Id>());
        psIB.stopMatching = true;
        Database.executeBatch(pSIB);
        
        c2g__codaBankAccount__c testBankAccount = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.id, accReceivableGLA.id);
        insert testBankAccount;
    }

/* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Test JVCO_codaBankStatementLineItemHandler.JVCO_codaBankStatementLineItemAllocation
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>       <Brief Description of Change> 
    16-Dec-2016 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    @isTest static void codaBankStatementLineItemAllocationTest()
    {
        c2g__codaInvoice__c testInvoice = [SELECT Id, Name, c2g__Account__c, c2g__OwnerCompany__c,
                                           c2g__Account__r.AccountNumber
                                           FROM c2g__codaInvoice__c
                                           LIMIT 1];

        c2g__codaBankAccount__c testBankAccount = [SELECT Id, Name, c2g__AccountName__c
                                                   FROM c2g__codaBankAccount__c
                                                   LIMIT 1];
        
        Test.startTest();
        //Test match account
        c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem bankStatementLine = new c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem();
        bankStatementLine.DateValue = System.today();
        bankStatementLine.Amount = 1000;
        bankStatementLine.Reference = testInvoice.c2g__Account__r.AccountNumber;
        bankStatementLine.OwnerCompany = c2g.CODAAPICommon.Reference.getRef(testInvoice.c2g__OwnerCompany__c, null);
        
        //Test match account and sales invoice
        c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem bankStatementLine2 = new c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem();
        bankStatementLine2.DateValue = System.today();
        bankStatementLine2.Amount = 1000;
        bankStatementLine2.Reference = testInvoice.c2g__Account__r.AccountNumber + testInvoice.Name;
        bankStatementLine2.OwnerCompany = c2g.CODAAPICommon.Reference.getRef(testInvoice.c2g__OwnerCompany__c, null);
        
        //Test no match account and match sales invoice
        c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem bankStatementLine3 = new c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem();
        bankStatementLine3.DateValue = System.today();
        bankStatementLine3.Amount = 1000;
        bankStatementLine3.Reference = '12344561'+ testInvoice.Name;
        bankStatementLine3.OwnerCompany = c2g.CODAAPICommon.Reference.getRef(testInvoice.c2g__OwnerCompany__c, null);
        
        //Test no match account/sales invoice
        c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem bankStatementLine4 = new c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem();
        bankStatementLine4.DateValue = System.today();
        bankStatementLine4.Amount = 1000;
        bankStatementLine4.Reference = '12344561';
        bankStatementLine4.OwnerCompany = c2g.CODAAPICommon.Reference.getRef(testInvoice.c2g__OwnerCompany__c, null);
        
        //Test only sales invoice
        c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem bankStatementLine5 = new c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem();
        bankStatementLine5.DateValue = System.today();
        bankStatementLine5.Amount = 1000;
        bankStatementLine5.Reference = testInvoice.Name;
        bankStatementLine5.OwnerCompany = c2g.CODAAPICommon.Reference.getRef(testInvoice.c2g__OwnerCompany__c, null);
        
        //Test salesinvoice and exclude it for cash entry generation
        c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem bankStatementLine6 = new c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem();
        bankStatementLine6.DateValue = System.today();
        bankStatementLine6.Amount = 1000;
        bankStatementLine6.Reference = testInvoice.Name + '``true';
        bankStatementLine6.OwnerCompany = c2g.CODAAPICommon.Reference.getRef(testInvoice.c2g__OwnerCompany__c, null);
        
        List<c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem> bankStatementLineItemList = new List<c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItem>();
        bankStatementLineItemList.add(bankStatementLine);
        bankStatementLineItemList.add(bankStatementLine2);
        bankStatementLineItemList.add(bankStatementLine3); 
        bankStatementLineItemList.add(bankStatementLine4);
        bankStatementLineItemList.add(bankStatementLine5);
        bankStatementLineItemList.add(bankStatementLine6);
        
        c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItems bankStatementLineItems = new c2g.CODAAPIBankStatementLineItemTypes_6_0.BankStatementLineItems();
        bankStatementLineItems.LineItemList = bankStatementLineItemList;
        
        c2g.CODAAPIBankStatementTypes_6_0.BankStatement bankStatement = new c2g.CODAAPIBankStatementTypes_6_0.BankStatement();
        bankStatement.BankAccount = c2g.CODAAPICommon.Reference.getRef(testBankAccount.Id, null);
        bankStatement.OwnerCompany = c2g.CODAAPICommon.Reference.getRef(testInvoice.c2g__OwnerCompany__c, null);
        bankStatement.Reference = 'test0001';
        bankStatement.OpeningBalance = 10100;
        bankStatement.StatementDate = System.today();
        bankStatement.LineItems = bankStatementLineItems;
        c2g.CODAAPIBankStatement_6_0.CreateBankStatement(new c2g.CODAAPICommon_6_0.Context(), bankStatement);
        Test.stopTest();
    }
}