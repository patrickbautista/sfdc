@isTest
private class JVCO_FFUtilTest
{
	@isTest
	static void testGetTaxRegistrationNumber()
	{

		Account a = JVCO_TestClassObjectBuilder.createCustomerAccount();
		a.Name = 'PRS';
		a.c2g__CODAVATRegistrationNumber__c = '1234567890';
		insert a;
		JVCO_FFUtil.constructSupplierRecordTaxRegistrationNumber();

		Test.startTest();
		List<blng__InvoiceLine__c> bInvLineList1 = new List<blng__InvoiceLine__c>();
		blng__InvoiceLine__c bInvLine1 = new blng__InvoiceLine__c();
		bInvLine1.Dimension_2s__c = 'PRS';
		bInvLine1.recalculateFormulas();
		bInvLineList1.add(bInvLine1);
		JVCO_FFUtil.getTaxRegistrationNumber(bInvLineList1);

		blng__InvoiceLine__c bInvLine1a = new blng__InvoiceLine__c();
		bInvLine1a.Dimension_2s__c = 'PPL';
		bInvLine1a.recalculateFormulas();
		bInvLineList1.add(bInvLine1a);
		JVCO_FFUtil.getTaxRegistrationNumber(bInvLineList1);

		List<blng__InvoiceLine__c> bInvLineList2 = new List<blng__InvoiceLine__c>();
		blng__InvoiceLine__c bInvLine2 = new blng__InvoiceLine__c();
		bInvLine2.Dimension_2s__c = 'PPL';
		bInvLine2.recalculateFormulas();
		bInvLineList2.add(bInvLine2);
		JVCO_FFUtil.getTaxRegistrationNumber(bInvLineList2);

		List<blng__InvoiceLine__c> bInvLineList3 = new List<blng__InvoiceLine__c>();
		blng__InvoiceLine__c bInvLine3 = new blng__InvoiceLine__c();
		bInvLine3.Dimension_2s__c = 'VPL';
		bInvLine3.recalculateFormulas();
		bInvLineList3.add(bInvLine3);
		JVCO_FFUtil.getTaxRegistrationNumber(bInvLineList3);

		List<blng__InvoiceLine__c> bInvLineList4 = new List<blng__InvoiceLine__c>();
		blng__InvoiceLine__c bInvLine4 = new blng__InvoiceLine__c();
		bInvLine4.Dimension_2s__c = 'HOT';
		bInvLine4.recalculateFormulas();
		bInvLineList4.add(bInvLine4);
		JVCO_FFUtil.getTaxRegistrationNumber(bInvLineList4);

		JVCO_FFUtil.getTaxPRSRegNum();
		Test.stopTest();
	}

	@isTest
	static void testGetOpenPeriodId()
	{
		JVCO_FFUtil.getOpenPeriodId();
	}

	@isTest
	static void testUpdateSalesInvoiceRecordType()
	{
		List<c2g__codaTransactionLineItem__c> transactionLineList = new List<c2g__codaTransactionLineItem__c>();
		c2g__codaTransactionLineItem__c newTransLine = new c2g__codaTransactionLineItem__c();
		newTransLine.c2g__LineType__c = 'Account';
		newTransLine.c2g__AccountValue__c = 500;
		newTransLine.c2g__AccountOutstandingValue__c = 0;
		transactionLineList.add(newTransLine);
		c2g__codaTransactionLineItem__c oldTransLine = new c2g__codaTransactionLineItem__c();
		oldTransLine.c2g__LineType__c = 'Account';
		oldTransLine.c2g__AccountValue__c = 500;
		oldTransLine.c2g__AccountOutstandingValue__c = 500;

		Map<Id, c2g__codaTransactionLineItem__c> oldMap = new Map<Id, c2g__codaTransactionLineItem__c>();
		oldMap.put(null, oldTransLine);

		try
		{
			JVCO_FFUtil.updateSalesInvoiceRecordType(transactionLineList, oldMap);
		}catch(Exception e)
		{
			System.assert(true);
		}
	}
}