/* ----------------------------------------------------------------------------------------------
    Name: JVCO_ErrorUtil.cls 
    Description: Logic class for Error Log Creation

    Date          Version      Author                      Summary of Changes 
    -----------   -------      -----------------           -----------------------------------------
    21-Sep-2017   0.1          eu.rey.t.cadag              Intial creation
    07-May-2018   0.2          franz.g.a.dimaapi           Create logErrorFromOrderGroup method
    14-May-2018   0.3          franz.g.a.dimaapi           Create logErrorFromCashEntry method
----------------------------------------------------------------------------------------------- */
public with sharing class JVCO_ErrorUtil 
{
    public static ffps_custRem__Custom_Log__c logError(Id RecId, String ErrMessage, String ErrStatus, String ProcessName)
    {
        
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
        errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(RecId);
        errLog.ffps_custRem__Detail__c = ErrMessage;
        errLog.ffps_custRem__Message__c = ProcessName;
        errLog.ffps_custRem__Grouping__c = ErrStatus;
        
        return errLog;
    }
    
    public void logErrorFromOrderGroup(List<JVCO_Order_Group__c> orderGroupList, String batchName, String errCode, String errMessage)
    {
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        List<Order> orderList = [SELECT Id
                                 FROM Order 
                                 WHERE JVCO_Order_Group__c IN :orderGroupList];
        for(Order o : orderList)
        {
            //Error Log
            ffps_custRem__Custom_Log__c errLog = createErrorLog(o.Id, batchName, errCode, errMessage);
            errLog.JVCO_Order__c = o.Id;
            errLogList.add(errLog);
            //Remove Tagged for Billing
            o.JVCO_TaggedForBilling__c = false;
        }

        insert errLogList;
        update orderList;
    }

    public void logErrorFromCashEntry(List<c2g__codaCashEntry__c> cashEntryList, String batchName, String errCode, String errMessage)
    {
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        for(c2g__codaCashEntry__c c : cashEntryList)
        {
            //Error Log
            ffps_custRem__Custom_Log__c errLog = createErrorLog(c.Id, batchName, errCode, errMessage);
            errLog.JVCO_CashEntry__c = c.Id;
            errLogList.add(errLog);
            //Remove Tagged for Posted
            c.JVCO_Posted__c = false;
        }

        insert errLogList;
        update cashEntryList;
    }

    private ffps_custRem__Custom_Log__c createErrorLog(Id refId, String batchName, String errCode, String errMessage)
    {
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
        errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(refId);
        errLog.ffps_custRem__Detail__c = errMessage;
        errLog.ffps_custRem__Message__c = batchName;
        errLog.ffps_custRem__Grouping__c = errCode;
        
        return errLog;
    }
    
    public void logErrorFromDunningBatch(Set<Account> accountList, String batchName, String errCode, String errMessage)
    {
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        for(Account acct : accountList)
        {
            //Error Log
            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
            errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
            errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(acct.ID);
            errLog.ffps_custRem__Detail__c = errMessage;
            errLog.ffps_custRem__Message__c = batchName;
            errLogList.add(errLog);
        }
        insert errLogList;
    }

    public void logErrorFromWebPayments(String inputDetails, String errorMessage)
    {
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
        errLog.ffps_custRem__Detail__c = inputDetails;
        errLog.ffps_custRem__Message__c = errorMessage;

        insert errLog;

    }

}