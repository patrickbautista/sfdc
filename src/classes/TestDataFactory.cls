@isTest
public class TestDataFactory {

  public enum AccountRecordType {
    CUSTOMER,
    CUSTOMER_ACCOUNT,
    LICENCE_ACCOUNT,
    SUPPLIER
  }

  public enum CustomerAccountType {
    AGENCY,
    ACCOUNT_MANAGER,
    CUSTOMER,
    KEY_ACCOUNT
  }

  public static List<Account> createLicenceAccounts(Integer numberOfAccounts, Id customerAccountId) {
    List<Account> accountsList = new List<Account>();
    for (Integer i = 0; i < numberOfAccounts; i++) {
      accountsList.add(getInstantiatedLicenceAccount('Licence Account ' + i, customerAccountId));
    }
    insert accountsList;

    return accountsList;
  }

  public static List<Account> createCustomerAccounts(Integer numberOfAccounts, CustomerAccountType cat) {
    List<Account> accountsList = new List<Account>();
    for (Integer i = 0; i < numberOfAccounts; i++) {
      accountsList.add(getInstantiatedCustomerAccount(cat, 'Customer Account ' + i));
    }
    insert accountsList;

    return accountsList;
  }

  public static List<JVCO_Venue__c> createVenues(Integer numberOfVenues) {
    List<JVCO_Venue__c> tmp = new List<JVCO_Venue__c>();
    for (Integer i = 0; i < numberOfVenues; i++) {
      tmp.add(getInstantiatedVenue('Venue ' + i));
    }
    insert tmp;

    return tmp;
  }

  public static List<Account> createLicenceAccountsWithVenues(Id keyaccountId, Integer numberOfLicenceAccounts, Integer numberOfVenues) {

    List<Account> licenceAccountsList = new List<Account>();
    licenceAccountsList.addAll(createLicenceAccounts(numberOfLicenceAccounts, keyaccountId));

    List<JVCO_Venue__c> venuesList = new List<JVCO_Venue__c>();
    for (Integer i = 0; i < licenceAccountsList.size(); i++) {
      for (Integer j = 0; j < numberOfVenues; j++) {
        venuesList.add(getInstantiatedVenue('Venue ' + j));
      }
    }

    insert venuesList;

    List<JVCO_Affiliation__c> affiliationsList = new List<JVCO_Affiliation__c>();
    for (Integer i = 0; i < licenceAccountsList.size(); i++) {
      for (Integer j = 0; j < numberOfVenues; j++) {
        affiliationsList.add(getInstantiatedAffiliation(licenceAccountsList.get(i).Id, venuesList.get(i).Id));
      }
    }

    insert affiliationsList;

    return licenceAccountsList;

  }

  public static List<Account> createKeyAcctsWithLicenceAcctsWithVenues(Integer numberOfKeyAccounts, Integer numberOfLicenceAccounts, Integer numberOfVenues) {
    
    List<Account> keyAccountsList = createCustomerAccounts(numberOfKeyAccounts, CustomerAccountType.KEY_ACCOUNT);
    
    for (Account ka : keyAccountsList) {
      createLicenceAccountsWithVenues(ka.Id, numberOfLicenceAccounts, numberOfVenues);
    }

    return keyAccountsList;
  }

  private static Account getInstantiatedLicenceAccount(String name, Id customerAccountId) {
    Account a = new Account();
    a.Name = name;
    a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    a.JVCO_Customer_Account__c = customerAccountId;
    a.SCMC__Preferred_Communication__c = 'Print';
    a.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
    a.BillingCity = 'BALLINGRY';
    a.BillingStreet = '40 Ballifeary Road';
    a.BillingPostalCode = 'KY5 4BT';
    a.BillingCountry = 'United Kingdom';

    Contact c = new Contact();
    c.LastName = name;
    c.AccountId = customerAccountId;
    c.MailingCity = 'BALLINGRY';
    c.MailingStreet = '40 Ballifeary Road';
    c.MailingPostalCode = 'KY5 4BT';
    c.MailingCountry = 'United Kingdom';
    c.Email = 'accountTest@testClass.com';
    Insert c;
    
    a.JVCO_Billing_Contact__c = c.Id;
    a.JVCO_Review_Contact__c = c.Id;
    a.JVCO_Licence_Holder__c = c.Id;

    c2g__codaGeneralLedgerAccount__c g = new c2g__codaGeneralLedgerAccount__c();
    g.Name = '10390 - VAT Output - Licensing TEST';
    g.c2g__ReportingCode__c = '10390TEST';
    g.c2g__Type__c = 'Balance Sheet';

    Insert g;

    c2g__codaTaxCode__c t = new c2g__codaTaxCode__c();
    t.Name = 'GB-O-STD-LIC';
    t.c2g__Description__c = 'UK Standard Rated (Output: VAT 20%) - Licensing TEST';
    t.c2g__GeneralLedgerAccount__c = g.Id;
    t.JVCO_Rate__c = 20;
    t.ffvat__NetBox__c = 'Box 6';
    t.ffvat__TaxBox__c = 'Box 1';

    Insert t;

    c2g__codaTaxRate__c r = new c2g__codaTaxRate__c();
    r.c2g__StartDate__c = Date.today();
    r.c2g__Rate__c = 20;
    r.c2g__TaxCode__c = t.Id;

    Insert r;

    a.c2g__CODAOutputVATCode__c = t.Id;

    return a;
  }

  private static Account getInstantiatedCustomerAccount(CustomerAccountType cat, String name) {
    
    Account a = new Account();

    c2g__codaDimension1__c dim = new c2g__codaDimension1__c();
    dim.c2g__ReportingCode__c = 'BC008TEST';
    dim.Name = 'General Purpose';
    insert dim;

    a.Name = name;
    a.c2g__CODADimension1__c = dim.Id;
    a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();

    if (cat == CustomerAccountType.AGENCY) {
      a.Type = 'Agency';
    } else if (cat == CustomerAccountType.ACCOUNT_MANAGER) {
      a.Type = 'Account Manager';
    } else if (cat == CustomerAccountType.CUSTOMER) {
      a.Type = 'Customer';
    } else if (cat == CustomerAccountType.KEY_ACCOUNT) {
      a.Type = 'Key Account';
    }

    return a;
  }

  private static JVCO_Venue__c getInstantiatedVenue(String venueName) {
    JVCO_Venue__c venue = new JVCO_Venue__c();
    venue.Name = venueName;
    venue.JVCO_City__c = 'BALLINGRY';
    venue.JVCO_Street__c = '40 Ballifeary Road';
    venue.JVCO_Postcode__c = 'KY5 4BT';
    venue.JVCO_Country__c = 'United Kingdom';
    return venue;
  }

  private static JVCO_Affiliation__c getInstantiatedAffiliation(Id accountId, Id venueId) {
    JVCO_Affiliation__c affiliation = new JVCO_Affiliation__c();
    affiliation.Name = accountId + ' : ' + venueId;
    affiliation.JVCO_Account__c = accountId;
    affiliation.JVCO_Venue__c = venueId;
    affiliation.JVCO_Start_Date__c = date.today();
    return affiliation;
  }

  public static JVCO_Affiliation__c getInstantiatedAffiliationPublic(Id accountId, Id venueId) {
    JVCO_Affiliation__c affiliation = new JVCO_Affiliation__c();
    affiliation.Name = accountId + ' : ' + venueId;
    affiliation.JVCO_Account__c = accountId;
    affiliation.JVCO_Venue__c = venueId;
    affiliation.JVCO_Start_Date__c = system.today();
    return affiliation;
  }

}