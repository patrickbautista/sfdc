/* ----------------------------------------------------------------------------------------------
   Name: JVCO_QuoteCreationController_Test.cls 
   Description: Test Class for JVCO_QuoteCreationController

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
   23-Mar-2017   0.1        kristoffer.d.martin Intial creation
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_QuoteCreationController_Test {
    
    @testSetup
    private static void setupData()
    {
        Test.startTest();

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        //Contact c = new Contact();
        //c.Lastname = 'Test Contact';
        //insert c;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        
        insert acc;

        //Contact con = JVCO_TestClassObjectBuilder.createContact(acc.id);
        //insert con;

        //acc.JVCO_Billing_Contact__c = con.id;
        //update acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        insert aff;

        

        Test.StopTest();
    }

    /* ----------------------------------------------------------------------------------------------------------
    Author:         kristoffer.d.martin
    Company:        Accenture
    Description:    Test Method for Quote Creation
    Inputs:         n/a
    Returns:        n/a
    <Date>          <Authors Name>                      <Brief Description of Change> 
    23-Mar-2017     kristoffer.d.martin                 Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/ 
    private static testMethod void quoteCreateTest() 
    {

        string LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    
        String instance = '';
        Organization o = [SELECT OrganizationType, InstanceName FROM Organization order by CreatedDate DESC limit 1];
        String orgType = o.OrganizationType;
        String insName = o.InstanceName;
        Account acc = [select Id from Account WHERE RecordTypeId = :LicenceRT];
         update acc;
      //  System.debug('@@ACCT - ' + acc);

        Apexpages.currentPage().getParameters().put('id',acc.Id);
        Apexpages.StandardSetController std;

        Test.startTest();
        ApexPages.currentPage().getHeaders().put('Host', insName+'.visual.force.com');
        JVCO_QuoteCreationController quoteController = new JVCO_QuoteCreationController(std);
        PageReference pageRef = quoteController.quoteCreate();
        system.assert(pageRef != null);

        Test.stopTest();
    }

    private static testMethod void quoteCreateTestMonthlyFrequency() 
    {

        string LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    
        String instance = '';
        Organization o = [SELECT OrganizationType, InstanceName FROM Organization order by CreatedDate DESC limit 1];
        String orgType = o.OrganizationType;
        String insName = o.InstanceName;
        Account acc = [select Id from Account WHERE RecordTypeId = :LicenceRT];
        acc.JVCO_Review_Frequency__c = 'Monthly';
         update acc;
      //  System.debug('@@ACCT - ' + acc);

        Apexpages.currentPage().getParameters().put('id',acc.Id);
        Apexpages.StandardSetController std;

        Test.startTest();
        ApexPages.currentPage().getHeaders().put('Host', insName+'.visual.force.com');
        JVCO_QuoteCreationController quoteController = new JVCO_QuoteCreationController(std);
        PageReference pageRef = quoteController.quoteCreate();
        system.assert(pageRef != null);

        Test.stopTest();
    }

    private static testMethod void quoteCreateTestQuarterlyFrequency() 
    {

        string LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    
        String instance = '';
        Organization o = [SELECT OrganizationType, InstanceName FROM Organization order by CreatedDate DESC limit 1];
        String orgType = o.OrganizationType;
        String insName = o.InstanceName;
        Account acc = [select Id from Account WHERE RecordTypeId = :LicenceRT];
        acc.JVCO_Review_Frequency__c = 'Quarterly';
        update acc;
      //  System.debug('@@ACCT - ' + acc);

        Apexpages.currentPage().getParameters().put('id',acc.Id);
        Apexpages.StandardSetController std;

        Test.startTest();
        ApexPages.currentPage().getHeaders().put('Host', insName+'.visual.force.com');
        JVCO_QuoteCreationController quoteController = new JVCO_QuoteCreationController(std);
        PageReference pageRef = quoteController.quoteCreate();
        system.assert(pageRef != null);

        Test.stopTest();
    }

    private static testMethod void quoteCreateTestNullFrequency() 
    {

        string LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    
        String instance = '';
        Organization o = [SELECT OrganizationType, InstanceName FROM Organization order by CreatedDate DESC limit 1];
        String orgType = o.OrganizationType;
        String insName = o.InstanceName;
        Account acc = [select Id from Account WHERE RecordTypeId = :LicenceRT];
        acc.JVCO_Review_Frequency__c = 'Ad-hoc';
        update acc;
      //  System.debug('@@ACCT - ' + acc);

        Apexpages.currentPage().getParameters().put('id',acc.Id);
        Apexpages.StandardSetController std;

        Test.startTest();
        ApexPages.currentPage().getHeaders().put('Host', insName+'.visual.force.com');
        JVCO_QuoteCreationController quoteController = new JVCO_QuoteCreationController(std);
        PageReference pageRef = quoteController.quoteCreate();
        system.assert(pageRef != null);

        Test.stopTest();
    }
    
    @isTest
    private static void testLEXUser(){
    Account acct = [SELECT ID FROM Account WHERE RecordType.Name = 'Licence Account'];
    
    Apexpages.currentPage().getParameters().put('id',acct.Id);
    Apexpages.StandardSetController std;

    Test.startTest();
        //ApexPages.currentPage().getHeaders().put('Host', insName+'.visual.force.com');
        JVCO_QuoteCreationController quoteController = new JVCO_QuoteCreationController(std);
        quoteController.uiThemeDisplayed = 'Theme4d';
        PageReference pageRef = quoteController.quoteCreate();
    Test.stopTest();
    }
    
    @isTest
    private static void testcheckUserIfRestricted(){
    List<Account> acct = [SELECT ID FROM Account WHERE RecordType.Name = 'Licence Account'];
        
    test.startTest();        
    	ApexPages.standardSetController sc = new ApexPages.standardSetController(acct);
    	JVCO_QuoteCreationController createQuote = new JVCO_QuoteCreationController(sc);
        createQuote.uiThemeDisplayed = 'Theme4d';
    	boolean userRestricted = JVCO_QuoteCreationController.checkUserIfRestricted();
    test.stopTest();
    }
}