@isTest
private class JVCO_LeadMappingLogicTest

{	@testSetup
    private static void setupData()
    {
    	Test.StartTest();

		Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
	    insert acc;
  
  		Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
	    insert acc2;

	//    Contact c = JVCO_TestClassObjectBuilder.createContact(acc.id);
	//    insert c; 

	    Lead l = new Lead();
    	l.LastName = 'Lead Test';
    	l.JVCO_Venue_Postcode__c = 'BL2 1AA';
    	l.JVCO_Field_Visit_Requested__c = true;
    	l.JVCO_Field_Visit_Requested_Date__c = date.today();
    	l.JVCO_Venue_Street__c = 'Test Street';
    	l.Company = 'Test Company';
    	l.JVCO_Preferred_Communication_Method__c = 'Email';
        l.Email= 'test@email.com';
    	insert l;

    	JVCO_Venue__c v = JVCO_TestClassObjectBuilder.createVenue();
	    insert v;

	    Opportunity o = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
	    insert o;
	    Test.StopTest();
    }     

    //SET RecordTypeID to Customer Account
	@isTest
	static void createAccount()
	{
		string LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
		string CustomerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
    	
    	Test.startTest();
    	Lead l = [SELECT Id, JVCO_Sole_Trader__c FROM Lead order by CreatedDate DESC limit 1];
    	Account a = [SELECT Id FROM Account WHERE RecordTypeId = :LicenceRT order by CreatedDate DESC limit 1];
		Test.StopTest();

		Account aObj = JVCO_LeadMappingLogic.createAccount(l, a.id);
 		System.AssertEquals(CustomerRT, aObj.RecordTypeId, 'Record Type ID was not changed to Customer Account');
	}  

	//Create Contact Obj
	@isTest
	static void createContact()
	{
		string LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    	
    	Test.startTest();
    	Lead l = [SELECT Id FROM Lead order by CreatedDate DESC limit 1];
    	Account a = [SELECT Id FROM Account WHERE RecordTypeId = :LicenceRT order by CreatedDate DESC limit 1];
		Test.StopTest();

		Contact cObj = JVCO_LeadMappingLogic.createContact(l, a.id);
 		System.Assert(!String.isEmpty(cObj.LastName), 'No Contact was created'); 
	}       

	//Set Venue Status to Active
	@isTest
	static void createVenue()
	{
		Test.startTest();
    	Lead l = [SELECT Id FROM Lead order by CreatedDate DESC limit 1];
    	JVCO_Venue__c v = [SELECT Id FROM JVCO_Venue__c order by CreatedDate DESC limit 1];
    	Test.StopTest();
		
		JVCO_Venue__c vObj = JVCO_LeadMappingLogic.createVenue(l, v.id);
 		System.AssertEquals('Active', vObj.JVCO_Status__c, 'Venue status was not set to Active'); 
	}  

	//Create Affiliation if none, and return null if it has data already
	@isTest
	static void createAffiliation()
	{
		string LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    	
    	Test.startTest();
    	Lead l = [SELECT Id, leadSource, JVCO_Start_Date__c  FROM Lead order by CreatedDate DESC limit 1];
    	JVCO_Venue__c v = [SELECT Id FROM JVCO_Venue__c order by CreatedDate DESC limit 1];
    	Account a = [SELECT Id FROM Account WHERE RecordTypeId = :LicenceRT order by CreatedDate DESC limit 1];
    	Test.StopTest();

		//No Affiliation created
		JVCO_Affiliation__c aObj = JVCO_LeadMappingLogic.createAffiliation(l, a.id, v.id);
		System.Assert(!String.isEmpty(aObj.JVCO_Account__c), 'No JVCO_Affiliation__c was created'); 


		//There's already an Affiliation created 
		JVCO_Affiliation__c aObj2 = JVCO_LeadMappingLogic.createAffiliation(l, a.id, v.id);
		System.assertEquals(null, aObj2, 'JVCO_Affiliation__c was created'); 
	}  

	//Set Account Record Type ID to Licence Account
	@isTest
	static void createLicenceAccount()
	{
		string LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    	
    	Test.startTest();
    	Lead l = [SELECT Id,JVCO_Start_Date__c FROM Lead order by CreatedDate DESC limit 1];
    	Account a = [SELECT Id, Name FROM Account WHERE RecordTypeId = :LicenceRT order by CreatedDate DESC limit 1];
    	Contact c = [SELECT Id FROM Contact order by CreatedDate DESC limit 1];
    	Test.StopTest();
		
		Account aObj = JVCO_LeadMappingLogic.createLicenceAccount(a.name, c.id, l.id);
		System.AssertEquals(LicenceRT, aObj.RecordTypeId, 'Record Type ID was not changed to Licence Account');
	}

	//Create Quote Object
	@isTest
	static void createQuote()
	{
		string LicenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
    	
    	Test.startTest();
    	Account a = [SELECT Id, Name FROM Account WHERE RecordTypeId = :LicenceRT order by CreatedDate DESC limit 1];
    	Contact c = [SELECT Id FROM Contact order by CreatedDate DESC limit 1];
    	Opportunity o = [SELECT Id FROM Opportunity order by CreatedDate DESC limit 1];
    	Test.StopTest();
		
		SBQQ__Quote__c qObj = JVCO_LeadMappingLogic.createQuote(a.id, c.id, o.id);
		System.Assert(!String.isEmpty(qObj.SBQQ__Account__c), 'No Quote was created'); 
	}
}