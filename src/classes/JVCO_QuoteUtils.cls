/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CaseUtils.cls 
   Description: Utility class for Case object

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  24-Oct-2016   0.1         eu.rey.t.cadag  Initial creation
 
  ----------------------------------------------------------------------------------------------- */
public with sharing class JVCO_QuoteUtils {
    
    /*
    public static Map<ID,Pricebook2> pbIdToPriceBookWithPbeMap;

    public static void getPriceBooks(){
        JVCO_QuoteUtils.pbIdToPriceBookWithPbeMap = new map<ID,Pricebook2> ([select id, name
      from Pricebook2 where isStandard = true and isActive = true]);
    }
    */
    
    public static void updateObject(List<sObject> lstObject)
    {
        if(lstObject.size() > 0)
        {
          List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
          List<Database.SaveResult> res = Database.update(lstObject,false); 
          Savepoint sp = Database.setSavepoint();
            for(Integer i = 0; i < lstObject.size(); i++)
            {
                Database.SaveResult srOppList = res[i];
                sObject origrecord = lstObject[i];
              if(!srOppList.isSuccess())
                {
                    Database.rollback(sp);
                    for(Database.Error objErr:srOppList.getErrors())
                    {
                        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                        errLog.ffps_custRem__Related_Object_Key__c= String.valueOf(origrecord.ID);
                        errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                        errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                        
                        if(origrecord.getSObjectType() == Order.sObjectType)
                            errLog.ffps_custRem__Message__c = 'JVCO_OrderTriggerHandler:priceCalculationFailed-Update Order Record';
                        else if(origrecord.getSObjectType() == OrderItem.sObjectType)
                            errLog.ffps_custRem__Message__c = 'JVCO_OrderTriggerHandler:priceCalculationFailed-Update OrderItem Record';
                        errLogList.add(errLog);                         
                    }
                }
            }
            if(!errLogList.isempty()){
                 try
                 {
                     insert errLogList;
                 }
                 catch(Exception ex)
                 {
                     System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                     
                 }
            }
        }
    }
}