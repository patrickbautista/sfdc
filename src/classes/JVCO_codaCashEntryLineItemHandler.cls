/* ----------------------------------------------------------------------------------------------
    Name: JVCO_codaCashEntryLineItemHandler
    Description: Handler class for JVCO_codaCashEntryLineItem trigger

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    11-Jul-2018  0.1         franz.g.a.dimaapi     Intial creation - GREEN-31903
----------------------------------------------------------------------------------------------- */
public class JVCO_codaCashEntryLineItemHandler
{
	public static void beforeInsert(List<c2g__codaCashEntryLineItem__c> cashEntryLineList)
    {
	}

	/* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Update the Sales Invoice's Cash Payment Method by Account Reference
        Inputs: List<c2g__codaCashEntryLineItem__c>
        Returns: 
        <Date>      <Authors Name>      <Brief Description of Change> 
        11-Jul-2018 franz.g.a.dimaapi    Initial version of function - GREEN-31903
    ----------------------------------------------------------------------------------------------- */
	public static void updateSalesInvoiceByAccountReference(Map<String, String> sInvToPaymentMethodMap)
	{
        List<c2g__codaInvoice__c> updatedSInvList = new List<c2g__codaInvoice__c>();
        for(c2g__codaInvoice__c sInv : [SELECT Id, Name, 
                                        JVCO_Invoice_Legacy_Number__c, 
                                        JVCO_Cash_Payment_Method__c
                                        FROM c2g__codaInvoice__c
                                        WHERE Name IN : sInvToPaymentMethodMap.keySet()
                                        OR JVCO_Invoice_Legacy_Number__c IN : sInvToPaymentMethodMap.keySet()])
        {
            String paymentMethodForLegacyNumber = sInvToPaymentMethodMap.get(sInv.JVCO_Invoice_Legacy_Number__c);
            String paymentMethodForSInvName = sInvToPaymentMethodMap.get(sInv.Name);
            if(paymentMethodForLegacyNumber != null || paymentMethodForSInvName != null)
            {
                sInv.JVCO_Cash_Payment_Method__c = paymentMethodForLegacyNumber != null ? paymentMethodForLegacyNumber : paymentMethodForSInvName;
                updatedSInvList.add(sInv);
            }
        }
        
        List<c2g__codaCreditNote__c> updatedSCNoteList = new List<c2g__codaCreditNote__c>();
        for(c2g__codaCreditNote__c cNote : [SELECT Id, Name, 
                                            //Invoice_Legacy_Number__c, 
                                            JVCO_Cash_Payment_Method__c
                                            FROM c2g__codaCreditNote__c
                                            WHERE Name IN : sInvToPaymentMethodMap.keySet()
                                            //OR Invoice_Legacy_Number__c IN : accRefToPaymentMethodMap.keySet()
                                           ])
        {
            //String paymentMethodForSCNoteLegacyNumber = accRefToPaymentMethodMap.get(cNote.Invoice_Legacy_Number__c);
            String paymentMethodForSCNoteName = sInvToPaymentMethodMap.get(cNote.Name);
            if(//paymentMethodForSCNoteLegacyNumber != null || 
                paymentMethodForSCNoteName != null)
            {
                //cNote.JVCO_Cash_Payment_Method__c = paymentMethodForSCNoteLegacyNumber != null ? paymentMethodForSCNoteLegacyNumber : paymentMethodForSCNoteName;
                cNote.JVCO_Cash_Payment_Method__c = paymentMethodForSCNoteName;
                updatedSCNoteList.add(cNote);
            }
        }
        
        if(!updatedSInvList.isEmpty())
        {
            update updatedSInvList;
        }
        
        if(!updatedSCNoteList.isEmpty())
        {
            update updatedSCNoteList;
        }
	}
}