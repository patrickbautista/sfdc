/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_CalcHistoricCurrentValuesScheduler.cls 
   Description:     Class that handles JVCO_CalcHistoricCurrentValuesBatch Schedulable APEX class
   Test class:      JVCO_CalcHistoricCurrentVSchedulerTest.cls 

   	Date                 Version     	Author                          	Summary of Changes 
   	-----------          -------     ---------------------------        -------------------------------------------
	18-Jun-2018           0.1        Accenture-reymark.j.l.arlos      	Intial draft          
  ------------------------------------------------------------------------------------------------ */
global class JVCO_CalcHistoricCurrentValuesScheduler implements Schedulable 
{
    private static final JVCO_HistoricCurrentValuesSetting__c customSet = JVCO_HistoricCurrentValuesSetting__c.getOrgDefaults();
    
	global void execute(SchedulableContext sc) 
	{
		JVCO_CalcHistoricCurrentValuesBatch calcHCVBatch = new JVCO_CalcHistoricCurrentValuesBatch();
		Database.executebatch(calcHCVBatch,  Integer.valueOf(customSet.JVCO_SchedulerBatchSize__c));
	}

	private static String getSchedulerExpression(DateTime dt) 
    {
         return '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();    
    }

    public static void CallExecuteMomentarily()  
    {
        System.schedule('JVCO_CalcHistoricCurrentValuesScheduler', getSchedulerExpression(DateTime.Now().addSeconds(5)), new JVCO_CalcHistoricCurrentValuesScheduler());
    }
}