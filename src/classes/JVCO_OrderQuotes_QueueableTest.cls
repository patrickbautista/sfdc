/* ----------------------------------------------------------------------------------------------
    Name: JVCO_OrderQuotes_QueueableTest
    Description: Test Class for JVCO_OrderQuotes_Queueable

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    08-Aug-2018     0.1         Jules Pablo         Intial creation
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_OrderQuotes_QueueableTest {

    @testSetup static void setupTestData() {
        Product2 prod1 = new Product2();
        prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 83202-C';
        a1.Type = 'Key Account';
        insert a1;
        
        Account a2 = new Account();
        a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.Id);
        insert a2;

        Pricebook2 testPB2 = new Pricebook2();
        testPB2.name = 'Standard Price Book';
        testPB2.IsActive = true;
        insert testPB2;
        
        PriceBookEntry pbe = new PriceBookEntry();
        pbe= JVCO_TestClassObjectBuilder.createPriceBookEntry(prod1.Id);
        insert pbe;
        
        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 83202';
        o.AccountId = a2.Id;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Renewal';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        insert q;
        
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        insert ql;
        
        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();
        venueRecord.Name = 'test1';              
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = new JVCO_Affiliation__c();
        affilRecord.JVCO_Account__c = a2.Id;
        affilRecord.JVCO_Venue__c = venueRecord.id;   
        affilRecord.JVCO_Start_Date__c = system.today();   
        Insert  affilRecord;   
        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;   

        Opportunity opp = new Opportunity();
        opp.Id = o.Id;
        opp.Amount = 10;
        opp.StageName = 'Closed Won';
        opp.SBQQ__Contracted__c = true;
        update opp;
    }

    private static testMethod void testMethodOrderQuotesExecute() {
        Set<Id> idsToPass = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [SELECT Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT limit 1];
        Opportunity o = [SELECT Id, Amount, SBQQ__PrimaryQuote__c FROM Opportunity where Name = 'Test opportunity 83202' and AccountId =: a.Id];
        idsToPass.add(o.Id);

        Test.startTest();
        System.enqueueJob(new JVCO_OrderQuotes_Queueable(a, idsToPass, true));
        Test.stopTest();
    }

    private static testMethod void testMethodOrderQuotesExecuteError() {
        Set<Id> idsToPass = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        JVCO_OrderQuotesHelper.testError = true;

        Account a = [SELECT Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT limit 1];
        Opportunity o = [SELECT Id, Amount, SBQQ__PrimaryQuote__c FROM Opportunity where Name = 'Test opportunity 83202' and AccountId =: a.Id];
        idsToPass.add(o.Id);

        Test.startTest();
        System.enqueueJob(new JVCO_OrderQuotes_Queueable(a, idsToPass, false));
        Test.stopTest();
    }

    private static testMethod void testMethodOrderQuotesReExecute() {
        Set<Id> idsToPass = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [SELECT Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT limit 1];
        Opportunity o = [SELECT Id, Amount, SBQQ__PrimaryQuote__c FROM Opportunity where Name = 'Test opportunity 83202' and AccountId =: a.Id];
        idsToPass.add(o.Id);

        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
        opportunityMap.put(o.Id, o);

        Map<String, String> quoteStringMap = new Map<String,String>();

        Test.startTest();
        System.enqueueJob(new JVCO_OrderQuotes_Queueable(opportunityMap, a, quoteStringMap, 1, false, 1));
        Test.stopTest();
    }

    private static testMethod void testMethodOrderQuotesReExecute2() {
        Set<Id> idsToPass = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [SELECT Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT limit 1];
        Opportunity o = [SELECT Id, Name, CloseDate, Amount, SBQQ__PrimaryQuote__c FROM Opportunity where Name = 'Test opportunity 83202' and AccountId =: a.Id];
        idsToPass.add(o.Id);

        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
        opportunityMap.put(o.Id, o);

        Opportunity testOpp = opportunityMap.values()[0].clone(false,false,false,false);
        //insert testOpp;

        opportunityMap.put(testOpp.Id, testOpp);

        Map<String, String> quoteStringMap = new Map<String,String>();

        Test.startTest();
        System.enqueueJob(new JVCO_OrderQuotes_Queueable(opportunityMap, a, quoteStringMap, 1, false, 4));
        Test.stopTest();
    }

    private static testMethod void testMethodOrderQuotesReExecute3() {
        Set<Id> idsToPass = new Set<Id>();
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a = [SELECT Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT limit 1];
        Opportunity o = [SELECT Id, Name, CloseDate, Amount, SBQQ__PrimaryQuote__c FROM Opportunity where Name = 'Test opportunity 83202' and AccountId =: a.Id];
        idsToPass.add(o.Id);

        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
        opportunityMap.put(o.Id, o);

        Opportunity testOpp = opportunityMap.values()[0].clone(false,false,false,false);
        //insert testOpp;

        opportunityMap.put(testOpp.Id, testOpp);

        Map<String, String> quoteStringMap = new Map<String,String>();

        Test.startTest();
        System.enqueueJob(new JVCO_OrderQuotes_Queueable(opportunityMap, a, quoteStringMap, 1, false, 1));
        Test.stopTest();
    }
}