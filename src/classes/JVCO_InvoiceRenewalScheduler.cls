/* --------------------------------------------------------------------------------------------------
Name:            JVCO_InvoiceRenewalScheduler.cls 
Description:     Class that handles InvoiceRenewalBatch Schedulable APEX class
Test class:      

Date                 Version     Author                          Summary of Changes 
-----------          -------     -----------------               -------------------------------------------
05-Jul-2017           0.1         Accenture-Mel Andrei Santos    Intial draft
24-Aug-2017           0.2         Accenture-Ryan Limlingan       Changed scope to 1 due to limitation of
                                                                 Bill Now operation
------------------------------------------------------------------------------------------------ */

global class JVCO_InvoiceRenewalScheduler implements Schedulable{
    
    global void execute (SchedulableContext sc)
    { 

        JVCO_InvoiceRenewalBatch InvoiceRenewalBatch = new JVCO_InvoiceRenewalBatch();
        JVCO_InvoiceRenewalScheduler InvoiceRenewalSched = new  JVCO_InvoiceRenewalScheduler();
        //String cronStr = '0 0 23 1/1 * ? *';
       // String jobID = System.schedule('Process InvoiceRenewalBatch', cronStr, InvoiceRenewalBatch);
        Database.executeBatch(InvoiceRenewalBatch,1);
    }
    
    private static String getSchedulerExpression(DateTime dt) 
    {
         return '' + dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + ' ' +  dt.day() + ' ' + dt.month() + ' ? ' + dt.year();    
    }

    public static void callExecuteMomentarily()  
    {
        System.schedule('JVCO_OrderRenewalScheduler', getSchedulerExpression(DateTime.Now().addSeconds(5)), new JVCO_InvoiceRenewalScheduler());
    }
}