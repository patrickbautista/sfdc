/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_ContentDocumentLinkTriggerHandler.cls 
   Description:     Handler Class for JVCO_ContentDocumentLinkTrigger
   Test class:      JVCO_ContentDocumentHandlerTest.cls 

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
  06-Apr-2018       0.1         Accenture-recuerdo.b.bregente     Initial draft           
  08-May-2018       0.2         Hazel Jurna M. Limot              Changes made to fix GREEN-31820
  11-May-2018       0.3         Hazel Jurna M. Limot              Changes made to fix GREEN-31820
  ---------------------------------------------------------------------------------------------------------- */
Public Class JVCO_ContentDocumentLinkTriggerHandler{
    
    public static void onBeforeInsert(List<ContentDocumentLink> links){
        List<ContentDocumentLink> linksToProcessForRestriction = new List<ContentDocumentLink>();
        Set<Id> accountIdsRestriction = new Set<Id>();
        
        for(ContentDocumentLink link : links){
            if(link.LinkedEntityId <> null){
                link.Visibility='AllUsers'; // Hazel Jurna M. Limot - Fix for GREEN-31820
                accountIdsRestriction.add(link.LinkedEntityId);
                linksToProcessForRestriction.add(link);
            }
        }
        restrictOnInfringementOrEnforcement(linksToProcessForRestriction, accountIdsRestriction);
        
    }
    
    private static void restrictOnInfringementOrEnforcement(List<ContentDocumentLink> links, Set<Id> accountIdsRestriction){
        Map<Id, Account> restrictedAccountsMap = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN :accountIdsRestriction AND 
                                                    (JVCO_In_Enforcement__c = true OR JVCO_In_Infringement__c = true) AND 
                                                    (JVCO_Credit_Status__c = 'Enforcement - Debt Recovery' OR JVCO_Credit_Status__c = 'Enforcement - Infringement')]);
                                                           
        Boolean isRestricted = (restrictedAccountsMap.size() > 0 && checkUserIfRestricted());
        
        for(ContentDocumentLink link : links){
            if(isRestricted && restrictedAccountsMap.containsKey(link.LinkedEntityId)){
                link.addError('Creating Notes/Files on Accounts marked "In Enforcement" or "In Infringement" is not allowed.'); // Hazel Jurna M. Limot - Fix for GREEN-31820 - Added /Files
            }
        }
    }
    
    @TestVisible // Hazel Jurna M. Limot - Fix for GREEN-31820
    private static Boolean checkUserIfRestricted(){
        String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
        Map<Id,PermissionSet> enAdPermSet = new Map<Id,PermissionSet>([SELECT Id FROM PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors']);
        List<PermissionSetAssignment> permissionSetList = [SELECT Id FROM PermissionSetAssignment WHERE 
                                                            AssigneeId = :UserInfo.getUserId() AND PermissionSetId IN :enAdPermSet.keySet()];
                                                            
        return (profileName <> 'System Administrator' && permissionSetList.isEmpty());
    }
}