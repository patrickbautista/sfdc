/* ----------------------------------------------------------------------------------------------
Author: kristoffer.d.martin
Company: Accenture
Description: Test Class for JVCO_InvokeDupeElimMergeProcessVenueExt
<Date>      <Authors Name>       <Brief Description of Change> 
31-May-2017 kristoffer.d.martin  Initial version
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_InvokeDupeElimMergeProcVenExt_Test {
	
	@testSetup static void setUpTestData(){
        
		Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='JVCO_Venue__c'); 
            listQueue.add(q1);
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }

        JVCO_Venue__c venue = new   JVCO_Venue__c();
        venue.Name = 'update venue';
        venue.JVCO_Field_Visit_Requested__c = false;
        venue.JVCO_Street__c = 'testStreet';
        venue.JVCO_City__c = 'testCoty';
        venue.JVCO_Postcode__c ='LL53 2LU';
        venue.JVCO_Country__c ='UK';

        insert venue;
    }
	
	/* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Test JVCO_InvokeDupeElimMergeProcessVenueExt.redirectVenueToDupeElimPage
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>       <Brief Description of Change> 
    16-Dec-2016 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    @isTest static void testRedirectVenueToDupeElimPage()
    {

    	List<JVCO_Venue__c> venueList = [SELECT Id, Name FROM JVCO_Venue__c order by CreatedDate DESC limit 1];

        Test.startTest();

        ApexPages.StandardSetController con = 
                           new ApexPages.StandardSetController(venueList);
            
        con.setSelected(venueList);

        JVCO_InvokeDupeElimMergeProcessVenueExt dupeElimMergeController = new JVCO_InvokeDupeElimMergeProcessVenueExt(con);
        PageReference pageRef = dupeElimMergeController.redirectVenueToDupeElimPage();
        system.assert(pageRef != null);
        system.debug('Chuck123===' + pageRef);

        Test.stopTest();

     //   PageReference pageRef2 = dupeElimMergeController.redirectVenueToDupeElimPage2(); 
    }
}