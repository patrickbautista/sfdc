/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_AttachmentTriggerHandler.cls 
   Description:     Handler Class for JVCO_AttachmentTrigger
   Test class:      JVCO_AttachmentTriggerHandlerTest.cls 

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
  05-Apr-2018       0.1         Accenture-recuerdo.b.bregente     Initial draft           
  ---------------------------------------------------------------------------------------------------------- */
public class JVCO_AttachmentTriggerHandler {
	public static void onBeforeInsert(List<Attachment> attachments){
        List<Attachment> attachmentToProcessForRestriction = new List<Attachment>();
        Set<Id> accountIdsRestriction = new Set<Id>();
        
        for(Attachment attachmentRec : attachments){
            if(attachmentRec.ParentId <> null){
                accountIdsRestriction.add(attachmentRec.ParentId);
                attachmentToProcessForRestriction.add(attachmentRec);
            }
        }
        restrictOnInfringementOrEnforcement(attachmentToProcessForRestriction, accountIdsRestriction);
    }
    
    public static void onBeforeUpdate(List<Attachment> attachments){
        List<Attachment> attachmentToProcessForRestriction = new List<Attachment>();
        Set<Id> accountIdsRestriction = new Set<Id>();
        
        for(Attachment attachmentRec : attachments){
            if(attachmentRec.ParentId <> null){
                accountIdsRestriction.add(attachmentRec.ParentId);
                attachmentToProcessForRestriction.add(attachmentRec);
            }
        }
        restrictUpdateAndDelete(attachmentToProcessForRestriction, accountIdsRestriction, 'Updating');
    }
    
    public static void onBeforeDelete(List<Attachment> attachments){
        List<Attachment> attachmentToProcessForRestriction = new List<Attachment>();
        Set<Id> accountIdsRestriction = new Set<Id>();
        
        for(Attachment attachmentRec : attachments){
            if(attachmentRec.ParentId <> null){
                accountIdsRestriction.add(attachmentRec.ParentId);
                attachmentToProcessForRestriction.add(attachmentRec);
            }
        }
        restrictUpdateAndDelete(attachmentToProcessForRestriction, accountIdsRestriction, 'Deleting');
    }
    
    private static void restrictOnInfringementOrEnforcement(List<Attachment> attachments, Set<Id> accountIdsRestriction){
        Map<Id, Account> restrictedAccountsMap = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN :accountIdsRestriction AND 
                                                    (JVCO_In_Enforcement__c = true OR JVCO_In_Infringement__c = true) AND 
                                                    (JVCO_Credit_Status__c = 'Enforcement - Debt Recovery' OR JVCO_Credit_Status__c = 'Enforcement - Infringement')]);
        
                                                            
        Boolean isRestricted = (restrictedAccountsMap.size() > 0 && checkUserIfRestricted());
       
        for(Attachment attachmentRec : attachments){
            if(isRestricted && restrictedAccountsMap.containsKey(attachmentRec.ParentId)){
                attachmentRec.addError('Attaching Files on Accounts marked "In Enforcement" or "In Infringement" is not allowed.');
            }
        }
    }
    
    private static void restrictUpdateAndDelete(List<Attachment> attachments, Set<Id> accountIdsRestriction, String operation){
        Map<Id, Account> restrictedAccountsMap = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN :accountIdsRestriction AND 
                                                    (JVCO_In_Enforcement__c = true OR JVCO_In_Infringement__c = true) AND 
                                                    (JVCO_Credit_Status__c = 'Enforcement - Debt Recovery' OR JVCO_Credit_Status__c = 'Enforcement - Infringement')]);
        
                                                            
        Boolean isRestricted = (restrictedAccountsMap.size() > 0 && checkUserIfRestricted());
       
        for(Attachment attachmentRec : attachments){
            if(isRestricted && restrictedAccountsMap.containsKey(attachmentRec.ParentId)){
                attachmentRec.addError(operation + ' Attachments on Accounts marked "In Enforcement" or "In Infringement" is not allowed.');
            }
        }
    }
    
    
    private static Boolean checkUserIfRestricted(){
        String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
        Map<Id,PermissionSet> enAdPermSet = new Map<Id,PermissionSet>([SELECT Id FROM PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors']);
        List<PermissionSetAssignment> permissionSetList = [SELECT Id FROM PermissionSetAssignment WHERE 
                                                            AssigneeId = :UserInfo.getUserId() AND PermissionSetId IN :enAdPermSet.keySet()];
                                                            
        return (profileName <> 'System Administrator' && permissionSetList.isEmpty());
    }
}