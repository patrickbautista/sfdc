/* ----------------------------------------------------------------------------------------------
    Name: JVCO_PayonomyPaymentAgreementLogic.cls 
    Description: Business logic class for <FOR UPDATE>

    Date         Version     Author                 Summary of Changes 
    -----------  -------     -----------------      -----------------------------------------
    29-Dec-2016   0.1         ryan.i.r.limlingan    Intial creation
    31-Jan-2017   0.2         ryan.i.r.limlingan    Changed logic of Sales Invoice Instalment Line Item creation, 
                                                    to consider First Interval Collection
    02-Mar-2017   0.3         ryan.i.r.limlingan    Added createDDMandate()
    28-May-2018   0.4         patrick.t.bautista    GREEN-32117 - Process Sales Invoice Installment line item one at a time
    25-Jul-2018   0.5         mary.ann.a.ruelan     GREEN-32835 - FF instalment lines calculating incorrectly
    18-Oct-2018   0.6         franz.g.a.dimaapi     GREEN-33471 - Clean Codes, Fix Installment Due Date Logic
----------------------------------------------------------------------------------------------- */
public class JVCO_PayonomyPaymentAgreementLogic
{
    //Query all holidays
    private static Set<Date> gbHolidayDateSet = new Set<Date>();
    private static Integer JOBS_PROCESSING_START = (Integer)JVCO_General_Settings__c.getOrgDefaults().JVCO_Jobs_Processing_Time_Start__c != null ? 
                                                    (Integer)JVCO_General_Settings__c.getOrgDefaults().JVCO_Jobs_Processing_Time_Start__c : 0;

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Links Agreements to corresponding Invoice Groups
        Inputs: List<PAYREC2__Payment_Agreement__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        10-Jan-2017 ryan.i.r.limlingan  Initial version of function
        01-Jun-2018 franz.g.a.dimaapi   
    ----------------------------------------------------------------------------------------------- */
    public static void linkToInvoiceGroup(List<PAYREC2__Payment_Agreement__c> agreements)
    {
        List<String> invoiceGroupNameList = new List<String>();
        for(PAYREC2__Payment_Agreement__c a : agreements)
        {
            invoiceGroupNameList.add(a.PAYREC2__Description__c);
        }

        // Retrieve Invoice Group records
        Map<String,Id> invoiceGroupNameIdMap = new Map<String, Id>();
        Map<Id, String> igIdToSInvNamesMap = new Map<Id, String>();
        for(JVCO_Invoice_Group__c ig : [SELECT Id, Name, CC_SINs__c 
                                        FROM JVCO_Invoice_Group__c
                                        WHERE Name IN :invoiceGroupNameList])
        {
            invoiceGroupNameIdMap.put(ig.Name, ig.Id);
            if(ig.CC_SINs__c != null)
            {
                igIdToSInvNamesMap.put(ig.Id, ig.CC_SINs__c);
            }    
        }

        for(PAYREC2__Payment_Agreement__c agreement : agreements)
        {
            agreement.JVCO_Invoice_Group__c = invoiceGroupNameIdMap.get(agreement.PAYREC2__Description__c);
        }

        if(!igIdToSInvNamesMap.isEmpty())
        {
            Map<String, Id> sInvNameToInvoiceGroupIdMap = new Map<String, Id>();
            for(Id igId: igIdToSInvNamesMap.keySet())
            {
                String sInvNameList = igIdToSInvNamesMap.get(igId);
                for(String sInvName : sInvNameList.split(','))
                {
                    sInvNameToInvoiceGroupIdMap.put(sInvName, igId);
                }                                
            }

            List<c2g__codaInvoice__c> sInvList = [SELECT Id, Name, 
                                                    JVCO_Invoice_Group__c
                                                    FROM c2g__codaInvoice__c
                                                    WHERE Name IN : sInvNameToInvoiceGroupIdMap.keySet()];
            for(c2g__codaInvoice__c sInv : sInvList)
            {
                sInv.JVCO_Invoice_Group__c = sInvNameToInvoiceGroupIdMap.get(sInv.Name);
            }

            update sInvList;
        }    
    }
    

    /* ----------------------------------------------------------------------------------------------
        Author: michael.alamag
        Company: Accenture
        Description: Update the Payment Agreement Lookup in Invoice Group
        Inputs: List<PAYREC2__Payment_Agreement__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        18-Apr-2017 michael.alamag      Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static void linkInvoiceGrouptoPaymentAgreement(List<PAYREC2__Payment_Agreement__c> newAgreements)
    {
        List<JVCO_Invoice_Group__c> invoiceGroupList = new List<JVCO_Invoice_Group__c>();
        for(PAYREC2__Payment_Agreement__c agreement : newAgreements)
        {
            if(agreement.JVCO_Invoice_Group__c != null)
            {
                JVCO_Invoice_Group__c rig = new JVCO_Invoice_Group__c();
                rig.Id = agreement.JVCO_Invoice_Group__c;
                rig.JVCO_Payment_Agreement__c = agreement.Id;
                invoiceGroupList.add(rig); 
            }
        }
        update invoiceGroupList;    
    }
    
    /* ----------------------------------------------------------------------------------------------
    Author: patrick.t.bautista
    Company: Accenture
    Description: Delete duplicate agreement
    Inputs: List<PAYREC2__Payment_Agreement__c>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    18-Apr-2017 patrick.t.bautista  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public static void deleteDuplicateAgreements(List<PAYREC2__Payment_Agreement__c> agreements)
    {
        List<PAYREC2__Payment_Agreement__c> aggrementListToDel = new List<PAYREC2__Payment_Agreement__c>();
        List<String> invoiceGroupNameList = new List<String>();
        for (PAYREC2__Payment_Agreement__c a : agreements)
        {
            invoiceGroupNameList.add(a.PAYREC2__Description__c);
        }
        String aggmentToDelCounter = '';
        Integer count = 0;
        
        for(PAYREC2__Payment_Agreement__c aggmntDel: [SELECT id, PAYREC2__Description__c from PAYREC2__Payment_Agreement__c 
                                                               WHERE PAYREC2__Description__c IN: invoiceGroupNameList
                                                               AND PAYREC2__Description__c != null])
        {
            count++;
            if(aggmentToDelCounter == '' || aggmentToDelCounter == aggmntDel.PAYREC2__Description__c)
            {
                aggmentToDelCounter = aggmntDel.PAYREC2__Description__c;
                if(count > 1)
                {
                    aggrementListToDel.add(aggmntDel);
                }
            }
        }
        
        delete aggrementListToDel;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Creates Installment Line Item records for each Sales Invoice record for Direct
                     Debit payment
        Inputs: List<PAYREC2__Payment_Agreement__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        29-Dec-2016 ryan.i.r.limlingan  Initial version of function
        02-Mar-2017 ryan.i.r.limlingan  Added call to createDDMandate()
        02-Apr-2018 john.patrick.valdez Added update on Payment Method
        03-Aug-2018 franz.g.a.dimaapi   GREEN-32835 - Optimize Method
    ----------------------------------------------------------------------------------------------- */
    public static void createInstalmentPlan(List<PAYREC2__Payment_Agreement__c> agreements)
    {
        Map<Id,PAYREC2__Payment_Agreement__c> agreementMap = new Map<Id,PAYREC2__Payment_Agreement__c>(
                                                            [SELECT Id, PAYREC2__Payment_Schedule__c, PAYREC2__First_Collection_Amount__c,
                                                            PAYREC2__Ongoing_Collection_Amount__c, PAYREC2__Final_Collection_Amount__c,
                                                            PAYREC2__Description__c, JVCO_Invoice_Group__c
                                                            FROM PAYREC2__Payment_Agreement__c
                                                            WHERE Id IN : agreements 
                                                            AND PAYREC2__Description__c != null]);
        
        

        Map<String,Id> groupInvoiceNameAgreementMap = new Map<String,Id>();
        for (PAYREC2__Payment_Agreement__c agreement : agreementMap.values())
        {
            groupInvoiceNameAgreementMap.put(agreement.PAYREC2__Description__c, agreement.Id);
        }

        // Retrieve the related invoice records
        Map<Id, c2g__codaInvoice__c> invoiceMap = new Map<Id, c2g__codaInvoice__c>(
                                                [SELECT Id, c2g__InvoiceTotal__c,
                                                c2g__NumberOfPayments__c, c2g__FirstDueDate__c,
                                                JVCO_Payment_Method__c, c2g__OutstandingValue__c
                                                FROM c2g__codaInvoice__c
                                                WHERE JVCO_Invoice_Group__r.Name IN :groupInvoiceNameAgreementMap.keySet()]);
              
        // Delete existing instalment plans
        delete [SELECT Id FROM c2g__codaInvoiceInstallmentLineItem__c WHERE c2g__Invoice__c IN :invoiceMap.keySet()];

        //Retrieve Payonomy Payment Schedule Map
        Map<Id,PAYREC2__Payment_Schedule__c> scheduleMap = new Map<Id,PAYREC2__Payment_Schedule__c>(
                                                                    [SELECT Id, PAYREC2__Day__c, PAYREC2__Max__c, 
                                                                    PAYFISH3__First_Collection_Interval__c
                                                                    FROM PAYREC2__Payment_Schedule__c]);

        generateHolidays(System.today(), 3);
        List<c2g__codaInvoiceInstallmentLineItem__c> invoiceLineItemList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
        for (Id aId : agreementMap.keySet())
        {
            PAYREC2__Payment_Agreement__c agreement = agreementMap.get(aId);
            PAYREC2__Payment_Schedule__c sched = scheduleMap.get(agreement.PAYREC2__Payment_Schedule__c);
            Double totalAmount = computeTotalAmount(agreement, sched.PAYREC2__Max__c);
            invoiceLineItemList.addAll(createInstalmentLineItem(agreement, sched, totalAmount, 
                                                                Integer.valueOf(sched.PAYREC2__Max__c), 
                                                                agreement.JVCO_Invoice_Group__c));
        }
        insert invoiceLineItemList;

        List<c2g__codaInvoice__c> updatePaymentMethod = new List <c2g__codaInvoice__c>();
        for(c2g__codaInvoice__c invMapPayment : invoiceMap.values())
        {
            invMapPayment.JVCO_Payment_Method__c = 'Direct Debit';
            updatePaymentMethod.add(invMapPayment);
        }
        update updatePaymentMethod;

        createDDMandate(agreementMap.keySet());
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Creates DD Mandate record associated to the Account
        Inputs: List<Account>, Map<Id, Account>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        02-Mar-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static void createDDMandate(Set<Id> agreementIdSet)
    {
        Map<Id, PAYREC2__Payment_Agreement__c> agreementMap = new Map<Id, PAYREC2__Payment_Agreement__c>(
                                                                [SELECT Id, PAYFISH3__Current_Bank_Account__r.PAYACCVAL1__Bank_Name__c, PAYREC2__Description__c,
                                                                PAYFISH3__Current_Bank_Account__r.PAYACCVAL1__Account_Number__c,
                                                                PAYFISH3__Current_Bank_Account__r.PAYACCVAL1__Sort_Code__c,
                                                                PAYFISH3__Calculated_AUUDIS_Ref__c, PAYREC2__Payment_Schedule__r.PAYREC2__Max__c,
                                                                PAYREC2__Payment_Schedule__r.PAYREC2__Day__c, PAYREC2__Account__c
                                                                FROM PAYREC2__Payment_Agreement__c WHERE Id IN :agreementIdSet]);

        Map<Id, PAYREC2__Payment_Agreement__c> accIdAgreementMap = new Map<Id, PAYREC2__Payment_Agreement__c>();
        for(Id aId : agreementMap.keySet())
        {
            PAYREC2__Payment_Agreement__c agreement = agreementMap.get(aId);
            accIdAgreementMap.put(agreement.PAYREC2__Account__c, agreement);
        }

        for(JVCO_DD_Mandate__c ddMandate : [SELECT JVCO_Licence_Account__c
                                            FROM JVCO_DD_Mandate__c
                                            WHERE JVCO_Licence_Account__c
                                            IN :accIdAgreementMap.keySet()
                                            AND JVCO_Deactivated__c = FALSE])
        {
            PAYREC2__Payment_Agreement__c agreement = agreementMap.get(ddMandate.JVCO_Licence_Account__c);
            // Remove from map Ids of Account with existing DD Mandate record
            accIdAgreementMap.remove(ddMandate.JVCO_Licence_Account__c);
        }

        List<JVCO_DD_Mandate__c> mandateList = new List<JVCO_DD_Mandate__c>();
        for(Id accId : accIdAgreementMap.keySet())
        {
            PAYREC2__Payment_Agreement__c agreement = accIdAgreementMap.get(accId);
            JVCO_DD_Mandate__c mandate = new JVCO_DD_Mandate__c();
            mandate.JVCO_Licence_Account__c = accId;
            mandate.JVCO_Bank_Name__c = agreement.PAYFISH3__Current_Bank_Account__r.PAYACCVAL1__Bank_Name__c;
            mandate.JVCO_Account_Number__c = agreement.PAYFISH3__Current_Bank_Account__r.PAYACCVAL1__Account_Number__c;
            mandate.JVCO_Sort_Code__c = agreement.PAYFISH3__Current_Bank_Account__r.PAYACCVAL1__Sort_Code__c;
            mandate.JVCO_AUDIS_Ref__c = agreement.PAYFISH3__Calculated_AUUDIS_Ref__c;
            mandate.JVCO_Collection_Day__c = Integer.valueOf(agreement.PAYREC2__Payment_Schedule__r.PAYREC2__Day__c);
            mandate.JVCO_Number_of_Payments__c = agreement.PAYREC2__Payment_Schedule__r.PAYREC2__Max__c;
            mandateList.add(mandate);
        }

        insert mandateList;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Computes the total amount to be collected from the Payment Agreement
        Inputs: PAYREC2__Payment_Agreement__c, Integer
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        29-Dec-2016 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static Double computeTotalAmount(PAYREC2__Payment_Agreement__c agreement,
                                            Decimal numberOfInstalments)
    {
        // Checks if First and/or Final Collection were set or not
        if(agreement.PAYREC2__First_Collection_Amount__c == null)
        {
            agreement.PAYREC2__First_Collection_Amount__c = agreement.PAYREC2__Ongoing_Collection_Amount__c;
        }
        if(agreement.PAYREC2__Final_Collection_Amount__c == null)
        {
            agreement.PAYREC2__Final_Collection_Amount__c = agreement.PAYREC2__Ongoing_Collection_Amount__c;
        }
        return (agreement.PAYREC2__First_Collection_Amount__c
                + agreement.PAYREC2__Final_Collection_Amount__c
                + ((numberOfInstalments - 2) * agreement.PAYREC2__Ongoing_Collection_Amount__c));
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Creates Installment Line Item for each Sales Invoice record
        Inputs: 
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        03-Jan-2017 ryan.i.r.limlingan  Initial version of function
        28-May-2018 patrick.t.bautista  GREEN-32117 - Process Sales Invoice Installment line item one at a time
        25-Jul-2018 mary.ann.a.ruelan   GREEN-32835 - changed argument in computepaidamount to i+1
        03-Aug-2018 franz.g.a.dimaapi   GREEN-32568 - Refactor Due Date Logic
    ----------------------------------------------------------------------------------------------- */
    public static List<c2g__codaInvoiceInstallmentLineItem__c> createInstalmentLineItem(PAYREC2__Payment_Agreement__c agreement, PAYREC2__Payment_Schedule__c sched,
                                                                                            Double totalAmount, Integer nthInstalment, Id invGrp)
    {   
        Date getFirstCollectionDate = computeDueDate(Integer.valueOf(sched.PAYREC2__Day__c), Integer.valueOf(sched.PAYFISH3__First_Collection_Interval__c));
        List<c2g__codaInvoiceInstallmentLineItem__c> newItemList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
        for (c2g__codaInvoice__c invoice : [SELECT id, JVCO_Invoice_Group__c, c2g__OutstandingValue__c
                                            FROM c2g__codaInvoice__c 
                                            WHERE JVCO_Invoice_Group__c = :invGrp])
        {   
            for(Integer i = 0; i < nthInstalment; i++)
            {
                c2g__codaInvoiceInstallmentLineItem__c item = new c2g__codaInvoiceInstallmentLineItem__c();
                item.c2g__Invoice__c = invoice.Id;
                item.c2g__Amount__c = computePaidAmount(agreement, sched, totalAmount, invoice.c2g__OutstandingValue__c, i+1);
                item.c2g__DueDate__c = getFirstCollectionDate.addMonths(i);
                newItemList.add(item);
            }
        }
        return newItemList;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Creates Installment Line Item for each Sales Invoice record
        Inputs: PAYREC2__Payment_Agreement__c, PAYREC2__Payment_Schedule__c, Double, Decimal, Integer
        Returns: Double
        <Date>      <Authors Name>      <Brief Description of Change> 
        03-Jan-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static Double computePaidAmount(PAYREC2__Payment_Agreement__c agreement,
        PAYREC2__Payment_Schedule__c sched, Double totalAmount, Decimal invoiceAmount,
        Integer nthInstalment)
    {
        if(nthInstalment == 1 && agreement.PAYREC2__First_Collection_Amount__c != null)
        {
            return ((agreement.PAYREC2__First_Collection_Amount__c / totalAmount) * invoiceAmount).setScale(2);
        }else if(nthInstalment == sched.PAYREC2__Max__c && agreement.PAYREC2__Final_Collection_Amount__c != null)
        {
            return ((agreement.PAYREC2__Final_Collection_Amount__c / totalAmount) * invoiceAmount).setScale(2);
        }else
        {
            return ((agreement.PAYREC2__Ongoing_Collection_Amount__c / totalAmount) * invoiceAmount).setScale(2);
        }
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Creates Installment Line Item for each Sales Invoice record
        Inputs: Integer
        Returns: Date
        <Date>      <Authors Name>      <Brief Description of Change> 
        03-Jan-2017 ryan.i.r.limlingan  Initial version of function
        31-Jan-2017 ryan.i.r.limlingan  Added First Interval Collection to the computation
        02-Aug-2018 franz.g.a.dimaapi   GREEN-32568 - Refactor and Added Bank Holiday Logic
    ----------------------------------------------------------------------------------------------- */
    public static Date computeDueDate(Integer day, Integer collectionInterval)
    {
        Date forecastedNewInstruction = System.now().hourGmt() > JOBS_PROCESSING_START ? System.today().addDays(1) : System.today();
        Integer monthAdded = forecastedNewInstruction.day() > day ? 1 : 0;

        Date computedCollectionDate = forecastedNewInstruction;
        Date firstMonday = Date.newInstance(1990, 1, 1);
        Integer workingDays = 0;
        while(workingDays < collectionInterval)
        {
            computedCollectionDate = computedCollectionDate.addDays(1);
            if(Math.mod(firstMonday.daysBetween(computedCollectionDate), 7) < 5)
            {  
                if(!gbHolidayDateSet.contains(computedCollectionDate) || 
                    (gbHolidayDateSet.contains(computedCollectionDate) && 
                        workingDays == (collectionInterval - 1)))
                {
                    workingDays++;
                }
            }
        }

        Date dueDate = Date.newInstance(forecastedNewInstruction.year(), forecastedNewInstruction.month() + monthAdded, day);
        return computedCollectionDate > dueDate ? dueDate.addMonths(1) : dueDate;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Creates a GB Holiday Sets
        Inputs: Date, Integer
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        03-Aug-2018 franz.g.a.dimaapi    GREEN-32568 - Initial version of function
    ----------------------------------------------------------------------------------------------- */
    private static void generateHolidays(Date d, Integer addedMonth)
    {
        if(gbHolidayDateSet.isEmpty())
        {
            for(JVCO_GBHoliday__c gbHoliday : [SELECT Id, JVCO_HolidayDate__c
                                                FROM JVCO_GBHoliday__c 
                                                WHERE JVCO_HolidayDate__c >= :d
                                                AND JVCO_HolidayDate__c <= :d.addMonths(addedMonth)
                                                AND JVCO_Is_Weekend__c = false])
            {
                gbHolidayDateSet.add(gbHoliday.JVCO_HolidayDate__c);
            }
        }    
    }
}