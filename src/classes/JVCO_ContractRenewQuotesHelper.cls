/* ------------------------------------------------------------------------------------------------
   Name:            JVCO_ContractRenewQuotesHelper
   Description:     Helper class for JVCO_ContractRenewQuotesBatch and JVCO_ContractRenewQuotes_Queueable

   Date            Version     Author                       Summary of Changes 
   -----------     -------     -----------------            ---------------------------------------
   09-Jul-2018     0.1         jules.osberg.a.pablo         Initial Version
   09-Oct-2018     0.3         rhys.j.c.dela.cruz           Added Queueable Status
   22-May-2019     0.2         rhys.j.c.dela.cruz           GREEN-33618 - Changes to queueable to accomodate new Temp Object
------------------------------------------------------------------------------------------------ */
public class JVCO_ContractRenewQuotesHelper {
    @testVisible
    private static Boolean testError = false;

    public JVCO_ContractRenewQuotesHelper() {
        
    }

    public static string getQueryString(Account accountRecord) {

        //rhys.j.c.dela.cruz - added addtlQuery, change query if queueable
        String addtlQuery = '';

        if(JVCO_KABatchSetting__c.getInstance(UserInfo.getUserId()).JVCO_ContractRenewQuotesQueueable__c){

            addtlQuery = ' and (SBQQ__Contracted__c = false OR (SBQQ__Contracted__c = true AND SBQQ__Ordered__c = false)) ';
        }
        else{

            addtlQuery = ' and SBQQ__Contracted__c = false ';
        }

        //String query = 'select Id, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName from Opportunity where StageName != \'Closed Won\' and SBQQ__Contracted__c = false and SBQQ__Ordered__c = false and AccountId = ' + '\'' + accountRecord.Id + '\'';
        //String query = 'select Id, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName from Opportunity where SBQQ__Contracted__c = false and AccountId = ' + '\'' + accountRecord.Id + '\'';
        String query = 'select Id, AccountId, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName from Opportunity where SBQQ__PrimaryQuote__r.JVCO_Number_Quote_Lines__c != 0 and SBQQ__PrimaryQuote__r.JVCO_Number_Quote_Lines__c != null ' + addtlQuery + ' and ( SBQQ__PrimaryQuote__r.SBQQ__Type__c IN (\'Renewal\',\'Quote\') or (SBQQ__PrimaryQuote__c = null AND (SBQQ__Renewal__c = true OR (SBQQ__Renewal__c = false AND SBQQ__AmendedContract__c = null) ) ) ) and AccountId = ' + '\'' + accountRecord.Id + '\''; // csaba.feher 12/04/2017 GREEN-26653 // mel.andrei.b.santos 12-Apr-2018 GREEN-31233 - added condition if primary quote = null
        return query;
    }

    public static string executeContractRenewalQuotes(List<Opportunity> processOpportunityList, Account accountRecord) {
        String returnErrorMessage = '';
        String recID = '';
        List<Opportunity> oppListToUpdate = new List<Opportunity>();
        List<SBQQ__Quote__c> quoteListToUpdate = new List<SBQQ__Quote__c>();
        List<sObject> lstObject = new List<sObject>();
        //New add
        String quoteRecId = '';
        Map<Id, Id> quoteIdMap = new Map<Id, Id>();

        Map<String, JVCO_Complete_Quote_Settings__c> settings = JVCO_Complete_Quote_Settings__c.getAll(); //mel.andrei.b.santos GREEN-31830

        Set<Id> quoteIdSet = new Set<Id>();
        Set<Id> tmpOppIds = new Set<Id>();

        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>(); //mel.andrei.b.santos GREEN-31830
        
        Savepoint sp = Database.setSavepoint();
        
        for (Opportunity opp : (processOpportunityList))
        {
            //oppIds.add(opp.Id);
            tmpOppIds.add(opp.Id);
        }
        
        //List<SBQQ__Quote__c> quoteList = [select Id, SBQQ__Status__c, SBQQ__Opportunity2__c, SBQQ__Opportunity2__r.StageName from SBQQ__Quote__c where SBQQ__Type__c = 'Renewal' and SBQQ__Primary__c = true and SBQQ__Opportunity2__c in: tmpOppIds];
        List<SBQQ__Quote__c> quoteList = [select Id, SBQQ__Status__c, SBQQ__Opportunity2__c, SBQQ__Opportunity2__r.StageName, SBQQ__NetAmount__c,NoOfQuoteLinesWithoutSPV__c, SBQQ__Opportunity2__r.SBQQ__PrimaryQuote__c from SBQQ__Quote__c where SBQQ__Type__c IN ('Renewal','Quote') and SBQQ__Primary__c = true and SBQQ__Opportunity2__c in: tmpOppIds]; // csaba.feher 12/04/2017 GREEN-26653 //23-01-2018 reymark.j.l.arlos added SBQQ__NetAmount__c on query
        
        if (quoteList.size() > 0) 
        {
            for (SBQQ__Quote__c quoteRec : quoteList) 
            {
                 //Start 11-Jul-2018 mel.andrei.b.santos GREEN-31830, added condition preventing the contracting of quotes if # of quote lines w/o SPV > 0  
                   
                if(quoteRec.NoOfQuoteLinesWithoutSPV__c > 0)
                    {
                        ffps_custRem__Custom_Log__c qErrLog = new ffps_custRem__Custom_Log__c();
                        /*
                        qErrLog.Name = String.valueOf(quoteRec.ID);
                        qErrLog.JVCO_FailedRecordObjectReference__c = String.valueOf(quoteRec.ID);
                        qErrLog.blng__FullErrorLog__c = string.valueof(settings.get('ErrorNoSPV').Value__c);
                        qErrLog.blng__ErrorCode__c = string.valueof(quoteRec.SBQQ__Opportunity2__c);
                        qErrLog.JVCO_ErrorBatchName__c = 'JVCO_ContractRenewQuotesBatch: No SPV';
                        */

                        qErrLog.ffps_custRem__Grouping__c = string.valueof(quoteRec.SBQQ__Opportunity2__c);
                        qErrLog.ffps_custRem__Detail__c = string.valueof(settings.get('ErrorNoSPV').Value__c);
                        qErrLog.ffps_custRem__Message__c = 'JVCO_ContractRenewQuotesBatch: No SPV';
                        qErrLog.ffps_custRem__Related_Object_Key__c = String.valueOf(quoteRec.ID);
                        qErrLog.ffps_custRem__Running_User__c = UserInfo.getUserId();

                        errLogList.add(qErrLog); 
                        returnErrorMessage += 'Record: ' + quoteRec + ' Cause'  + qErrLog.ffps_custRem__Detail__c + '<br>';
                    }
                    //end
                else
                {
                    if(quoteRec.SBQQ__Opportunity2__c != null)
                    {
                        Opportunity OppRec = quoteRec.SBQQ__Opportunity2__r;
                        OppRec.Id = quoteRec.SBQQ__Opportunity2__c;
                        OppRec.StageName = 'Closed Won';
                        OppRec.Amount = quoteRec.SBQQ__NetAmount__c; //23-Jan-2018 reymark.j.l.arlos setting amount on opp from quote
                        if(OppRec.SBQQ__PrimaryQuote__c == null){
                            OppRec.SBQQ__PrimaryQuote__c = quoteRec.Id; //Set Primary Quote    
                        }
                        OppRec.JVCO_OpportunityCancelled__c = false;
                        lstObject.add((sObject) OppRec);
                        quoteIdMap.put(OppRec.Id, quoteRec.Id);
                    }

                    quoteRec.SBQQ__Status__c = 'Approved';
                    quoteRec.JVCO_QuoteComplete__c = true;
                    quoteRec.JVCO_Cancelled__c = false;
                    lstObject.add((sObject)quoteRec);

                    quoteIdSet.add(quoteRec.Id);
                    System.debug('Debug Quote Id:' + quoteRec.Id);
                }
            }
        }

        /* ----------------------------------------------------------------------------------------------
            Date            Version     Author              Code Summary 
            -----------     -------     -----------------   -----------------------------------------
            28-Mar-2017     0.1         Jules Pablo         Unchecks the JVCO_IsComplete__c checkbox of the JVCO_Affiliation__c under SBQQ__QuoteLineGroup__c under SBQQ__Quote__c
        ----------------------------------------------------------------------------------------------- */   
        List<JVCO_Affiliation__c> affilToUpdate = new List<JVCO_Affiliation__c>();
        List<SBQQ__QuoteLineGroup__c> qlgToUpdate = new List<SBQQ__QuoteLineGroup__c>();
        //List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();

        Set<Id> qlgSetId = new Set<Id>();
        Set<Id> affilSetId = new Set<Id>();

        System.debug('Uncheck completed checkbox started');    
        if(quoteIdSet.size() > 0)
        {
            List<SBQQ__QuoteLineGroup__c> qlgList = [SELECT Id, JVCO_Affiliated_Venue__c, JVCO_IsComplete__c, SBQQ__Quote__c FROM SBQQ__QuoteLineGroup__c WHERE SBQQ__Quote__c in :quoteIdSet AND JVCO_Affiliated_Venue__c != NULL];
            for(SBQQ__QuoteLineGroup__c qlg : qlgList)
            {
                System.debug('Debug QLG Id:' + qlg.Id);
                if(qlg.JVCO_IsComplete__c){
                    qlg.JVCO_IsComplete__c = false;
                    lstObject.add((sObject) qlg);
                    //Moved into if to prevent additional updates to records
                    System.debug('Debug Affiliation Id:' + qlg.JVCO_Affiliated_Venue__c);        
                    affilSetId.add(qlg.JVCO_Affiliated_Venue__c);
                    quoteIdMap.put(qlg.Id, qlg.SBQQ__Quote__c);
                }
            } 

            for(JVCO_Affiliation__c affil : [SELECT Id, JVCO_IsComplete__c FROM JVCO_Affiliation__c WHERE Id in :affilSetId])
            {   
                if(affil.JVCO_IsComplete__c){
                    //Moved into if to prevent additional updates to records
                    affil.JVCO_IsComplete__c = false;
                    lstObject.add((sObject)affil);     
                }
            }         
        } 

        if(lstObject.size() > 0)
        {
            if(!lstObject.isEmpty())
            {
                if(Test.isRunningTest() && testError)
                {
                    lstObject.add(new Opportunity());
                    //lstObject.add(new SBQQ__QuoteLineGroup__c());
                    //lstObject.add(new JVCO_Affiliation__c());
                }
                // update oppListForUpdate; start Changed DML statement to database.update as per Ticket GREEN-17932 - Mel Andrei Santos 27-07-2017
                List<Database.SaveResult> res = Database.update(lstObject,false); // Start 24-08-2017 mel.andrei.b.santos
                for(Integer i = 0; i < lstObject.size(); i++)
                {
                    Database.SaveResult srOppList = res[i];
                    sObject origrecord = lstObject[i];
                    if(!srOppList.isSuccess())
                    {
                        Database.rollback(sp);
                        System.debug('Update sObject fail: ');
                        for(Database.Error objErr:srOppList.getErrors())
                        {
                            ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();

                            //errLog.Name = String.valueOf(origrecord.ID);
                            errLog.ffps_custRem__Related_Object_Key__c = String.valueOf(origrecord.ID);
                            errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                            errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                            errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();

                            if(origrecord.getSObjectType() == Opportunity.sObjectType){
                                
                                errLog.ffps_custRem__Message__c = 'JVCO_CompleteQuoteContracted: Update Opportunity Record';
                                if(quoteIdMap.containsKey(origrecord.Id)){
                                    quoteRecId = 'Quote: ' + String.valueOf(quoteIdMap.get(origrecord.id));
                                    errLog.ffps_custRem__Detail__c += '\nQuote: ' + String.valueOf(quoteIdMap.get(origrecord.id));
                                }
                            }
                            else if(origrecord.getSObjectType() == SBQQ__Quote__c.sObjectType){

                                errLog.ffps_custRem__Message__c = 'JVCO_CompleteQuoteContracted: Update Quote Record';
                                quoteRecId = 'Quote: ' + String.valueOf(origrecord.Id);
                                errLog.ffps_custRem__Detail__c += '\nQuote: ' + String.valueOf(origrecord.Id);
                            }
                            else if(origrecord.getSObjectType() == SBQQ__QuoteLineGroup__c.sObjectType){

                                errLog.ffps_custRem__Message__c = 'JVCO_CompleteQuoteContracted: Update SBQQ__QuoteLineGroup__c Record';
                                if(quoteIdMap.containsKey(origrecord.Id)){
                                    quoteRecId = 'Quote: ' + String.valueOf(quoteIdMap.get(origrecord.id));
                                    errLog.ffps_custRem__Detail__c += '\nQuote: ' + String.valueOf(quoteIdMap.get(origrecord.id));
                                }
                            }
                            else if(origrecord.getSObjectType() == JVCO_Affiliation__c.sObjectType){
                                errLog.ffps_custRem__Message__c = 'JVCO_CompleteQuoteContracted: Update JVCO_Affiliation__c Record';
                            }

                            recID = String.valueOf(origrecord.ID);
                            returnErrorMessage += quoteRecId + ' Record: ' + recId + ' '  + string.valueof(objErr.getStatusCode()) + '<br>';
                            errLogList.add(errLog);                         
                        }
                    }
                }
            }
        }
        if(errLogList.isempty())
        {
             // Start mel.andrei.b.santos 12-04-2018 GREEN-31233
            /* Moved logic to first part of class
            Set<ID> idOppSet = new Set<ID>();
            for(SBQQ__Quote__c qRec:quoteList)
            {
                idOppSet.add(qRec.SBQQ__Opportunity2__c);
                system.debug('@@@ check idOppSet ' + idOppset);
            }
            
            List<Opportunity> setOpp = new List<Opportunity>();
            setOpp = [Select ID,SBQQ__PrimaryQuote__c from Opportunity where id in :idOppSet];
            system.debug('@@@ check setOpp ' + setOpp);
            JVCO_OpportunityTriggerHandler.setPrimaryQuote(setOpp);
            */

            // END */
            for(SBQQ__Quote__c qRec:quoteList)
            {
               
                try
                {
                    SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractGenerator',qRec.SBQQ__Opportunity2__c , null); 
                }
                catch(Exception e)
                {
                    String ErrorLineNumber = e.getLineNumber() != Null ? String.ValueOf(e.getLineNumber()):'None';
                    String ErrorMessage = 'Cause: ' + e.getCause() + ' ErrMsg: ' + e.getMessage() + ' Quote: ' + qRec.id;
                    
                    if(System.isQueueable()){
                        errLogList.add(JVCO_ErrorUtil.logError(qRec.SBQQ__Opportunity2__c, ErrorMessage , ErrorLineNumber , 'JVCO_ContractRenewQuotesQueueable:SBQQ.ContractManipulationAPI'));
                    }
                    else{
                        errLogList.add(JVCO_ErrorUtil.logError(qRec.SBQQ__Opportunity2__c, ErrorMessage , ErrorLineNumber , 'JVCO_ContractRenewQuotesBatch:SBQQ.ContractManipulationAPI'));    
                    }

                    returnErrorMessage += 'Line Number: ' + ErrorLineNumber+ ' '  + ErrorMessage + '<br>';
                    Database.rollback(sp);
                }
            }
        }

        if(!errLogList.isempty()){
             try
             {
                 insert errLogList;

                 //16-May-2019 rhys.j.c.dela.cruz Upserting kaTempHolder record if Contracting Failed
                 if(System.isQueueable()){

                    JVCO_KATempHolder__c kaTempHolder = new JVCO_KATempHolder__c();
                    kaTempHolder.Name = processOpportunityList[0].Id;
                    kaTempHolder.JVCO_AccountId__c = accountRecord.Id;
                    kaTempHolder.JVCO_OppId__c = processOpportunityList[0].Id;
                    kaTempHolder.JVCO_Contracted__c = false;
                    kaTempHolder.JVCO_ErrorMessage__c = returnErrorMessage;

                    upsert kaTempHolder Name;
                 }
             }
             catch(Exception ex)
             {
                 System.debug('Error: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
                 
             }
        }
        else{

            //16-May-2019 rhys.j.c.dela.cruz Upserting kaTempHolder record if Contracting Success
            if(System.isQueueable()){

                JVCO_KATempHolder__c kaTempHolder = new JVCO_KATempHolder__c();
                kaTempHolder.Name = processOpportunityList[0].Id;
                kaTempHolder.JVCO_AccountId__c = accountRecord.Id;
                kaTempHolder.JVCO_OppId__c = processOpportunityList[0].Id;
                kaTempHolder.JVCO_Contracted__c = true;

                upsert kaTempHolder Name;
             }
        }

        return returnErrorMessage;
    }

    public static void sendCompletionEmail(Account accountRecord, String email, Integer recCount, Integer recErrCount, string batchErrorMessage, Integer querySize) {

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(UserInfo.getUserId());
        mail.setSaveAsActivity(false);
        mail.setReplyTo('no-reply@salesforce.com');
        if(System.isQueueable()){
            mail.setSenderDisplayName('Queueable Processing');
            mail.setSubject('Contract Renew Quotes Queueable Process Completed');
        }else{
            mail.setSenderDisplayName('Batch Processing');
            mail.setSubject('Contract Renew Quotes Batch Process Completed');
        }

        List<JVCO_KATempHolder__c> kaTempList = new List<JVCO_KATempHolder__c>();
        Integer totalContracts = 0;

        //29-May-2019 rhys.j.c.dela.cruz - GREEN-33618 - Use KA Temp Holder for count. Check if queueable
        if(System.isQueueable()){

            kaTempList = [SELECT Id, JVCO_AccountId__c, JVCO_OppId__c, JVCO_Contracted__c, JVCO_Ordered__c, JVCO_ProcessCompleted__c, JVCO_ErrorMessage__c FROM JVCO_KATempHolder__c WHERE JVCO_AccountId__c =: accountRecord.Id AND JVCO_OppId__c != null];

            totalContracts = 0;
            recErrCount = 0;
            batchErrorMessage = '';

            if(!kaTempList.isEmpty()){

                for(JVCO_KATempHolder__c kaTempRec : kaTempList){

                    if(kaTempRec.JVCO_Contracted__c){
                        totalContracts++;
                    }
                    else if(!kaTempRec.JVCO_Contracted__c){
                        recErrCount++;
                        batchErrorMessage += '<li>' + kaTempRec.JVCO_ErrorMessage__c + '</li>';
                    }
                }
            }
        }
        else{

            totalContracts = querySize - recErrCount;
        }
        //Integer totalContracts = contractBatchSize * a.JobItemsProcessed;
        //Integer totalContracts = querySize - recErrCount; 
        //mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus);
        //mail.setPlainTextBody('This is to notify you that the process to Contract Renew Quotes has now completed for ' + accountRecord.Name + '. Number of Quotes Contracted: ' + totalContracts + 'Number of failures: ' + (contractBatchSize * a.NumberOfErrors));
        String emailMsg = '<html><body>This is to notify you that the process to Contract Renew Quotes has now completed for ' + accountRecord.Name + '.<br><br>Number of Renew Quotes Contracted: ' + totalContracts;
        
        //if(errorMessage != '') {
        if(batchErrorMessage != '') {   
            emailMsg += '<br><br>Number of failures: ' + recErrCount + '</br> + Error: <br><ul>' + batchErrorMessage + '</ul>';
        }
        
        emailMsg += ' </body></html> ';

        mail.setHTMLBody(emailMsg);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

        //Check if queueable
        if(System.isQueueable()){
            JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_ContractRenewQuotesQueueable__c', '','JVCO_ContractRenewQuotesLastSubmitted__c');
        }
        else{
            JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_ContractRenewQuotesApexJob__c', '','JVCO_ContractRenewQuotesLastSubmitted__c');
        }
        
        //if(a.Status != 'Failed') {
        //    Id batchId = database.executeBatch(new JVCO_OrderQuotesBatch(accountRecord, oppIds, false), contractBatchSize);
        //    JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_OrderRenewQuotesApexJob__c', batchId,'JVCO_ContractRenewQuotesLastSubmitted__c');
        //}
    }

    //Method for sending Email of Started Job
    public static void sendBeginJobEmail(Account accountRecord, Integer sizeOfJob, Boolean queueableProcess){

        //User currentUser = [SELECT Id, Email FROM USER WHERE Id = :UserInfo.getUserId() LIMIT 1];

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(UserInfo.getUserId());
        mail.setSaveAsActivity(false);

        mail.setReplyTo('no-reply@salesforce.com');
        if(queueableProcess){
            mail.setSenderDisplayName('Queueable Processing');
            mail.setSubject('Contract Renew Quotes Queueable Process Started');
        }else{
            mail.setSenderDisplayName('Batch Processing');
            mail.setSubject('Contract Renew Quotes Batch Process Started');
        }
        
        String emailMsg = '<html><body>This is to notify you that the process to Contract Renew Quotes has now been started for ' + accountRecord.Name + ' ('+ System.now() + '). <br><br>Number of Renew Quotes to be Contracted: ' + sizeOfJob + ' records.';
        emailMsg += ' </body></html> ';

        mail.setHTMLBody(emailMsg);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

}