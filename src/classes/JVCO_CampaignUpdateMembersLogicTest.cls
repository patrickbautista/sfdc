/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CampaignUpdateMembersLogicTest.cls 
   Description: Test Class for JVCO_CampaignUpdateMembersLogic

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  01-Sep-2016   0.1         ryan.i.r.limlingan  Intial creation
  07-Sep-2016   0.2         ryan.i.r.limlingan  Refactored code and added new functions
  13-Sep-2016   0.3         ryan.i.r.limlingan  Added test class for negative testing
  14-Sep-2016   0.4         ryan.i.r.limlingan  Refactored checking of records in each test function
  15-Sep-2016   0.5         ryan.i.r.limlingan  Reflected reference to renamed field
  ----------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_CampaignUpdateMembersLogicTest {

    static Id queueId;
    // Constant used to determine number of records to create
    static final Integer NUM_RECORDS_TO_CREATE = 200;

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Sets up the needed records for testing (Campaign, Leads, and CampaignMembers)
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    01-Sep-2016 ryan.i.r.limlingan  Initial version of function
    07-Sep-2016 ryan.i.r.limlingan  Refactored code to improve readability
  ----------------------------------------------------------------------------------------------- */
    @testSetup static void setUpTestData()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'utest', Email='unit.test@unit.test.com',
                          EmailEncodingKey='UTF-8', LastName='Unit Test', 
                          LanguageLocaleKey='en_US', LocaleSIdKey='en_GB', ProfileId = p.Id,
                          TimezoneSIdKey='Europe/London', Username='unit.test@unit.test.com',
                          Division='Legal', Department='Legal');
        System.runAs(u)
        {
            // Setup for Queue related operations
            List<Group> groupToCheckList = [SELECT Id FROM Group
                                            WHERE Name LIKE 'Telesales Partner%'];
            if (groupToCheckList.size() < 1)
            {
                List<Group> groupList = new List<Group>{
                    (new Group(Name='MarketMakers', type='Queue')),
                    (new Group(Name='Ventrica', type='Queue'))
                };
                insert groupList;
    
                List<QueueSObject> queueList = new List<QueueSObject>();
                for (Group g : groupList)
                {
                    queueList.add(new QueueSObject(QueueID=g.id, SobjectType='Lead'));
                }
                insert queueList;
            }
        }

        // Setup for Campaign Related operations
        List<Campaign> newCampaignList = new List<Campaign>{
            (createCampaign('Campaign_ChannelUpdatedxx1', 'Email', null)),
            (createCampaign('Campaign_PartnerUpdatedxx2', 'Telesales', 'Ventrica')),
            (createCampaign('Campaign_AddInvalidMembersxx', 'Telesales', 'MarketMakers'))
        };
        insert newCampaignList;

        // Insert Leads to be used for both tests
        List<Lead> allNewLeadsList = new List<Lead>();
        allNewLeadsList.addAll(createLeads('Channel_Leadsxx'));
        allNewLeadsList.addAll(createLeads('Partner_Leadxx'));
        insert allNewLeadsList;

        // List to be used for bulk insert of CampaignMember records
        List<CampaignMember> campaignMembersList = new List<CampaignMember>();
        campaignMembersList.addAll(populateCampaignMemberList('Campaign_ChannelUpdatedxx1',
                                                              'Channel_Leadsxx%'));
        campaignMembersList.addAll(populateCampaignMemberList('Campaign_PartnerUpdatedxx2',
                                                              'Partner_Leadxx%'));
        // Insert all created CampaignMember records for both tests
        insert campaignMembersList;
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Bulk test for the case when the Channel of the Campaign is updated to Telesales
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    01-Sep-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    @isTest static void CampaignChannelUpdatedBulk_Test()
    {
        Campaign c = [SELECT Id, JVCO_Campaign_Channel__c, JVCO_Telesales_Partner__c
                      FROM Campaign WHERE Name='Campaign_ChannelUpdatedxx1'];

        // Updates that will fire the trigger
        c.JVCO_Campaign_Channel__c = 'Telesales';
        c.JVCO_Telesales_Partner__c = 'MarketMakers';

        Test.startTest();
        update c;
        Test.stopTest();

        for (Lead l : retrieveRecordsToAssert(c.Id))
        {
            System.assertEquals(getQueueId(), l.OwnerId,
                'Lead Record ' + l.LastName + ' was not reassigned to correct Queue');
        }
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Bulk test for the case when the Telesales Partner of the Campaign is updated
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    01-Sep-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    @isTest static void CampaignTelesalesPartnerUpdatedBulk_Test()
    {
        Campaign c = [SELECT Id, JVCO_Telesales_Partner__c FROM Campaign
                      WHERE Name='Campaign_PartnerUpdatedxx2'];

        // Update that will fire the trigger
        c.JVCO_Telesales_Partner__c = 'MarketMakers';

        Test.startTest();
        update c;
        Test.stopTest();

        for (Lead l : retrieveRecordsToAssert(c.Id))
        {
            System.assertEquals(getQueueId(), l.OwnerId,
            '   Lead Record ' + l.LastName + ' was not reassigned to correct Queue');
        }
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Bulk test for the case when created Campaign Member is invalid
    Inputs: N/A
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    13-Sep-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    @isTest static void CampaignAddMemberInvalidBulk_Test()
    {
        Campaign c = [SELECT Id, JVCO_Campaign_Channel__c, JVCO_Telesales_Partner__c
                      FROM Campaign WHERE Name='Campaign_AddInvalidMembersxx'];

        // Insert Leads to be used for test
        List<Lead> allNewLeadsList = new List<Lead>(createLeads('Add_Leadsxx'));
        insert allNewLeadsList;

        // List to be used for bulk insert of invalid CampaignMember records
        List<CampaignMember> campaignMembersList = new List<CampaignMember>(
                populateCampaignMemberList('Campaign_AddInvalidMembersxx', 'Add_Leadsxx%'));
        
        JVCO_CampaignSharedLogic.shouldInvalidate = TRUE;
        Test.startTest();
        try
        {
            insert campaignMembersList;
        } catch (Exception e){
            JVCO_CampaignSharedLogic.shouldInvalidate = FALSE;
            System.assertEquals('DmlException', e.getTypeName());
        }
        Test.stopTest();

        Integer i = 0;
        for (Lead l : retrieveRecordsToAssert(c.Id))
        {
            if (i++ < JVCO_CampaignSharedLogic.NUM_RECORDS_TO_FAIL)
            {
                System.assertNotEquals(getQueueId(), l.OwnerId,
                    'Lead Record ' + l.LastName + ' was reassigned to correct Queue');
            } else {
                System.assertEquals(getQueueId(), l.OwnerId,
                    'Lead Record ' + l.LastName + ' was not reassigned to correct Queue');
            }
        }
    }


/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Retrieve records to be asserted for the test functions
    Inputs: Id
    Returns: List<Lead>
    <Date>      <Authors Name>      <Brief Description of Change> 
    14-Sep-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    static List<Lead> retrieveRecordsToAssert(Id campaignId)
    {
        List<CampaignMember> campaignMemberList = [SELECT LeadId FROM CampaignMember
                                                   WHERE CampaignId = :campaignId];
        Set<Id> leadIdSet = new Set<Id>();
        for (CampaignMember cm : campaignMemberList)
        {
            if (!leadIdSet.contains(cm.LeadId))
            {
                leadIdSet.add(cm.LeadId);
            }
        }
        return [SELECT LastName, OwnerId FROM Lead WHERE Id IN :leadIdSet];
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Creates new Campaign records
    Inputs: String, String, String
    Returns: Campaign
    <Date>      <Authors Name>      <Brief Description of Change> 
    01-Sep-2016 ryan.i.r.limlingan  Initial version of function
    15-Sep-2016 ryan.i.r.limlingan  Reflected reference to JVCO_Campaign_Category__c
  ----------------------------------------------------------------------------------------------- */
    static Campaign createCampaign(String name, String channel, String partner)
    {
        Date startDate = Date.newInstance(2016, 10, 10);
        Date endDate = Date.newInstance(2016, 11, 11);

        return (new Campaign(Name=name, Type='Churn',
                JVCO_Campaign_Channel__c=channel, JVCO_Campaign_Category__c='Shop',
                JVCO_Telesales_Partner__c=partner, StartDate=startDate, EndDate=endDate));
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Creates new Lead records
    Inputs: String
    Returns: Lead
    <Date>      <Authors Name>      <Brief Description of Change> 
    01-Sep-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    static List<Lead> createLeads(String name)
    {
        List<Lead> newLeads = new List<Lead>();
        for (Integer i = 0; i < NUM_RECORDS_TO_CREATE; i++)
        {
            newLeads.add(new Lead(Company='LeadTest_Company' + i, LastName=name + i,
                Email='email' + i + '@test.com', Status='Open'));
        }
        return newLeads;
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Creates CampaignMember records related to a Campaign specified by the filters
    Inputs: String, String
    Returns: List<CampaignMember>
    <Date>      <Authors Name>      <Brief Description of Change> 
    07-Sep-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    static List<CampaignMember> populateCampaignMemberList(String campaignFilter, String leadFilter)
    {
        List<CampaignMember> campaignMemberList = new List<CampaignMember>();
        // Query target Campaign
        Campaign c = [SELECT Id FROM Campaign WHERE Name LIKE :campaignFilter];
        // Query target Leads
        List<Lead> leadList = [SELECT Id FROM Lead WHERE Name LIKE :leadFilter];
        for (Lead l : leadList)
        {
            campaignMemberList.add(new CampaignMember(Status='Responded', CampaignId=c.Id,
                    LeadId=l.Id));
        }
        return campaignMemberList;
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Retrieves Id of the Queue where the Lead records will be reassigned
    Inputs: String
    Returns: Lead
    <Date>      <Authors Name>      <Brief Description of Change> 
    01-Sep-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    static Id getQueueId()
    {
        if (queueId == null)
        {
            queueId = [SELECT Id FROM Group
                       WHERE Type = 'Queue' AND Name='MarketMakers'].Id;
        }
        return queueId;
    }
}