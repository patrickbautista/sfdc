/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CampaignMemberReassignToQueueLogic.cls 
   Description: Business logic class for updating Ownership of CampaignMembers after insert

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  02-Sep-2016   0.1         ryan.i.r.limlingan  Intial creation
  07-Sep-2016   0.2         ryan.i.r.limlingan  Updated to call functions from a shared class
  19-Sep-2016   0.3         ryan.i.r.limlingan  Updated to reflect changes in a function
  21-Sep-2016   0.4         ryan.i.r.limlingan  Changed logic of reassignMembers to already filter
                                                CampaignMember list
  ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_CampaignMemberReassignToQueueLogic
{

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Checks if Campaign from which the CampaignMember was added has Telesales channel
    Inputs: List<CampaignMember>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    02-Sep-2016 ryan.i.r.limlingan  Initial version of function
    21-Sep-2016 ryan.i.r.limlingan  Filtered CampaignMember list to only include new records;
                                    Directly called updateCampaignMemberOwner
  ----------------------------------------------------------------------------------------------- */
    public static void reassignMembers(List<CampaignMember> members)
    {

        // Retrieve the Campaigns from where the members are added
        Set<Id> campaignIdSet = new Set<Id>();
        for (CampaignMember cm : members)
        {
            if (!campaignIdSet.contains(cm.CampaignId))
            {
                campaignIdSet.add(cm.CampaignId);
            }
        }

        // Filter out Campaigns that do not specify Telesales as Channel
        List<Campaign> campaignList = [SELECT Id FROM Campaign WHERE Id IN :campaignIdSet
                                       AND JVCO_Campaign_Channel__c = 'Telesales'];

        campaignIdSet.clear(); // Reuse variable
        for (Campaign c : campaignList)
        {
            campaignIdSet.add(c.Id);
        }

        Set<CampaignMember> filteredMembersSet = new Set<CampaignMember>(members);
        for (CampaignMember cm : members)
        {
            // Filter CampaignMember records to be updated;
            // If CampaignId is not found in campaignIdSet, remove from Set
            if (!campaignIdSet.contains(cm.CampaignId))
            {
                filteredMembersSet.remove(cm);
            }
        }

        List<CampaignMember> membersToUpdateList = new List<CampaignMember>(filteredMembersSet);
        if (membersToUpdateList.size() > 0)
        {
            // Call function to perform actual reassignment
            JVCO_CampaignSharedLogic.updateCampaignMemberOwner(membersToUpdateList);
        }
    }
}