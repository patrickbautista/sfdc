/* ----------------------------------------------------------------------------------------------
   Name: JVCO_VenueAssignFieldAgentsPostLogicTest.cls 
   Description: Test Class for JVCO_VenueAssignFieldAgentsPostCodeLogic

   Date         Version     Author                 Summary of Changes 
   -----------  -------     -----------------      -----------------------------------------
  07-Sep-2016   0.1         mark.glenn.b.ayson      Intial creation
  08-Sep-2016   0.2         mark.glenn.b.ayson      Added test methods
  24-Dec-2016   0.3        Robert John B. Lacatan   Added more test methods
  02-Jan-2016   0.4        Robert John B. Lacatan   Added comments
  ----------------------------------------------------------------------------------------------- */

@isTest
public with sharing class JVCO_VenueAssignFieldAgentsPostLogicTest {
    @testSetup static void setUpTestData(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'utest', Email='unit.test@unit.test.com',
                          EmailEncodingKey='UTF-8', LastName='Unit Test', 
                          LanguageLocaleKey='en_US', LocaleSIdKey='en_GB', ProfileId = p.Id,
                          TimezoneSIdKey='Europe/London', Username='unit.test@unit.test.com',
                          Division='Legal', Department='Legal');

        System.runAs(u){
                    JVCO_Venue__c venue = new   JVCO_Venue__c();
                    venue.Name = 'update venue';
                    venue.JVCO_Field_Visit_Requested__c = false;
                    venue.JVCO_Street__c = 'testStreet';
                    venue.JVCO_City__c = 'testCoty';
                    venue.JVCO_Postcode__c ='LL53 2LU';
                    venue.JVCO_Country__c ='UK';

                    insert venue;
        
                    // Check if the Queue exist
                    List<Group> groupToCheckList = [SELECT Id FROM Group WHERE Name LIKE 'Unresolved Addresses%'];

                    // If the Queue does not exist, create the queue
                    if (groupToCheckList.size() < 1)
                    {
                        List<Group> groupList = new List<Group>{
                            (new Group(Name='Unresolved Addresses', type='Queue'))
                        };
                        insert groupList;

                        List<QueueSObject> queueList = new List<QueueSObject>();
                        for (Group g : groupList)
                        {
                            queueList.add(new QueueSObject(QueueID=g.id, SobjectType='Lead'));
                            queueList.add(new QueueSObject(QueueID=g.id, SobjectType='JVCO_Venue__c'));
                        }
                        insert queueList;
                    }

                }
    }
    
    /* ----------------------------------------------------------------------------------------------
   Name: JVCO_VenueAssignFieldAgentsPostLogicTest.cls 
   Description: Test method for insert scenario where request visit is false with postcode populated

   Date         Version     Author                 Summary of Changes 
   -----------  -------     -----------------      -----------------------------------------
  24-Dec-2016   0.1        Robert John B. Lacatan   Creation
  ----------------------------------------------------------------------------------------------- */
     
     @isTest static void testInsertvenue(){
            User u = [SELECT id FROM User WHERE Alias = 'utest'];

            JVCO_Venue__c venue = new   JVCO_Venue__c();
            venue.Name = 'Test Venue';
            venue.JVCO_Field_Visit_Requested__c = false;
            venue.JVCO_Street__c = 'testStreet';
            venue.JVCO_City__c = 'testCoty';
            venue.JVCO_Postcode__c ='AKL1 1234';
            venue.JVCO_Country__c ='UK';

            test.startTest();
            system.runAs(u){
                insert venue;
            }
            test.stopTest();

            JVCO_Venue__c iVenue = [SELECT ownerId from JVCO_Venue__c WHERE Name ='Test Venue'];
            
            //assert that the owner of the record is uTest.
            system.assertEquals(iVenue.ownerId,u.id);
     }


  /* ----------------------------------------------------------------------------------------------
   Name: JVCO_VenueAssignFieldAgentsPostLogicTest.cls 
   Description: Test method for insert scenario where request visit is true with postcode populated

   Date         Version     Author                 Summary of Changes 
   -----------  -------     -----------------      -----------------------------------------
  24-Dec-2016   0.1        Robert John B. Lacatan   Creation
  ----------------------------------------------------------------------------------------------- */
     @isTest static void testInsertvenueVisitRequested(){
            User u = [SELECT id FROM User WHERE Alias = 'utest'];

            JVCO_Venue__c venue = new   JVCO_Venue__c();
            venue.Name = 'Test Venue1';
            venue.JVCO_Field_Visit_Requested__c = true;
            venue.JVCO_Field_Visit_Requested_Date__c = date.today();
            venue.JVCO_Street__c = 'testStreet';
            venue.JVCO_City__c = 'testCoty';
            venue.JVCO_Postcode__c ='BL1 12345';
          //  venue.JVCO_Country__c ='UK';

            test.startTest();
            system.runAs(u){
                insert venue;
            }
            test.stopTest();

            JVCO_Venue__c iVenue = [SELECT ownerId from JVCO_Venue__c WHERE Name ='Test Venue1'];
            
            //assert that the owner of the record is not uTest
            //system.assertNotEquals(iVenue.ownerId,u.id);
     }


  /* ----------------------------------------------------------------------------------------------
   Name: JVCO_VenueAssignFieldAgentsPostLogicTest.cls 
   Description: Test method for insert scenario where request visit is true with postcode populated
                and ownership is assigned to Unresolve Addresses queue

   Date         Version     Author                 Summary of Changes 
   -----------  -------     -----------------      -----------------------------------------
  24-Dec-2016   0.1        Robert John B. Lacatan   Creation
  ----------------------------------------------------------------------------------------------- */
     @isTest static void testInsertvenueVisitRequestedAssignQueue(){
            User u = [SELECT id FROM User WHERE Alias = 'utest'];

            JVCO_Venue__c venue = new   JVCO_Venue__c();
            venue.Name = 'Test Venue1';
            venue.JVCO_Field_Visit_Requested__c = true;
            venue.JVCO_Street__c = 'testStreet';
            venue.JVCO_City__c = 'testCoty';
            venue.JVCO_Postcode__c ='XXX 12345';
            venue.JVCO_Country__c ='UK';

            test.startTest();
            system.runAs(u){
                try{
                    insert venue;
                } catch(Exception e){
                    
                    //assert that record throws an error whenever it is assigned to Unresolved Addresses Queue
                    System.Assert(e.getMessage().contains('The address of this Venue cannot be resolved.'));
                }
                
            }
            test.stopTest();

            
     }
     
     /* ----------------------------------------------------------------------------------------------
   Name: JVCO_VenueAssignFieldAgentsPostLogicTest.cls 
   Description: Test method for update scenario where request visit is true with postcode populated

   Date         Version     Author                 Summary of Changes 
   -----------  -------     -----------------      -----------------------------------------
  24-Dec-2016   0.1        Robert John B. Lacatan   Creation
  ----------------------------------------------------------------------------------------------- */

     @isTest static void testUpdatevenueVisitRequested(){
            User u = [SELECT id FROM User WHERE Alias = 'utest'];

            JVCO_Venue__c venue = [SELECT ownerId from JVCO_Venue__c WHERE Name ='update venue'];
            
            venue.JVCO_Field_Visit_Requested__c = true;
            venue.JVCO_Field_Visit_Requested_Date__c = date.today();
            venue.JVCO_Postcode__c ='BL1 12345';
            

            test.startTest();
            system.runAs(u){
                update venue;
            }
            test.stopTest();

            JVCO_Venue__c iVenue = [SELECT ownerId from JVCO_Venue__c WHERE Name ='update venue'];
            
            //assert that owner was assigned to the user with matching postcode
            //system.assertNotEquals(iVenue.ownerId,u.id);
     }
          
}