/**********************************************************************************
 * @Author: jason.e.mactal
 * @Company: Accenture
 * @Description: Test Class for JVCO_codaPurchaseInvoiceTriggerHandler and JVCO_codaPurchaseInvoiceTrigger
 * @Created Date: 10.25.2017
 * @Revisions:
 *      <Name>              <Date>          <Description>
 *      jason.e.mactal      10.25.2017      Initial version
 * 
 **********************************************************************************/ 
@isTest
private class JVCO_codaPurchaseInvoiceTriggerTest{
    
    @testSetup static void testSetup(){

        JVCO_TestClassHelper.createBillingConfig();
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
    
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        insert nonConvGLA;
        
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2();
        insert JVCO_TestClassHelper.setDim3();

        Test.startTest();
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        insert JVCO_TestClassHelper.setSuppAcc(taxCode.Id, c.Id, accReceivableGLA.Id);
        Test.stopTest();

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
    }

    @isTest
    static void testHoldLogic()
    {   
        Id suppRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = [SELECT Id, c2g__CODAAccountsPayableControl__c
                    FROM Account
                    WHERE RecordTypeId = :suppRecTypeId
                    LIMIT 1];
                    
        c2g__codaPurchaseInvoice__c pInv = new c2g__codaPurchaseInvoice__c();
        pInv.c2g__Account__c = a.Id;
        pInv.c2g__AccountInvoiceNumber__c = '12345';
        insert pInv;
        c2g__codaPurchaseInvoiceExpenseLineItem__c piel = new c2g__codaPurchaseInvoiceExpenseLineItem__c();
        piel.c2g__PurchaseInvoice__c = pInv.Id;
        piel.c2g__GeneralLedgerAccount__c = a.c2g__CODAAccountsPayableControl__c;
        piel.c2g__NetValue__c = 100;
        insert piel;

        c2g.CODAAPICommon_9_0.Context context = new c2g.CODAAPICommon_9_0.Context();
        c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
        ref.Id = pInv.Id;
        c2g.CODAAPIPurchaseInvoice_9_0.PostPurchaseInvoice(context, ref);               
    }

}