/* ----------------------------------------------------------------------------------------------
   Name: JVCO_BulkResetAmendmentHelperTest
   Description: Test Class for JVCO_BulkResetAmendmentHelper

   Date         Version     Author               Summary of Changes 
   -----------  -------     -----------------    -----------------------------------------
   09-Nov-2018   0.1        rhys.j.c.dela.cruz   Intial creation
   20-May-2019   0.2        rhys.j.c.dela.cruz   GREEN-34604 - Update test class
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_BulkResetAmendmentHelperTest{
        
        @testSetup static void setupTestData(){

                Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;
        
        Product2 prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.JVCO_GenerateReviewQuotesLastSubmitted__c = System.now();
        insert acc2;

        Opportunity o = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Amendment';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = acc2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        q.JVCO_AmendmentReset__c = false;
        insert q;

        SBQQ__QuoteLineGroup__c qlg = new SBQQ__QuoteLineGroup__c();
        qlg.SBQQ__Quote__c = q.Id;
        qlg.SBQQ__NetTotal__c = 12345;
        qlg.SBQQ__ListTotal__c = 12345;
        qlg.SBQQ__CustomerTotal__c = 12345;
        insert qlg;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        qL.SPVAvailability__c = true;
        ql.Terminate__c = false;
        ql.JVCO_Steelbrick_Last_Recalculated_Time__c = System.now();
        ql.SBQQ__Group__c = qlg.Id;
        insert ql;

        q.Recalculate__c = true;
        update q;

        JVCO_Constants__c constants = JVCO_Constants__c.getOrgDefaults();
        constants.JVCO_KABulkResetBufferMins__c = 5;
        constants.JVCO_Quote_Recalculation_Time_Buffer__c = 10;
        upsert constants JVCO_Constants__c.Id;
        }

        @isTest static void runResetTest(){
                
                Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

                List<Account> accountList = new List<Account>([SELECT Id, JVCO_GenerateReviewQuotesLastSubmitted__c, JVCO_Customer_Account__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1]);

                Test.startTest();

                User keyAccMngr = JVCO_TestClassObjectBuilder.createUser('Key Accounts Manager');
            insert keyAccMngr;

                System.runAs(keyAccMngr){
                        ApexPages.StandardController standardController = new ApexPages.StandardController(accountList[0]);
                JVCO_BulkResetAmendmentHelper controller = new JVCO_BulkResetAmendmentHelper(standardController);

                controller.updateQuoteLines();
                controller.backToAcc();
                }

        Test.stopTest();
        }
        
        @isTest static void runReset2(){
                
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

        List<Account> accountList = new List<Account>([SELECT Id, JVCO_GenerateReviewQuotesLastSubmitted__c, JVCO_Customer_Account__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1]);

        JVCO_BulkResetAmendmentHelper.testError = true;

        Test.startTest();
        JVCO_BulkResetAmendmentHelper.runReset(accountList[0].Id);
        Test.stopTest();
        }

    @isTest static void runReset3(){
            
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

        List<Account> accountList = new List<Account>([SELECT Id, JVCO_GenerateReviewQuotesLastSubmitted__c, JVCO_Customer_Account__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1]);

        JVCO_BulkResetAmendmentHelper.testError2 = true;

        Test.startTest();
        JVCO_BulkResetAmendmentHelper.runReset(accountList[0].Id);
        Test.stopTest();
    }

    @isTest static void runReset4(){
            
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

        List<Account> accountList = new List<Account>([SELECT Id, JVCO_GenerateReviewQuotesLastSubmitted__c, JVCO_Customer_Account__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1]);

        SBQQ__QuoteLineGroup__c qlg = [SELECT Id, SBQQ__ListTotal__c, SBQQ__CustomerTotal__c, SBQQ__NetTotal__c, SBQQ__Quote__r.SBQQ__NetAmount__c FROM SBQQ__QuoteLineGroup__c LIMIT 1];

        qlg.SBQQ__ListTotal__c = 12345;
        qlg.SBQQ__CustomerTotal__c = 12345;
        qlg.SBQQ__NetTotal__c = 12345;

        update qlg;

        Test.startTest();
        JVCO_BulkResetAmendmentHelper.runReset(accountList[0].Id);
        Test.stopTest();
    }
}