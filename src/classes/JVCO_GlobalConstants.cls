/**
* @author Recuerdo B. Bregente
* @date 11/09/2017
* @description Repository for Constant variables.
*/
Public Without Sharing Class JVCO_GlobalConstants{

    /*******************************************************************************************************
    * @description Constant variables for Matching Background taken from Custom Settings
    ********************************************************************************************************/
    public static final String DEFAULT_TRADING_CURRENCY         = (JVCO_BackgroundMatching__c.getValues('Trading Currency') <> null)? JVCO_BackgroundMatching__c.getValues('Trading Currency').JVCO_Default_Value__c : null;
    public static final String DEFAULT_RECEIVABLE_CONTROL       = (JVCO_BackgroundMatching__c.getValues('Receivable Control') <> null)? JVCO_BackgroundMatching__c.getValues('Receivable Control').JVCO_Default_Value__c : null;
    public static final String DEFAULT_TAX_CODE                 = (JVCO_BackgroundMatching__c.getValues('Output Vat Code') <> null)? JVCO_BackgroundMatching__c.getValues('Output Vat Code').JVCO_Default_Value__c : null;
    
    
    /*******************************************************************************************************
    * @description Constant variables for ACCOUNT recordtype IDs
    ********************************************************************************************************/
    public static final String ACCOUNT_CUSTOMER_RECORDTYPE_ID   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
    public static final String ACCOUNT_LICENCE_RECORDTYPE_ID    = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    public static final String ACCOUNT_SUPPLIER_RECORDTYPE_ID   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
}