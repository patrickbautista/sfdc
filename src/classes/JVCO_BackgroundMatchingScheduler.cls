public class JVCO_BackgroundMatchingScheduler implements Schedulable
{
    public void execute(SchedulableContext sc)
    {
		Database.executeBatch(new JVCO_CreditNoteBackgroundMatchingBatch(), 1);
    }
}