@isTest
public class JVCO_InvoiceAndCreditNoteManualLogicTest
{
    @testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        //JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;

        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        List<c2g__codaDimension2__c> dim2List = new List<c2g__codaDimension2__c>();
        dim2List.add(JVCO_TestClassHelper.setDim2('PPL'));
        dim2List.add(JVCO_TestClassHelper.setDim2('PRS'));
        dim2List.add(JVCO_TestClassHelper.setDim2('VPL'));
        insert dim2List;

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        Test.startTest();
        List<Product2> pList = new List<Product2>();
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        pList.add(p);
        Product2 pPRS = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        pPRS.Name = 'PRS Product';
        pList.add(pPRS);
        Product2 pVPL = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        pVPL.Name = 'VPL Product';
        pList.add(pVPL);
        insert pList;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;

        //Order for Sales Invoice
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.SBQQ__Quote__c = null;
        o.JVCO_DueDate__c = Date.today();
        insert o;
        insert JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        
        //Order for Sales Credit Note
        Order o2 = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o2.SBQQ__Quote__c = null;
        o2.JVCO_DueDate__c = Date.today();
        insert o2;
        OrderItem oi2 = JVCO_TestClassHelper.setOrderItem(o2.Id, pbe.Id, ql.Id);
        oi2.UnitPrice = -100;
        insert oi2;
        
        JVCO_TestClassHelper.setGeneralSettingsCS();
        Test.stopTest();
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        
    }

    @isTest
    static void testManualSalesCreditNote()
    {
        Order o = [SELECT Id, JVCO_Order_Status__c, Status FROM Order WHERE JVCO_Total_Amount__c < 0 LIMIT 1];
        o.JVCO_Order_Status__c = 'Posted';
        o.Status = 'Activated';
        o.JVCO_Manual_Invoice__c = true;
        
        Test.startTest();
        update o;
        Test.stopTest();
    }

    @isTest
    static void testManualSalesInvoice()
    {
        Order o = [SELECT Id, JVCO_Order_Status__c, Status FROM Order WHERE JVCO_Total_Amount__c > 0 LIMIT 1];
        o.JVCO_Order_Status__c = 'Posted';
        o.Status = 'Activated';
        o.JVCO_Manual_Invoice__c = true;
        
        Test.startTest();
        update o;
        Test.stopTest();
    }
}