/* ----------------------------------------------------------------------------------------------------------
Name:            JVCO_FestivalTriggerHandlerTest.cls
Description:     TEst class for JVCO_FestivalTriggerHandler
     

Date             Version     Author                            Summary of Changes 
-----------      -------     -----------------           -------------------------------------------
06-Apr-2018      0.1        robert.j.b.lacatan              Initial version of code
---------------------------------------------------------------------------------------------------------- */


@isTest
private class JVCO_FestivalTriggerHandlerTest{

    @testSetup
    private static void setUpData(){
    
    JVCO_Festival__c festival1 = new JVCO_Festival__c();
    festival1.Name = 'Test Festival';
    festival1.JVCO_Start_Date__c = Date.newInstance(2016,01,01);
    festival1.JVCO_End_Date__c = Date.newInstance(2016,02,01);
    
    insert festival1;
    
    }
    
    @isTest
    static void testUpdate(){
    
    JVCO_Festival__c fest = [Select id,JVCO_Start_Date__c,JVCO_End_Date__c from JVCO_Festival__c limit 1];
    update fest;
    
    fest = [Select id, JVCO_Start_Date__c, JVCO_End_Date__c, JVCO_StartDate_Text__c from JVCO_Festival__c limit 1];
    system.AssertEquals(fest.JVCO_StartDate_Text__c, string.valueof(fest.JVCO_Start_Date__c.Day()) + '/' + string.valueof(fest.JVCO_Start_Date__c.Month()) + '/' + string.valueof(fest.JVCO_Start_Date__c.Year()));
    
    }
    
    @isTest
    static void bulkTest(){
    list<JVCO_Festival__c> lFest = new list<JVCO_Festival__c>();
    JVCO_Festival__c fest1 = new JVCO_Festival__c(Name='Test1',JVCO_Start_Date__c = Date.newInstance(2016,03,01), JVCO_End_Date__c = Date.newInstance(2016,05,01));
    JVCO_Festival__c fest2 = new JVCO_Festival__c(Name='Test2',JVCO_Start_Date__c = Date.newInstance(2016,08,01), JVCO_End_Date__c = Date.newInstance(2016,11,01));
    lFest.add(fest1);
    lFest.add(fest2);
    
    
    JVCO_Festival__c fest = [Select id,JVCO_Start_Date__c,JVCO_End_Date__c from JVCO_Festival__c limit 1];
    fest.JVCO_Start_Date__c = Date.newInstance(2016,01,06);
    lFest.add(fest);
    
    upsert lFest;
    
    fest = [Select id, JVCO_Start_Date__c, JVCO_End_Date__c, JVCO_StartDate_Text__c from JVCO_Festival__c limit 1];
    system.AssertEquals(fest.JVCO_StartDate_Text__c, string.valueof(fest.JVCO_Start_Date__c.Day()) + '/' + string.valueof(fest.JVCO_Start_Date__c.Month()) + '/' + string.valueof(fest.JVCO_Start_Date__c.Year()));
    
   
    
    }
    

}