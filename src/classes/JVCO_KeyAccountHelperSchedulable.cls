/* ------------------------------------------------------------------------------------------------
   Name:            JVCO_KeyAccountHelperSchedulable
   Description:     Schedule Helper for KA Queueable Jobs

   Date            Version     Author            		    Summary of Changes 
   -----------     -------     -----------------            ---------------------------------------
   19-Mar-2019     0.1         rhys.j.c.dela.cruz	 		Initial Version
------------------------------------------------------------------------------------------------ */
global class JVCO_KeyAccountHelperSchedulable implements Schedulable {

	global Map<Id, Opportunity> opportunityMap;
	global Account accountRecord;
	global Integer quoteCount;
	global Integer errorQuoteCount;
	global String queueableErrorMessage;
	global Set<Id> opportunityIdSet;
	global Integer opportunityOriginalSize;
	global Integer qblTempCtr;
	global String queueableType;

	//Generate Quotes
    global Map<Id, Contract> contractMap;
    global Set<Id> contractIds;

	global Map<String, String> quoteStringMap;
	global Integer tempOriginalSize;
	global Boolean isReview;

	//Contract Review/Renew
	public JVCO_KeyAccountHelperSchedulable(Map<Id, Opportunity> opportunityMap, Account accountRecord, Integer quoteCount, Integer errorQuoteCount, String queueableErrorMessage, Set<Id> opportunityIdSet, Integer opportunityOriginalSize, String qType){

		this.opportunityMap = opportunityMap;
		this.accountRecord = accountRecord;
		this.quoteCount = quoteCount;
		this.errorQuoteCount = errorQuoteCount;
		this.queueableErrorMessage = queueableErrorMessage;
		this.opportunityIdSet = opportunityIdSet;
		this.opportunityOriginalSize = opportunityOriginalSize;
		qblTempCtr = 0;
		queueableType = qType;
	}

	//Generate Review/Renew
    public JVCO_KeyAccountHelperSchedulable(Map<Id, Contract> contractMap, Set<Id> contractIds, Account accountRecord, String queueableErrorMessage, Integer quoteCount, Integer errorQuoteCount, String qType){

        this.contractMap = contractMap;
        this.contractIds = contractIds;
        this.accountRecord = accountRecord;
        this.queueableErrorMessage = queueableErrorMessage;
        this.quoteCount = quoteCount;
        this.errorQuoteCount = errorQuoteCount;
        qblTempCtr = 0;
        queueableType = qType;
    }   

	//Order Quotes
	public JVCO_KeyAccountHelperSchedulable(Map<Id, Opportunity> opportunityMap, Account accountRecord, Map<String, String> quoteStringMap, Integer tempOriginalSize, Boolean isReview, String qType){

		this.opportunityMap = opportunityMap;
		this.accountRecord = accountRecord;
		this.quoteStringMap = quoteStringMap;
		this.tempOriginalSize = tempOriginalSize;
		this.isReview = isReview;
		qblTempCtr = 0;
		queueableType = qType;
	}

	global void execute(SchedulableContext sc) {
		
		CronTrigger[] cT = [SELECT Id, CronJobDetail.Name FROM CronTrigger where id =: sc.getTriggerId() LIMIT 1];

		if(cT.size() > 0){

			String cronJobName = cT[0].CronJobDetail.Name;
			System.debug(cronJobName);

			Id queueableId = null;
			JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
			kaTempRec.JVCO_ErrorMessage__c = '';

			if(queueableType == 'ContractReview'){

				if(!Test.isRunningTest()){

					queueableId = System.enqueueJob(new JVCO_ContractReviewQuotes_Queueable(opportunityMap, accountRecord, quoteCount, errorQuoteCount, queueableErrorMessage, opportunityIdSet, opportunityOriginalSize, qblTempCtr));	
				}

                kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractRenew';
                kaTempRec.JVCO_JobId__c = queueableId;

                upsert kaTempRec Name;
			}
			else if(queueableType == 'ContractRenew'){

				if(!Test.isRunningTest()){

					queueableId = System.enqueueJob(new JVCO_ContractRenewQuotes_Queueable(opportunityMap, accountRecord, quoteCount, errorQuoteCount, queueableErrorMessage, opportunityIdSet, opportunityOriginalSize, qblTempCtr));
				}

                kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractRenew';
                kaTempRec.JVCO_JobId__c = queueableId;

                upsert kaTempRec Name;
			}
			else if(queueableType == 'GenerateRenewals'){

                if(!Test.isRunningTest()){

                    queueableId = System.enqueueJob(new JVCO_GenerateRenewalQuotes_Queueable(contractMap, contractIds, accountRecord, queueableErrorMessage, quoteCount, errorQuoteCount, qblTempCtr));
                }

                kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':GenerateRenewals';
                kaTempRec.JVCO_JobId__c = queueableId;

                upsert kaTempRec Name;
            }
			else if(queueableType == 'OrderQuotes'){

				if(!Test.isRunningTest()){

					queueableId = System.enqueueJob(new JVCO_OrderQuotes_Queueable(opportunityMap, accountRecord, quoteStringMap, tempOriginalSize, isReview, qblTempCtr));
				}

                if(isReview){
                    kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractReview';
                }
                else{
                    kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractRenew';   
                }
                kaTempRec.JVCO_JobId__c = queueableId;

                upsert kaTempRec Name;
			}

			System.abortJob(sc.getTriggerId());
		}
	}
}