public class JVCO_RunCreditNoteBatch{

    final JVCO_General_Settings__c GeneralSettings = JVCO_General_Settings__c.getInstance();
    final Decimal cancelCredBatchSize = GeneralSettings != null ? GeneralSettings.JVCO_Cancel_Credit_Note_batch_size__c : 100;
    
    c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
    public c2g__codaCreditNote__c cNoteRec {get;set;}
    public Boolean isButtonVisible {get;set;}
    
    public JVCO_RunCreditNoteBatch(ApexPages.StandardController stdController){
        
        this.cNote = (c2g__codaCreditNote__c)stdController.getRecord();
        isButtonVisible = true;
        cNoteRec = [SELECT Id, Name, c2g__Account__c,
                    JVCO_Credit_Note_Cancelled__c,
                    JVCO_Credit_Reason__c,
                    c2g__CreditNoteStatus__c, 
                    c2g__PaymentStatus__c,
                    JVCO_Period_Start_Date__c,      
                    JVCO_Period_End_Date__c,
                    JVCO_Sales_Rep__c,
                    CreatedDate
                    FROM c2g__codaCreditNote__c WHERE Id =: cNote.Id LIMIT 1];
        if(cNoteRec.JVCO_Credit_Note_Cancelled__c 
           || cNoteRec.JVCO_Credit_Reason__c != 'Invoice amendment'
           || cNoteRec.CreatedDate.year() != System.today().year())
        {
            isButtonVisible = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.error, Label.JVCO_CancelCreditErrorMessage));
        }
    }
    
    public PageReference runConvert(){
        Database.executeBatch(new JVCO_CreditCancellationLogic(cNoteRec), Integer.valueOf(cancelCredBatchSize));
        return new PageReference('/' + cNoteRec.Id);
    }

    public PageReference returnTocNote(){
    
        PageReference cNotePage = new PageReference('/' + cNoteRec.Id);
        
        return cNotePage;
    }
}