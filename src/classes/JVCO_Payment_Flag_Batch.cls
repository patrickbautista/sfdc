/* ----------------------------------------------------------------------------------------------
    Name: JVCO_Payment_Flag_Batch
    Description: Batch for DD Payment Docgen

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    13-Mar-2018  0.1         franz.g.a.dimaapi     Intial creation
    24-Mar-2018  0.2         franz.g.a.dimaapi     Changed Criteria of Batch to improve performance
    04-Dec-2018  0.3         franz.g.a.dimaapi     GREEN-34103 - Add Switch for Exception Document
    09-Dec-2019  0.4         franz.g.a.dimaapi     GREEN-35057 - Added DD Payee Contact Filter
----------------------------------------------------------------------------------------------- */
public class JVCO_Payment_Flag_Batch implements Database.Batchable<sObject>
{

    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
        return Database.getQueryLocator([SELECT Id
                                        FROM PAYREC2__Payment_Agreement__c
                                        WHERE PAYREC2__Account__r.ffps_custRem__Preferred_Communication_Channel__c IN ('Print', 'Email')
                                        AND PAYREC2__Account__r.JVCO_DD_Payee_Contact__c != NULL
                                        AND Id IN (SELECT PAYREC2__Payment_Agreement__c
                                                    FROM PAYBASE2__Payment__c 
                                                    WHERE PAYBASE2__Status_Text__c IN ('New Instruction', 'Cancelled')
                                                    AND JVCO_Processed_By_Batch__c = FALSE
                                                    AND PAYBASE2__Created_From_Account__c != NULL
                                                    AND PAYREC2__Payment_Agreement__c != NULL)
                                        ]);
    }

    public void execute(Database.BatchableContext BC, List<PAYREC2__Payment_Agreement__c> paymentAgreement) 
    {   
        List<PAYBASE2__Payment__c> paymentList = [SELECT Id, JVCO_Processed_By_Batch__c,
                                                    PAYBASE2__Status_Text__c,
                                                    PAYBASE2__Created_From_Account__r.JVCO_Send_Direct_Debit_Reminder__c,
                                                    PAYBASE2__Created_From_Account__r.ffps_custRem__Preferred_Communication_Channel__c,
                                                    PAYREC2__Payment_Agreement__r.JVCO_Generate_DD_Doc_Flag__c, 
                                                    PAYREC2__Payment_Agreement__r.JVCO_Ready_For_DD_Doc_Gen__c,
                                                    PAYREC2__Payment_Agreement__r.JVCO_DD_Confirmation_Exception__c 
                                                    FROM PAYBASE2__Payment__c 
                                                    WHERE JVCO_Processed_By_Batch__c = FALSE
                                                    AND PAYBASE2__Created_From_Account__c != NULL
                                                    AND PAYBASE2__Status_Text__c IN ('New Instruction','Cancelled') 
                                                    AND PAYREC2__Payment_Agreement__c IN : paymentAgreement
                                                    ORDER BY ID];
        paymentFlagLogic(paymentList);
    }

    private void paymentFlagLogic(List<PAYBASE2__Payment__c> paymentList)
    {
        Set<Account> updatedAccSet = new Set<Account>();
        Map<Id, PAYREC2__Payment_Agreement__c> updatedPaymentAgreementMap = new Map<Id, PAYREC2__Payment_Agreement__c>();
        for(PAYBASE2__Payment__c payment : paymentList)
        {   
            if(!JVCO_DocumentGenerationWrapper.generalSettings.JVCO_Disable_Exception_Document__c || !payment.PAYREC2__Payment_Agreement__r.JVCO_DD_Confirmation_Exception__c)
            {
                //Account Communication Channel
                String DOCTYPE = payment.PAYBASE2__Created_From_Account__r.ffps_custRem__Preferred_Communication_Channel__c;
                //Payment Agreement
                PAYREC2__Payment_Agreement__c paymentAgreement = new PAYREC2__Payment_Agreement__c();
                paymentAgreement.Id = payment.PAYREC2__Payment_Agreement__c;
                paymentAgreement.JVCO_Generate_DD_Doc_Flag__c = payment.PAYBASE2__Status_Text__c == 'New Instruction' ? 'New Instruction (' + DOCTYPE + ')' : 'Canceled (' + DOCTYPE + ')';
                paymentAgreement.JVCO_Ready_For_DD_Doc_Gen__c = (payment.PAYBASE2__Status_Text__c == 'New Instruction');
                updatedPaymentAgreementMap.put(paymentAgreement.Id, paymentAgreement);

                //Account
                if(payment.PAYBASE2__Created_From_Account__r.JVCO_Send_Direct_Debit_Reminder__c)
                {
                    Account a = new Account();
                    a.Id = payment.PAYBASE2__Created_From_Account__c;
                    a.JVCO_Send_Direct_Debit_Reminder__c = false;
                    updatedAccSet.add(a);
                }
            }

            //Payment
            payment.JVCO_Processed_By_Batch__c = true;
        }

        update paymentList;
        update updatedPaymentAgreementMap.values();
        update new List<Account>(updatedAccSet);
    }

    public void finish(Database.BatchableContext BC) 
    {

    }

}