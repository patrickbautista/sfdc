/* ----------------------------------------------------------------------------------------------
Name: JVCO_codaBankStatementHandler.cls 
Description: Handler class for c2g__codaBankStatement__c object

Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
28-Jul-2017  0.1         kristoffer.d.martin   Intial creation
25-Oct-2017  0.2         franz.g.a.dimaapi     Refactor Code used Queueable - GREEN-24992
19-Dec-2017  0.3         chun.yin.tang         Add disabling trigger to Bank Statement Handler
26-Jun-2019  0.4         franz.g.a.dimaapi     GREEN-34731 - Refactor
22-Jun-2020  0.5         patrick.t.bautista    GREEN-35648 - added JVCO_Exclude to filter
18-Nov-2020  0.6         patrick.t.bautista    GREEN-36082 - Refactor to new queueable for invoice update
----------------------------------------------------------------------------------------------- */
public class JVCO_codaBankStatementHandler
{
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_codaBankStatementTrigger__c  : false;
    
    public static void isBeforeUpdate(List<c2g__codaBankStatement__c> newBankStatementList, Map<Id, c2g__codaBankStatement__c> oldBankStatementMap)
    {
        if(!skipTrigger) 
        {
            updateBankStatementStatus(newBankStatementList, oldBankStatementMap);
        }
    }
    
    public static void isAfterUpdate(List<c2g__codaBankStatement__c> bankStatementList)
    {
        if(!skipTrigger) 
        {
            Set<Id> bsIdSet = new Set<Id>();
            for(c2g__codaBankStatement__c bs : bankStatementList)
            {
                if(bs.JVCO_Bank_Transfer__c)
                {
                    bsIdSet.add(bs.Id);
                }
            }
            
            //Bank Transfer Logic
            if(!bsIdSet.isEmpty())
            {
                createCashEntryFromBankstatementLine(bsIdSet);
            }
        }
    }
    
    private static void updateBankStatementStatus(List<c2g__codaBankStatement__c> newBankStatementList, Map<Id, c2g__codaBankStatement__c> oldBankStatementMap)
    {
        for(c2g__codaBankStatement__c bankStatement : newBankStatementList)
        {
            if(bankStatement.JVCO_Bank_Transfer__c)
            {
                if(checkIfUserHasPendingBankStatementQueueable())
                {
                    bankStatement.addError('Bank Statement Queueable is already In Progress');
                }else if(bankStatement.JVCO_Status__c == 'Completed')
                {
                    bankStatement.addError('Bank Statement Queueable is already Completed');
                }else
                {
                    bankStatement.JVCO_Status__c = 'Started';
                    bankStatement.JVCO_Bank_Statement_Queueable_Completed__c = false;
                }
            }else if(bankStatement.JVCO_Total_Cash_Entries_Processed__c > 0 && 
                     (bankStatement.JVCO_Total_Cash_Entries_Processed__c != oldBankStatementMap.get(bankStatement.Id).JVCO_Total_Cash_Entries_Processed__c) ||
                     bankStatement.JVCO_Bank_Statement_Queueable_Completed__c)
            {
                if(bankStatement.JVCO_Total_Cash_Entries_Processed__c == bankStatement.JVCO_Total_Bank_Statement_Lines__c)
                {
                    bankStatement.JVCO_Status__c = 'Completed';
                }else
                {
                    bankStatement.JVCO_Status__c = bankStatement.JVCO_Bank_Statement_Queueable_Completed__c ? 'Completed with Failure' : 'In Progress';
                }
            }
        }
    }
    
    private static void createCashEntryFromBankstatementLine(Set<Id> bsIdSet)
    {
        List<c2g__codaBankStatementLineItem__c> bsLineList = getBankStatmentLineList(bsIdSet);
        List<c2g__codaCashEntry__c> updatedCashEntryList = new List<c2g__codaCashEntry__c>();
        
        Map<Id, String> bslineToAcctRefMap = new Map<Id, String>();
        for(c2g__codaBankStatementLineItem__c bsLine : bsLineList)
        {   
            // Create a new cash entry record
            c2g__codaCashEntry__c cashEntry = new c2g__codaCashEntry__c();
            cashEntry.c2g__Date__c = bsLine.c2g__Date__c;
            cashEntry.c2g__Account__c = bsLine.JVCO_Account__c;
            cashEntry.c2g__Value__c = bsLine.c2g__Amount__c;
            cashEntry.c2g__BankAccountValue__c = bsLine.c2g__Amount__c;
            cashEntry.c2g__NetValue__c = bsLine.c2g__Amount__c;
            cashEntry.c2g__Type__c = 'Receipt';
            cashEntry.c2g__PaymentMethod__c = 'Electronic';
            cashEntry.JVCO_Bank_Statement_Line_Item__c = bsLine.Id;
            cashEntry.c2g__BankAccount__c = bsLine.JVCO_Bank_Account__c;
            cashEntry.ffcash__DerivePeriod__c = TRUE;
            updatedCashEntryList.add(cashEntry);
            bslineToAcctRefMap.put(bsLine.Id, bsLine.JVCO_SalesInvoice__r.Name);
        }
        
        if(!bslineToAcctRefMap.isEmpty())
        {
            //Create Cash Entry/Lines
            Id bankStatementId = new List<Id>(bsIdSet)[0];
            Id jobId = System.enqueueJob(new JVCO_BankStatementLogicQueueable(updatedCashEntryList, bslineToAcctRefMap, bankStatementId));
        }
    }
    
    private static List<c2g__codaBankStatementLineItem__c> getBankStatmentLineList(Set<Id> bsIdSet)
    {
        return [SELECT Id, c2g__Date__c, c2g__Amount__c,
                JVCO_Account__c, JVCO_Bank_Account__c, 
                JVCO_SalesInvoice__r.Name
                FROM c2g__codaBankStatementLineItem__c 
                WHERE c2g__BankStatement__c IN :bsIdSet
                AND JVCO_Cash_Entry__c = NULL
                AND JVCO_Exclude__c = false];
    }
    
    private static Boolean checkIfUserHasPendingBankStatementQueueable()
    {
        List<AsyncApexJob> asynchApexJobList = [SELECT ID
                                                FROM AsyncApexJob
                                                WHERE ApexClass.Name = 'JVCO_BankStatementLogicQueueable'
                                                AND CreatedById = :UserInfo.getUserId()
                                                AND Status IN ('Holding','Queued','Preparing','Processing')];
        return !asynchApexJobList.isEmpty();
    }
}