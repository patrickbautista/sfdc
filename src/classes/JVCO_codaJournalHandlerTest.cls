@isTest
public class JVCO_codaJournalHandlerTest {
    @testSetup static void setupTestDate(){
        JVCO_TestClassHelper.createBillingConfig();
        
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        insert dt;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        Account licAcc3 = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        licAcc3.AccountNumber = '832022';
        LicAcc3.JVCO_Live__c = true;
        insert licAcc3; 
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        Test.startTest();
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        JVCO_Venue__c ven = JVCO_TestClassHelper.createVenue();
        insert ven;
        
        JVCO_Event__c event = new JVCO_Event__c();
        event.JVCO_Venue__c = ven.id;
        event.JVCO_Tariff_Code__c = 'LC';
        event.JVCO_Event_Type__c = 'Concert - Qualifies for Old Rate';
        event.JVCO_Event_Start_Date__c = date.today();
        event.JVCO_Event_End_Date__c = date.today() + 7;
        event.License_Account__c=licAcc3.id;
        event.Headline_Type__c = 'No Headliner';
        event.JVCO_Number_of_Set_Lists_Expected__c = 2;
        insert event;
        
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        ql.JVCO_Event__c = event.id;
        ql.JVCO_Venue__c = ven.id;
        insert ql;
        
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        
        c2g__codaInvoice__c invoice = JVCO_TestClassHelper.getSalesInvoice(licAcc.id);
        insert invoice;
        
        JVCO_TestClassHelper.setGeneralSettingsCS();
        Test.stopTest();
    }
    @isTest
    static void createJournalandJournalline()
    {
        c2g__codaAccountingCurrency__c accCurrency = [select id from c2g__codaAccountingCurrency__c limit 1];
        c2g__codaGeneralLedgerAccount__c gla = [select id from c2g__codaGeneralLedgerAccount__c limit 1];
        c2g__codaCompany__c comp = [select id from c2g__codaCompany__c limit 1];
        c2g__codaInvoice__c invoice = [select id, Name from c2g__codaInvoice__c limit 1];
        test.startTest();
        
        c2g__codaJournal__c journal = new c2g__codaJournal__c();
        journal.c2g__JournalDate__c = date.today();
        journal.c2g__Type__c = 'Manual Journal';
        journal.c2g__JournalDescription__c = 'Test';
        journal.c2g__DeriveCurrency__c = true;
        journal.c2g__DerivePeriod__c = true;
        journal.c2g__OwnerCompany__c = comp.id;
        journal.c2g__JournalCurrency__c = accCurrency.id;
        insert journal;
       
        c2g__codaJournalLineItem__c journalline = new c2g__codaJournalLineItem__c();
        journalline.c2g__LineType__c = 'General Ledger Account';
        journalline.c2g__LineNumber__c = 1;
        journalline.c2g__Journal__c = journal.id;
        journalline.c2g__Value__c = 1;
        journalline.c2g__GeneralLedgerAccount__c = gla.id;
        journalline.c2g__LineDescription__c = invoice.Name;
        insert journalline;
        system.debug('journalline ss'+journalline);
        system.debug('journaline name'+journalline.c2g__LineDescription__c);
        test.stopTest();
    }
    @isTest
    static void updateJournal()
    {
        c2g__codaAccountingCurrency__c accCurrency = [select id from c2g__codaAccountingCurrency__c limit 1];
        c2g__codaGeneralLedgerAccount__c gla = [select id from c2g__codaGeneralLedgerAccount__c limit 1];
        c2g__codaCompany__c comp = [select id from c2g__codaCompany__c limit 1];
        c2g__codaInvoice__c invoice = [select id, Name from c2g__codaInvoice__c limit 1];
        
        c2g__codaJournal__c journal = new c2g__codaJournal__c();
        journal.c2g__JournalDate__c = date.today();
        journal.c2g__Type__c = 'Manual Journal';
        journal.c2g__JournalDescription__c = 'Test';
        journal.c2g__DeriveCurrency__c = true;
        journal.c2g__DerivePeriod__c = true;
        journal.c2g__OwnerCompany__c = comp.id;
        journal.c2g__JournalCurrency__c = accCurrency.id;
        insert journal;
       
        c2g__codaJournalLineItem__c journalline = new c2g__codaJournalLineItem__c();
        journalline.c2g__LineType__c = 'General Ledger Account';
        journalline.c2g__LineNumber__c = 1;
        journalline.c2g__Journal__c = journal.id;
        journalline.c2g__Value__c = 1;
        journalline.c2g__GeneralLedgerAccount__c = gla.id;
        journalline.c2g__LineDescription__c = invoice.Name;
        insert journalline;
        system.debug('journalline ss'+journalline);
        system.debug('journaline name'+journalline.c2g__LineDescription__c);
         User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Licensing User'].Id,
            LastName = 'last',
            Email = 'sample000@email.com',
            Username = 'puser000@email.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Division='Legal',
            Department='Legal'
        );
        test.startTest();
        System.runAs(u)
        {
            try{
                update journal;
            }
            catch(Exception ex){
                 Boolean expectedExceptionThrown =  ex.getMessage().contains('Can\'t update a record');
			     System.AssertEquals(expectedExceptionThrown, true);
            }
        }
        test.stopTest();
    }
    @isTest
    static void updateJournalline()
    {
        c2g__codaAccountingCurrency__c accCurrency = [select id from c2g__codaAccountingCurrency__c limit 1];
        c2g__codaGeneralLedgerAccount__c gla = [select id from c2g__codaGeneralLedgerAccount__c limit 1];
        c2g__codaCompany__c comp = [select id from c2g__codaCompany__c limit 1];
        c2g__codaInvoice__c invoice = [select id, Name from c2g__codaInvoice__c limit 1];
        
        c2g__codaJournal__c journal = new c2g__codaJournal__c();
        journal.c2g__JournalDate__c = date.today();
        journal.c2g__Type__c = 'Manual Journal';
        journal.c2g__JournalDescription__c = 'Test';
        journal.c2g__DeriveCurrency__c = true;
        journal.c2g__DerivePeriod__c = true;
        journal.c2g__OwnerCompany__c = comp.id;
        journal.c2g__JournalCurrency__c = accCurrency.id;
        insert journal;
       
        c2g__codaJournalLineItem__c journalline = new c2g__codaJournalLineItem__c();
        journalline.c2g__LineType__c = 'General Ledger Account';
        journalline.c2g__LineNumber__c = 1;
        journalline.c2g__Journal__c = journal.id;
        journalline.c2g__Value__c = 1;
        journalline.c2g__GeneralLedgerAccount__c = gla.id;
        journalline.c2g__LineDescription__c = invoice.Name;
        insert journalline;
        system.debug('journalline ss'+journalline);
        system.debug('journaline name'+journalline.c2g__LineDescription__c);
         User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Licensing User'].Id,
            LastName = 'last',
            Email = 'sample000@email.com',
            Username = 'puser000@email.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Division='Legal',
            Department='Legal'
        );
        test.startTest();
        System.runAs(u)
        {
            try{
                update journalline;
            }
            catch(Exception ex){
                 Boolean expectedExceptionThrown =  ex.getMessage().contains('Can\'t update a record');
			     System.AssertEquals(expectedExceptionThrown, true);
            }
        }
        test.stopTest();
    }
}