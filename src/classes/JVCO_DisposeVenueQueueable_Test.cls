@isTest
private class JVCO_DisposeVenueQueueable_Test 
{

	@testSetup 
  	static void init() 
  	{
	    
  	}

  	@isTest
  	static void confirmDisposal_Test() 
  	{
  		JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_LeadTrigger__c = true;
        insert dt;

  		JVCO_VenueTransfer vt = new JVCO_VenueTransfer();
	    Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
	    Map<String, Date> mapTempIdJVCOAffiliationDate = new Map<String, Date>();
    	Map<String, String> mapTempIdJVCOAffiliationCloseReason = new Map<String, String>();
    	Map<String, String> mapTempIdJVCOAffiliationAccId = new Map<String, String>();
	    
	    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
	    Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
	    
	    Test.startTest();

	    List<Account> customerAccounts = [select id, name from Account where RecordTypeId = :customerRT];
	    List<Account> licenceAccounts = [select id, name from Account where RecordTypeId = :licenceRT];

	   // system.assertEquals(null, customerAccounts,'not equals');
	    
	    Account a1 = new Account();
	    a1.RecordTypeId = customerRT;
	    a1.Name = 'Test Customer0822201 Account';
	    a1.Type = 'Key Account';
	    insert a1;
	    
	    Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
	    a2.RecordTypeId = licenceRT;
	    a2.Name = 'Test Licence Account';
	    a2.JVCO_Customer_Account__c = a1.Id;
	    insert a2;
	    
	    List<JVCO_Venue__c> venList = new List<JVCO_Venue__c>();
	    List<JVCO_Affiliation__c> affList = new List<JVCO_Affiliation__c>();
        String cat = 'TEST';
        for(Integer i=1; i<5; i++) 
        {
            JVCO_Venue__c ven = new JVCO_Venue__c();
            ven.Name = 'Venue-'+cat+i;
            ven.JVCO_Lead_Source__c = 'Churn';
            //ven.JVCO_Venue_Type__c = 'Activity Centre'; 
            //ven.JVCO_Venue_Sub_Type__c = 'Artificial Ski Centre';
            //ven.JVCO_Budget_Category__c = 'General Purpose';
            //ven.JVCO_Primary_Tariff__c = 'GP_PRS';
            ven.JVCO_City__c = 'City'+cat+i;
            ven.JVCO_Country__c = 'United Kingdom';
            ven.JVCO_Street__c = 'Street'+cat+i;
            ven.JVCO_Postcode__c = 'ND'+cat+i+' 8TH'; 

            venList.add(ven);
        }

        Insert venList;

        Map<Id, JVCO_Venue__c> venMap = new Map<Id, JVCO_Venue__c>([Select Id, Name from JVCO_Venue__c where Name like 'Venue-%']);
	    
	    for(JVCO_Venue__c verRec : venList) 
	    {            
            JVCO_Affiliation__c aff = new JVCO_Affiliation__c();
            aff = JVCO_TestClassObjectBuilder.createAffiliation(a2.id, verRec.Id);
            affList.add(aff);       
        }
	    
	    insert affList;
	    
	    Account a3 = JVCO_TestClassObjectBuilder.createCustomerAccount();
	    //a3.RecordTypeId = customerRT;
	    a3.Name = 'Test Account 022345-C';
	    //a3.Type = 'Key Account';
	    a3.ShippingPostalCode = 'E10 4XX';
	    a3.ShippingCity = 'London';
	    a3.ShippingCountry = 'TestCountry1';
	    a3.ShippingState = 'TestState1';
	    a3.ShippingStreet = 'TestStreet1';
	    insert a3;
	    
	    Account a4 = JVCO_TestClassObjectBuilder.createLicenceAccount(a3.id);
	    //a4.RecordTypeId = licenceRT;
	    a4.Name = 'Test Account 022345-L';
	    //a4.JVCO_Customer_Account__c = a3.Id;
	    a3.ShippingPostalCode = 'E10 4XX';
	    a3.ShippingCity = 'London';
	    a3.ShippingCountry = 'TestCountry2';
	    a3.ShippingState = 'TestState2';
	    a3.ShippingStreet = 'TestStreet2';
	    insert a4;
	    
	    for(JVCO_Affiliation__c affRec : affList) 
	    {
	        JVCO_VenueTransfer.JVCO_TransferObject tc = new JVCO_VenueTransfer.JVCO_TransferObject();
	        JVCO_Venue__c venue = venMap.get(affRec.JVCO_Venue__c);
	        tc.affiliationRecord = affRec;
	        tc.venueRecord = venue;
	        tc.accountRecord = a2;
	        transferSelectionsMap.put(affRec.Id, tc);
	    }

	    mapTempIdJVCOAffiliationDate.clear();
	    if(transferSelectionsMap.size() > 0) 
	    {  
	        for(String affKey: transferSelectionsMap.keySet())
	        {
	            mapTempIdJVCOAffiliationDate.put(affKey,Date.valueOf(transferSelectionsMap.get(affKey).affiliationRecord.JVCO_Start_Date__c));
	        }
	    }

	    mapTempIdJVCOAffiliationCloseReason.clear();
	    if(transferSelectionsMap.size() > 0) 
	    {  
	        for(String kAffli: transferSelectionsMap.keySet()){
                //mapTempIdJVCOAffiliationCloseReason.get(kAffli).affiliationRecord.JVCO_Closure_Reason__c = 'Terminated by PPL PRS';
                mapTempIdJVCOAffiliationCloseReason.put(kAffli, 'Terminated by PPL PRS');
            }
    	}

    	mapTempIdJVCOAffiliationAccId.clear();
	    if(transferSelectionsMap.size() > 0) 
	    {  
	        for(String kAffli: transferSelectionsMap.keySet()){
                //mapTempIdJVCOAffiliationAccId.get(kAffli).accountId = String.valueOf(a4);
                mapTempIdJVCOAffiliationAccId.put(kAffli, String.valueOf(a4));
            }
    	}

    
    Id queueID = System.enqueueJob(new JVCO_DisposeVenueQueueable( a4, mapTempIdJVCOAffiliationDate , mapTempIdJVCOAffiliationCloseReason, mapTempIdJVCOAffiliationAccId, transferSelectionsMap,  1));

    Test.stopTest();
  }

  @isTest
  static void noDisplayMap_Test() 
  	{
  		JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_LeadTrigger__c = true;
        insert dt;
        
  		JVCO_VenueTransfer vt = new JVCO_VenueTransfer();
	    Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
	    Map<String, Date> mapTempIdJVCOAffiliationDate = new Map<String, Date>();
    	Map<String, String> mapTempIdJVCOAffiliationCloseReason = new Map<String, String>();
    	Map<String, String> mapTempIdJVCOAffiliationAccId = new Map<String, String>();
    	
	    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
	    Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
	    
	    Test.startTest();

	    List<Account> customerAccounts = [select id, name from Account where RecordTypeId = :customerRT];
	    List<Account> licenceAccounts = [select id, name from Account where RecordTypeId = :licenceRT];

	   // system.assertEquals(null, customerAccounts,'not equals');
	    
	    Account a1 = new Account();
	    a1.RecordTypeId = customerRT;
	    a1.Name = 'Test Customer0822201 Account';
	    a1.Type = 'Key Account';
	    insert a1;
	    
	    Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
	    a2.RecordTypeId = licenceRT;
	    a2.Name = 'Test Licence Account';
	    a2.JVCO_Customer_Account__c = a1.Id;
	    insert a2;
	    
	    List<JVCO_Venue__c> venList = new List<JVCO_Venue__c>();
	    List<JVCO_Affiliation__c> affList = new List<JVCO_Affiliation__c>();
        String cat = 'TEST';
        for(Integer i=1; i<5; i++) 
        {
            JVCO_Venue__c ven = new JVCO_Venue__c();
            ven.Name = 'Venue-'+cat+i;
            ven.JVCO_Lead_Source__c = 'Churn';
            //ven.JVCO_Venue_Type__c = 'Activity Centre'; 
            //ven.JVCO_Venue_Sub_Type__c = 'Artificial Ski Centre';
            //ven.JVCO_Budget_Category__c = 'General Purpose';
            //ven.JVCO_Primary_Tariff__c = 'GP_PRS';
            ven.JVCO_City__c = 'City'+cat+i;
            ven.JVCO_Country__c = 'United Kingdom';
            ven.JVCO_Street__c = 'Street'+cat+i;
            ven.JVCO_Postcode__c = 'ND'+cat+i+' 8TH'; 

            venList.add(ven);
        }

        Insert venList;

        Map<Id, JVCO_Venue__c> venMap = new Map<Id, JVCO_Venue__c>([Select Id, Name from JVCO_Venue__c where Name like 'Venue-%']);
	    
	    for(JVCO_Venue__c verRec : venList) 
	    {            
            JVCO_Affiliation__c aff = new JVCO_Affiliation__c();
            aff = JVCO_TestClassObjectBuilder.createAffiliation(a2.id, verRec.Id);
            affList.add(aff);       
        }
	    
	    insert affList;
	    
	    Account a3 = JVCO_TestClassObjectBuilder.createCustomerAccount();
	    //a3.RecordTypeId = customerRT;
	    a3.Name = 'Test Account 022345-C';
	    //a3.Type = 'Key Account';
	    a3.ShippingPostalCode = 'E10 4XX';
	    a3.ShippingCity = 'London';
	    a3.ShippingCountry = 'TestCountry1';
	    a3.ShippingState = 'TestState1';
	    a3.ShippingStreet = 'TestStreet1';
	    insert a3;
	    
	    Account a4 = JVCO_TestClassObjectBuilder.createLicenceAccount(a3.id);
	    //a4.RecordTypeId = licenceRT;
	    a4.Name = 'Test Account 022345-L';
	    //a4.JVCO_Customer_Account__c = a3.Id;
	    a3.ShippingPostalCode = 'E10 4XX';
	    a3.ShippingCity = 'London';
	    a3.ShippingCountry = 'TestCountry2';
	    a3.ShippingState = 'TestState2';
	    a3.ShippingStreet = 'TestStreet2';
	    insert a4;
	  

    
    Id queueID = System.enqueueJob(new JVCO_DisposeVenueQueueable( a4, mapTempIdJVCOAffiliationDate , mapTempIdJVCOAffiliationCloseReason, mapTempIdJVCOAffiliationAccId, transferSelectionsMap,  1));

    Test.stopTest();
  }
}
