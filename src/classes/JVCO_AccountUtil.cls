/* -------------------------------------------------------------------------------------------------------
   Name:            JVCO_AccountUtil.cls 
   Description:     Custom Code for Account DLRS
   Test class:      

   Date            Version     Author              Summary of Changes 
   -----------     -------     -----------------   ----------------------------------------
   28-Apr-2017     0.1         franz.g.a.dimaapi   Intial draft
   26-Mar-2018     0.2         franz.g.a.dimaapi   Fixed Customer Account Balance - GREEN-30942
   22-Jun-2018     0.3         reymark.j.l.arlos   added logic to include roll up of historic and current values from LA - CA GREEN-32402
   04-Apr-2019     0.4         rhys.j.c.dela.cruz  GREEN-34465 - Updated code for Last Year Values
   07-Dec-2020     0.5         patrick.t.bautista  GREEN-36089 - Move update on transaction and transaction line to account trigger
------------------------------------------------------------------------------------------------------- */
public class JVCO_AccountUtil
{
    //private static final Id licAccRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    //private static final Boolean triggerBypassFilter = (Boolean)JVCO_Constants__c.getOrgDefaults().JVCO_TriggerBypassFilter__c;

    public static void afterUpdate(Map<Id, Account> oldAccMap, Map<Id, Account> newAccMap)
    {   
        /*Set<Id> custAccIdSet = new Set<Id>();
        for(Account a : newAccMap.values())
        {   
            if(a.JVCO_Customer_Account__c != null && a.RecordTypeId == licAccRecTypeId)
            {
                //for Licence Date Logic
                if(a.JVCO_Earliest_Licence_Start_Date__c != oldAccMap.get(a.Id).JVCO_Earliest_Licence_Start_Date__c ||
                    a.JVCO_Latest_Legacy_Invoice_Date_Licence__c != oldAccMap.get(a.Id).JVCO_Latest_Legacy_Invoice_Date_Licence__c ||
                    a.JVCO_Latest_Licence_End_Date__c != oldAccMap.get(a.Id).JVCO_Latest_Licence_End_Date__c || 
                    a.JVCO_PRS_Contract_End_Date__c != oldAccMap.get(a.Id).JVCO_PRS_Contract_End_Date__c)
                {
                    custAccIdSet.add(a.JVCO_Customer_Account__c);
                }
                //for Account Balance Logic
                if(a.ffps_accbal__Account_Balance__c != oldAccMap.get(a.Id).ffps_accbal__Account_Balance__c)
                {
                    custAccIdSet.add(a.JVCO_Customer_Account__c);
                }
                //for Licence Closure Date Logic
                if(a.JVCO_Closure_Date__c != oldAccMap.get(a.Id).JVCO_Closure_Date__c)
                {
                    custAccIdSet.add(a.JVCO_Customer_Account__c);
                }
                //for Licence Historic and Current values per society logic
                //04-Apr-2019 rhys.j.c.dela.cruz Added fields for Last Year Value
                if(a.JVCO_PPL_Historic_Value_Total__c != oldAccMap.get(a.Id).JVCO_PPL_Historic_Value_Total__c ||
                    a.JVCO_PRS_Historic_Value_Total__c != oldAccMap.get(a.Id).JVCO_PRS_Historic_Value_Total__c ||
                    a.JVCO_VPL_Historic_Value_Total__c != oldAccMap.get(a.Id).JVCO_VPL_Historic_Value_Total__c ||
                    a.JVCO_PPL_Current_Value_Total__c != oldAccMap.get(a.Id).JVCO_PPL_Current_Value_Total__c ||
                    a.JVCO_PRS_Current_Value_Total__c != oldAccMap.get(a.Id).JVCO_PRS_Current_Value_Total__c ||
                    a.JVCO_VPL_Current_Value_Total__c != oldAccMap.get(a.Id).JVCO_VPL_Current_Value_Total__c ||
                    a.JVCO_PRS_Last_Year_Value__c != oldAccMap.get(a.Id).JVCO_PRS_Last_Year_Value__c ||
                    a.JVCO_PPL_Last_Year_Value__c != oldAccMap.get(a.Id).JVCO_PPL_Last_Year_Value__c ||
                    a.JVCO_VPL_Last_Year_Value__c != oldAccMap.get(a.Id).JVCO_VPL_Last_Year_Value__c ||
                    a.JVCO_HistoricCurrentValuesBatchProcessed__c)
                {
                    custAccIdSet.add(a.JVCO_Customer_Account__c);
                }

                //For Expiry Date Logic
                if(a.JVCO_NextRenewalDate__c != oldAccMap.get(a.Id).JVCO_NextRenewalDate__c)
                {
                    custAccIdSet.add(a.JVCO_Customer_Account__c);
                }
                if(a.JVCO_Review_Type__c != oldAccMap.get(a.Id).JVCO_Review_Type__c){
                    custAccIdSet.add(a.JVCO_Customer_Account__c);
                }

                //Custom Setting Bypass filter
                if(triggerBypassFilter){
                    custAccIdSet.add(a.JVCO_Customer_Account__c);   
                }
            }
        }
        if(!custAccIdSet.isEmpty())
        {
            Map<Id, AggregateResult> custAccIdToAggregateResultMap = getCustAccIdToAggregateResultMap(custAccIdSet);
            updateCustomerLicenceDate(custAccIdToAggregateResultMap);
        }*/
    }
    
    public static List<Account> updateCustomerAccountUponLicenceAccountUpdate(Set<Id> custAccIdSet)
    {
        List<Account> accList = new List<Account>();
        if(!custAccIdSet.isEmpty())
        {
            Map<Id, AggregateResult> custAccIdToAggregateResultMap = getCustAccIdToAggregateResultMap(custAccIdSet);
            accList.addAll(updateCustomerLicenceDate(custAccIdToAggregateResultMap));
        }
        return accList;
    }

    private static Map<Id, AggregateResult> getCustAccIdToAggregateResultMap(Set<Id> custAccIdSet)
    {
        Map<Id, AggregateResult> custAccIdToAggregateResultMap = new Map<Id, AggregateResult>();
        for(AggregateResult ar : [SELECT JVCO_Customer_Account__c custAccId, 
                                    MIN(JVCO_Earliest_Licence_Start_Date__c)minStartDate, 
                                    MIN(JVCO_Latest_Legacy_Invoice_Date_Licence__c)minLegacyEndDate,
                                    MAX(JVCO_Latest_Licence_End_Date_With_Permit__c)maxEndDate,
                                    MIN(JVCO_PRS_Contract_End_Date__c)earliestPRSLegacyEndDate,
                                    SUM(ffps_accbal__Account_Balance__c)totalAccountBalance,
                                    COUNT(JVCO_Closure_Date__c)countClosureDate,
                                    COUNT(Id)totalLicenceAccount,
                                    SUM(JVCO_PRS_Historic_Value_Total__c) prsHistoric,
                                    SUM(JVCO_PPL_Historic_Value_Total__c) pplHistoric,
                                    SUM(JVCO_VPL_Historic_Value_Total__c) vplHistoric,
                                    SUM(JVCO_PRS_Current_Value_Total__c) prsCurrent,
                                    SUM(JVCO_PPL_Current_Value_Total__c) pplCurrent,
                                    SUM(JVCO_VPL_Current_Value_Total__c) vplCurrent,
                                    SUM(JVCO_PRS_Last_Year_Value__c) prsLastYear,
                                    SUM(JVCO_PPL_Last_Year_Value__c) pplLastYear,
                                    SUM(JVCO_VPL_Last_Year_Value__c) vplLastYear
                                    FROM Account
                                    WHERE JVCO_Customer_Account__c IN :custAccIdSet
                                    GROUP BY JVCO_Customer_Account__c])
        {
            custAccIdToAggregateResultMap.put((Id)ar.get('custAccId'), ar);
        }
        
        return custAccIdToAggregateResultMap;
    }
    
    private static List<Account> updateCustomerLicenceDate(Map<Id, AggregateResult> custAccIdToAggregateResultMap)
    {
        Set<Account> updatedAccSet = new Set<Account>();
        //Call Method for Expiry Date
        Map<Id, Account> accCustBulkExpiryDate = new Map<Id,Account>(custAccExpiryDateMethod(custAccIdToAggregateResultMap.keySet()));
        Map<Id, Account> accCustNextRenewalDate = new Map<Id,Account>(custAccNextRenewalDateMethod(custAccIdToAggregateResultMap.keySet()));

        for(Id custAccId : custAccIdToAggregateResultMap.keySet())
        {
            AggregateResult ar = custAccIdToAggregateResultMap.get(custAccId);
            Date startDate;
            
            Account a = new Account();
            a.Id = custAccId;
            a.JVCO_Customer_Account_Balance__c = (Decimal)ar.get('totalAccountBalance');
            a.JVCO_Renewable_Licence_Accounts__c = (Decimal)ar.get('totalLicenceAccount') - (Decimal)ar.get('countClosureDate');
            if(ar.get('earliestPRSLegacyEndDate') != null)
            {   
                startDate = ((Date)ar.get('earliestPRSLegacyEndDate')).addDays(1);
            }else if(ar.get('minLegacyEndDate') != null)
            {
                startDate = ((Date)ar.get('minLegacyEndDate')).addDays(1);
            }else
            {
                startDate = (Date)ar.get('minStartDate');
            }
            
            if(startDate!= null){a.JVCO_Customer_Licence_Start_Date2__c = startDate;}
            
            a.JVCO_Customer_Licence_End_Date2__c = (Date)ar.get('maxEndDate');

            //for Licence Historic and Current values per society roll up
            a.JVCO_PRS_Historic_Value_Total__c = (decimal)ar.get('prsHistoric');
            a.JVCO_PPL_Historic_Value_Total__c = (decimal)ar.get('pplHistoric');
            a.JVCO_VPL_Historic_Value_Total__c = (decimal)ar.get('vplHistoric');
            a.JVCO_PRS_Current_Value_Total__c = (decimal)ar.get('prsCurrent');
            a.JVCO_PPL_Current_Value_Total__c = (decimal)ar.get('pplCurrent');
            a.JVCO_VPL_Current_Value_Total__c = (decimal)ar.get('vplCurrent');

            //For Last Year Value
            a.JVCO_PRS_Last_Year_Value__c = (decimal)ar.get('prsLastYear');
            a.JVCO_PPL_Last_Year_Value__c = (decimal)ar.get('pplLastYear');
            a.JVCO_VPL_Last_Year_Value__c = (decimal)ar.get('vplLastYear');

            if(accCustNextRenewalDate.containsKey(a.Id))
            {
                a.JVCO_NextRenewalDateLive__c = (Date)accCustNextRenewalDate.get(a.Id).JVCO_NextRenewalDateLive__c;
                a.JVCO_NextRenewalDateNonLive__c = (Date)accCustNextRenewalDate.get(a.Id).JVCO_NextRenewalDate__c;
            }
                
            if(accCustBulkExpiryDate.containsKey(a.Id))
            {
                a.JVCO_HighValueRenewalDateNonLive__c = (Date)accCustBulkExpiryDate.get(a.Id).JVCO_HighValueRenewalDateNonLive__c;
            }
            
            updatedAccSet.add(a);
        }
        return new List<Account>(updatedAccSet);
    }


    /* ----------------------------------------------------------------------------------------------------------
        Author:         rhys.j.c.dela.cruz
        Company:        Accenture
        Description:    Set Customer Account Expiry Date Method
        Inputs:         Accounts
        Returns:        n/a
        <Date>          <Authors Name>                      <Brief Description of Change> 
        30-Oct-2018     rhys.j.c.dela.cruz                  GREEN-32809 - Method for Setting Expiry Date on Customer Account
        03-Jan-2019     rhys.j.c.dela.cruz                  GREEN-32809 - Additional update to query
    ----------------------------------------------------------------------------------------------------------*/
    private static Map<Id, Account> custAccExpiryDateMethod(Set<Id> accIdSet)
    {
        List<Account> accList = new List<Account>();
        Map<Id, Account> custAccBulkExpiryDate = new Map<Id, Account>();

        Decimal tempNonLiveAccCurrentVal = 0;
        for(Account custAcc : [SELECT id, Type, 
                                        (SELECT Id, Type, JVCO_Live__c, JVCO_NextRenewalDate__c, JVCO_AccCurrentValueTotal__c, JVCO_Review_Type__c
                                        FROM Accounts1__r
                                        WHERE JVCO_NextRenewalDate__c != null
                                        AND JVCO_Live__c = FALSE
                                        ORDER BY JVCO_AccCurrentValueTotal__c DESC)
                                    FROM Account
                                    WHERE Id IN: accIdSet
                                    ORDER BY Id ASC])
        {
            custAcc.JVCO_HighValueRenewalDateNonLive__c = null;
            tempNonLiveAccCurrentVal = 0;

            for(Account licAcc : custAcc.Accounts1__r)
            {
                if(!licAcc.JVCO_Live__c &&tempNonLiveAccCurrentVal <= licAcc.JVCO_AccCurrentValueTotal__c)
                {
                    if(licAcc.JVCO_AccCurrentValueTotal__c == 0)
                    {
                        custAcc.JVCO_HighValueRenewalDateNonLive__c = null;
                    }else
                    {
                        custAcc.JVCO_HighValueRenewalDateNonLive__c = licAcc.JVCO_NextRenewalDate__c;
                        tempNonLiveAccCurrentVal = licAcc.JVCO_AccCurrentValueTotal__c;
                    }
                }
            }
            custAccBulkExpiryDate.put(custAcc.Id, custAcc);
        }

        return custAccBulkExpiryDate;
    }

    /* ----------------------------------------------------------------------------------------------------------
        Author:         rhys.j.c.dela.cruz
        Company:        Accenture
        Description:    Set Customer Account Next Renewal Date Live/Non-Live
        Inputs:         Accounts
        Returns:        n/a
        <Date>          <Authors Name>                      <Brief Description of Change> 
        11-Dec-2018     rhys.j.c.dela.cruz                  GREEN-32809 - Method for Setting Expiry Date on Customer Account
        03-Jan-2019     rhys.j.c.dela.cruz                  GREEN-32809 - Additional update to query and fix issue
    ----------------------------------------------------------------------------------------------------------*/
    private static Map<Id, Account> custAccNextRenewalDateMethod(Set<Id> accIdSet){

        List<Account> accList = new List<Account>();
        Map<Id, Account> custAccNextRenewalDate = new Map<Id, Account>();

        Date tempNonLiveNextRenewalDate = null;
        Date tempLiveNextRenewalDate = null;

        for(Account acc : [SELECT JVCO_Customer_Account__c, Id, Type,
                            JVCO_Live__c, JVCO_Review_Type__c, JVCO_NextRenewalDate__c,
                            JVCO_NonLiveNextRenewalDateHolder__c, JVCO_LiveNextRenewalDateHolder__c
                            FROM Account
                            WHERE JVCO_Customer_Account__c IN: accIdSet
                            AND JVCO_NextRenewalDate__c != null
                            ORDER BY JVCO_Customer_Account__c ASC]){

            if(!custAccNextRenewalDate.containsKey(acc.JVCO_Customer_Account__c)){
                tempNonLiveNextRenewalDate = null;
                tempLiveNextRenewalDate = null;
            }

            Account tempAcc = new Account();

            if(acc.JVCO_Live__c && acc.JVCO_Review_Type__c != 'Do not renew'){
                if(tempLiveNextRenewalDate >= acc.JVCO_NextRenewalDate__c || tempLiveNextRenewalDate == null){
                    tempAcc.Id = acc.JVCO_Customer_Account__c;
                    tempAcc.JVCO_NextRenewalDateLive__c = acc.JVCO_NextRenewalDate__c;
                    tempLiveNextRenewalDate = acc.JVCO_NextRenewalDate__c;
                }
            }
            else if(!acc.JVCO_Live__c){
                if(tempNonLiveNextRenewalDate >= acc.JVCO_NextRenewalDate__c || tempNonLiveNextRenewalDate == null){
                    tempAcc.Id = acc.JVCO_Customer_Account__c;
                    tempAcc.JVCO_NextRenewalDate__c = acc.JVCO_NextRenewalDate__c;
                    tempNonLiveNextRenewalDate = acc.JVCO_NextRenewalDate__c;
                }
            }

            if(tempAcc.Id != null){
                if(custAccNextRenewalDate.containsKey(tempAcc.Id)){

                    if(tempAcc.JVCO_NextRenewalDateLive__c != null){
                        custAccNextRenewalDate.get(tempAcc.Id).JVCO_NextRenewalDateLive__c = tempAcc.JVCO_NextRenewalDateLive__c;
                    }

                    if(tempAcc.JVCO_NextRenewalDate__c != null){
                        custAccNextRenewalDate.get(tempAcc.Id).JVCO_NextRenewalDate__c = tempAcc.JVCO_NextRenewalDate__c;
                    }
                }
                else{
                    custAccNextRenewalDate.put(tempAcc.Id, tempAcc);
                }
            }
        }

        return custAccNextRenewalDate;
    }
    
    /* ----------------------------------------------------------------------------------------------
        Author: patrick.t.bautista
        Company: Accenture
        Description: Update JVCO_Number_of_Outstanding_Invoices__c to get the number of sales invoice
        Inputs: List<c2g__codaInvoice__c>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        21-Apr-2018 patrick.t.bautista       Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public static List<Account> getTheTotalNumberOfInvoices(Set<Id> accIdSet)
    {
        Map<Id, AggregateResult> accIdToAggregateResultMap = new Map<Id, AggregateResult>();
        for(AggregateResult ar : [SELECT c2g__Account__c  accId,
                                  COUNT(c2g__MatchingStatus__c) totalOutInv
                                  FROM c2g__codaTransactionLineItem__c
                                  WHERE c2g__MatchingStatus__c = 'Available'
                                  AND c2g__LineType__c = 'Account'
                                  AND c2g__Account__c IN: accIdSet
                                  AND c2g__Transaction__r.c2g__TransactionType__c = 'Invoice'
                                  AND c2g__MatchingStatus__c != 'Paid'
                                  AND c2g__Account__r.JVCO_In_Infringement__c = false 
                                  AND c2g__Account__r.JVCO_In_Enforcement__c = false
                                  GROUP by c2g__Account__c])
        {   
            accIdToAggregateResultMap.put((Id)ar.get('accId'), ar);
        }
        
        Set<Account> updatedAccountSet = new Set<Account>();
        for(Id accId : accIdSet)
        {
            Account acc = new Account();
            acc.Id = accId;
            if(accIdToAggregateResultMap.containsKey(accId)){
                acc.JVCO_Number_of_Outstanding_Invoices__c = (Decimal)accIdToAggregateResultMap.get(accId).get('totalOutInv');
            }else{
                acc.JVCO_Number_of_Outstanding_Invoices__c = 0;
            }
            updatedAccountSet.add(acc);
        }
        return new List<Account>(updatedAccountSet);
    }
}