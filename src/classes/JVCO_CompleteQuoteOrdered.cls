/*
    Created by: Eu Rey Cadag 
     Date Created: 11-Sept-2017
     Details: Queueable class to create contract for Opportunity
     Version: v1.0

    Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
     26-Dec-2017       1.2        Accenture-mel.andrei.b.santos     Updated controller to run JVCO_CompleteQuoteOrdered first before JVCO_CompleteQuoteController as per GREEN-26719
     04-May-2018       1.3        Accenture-rhys.j.c.dela.cruz      GREEN-31707 - Updated controller to have a flag that will prevent Account Trigger (onUpdate) from running to prevent encountering Locked Row error.
     31-May-2018       1.4        Accenture-rhys.j.c.dela.cruz      GREEN-32158 - Updated class to have retry codes when order did not generate on first run.
     19-Sept-2018      1.5        Accenture-rhys.j.c.dela.cruz      GREEN-33476 - Adjusted method for Partner Users so Complete and Invoiced will not be overwritten
     03-Dec-2018      1.6     mel.andrei.b.santos         GREEN-34091 - Update controller to consider initial value for rollback
     24-Apr-2019      1.7     rhys.j.c.dela.cruz          GREEN-34512 - Check if order is generated then revert ordered field if no order
     26-Jun-2019      1.8     rhys.j.c.dela.cruz                    GREEN-34698 - Adjusted handling for Opportunity that did not create an Order
 */
 public class JVCO_CompleteQuoteOrdered implements Queueable {
     
    Opportunity OppoRecord;
    List<sObject> lstObjectRecs = new List<sObject>();
    SBQQ__Quote__c Quote;
    //GREEN-33476
    SBQQ__Quote__c oldQuote; //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
    Id profileId = userinfo.getProfileId();
    String profileName = [SELECT Id, Name FROM Profile WHERE Id =: profileId].Name;
 
     public JVCO_CompleteQuoteOrdered(Opportunity OppoRecord, SBQQ__Quote__c Quote, SBQQ__Quote__c QuoteOld) //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
     {
       this.OppoRecord = OppoRecord;
       lstObjectRecs.add((sObject) this.OppoRecord);
       this.Quote = Quote;
       oldQuote = QuoteOld; //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
     } 
 
     public void execute(QueueableContext context) 
     {
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;

        //GREEN-32158 - Get custom settings to be the maximum number of tries in while loop
        Integer MaxOrderRetries;
        Integer j = 0;
        MaxOrderRetries = (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_MaxOrderRetries__c != null ? (Integer)JVCO_Constants__c.getOrgDefaults().JVCO_MaxOrderRetries__c : 3;

        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        if(OppoRecord != null)
        {
            Savepoint sp = Database.setSavepoint();
            OppoRecord.SBQQ__Ordered__c = True;

            if(Test.isRunningTest()){ //Added isRunningTest to create DML error - reymark.j.l.arlos
                lstObjectRecs.add(new Opportunity());
            }
            try
            {
              if(lstObjectRecs.size() > 0)
              { 
                do
                {
                  system.debug('### lstObjectRecs: '+ lstObjectRecs);
                    List<Database.SaveResult> res = Database.update(lstObjectRecs,false);

                    system.debug('### lstObjectRecs: '+ lstObjectRecs.size());
                    for(Integer i = 0; i < lstObjectRecs.size(); i++)
                    {
                        Database.SaveResult srOppList = res[i];
                        
                        sObject origrecord = lstObjectRecs[i];
                        system.debug('###srOppList: ' + srOppList.isSuccess());

                        if(!srOppList.isSuccess())
                        {
                            Database.rollback(sp);
                            for(Database.Error objErr:srOppList.getErrors())
                            {
                              ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();

                              //errLog.Name = String.valueOf(origrecord.ID);
                              errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                              errLog.ffps_custRem__Related_Object_Key__c  = String.valueOf(origrecord.ID);
                              errLog.ffps_custRem__Detail__c  = string.valueof(objErr.getMessage());
                              errLog.ffps_custRem__Grouping__c   = string.valueof(objErr.getStatusCode());
                              errLog.JVCO_Number_of_Tries__c = j + 1;
                              /* 13-10-2017 mel.andrei.b.santos 
                              if(origrecord.getSObjectType() == Opportunity.sObjectType) 
                                errLog.JVCO_ErrorBatchName__c = 'JVCO_CompleteQuoteContracted: Update Opportunity Record';
                              else if(origrecord.getSObjectType() == SBQQ__Quote__c.sObjectType)
                                errLog.JVCO_ErrorBatchName__c = 'JVCO_CompleteQuoteContracted: Update Quote Record';
                                */

                               errLog.ffps_custRem__Message__c = 'JVCO_CompleteQuoteOrdered';

                              errLogList.add(errLog);   
                            }

                        }
                    }
                    j++;
                  }
                  while (errLogList.size() > 0 && j < MaxOrderRetries);

                  system.debug('### Errloglist: '+ errLogList);
                  system.debug('### Errloglist: '+ errLogList.isEmpty());

                  //GREEN-34512
                  List<Order> orderGenerated = [SELECT Id FROM Order WHERE SBQQ__Quote__c =: Quote.Id AND SBQQ__Quote__r.SBQQ__Primary__c = true];
                  System.debug('Order Generated: ' + orderGenerated.size());

                  if( !errLogList.isEmpty() || orderGenerated.isEmpty())
                  {
                    Database.rollback(sp);

                    insert errLogList;    
                    errLogList.clear();
                    Quote.JVCO_QuoteComplete__c = oldQuote.JVCO_QuoteComplete__c; //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
                    Quote.SBQQ__Status__c = oldQuote.SBQQ__Status__c; //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
                    system.debug('### Quote: ' + Quote);      
                    update Quote;          
                  }
                  else if(Quote.SBQQ__Opportunity2__r.SBQQ__Contracted__c)
                  {
                    system.debug('### Check SBQQ__Ordered__c true: ' + Quote.SBQQ__Opportunity2__r.SBQQ__Ordered__c);
                    //GREEN-33476
                    if(profileName != 'JVCO Partner Community User'){
                      Quote.JVCO_QuoteComplete__c = true;  
                      update Quote;   
                    }
                  }

                  system.debug('### OppoRecord: ' + OppoRecord.id);
                  //OppoRecord.StageName = 'Closed Won';
                  //OppoRecord.Probability = 100;
                  //OppoRecord.SBQQ__Contracted__c = true;

                  //start 28-12-2017 reymark.j.l.arlos
                  OppoRecord.Amount = Quote.SBQQ__NetAmount__c;
                  //end 28-12-2017

                  if(Test.isRunningTest()){ //Added isRunningTest to not chain call reymark.j.l.arlos
                    // do nothing
                  }
                  //else
                  //{
                    // call queable job
                    //JVCO_CompleteQuoteContracted CompleteQuoteContracted = new JVCO_CompleteQuoteContracted(OppoRecord, Quote);
                    //System.enqueueJob(CompleteQuoteContracted); 
                    //Quote.JVCO_QuoteComplete__c = true;  
                   // update quote; 
                  //} 
                    
              }
            }
            catch(Exception objErr)
            {
                ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
                String ErrorLineNumber = objErr.getLineNumber() != Null ? String.ValueOf(objErr.getLineNumber()):'None';
                String ErrorMessage = 'Cause: ' + objErr.getCause() + ' ErrMsg: ' + objErr.getMessage();
                errLogList.add(JVCO_ErrorUtil.logError(String.valueOf(OppoRecord.ID), ErrorMessage, ErrorLineNumber, 'JVCO_CompleteQuoteOrdered:Order'));    

                insert errLogList;
                errLogList.clear();

                Quote.JVCO_QuoteComplete__c = oldQuote.JVCO_QuoteComplete__c;   //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
                Quote.SBQQ__Status__c = oldQuote.SBQQ__Status__c; //03-Dec-2018  mel.andrei.b.santos GREEN-34091 
                update Quote; 
            }
        }  
     } 
 }