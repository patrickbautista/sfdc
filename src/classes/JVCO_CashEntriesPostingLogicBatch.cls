/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CashEntriesPostingLogicBatch.cls 
   Description: Batch class for JVCO_CashEntriesPostingLogic object

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
   04-Nov-2017  0.2         franz.g.a.dimaapi   Intial creation - GREEN-25465
   04-Jul-2018  0.3         franz.g.a.dimaapi   GREEN-32622 - Refactor for Performance Improvement
   07-Dec-2020  0.4         patrick.t.bautista  GREEN-36089 - Added queable logic for invoice update
----------------------------------------------------------------------------------------------- */
public class JVCO_CashEntriesPostingLogicBatch implements Database.Batchable<sObject>
{   
    private Set<Id> cashEntryIdSet;
    private Boolean useBatch;
    private Boolean useBatchAndIsSmarterProcess;
    
    public JVCO_CashEntriesPostingLogicBatch(){}
    
    public JVCO_CashEntriesPostingLogicBatch(Set<Id> cashEntryIdSet)
    {
        this.cashEntryIdSet = cashEntryIdSet;
        useBatch = false;
        useBatchAndIsSmarterProcess = false;
    }
    
    public JVCO_CashEntriesPostingLogicBatch(Set<Id> cashEntryIdSet, Boolean useBatch)
    {
        this.cashEntryIdSet = cashEntryIdSet;
        this.useBatch = useBatch;
        useBatchAndIsSmarterProcess = false;
    }
    
    public JVCO_CashEntriesPostingLogicBatch(Boolean useBatchAndIsSmarterProcess, Set<Id> cashEntryIdSet)
    {
        this.cashEntryIdSet = cashEntryIdSet;
        this.useBatchAndIsSmarterProcess = useBatchAndIsSmarterProcess;
        this.useBatch = useBatchAndIsSmarterProcess;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        if(cashEntryIdSet != null)
        {
            return Database.getQueryLocator([SELECT Id, JVCO_Posted__c
                                            FROM c2g__codaCashEntry__c 
                                            WHERE Id IN :cashEntryIdSet]);
        }else
        {
            return null;
        }
    }

    public void execute(Database.BatchableContext BC, List<c2g__codaCashEntry__c> scope)
    { 
        c2g.CODAAPICommon_7_0.Context contextObj = new c2g.CODAAPICommon_7_0.Context();
        List<c2g.CODAAPICommon.Reference> referenceList = new List<c2g.CODAAPICommon.Reference>();
        for(c2g__codaCashEntry__c cashEntry : scope)
        {
            c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
            ref.Id = cashEntry.Id;
            referenceList.add(ref);
        }

        try
        {
            JVCO_BackgroundMatchingLogic.stopMatching = true;
            c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(contextObj, referenceList);
        }catch(Exception e)
        {   
            JVCO_ErrorUtil errorUtil = new JVCO_ErrorUtil();
            errorUtil.logErrorFromCashEntry(scope, 'Posting of Cash Entry', 'PC-001', e.getMessage());
        }
    }

    public void finish(Database.BatchableContext BC)
    {
        if(cashEntryIdSet != null)
        {
            Database.executeBatch(new JVCO_CashBackgroundMatchingBatch(cashEntryIdSet, useBatch, useBatchAndIsSmarterProcess), 1);
        } 
    }
}