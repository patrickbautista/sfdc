/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CampaignMemberReassignmentBatch.cls 
   Description: Batch class that handles reassignment of CampaignMembers

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  06-Sep-2016   0.1         ryan.i.r.limlingan  Intial creation
  07-Sep-2016	0.2			ryan.i.r.limlingan	Changed start() to return CampaignMember
  ----------------------------------------------------------------------------------------------- */
public class JVCO_CampaignMemberReassignmentBatch implements Database.Batchable<sObject>
{
    List<CampaignMember> campaignMemberFilterList;
    
/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Constructor that initializes the campaignMemberFilterList
    Inputs: List<CampaignMember>
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    06-Sep-2016 ryan.i.r.limlingan  Initial version of function
    07-Sep-2016 ryan.i.r.limlingan	Changed to accept List<CampaignMember>
  ----------------------------------------------------------------------------------------------- */
    public JVCO_CampaignMemberReassignmentBatch(List<CampaignMember> campaignMemberList)
    {
        campaignMemberFilterList = campaignMemberList;
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Queries CampaignMember based on campaignMemberList
    Inputs: List<CampaignMember>
    Returns: Database.QueryLocator
    <Date>      <Authors Name>      <Brief Description of Change> 
    06-Sep-2016 ryan.i.r.limlingan  Initial version of function
    07-Sep-2016 ryan.i.r.limlingan	Modified query to lookup CampaignMember instead
  ----------------------------------------------------------------------------------------------- */
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([SELECT Id, CampaignId, LeadId FROM CampaignMember
        								 WHERE Id IN :campaignMemberFilterList]);
    }

/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Calls function in SharedLogic class to do the actual reassignment
    Inputs: Database.BatchableContext, List<CampaignMember>
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    06-Sep-2016 ryan.i.r.limlingan  Initial version of function
    07-Sep-2016 ryan.i.r.limlingan	Changed parameter
  ----------------------------------------------------------------------------------------------- */    
    public void execute(Database.BatchableContext BC, List<CampaignMember> scope)
    {
        JVCO_CampaignSharedLogic.updateCampaignMemberOwner(scope);
    }
    
/* ----------------------------------------------------------------------------------------------
    Author: ryan.i.r.limlingan
    Company: Accenture
    Description: Logs results of execute()
    Inputs: Database.BatchableContext
    Returns: void
    <Date>      <Authors Name>      <Brief Description of Change> 
    08-Sep-2016 ryan.i.r.limlingan  Initial version of function
  ----------------------------------------------------------------------------------------------- */ 
    public void finish(Database.BatchableContext BC)
    {
		if (!JVCO_CampaignSharedLogic.errorsList.isEmpty())
		{
			AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			                         TotalJobItems, CreatedBy.Email
			                  FROM AsyncApexJob 
			                  WHERE Id = :BC.getJobId()];

			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			String[] toAddresses = new String[] {UserInfo.getUserId()};
			mail.setToAddresses(toAddresses);
			mail.setSubject('Campaign Member : Reassign to Telesales Partner Queue ' + a.Status);

			String emailBody = '';
			emailBody += 'The batch processed ' + a.TotalJobItems + ' batches with '
							+  a.NumberOfErrors + ' failures.';
			emailBody += '\nThe following records have failed to update:\n';
			for (String error : JVCO_CampaignSharedLogic.errorsList)
			{
				emailBody += error;
			}
			mail.setPlainTextBody(emailBody);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		}
    }
    
}