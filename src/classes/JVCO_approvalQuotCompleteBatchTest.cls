@isTest
private class JVCO_approvalQuotCompleteBatchTest
{
    @testSetup static void insertValues()
    {
        List<JVCO_Constants__c> custSetting = new List<JVCO_Constants__c>();
        custSetting.add(new JVCO_Constants__c(JVCO_Quote_Recalculation_Time_Buffer__c = 5));
        insert custSetting;
        
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();  
        insert qProcess;

        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Account acc =  JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.Type = 'Customer';
        acc2.JVCO_Renewal_Scenario__c = '';
        insert acc2;

        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        PriceBookEntry pbe = JVCO_TestClassObjectBuilder.createPriceBookEntry(prod.Id);
        insert pbe;


        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_End_Date__c = null;
        aff.JVCO_Closure_Reason__c = null;
        insert aff;

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        opp.JVCO_OpportunityCancelled__c = false;
        insert opp;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, 'Annually');
        q.JVCO_Quote_Auto_Renewed__c = true;
        q.SBQQ__Status__c = 'Draft';
        q.Recalculate__c = false;
        q.SBQQ__Type__c = 'Quote';   
        insert q;

        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];
       
        q.SBQQ__Type__c = 'Renewal';
        q.Recalculate__c = true;
        update q;
        

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.Id, prod.Id);

        ql.CreatedDate = Datetime.Now();
        //ql.JVCO_Salesforce_Last_Recalculated_Time__c = Datetime.Now().addDays(3);
        ql.JVCO_Steelbrick_Last_Recalculated_Time__c = Datetime.Now();
        ql.SPVAvailability__c = true;
        insert ql;


        List<JVCO_Renewal_Settings__c> settings = new List<JVCO_Renewal_Settings__c>();
        settings.add(new JVCO_Renewal_Settings__c(Name = 'Automatic Quote Completion', Days__c = 0.0));
        insert settings;

    }

    @isTest
    static void JVCO_approvalQuotesCreationCompleteBatchtest()
    {
        SBQQ__Quote__c q = [select Id, SBQQ__Status__c from SBQQ__Quote__c  limit 1];
        set<id> idrec = new Set<Id>();
        idrec.add(q.id);
        Test.startTest();
        database.executeBatch(new JVCO_approvalQuotesCreationCompleteBatch(idrec),1);
        Test.stopTest();

        Opportunity o = [select Id, StageName from Opportunity limit 1];
        SBQQ__Quote__c q2 = [select Id, SBQQ__Status__c, JVCO_Recalculated__c, SBQQ__Primary__c, NoOfQuoteLinesWithoutSPV__c, SBQQ__Opportunity2__r.SBQQ__Contracted__c, SBQQ__NetAmount__c,SBQQ__Opportunity2__r.JVCO_OpportunityCancelled__c, JVCO_QuoteComplete__c from SBQQ__Quote__c  where id =:q.id];

        system.assertEquals(true,q2.SBQQ__Opportunity2__r.SBQQ__Contracted__c);
         
    }

    @isTest
    static void JVCO_approvalQuotesCreationCompleteBatchtestAmendment()
    {
        SBQQ__Quote__c q = [select Id, SBQQ__Status__c, SBQQ__Type__c from SBQQ__Quote__c  limit 1];
        q.SBQQ__Type__c = 'Amendment';

        SBQQ__QuoteLine__c qlOld = [Select id, SBQQ__PriorQuantity__c, SBQQ__Existing__c, SBQQ__Quantity__c, Affiliation__c ,SBQQ__Quote__c, SBQQ__Group__c, SBQQ__Product__c , SBQQ__UpgradedQuantity__c from SBQQ__QuoteLine__c where SBQQ__Quote__c =:q.id limit 1];

        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.Affiliation__c = qlOld.Affiliation__c;
        ql.SBQQ__Quote__c = qlOld.SBQQ__Quote__c;
        ql.SBQQ__Group__c = qlOld.SBQQ__Group__c;
        ql.SBQQ__Product__c = qlOld.SBQQ__Product__c;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 100;
        ql.SBQQ__CustomerPrice__c = 100;
        ql.SBQQ__NetPrice__c = 100;
        ql.SBQQ__SpecialPrice__c = 100;
        ql.SBQQ__RegularPrice__c = 100;
        ql.SBQQ__UnitCost__c = 100;
        ql.SBQQ__ProratedListPrice__c = 100;
        ql.SBQQ__SpecialPriceType__c = 'Custom'; 
        ql.NoOfQuizPerAnnum__c = 100;
        ql.SBQQ__PriorQuantity__c = 1.0001;
        ql.SBQQ__Existing__c = true;
        ql.SBQQ__UpgradedQuantity__c = 0;
        ql.JVCO_Steelbrick_Last_Recalculated_Time__c = Datetime.Now();
        ql.SPVAvailability__c = true;
        

        SBQQ.TriggerControl.disable();
        update q;
        insert ql;
        SBQQ.TriggerControl.enable();

        set<id> idrec = new Set<Id>();
        idrec.add(q.id);
        Test.startTest();
        database.executeBatch(new JVCO_approvalQuotesCreationCompleteBatch(idrec),1);
        Test.stopTest();

        Opportunity o = [select Id, StageName from Opportunity limit 1];
        SBQQ__Quote__c q2 = [select Id, SBQQ__Status__c,SBQQ__LineItemCount__c, JVCO_Recalculated__c, SBQQ__Primary__c, NoOfQuoteLinesWithoutSPV__c, SBQQ__Opportunity2__r.SBQQ__Contracted__c, SBQQ__NetAmount__c,SBQQ__Opportunity2__r.JVCO_OpportunityCancelled__c, JVCO_QuoteComplete__c from SBQQ__Quote__c  where id =:q.id];
        
        system.assertEquals(true,q2.SBQQ__Opportunity2__r.SBQQ__Contracted__c);
         
    }
}