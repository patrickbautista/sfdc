@isTest
public class JVCO_LateDDIReminderTest {
    
    @testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim2('PRS');
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Test.startTest();
        SBQQ.TriggerControl.disable();
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        insert JVCO_TestClassHelper.setQuoteProcess();
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;

        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        SBQQ.TriggerControl.enable();
        
        JVCO_ResetAccountStatusSettings__c DunningSettings = new JVCO_ResetAccountStatusSettings__c();
        DunningSettings.JVCO_BatchSizeInteger__c = 1;
        DunningSettings.JVCO_EmailAddress__c = 'test@pplprs.com';
        DunningSettings.JVCO_Limit__c = 0;
        insert DunningSettings;
               
        Test.stopTest();
        
    }
    
    
    @isTest
    static void testExecute()
    {
        JVCO_Constants__c lateDDSettings = new JVCO_Constants__c();
        lateDDSettings.JVCO_LateDDIReminder_Batch_Size__c = 20;
        insert lateDDSettings;
        
        List<Account> acctList = new List<Account>();       
        Account acct = [Select id,
                        JVCO_Customer_Account__c
                        FROM Account 
                        WHERE RecordType.Name = 'Licence Account' 
                        limit 1
                       ];

		acctList.add(acct);
		Test.startTest();
		JVCO_LateDDIReminderScheduler jc = new JVCO_LateDDIReminderScheduler();
		jc.execute(null);
		Test.stopTest(); 
		System.assertEquals(true, acctList.size() != 0);

    }
    
    
    // Account with Document Queues for DD Mandate but without Income Direct Debit
	@isTest static void testDDMandateWithoutDD(){
        
        Account acct = [Select id,
                        JVCO_Customer_Account__c
                        FROM Account 
                        WHERE RecordType.Name = 'Licence Account' 
                        limit 1
                       ];
        
        Contact con = [Select id
                       From Contact
                       LIMIT 1];
		
        JVCO_Document_Queue__c DocQue = new JVCO_Document_Queue__c(
            	Scenario_Category__c = 'Direct Debit',
            	JVCO_Scenario__c = 'Direct Debit Mandate',
            	JVCO_Generation_Status__c = 'Generated',
            	JVCO_Format__c = 'PDF',
            	JVCO_Transmission__c = 'Letter',
            	JVCO_Print_Status__c = 'Printed',
            	JVCO_Inclusions__c = 'BRE Envelope 1',
            	JVCO_Recipient__c = con.id,
            	JVCO_Related_Account__c = acct.id,
            	JVCO_Related_Customer_Account__c = acct.JVCO_Customer_Account__c,	
            	CreatedDate = system.today()-10
        );
        insert DocQue;
		
        Test.startTest();
		Database.executeBatch(new JVCO_LateDDIReminder());
        Test.stopTest();
        
        List<JVCO_Document_Queue__c> LateDDIs = [SELECT ID
                                         FROM JVCO_Document_Queue__c
                                         WHERE JVCO_Scenario__c = 'Late Direct Debit Instruction Return'
                                         LIMIT 1];
       	System.assertEquals(1,LateDDIs.size());
    }
    
    @isTest static void testDDMandateWithDD(){
         
        Account acct = [SELECT id,
                        JVCO_Customer_Account__c
                        FROM Account 
                        WHERE RecordType.Name = 'Licence Account' 
                        limit 1
                       ];
        
        Contact con = [SELECT id
                       FROM Contact
                       LIMIT 1];
		
        JVCO_Document_Queue__c DocQue = new JVCO_Document_Queue__c(
            	Scenario_Category__c = 'Direct Debit',
            	JVCO_Scenario__c = 'Direct Debit Mandate',
            	JVCO_Generation_Status__c = 'Generated',
            	JVCO_Format__c = 'PDF',
            	JVCO_Transmission__c = 'Letter',
            	JVCO_Print_Status__c = 'Printed',
            	JVCO_Inclusions__c = 'BRE Envelope 1',
            	JVCO_Recipient__c = con.id,
            	JVCO_Related_Account__c = acct.id,
            	JVCO_Related_Customer_Account__c = acct.JVCO_Customer_Account__c,	
            	CreatedDate = system.today()-10
        );
        insert DocQue;
        
        Income_Direct_Debit__c IDD = new Income_Direct_Debit__c(
        		Account__c = acct.id,
            	Contact__c = con.id,
            	DD_Bank_Account_Number__c = '70872490',
            	DD_Bank_Sort_Code__c = '404784'
        );
        insert IDD;
        		
        Test.startTest();
		Database.executeBatch(new JVCO_LateDDIReminder());
        Test.stopTest();
        
        List<JVCO_Document_Queue__c> LateDDIs = [SELECT ID
                                         FROM JVCO_Document_Queue__c
                                         WHERE JVCO_Scenario__c = 'Late Direct Debit Instruction Return'
                                         LIMIT 1];
       	System.assertEquals(0,LateDDIs.size());
    }
    
    @isTest static void testCommsPrefPrint(){
        
        Account acct = [SELECT id,
                        JVCO_Customer_Account__c
                        FROM Account 
                        WHERE RecordType.Name = 'Licence Account' 
                        limit 1
                       ];
        
        acct.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
        update acct;
        
        Contact con = [SELECT id
                       FROM Contact
                       LIMIT 1];
		
        JVCO_Document_Queue__c DocQue = new JVCO_Document_Queue__c(
            	Scenario_Category__c = 'Direct Debit',
            	JVCO_Scenario__c = 'Direct Debit Mandate',
            	JVCO_Generation_Status__c = 'Generated',
            	JVCO_Format__c = 'PDF',
            	JVCO_Transmission__c = 'Letter',
            	JVCO_Print_Status__c = 'Printed',
            	JVCO_Inclusions__c = 'BRE Envelope 1',
            	JVCO_Recipient__c = con.id,
            	JVCO_Related_Account__c = acct.id,
            	JVCO_Related_Customer_Account__c = acct.JVCO_Customer_Account__c,	
            	CreatedDate = system.today()-10
        );
        insert DocQue;

        		
        Test.startTest();
		Database.executeBatch(new JVCO_LateDDIReminder());
        Test.stopTest();
        
        List<JVCO_Document_Queue__c> LateDDIs = [SELECT ID
                                         FROM JVCO_Document_Queue__c
                                         WHERE JVCO_Scenario__c = 'Late Direct Debit Instruction Return'
                                               AND JVCO_Transmission__c = 'Letter'
                                         LIMIT 1];
       	System.assertEquals(1,LateDDIs.size());
        
    }
    
    @isTest static void testCreateErrorLog(){
        JVCO_LateDDIReminder LateDDIReminder = new JVCO_LateDDIReminder();
        
        Test.startTest();
        insert LateDDIReminder.createErrorLog('Late DD','Error Message','JVCO_LateDDIReminder');
        Test.stopTest();
        
        List<ffps_custRem__Custom_Log__c> errorLog = [SELECT ID
                                         	FROM ffps_custRem__Custom_Log__c
                                         	WHERE ffps_custRem__Grouping__c = 'Late DD'
        									AND	ffps_custRem__Message__c = 'JVCO_LateDDIReminder'
                                         	LIMIT 1];
       	System.assertEquals(1,errorLog.size());
    }
}