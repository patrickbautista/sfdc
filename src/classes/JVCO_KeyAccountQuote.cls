/* -------------------------------------------------------------------------------------
    Name: JVCO_KeyAccountQuote
    Description: Class for creating blank quotes for JVCO_KeyAccountQuoteCreationBatch

    Date            Version     Author                Summary of Changes 
    -----------     -------     -----------------     ----------------------------------
    17-Nov-2017     0.1         jules.osberg.a.pablo  Intial creation
    15-Dec-2017     0.2         louis.a.del.rosario   Updated to display the failed renewal quotes in the email as a hyperlink.
------------------------------------------------------------------------------------- */
public class JVCO_KeyAccountQuote {

    public JVCO_KeyAccountQuote() {
        
    }

    public static integer createQuote(List<JVCO_Affiliation__c> affilationsForCreation, Id pricebookId, Date endDate, account accountRecord) {
        Integer insertedQuoteCount = 0;
        Map<Id, Opportunity> oppsMapToInsert = new Map<Id, Opportunity>();
        Map<Id, SBQQ__Quote__c> quotesMapToInsert = new Map<Id, SBQQ__Quote__c>();
        List<SBQQ__QuoteLineGroup__c> quoteLineGroupsToInsert = new List<SBQQ__QuoteLineGroup__c>();

        if(!affilationsForCreation.isEmpty()) {
            for(JVCO_Affiliation__c affRec : affilationsForCreation) {
                // creation of Opportunity
                Opportunity opp = new Opportunity();
                opp.AccountId = affRec.JVCO_Account__c;
                opp.Name = 'Acquisition Opportunity - ' + affRec.JVCO_Venue__r.Name;
                opp.CloseDate = System.today();
                opp.StageName = 'Quoting';
                opp.Pricebook2Id = pricebookId;
                oppsMapToInsert.put(affRec.Id, opp);
            }    
            try {      
                insert oppsMapToInsert.values();
            } catch (Exception ex){
                System.debug('###JVCO_KeyAccountQuoteCreation - Insert Opportunity: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
            }

            for(JVCO_Affiliation__c affRec : affilationsForCreation) {
                Opportunity opp = oppsMapToInsert.get(affRec.Id);
                // creation of Quote
                SBQQ__Quote__c quoteItem = new SBQQ__Quote__c();
                quoteItem.Start_Date__c = affRec.JVCO_Start_Date__c;
                quoteItem.JVCO_CurrentStartDate__c = affRec.JVCO_Start_Date__c;
                if (endDate == NULL || endDate < affRec.JVCO_Start_Date__c){
                    quoteItem.End_Date__c = affRec.JVCO_Start_Date__c.addDays(-1).addYears(1);
                    quoteItem.SBQQ__EndDate__c = affRec.JVCO_Start_Date__c.addDays(-1).addYears(1);
                }else{
                    quoteItem.End_Date__c = endDate;
                    quoteItem.SBQQ__EndDate__c = endDate;
                }            
                quoteItem.SBQQ__StartDate__c = affRec.JVCO_Start_Date__c;
                quoteItem.SBQQ__Primary__c = true;
                quoteItem.SBQQ__Opportunity2__c = opp.Id;
                quoteItem.SBQQ__Account__c = affRec.JVCO_Account__c;
                quotesMapToInsert.put(affRec.Id, quoteItem);
            }
            try {
                insert quotesMapToInsert.values();
                insertedQuoteCount = affilationsForCreation.Size();       
            } catch (Exception ex){
                System.debug('###JVCO_KeyAccountQuoteCreation - Insert Quotes: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
            }

            for(JVCO_Affiliation__c affRec : affilationsForCreation) {
                // Create Quote Line Groups
                SBQQ__Quote__c qRec = quotesMapToInsert.get(affRec.Id);
                SBQQ__QuoteLineGroup__c qlgRec = new SBQQ__QuoteLineGroup__c();
                qlgRec.Name = affRec.JVCO_Venue__r.Name;
                qlgRec.JVCO_Affiliated_Venue__c = affRec.Id;
                qlgRec.JVCO_Venue__c = affRec.JVCO_Venue__c;
                qlgRec.SBQQ__Account__c = qRec.SBQQ__Account__c;
                qlgRec.SBQQ__Number__c = 1;
                qlgRec.SBQQ__Quote__c = qRec.Id;
                qlgRec.SBQQ__ListTotal__c = 0;
                qlgRec.SBQQ__CustomerTotal__c = 0;
                qlgRec.SBQQ__NetTotal__c = 0;
                quoteLineGroupsToInsert.add(qlgRec);
            }
            try {
                insert quoteLineGroupsToInsert; 
            } catch (Exception ex){
                System.debug('###JVCO_KeyAccountQuoteCreation - Insert QLG: ' + ex.getMessage() + ex.getCause() + ex.getStackTraceString());
            }
        }   

        return insertedQuoteCount;   
    }

    public static void sendEmailNotification(account accountRecord, Integer quoteRecCount, Integer quoteErrRecCount, String errorMessage, Integer amendQuoteRecCount, Integer amendQuoteErrRecCount, String Email, Boolean isAmendment, Set<String> setContractIdFailed) {
        
        String quoteType = 'Amendment';
        String contractRecordURL = '';
        If(!isAmendment) {
            quoteType = 'Renewal';
        }
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {email});
        mail.setReplyTo('no-reply@salesforce.com');
        if(System.isQueueable()){

            mail.setSenderDisplayName('Queueable Processing');
            mail.setSubject('Generate ' + quoteType + ' Quotes Queueable Process Completed');
        }
        else{

            mail.setSenderDisplayName('Batch Processing');
            mail.setSubject('Generate ' + quoteType + ' Quotes Batch Process Completed');
        }

        String emailMsg = 
        '<html><body>The process to create ' + quoteType + ' quotes has now completed for ' + accountRecord.Name + ' including:'+
        '<br><br>' + quoteType + ' Quotes: ' + amendQuoteRecCount +
        '<br>New Quotes: ' + quoteRecCount +
        '<br>Total: ' + (quoteRecCount + amendQuoteRecCount);
        
        if((quoteErrRecCount + amendQuoteErrRecCount) > 0) {     
            emailMsg += '<br><br>Number of failures: ' + (quoteErrRecCount + amendQuoteErrRecCount) + '<br>Error: ' + errorMessage + '.';
        }
        
        //start GREEN-26655 15-12-2017 louis.a.del.rosario
        if(setContractIdFailed !=null){
            if(setContractIdFailed.size() > 0 ){
                emailMsg += '<br><br> List of Contracts that have failed: <br><br>';
                for(String i:setContractIdFailed){
                    emailMsg += '<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + i + '">'+URL.getSalesforceBaseUrl().toExternalForm() + '/' + i+'</a><br>'; 
                }
                emailMsg += '<br><br> Please tick the "Renewal Quoted" field on the contract. <br><br>';                
            }
        }
        
        //end GREEN-26655 15-12-2017 louis.a.del.rosario

        emailMsg += '</body></html>';

        mail.setHTMLBody(emailMsg);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    public static void updateAccountProcessingStatus(Account accountRecord,String apexJobField,String apexJobId,String timeStampField){
        accountRecord.put(apexJobField,apexJobId);
        accountRecord.put(timeStampField,datetime.now());
        update accountRecord;
    }
}