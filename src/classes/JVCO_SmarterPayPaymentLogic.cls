/* ----------------------------------------------------------------------------------------------
Name: JVCO_SmarterPayPaymentLogic.cls 
Description: Logic that creates Cash entry and cash entry line and match it to invoice(s)

Date         Version     Author              Summary of Changes 

-----------  -------     -----------------   -----------------------------------------
27-Mar-2020   0.1        patrick.t.bautista  Intial creation
15-Jul-2020   0.2        patrick.t.bautista  GREEN-35718 - process cash entry for iddh through CCSINs
19-Nov-2020   0.3        patrick.t.bautista  GREEN-36084 - fixed duplicate cash entry lines if there's multiple DDInvGrp
----------------------------------------------------------------------------------------------- */
public class JVCO_SmarterPayPaymentLogic {
    public static c2g__codaCashEntry__c cashEntry = new c2g__codaCashEntry__c();
    public static List<paymentWrapper> paymentToProcessList = new List<paymentWrapper>();
    public static List<paymentWrapper> refundPaymentToProcessList = new List<paymentWrapper>();
    public static List<c2g__codaCashEntry__c> refundCashList = new List<c2g__codaCashEntry__c>();
    public static Set<Id> refundCashSet = new Set<Id>();
    public static Boolean processPayment(String paymentProcess, List<sObject> paymentList)
    {
        List<c2g__codaCashEntryLineItem__c> itemList = new List<c2g__codaCashEntryLineItem__c>();
        //get invoice group if direct debit or salesinvoice if card payment
        getInvoiceGroupOrRelatedInvoice(paymentList, paymentProcess);
        
        //Process payments as per payment method
        if(!paymentToProcessList.isEmpty()){
            if(createCashEntry(paymentProcess)){
                for(paymentWrapper payment : paymentToProcessList)
                {
                    c2g__codaCashEntryLineItem__c cashEntryLine = getCashEntryLine(cashEntry, payment);
                    itemList.add(cashEntryLine);
                }        
            }
        }
        if(!refundPaymentToProcessList.isEmpty()){
            if(createRefundCashEntry(refundPaymentToProcessList)){
                for(c2g__codaCashEntry__c cashEntry : refundCashList){
                    refundCashSet.add(cashEntry.Id);
                    for(paymentWrapper payment : refundPaymentToProcessList)
                    {
                        if(cashEntry.JVCO_Income_Debit_History__c == payment.IncomeCCDDHistoryId){
                            c2g__codaCashEntryLineItem__c cashEntryLine = getCashEntryLine(cashEntry, payment);
                            itemList.add(cashEntryLine);
                        }
                    }
                }
            }
        }
        if(!itemList.isEmpty()){
            insert itemList;
            return true;
        }
        return false;
    }
    
    public class paymentWrapper{
        //Store Income Direct Debit History and Income Card Payment history
        public JVCO_Invoice_Group__c invoiceGroup {get;set;}
        public Double outstandingValue {get;set;}
        public String invoiceName {get;set;}
        public Double invoiceOutsVal {get;set;}
        public Id IncomeCCDDHistoryId {get;set;}
        public Double invGrpTotalAmount {get;set;}
        public Double cashentryLineAmount {get;set;}
        public Id AccountId {get;set;}
        public Date payDate {get;set;}
        public String vendorTransId {get;set;}
    }
    
    private static boolean createCashEntry(String paymentProcess)
    {
        cashEntry = new c2g__codaCashEntry__c();
        cashEntry.c2g__Date__c = Date.today();
        cashEntry.c2g__PaymentMethod__c = (paymentProcess.equals('Direct Debit'))?'Direct Debit':'Debit/Credit Card';
        cashEntry.ffcash__DerivePeriod__c = TRUE;
        insert cashEntry;
        return true;
    }
    
    private static boolean createRefundCashEntry(List<paymentWrapper> refundPaymentToProcessList)
    {
        for(paymentWrapper payWrap : refundPaymentToProcessList){
            c2g__codaCashEntry__c refundCash = new c2g__codaCashEntry__c();
            refundCash.c2g__Account__c = paywrap.AccountId;
            refundCash.c2g__Date__c = Date.today();
            refundCash.JVCO_Income_Debit_History__c = paywrap.IncomeCCDDHistoryId;
            refundCash.ffcash__DerivePeriod__c = true;
            refundCash.c2g__Type__c = 'Refund';
            refundCash.JVCO_Receipt_reversal_failed__c = true;
            refundCash.c2g__PaymentMethod__c = 'Direct Debit';
            refundCashList.add(refundCash);
        }
        insert refundCashList;
        return true;
    }
    
    private static c2g__codaCashEntryLineItem__c getCashEntryLine(c2g__codaCashEntry__c cashEntry, paymentWrapper payment)
    {
        c2g__codaCashEntryLineItem__c cashEntryLI = new c2g__codaCashEntryLineItem__c();
        cashEntryLI.c2g__CashEntry__c = cashEntry.Id;
        cashEntryLI.c2g__Account__c = payment.AccountId;
        cashEntryLI.c2g__LineDescription__c = payment.invoiceOutsVal < payment.cashentryLineAmount ? 'Not for Matching' : payment.vendorTransId;
        cashEntryLI.c2g__CashEntryValue__c = payment.cashentryLineAmount;
        cashEntryLI.c2g__AccountPaymentMethod__c = cashEntry.c2g__PaymentMethod__c;
        cashEntryLI.c2g__AccountReference__c = payment.invoiceName;
        cashEntryLI.JVCO_Income_Debit_History__c = cashEntry.c2g__PaymentMethod__c == 'Direct Debit' ? payment.IncomeCCDDHistoryId : null;
        cashEntryLI.JVCO_Income_Card_Payment_History__c = cashEntry.c2g__PaymentMethod__c == 'Direct Debit' ? null: payment.IncomeCCDDHistoryId;
        cashEntryLI.ffcash__DeriveLineNumber__c = TRUE;
        cashEntryLI.JVCO_Payment_Status__c = 'Completed';
        cashEntryLI.JVCO_Payonomy_Pay_Date__c = payment.payDate;
        return cashEntryLI;
    }
    
    public static void getInvoiceGroupOrRelatedInvoice(List<sObject> paymentList, String paymentProcess){
        Map<Id,Income_Debit_History__c> incomeDDMap;
        Map<Id,Income_Card_Payment_History__c> incomeCCMap;
        Map<Id,Set<Id>> incomeDDCCToDDCCHistoryMap = new Map<Id,Set<Id>>();
        Set<Income_Debit_History__c> processRefundforDDICASet = new Set<Income_Debit_History__c>();
        if(paymentProcess.equals('Direct Debit'))
        {//Direct debit 
            incomeDDMap = new Map<Id,Income_Debit_History__c>((List<Income_Debit_History__c>)paymentList);
            for(Income_Debit_History__c incomeDD : incomeDDMap.values()){
                if(incomeDD.Amount__c > 0){
                    if(!incomeDDCCToDDCCHistoryMap.containsKey(incomeDD.Income_Direct_Debit__c)){
                        incomeDDCCToDDCCHistoryMap.put(incomeDD.Income_Direct_Debit__c, new Set<id>());
                    }
                    incomeDDCCToDDCCHistoryMap.get(incomeDD.Income_Direct_Debit__c).add(incomeDD.Id);
                }else if(incomeDD.Amount__c < 0 && incomeDD.DD_Code__c != null && incomeDD.DD_Code__c.toLowerCase().contains('ddica')){
                    processRefundforDDICASet.add(incomeDD);
                }
            }
            processDDRefundPayment(processRefundforDDICASet);
            processDirectDebit(incomeDDCCToDDCCHistoryMap, incomeDDMap);
        }else{
            //credit card
            incomeCCMap = new Map<Id,Income_Card_Payment_History__c>((List<Income_Card_Payment_History__c>)paymentList);
            
            for(Income_Card_Payment_History__c incomeCC : incomeCCMap.values()){
                if(!incomeDDCCToDDCCHistoryMap.containsKey(incomeCC.Income_Card_Payment__c)){
                    incomeDDCCToDDCCHistoryMap.put(incomeCC.Income_Card_Payment__c, new Set<Id>());
                }
                incomeDDCCToDDCCHistoryMap.get(incomeCC.Income_Card_Payment__c).add(incomeCC.Id);
            }
            processCreditCard(incomeDDCCToDDCCHistoryMap, incomeCCMap);
        }
    }
    
    public static void processCreditCard(Map<Id,Set<Id>> incomeDDCCToDDCCHistoryMap, Map<Id,Income_Card_Payment_History__c> incomeCCMap){
        Map<Id, Set<Id>> invoiceGroupToPaymentMap = new Map<Id, Set<Id>>();
        List<String> invoiceName = new List<String>();
        paymentWrapper paywrap;
        //Get invoice group of income card
        for(JVCO_Invoice_Group__c invoiceGroupCC : [SELECT id, Income_Card_Payment__c,
                                                    CC_SINs__c
                                                    FROM JVCO_Invoice_Group__c
                                                    WHERE Income_Card_Payment__c IN: incomeDDCCToDDCCHistoryMap.keyset()])
        {
            invoiceGroupToPaymentMap.put(invoiceGroupCC.id, incomeDDCCToDDCCHistoryMap.get(invoiceGroupCC.Income_Card_Payment__c));
            if(invoiceGroupCC.CC_SINs__c != null){
                invoiceName.addAll(invoiceGroupCC.CC_SINs__c.split(','));
            }
        }
        
        if(!invoiceGroupToPaymentMap.isEmpty())
        {
            Map<String, Double> invoiceNameToOutsValMap = invoiceMapping(invoiceName);
            
            //Get CC mapping on related invoice group
            for(JVCO_CC_Outstanding_Mapping__c ccOuts :  [SELECT Id, JVCO_CC_Invoice_Group__c, 
                                                          JVCO_CC_Outstanding_Value__c,
                                                          JVCO_Sales_Invoice_Name__c,
                                                          JVCO_CC_Invoice_Group__r.JVCO_Total_Amount__c
                                                          FROM JVCO_CC_Outstanding_Mapping__c
                                                          WHERE JVCO_CC_Invoice_Group__c IN : invoiceGroupToPaymentMap.keySet()
                                                          AND JVCO_CC_Outstanding_Value__c > 0])
            {
                for(Id payHistory : invoiceGroupToPaymentMap.get(ccOuts.JVCO_CC_Invoice_Group__c)){
                    paywrap = new paymentWrapper();
                    paywrap.invoiceGroup = new JVCO_Invoice_Group__c(Id = ccOuts.JVCO_CC_Invoice_Group__c);
                    paywrap.invoiceName = ccOuts.JVCO_Sales_Invoice_Name__c;
                    paywrap.outstandingValue = ccOuts.JVCO_CC_Outstanding_Value__c;
                    paywrap.invoiceOutsVal = invoiceNameToOutsValMap != null ? invoiceNameToOutsValMap.get(paywrap.invoiceName) : 0;
                    paywrap.IncomeCCDDHistoryId = payHistory;
                    paywrap.invGrpTotalAmount = ccOuts.JVCO_CC_Invoice_Group__r.JVCO_Total_Amount__c;
                    paywrap.cashentryLineAmount = ((incomeCCMap.get(payHistory).Amount__c / paywrap.invGrpTotalAmount) * paywrap.outstandingValue).setScale(2);
                    paywrap.AccountId = incomeCCMap.get(payHistory).Income_Card_Payment__r.Account__c;
                    DateTime CCdateTime = incomeCCMap.get(payHistory).Authorisation_Date__c;
                    paywrap.payDate = Date.newInstance(CCdateTime.year(), CCdateTime.month(), CCdateTime.day());
                    paywrap.vendorTransId = incomeCCMap.get(payHistory).Sagepay_Transaction_ID__c;
                    paymentToProcessList.add(paywrap);
                }
            }
        }
    }
    public static void processDirectDebit(Map<Id,Set<Id>> incomeDDCCToDDCCHistoryMap, Map<Id,Income_Debit_History__c> incomeDDMap){
        Map<Id, Set<Id>> invoiceGroupToPaymentMap = new Map<Id, Set<Id>>();
        Map<String, Set<Id>> invoiceNameToInvoiceGroupMap = new Map<String, Set<Id>>();
        Map<Id,List<String>> invGrpToInvNameMap = new Map<Id,List<String>>();
        //Get invoice group related to income dd
        paymentWrapper paywrap;
        for(SMP_DirectDebit_GroupInvoice__c invoiceGroupDD : [SELECT Invoice_Group__c,
                                                              Income_Direct_Debit__c,
                                                              Income_Direct_Debit__r.Number_Of_Payments__c,
                                                              Invoice_Group__r.JVCO_Total_Amount__c,
                                                              Invoice_Group__r.CC_SINs__c
                                                              FROM SMP_DirectDebit_GroupInvoice__c
                                                              WHERE Income_Direct_Debit__c IN: incomeDDCCToDDCCHistoryMap.keySet()
                                                              AND Invoice_Group__c != null])
        {
            for(String ccsins : invoiceGroupDD.Invoice_Group__r.CC_SINs__c.split(',')){
                if(!invoiceNameToInvoiceGroupMap.containsKey(ccsins)){
                    invoiceNameToInvoiceGroupMap.put(ccsins, new Set<Id>());
                }
                invoiceNameToInvoiceGroupMap.get(ccsins).add(invoiceGroupDD.Invoice_Group__c);
            }
            invoiceGroupToPaymentMap.put(invoiceGroupDD.Invoice_Group__c, incomeDDCCToDDCCHistoryMap.get(invoiceGroupDD.Income_Direct_Debit__c));
        }
        
        Map<Id, List<c2g__codaInvoiceInstallmentLineItem__c>> iDDHToITLMap = new Map<Id, List<c2g__codaInvoiceInstallmentLineItem__c>>();
        
        if(!invoiceGroupToPaymentMap.isEmpty()){
            for(c2g__codaInvoiceInstallmentLineItem__c itl : [SELECT Id, 
                                                              c2g__Amount__c,
                                                              c2g__DueDate__c,
                                                              JVCO_DD_Failed__c,
                                                              c2g__Invoice__r.Name,
                                                              c2g__Invoice__r.c2g__Account__c,
                                                              c2g__Invoice__r.JVCO_Invoice_Group__c,
                                                              c2g__Invoice__r.c2g__OutstandingValue__c
                                                              FROM c2g__codaInvoiceInstallmentLineItem__c
                                                              WHERE c2g__Invoice__r.Name IN: invoiceNameToInvoiceGroupMap.keySet()
                                                              AND JVCO_Payment_Status__c <> 'Paid'
                                                              AND c2g__Amount__c != 0])
            {
                for(Id invGroup : invoiceNameToInvoiceGroupMap.get(itl.c2g__Invoice__r.Name)){
                    for(Id iddh : invoiceGroupToPaymentMap.get(invGroup)){
                        if((incomeDDMap.get(iddh).DD_Collection_Date__c == itl.c2g__DueDate__c)
                           || (incomeDDMap.get(iddh).DD_Status__c != null 
                               && incomeDDMap.get(iddh).DD_Status__c.contains('Represent') 
                               && itl.JVCO_DD_Failed__c))
                        {
                            if(!iDDHToITLMap.containsKey(iddh)){
                                iDDHToITLMap.put(iddh, new List<c2g__codaInvoiceInstallmentLineItem__c>());
                            }
                            if(!iDDHToITLMap.get(iddh).contains(itl)){
                                iDDHToITLMap.get(iddh).add(itl);
                            }
                        }
                    }
                }
            }
            if(!iDDHToITLMap.isEmpty()){
                for(id iddh : iDDHToITLMap.keyset()){
                    for(c2g__codaInvoiceInstallmentLineItem__c tli : iDDHToITLMap.get(iddh)){
                        paywrap = new paymentWrapper();
                        paywrap.invoiceOutsVal = tli.c2g__Invoice__r.c2g__OutstandingValue__c;
                        paywrap.invoiceName = tli.c2g__Invoice__r.Name;
                        paywrap.IncomeCCDDHistoryId = iddh;
                        paywrap.AccountId = tli.c2g__Invoice__r.c2g__Account__c;
                        paywrap.payDate = incomeDDMap.get(iddh).DD_Collection_Date__c;
                        paywrap.cashentryLineAmount = tli.c2g__Amount__c;
                        paymentToProcessList.add(paywrap);
                    }
                }
            }
        }
    }
    
    public static void processDDRefundPayment(Set<Income_Debit_History__c> processRefundforDDICASet){
        for(Income_Debit_History__c incomeDD : processRefundforDDICASet){
            paymentWrapper paywrap = new paymentWrapper();
            paywrap.invoiceOutsVal = 0;
            paywrap.invoiceName = null;
            paywrap.IncomeCCDDHistoryId = incomeDD.Id;
            paywrap.AccountId = incomeDD.Income_Direct_Debit__r.Account__c;
            paywrap.payDate = incomeDD.DD_Collection_Date__c;
            paywrap.cashentryLineAmount = Math.abs(incomeDD.Amount__c);
            refundPaymentToProcessList.add(paywrap);
        }
    }
        
    public static Map<String, Double> invoiceMapping(List<String> invoiceName){
        Map<String, Double> invoiceNameToInvoiceMap = new Map<String, Double>();
        for(c2g__codaInvoice__c invoice : [SELECT Name, c2g__OutstandingValue__c
                                           FROM c2g__codaInvoice__c
                                           WHERE NAME IN: invoiceName])
        {
            invoiceNameToInvoiceMap.put(invoice.Name, invoice.c2g__OutstandingValue__c);
        }
        return invoiceNameToInvoiceMap;
    }
}