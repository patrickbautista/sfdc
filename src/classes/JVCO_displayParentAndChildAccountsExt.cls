/* -------------------------------------------------------------------------------------
    Name: JVCO_DisplayParentAndChildAccountsExtension
    Description: Extension class for JVCO_DisplayParentAndChildAccountsExtension page

    Date            Version     Author                Summary of Changes 
    -----------     -------     -----------------     ----------------------------------
    12-Oct-2018     0.1         joseph.g.barrameda    Intial creation
------------------------------------------------------------------------------------- */


public class JVCO_displayParentAndChildAccountsExt{

    public Account accountRecord;
    public String displayText {get;set;}
    public List<Account> parentAccountList {get;set;}
    
    public JVCO_displayParentAndChildAccountsExt(ApexPages.StandardController stdController){
        accountRecord = (Account)stdController.getRecord();
        accountRecord= [SELECT Id, ParentID, Name FROM Account WHERE Id=:accountRecord.Id];
        parentAccountList =[SELECT Id, Name, ParentID FROM Account WHERE ParentId =: accountRecord.Id];
        
        if(parentAccountList.size()> 0 && accountRecord.ParentId != null ){
            displayText='Parent/Child';
        }else if(parentAccountList.size()> 0 && accountRecord.ParentId == null){
            displayText='Parent';
        }else if(parentAccountList.size()==0 && accountRecord.ParentId != null){    
            displayText='Child';        
        }
    }
    
}