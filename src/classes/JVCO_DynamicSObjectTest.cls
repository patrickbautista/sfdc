/* ----------------------------------------------------------------------------------------------
Author: Recuerdo Bregente
Company: Accenture
Description: Test Class for JVCO_DynamicSObject
<Date>      <Authors Name>       <Brief Description of Change> 
23-Aug-2017 Recuerdo Bregente  Initial version
----------------------------------------------------------------------------------------------- */
@isTest
private class JVCO_DynamicSObjectTest {
   private static List<Account> licAccounts;
  
  @testSetup static void setUpTestData(){
        
    Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='JVCO_Venue__c'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q2);
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }

        JVCO_Venue__c venue = new JVCO_Venue__c();
        venue.Name = 'test venue 1';
        venue.JVCO_Field_Visit_Requested__c = false;
        venue.JVCO_Street__c = 'testStreet';
        venue.JVCO_City__c = 'testCoty';
        venue.JVCO_Postcode__c ='TN32 4SL';
        venue.JVCO_Country__c ='UK';

        insert venue;
        
        JVCO_Venue__c venue2 = new JVCO_Venue__c();
        venue2.Name = 'update venue';
        venue2.JVCO_Field_Visit_Requested__c = false;
        venue2.JVCO_Street__c = 'updateStreet';
        venue2.JVCO_City__c = 'updateCoty';
        venue2.JVCO_Postcode__c ='TN32 4SL';
        venue2.JVCO_Country__c ='UK';

        insert venue2;
        
        //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc = new c2g__codaGeneralLedgerAccount__c();
        testGeneralLedgerAcc.c2g__AdjustOperatingActivities__c = false;
        testGeneralLedgerAcc.c2g__AllowRevaluation__c = false;
        testGeneralLedgerAcc.c2g__CashFlowCategory__c = 'Operating Activities';
        testGeneralLedgerAcc.c2g__CashFlowLineSummary__c = 'Increase / (Decrease) in Other Current Liabilities';
        testGeneralLedgerAcc.c2g__GLAGroup__c = 'Accounts Receivable';
        testGeneralLedgerAcc.c2g__ReportingCode__c = '10020';
        testGeneralLedgerAcc.c2g__TrialBalance1__c = 'Assets';
        testGeneralLedgerAcc.c2g__TrialBalance2__c = 'Current Assets';
        testGeneralLedgerAcc.c2g__TrialBalance3__c = 'Accounts Receivables';
        testGeneralLedgerAcc.c2g__Type__c = 'Balance Sheet';
        testGeneralLedgerAcc.c2g__UnitOfWork__c = 1.0;
        testGeneralLedgerAcc.Dimension_1_Required__c = false;
        testGeneralLedgerAcc.Dimension_2_Required__c = true;
        testGeneralLedgerAcc.Dimension_3_Required__c = false;
        testGeneralLedgerAcc.Dimension_4_Required__c = false;
        testGeneralLedgerAcc.Name = '10020 - Cash at bank and in hand';
        testGeneralLedgerAcc.ownerid = testGroup.Id;
        testGeneralLedgerAcc.c2g__ReportingCode__c = '30030';
        insert testGeneralLedgerAcc;
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedgerAcc.id);
        insert taxCode;

        //Create Account
        Account testAccCust = new Account(Name='Test Customer Account');
        testAccCust.AccountNumber = '999999';
        testAccCust.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust.Type='Customer';
        testAccCust.JVCO_Additional_Information__c = '';
        testAccCust.JVCO_Date_TCs_Accepted__c = System.Today().addDays(1);
        testAccCust.RecordTypeId = accountRecordTypes.get('Customer Account');
        testAccCust.AnnualRevenue=100000;
        testAccCust.Description = 'Description';
        insert testAccCust;
        
        Account testAccCust2 = new Account(Name='Test Customer Account 2');
        testAccCust2.AccountNumber = '2222222';
        testAccCust2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testAccCust2.Type='Customer';
        testAccCust2.JVCO_Additional_Information__c = '';
        testAccCust2.JVCO_Date_TCs_Accepted__c = System.Today().addDays(1);
        testAccCust2.RecordTypeId = accountRecordTypes.get('Customer Account');
        testAccCust2.AnnualRevenue=100000;
        testAccCust2.Description = 'Description';
        insert testAccCust2;

         Contact c = JVCO_TestClassObjectBuilder.createContact(testAccCust.id);
        insert c;

        Contact con = JVCO_TestClassObjectBuilder.createContact(testAccCust2.id);
        con.LastName = 'LastName';
        con.FirstName = 'FirstName';
        con.Email = 'test@hotmail.com';
        insert con;

        
        Account testLicAcc1 = new Account(Name='Test Lic Account 1');
        testLicAcc1.AccountNumber = '987654';
        testLicAcc1.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testLicAcc1.c2g__CODAAllowDeleteInUse__c = false;
        testLicAcc1.c2g__CODACreditLimitEnabled__c = false;
        testLicAcc1.c2g__CODACreditStatus__c = 'Credit Allowed';
        testLicAcc1.c2g__CODADaysOffset1__c = 0.0;
        testLicAcc1.c2g__CODADiscount1__c = 0.0;
        testLicAcc1.c2g__CODAFederallyReportable1099__c = false;
        testLicAcc1.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testLicAcc1.c2g__CODAIntercompanyAccount__c = false;
        testLicAcc1.c2g__CODASalesTaxStatus__c = 'Taxable';
        testLicAcc1.JVCO_Customer_Account__c = testAccCust.id;
        testLicAcc1.RecordTypeId = accountRecordTypes.get('Licence Account');
        testLicAcc1.c2g__CODAOutputVATCode__c = taxCode.id;
        testLicAcc1.JVCO_Preferred_Contact_Method__c = 'Email';
        testLicAcc1.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        testLicAcc1.JVCO_Billing_Contact__c = c.id;
        testLicAcc1.JVCO_Licence_Holder__c = c.id;
        testLicAcc1.JVCO_Review_Contact__c = c.id;
        insert testLicAcc1;
        
        Account testLicAcc2 = new Account(Name='Test Lic Account 2');
        testLicAcc2.AccountNumber = '9876543';
        testLicAcc2.c2g__CODAAccountsReceivableControl__c = testGeneralLedgerAcc.id;
        testLicAcc2.c2g__CODAAllowDeleteInUse__c = false;
        testLicAcc2.c2g__CODACreditLimitEnabled__c = false;
        testLicAcc2.c2g__CODACreditStatus__c = 'Credit Allowed';
        testLicAcc2.c2g__CODADaysOffset1__c = 0.0;
        testLicAcc2.c2g__CODADiscount1__c = 0.0;
        testLicAcc2.c2g__CODAFederallyReportable1099__c = false;
        testLicAcc2.c2g__CODAIncomeTaxType__c = 'Not Reportable';
        testLicAcc2.c2g__CODAIntercompanyAccount__c = false;
        testLicAcc2.c2g__CODASalesTaxStatus__c = 'Taxable';
        testLicAcc2.JVCO_Customer_Account__c = testAccCust2.id;
        testLicAcc2.RecordTypeId = accountRecordTypes.get('Licence Account');
        testLicAcc2.c2g__CODAOutputVATCode__c = taxCode.id;
        testLicAcc2.JVCO_Preferred_Contact_Method__c = 'Email';
        testLicAcc2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        testLicAcc2.JVCO_Billing_Contact__c = con.id;
        testLicAcc2.JVCO_Licence_Holder__c = con.id;
        testLicAcc2.JVCO_Review_Contact__c = con.id;
        insert testLicAcc2;
        
        licAccounts = new List<Account>();
        licAccounts.add(testLicAcc1);
        licAccounts.add(testLicAcc2);
        
    }
    
    @isTest static void testConstructor()
    {
        List<Account> accList = [SELECT Id, Name, JVCO_Additional_Information__c, JVCO_Date_TCs_Accepted__c, c2g__CODAAccountsReceivableControl__c 
                                FROM Account WHERE RecordType.Name = 'Customer Account' LIMIT 2];
        System.assert(accList.size() > 0);
        Test.startTest();
        
        long now = System.currentTimeMillis();
        
        
        
        JVCO_QueryBuilder queryBuilder = new JVCO_QueryBuilder();
        queryBuilder.objectName = 'Account';
        queryBuilder.isValidObject('Account');
        queryBuilder.setObjectFields('JVCO_Venue__c', 'Licence Account');
        queryBuilder.setObjectFields('Account', 'Licence Account');
        queryBuilder.setObjectFields('Account', 'Customer Account');
        queryBuilder.isValidField('Name');
        queryBuilder.setFieldName(1, 'Name');
        queryBuilder.setOperatorValue(1, 'equals');
        queryBuilder.setInputValue(1, accList[0].Name);
        queryBuilder.setFieldName(2, 'CreatedDate');
        queryBuilder.setOperatorValue(2, 'greater or equal');
        queryBuilder.setInputValue(2, DateTime.newInstance(now).format('MM/dd/yyyy'));
        queryBuilder.setFieldName(3, 'AnnualRevenue');
        queryBuilder.setOperatorValue(3, 'equals');
        queryBuilder.setInputValue(3, '10000');
        queryBuilder.setFieldName(4, 'c2g__CODAAccountsReceivableControl__c');
        queryBuilder.setOperatorValue(4, 'not equal to');
        queryBuilder.setInputValue(4, '');
       	queryBuilder.setFieldName(4, 'Description');
        queryBuilder.setOperatorValue(4, 'not equal to');
        queryBuilder.setInputValue(4, '');
		queryBuilder.setFieldName(5, 'JVCO_Customer_Account__c');
        queryBuilder.setOperatorValue(5, 'not equal to');
        queryBuilder.setInputValue(5, accList[1].id);
        
        queryBuilder.objectFields.add('CreatedDate');
        queryBuilder.objectFields.add('AnnualRevenue');
        queryBuilder.objectFields.add('c2g__CODAAccountsReceivableControl__c');
        queryBuilder.objectFields.add('Description');
		queryBuilder.objectFields.add('JVCO_Customer_Account__c');
        String queryStr = queryBuilder.getQueryString(TRUE);
        queryStr = queryStr.remove('OR ((Description != null))');
        List<SObject> sobjectResults = Database.query(queryStr);
        List<JVCO_DynamicSObject> results = new List<JVCO_DynamicSObject>();
        
        for (SObject s : sobjectResults) {
            results.add(new JVCO_DynamicSObject(s, queryBuilder.objectName, queryBuilder.getQueryFields(), queryBuilder.getReferenceFields()));
        }
        
        results[0].setSelected(true);
        System.assert(results[0].getSelected());
        
        System.assert(results[0].getID() != null);
        System.assert(results[0].getDisplayID() != null);
        System.assert(results[0].getIdRefUrl() != null);
        System.assert(results[0].getIdRefName() != null);
        System.assert(results[0].getIsPersonAcc() != null);
        System.assert(results[0].getSObject() != null);
        System.assert(results[0].getField('Name') != null);
        System.assert(results[0].getField1() != null);
        System.assert(results[0].getField2() != null);
        System.assert(results[0].getField3() != null);
        System.assert(results[0].getField4() != null);
        System.assert(results[0].getField5() != null);
        System.assert(results[0].hasField('Name'));
        System.assert(results[0].getFields() != null);
        results[0].selectAllFields(TRUE);
        results[0].selectField('Name',TRUE);
        System.assert(results[0].getReferenceFields() != null);
        
        System.assert(results[0].getFields().size() > 0);
        for(Integer i = 0; i<results[0].getFields().size(); i++){
            results[0].getFields()[i].setSelected(TRUE);
            results[0].getFields()[i].getIsChangeable();
            
            System.assert(results[0].getFields()[i].getSelected());
            results[0].getFields()[i].getDisplayValue();
            results[0].getFields()[i].getRefUrl();
            results[0].getFields()[i].getRefName();
            if(results[0].getFields()[i].getIsReference()){
                results[0].getFields()[i].setValue(accList[0].c2g__CODAAccountsReceivableControl__c);
            }
        }
        
        Test.stopTest();
    }
}