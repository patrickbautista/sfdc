/* ----------------------------------------------------------------------------------------------
Name: JVCO_TaskLinkToCaseAndContact.cls 
Description: Batch class that handles Tasks Linking to the correct Case and Contact

Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
09-Nov-2016  0.1         kristoffer.d.martin   Intial creation
22-Jan-2018  0.2         desiree.m.quijada     GREEN-28572
----------------------------------------------------------------------------------------------- */
global class JVCO_TaskLinkToCaseAndContact implements Database.Batchable<sObject>, Database.Stateful {
  
    public Map <id , string> FailedTask= new Map <id , string>();

    public String query;
  
    /* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Constructor
    Inputs: Integer limitNumber
    Returns:
    <Date>      <Authors Name>       <Brief Description of Change> 
    11-Nov-2016 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    global JVCO_TaskLinkToCaseAndContact(integer limitNumber) {
        loadQuery() ;
        query += ' Limit ' + limitnumber;
    }

    /* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Constructor
    Inputs:
    Returns:
    <Date>      <Authors Name>       <Brief Description of Change> 
    11-Nov-2016 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    global JVCO_TaskLinkToCaseAndContact(){
        loadQuery() ;
    }

    /* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: creates the query
    Inputs: 
    Returns: void
    <Date>      <Authors Name>       <Brief Description of Change> 
    11-Nov-2016 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    void loadQuery()
    {
  
        Integer year = 2016;
        Integer month = 5 ;
        Integer dateD = 24;
        Integer hour = 0;
        Integer minute = 0;
        Integer second  = 0;
        
        dateTime DT = Datetime.newInstance(year,month,dateD,hour,minute,second);
        
        string DTstring = DT.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
          
        query = 'select id , lastModifiedDate,  JVCO_Temp_Case_External_Id__c, JVCO_Temp_Contact_External_Id__c, OwnerId , WhoId , WhatId  from Task  ' ;
        query +=  ' where ( ( JVCO_Temp_Case_External_Id__c != null ) OR ( JVCO_Temp_Contact_External_Id__c != null ) )'; 
        query +=  ' AND lastmodifieddate >='+ DTstring  ;

    }

    /* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Standard start method for batch
    Inputs: Database.BatchableContext BC
    Returns: void
    <Date>      <Authors Name>       <Brief Description of Change> 
    11-Nov-2016 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('query --->'+query);
        return Database.getQueryLocator(query);
    }

    /* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Standard execute method for batch
    Inputs: Database.BatchableContext BC and returned list from the query
    Returns: void
    <Date>      <Authors Name>       <Brief Description of Change> 
    11-Nov-2016 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    global void execute(Database.BatchableContext BC, List<Task> scope) 
    {
    
        List<Task> taskToUpdate = new List<Task>();
        Map<String, Id> mapCaseExternalIdToCaseId = new Map<String, Id>();
        Map<String, Id> mapContactExternalIdToContactId = new Map<String, Id>();
        Map<String, Id> mapCaseExternalIdToLicAccountId = new Map<String, Id>();

        for (Task t : scope) 
        {
          if ( String.isNotEmpty(T.JVCO_Temp_Case_External_Id__c)  &&  string.isNotBlank( T.JVCO_Temp_Case_External_Id__c)  )
          {
                    mapCaseExternalIdToCaseId.put(T.JVCO_Temp_Case_External_Id__c, null) ;
                    mapCaseExternalIdToLicAccountId.put(T.JVCO_Temp_Case_External_Id__c, null);
                }

                if ( String.isNotEmpty(T.JVCO_Temp_Contact_External_Id__c)  &&  string.isNotBlank( T.JVCO_Temp_Contact_External_Id__c)  )
          {
                    mapContactExternalIdToContactId.put(T.JVCO_Temp_Contact_External_Id__c, null) ;
                }
        }
        if (mapCaseExternalIdToCaseId.keySet().isEmpty() == false) 
        {
                for (Case c : [Select Id, JVCO_Temp_Case_External_Id__c From Case Where JVCO_Temp_Case_External_Id__c IN :mapCaseExternalIdToCaseId.keySet()] )
                {
                    mapCaseExternalIdToCaseId.put(c.JVCO_Temp_Case_External_Id__c, c.Id) ;
                }
        }
        //GREEN-28572 
        if(mapCaseExternalIdToLicAccountId.keySet().isEmpty() == false)
        {
            
                for(Account a : [SELECT id, JVCO_Merge_Id__c FROM ACCOUNT WHERE JVCO_Merge_Id__c IN :mapCaseExternalIdToLicAccountId.keySet()])
                {
                    mapCaseExternalIdToLicAccountId.put(a.JVCO_Merge_Id__c, a.Id);
                }
        }
        //
        if (mapContactExternalIdToContactId.keySet().isEmpty() == false) 
        {
            for (Contact c : [Select Id, JVCO_Merge_Id__c From Contact Where JVCO_Merge_Id__c IN :mapContactExternalIdToContactId.keySet()] )
            {
                mapContactExternalIdToContactId.put(c.JVCO_Merge_Id__c, c.Id) ;
            }
            
        }
        system.debug('mapCaseExternalIdToCaseId-->'+mapCaseExternalIdToCaseId);

        for (Task t : scope) 
        {
        system.debug('KCM: task in scope-->'+T.Id); 
            system.debug('KCM: task in scope-prior Whoid->'+T.WhoId);
            system.debug('KCM: task in scope-prior Ownerid->'+T.OwnerId);
            system.debug('KCM: task in scope-prior whatid->'+T.WhatId);

            boolean addtoList = false;
            if (string.isNotEmpty(T.JVCO_Temp_Case_External_Id__c) && string.isNotBlank( T.JVCO_Temp_Case_External_Id__c))
            {
                if (mapCaseExternalIdToCaseId.containsKey(T.JVCO_Temp_Case_External_Id__c) && mapCaseExternalIdToCaseId.get( T.JVCO_Temp_Case_External_Id__c) != null) 
                {
                    T.WhatId =  mapCaseExternalIdToCaseId.get( T.JVCO_Temp_Case_External_Id__c)  ;
                    system.debug('KCM: task in scope-updated--------- --WhatId->' + T.WhatId);
                    addtoList = true;
                }
                if(mapCaseExternalIdToLicAccountId.containsKey(T.JVCO_Temp_Case_External_Id__c) && mapCaseExternalIdToLicAccountId.get( T.JVCO_Temp_Case_External_Id__c) != null) 
                {
                    T.WhatId =  mapCaseExternalIdToLicAccountId.get( T.JVCO_Temp_Case_External_Id__c)  ;
                    system.debug('KCM: task in scope-updated--------- --WhatId->' + T.WhatId);
                    addtoList = true;
                }
            }
            if (string.isNotEmpty(T.JVCO_Temp_Contact_External_Id__c ) && string.isNotBlank( T.JVCO_Temp_Contact_External_Id__c ))
            {
                if (mapContactExternalIdToContactId.containsKey(T.JVCO_Temp_Contact_External_Id__c) && mapContactExternalIdToContactId.get(T.JVCO_Temp_Contact_External_Id__c) != null) 
                {
                    T.Whoid =  mapContactExternalIdToContactId.get(T.JVCO_Temp_Contact_External_Id__c )  ;
                    system.debug('KCM: task in scope-updated--------- --Whoid->' + T.Whoid);
                    addtoList = true;
                }
            }
            system.debug('KCM: task in scope-Whoid->'+T.WhoId);
            system.debug('KCM: task in scope-Ownerid->'+T.OwnerId);
            system.debug('KCM: task in scope-Watid->'+T.WhatId);

            if (addtoList == true)
            {
              taskToUpdate.add(T);
            }
        }

        if (!taskToUpdate.isEmpty()) 
        {
            Database.SaveResult[] saveResultList = database.update(taskToUpdate, false);
            for(Integer i=0; i<saveResultList.size(); i++)
            {
                if (saveResultList.get(i).isSuccess())
                {
                    saveResultList.get(i).getId();
                } 
                else if (!saveResultList.get(i).isSuccess())
                {            
                  Database.Error error = saveResultList.get(i).getErrors().get(0);
                    String failedDML = error.getMessage();
                    System.debug('The following error has occurred.-->' + failedDML);  
                    taskToUpdate.get(i);//failed record from the list
                    system.debug('Failed ID' + taskToUpdate.get(i).Id);
                    FailedTask.put(taskToUpdate.get(i).Id, failedDML);
                }           
            }              
        }

    }

    /* ----------------------------------------------------------------------------------------------
    Author: kristoffer.d.martin
    Company: Accenture
    Description: Standard finish method for batch
    Inputs: Database.BatchableContext BC
    Returns: void
    <Date>      <Authors Name>       <Brief Description of Change> 
    11-Nov-2016 kristoffer.d.martin  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    global void finish(Database.BatchableContext BC) 
    {
        if (FailedTask.keySet().isempty() == false) 
        {
          Messaging.singleEmailMessage mail = new Messaging.singleEmailMessage();
                
                mail.ToAddresses = new String[]{ 'kristoffer.d.martin@accenture.com'};
                    mail.setSubject('Batch Code Complete -task link fail report- ');
                mail.setUseSignature(false);

                String htmlBody =  '';
                htmlBody +='<table style="border:3px solid green; border-collapse:collapse;">' ;
                htmlBody +='<tr><th> Task ID</th> ';
                htmlBody += '<th> Error</th> </tr>    ' ;
                
                for (id i :FailedTask.keySet())
                {
                    htmlBody+= ' <tr> <td>'+ i + '</td> <td>'+ FailedTask.get(i)+'</td>  </tr>' ;
                }
                
                htmlBody +='</table>' ;
                
                mail.setHtmlBody(htmlBody);
                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                mails.add(mail);
                Messaging.sendEmail(mails);
        }

    }
  
}