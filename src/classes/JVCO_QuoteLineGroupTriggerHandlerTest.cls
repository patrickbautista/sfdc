/* ----------------------------------------------------------------------------------------------
   Name: JVCO_QuoteLineGroupTriggerHandlerTest
   Description: Test Class of Quote Line Group Trigger Handler

   Date         Version     Author                          Summary of Changes 
   -----------  -------     -----------------               -----------------------------------------
  05-Jul-2017   0.1         Accenture-erica.jane.w.matias   Intial creation
  
  ----------------------------------------------------------------------------------------------- */

@isTest
private class JVCO_QuoteLineGroupTriggerHandlerTest
{
    @testSetup
    public static void testSetup()
    {     
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        PriceBook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;

        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;
      
        Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
        insert prod;

        JVCO_Venue__c ven = JVCO_TestClassObjectBuilder.createVenue();
        ven.JVCO_Primary_Tariff__c = 'AC_PRS';
        insert ven;

        JVCO_Affiliation__c aff = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, ven.id);
        aff.JVCO_End_Date__c = null;
        aff.JVCO_Closure_Reason__c = null;
        insert aff;



        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        insert opp;
        test.StartTest();
        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
        insert q;
        
        
        SBQQ__QuoteLineGroup__c qlg =  [select Id, SBQQ__Account__c, JVCO_Primary_Tariff__c, JVCO_Affiliated_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];
        qlg.SBQQ__Account__c = acc2.Id;
        update qlg;

        SBQQ__QuoteLine__c ql = JVCO_TestClassObjectBuilder.createQuoteLine(qlg.JVCO_Affiliated_Venue__c, qlg.SBQQ__Quote__c, qlg.id, prod.id);
        insert ql;

        List<JVCO_Primary_Tariff_Mapping__c> settings = new List<JVCO_Primary_Tariff_Mapping__c>();
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Airport'));
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Aircraft'));
        insert settings;

        test.Stoptest();
    }
    
    @isTest
    static void test_SetqlgAlreadyUpdatedQliPrimaryTarifftoFalseWhenNull()
    {
        JVCO_QuoteLineGroupTriggerHandler.qlgAlreadyUpdatedQliPrimaryTariff = null;
        system.assertEquals(False, JVCO_QuoteLineGroupTriggerHandler.qlgAlreadyUpdatedQliPrimaryTariff, 'Null value for qlgAlreadyUpdatedQliPrimaryTariff was not handled');
    } 
    
    @isTest
    static void test_UpdateQLGChildTariff(){
        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Primary_Tariff__c, JVCO_Affiliated_Venue__c, JVCO_Affiliated_Venue__r.JVCO_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];
        JVCO_Venue__c ven = [select Id, JVCO_Primary_Tariff__c from JVCO_Venue__c where Id = :qlg.JVCO_Affiliated_Venue__r.JVCO_Venue__c];
        
        ven.JVCO_Primary_Tariff__c = 'B_PRS';
        
        update ven;
        
        test.startTest();
        update qlg;
        test.stopTest();
        
        system.assertEquals(false, JVCO_QuoteLineGroupTriggerHandler.qlgAlreadyUpdatedQliPrimaryTariff);
    } 


    @isTest
    static void test_UpdateQLGAccountVenue(){
        Account acc = [SELECT Id, Type FROM Account WHERE Name = 'Account 1' LIMIT 1];
        acc.Type = 'Key Account';
        update acc;

        Account acc2 = [SELECT Id, Name, Type FROM Account WHERE JVCO_Customer_Account__c = :acc.Id LIMIT 1];
        system.assertEquals('Key Account', acc2.Type);

        SBQQ__QuoteProcess__c qProcess = [SELECT Id FROM SBQQ__QuoteProcess__c LIMIT 1];

        JVCO_Venue__c ven = new JVCO_Venue__c();
        string cat = 'Ven1';
        ven.Name = 'TEST-'+cat;
        ven.JVCO_Lead_Source__c = 'Churn';
        ven.JVCO_Venue_Type__c = 'Airport';
        ven.JVCO_City__c = 'City'+cat;
        ven.JVCO_Country__c = 'United Kingdom';
        ven.JVCO_Street__c = 'Street'+cat;
        ven.JVCO_Postcode__c = 'ND'+cat+' 8TH';
        Insert ven;

        JVCO_Affiliation__c aff = new JVCO_Affiliation__c();
        aff.JVCO_Account__c = acc2.Id;
        aff.JVCO_Venue__c = ven.Id;
        aff.JVCO_Start_Date__c = date.today();
        insert aff;

        List<JVCO_Affiliation__c> affRec = [select id from JVCO_Affiliation__c];
        system.assertEquals(2, affRec.size());

        Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        insert opp;

        SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(acc2.id, qProcess.id, opp.id, null, null);
        insert q;

        SBQQ__QuoteLineGroup__c newQLG = new SBQQ__QuoteLineGroup__c(
                                                             Name = 'Group1',
                                                             SBQQ__Number__c = 1,
                                                             SBQQ__Quote__c = q.Id,
                                                             JVCO_Affiliated_Venue__c = aff.Id);
        Insert newQLG;
        
        SBQQ__QuoteLineGroup__c qlg = [select Id, Name, SBQQ__Account__c, JVCO_Affiliated_Venue__c, JVCO_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c WHERE Id = :newQLG.Id LIMIT 1];
    
        system.assertEquals('Group1', qlg.Name);
        system.assertEquals(false, string.isBlank(qlg.SBQQ__Account__c));
        system.assertEquals(false, string.isBlank(qlg.JVCO_Venue__c));
    }

    @isTest
    static void test_EditQLGName(){

        User currentUser = JVCO_TestClassObjectBuilder.createUser('Licensing User');

        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Primary_Tariff__c, JVCO_Affiliated_Venue__c, JVCO_Affiliated_Venue__r.JVCO_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];

        SBQQ__Quote__c qRec = [SELECT id, SBQQ__Status__c, JVCO_Cancelled__c FROM SBQQ__Quote__c LIMIT 1];

        Test.startTest();
        qRec.SBQQ__Status__c = 'Presented';
        update qRec;

        System.runAs(currentUser){
            try{

                qlg.Name = 'Test2';
                update qlg;

            }catch(Exception e){

                Boolean expectedError = e.getMessage().contains('cannot change details of Group')? true : false;
                system.assert(expectedError);
            }
            
        }
        Test.stopTest();
    }

    @isTest
    static void test_DeleteQLG(){

        User currentUser = JVCO_TestClassObjectBuilder.createUser('Licensing User');

        SBQQ__QuoteLineGroup__c qlg =  [select Id, JVCO_Primary_Tariff__c, JVCO_Affiliated_Venue__c, JVCO_Affiliated_Venue__r.JVCO_Venue__c, SBQQ__Quote__c from SBQQ__QuoteLineGroup__c limit 1];

        SBQQ__Quote__c qRec = [SELECT id, SBQQ__Status__c, JVCO_Cancelled__c FROM SBQQ__Quote__c LIMIT 1];

        Test.startTest();
        qRec.SBQQ__Status__c = 'Presented';
        update qRec;

        System.runAs(currentUser){
            try{

                delete qlg;

            }catch(Exception e){

                Boolean expectedError = e.getMessage().contains('cannot delete a Quote Line Group')? true : false;
                system.assert(expectedError);
            }
            
        }
        Test.stopTest();
    }

}