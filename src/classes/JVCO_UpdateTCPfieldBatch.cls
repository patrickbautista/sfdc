/* ----------------------------------------------------------------------------------------------------------
Name:            JVCO_UpdateTCPfieldBatch.cls
Description:     Updates Credit note paid amount and write off per lines fields
Test class:      JVCO_CashTransferLogicTest.cls - testCreditNoteToInvoiceMatching method

Date             Version     Author                            Summary of Changes
-----------      -------     -----------------                 -------------------------------------------
25-jul-2019      0.1         patrick.t.bautista                Initial version of the code
---------------------------------------------------------------------------------------------------------- */
public class JVCO_UpdateTCPfieldBatch implements Database.Batchable<sObject>
{
    public Set<id> cashMatchingRefIdSet = new Set<id>();
    public Boolean allowOnlyCpqUpdate = false;
    public JVCO_UpdateTCPfieldBatch(){}
    public JVCO_UpdateTCPfieldBatch(Set<id> cashMatchingRefIdSet)
    {
        this.cashMatchingRefIdSet = cashMatchingRefIdSet;
    }
    public JVCO_UpdateTCPfieldBatch(Set<id> cashMatchingRefIdSet, Boolean allowOnlyCpqUpdate)
    {
        this.cashMatchingRefIdSet = cashMatchingRefIdSet;
        this.allowOnlyCpqUpdate = allowOnlyCpqUpdate;
    }
    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
            return Database.getQueryLocator([select id
                                             from c2g__codaMatchingReference__c
                                             where id IN: cashMatchingRefIdSet]);
    }
    
    public void execute(Database.BatchableContext BC, List<c2g__codaMatchingReference__c> scope) 
    {
        JVCO_FFUtil.allowOnlyOrderAndOrderItemUpdate = allowOnlyCpqUpdate;
        JVCO_CashTransferLogic tcp = new JVCO_CashTransferLogic();
        tcp.disableJournalAndBInvUpt = true;
        tcp.createJournalUsingBatch(scope);
    }
    public void finish(Database.BatchableContext BC) 
    {
        
    }
}