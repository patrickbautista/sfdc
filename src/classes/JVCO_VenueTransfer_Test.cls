@isTest
private class JVCO_VenueTransfer_Test
{

  @testSetup 
  static void init() {

    //TestDataFactory.createKeyAcctsWithLicenceAcctsWithVenues(0, 0, 4);
    //TestDataFactory.createVenues(4);


    
    
  }

  @isTest 
  static void searchAffiliations_Disposal_Test() {
    TestDataFactory.createVenues(4);
    JVCO_VenueTransfer vt = new JVCO_VenueTransfer();

    Id licenceAcctRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    Id customerAcctRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();

    Account customerAccount = JVCO_TestClassObjectBuilder.createCustomerAccount();
    insert customerAccount;

    Account acct = JVCO_TestClassObjectBuilder.createLicenceAccount(customerAccount.id);
    insert acct;

    

    Map<String, JVCO_VenueTransfer.JVCO_TransferObject> searchResultsMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();

    List<JVCO_Venue__c> listVenue = [select id from JVCO_Venue__c];
    List<JVCO_Affiliation__c> affiliationsList = new List<JVCO_Affiliation__c>();
    List<Id> searchResultsIds = new List<Id>();
    for (JVCO_Venue__c v : listVenue) {
        //system.assertEquals(null, v.id);
      searchResultsIds.add(v.Id);
      affiliationsList.add(TestDataFactory.getInstantiatedAffiliationPublic(acct.id, v.Id));
    }

    //update listVenue;
    insert affiliationsList;

    System.assertEquals(4, searchResultsIds.size());

    Test.startTest();

    Test.setFixedSearchResults(searchResultsIds);

    // SCENARIO: search without exclusion must return all venues
    searchResultsMap = vt.searchAffiliations(JVCO_VenueTransfer.TransferType.DISPOSAL, 'ven', acct.Id, new Set<String>(), false);
    System.assertEquals(searchResultsIds.size(), searchResultsMap.size());

    // SCENARIO: search with 1 excluded affiliation must return N-1 venues
    Id idToExcludeFromSearch = searchResultsMap.values().get(1).affiliationRecord.Id;
    searchResultsMap = vt.searchAffiliations(JVCO_VenueTransfer.TransferType.DISPOSAL, 'ven', acct.Id, new Set<String>{ idToExcludeFromSearch }, false);
    //System.assertEquals(searchResultsIds.size() - 1, searchResultsMap.size());
    //System.assertEquals(null, searchResultsMap.get(idToExcludeFromSearch));

    // SCENARIO: search blank should result in a blank result set
    // This is tested via the controller's test.

    Test.stopTest();
  }

  @isTest 
  static void searchAffiliations_Acquisition_Test() {
    TestDataFactory.createVenues(2);
    JVCO_VenueTransfer vt = new JVCO_VenueTransfer();

    Id licenceAcctRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    Id customerAcctRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();

    Account customerAccount = JVCO_TestClassObjectBuilder.createCustomerAccount();
    insert customerAccount;

    Account customerAccount2 = JVCO_TestClassObjectBuilder.createCustomerAccount();
    customerAccount2.name = 'TestCustomer2';
    insert customerAccount2;

    Account acct = JVCO_TestClassObjectBuilder.createLicenceAccount(customerAccount.id);
    insert acct;

    Account acct2 = JVCO_TestClassObjectBuilder.createLicenceAccount(customerAccount2.id);
    acct2.name = 'TestLicence2';
    insert acct2;


    Map<String, JVCO_VenueTransfer.JVCO_TransferObject> searchResultsMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();

    //Id keyAccountId = [SELECT Id FROM Account order by CreatedDate DESC limit 1].get(0).Id;
    //List<Account> licenceAccountsList = TestDataFactory.createLicenceAccountsWithVenues(keyAccountId, 1, 2);
    List<Account> licenceAccountsList = new List<Account>();
    licenceAccountsList.add(acct2);

    List<JVCO_Venue__c> listVenue = [select id from JVCO_Venue__c];
    List<JVCO_Affiliation__c> affiliationsList = new List<JVCO_Affiliation__c>();
    //List<Id> searchResultsIds = new List<Id>();
    for (JVCO_Venue__c v : listVenue) {
        //system.assertEquals(null, v.id);
      //searchResultsIds.add(v.Id);
      affiliationsList.add(TestDataFactory.getInstantiatedAffiliationPublic(acct2.id, v.Id));
    }
    insert affiliationsList;


    List<Id> searchResultsIds = new List<Id>();
    for (JVCO_Affiliation__c aff : [SELECT JVCO_Venue__c FROM JVCO_Affiliation__c WHERE JVCO_Account__c IN :licenceAccountsList]) {
      searchResultsIds.add(aff.JVCO_Venue__c);
    }

    System.assertNotEquals(acct.Id, licenceAccountsList.get(0).Id);
    System.assertEquals(2, searchResultsIds.size());

    Test.startTest();

    Test.setFixedSearchResults(searchResultsIds);

    // SCENARIO: search without exclusion must return all venues
    searchResultsMap = vt.searchAffiliations(JVCO_VenueTransfer.TransferType.ACQUISITION, 'ven', acct.Id, new Set<String>(), true);
    System.assertEquals(searchResultsIds.size() , searchResultsMap.size());

    // SCENARIO: search with 1 excluded affiliation must return N-1 venues
    //Id idToExcludeFromSearch = searchResultsMap.values().get(1).affiliationRecord.Id;
    searchResultsMap = vt.searchAffiliations(JVCO_VenueTransfer.TransferType.ACQUISITION, 'ven', acct.Id, new Set<String>(), false);
    System.assertEquals(searchResultsIds.size(), searchResultsMap.size());
    //System.assertEquals(null, searchResultsMap.get(idToExcludeFromSearch));

    // SCENARIO: search blank should result in a blank result set
    // This is tested via the controller's test.

    Test.stopTest();
  }

  @isTest 
  static void getFieldPathsFromFsmList_Test() {
    // WARNING: JVCO_Affiliation__c object's JVCO_Disposals_Defaults fieldset must be fully set up before running this test.
    List<Schema.FieldSetMember> fsmList = SObjectType.JVCO_Affiliation__c.FieldSets.JVCO_Disposals_Defaults.getFields();

    JVCO_VenueTransfer vt = new JVCO_VenueTransfer();
    Set<String> fieldPathsSet = vt.getFieldPathsFromFsmList(fsmList);

    // NOTE: as of time of writing, only JVCO_Closure_Reason__c and JVCO_End_Date__c are contained in the JVCO_Disposals_Defaults field set.
    System.assertEquals(true, fieldPathsSet.contains('JVCO_Closure_Reason__c'));
    System.assertEquals(true, fieldPathsSet.contains('JVCO_End_Date__c'));
  }

  @isTest
  static void fixDateFormat_Test() {

    // NOTE: Dates in test are using the UK locale format. After majority, if not all, users are from UK.
    JVCO_VenueTransfer vt = new JVCO_VenueTransfer();

    // SCENARIO: pattern for UK locale date found. Input is DD/MM/YYYY (March 22, 2017 [22/03/2017]) Excpeted result: JSON.deserializeStrict-recognizable date format.
    System.assertEquals('2017-03-22', vt.fixDateFormat('22/03/2017'));

    // SCENARIO: no matched patterns. to return as is.
    System.assertEquals('asdf', vt.fixDateFormat('asdf'));

  }

  @isTest
  static void applyOverarchingValuesToTransferSelections_Test() {

    // WARNING: JVCO_Affiliation__c object's JVCO_Disposals_Defaults fieldset must be fully set up before running this test.
    JVCO_VenueTransfer vt = new JVCO_VenueTransfer();
    String fieldPaths = String.join(vt.getSelectedAffiliationFieldsForDisposal(), ',');
    Test.startTest();
    Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelections = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
    for (JVCO_Affiliation__c affiliation : Database.query('SELECT ' + fieldPaths + ' FROM JVCO_Affiliation__c LIMIT 5')) {
      JVCO_VenueTransfer.JVCO_TransferObject transferObject = new JVCO_VenueTransfer.JVCO_TransferObject();
      transferObject.affiliationRecord = affiliation;
      transferSelections.put(affiliation.Id, transferObject);

      System.assertEquals(null, affiliation.JVCO_Closure_Reason__c);
      System.assertEquals(null, affiliation.JVCO_End_Date__c);
    }

    String overarchingClosureReason = 'Venue no longer exists';
    String overarchingNotYetFixedEndDate = '22/03/2017';
    Date overarchingFixedEndDate = Date.newInstance(2017, 03, 22);
    String overarchingValues = '{ "JVCO_Closure_Reason__c" : "' + overarchingClosureReason + '", "JVCO_End_Date__c" : "' + overarchingNotYetFixedEndDate + '" }';
    JVCO_Affiliation__c affOverArcher = new JVCO_Affiliation__c();
    affOverArcher.JVCO_Closure_Reason__c = overarchingClosureReason;
    //affOverArcher.JVCO_End_Date__c = Date.valueOf(overarchingNotYetFixedEndDate);
    affOverArcher.JVCO_End_Date__c = System.today() - 10;
    transferSelections = vt.applyOverarchingValuesToTransferSelections(JVCO_VenueTransfer.TransferType.DISPOSAL, transferSelections, affOverArcher, '');
    affOverArcher.JVCO_Start_Date__c = System.today() + 10;
    transferSelections = vt.applyOverarchingValuesToTransferSelections(JVCO_VenueTransfer.TransferType.ACQUISITION, transferSelections, affOverArcher, '');

    for (JVCO_VenueTransfer.JVCO_TransferObject to : transferSelections.values()) {
      System.assertEquals(overarchingClosureReason, to.affiliationRecord.JVCO_Closure_Reason__c);
      System.assertEquals(System.today() - 10, to.affiliationRecord.JVCO_End_Date__c);
    }

    // SCENARIO: JVCO_End_Date__c input is blank. Expected result is, records' JVCO_End_Date__c field's value will become blank as well.
    overarchingValues = '{ "JVCO_End_Date__c" : "" }';
    affOverArcher.JVCO_End_Date__c = null;
    transferSelections = vt.applyOverarchingValuesToTransferSelections(JVCO_VenueTransfer.TransferType.DISPOSAL, transferSelections, affOverArcher, '');
    for (JVCO_VenueTransfer.JVCO_TransferObject to : transferSelections.values()) {
      System.assertEquals(null, to.affiliationRecord.JVCO_End_Date__c);
    }
    Test.stopTest();
  }

  @isTest
  static void convertToFieldSetWrapperList_Test() {
    // WARNING: JVCO_Affiliation__c object's JVCO_Disposals_Defaults fieldset must be fully set up before running this test.
    List<Schema.FieldSetMember> fsmList = SObjectType.JVCO_Affiliation__c.FieldSets.JVCO_Disposals_Defaults.getFields();
    
    JVCO_VenueTransfer vt = new JVCO_VenueTransfer();
    Test.startTest();
    Set<String> expectedApiNames = new Set<String> { 'JVCO_Closure_Reason__c', 'JVCO_End_Date__c' };
    List<JVCO_VenueTransfer.FieldSetWrapper> fswList = vt.convertToFieldSetWrapperList(fsmList);
    for(JVCO_VenueTransfer.FieldSetWrapper fsw : fswList) {
      System.assertEquals(true, expectedApiNames.contains(fsw.fApiName));
    }
    Test.stopTest();
  }
  
  @isTest
  static void coverApiFieldNames_Test() {
    JVCO_VenueTransfer vt = new JVCO_VenueTransfer();
    
    Test.startTest();
    List<String> candidateVenueFieldsForDisposal = vt.getCandidateVenueFieldsForDisposal();
    List<String> selectedVenueFieldsForDisposal = vt.getSelectedVenueFieldsForDisposal();
    List<String> candidateVenueFieldsForAcquisition = vt.getCandidateVenueFieldsForAcquisition();
    List<String> candidateAffiliationFieldsForAcquisition = vt.getCandidateAffiliationFieldsForAcquisition();
    List<String> selectedVenueFieldsForAcquisition = vt.getSelectedVenueFieldsForAcquisition();
    List<String> defaultAffiliationFieldsForAcquisition = vt.getDefaultsAffiliationFieldsForAcquisition();
    List<String> selectedAffiliationFieldsForAcquisition = vt.getSelectedAffiliationFieldsForAcquisition();
    List<String> candidateAccountFieldsForAcquisition = vt.getCandidateAccountFieldsForAcquisition();
    List<String> affiliationAdditionalFieldsForDisplay = vt.getAdditionalFieldsForDisplay();
    Test.stopTest();
  }
  
  @isTest
  static void confirmAcquisition_Test() {
    JVCO_VenueTransfer vt = new JVCO_VenueTransfer();
    Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
    List<JVCO_Affiliation__c> affList = new List<JVCO_Affiliation__c>();
    
    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
    
    List<Account> customerAccounts = [select id, name from Account where RecordTypeId = :customerRT];
    List<Account> licenceAccounts = [select id, name from Account where RecordTypeId = :licenceRT];

   // system.assertEquals(null, customerAccounts,'not equals');
    
    Account a1 = new Account();
    a1.RecordTypeId = customerRT;
    a1.Name = 'Test Customer0822201 Account';
    a1.Type = 'Key Account';
    insert a1;
    
    Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
    a2.RecordTypeId = licenceRT;
    a2.Name = 'Test Licence Account';
    a2.JVCO_Customer_Account__c = a1.Id;
    insert a2;
    
    JVCO_Venue__c venue1 = JVCO_TestClassObjectBuilder.createVenue();
    venue1.Name = 'Sample Venue 1 - 634343';
    insert venue1;
    
    JVCO_Affiliation__c affiliation1 = new JVCO_Affiliation__c();
    affiliation1.Name = a2.Id + ' : ' + venue1.Id;
    affiliation1.JVCO_Account__c = a2.Id;
    affiliation1.JVCO_Venue__c = venue1.Id;
    affiliation1.JVCO_Start_Date__c = System.today() - 20;
    affiliation1.JVCO_Status__c = 'Trading';
    affList.add(affiliation1);
    
    insert affList;
    
    Account a3 = JVCO_TestClassObjectBuilder.createCustomerAccount();
    //a3.RecordTypeId = customerRT;
    a3.Name = 'Test Account 022345-C';
    //a3.Type = 'Key Account';
    a3.ShippingPostalCode = 'E10 4XX';
    a3.ShippingCity = 'London';
    a3.ShippingCountry = 'TestCountry1';
    a3.ShippingState = 'TestState1';
    a3.ShippingStreet = 'TestStreet1';
    insert a3;
    
    Account a4 = JVCO_TestClassObjectBuilder.createLicenceAccount(a3.id);
    //a4.RecordTypeId = licenceRT;
    a4.Name = 'Test Account 022345-L';
    //a4.JVCO_Customer_Account__c = a3.Id;
    a3.ShippingPostalCode = 'E10 4XX';
    a3.ShippingCity = 'London';
    a3.ShippingCountry = 'TestCountry2';
    a3.ShippingState = 'TestState2';
    a3.ShippingStreet = 'TestStreet2';
    insert a4;
    
    for(JVCO_Affiliation__c affRec : affList) {
        JVCO_VenueTransfer.JVCO_TransferObject tc = new JVCO_VenueTransfer.JVCO_TransferObject();
        tc.affiliationRecord = affRec;
        tc.venueRecord = venue1;
        tc.accountRecord = a2;
        transferSelectionsMap.put(affRec.Id, tc);
    }
    
    Test.startTest();
    Boolean retAcquisition = vt.confirmAcquisition(a4, false, transferSelectionsMap, false);
    Test.stopTest();
  }
  
  @isTest
  static void confirmDisposal_Test() {
    JVCO_VenueTransfer vt = new JVCO_VenueTransfer();
    Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
    List<JVCO_Affiliation__c> affList = new List<JVCO_Affiliation__c>();
    
    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
    Account a1 = new Account();
    a1.RecordTypeId = customerRT;
    a1.Name = 'Test Customer0822201 Account';
    a1.Type = 'Key Account';
    insert a1;
    
    Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
    a2.RecordTypeId = licenceRT;
    a2.Name = 'Test Licence Account';
    a2.JVCO_Customer_Account__c = a1.Id;
    insert a2;
    
    JVCO_Venue__c venue1 = JVCO_TestClassObjectBuilder.createVenue();
    venue1.Name = 'Sample Venue 1 - 634343';
    insert venue1;
    
    JVCO_Affiliation__c affiliation1 = new JVCO_Affiliation__c();
    affiliation1.Name = a2.Id + ' : ' + venue1.Id;
    affiliation1.JVCO_Account__c = a2.Id;
    affiliation1.JVCO_Venue__c = venue1.Id;
    affiliation1.JVCO_Start_Date__c = System.today() - 20;
    affiliation1.JVCO_Status__c = 'Trading';
    affList.add(affiliation1);
    
    insert affList;
    
    Account a3 = JVCO_TestClassObjectBuilder.createCustomerAccount();
    //a3.RecordTypeId = customerRT;
    a3.Name = 'Test Account 022344-C1';
    //a3.Type = 'Key Account';
    a3.ShippingPostalCode = 'E10 4XX';
    a3.ShippingCity = 'London';
    a3.ShippingCountry = 'TestCountry1';
    a3.ShippingState = 'TestState1';
    a3.ShippingStreet = 'TestStreet1';
    insert a3;
    
    Account a4 = JVCO_TestClassObjectBuilder.createLicenceAccount(a3.id);
    //a4.RecordTypeId = licenceRT;
    a4.Name = 'Test Account 022344-L';
    //a4.JVCO_Customer_Account__c = a3.Id;
    a3.ShippingPostalCode = 'E10 4XX';
    a3.ShippingCity = 'London';
    a3.ShippingCountry = 'TestCountry2';
    a3.ShippingState = 'TestState2';
    a3.ShippingStreet = 'TestStreet2';
    insert a4;
    
    Account a5 = [select Id, Name, Licence_Account_Number__c from Account where Id =: a4.Id];
    
    for(JVCO_Affiliation__c affRec : affList) {
        JVCO_VenueTransfer.JVCO_TransferObject tc = new JVCO_VenueTransfer.JVCO_TransferObject();
        tc.affiliationRecord = affRec;
        tc.venueRecord = venue1;
        tc.accountRecord = a2;
        tc.accountId = a5.Licence_Account_Number__c;
        transferSelectionsMap.put(affRec.Id, tc);
    }

    Test.startTest();
    JVCO_Array_Size__c asCS = new JVCO_Array_Size__c();
    asCS.JVCO_Contract_Batch_Size__c = 1;
    insert asCS;
    
    Boolean retAcquisition = vt.confirmDisposals(transferSelectionsMap, true);
    Test.stopTest();
    
  }

  @isTest
  static void confirmDisposal_TestQuote() {
    JVCO_VenueTransfer vt = new JVCO_VenueTransfer();
    Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
    List<JVCO_Affiliation__c> affList = new List<JVCO_Affiliation__c>();
    
    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();    

    SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
    insert qProcess;

    Pricebook2 testPB2 = new Pricebook2();
    testPB2.name = 'Standard Price Book';
    testPB2.IsActive = true;
    insert testPB2;
    
    Account a1 = new Account();
    a1.RecordTypeId = customerRT;
    a1.Name = 'Test Customer0822201 Account';
    a1.Type = 'Key Account';
    insert a1;
    
    Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
    a2.RecordTypeId = licenceRT;
    a2.Name = 'Test Licence Account';
    a2.JVCO_Customer_Account__c = a1.Id;
    insert a2;

    

    Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
    insert prod;
    
    JVCO_Venue__c venue1 = JVCO_TestClassObjectBuilder.createVenue();
    venue1.Name = 'Sample Venue 1 - 634343';
    insert venue1;
    
    JVCO_Affiliation__c affiliation1 = new JVCO_Affiliation__c();
    affiliation1.Name = a2.Id + ' : ' + venue1.Id;
    affiliation1.JVCO_Account__c = a2.Id;
    affiliation1.JVCO_Venue__c = venue1.Id;
    affiliation1.JVCO_Start_Date__c = System.today() - 20;
    affiliation1.JVCO_Status__c = 'Trading';
    affList.add(affiliation1);
    insert affList;

    Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(a2);
    insert opp;

    SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(a2.id, qProcess.id, opp.id, null, null);
    q.SBQQ__Pricebook__c = test.getStandardPriceBookId();
    insert q;

    SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(a2.id, prod.id);
    insert s;

    Contract contr = new Contract();
    contr.AccountId = a2.Id;
    contr.Status = 'Draft';
    contr.StartDate = Date.today();
    contr.ContractTerm = 12;
    contr.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
    contr.SBQQ__RenewalQuoted__c = FALSE;
    contr.SBQQ__Quote__c = q.id;
    contr.SBQQ__Opportunity__c = opp.id;
    insert contr;

    q.SBQQ__MasterContract__c = contr.id;
    update q;
    
    Account a3 = JVCO_TestClassObjectBuilder.createCustomerAccount();
    //a3.RecordTypeId = customerRT;
    a3.Name = 'Test Account 022346-C';
    //a3.Type = 'Key Account';
    a3.ShippingPostalCode = 'E10 4XX';
    a3.ShippingCity = 'London';
    a3.ShippingCountry = 'TestCountry1';
    a3.ShippingState = 'TestState1';
    a3.ShippingStreet = 'TestStreet1';
    insert a3;
    
    Account a4 = JVCO_TestClassObjectBuilder.createLicenceAccount(a3.id);
    //a4.RecordTypeId = licenceRT;
    a4.Name = 'Test Account 022346-L';
    //a4.JVCO_Customer_Account__c = a3.Id;
    a3.ShippingPostalCode = 'E10 4XX';
    a3.ShippingCity = 'London';
    a3.ShippingCountry = 'TestCountry2';
    a3.ShippingState = 'TestState2';
    a3.ShippingStreet = 'TestStreet2';
    insert a4;
    
    Account a5 = [select Id, Name, Licence_Account_Number__c from Account where Id =: a4.Id];
    
    for(JVCO_Affiliation__c affRec : affList) {
        JVCO_VenueTransfer.JVCO_TransferObject tc = new JVCO_VenueTransfer.JVCO_TransferObject();
        tc.affiliationRecord = affRec;
        tc.venueRecord = venue1;
        tc.accountRecord = a2;
        tc.accountId = a5.Licence_Account_Number__c;
        tc.selected = true;
        transferSelectionsMap.put(affRec.Id, tc);
    }

    Test.startTest();
    JVCO_Array_Size__c asCS = new JVCO_Array_Size__c();
    asCS.JVCO_Contract_Batch_Size__c = 1;
    insert asCS;
    
    Boolean retAcquisition = vt.confirmDisposals(transferSelectionsMap, true);
    Test.stopTest();
    
  }

  @isTest
  static void confirmAcquisition_TestContract() {
    JVCO_VenueTransfer vt = new JVCO_VenueTransfer();
    Map<String, JVCO_VenueTransfer.JVCO_TransferObject> transferSelectionsMap = new Map<String, JVCO_VenueTransfer.JVCO_TransferObject>();
    List<JVCO_Affiliation__c> affList = new List<JVCO_Affiliation__c>();
    
    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
    Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();   

    List<Account> customerAccounts = [select id, name from Account where RecordTypeId = :customerRT];
    List<Account> licenceAccounts = [select id, name from Account where RecordTypeId = :licenceRT];

   // system.assertEquals(null, customerAccounts,'not equals');

    SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
    insert qProcess;

    Pricebook2 testPB2 = new Pricebook2();
    testPB2.name = 'Standard Price Book';
    testPB2.IsActive = true;
    insert testPB2;
    
    Account a1 = new Account();
    a1.RecordTypeId = customerRT;
    a1.Name = 'Test Customer0822201 Account';
    a1.Type = 'Key Account';
    insert a1;
    
    Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
    a2.RecordTypeId = licenceRT;
    a2.Name = 'Test Licence Account';
    a2.JVCO_Customer_Account__c = a1.Id;
    insert a2;

    Product2 prod = JVCO_TestClassObjectBuilder.createProduct();
    insert prod;
    
    JVCO_Venue__c venue1 = JVCO_TestClassObjectBuilder.createVenue();
    venue1.Name = 'Sample Venue 1 - 634343';
    insert venue1;
    
    JVCO_Affiliation__c affiliation1 = new JVCO_Affiliation__c();
    affiliation1.Name = a2.Id + ' : ' + venue1.Id;
    affiliation1.JVCO_Account__c = a2.Id;
    affiliation1.JVCO_Venue__c = venue1.Id;
    affiliation1.JVCO_Start_Date__c = System.today() - 20;
    affiliation1.JVCO_Status__c = 'Trading';
    affList.add(affiliation1);
    insert affList;

    Opportunity opp = JVCO_TestClassObjectBuilder.createOpportunity(a2);
    insert opp;

    SBQQ__Quote__c q = JVCO_TestClassObjectBuilder.createQuote(a2.id, qProcess.id, opp.id, null, null);
    q.SBQQ__Pricebook__c = test.getStandardPriceBookId();
    insert q;

    SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(a2.id, prod.id);
    insert s;

    Contract contr = new Contract();
    contr.AccountId = a2.Id;
    contr.Status = 'Draft';
    contr.StartDate = Date.today();
    contr.ContractTerm = 12;
    contr.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
    contr.SBQQ__RenewalQuoted__c = FALSE;
    contr.SBQQ__Quote__c = q.id;
    contr.SBQQ__Opportunity__c = opp.id;
    insert contr;
    
    Account a3 = JVCO_TestClassObjectBuilder.createCustomerAccount();
    //a3.RecordTypeId = customerRT;
    a3.Name = 'Test Account 022347-C';
    //a3.Type = 'Key Account';
    a3.ShippingPostalCode = 'E10 4XX';
    a3.ShippingCity = 'London';
    a3.ShippingCountry = 'TestCountry1';
    a3.ShippingState = 'TestState1';
    a3.ShippingStreet = 'TestStreet1';
    insert a3;
    
    Account a4 = JVCO_TestClassObjectBuilder.createLicenceAccount(a3.id);
    //a4.RecordTypeId = licenceRT;
    a4.Name = 'Test Account 022347-L';
    //a4.JVCO_Customer_Account__c = a3.Id;
    a3.ShippingPostalCode = 'E10 4XX';
    a3.ShippingCity = 'London';
    a3.ShippingCountry = 'TestCountry2';
    a3.ShippingState = 'TestState2';
    a3.ShippingStreet = 'TestStreet2';
    insert a4;
    
    for(JVCO_Affiliation__c affRec : affList) {
        JVCO_VenueTransfer.JVCO_TransferObject tc = new JVCO_VenueTransfer.JVCO_TransferObject();
        tc.affiliationRecord = affRec;
        tc.venueRecord = venue1;
        tc.accountRecord = a2;
        transferSelectionsMap.put(affRec.Id, tc);
    }
    
    Test.startTest();
    Boolean retAcquisition = vt.confirmAcquisition(a4, false, transferSelectionsMap, false);
    Test.stopTest();
  }
}