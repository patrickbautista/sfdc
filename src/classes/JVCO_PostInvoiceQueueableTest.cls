/**********************************************************************************
 * @Author: 
 * @Company: Accenture
 * @Description: Tess class for JVCO_blngInvoiceHandler
 * @Created Date:
 * @Revisions:
 *      <Name>              <Date>          <Description>
 *      jason.e.mactal      10.31.2017      Changed to reference static methods
 * 
 **********************************************************************************/
@isTest
private class JVCO_PostInvoiceQueueableTest
{
    @testSetup static void createTestData() 
     {
        JVCO_TestClassObjectBuilder.createBillingConfig();


        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
            listQueue.add(q2);
            queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
            listQueue.add(q3);
            queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
            listQueue.add(q4);
            queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
            listQueue.add(q5);
            queuesobject q6 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankAccount__c'); 
            listQueue.add(q6);
            queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q7);
            queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatement__c'); 
            listQueue.add(q8);          
            queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntry__c'); 
            listQueue.add(q9);  
            queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='PAYBASE2__Payment__c'); 
            listQueue.add(q10);
            queuesobject q11 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransaction__c'); 
            listQueue.add(q11);
            //queuesobject q12 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransactionLineItem__c'); 
            //listQueue.add(q12);
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }
        
         //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }

        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc= JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert testGeneralLedgerAcc;
      
        c2g__codaCompany__c testCompany= JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert testCompany;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(testGeneralLedgerAcc.Id);
        insert taxCode;
        
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;

        //Create Account               
        Account testAccCust = JVCO_TestClassHelper.setCustAcc(testGeneralLedgerAcc.Id, dim1.Id);
        insert testAccCust ;
        Contact c = JVCO_TestClassHelper.setContact(testAccCust.id);
        insert c;
        Account testLicAcc = JVCO_TestClassHelper.setLicAcc(testAccCust.Id, taxCode.Id, c.Id,testGeneralLedgerAcc.Id);
        testLicAcc.JVCO_Credit_Status__c = 'Debt - 1st stage';
        insert testLicAcc;
        

        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(testCompany.Id, testGroup.Id);
        insert accCurrency;
        

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(testCompany.Id, testGroup.Id);
        insert yr;
        
        
        insert JVCO_TestClassHelper.setPeriod(testCompany.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(testCompany.Id);

        Test.startTest();
        Product2 p = JVCO_TestClassHelper.setProduct(testGeneralLedgerAcc.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(testLicAcc.Id);
        insert opp;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(testLicAcc.Id, opp.Id);

        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;

        Contract contr= new Contract();
        contr.AccountId = testLicAcc.Id;
        contr.Status = 'Draft';
        contr.StartDate = Date.today();
        contr.ContractTerm = 12;
        contr.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
        insert contr;
        
        Test.stopTest();

        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        insert dt;
        
        Order o = JVCO_TestClassHelper.setOrder(testLicAcc.Id, q.Id);
        insert o;
        
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        OrderItem oi1 = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi1;

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(testLicAcc.Id, o.Id);
        insert bInv;
        
        blng__Invoice__c bInv2 = JVCO_TestClassHelper.setBInv(testLicAcc.Id, o.Id);
        insert bInv2;

    }

    
    @isTest
    static void itShould()
    {

        Test.startTest();
            JVCO_PostInvoiceQueueable.start();
        Test.stopTest();
        System.assertEquals(2, [SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = 'Posted'].size()); 

    }

    @isTest
    static void itShouldQstringLimit()
    {
        string sQuery = 'SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = \'Draft\' ORDER BY blng__Account__c LIMIT 50000';
        string sQueryPosted = 'SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = \'Posted\' ORDER BY blng__Account__c LIMIT 50000';
        Test.startTest();
            JVCO_PostInvoiceQueueable.start(sQuery, 1);
        Test.stopTest();
        System.assertEquals(2, [SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = 'Posted'].size());
        JVCO_PostInvoiceQueueable.start(sQueryPosted, 1);
        System.assertEquals(2, [SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = 'Posted'].size()); 


    }
    
    @isTest
    static void itShouldQstringLimit2()
    {
        string sQuery = 'SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = \'Draft\' ORDER BY blng__Account__c LIMIT 50000';
        string sQueryPosted = 'SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = \'Posted\' ORDER BY blng__Account__c LIMIT 50000';
        Test.startTest();
            JVCO_PostInvoiceQueueable.start(sQuery);       
        Test.stopTest();
        System.assertEquals(2, [SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = 'Posted'].size()); 
        JVCO_PostInvoiceQueueable.start(sQueryPosted);
        System.assertEquals(2, [SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = 'Posted'].size()); 
        
    }
    
    
    @isTest
    static void itShouldoverrideBatchsize()
    {
        string sQueryPosted = 'SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = \'Posted\' ORDER BY blng__Account__c LIMIT 50000';
        Test.startTest();
        JVCO_PostInvoiceQueueable.start(10);
        Test.stopTest();
        System.assertEquals(2, [SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = 'Posted'].size()); 
        JVCO_PostInvoiceQueueable.start(sQueryPosted);
        System.assertEquals(2, [SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = 'Posted'].size());
    }
    
    @isTest
    static void itShouldoverrideBatchsizeLimit()
    {
        string sQueryPosted = 'SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = \'Posted\' ORDER BY blng__Account__c LIMIT 50000';
        Test.startTest();
        JVCO_PostInvoiceQueueable.start(1,1);
        Test.stopTest();
        System.assertEquals(2, [SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = 'Posted'].size()); 
        JVCO_PostInvoiceQueueable.start(sQueryPosted);
        System.assertEquals(2, [SELECT Id FROM blng__Invoice__c WHERE blng__InvoiceStatus__c = 'Posted'].size());
    }
    
}