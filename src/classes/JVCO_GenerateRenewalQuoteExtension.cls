public class JVCO_GenerateRenewalQuoteExtension{

	public class QuoteRenewalException extends Exception {}

    public Contract contractRecord {get; set;}
    public Boolean renderBackBtn {get; set;}
    public static Boolean isSuccessful = true;
    SBQQ__Quote__c renewQuote = new SBQQ__Quote__c();
    
    public JVCO_GenerateRenewalQuoteExtension(ApexPages.StandardController ctrl){
        this.contractRecord = (Contract) ctrl.getRecord();
        this.renderBackBtn = !isSuccessful;
    }

    public void renewalUpdate(){
        system.debug('@@@*** entered renewalUpdate');
        
        try{
			String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
            Map<Id,PermissionSet> enAdPermSet = new Map<Id,PermissionSet>([SELECT Id FROM PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors']);
            List<PermissionSetAssignment> permissionSetList = [SELECT Id FROM PermissionSetAssignment WHERE 
                                                    AssigneeId = :UserInfo.getUserId() AND PermissionSetId IN :enAdPermSet.keySet()];
            List<Contract> contracts = [SELECT Id FROM Contract WHERE Id = :contractRecord.Id AND 
                                                (Account.JVCO_In_Enforcement__c = true OR Account.JVCO_In_Infringement__c = true) AND 
                                                (Account.JVCO_Credit_Status__c = 'Enforcement - Debt Recovery' OR 
                                                Account.JVCO_Credit_Status__c = 'Enforcement - Infringement')];
             Contract cont = [select id, SBQQ__RenewalQuoted__c from Contract where id = :contractRecord.Id limit 1 for update];
            
            
            if(cont.SBQQ__RenewalQuoted__c)
                throw new QuoteRenewalException(JVCO_ApexJobController.RenewedMsg);
            JVCO_ApexJobController.retrieveJobs();
            if(JVCO_ApexJobController.lstApexJobs.size() > 0)
                throw new QuoteRenewalException(JVCO_ApexJobController.RenewedInProgMsg);
            if(contracts.size() > 0 && profileName <> 'System Administrator' && permissionSetList.isEmpty()){
                throw new QuoteRenewalException('Quote renewal with Account marked "In Enforcement" or "In Infringement" is not allowed.');
            }
            
            cont.SBQQ__RenewalQuoted__c = true;
            cont.ForceDeferred__c = 1;

            system.debug('@@@*** renewed contract :' + cont);
            
            update cont;
        }catch(Exception ex){
            system.debug('@@@*** ex.getMessage: ' + ex.getMessage());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, ex.getMessage()));
            isSuccessful = false;
			this.renderBackBtn = true;
        }
		
		return;
    }
    
    public PageReference quoteCreated(){
        
        PageReference pRef;
        //GREEN-24950: Andrew - vairable should be declared in the class and not in this method.
        //10/18/17 - wendy.a.kelley - declared variable outside the method
        //SBQQ__Quote__c renewQuote = new SBQQ__Quote__c();
        renewQuote = new SBQQ__Quote__c();

        try{
            //GREEN-24950: Andrew - Here SBQQ__RenewalOpportunity__c is being returned as null  
            Contract renewCont = [select id, SBQQ__RenewalOpportunity__c, SBQQ__RenewalOpportunity__r.SBQQ__PrimaryQuote__c from Contract where id = :contractRecord.Id limit 1];
            system.debug('@@@*** renewed contract quoteCreated:' + renewCont);
            system.debug('@@@*** isSuccessful: ' + isSuccessful );
            system.debug('@@@*** renewCont.SBQQ__RenewalOpportunity__r.SBQQ__PrimaryQuote__c: ' + renewCont.SBQQ__RenewalOpportunity__r.SBQQ__PrimaryQuote__c );
            //GREEN-24950: Andrew - we then query Quotes where SBQQ__Opportunity2__c = null and return a random quote from the system that doesn't have an opportunity       
            //GREEN-24950: Andrew - and redirect the user to this random quote. Need to add in a null check here on renewCont.SBQQ__RenewalOpportunity__c before running this query. 
            //10/18/17 - wendy.a.kelley - added null checking and additional query for retrieving quote record
            if(renewCont.SBQQ__RenewalOpportunity__c == null) {
                //start 13-12-2017 reymark.j.l.arlos removed the redirection to a random quote to continue waiting/checking if opportunity is created
                isSuccessful = false;
                //end 13-12-2017 reymark.j.l.arlos
            } else {
                renewQuote = [select id from SBQQ__Quote__c where SBQQ__Opportunity2__c = :renewCont.SBQQ__RenewalOpportunity__c and SBQQ__Primary__c = true limit 1];
                //start 13-12-2017 reymark.j.l.arlos setting to true to redirect user to renewal quote
                isSuccessful = true;
                //end 13-12-2017 reymark.j.l.arlos
            }
        }catch(Exception ex){
            system.debug('@@@*** ex.getMessage: ' + ex.getMessage());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Renewal Opportunity has no Primary Quote.'));
            isSuccessful = false;
            renderBackBtn = true;
        }
        //if(renewCont.SBQQ__RenewalOpportunity__r.SBQQ__PrimaryQuote__c != NULL && isSuccesskful){
        if(isSuccessful){
            //pRef = new PageReference('/' + renewCont.SBQQ__RenewalOpportunity__r.SBQQ__PrimaryQuote__c);
            pRef = new PageReference('/' + renewQuote.Id);
        }
        else{
            pRef = null;
        }

        //start 13-12-2017 reymark.j.l.arlos
        isSuccessful = true;
        //end 13-12-2017 reymark.j.l.arlos
        return pRef;
        
    }
    
    public PageReference backToContract(){
        return new PageReference('/' + contractRecord.Id);
    }
    
    
}