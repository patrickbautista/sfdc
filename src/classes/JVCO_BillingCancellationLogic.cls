/* ----------------------------------------------------------------------------------------------
    Name: JVCO_BillingCancellationLogic.cls 
    Description: Batch Class That Cancels Billing, Order, Quote, Opportunity, Contract, Subscriptions

    Date          Version     Author              Summary of Changes 
    -----------   -------     -----------------   -----------------------------------------
    10-Oct-2018   0.1         franz.g.a.dimaapi   GREEN-32724 - Refactor
    08-Nov-2018   0.2         franz.g.a.dimaapi   GREEN-33937 - Fix Subscription Duplicate Issue
    25-Oct-2019   0.3         franz.g.a.dimaapi   GREEN-35077 - Removed Billing Credited
  ----------------------------------------------------------------------------------------------- */
public class JVCO_BillingCancellationLogic implements Database.Batchable<sObject>
{
    private Set<Id> orderIdSet = new Set<Id>();
    private Set<Id> quoteIdSet = new Set<Id>();
    private Set<Id> quoteIdToDeleteSet = new Set<Id>();
    private Set<Id> oppIdSet = new Set<Id>();
    private Set<Id> oppIdToDeleteSet = new Set<Id>();
    private Set<Id> relatedContractToUpdateSet = new Set<Id>();
    private Map<Id, c2g__codaCreditNote__c> sInvIdToCNoteMap;
    private Id sInvId;
    
    public JVCO_BillingCancellationLogic()
    {
        this.sInvIdToCNoteMap = new Map<Id, c2g__codaCreditNote__c>();
    }
    
    public JVCO_BillingCancellationLogic(Map<Id, c2g__codaCreditNote__c> sInvIdToCNoteMap)
    {
        this.sInvIdToCNoteMap = sInvIdToCNoteMap;
        sInvId = (new List<Id>(sInvIdToCNoteMap.keySet()))[0];
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([SELECT Id, JVCO_Invoice_Type__c, JVCO_Sales_Invoice__c,
                                         JVCO_Sales_Invoice__r.JVCO_Credit_Reason__c,
                                         SBQQ__Quote__r.SBQQ__Opportunity2__c,
                                         SBQQ__Quote__c, JVCO_Total_Amount__c,
                                         SBQQ__Quote__r.SBQQ__Opportunity2__r.SBQQ__RenewedContract__c,
                                         SBQQ__Quote__r.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.SBQQ__RenewalQuoted__c
                                         FROM Order
                                         WHERE JVCO_Sales_Invoice__c IN :sInvIdToCNoteMap.keySet()
                                         ORDER BY Id DESC]);
    }

    public void execute(Database.BatchableContext BC, List<Order> scope)
    {
        stopperMethod();
        cancelBilling(scope);
        if(!orderIdSet.isEmpty())
        {
            cancelOrder();
            cancelQuote();
            cancelOpportunity();
            deleteContractAndSubscription();
        }    
    }

    private void cancelBilling(List<Order> orderList)
    {
        List<Order> updatedOrderList = new List<Order>();
        //Call Salesforce Billing API to create Billing Credit Note
        for(Order ord : orderList)
        {
            
            Order o = new Order();
            o.Id = ord.Id;
            o.JVCO_Sales_Credit_Note__c = sInvIdToCNoteMap.get(ord.JVCO_Sales_Invoice__c).Id;
            o.JVCO_Cancelled__c = true;
            o.JVCO_Surcharge_Generated__c = false;
            o.OpportunityId = null;
            o.SBQQ__Quote__c = null;
            updatedOrderList.add(o);
            //Only Applies to Non-Surcharge - GREEN-22473 - franz.g.a.dimaapi - 08/27/2017
            if(ord.JVCO_Invoice_Type__c != 'Surcharge')
            {   
                orderIdSet.add(ord.Id);
                quoteIdSet.add(ord.SBQQ__Quote__c);
                if(ord.JVCO_Sales_Invoice__r.JVCO_Credit_Reason__c == 'Reset to Previous Year - COVID' 
                   && ord.SBQQ__Quote__r.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.SBQQ__RenewalQuoted__c)
                {
                    quoteIdToDeleteSet.add(ord.SBQQ__Quote__c);
                    oppIdToDeleteSet.add(ord.SBQQ__Quote__r.SBQQ__Opportunity2__c);
                    relatedContractToUpdateSet.add(ord.SBQQ__Quote__r.SBQQ__Opportunity2__r.SBQQ__RenewedContract__c);
                }
                oppIdSet.add(ord.SBQQ__Quote__r.SBQQ__Opportunity2__c);
            }
        }
        update updatedOrderList;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Cancel Order and Removed Billing Date in OrderProduct

        Inputs: 
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        27-Aug-2017 franz.g.a.dimaapi    Initial version of function
        11-Oct-2017 jasper.j.figueroa    Addedd JVCO_Sales_Invoice__c in the query and set it to NULL - GREEN-22873
        16-Jan-2017 michael.alamag       Updated the blng__HoldBilling__c in OrderItems - GREEN 27031
        13-Jun-2018 franz.g.a.dimaapi    GREEN-32217 - Added Billing Rule
        10-Oct-2018 franz.g.a.dimaapi    GREEN-32724 - Clean method
        30-Jan-2019 mel.andrei.b.santos  GREEN-33826 - updated code to unlink quote line from the order product 
    ----------------------------------------------------------------------------------------------- */
    private void cancelOrder()
    {
        //Start 30-Jan-2019 mel.andrei.b.santos  GREEN-33826
        List<OrderItem> updateOrderProductList = new List<OrderItem>();
        List<OrderItem> orderProductList = [SELECT id, SBQQ__QuoteLine__c, blng__HoldBilling__c, SBQQ__Activated__c, 
                                            blng__BilledAmountwithouttax__c, blng__BilledTax__c  
                                            FROM OrderItem WHERE orderid IN : orderIdSet];

        for(OrderItem orderprod : orderProductList)
        {
            orderprod.SBQQ__QuoteLine__c = null;
            orderprod.blng__HoldBilling__c = 'Yes';
            orderprod.SBQQ__Activated__c = false;
            orderprod.blng__BilledTax__c = 0;
            orderprod.blng__BilledAmountwithouttax__c = 0;
            updateOrderProductList.add(orderprod);

        }
        update updateOrderProductList;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Delete Related Contract From Opportunity
                     After Cancelling Sales Invoice- GREEN-22487

        Inputs: 
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        13-Nov-2017 franz.g.a.dimaapi    Initial version of function
        20-Mar-2017 franz.g.a.dimaapi    Fix Quote record type - GREEN-30969
        18-Sept-2020 rhys.j.c.dela.cruz  GREEN-35878 - Adjusted method to update OwnerId when Type Quote is created by Automation
    ----------------------------------------------------------------------------------------------- */
    private void cancelQuote()
    {
        List<SBQQ__Quote__c> updatedQuoteList = new List<SBQQ__Quote__c>();

        List<User> userList = [SELECT Id FROM User WHERE Name = 'Automation User' LIMIT 1];
        User tempUser = new User();
        if(!userList.isEmpty()){
            tempUser = userList[0];
        }

        Map<Id, SBQQ__Quote__c> qMap = new Map<Id, SBQQ__Quote__c>([SELECT Id, CreatedById, SBQQ__Type__c FROM SBQQ__Quote__c WHERE Id IN: quoteIdSet]);

        for (Id qId : quoteIdSet)
        {   
            SBQQ__Quote__c q = new SBQQ__Quote__c();
            q.Id = qId;
            if(!quoteIdToDeleteSet.contains(qId))
                q.EditLinesFieldSetName__c = 'Cancelled_Invoice';//GREEN-18635 - franz.g.a.dimaapi
            q.JVCO_QuoteComplete__c = false;
            q.JVCO_Cancelled__c = true;
            q.JVCO_Sales_Invoice__c = null;//Set to NULL - GREEN-22873 - jasper.j.figueroa
            q.JVCO_Prior_Sales_Invoice__c = sInvId;
            q.SBQQ__Status__c = 'Draft';//Changed to Draft - GREEN-22487 - franz.g.a.dimaapi - 08/26/2017
            q.RecordTypeId = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('Open Quote').getRecordTypeId();

            if(qMap.containsKey(qId)){

                if(qMap.get(qId).CreatedById == tempUser.Id){
                    q.OwnerId = UserInfo.getUserId();
                }
            }

            updatedQuoteList.add(q);
        }
        update updatedQuoteList;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Update Opportunity Fields
                     After Cancelling Sales Invoice- GREEN-22487

        Inputs: Set<Id>
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change>
        11-Sep-2017 franz.g.a.dimaapi    Initial version of function
        30-Jan-2018 mel.andrei.b.santos  Updated cancelOpportunity which will set JVCO_OpportunityCancelled__c to true if triggered based on GREEN-29724
    ----------------------------------------------------------------------------------------------- */
    private void cancelOpportunity()
    {
        List<Opportunity> updatedOppList = new List<Opportunity>();
        for(Id oppId : oppIdSet)
        {
            Opportunity opp = new Opportunity();
            opp.Id = oppId;
            opp.StageName = 'Quoting';
            opp.Probability = 50;
            opp.JVCO_Invoiced__c = FALSE;
            opp.SBQQ__Ordered__c = FALSE;
            opp.SBQQ__Contracted__c = FALSE;
            opp.JVCO_OpportunityCancelled__c = TRUE;
            updatedOppList.add(opp);
        }
        update updatedOppList;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: franz.g.a.dimaapi
        Company: Accenture
        Description: Delete Related Contract From Opportunity
                     After Cancelling Sales Invoice- GREEN-22487

        Inputs: 
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        27-Aug-2017 franz.g.a.dimaapi    Initial version of function
        07-Aug-2018 franz.g.a.dimaapi    GREEN-32724 - Delete Subscriptions For Amendments
        24-Sep-2018 franz.g.a.dimaapi    GREEN-32724 - Reset Subscription Terminated Date
        15-Nov-2018 franz.g.a.dimaapi    GREEN-33962 - Fixed Terminated QL but no Subscription
    ----------------------------------------------------------------------------------------------- */
    private void deleteContractAndSubscription()
    {
        JVCO_FFUtil.stopContractDeletion = true;
        JVCO_FFUtil.stopSubscriptionDeletion = true;
        //Delete Subscription for Amendment
        Set<SBQQ__Subscription__c> terminatedSubscriptionSet = new Set<SBQQ__Subscription__c>();
        List<SBQQ__Subscription__c> subscriptionList = [SELECT Id, SBQQ__QuoteLine__r.Terminate__c,
                                                        SBQQ__QuoteLine__r.SBQQ__UpgradedSubscription__c
                                                        FROM SBQQ__Subscription__c
                                                        WHERE SBQQ__QuoteLine__r.SBQQ__Quote__c IN :quoteIdSet
                                                        AND SBQQ__QuoteLine__r.SBQQ__Quote__r.SBQQ__Type__c IN ('Amendment')];
        for(SBQQ__Subscription__c subscription : subscriptionList)
        {
            if(subscription.SBQQ__QuoteLine__r.Terminate__c && subscription.SBQQ__QuoteLine__r.SBQQ__UpgradedSubscription__c != null)
            {
                SBQQ__Subscription__c terminatedSubscription = new SBQQ__Subscription__c();
                terminatedSubscription.Id = subscription.SBQQ__QuoteLine__r.SBQQ__UpgradedSubscription__c;
                terminatedSubscription.SBQQ__TerminatedDate__c = null;
                terminatedSubscriptionSet.add(terminatedSubscription);
            }
        }
        //Update Terminated Subscription
        update new List<SBQQ__Subscription__c>(terminatedSubscriptionSet);
        delete subscriptionList;
        //Delete Contract
        delete [SELECT Id FROM Contract WHERE SBQQ__Quote__c IN :quoteIdSet];
        
        List<Contract> contractToUpdateList = new List<Contract>();
        
        for(Id contractId : relatedContractToUpdateSet)
        {
            Contract contract = new Contract();
            contract.Id = contractId;
            contract.SBQQ__RenewalQuoted__c = false;
            contractToUpdateList.add(contract);
        }
        if(!contractToUpdateList.isEmpty())
            update contractToUpdateList;
        
        delete [SELECT Id FROM SBQQ__Quote__c WHERE Id IN :quoteIdToDeleteSet];
        delete [SELECT Id FROM Opportunity WHERE Id IN: oppIdToDeleteSet];
    }

    private void stopperMethod()
    {
        JVCO_FFUtil.stopAccountDLRSHandler = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = false;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        JVCO_FFUtil.stopCodaInvoiceHandlerAfterUpdate = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_FFUtil.stopContractHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopContractHandlerAfterUpdate = true;
        JVCO_FFUtil.stopQuoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopQuoteHandlerBeforeUpdate = true;
        //JVCO_FFUtil.stopOpportunityHandlerAfterUpdate = true;
        JVCO_FFUtil.stopOpportunityHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopOrderAfterUpdateHandler = true;
        JVCO_FFUtil.stopOrderBeforeUpdateHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
    }

    public void finish(Database.BatchableContext BC)
    {
        Id jobId = Database.executeBatch(new JVCO_SurchargeInvoiceCancellationBatch(sInvIdToCNoteMap));
        //START    
        if(!sInvIdToCNoteMap.isEmpty()){
            String key = '';
            for (String S: sInvIdToCNoteMap.keySet()){
                key = s;
                system.debug(key);
                break;
            }
            Account accRec = new Account();
            accRec.Id = sInvIdToCNoteMap.get(key).c2g__Account__c;
            accRec.JVCO_BillingCancellationApexJob__c = jobId;
            update accRec;  
        }
        //END  GREEN-35872 - mariel.m.buena - 23/10/2020
    }
}