/* -----------------------------------------------------------------------------------------------
   Name: JVCO_RemoveSINFromCaseLogic.cls 
   Description: Business logic class for removing Sales Invoices from Case

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   --------------------------------------------------
   11-Feb-2021   0.1         Luke.Walker      Intial creation
   ----------------------------------------------------------------------------------------------- */

public with sharing class JVCO_RemoveSINFromCaseLogic 
{
    private ApexPages.StandardSetController stdCtrl;
    private List<c2g__codaInvoice__c> selectedInvoices;
    private id caseID;
    public Integer numOfSelectedInvoices {get; set;}
    public final String NO_RECORDS_ERR = System.Label.JVCO_NoSalesInvoice;

    //constructor
    public JVCO_RemoveSINFromCaseLogic(ApexPages.StandardSetController ssc){
       stdCtrl = ssc;
       selectedInvoices = (List<c2g__codaInvoice__c>)ssc.getSelected();
       numOfSelectedInvoices = selectedInvoices.size();
       caseID = ApexPages.currentPage().getParameters().get('id');
    }
    
    public PageReference remove()
    {  
        List<c2g__codaInvoice__c> InvoiceList = new List<c2g__codaInvoice__c>();
        Case caseRecord;
        
        if(numOfSelectedInvoices > 0){
            InvoiceList = [SELECT ID, JVCO_Case_Id__c FROM c2g__codaInvoice__c WHERE ID IN :selectedInvoices];
            caseRecord = [SELECT ID FROM Case WHERE ID = :caseID];

            List<c2g__codaInvoice__c> updateInvoiceList = new List<c2g__codaInvoice__c>();
            
            for(c2g__codaInvoice__c c: InvoiceList){
                c.JVCO_Case_Id__c = NULL;
                c.JVCO_Write_Off_Status__c = NULL;
                updateInvoiceList.add(c);
            }
            
            if(updateInvoiceList.size() > 0){
                update updateInvoiceList;
                update caseRecord;
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, NO_RECORDS_ERR));
            return null;
        }
        
        String uiThemeDisplayed = UserInfo.getUiThemeDisplayed();
        PageReference pageRef = new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
        
        //Create URL for Lightning or Classic
        if(uiThemeDisplayed == 'Theme4d' || uiThemeDisplayed == 'Theme4u'){
            aura.redirect(pageRef);
            return pageRef;
        }
        pageRef.setRedirect(true);    
        return pageRef;
    }
    
    public PageReference returnToCase()
    {
        PageReference page = new PageReference(stdCtrl.cancel().getUrl());
        page.setRedirect(TRUE);
        return page;
    }
    
}