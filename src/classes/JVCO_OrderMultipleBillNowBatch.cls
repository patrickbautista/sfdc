/* ----------------------------------------------------------------------------------------------
    Name: JVCO_OrderMultipleBillNowBatch.cls 
    Description: Batch class that handles Bill Now operation for multiple Order records

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    23-Mar-2017   0.1         ryan.i.r.limlingan  Intial creation
    17-Jul-2017   0.2         franz.g.a.dimaapi   Added JVCO_Surcharge_Tarrifs__c on blngInvoiceMap query
    07-Sep-2017   0.2         franz.g.a.dimaapi   Added JVCO_PONumber__c and CreatedDate on query
    15-Nov-2017   0.3         michael.alamag      Add two fields in in query (JVCO_PPL_Gap__c and JVCO_Contact_Type_c ) 
    06-Dec-2017   0.4         john.patrick.valdez Added checking of JVCO_Total_Amount__c (Positive=Sales Invoice 
                                                / Negative=Sales Credit Note)
    11-Apr-2018   0.5         franz.g.a.dimaapi   Refactor whole class
----------------------------------------------------------------------------------------------- */
public class JVCO_OrderMultipleBillNowBatch implements Database.Batchable<sObject>, Database.Stateful
{    
    private List<Order> orderToProcessList;
    private Set<Id> orderGroupSet = new Set<Id>();

    public JVCO_OrderMultipleBillNowBatch(List<Order> oList)
    {
        orderToProcessList = oList;    
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([SELECT Id, blng__BillNow__c, JVCO_Order_Group__c
                                         FROM Order WHERE Id IN :orderToProcessList]);
    }

    public void execute(Database.BatchableContext BC, List<Order> scope)
    {
        List<Order> updatedOrderList = new List<Order>();
        for(Order o : scope)
        {
            o.blng__BillNow__c = true;
            updatedOrderList.add(o);
            orderGroupSet.add(o.JVCO_Order_Group__c);
        }
        update updatedOrderList;
    }
    
    public void finish(Database.BatchableContext BC)
    {
        Id jobId = Database.executeBatch(new JVCO_OrderMultipleBillNowLogicBatch(orderGroupSet), 1);
    }   
}