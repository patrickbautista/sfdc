public class MergeUtility{
    
    public static void calculateBalance(Set<Id> masterAccountIds){
        c2g.BalanceUpdateService.updateBalancesForMergedAccountsAsync(masterAccountIds);
    }
}