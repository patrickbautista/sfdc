/* ----------------------------------------------------------------------------------------------
    Name: JVCO_CreateDDFromDDMandateBatch
    Description: Batch for Posting Sales Invoice, Email Invalid DD Mandate, Creation of Direct Debit

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    18-May-2018  0.1         franz.g.a.dimaapi     Intial creation
    05-Oct-2018  0.2         john.patrick.valdez   GREEN-33185 - CC Sin field not always populating in Invoice Group
    07-Feb-2018  0.4         franz.g.a.dimaapi     GREEN-34314 - Changed Bank Details Mapping
----------------------------------------------------------------------------------------------- */
public class JVCO_CreateDDFromDDMandateBatch implements Database.Batchable<sObject>{

    private Set<Id> sInvIdSet = new Set<Id>();

    public JVCO_CreateDDFromDDMandateBatch(){}
    public JVCO_CreateDDFromDDMandateBatch(Set<Id> sInvIdSet)
    {
        this.sInvIdSet = sInvIdSet;
    }

    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT Id, Name, JVCO_Invoice_Group__c,
                                        c2g__OutstandingValue__c,
                                        c2g__Account__r.JVCO_DD_Mandate_Checker__c,
                                        JVCO_Outstanding_Amount_to_Process__c
                                        FROM c2g__codaInvoice__c
                                        WHERE Id IN : sInvIdSet
                                        AND JVCO_Invoice_Group__c = NULL
                                        AND c2g__OutstandingValue__c > 0
                                        AND c2g__Account__r.JVCO_DD_Mandate_Checker__c = false]);
    }

    public void execute(Database.BatchableContext bc, List<c2g__codaInvoice__c> scope)
    {   
        createDirectDebit(scope);
    }

    private void createDirectDebit(List<c2g__codaInvoice__c> sInvList)
    {
        Map<Id, List<c2g__codaInvoice__c>> accIdToSInvListMap = new Map<Id, List<c2g__codaInvoice__c>>();
        Map<Id, Double> accIdToSInvTotAmt = new Map<Id, Double>();
        for(c2g__codaInvoice__c sInv : sInvList)
        {   
            //Account to Sales Invoice List Map
            Id accId = sInv.c2g__Account__c;
            if(!accIdToSInvListMap.containsKey(accId))
            {
                accIdToSInvListMap.put(accId, new List<c2g__codaInvoice__c>{});
            }
            accIdToSInvListMap.get(accId).add(sInv);
            //Account to Sales Invoice Total Amount Map
            if(!accIdToSInvTotAmt.containsKey(sInv.c2g__Account__c))
            {
                accIdToSInvTotAmt.put(accId, 0);
            }
            accIdToSInvTotAmt.put(accId, accIdToSInvTotAmt.get(accId) + sInv.c2g__OutstandingValue__c);
        }

        //List of DD Mandates from Account
        List<JVCO_DD_Mandate__c> ddMandateList = null;
        if(!accIdToSInvListMap.isEmpty())
        {
            ddMandateList = [SELECT Id, Name, 
                            JVCO_Licence_Account__r.JVCO_DD_Payee_Contact__c, 
                            JVCO_Account_Number__c,
                            JVCO_Sort_Code__c,
                            JVCO_Number_of_Payments__c, JVCO_Collection_Day__c,
                            JVCO_Deactivated__c 
                            FROM JVCO_DD_Mandate__c 
                            WHERE JVCO_Licence_Account__c IN: accIdToSInvListMap.keySet()
                            AND JVCO_Number_of_Payments__c != NULL
                            AND JVCO_Collection_Day__c != NULL
                            AND JVCO_Account_Number__c != NULL
                            AND JVCO_Sort_Code__c != NULL
                            AND JVCO_Deactivated__c = FALSE];
        }

        //AccountId to Payment Plan Map                                
        Map<Id, String> ddMandateAccIdToPymtPlanMap = new Map<Id, String>();
        Map<Id, JVCO_DD_Mandate__c> accIDToDDMandateMap = new Map<Id, JVCO_DD_Mandate__c>();
        Set<String> accNumberSet = new Set<String>();
        if(ddMandateList != null)
        {
            for(JVCO_DD_Mandate__c ddMandate : ddMandateList)
            {
                String pymtPlanMapping = ddMandate.JVCO_Number_of_Payments__c + '-' + ddMandate.JVCO_Collection_Day__c;
                ddMandateAccIdToPymtPlanMap.put(ddMandate.JVCO_Licence_Account__c, pymtPlanMapping);
                accIDToDDMandateMap.put(ddMandate.JVCO_Licence_Account__c, ddMandate);
                accNumberSet.add(ddMandate.JVCO_Account_Number__c);
            }
        }

        Map<Id, JVCO_Invoice_Group__c> accIdToIGMap = new Map<Id, JVCO_Invoice_Group__c>();
        for(Id accId : ddMandateAccIdToPymtPlanMap.keySet())
        {   
            JVCO_Invoice_Group__c ig = new JVCO_Invoice_Group__c();
            ig.JVCO_Total_Amount__c = accIdToSInvTotAmt.get(accId);
            String sInvNameStr = '';
            for(c2g__codaInvoice__c sInv : accIdToSInvListMap.get(accId))
            {
                sInvNameStr += sInv.Name + ',';
            }
            ig.CC_SINs__c = sInvNameStr;
            accIdToIGMap.put(accId, ig);
        }

        if(!accIdToIGMap.isEmpty())
        {
            //Insert Invoice Group
            insert accIdToIGMap.values();
            List<c2g__codaInvoice__c> updatedSInvList = new List<c2g__codaInvoice__c>();
            for (Id accId : accIdToIGMap.keySet())
            {
                JVCO_Invoice_Group__c ig = accIdToIGMap.get(accId);
                for(c2g__codaInvoice__c sInv : accIdToSInvListMap.get(accId))
                {   
                    // Fields to update
                    sInv.JVCO_Invoice_Group__c = ig.Id;
                    sInv.JVCO_Outstanding_Amount_to_Process__c = sInv.c2g__OutstandingValue__c;
                    updatedSInvList.add(sInv);
                }    
            }
            if(!updatedSInvList.isEmpty())
            {
                //Update Sales Invoice's Invoice List
                update updatedSInvList;
                //Create Payonomy Payment Agreement
                createPaymentAgreement(accIdToIGMap, accNumberSet, accIDToDDMandateMap, ddMandateAccIdToPymtPlanMap);
            }   
        }
    }

    private void createPaymentAgreement(Map<Id, JVCO_Invoice_Group__c> accIdToIGMap,
                                        Set<String> accNumberSet,
                                        Map<Id, JVCO_DD_Mandate__c> accIDToDDMandateMap,
                                        Map<Id, String> ddMandateAccIdToPymtPlanMap)
    {   
        //Set Account Id to Bank Account Map
        Map<Id, Id> accIdToBankAccIdMap = setAccIdToBankAccIdMap(accNumberSet, accIDToDDMandateMap);
        //Set Account Id To Payonomy Payment Schedule Map
        Map<Id, PAYREC2__Payment_Schedule__c> accIdToPymtSchedMap = setAccIdToPymtSchedMap(ddMandateAccIdToPymtPlanMap);
        //Set Invoice Group Map
        Map<Id, JVCO_Invoice_Group__c> igMap = new Map<Id,JVCO_Invoice_Group__c>(
                                                    [SELECT Name 
                                                    FROM JVCO_Invoice_Group__c 
                                                    WHERE Id IN :accIdToIGMap.values()]);

        List<PAYREC2__Payment_Agreement__c> agreementList = new List<PAYREC2__Payment_Agreement__c>();
        for(Id accId : accIdToIGMap.keySet())
        {
            JVCO_Invoice_Group__c ig = accIdToIGMap.get(accId);
            PAYREC2__Payment_Agreement__c agreement = new PAYREC2__Payment_Agreement__c();
            agreement.PAYREC2__Account__c = accId;
            agreement.PAYREC2__Payment_Schedule__c = accIdToPymtSchedMap.get(accId).Id;
            agreement.PAYREC2__Description__c = igMap.get(ig.Id).Name;
            agreement.PAYREC2__Status__c = 'New instruction';
            agreement.PAYFISH3__Current_Bank_Account__c = accIdToBankAccIdMap.get(accId);
            agreement.JVCO_BypassVR__c = true;
            agreement.JVCO_DD_Confirmation_Exception__c = true;

            // Collection amounts computation
            Integer numberOfInstalments = Integer.valueOf(accIdToPymtSchedMap.get(accId).PAYREC2__Max__c);
            Double totalAmount = ig.JVCO_Total_Amount__c;
            if(numberOfInstalments == 1)
            {
                agreement.PAYREC2__Ongoing_Collection_Amount__c = totalAmount;   
            }else
            {
                Decimal ongoingCollectionAmt = totalAmount / numberOfInstalments;
                agreement.PAYREC2__Ongoing_Collection_Amount__c = ongoingCollectionAmt.setScale(2);
                if(numberOfInstalments > 2)
                {
                    agreement.PAYREC2__First_Collection_Amount__c = ongoingCollectionAmt.setScale(2);
                }
                Decimal finalCollectionAmt = totalAmount - (ongoingCollectionAmt.setScale(2) * (numberOfInstalments - 1));
                agreement.PAYREC2__Final_Collection_Amount__c = finalCollectionAmt.setScale(2);
            }
            agreementList.add(agreement);
        }

        insert agreementList;
    }

    private Map<Id, Id> setAccIdToBankAccIdMap(Set<String> accNumberSet, Map<Id, JVCO_DD_Mandate__c> accIDToDDMandateMap)
    {   
        Map<String, Map<String, Id>> accIdToBankAccDetailsMap = new Map<String, Map<String, Id>>();
        Map<Id, Id> accIdToBankAccIdMap = new Map<Id, Id>();
        for(PAYACCVAL1__Bank_Account__c bankAccount : [SELECT Id, PAYACCVAL1__Account_Number__c,
                                                        PAYACCVAL1__Sort_Code__c
                                                        FROM PAYACCVAL1__Bank_Account__c
                                                        WHERE PAYACCVAL1__Account_Number__c IN :accNumberSet])
        {
            if(!accIdToBankAccDetailsMap.containsKey(bankAccount.PAYACCVAL1__Account_Number__c))
            {
                accIdToBankAccDetailsMap.put(bankAccount.PAYACCVAL1__Account_Number__c, new Map<String, Id>());
            }
            accIdToBankAccDetailsMap.get(bankAccount.PAYACCVAL1__Account_Number__c).put(bankAccount.PAYACCVAL1__Sort_Code__c, bankAccount.Id);
        }

        for(Id accId : accIDToDDMandateMap.keySet())
        {
            JVCO_DD_Mandate__c ddMandate = accIDToDDMandateMap.get(accId);
            if(accIdToBankAccDetailsMap.containsKey(ddMandate.JVCO_Account_Number__c) &&
                accIdToBankAccDetailsMap.get(ddMandate.JVCO_Account_Number__c).containsKey(ddMandate.JVCO_Sort_Code__c))
            {
                accIdToBankAccIdMap.put(accId, accIdToBankAccDetailsMap.get(ddMandate.JVCO_Account_Number__c).get(ddMandate.JVCO_Sort_Code__c));
            }
        }
        return accIdToBankAccIdMap;
    }

    private Map<Id, PAYREC2__Payment_Schedule__c> setAccIdToPymtSchedMap(Map<Id, String> ddMandateAccIdToPymtPlanMap)
    {
        Map<Id, PAYREC2__Payment_Schedule__c> accIdToPymtSchedMap = new Map<Id, PAYREC2__Payment_Schedule__c>();
        //Map Payment Plan Mapping to Payment Schedule
        Map<String, PAYREC2__Payment_Schedule__c> pymtPlanToPymtSchedIdMap = new Map<String, PAYREC2__Payment_Schedule__c>();
        for(PAYREC2__Payment_Schedule__c pymtSched : [SELECT Id, JVCO_Payment_Plan_Mapping__c, 
                                                        PAYREC2__Max__c
                                                        FROM PAYREC2__Payment_Schedule__c
                                                        WHERE JVCO_Payment_Plan_Mapping__c 
                                                        IN :ddMandateAccIdToPymtPlanMap.values()
                                                        AND PAYREC2__Max__c <> NULL
                                                        AND PAYREC2__Day__c <> NULL])
        {
            pymtPlanToPymtSchedIdMap.put(pymtSched.JVCO_Payment_Plan_Mapping__c, pymtSched);
        }

        //Map Account Id to Payment Plan Schedule
        for(Id accId : ddMandateAccIdToPymtPlanMap.keySet())
        {   
            String pymtMapping = ddMandateAccIdToPymtPlanMap.get(accId);
            if(pymtPlanToPymtSchedIdMap.containsKey(pymtMapping))
            {
                accIdToPymtSchedMap.put(accId, pymtPlanToPymtSchedIdMap.get(pymtMapping));
            }    
        }
        return accIdToPymtSchedMap;
    }

    public void finish(Database.BatchableContext bc){}  
}