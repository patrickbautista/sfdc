@isTest
private class JVCO_ActivateOrderLogicTest
{
	@testSetup static void setupTestDate(){
        JVCO_TestClassObjectBuilder.createBillingConfig();
        
        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Test.startTest();
		Product2 prod1 = new Product2();
        prod1.Name = 'Test Product 83202-1';
        prod1.ProductCode = 'TP 83202-1';
        prod1.SBQQ__ChargeType__c= 'One-Time';
        insert prod1;

        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_Opportunity', Value__c = 'Quote did not successfully complete because the \'Opportunity\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_ContactType', Value__c = 'Quote did not successfully complete because the \'Contact Type\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_Primary', Value__c = 'Quote did not successfully complete because the \'Primary\' checkbox was not ticked'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_StartDate', Value__c = 'Quote did not successfully complete because the \'Start Date\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_SubscriptionTerm', Value__c = 'Quote did not successfully complete because the \'Subscription Term\' was not provided'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'ThresholdAndStatusErrorMsg', Value__c = 'You cannot complete the quote if its credit is requiring an approval'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'UpdateOppAndQuoteFail', Value__c = 'The update did not push through because an error occurred. Details are as follows:'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'UpdateOppAndQuoteSuccess', Value__c = 'Quote Successfully Completed'));
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'MissingMandatoryField_AcceptT&Cs', Value__c = 'Quote did not successfully complete because the \'T&C\' was not checked'));
        insert settings;
        
        Account a1 = JVCO_TestClassObjectBuilder.createCustomerAccount();
        a1.Name = 'Test Account 83202-C';
        a1.Type = 'Key Account';
        insert a1;
        
        //Create Contact
        Contact testContact = JVCO_TestClassObjectBuilder.createContact(a1.Id);
        testContact.Firstname = 'Test';
        testContact.LastName = 'TestContact';
        insert testContact;
        
        Account a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.id);
        a2.Name = 'Test Account 83202-L';
        a2.JVCO_Customer_Account__c = a1.Id;
        //a2.JVCO_Preferred_Contact_Method__c = 'Email';
        a2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        a2.JVCO_Billing_Contact__c = testContact.id;
        a2.JVCO_Licence_Holder__c = testContact.id;
        a2.JVCO_Review_Contact__c = testContact.id;
        a2.Type = 'Agency';
        insert a2;

        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();
        venueRecord.Name = 'test1';              
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = new JVCO_Affiliation__c();
        affilRecord.JVCO_Account__c = a2.Id;
        affilRecord.JVCO_Venue__c = venueRecord.id;   
        affilRecord.JVCO_Start_Date__c = system.today();   
        Insert  affilRecord;  
        
        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 83202';
        o.AccountId = a2.Id;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o; 

        Pricebook2 testPB2 = new Pricebook2();
        testPB2.name = 'Standard Price Book';
        testPB2.IsActive = true;
        
        insert testPB2;

        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Renewal';
        q.SBQQ__Status__c = 'Approved';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        //q.SBQQ__PricebookId__c = test.getStandardPriceBookId();
        q.JVCO_Contact_Type__c = 'Email';
        q.Accept_TCs__c = 'yes';
        insert q;

        o.Probability = 100;
        o.Amount = 10.00;
        o.StageName = 'Draft';
        update o;  

        Contract contr = new Contract();
        contr.AccountId = a2.Id;
        contr.Status = 'Draft';
        contr.StartDate = Date.today();
        contr.ContractTerm = 12;
        contr.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
        contr.SBQQ__RenewalQuoted__c = FALSE;
        contr.SBQQ__Quote__c = q.id;
        contr.SBQQ__Opportunity__c = o.id;
        insert contr;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        ql.SBQQ__ChargeType__c= 'One-Time';
        insert ql; 

        PriceBookEntry testPB = new PriceBookEntry();
        testPB.Pricebook2Id = test.getStandardPriceBookId();
        testPB.product2id = prod1.id;
        testPB.IsActive = true;
        testPB.UnitPrice = 100.0;
        insert testPB;
		
		Test.stopTest();
        /*OpportunityLineItem testOppLineItem = new OpportunityLineItem();
        testOppLineItem.OpportunityId = o.id;
        testOppLineItem.Quantity = 1.0;
        testOppLineItem.UnitPrice = 100;
        testOppLineItem.PricebookEntryId = testPB.id;
        insert testOppLineItem;*/

        /*o.SBQQ__Contracted__c = true;
        o.SBQQ__Ordered__c = true;

        update o;*/



	}

	@isTest
	static void OrderActivate()
	{

		Account testAccount = [select id from account order by CreatedDate DESC limit 1];
        

        system.debug('TestAccount' + testAccount);

        Product2 testProd = [select id from product2 order by CreatedDate DESC limit 1];

        Opportunity testOpp = [select id, StageName, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity order by CreatedDate DESC limit 1];

        SBQQ__Quote__c testQuote = [select id, SBQQ__NetAmount__c, Recalculate__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c order by CreatedDate DESC limit 1];
        
        SBQQ__QuoteLine__c ql = [select id, 
                                    SBQQ__SpecialPriceType__c, 
                                    SBQQ__ListPrice__c, 
                                    SBQQ__CustomerPrice__c,
                                    SBQQ__NetPrice__c,
                                    SBQQ__SpecialPrice__c,
                                    SBQQ__RegularPrice__c,
                                    SBQQ__UnitCost__c,
                                    SBQQ__ProratedListPrice__c
                                 from SBQQ__QuoteLine__c order by CreatedDate DESC limit 1];

        Order testOrderProd = new Order();

        Test.startTest();

		Order testOrder = new Order();
        testorder.Accountid = testAccount.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAccount.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        insert testOrder;  



        PriceBookEntry testPB = [select id from PriceBookEntry order by CreatedDate DESC limit 1];

        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = 100.00;
        testOrderItem.EndDate = date.today().addMonths(5);
        insert testOrderItem;

        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
        update testOrderItem;
        

        List<Order> listOrder = new list<Order>();
        listOrder.add(testOrder);

        testAccount.type = 'Agency';
        update testAccount;

        
        //testOrderProd = [select id from Order order by CreatedDate DESC limit 1];
        //system.assertEquals(null, testOrderProd.id, 'Error in id');

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(sc);
        actOrder.activate();

		//Order selectOrder = [select id, Status from Order where id = :testOrder.id];
		//system.assertEquals('Activated',selectOrder.status,'Error in selectedOrder Status');

        test.stoptest();
	}

    @isTest
    static void OrderActivatePositive()
    {

        Account testAccount = [select id from account order by CreatedDate DESC limit 1];
        

        system.debug('TestAccount' + testAccount);

        Product2 testProd = [select id from product2 order by CreatedDate DESC limit 1];

        Opportunity testOpp = [select id, StageName, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity order by CreatedDate DESC limit 1];

        SBQQ__Quote__c testQuote = [select id, SBQQ__NetAmount__c, Recalculate__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c order by CreatedDate DESC limit 1];
        
        SBQQ__QuoteLine__c ql = [select id, 
                                    SBQQ__SpecialPriceType__c, 
                                    SBQQ__ListPrice__c, 
                                    SBQQ__CustomerPrice__c,
                                    SBQQ__NetPrice__c,
                                    SBQQ__SpecialPrice__c,
                                    SBQQ__RegularPrice__c,
                                    SBQQ__UnitCost__c,
                                    SBQQ__ProratedListPrice__c
                                 from SBQQ__QuoteLine__c order by CreatedDate DESC limit 1];

        Order testOrderProd = new Order();

        Test.startTest();

        Order testOrder = new Order();
        testorder.Accountid = testAccount.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAccount.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        insert testOrder;  



        PriceBookEntry testPB = [select id from PriceBookEntry order by CreatedDate DESC limit 1];

        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = 100.00;
        testOrderItem.EndDate = date.today().addMonths(5);
        insert testOrderItem;

        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
        update testOrderItem;
        

        List<Order> listOrder = new list<Order>();
        listOrder.add(testOrder);
        update listOrder;

        testAccount.type = 'Agency';
        update testAccount;

        
        //testOrderProd = [select id from Order order by CreatedDate DESC limit 1];
        //system.assertEquals(null, testOrderProd.id, 'Error in id');

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(sc);
        actOrder.activate();

        //Order selectOrder = [select id, Status from Order where id = :testOrder.id];
        //system.assertEquals('Activated',selectOrder.status,'Error in selectedOrder Status');

        test.stoptest();
    }

    @isTest
    static void OrderActivateNegative()
    {

        Account testAccount = [select id from account order by CreatedDate DESC limit 1];
        

        system.debug('TestAccount' + testAccount);

        Product2 testProd = [select id from product2 order by CreatedDate DESC limit 1];

        Opportunity testOpp = [select id, StageName, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity order by CreatedDate DESC limit 1];

        SBQQ__Quote__c testQuote = [select id, SBQQ__NetAmount__c, Recalculate__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c order by CreatedDate DESC limit 1];
        
        SBQQ__QuoteLine__c ql = [select id, 
                                    SBQQ__SpecialPriceType__c, 
                                    SBQQ__ListPrice__c, 
                                    SBQQ__CustomerPrice__c,
                                    SBQQ__NetPrice__c,
                                    SBQQ__SpecialPrice__c,
                                    SBQQ__RegularPrice__c,
                                    SBQQ__UnitCost__c,
                                    SBQQ__ProratedListPrice__c
                                 from SBQQ__QuoteLine__c order by CreatedDate DESC limit 1];

        Order testOrderProd = new Order();

        Test.startTest();

        Order testOrder = new Order();
        testorder.Accountid = testAccount.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAccount.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        testOrder.JVCO_Credit_Reason__c  = 'Invoice amendment';
        insert testOrder;  



        PriceBookEntry testPB = [select id from PriceBookEntry order by CreatedDate DESC limit 1];

        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = -100.00;
        testOrderItem.SBQQ__TotalAmount__c = -1000.00;
        testOrderItem.EndDate = date.today().addMonths(5);
        insert testOrderItem;

        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
        update testOrderItem;
        

        List<Order> listOrder = new list<Order>();
        listOrder.add(testOrder);
        update listOrder;

        testAccount.type = 'Agency';
        update testAccount;

        
        //testOrderProd = [select id from Order order by CreatedDate DESC limit 1];
        //system.assertEquals(null, testOrderProd.id, 'Error in id');

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(sc);
        actOrder.activate();

        //Order selectOrder = [select id, Status from Order where id = :testOrder.id];
        //system.assertEquals('Activated',selectOrder.status,'Error in selectedOrder Status');

        test.stoptest();
    }

    

    @isTest
    static void OrderActivatePOC()
    {

        Account testAccount = [select id from account order by CreatedDate DESC limit 1];
        system.debug('TestAccount' + testAccount);
        Account custAccount = [select id from account where name = 'Test Account 83202-C' limit 1 ];

        PageReference dumpRef;

        Product2 testProd = [select id from product2 order by CreatedDate DESC limit 1];

        Opportunity testOpp = [select id, StageName, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity order by CreatedDate DESC limit 1];

        SBQQ__Quote__c testQuote = [select id, SBQQ__NetAmount__c, Recalculate__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c order by CreatedDate DESC limit 1];
        
        SBQQ__QuoteLine__c ql = [select id, 
                                    SBQQ__SpecialPriceType__c, 
                                    SBQQ__ListPrice__c, 
                                    SBQQ__CustomerPrice__c,
                                    SBQQ__NetPrice__c,
                                    SBQQ__SpecialPrice__c,
                                    SBQQ__RegularPrice__c,
                                    SBQQ__UnitCost__c,
                                    SBQQ__ProratedListPrice__c
                                 from SBQQ__QuoteLine__c order by CreatedDate DESC limit 1];

        Order testOrderProd = new Order();

        Test.startTest();

        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'AmountThreshold', Value__c = '1000.00'));
        insert settings;

        Order testOrder = new Order();
        testorder.Accountid = testAccount.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAccount.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        insert testOrder;  

        PriceBookEntry testPB = [select id from PriceBookEntry order by CreatedDate DESC limit 1];

        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = 100.00;
        testOrderItem.EndDate = date.today().addMonths(5);
        insert testOrderItem;

        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
        update testOrderItem;
        

        List<Order> listOrder = new list<Order>();
        listOrder.add(testOrder);

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(sc);
        actOrder.activate();

        custAccount.type = 'Customer';
        update custAccount;

        Test.setCurrentPage(Page.JVCO_CompleteQuote);
        ApexPages.StandardController scQuote = new ApexPages.StandardController(testQuote);
        JVCO_CompleteQuoteController cont = new JVCO_CompleteQuoteController(scQuote);
        //cont.updateOppAndQuote();
        dumpRef = cont.returnToQuote();


        test.stoptest();
    }

    @isTest
    static void OrderActivate_invalid()
    {

        Account testAccount = [select id from account order by CreatedDate DESC limit 1];
        system.debug('TestAccount' + testAccount);

        Product2 testProd = [select id from product2 order by CreatedDate DESC limit 1];

        Opportunity testOpp = [select id, StageName, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity order by CreatedDate DESC limit 1];

        SBQQ__Quote__c testQuote = [select id, SBQQ__NetAmount__c, Recalculate__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c order by CreatedDate DESC limit 1];
        
        SBQQ__QuoteLine__c ql = [select id, 
                                    SBQQ__SpecialPriceType__c, 
                                    SBQQ__ListPrice__c, 
                                    SBQQ__CustomerPrice__c,
                                    SBQQ__NetPrice__c,
                                    SBQQ__SpecialPrice__c,
                                    SBQQ__RegularPrice__c,
                                    SBQQ__UnitCost__c,
                                    SBQQ__ProratedListPrice__c
                                 from SBQQ__QuoteLine__c order by CreatedDate DESC limit 1];

        Order testOrderProd = new Order();

        Test.startTest();

        Order testOrder = new Order();
        testorder.Accountid = testAccount.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAccount.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        insert testOrder;  



        PriceBookEntry testPB = [select id from PriceBookEntry order by CreatedDate DESC limit 1];

        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = 100.00;
        testOrderItem.EndDate = date.today().addMonths(5);
        insert testOrderItem;

        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
        update testOrderItem;
        

        List<Order> listOrder = new list<Order>();
        listOrder.add(testOrder);

        

        
        //testOrderProd = [select id from Order order by CreatedDate DESC limit 1];
        //system.assertEquals(null, testOrderProd.id, 'Error in id');

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(sc);
        actOrder.activate();

        //Order selectOrder = [select id, Status from Order where id = :testOrder.id];
        //system.assertEquals('Activated',selectOrder.status,'Error in selectedOrder Status');

        test.stoptest();
    }

	@isTest
	static void OrderActivate_NoneSelected()
	{

		Account testAccount = [select id from account order by CreatedDate DESC limit 1];
        system.debug('TestAccount' + testAccount);

        Product2 testProd = [select id from product2 order by CreatedDate DESC limit 1];

        Opportunity testOpp = [select id, StageName, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity order by CreatedDate DESC limit 1];

        SBQQ__Quote__c testQuote = [select id, SBQQ__NetAmount__c, Recalculate__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c order by CreatedDate DESC limit 1];
        
        SBQQ__QuoteLine__c ql = [select id, 
                                    SBQQ__SpecialPriceType__c, 
                                    SBQQ__ListPrice__c, 
                                    SBQQ__CustomerPrice__c,
                                    SBQQ__NetPrice__c,
                                    SBQQ__SpecialPrice__c,
                                    SBQQ__RegularPrice__c,
                                    SBQQ__UnitCost__c,
                                    SBQQ__ProratedListPrice__c
                                 from SBQQ__QuoteLine__c order by CreatedDate DESC limit 1];

		Order testOrder = new Order();
        testorder.Accountid = testAccount.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAccount.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        insert testOrder;  



        PriceBookEntry testPB = [select id from PriceBookEntry order by CreatedDate DESC limit 1];

        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = 100.00;
        testOrderItem.EndDate = date.today().addMonths(5);

        
        insert testOrderItem;

        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
        update testOrderItem;
        

        List<Order> listOrder = new list<Order>();
        listOrder.add(testOrder);

        Test.startTest();

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        //sc.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(sc);
        actOrder.activate();

		Order selectOrder = [select id, Status from Order where id = :testOrder.id];
		system.assertEquals('Draft',selectOrder.status,'Error in selectedOrder Status');

        test.stoptest();
	}

    @isTest
    static void OrderActivate_ReturnToAccount()
    {

        Account testAccount = [select id from account order by CreatedDate DESC limit 1];
        system.debug('TestAccount' + testAccount);

        Product2 testProd = [select id from product2 order by CreatedDate DESC limit 1];

        Opportunity testOpp = [select id, StageName, SBQQ__Contracted__c, SBQQ__Ordered__c from Opportunity order by CreatedDate DESC limit 1];

        SBQQ__Quote__c testQuote = [select id, SBQQ__NetAmount__c, Recalculate__c, SBQQ__Status__c, SBQQ__Primary__c from SBQQ__Quote__c order by CreatedDate DESC limit 1];
        
        SBQQ__QuoteLine__c ql = [select id, 
                                    SBQQ__SpecialPriceType__c, 
                                    SBQQ__ListPrice__c, 
                                    SBQQ__CustomerPrice__c,
                                    SBQQ__NetPrice__c,
                                    SBQQ__SpecialPrice__c,
                                    SBQQ__RegularPrice__c,
                                    SBQQ__UnitCost__c,
                                    SBQQ__ProratedListPrice__c
                                 from SBQQ__QuoteLine__c order by CreatedDate DESC limit 1];

        Order testOrderProd = new Order();



        Order testOrder = new Order();
        testorder.Accountid = testAccount.id;
        testorder.effectivedate = date.today();
        testOrder.SBQQ__Quote__c = testQuote.id;
        testOrder.blng__BillingAccount__c = testAccount.id;
        testOrder.SBQQ__PaymentTerm__c = 'Net 30';
        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.status = 'Draft';
        testOrder.Pricebook2Id = test.getStandardPriceBookId();
        //testOrder.SBQQ__TotalAmount__c = 100; 
        //insert testOrder;  



        PriceBookEntry testPB = [select id from PriceBookEntry order by CreatedDate DESC limit 1];

        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrder.id;
        testOrderItem.PriceBookEntryId = testPB.Id;
        testOrderItem.SBQQ__QuoteLine__c = ql.id;
        testOrderItem.SBQQ__ChargeType__c = 'One-Time';
        testOrderItem.Quantity = 1.0;
        testOrderItem.UnitPrice = 100.00;
        testOrderItem.EndDate = date.today().addMonths(5);

        
       // insert testOrderItem;

        testOrderItem.blng__OverrideNextBillingDate__c = null;
        testOrderItem.blng__BillThroughDateOverride__c = null;
       // update testOrderItem;
        

        List<Order> listOrder = new list<Order>();
        listOrder.add(testOrder);

        Test.startTest();

        //testOrderProd = [select id from Order order by CreatedDate DESC limit 1];
        //system.assertEquals(null, testOrderProd.id, 'Error in id');

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listOrder);
        sc.setSelected(listOrder);
        JVCO_ActivateOrderLogic actOrder = new JVCO_ActivateOrderLogic(sc);
        actOrder.returnToAccount();

        //Order selectOrder = [select id, Status from Order where id = :testOrder.id];
        //system.assertEquals('Activated',selectOrder.status,'Error in selectedOrder Status');

        test.stoptest();
    }
}