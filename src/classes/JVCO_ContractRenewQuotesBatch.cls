/* ----------------------------------------------------------------------------------------------
    Name: JVCO_ContractRenewQuotesBatch 
    Description: Batch Class for updating Opportunities and Quotes related to an Account

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    13-Mar-2017     0.1         Marlon Ocillos      Intial creation
    28-Mar-2017     0.2         Jules Pablo         Added code segment that unchecks the JVCO_IsComplete__c
    12-Sep-2017     0.3         Eu Rey Cadag        [Update] to use SBQQ.ContractManipulationAPI.ContractGenerator and remove SBQQ__Contracted__c
    10-Oct-2017     0.4         Jules Pablo         Added GREEN-23873 Fix, updated initial query and email section 
    06-Dec-2017     0.5         Csaba Feher         Added GREEN-26653 Fix, updated initial query and quoteList
    23-Jan-2018     0.6         reymark.j.l.arlos   updated to set amount on opportunity from quote GREEN-28620
    12-Apr-2018     0.7         mel.andrei.b.Santos GREEN-31233 - added code to call setPrimaryQuote from JVCO_OpportunityTriggerHandler
    12-Jul-2018     0.8         mel.andrei.b.santos GREEN-31830, added condition preventing the contracting of quotes if # of quote lines w/o SPV > 0 
    10-Aug-2018     0.9         jules.osberg.a.pablo Placed Logic in JVCO_ContractRenewQuotesHelper
    09-Oct-2018     1.0         rhys.j.c.dela.cruz  Updated method calls due to updated helper classes 
----------------------------------------------------------------------------------------------- */
public class JVCO_ContractRenewQuotesBatch implements Database.Batchable<sObject>, Database.Stateful 
{
    public final Account accountRecord;
    public Set<Id> oppIds;
    public Integer contractBatchSize;
    public Integer quoteCount;
    public Integer errorQuoteCount;
    public String batchErrorMessage;
    public Integer toBeProcessed;

    public JVCO_ContractRenewQuotesBatch (Account a, Integer initialQuery)
    {
        accountRecord = a;
        oppIds = new Set<Id>();      
        quoteCount = 0;
        errorQuoteCount = 0;
        batchErrorMessage = '';
        toBeProcessed = initialQuery;

        if (accountRecord.JVCO_Contract_Batch_Size__c != null && accountRecord.JVCO_Contract_Batch_Size__c != 0) 
        {
            contractBatchSize = (Integer)accountRecord.JVCO_Contract_Batch_Size__c;   
        } else {
            contractBatchSize = 1;
        }

        //To be used for Start Job Email
        JVCO_ContractRenewQuotesHelper.sendBeginJobEmail(accountRecord, toBeProcessed, false);
    }
    
    public Database.QueryLocator start (Database.BatchableContext BC) 
    {
        String query = JVCO_ContractRenewQuotesHelper.getQueryString(accountRecord);

        return Database.getQueryLocator(query);
    }
    
    public void execute (Database.BatchableContext BC, List<sObject> scope) 
    {
        if (scope.size() > 0) {
            String errorMessage = JVCO_ContractRenewQuotesHelper.executeContractRenewalQuotes((List<Opportunity>) scope, accountRecord);
            if(errorMessage == ''){
                quoteCount += quoteCount * contractBatchSize;
            }else{
                errorQuoteCount += errorQuoteCount * contractBatchSize;
                batchErrorMessage += '<li>' + errorMessage + '</li>';
            }

            for (Opportunity opportunityRecord : (List<Opportunity>) scope) {
                oppIds.add(opportunityRecord.Id);
            }
        }
    }
    
    public void finish (Database.BatchableContext BC) 
    {
        AsyncApexJob aJob = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id = :BC.getJobId()];
        
        JVCO_ContractRenewQuotesHelper.sendCompletionEmail(accountRecord, aJob.Createdby.Email, quoteCount, errorQuoteCount, batchErrorMessage, toBeProcessed);

        if(aJob.Status != 'Failed') {
            Id batchId = database.executeBatch(new JVCO_OrderQuotesBatch(accountRecord, oppIds, false), contractBatchSize);
            JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_OrderRenewQuotesApexJob__c', batchId,'JVCO_ContractRenewQuotesLastSubmitted__c');
        }
    }
}