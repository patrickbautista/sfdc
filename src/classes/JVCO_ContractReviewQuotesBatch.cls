/* ----------------------------------------------------------------------------------------------
    Name: JVCO_ContractReviewQuotesBatch
    Description: Batch Class for updating Opportunities and Quotes related to an Account

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    03-Mar-2017     0.1         Marlon Ocillos      Intial creation
    28-Mar-2017     0.2         Jules Pablo         Added code segment that unchecks the JVCO_IsComplete__c
    12-Sep-2017     0.3         Eu Rey Cadag        [Update] to use SBQQ.ContractManipulationAPI.ContractGenerator and remove SBQQ__Contracted__c
    29-Sep-2017     0.4         eu.rey.t.Cadag      [Update] Update code to include SavePoint and Rollback, purpose is to catch any error from API ContractGenerator
                                                             and DML on Opportunity, Quote Line Group and Affiliation record
    12-Oct-2017     0.5         eu.rey.t.cadag      [Update] remove code that update Contracted field of Opportunity to true and revise the order of execution, DML -> Call of API method                                                         
    17-Oct-2017     0.6         Jules Pablo         Added GREEN-23493 Fix, changed query to pick amendment and quotes
    05-Dec-2017     0.7         mel.andrei.b.Santos Updated code to consider the error logs in the finish method as fix for GREEN-26379
    09-Dec-2017     0.8         eu.rey.t.Cadag      to eliminate defect-"First error: Duplicate id in list" mentioned in GREEN-26984
    23-Jan-2018     0.8         reymark.j.l.arlos   updated to set amount on opportunity from quote GREEN-28620
    14-Feb-2018     0.9         carmela.santos      Remove affiliation related updates as per GREEN-28616.
    12-Apr-2018     1.0         mel.andrei.b.Santos GREEN-31233 - added code to call setPrimaryQuote from JVCO_OpportunityTriggerHandler
    11-Jul-2018     1.1         mel.andrei.b.santos GREEN-31830, added condition preventing the contracting of quotes if # of quote lines w/o SPV > 0   
    10-Aug-2018     1.2         jules.osberg.a.pablo Placed Logic in JVCO_ContractReviewQuotesHelper
    02-Oct-2018     1.3         rhys.j.c.dela.cruz  Added call for Start Job Email
----------------------------------------------------------------------------------------------- */
public class JVCO_ContractReviewQuotesBatch implements Database.Batchable<sObject>, Database.Stateful 
{
    public final Account accountRecord;
    public Set<Id> oppIds;
    public Integer contractBatchSize;
    // Start  05-Dec-2017   mel.andrei.b.Santos Updated code to consider the error logs in the finish method as fix for GREEN-26379
    public Integer quoteCount;
    public Integer errorQuoteCount;
    public String batchErrorMessage;
    public Integer toBeProcessed;
    // END

    public JVCO_ContractReviewQuotesBatch (Account a, Integer initialQuery) 
    {
        accountRecord = a;
        oppIds = new Set<Id>();
        quoteCount = 0;
        errorQuoteCount = 0;
        batchErrorMessage = '';
        toBeProcessed = initialQuery;
        
        if (accountRecord.JVCO_Contract_Batch_Size__c != null && accountRecord.JVCO_Contract_Batch_Size__c != 0) 
        {
            contractBatchSize = (Integer)accountRecord.JVCO_Contract_Batch_Size__c;   
        } else {
            contractBatchSize = 1;
        }

        //To be used for Start Job Email
        JVCO_ContractReviewQuotesHelper.sendBeginJobEmail(accountRecord, toBeProcessed, false);
    }
    
    public Database.QueryLocator start (Database.BatchableContext BC) 
    {     
        String query = JVCO_ContractReviewQuotesHelper.getQueryString(accountRecord);
        return Database.getQueryLocator(query);
    }
    
    public void execute (Database.BatchableContext BC, List<sObject> scope) 
    {
        if (scope.size() > 0) {
            String errorMessage = JVCO_ContractReviewQuotesHelper.executeContractAmendmentQuotes((List<Opportunity>) scope);
            if(errorMessage == ''){
                quoteCount += quoteCount * contractBatchSize;
            }else{
                errorQuoteCount += errorQuoteCount * contractBatchSize;
                batchErrorMessage += '<li>' + errorMessage + '</li>';
            }

            for (Opportunity opportunityRecord : (List<Opportunity>) scope) {
                oppIds.add(opportunityRecord.Id);
            }
        }
    }
    
    public void finish (Database.BatchableContext BC) {
        AsyncApexJob aJob = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Id, ExtendedStatus from AsyncApexJob where Id = :BC.getJobId()];

        JVCO_ContractReviewQuotesHelper.sendCompletionEmail(accountRecord, aJob.Createdby.Id, quoteCount, errorQuoteCount, batchErrorMessage, toBeProcessed);

        if(aJob.Status != 'Failed') {
            Id batchId = database.executeBatch(new JVCO_OrderQuotesBatch(accountRecord, oppIds, true), contractBatchSize);
            JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_OrderReviewQuotesApexJob__c', batchId,'JVCO_OrderReviewQuotesLastSubmitted__c');
        }
    }
}