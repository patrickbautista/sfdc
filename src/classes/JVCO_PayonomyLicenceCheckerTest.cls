@isTest
public class JVCO_PayonomyLicenceCheckerTest
{
	@isTest
   	static void testExecute()
    {
        JVCO_PayonomyLicenceCheckerScheduler.callExecuteMomentarily(System.today().format());
        JVCO_PayonomyLicenceCheckerScheduler plcs = new JVCO_PayonomyLicenceCheckerScheduler();
        String sch = '5 0 0 * * ?'; 
        System.schedule('JVCO_PayonomyLicenceCheckerTest', sch, plcs);
        JVCO_PayonomyLicenceCheckerScheduler.sagePayLicenceChecker();
        JVCO_PayonomyLicenceCheckerScheduler.directDebitLicenceChecker();
    }
}