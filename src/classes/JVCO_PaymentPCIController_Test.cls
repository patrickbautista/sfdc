@IsTest
private class JVCO_PaymentPCIController_Test{

    @testSetup static void createTestData() 
    {
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
            
            //Update NVM Id 
            User u=[SELECT id,NVMContactWorld__NVM_Agent_Id__c FROM User WHERE ID=:UserInfo.getUserId()];
            u.NVMContactWorld__NVM_Agent_Id__c = '1010';
            update u; 
                       
        }
        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        //JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        Test.startTest();
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        List<Order> orderList = new List<Order>();
        orderList.add(o);
        
        Set<Id> orderGroupIdSet = new Set<Id>();  
        orderGroupIdSet.add(orderGroup.Id);

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_TestClassHelper.setGeneralSettingsCS();
        insert new GLA_for_Cash_Transfer__c(name = '10100 - Bank Cash'); 
        //JVCO_BackgroundMatchingLogic.stopMatching = true;
        JVCO_FFUtil.stopCashTransferLogic = true;
        
        Database.executeBatch(new JVCO_OrderMultipleBillNowLogicBatch(orderGroupIdSet));
        Test.stopTest();
               
        c2g__codaInvoice__c invoice = [SELECT Id, Name,c2g__PaymentStatus__c,c2g__OutstandingValue__c,c2g__InvoiceTotal__c,c2g__NetTotal__c,
                                       JVCO_DocGen_Number_of_Invoice_Lines__c
                                       FROM c2g__codaInvoice__c LIMIT 1];
        
    }


    private class Mock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
        
            if (req.getEndpoint().endsWith('token')) {
                HTTPResponse res = new HTTPResponse();
                String tokenRes = '{"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6N_PCI","expires_in":7200,"token_type":"Bearer"}';
                res.setBody(tokenRes);
                res.setStatusCode(200);
                return res;
                
            } else if (req.getEndpoint().endsWith('session')) {
                HTTPResponse res = new HTTPResponse();
                String resPCI = '{'+
                '    \"sessionGuid\": \"b4ee5035-f43e-41f1-96a0-628881f6efde_GUID\",'+            //Do not change the suffix as this will be used for reference as GUID
                '    \"agentAccessToken\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJodHRw\",'+
                '    \"agentRefreshToken\": \"1a6a80c048824d95883d93e25ee122d8\",'+
                '    \"iframeUrl\": \"https://euwest1.pcipalstaging.cloud/session/258/view/63c22c53-387f-467c-a1cd-a63ff2c97448/framed\",'+
                '    \"providerSessionGuid\": \"63c22c53-387f-467c-a1cd-a63ff2c97448\"'+
                '}';
                res.setBody(resPCI);
                res.setStatusCode(200);
                return res;    
                        
            } else if (req.getEndpoint().endsWith('_GUID')) {
               HTTPResponse res = new HTTPResponse();
               String json = '{'+
                '    \"lastUpdatedOn\": \"2020-04-17T16:14:57.865143Z\",'+
                '    \"callGuid\": \"017188e8-38f5-f014-6004-1cb67af5a629\",'+
                '    \"createdOn\": \"2020-04-17T16:11:29.549945Z\", '+
                '    \"sessionGuid\": \"b4ee5035-f43e-41f1-96a0-628881f6efde\",'+
                '    \"paymentResultData\": {'+
                '        \"OutputSecurityKey\": \"NJYEFYIAD5\\r\\n\",'+                
                '        \"OutputExpiryDate\": \"1229\\r\\n\",'+
                '        \"OutputBankAuthCode\": \"999778\",'+
                '        \"VPSProtocol\": \"3.00\",'+
                '        \"GiftAidPayment\": \"1\",'+
                '        \"CustomerEmail\": \"missfake@email.com\", '+
                '        \"OutputTxAuthNo\": \"5376191\\r\\n\", '+       
                '        \"OutputStatusDetails\": \"0000 : The Authorisation was Successful.\\r\\n\", '+
                '        \"Amount\": \"156\", '+
                '        \"Currency\": \"GBP\",'+
                '        \"Vendor\": \"PPLPRS\", '+
                '        \"VendorTxCode\": \"63c22c53-387f-467c-a1cd-a63ff2c97448\", '+
                '        \"Description\": \"Payment\", '+
                '        \"BillingSurname\": \"Fake\",  '+
                '        \"BillingFirstnames\": \"Flick\", '+
                '        \"BillingAddress1\": \"Fake House\",'+
                '        \"BillingAddress2\": \"Fake Street\", '+
                '        \"BillingCity\": \"Fake City\",'+
                '        \"BillingPostCode\": \"L1 8JQ\", '+
                '        \"OutputVPSTxId\": \"{504B72A9-22C8-D151-E09A-32612243D641}\\r\\n\", '+
                '        \"BillingCountry\": \"GB\", '+
                '        \"DeliveryFirstnames\": \"Card\",'+
                '        \"DeliveryAddress1\": \"88\",'+
                '        \"DeliveryCity\": \"Ipswich\",'+
                '        \"DeliveryPostCode\": \"412\",'+
                '        \"DeliveryCountry\": \"GB\", '+
                '        \"CardHolder\": \"Miss Fake\", '+
                '        \"CardType\": \"MC\", '+
                '        \"TxType\": \"PAYMENT\",'+
                '        \"CreateToken\": \"1\",'+
                '        \"OutputVpsProtocol\": \"3.00\\r\\n\",'+
                '        \"OutputStatus\": \"OK\\r\\n\", '+
                '        \"DeliverySurname\": \"Test\"'+
                '    },'+
                '    \"providerSessionGuid\": \"63c22c53-387f-467c-a1cd-a63ff2c97448\",'+
                '    \"sessionState\": \"Finished\"'+
                '}';
                res.setBody(json);
                res.setStatusCode(200);
                return res;
                
            } else {
                System.assert(false, 'unexpected endpoint ' + req.getEndpoint());
                return null;
            }
        }
    }

    @IsTest
    static void nvmTest() {
        Test.setMock(HttpCalloutMock.class, new Mock());
        RecordType licenseAcctRecType = [SELECT id from Recordtype WHERE Name='Licence Account' AND SobjectType='Account' LIMIT 1];

        //Custom Settings
        JVCO_NVM_PCI_Settings__c config = new JVCO_NVM_PCI_Settings__c();
        config.JVCO_NVM_Endpoint__c ='https://emea.newvoicemedia.com/token';
        config.JVCO_PCI_Endpoint__c ='https://emea.globalpci.com/session';
        config.JVCO_Client_Id__c = '123456789101112131415';
        config.JVCO_Client_Secret__c = 't1h1i1s1a1s1e1c1r1e1t1'; 
        config.JVCO_AccountConfigurationId__c = '0';
        config.JVCO_VendorTxCode__c = 'PRS2020';
        insert config; 
            
        Test.StartTest(); 
            
            Account acc = [SELECT Id from Account WHERE RecordTypeId=:licenseAcctRecType.Id  LIMIT 1];
            Contact con = [SELECT Id from Contact LIMIT 1];
            c2g__codaInvoice__c sinv = [SELECT Id from c2g__codaInvoice__c LIMIT 1];
            
            ApexPages.currentPage().getParameters().put('accountid',acc.id );
            ApexPages.currentPage().getParameters().put('contactid',con.Id );
            ApexPages.currentPage().getParameters().put('amount', '123.45');
            ApexPages.currentPage().getParameters().put('selectedsinv',sinv.id ) ;
            
            JVCO_PaymentPCIController testPCI = new JVCO_PaymentPCIController();
            testPCI.authenticateNVM();
            testPCI.finishTransaction();
            testPCI.cancel();
            
        Test.StopTest();
    }

    
}