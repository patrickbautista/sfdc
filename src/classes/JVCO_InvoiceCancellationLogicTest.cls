@isTest
private class JVCO_InvoiceCancellationLogicTest {
    @testSetup static void createTestData() 
    {   
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        //JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim3();

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        Test.startTest();
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        Test.stopTest();
        
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        Set<Id> orderGroupIdSet = new Set<Id>();
        orderGroupIdSet.add(orderGroup.id);
        
        JVCO_TestClassHelper.setGeneralSettingsCS();
        
        List<JVCO_TransitionManualReviewNotifSettings__c> settings = new List<JVCO_TransitionManualReviewNotifSettings__c>();
        settings.add(new JVCO_TransitionManualReviewNotifSettings__c(Name = 'Limit', Limit__c = '3000'));
        insert settings;
        
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        JVCO_FFUtil.stopCodaTransasctionHandlerAfterUpdate = true;
        insert new GLA_for_Cash_Transfer__c(name = '10100 - Bank Cash');
        
        Database.executeBatch(new JVCO_OrderMultipleBillNowLogicBatch(orderGroupIdSet));
    }

    @isTest
    static void testCancellationOfSingleOrder()
    {   
        c2g__codaInvoice__c sInv = [SELECT Id, c2g__Account__c,c2g__InvoiceCurrency__c, c2g__NetTotal__c, 
                                    c2g__Period__c FROM c2g__codaInvoice__c LIMIT 1];
        Product2 p = [SELECT Id FROM Product2 order by CreatedDate DESC limit 1];
        c2g__codaTaxCode__c taxCode = [SELECT Id FROM c2g__codaTaxCode__c order by CreatedDate DESC limit 1];
        c2g__codaDimension1__c dim1 = [SELECT Id FROM c2g__codaDimension1__c order by CreatedDate DESC limit 1];
        c2g__codaDimension2__c dim2 = [SELECT Id FROM c2g__codaDimension2__c order by CreatedDate DESC limit 1];
        c2g__codaDimension3__c dim3 = [SELECT Id FROM c2g__codaDimension3__c order by CreatedDate DESC limit 1];

        sInv.JVCO_From_Convert_to_Credit__c = true;
        update sInv;

        List<c2g__codaCreditNote__c> cNoteList = new List<c2g__codaCreditNote__c>();
        c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
        cNote.c2g__Invoice__c = sInv.Id;
        cNote.c2g__Account__c = sInv.c2g__Account__c;
        cNote.c2g__DueDate__c = Date.today();
        cNote.c2g__CreditNoteCurrency__c = sInv.c2g__InvoiceCurrency__c;
        cNote.c2g__Period__c = sInv.c2g__Period__c;
        cNote.c2g__CreditNoteDate__c = Date.today();
        cNote.JVCO_Credit_Reason__c = 'Invoice amendment';
        insert cNote;
        cNoteList.add(cNote);

        c2g__codaCreditNoteLineItem__c cNoteLine = new c2g__codaCreditNoteLineItem__c();
        cNoteLine.c2g__CreditNote__c = cNote.Id;
        cNoteLine.c2g__UnitPrice__c = sInv.c2g__NetTotal__c;
        cNoteLine.c2g__Dimension1__c = dim1.Id;
        cNoteLine.c2g__Dimension2__c = dim2.Id;
        cNoteLine.c2g__Dimension3__c = dim3.Id;
        cNoteLine.c2g__Quantity__c = 1;
        cNoteLine.c2g__TaxCode1__c = taxCode.Id;
        cNoteLine.c2g__DeriveTaxRateFromCode__c = TRUE;
        cNoteLine.c2g__CalculateTaxValueFromRate__c = TRUE;
        cNoteLine.c2g__DeriveLineNumber__c = TRUE;
        cNoteLine.c2g__DeriveUnitPriceFromProduct__c = FALSE;
        cNoteLine.c2g__Product__c = p.Id;
        insert cNoteLine;
        
        Test.startTest();
        PageReference pageRef = Page.JVCO_PostAndMatch;
        Test.setCurrentPage(pageRef);
        apexPages.Currentpage().getParameters().put('Id', cNote.id);
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(cNoteList);
        sc.setSelected(cNoteList);
        JVCO_PostAndMatchController postAndMatchController = new JVCO_PostAndMatchController(sc);
        postAndMatchController.init();
        postAndMatchController.returnToCreditNote();
        Test.stopTest();
    }

    @isTest
    static void testCancellationWithSurcharge()
    {                           
        c2g__codaInvoice__c sInv = [SELECT Id, c2g__Account__c,c2g__InvoiceCurrency__c, c2g__NetTotal__c, 
                                    c2g__Period__c FROM c2g__codaInvoice__c LIMIT 1];
        Product2 p = [SELECT Id FROM Product2 order by CreatedDate DESC limit 1];
        c2g__codaTaxCode__c taxCode = [SELECT Id FROM c2g__codaTaxCode__c order by CreatedDate DESC limit 1];
        c2g__codaDimension1__c dim1 = [SELECT Id FROM c2g__codaDimension1__c order by CreatedDate DESC limit 1];
        c2g__codaDimension2__c dim2 = [SELECT Id FROM c2g__codaDimension2__c order by CreatedDate DESC limit 1];
        c2g__codaDimension3__c dim3 = [SELECT Id FROM c2g__codaDimension3__c order by CreatedDate DESC limit 1];

        c2g__codaInvoice__c surSInv = new c2g__codaInvoice__c();
        surSInv = JVCO_TestClassHelper.getSalesInvoice(sInv.c2g__Account__c);
        surSInv.JVCO_Original_Invoice__c = sInv.Id;
        surSInv.JVCO_Invoice_Type__c = 'Surcharge';
        insert surSInv;

        c2g__codaInvoiceLineItem__c sInvLine = JVCO_TestClassHelper.getSalesInvoiceLine(surSInv.Id, dim1.Id, dim3.Id, 
                                                                                        taxCode.Id, dim2.Id, p.Id);
        sInvLine.c2g__Quantity__c = 1;
        sInvLine.c2g__UnitPrice__c = sInv.c2g__NetTotal__c / 2;
        insert sInvLine;
        
        c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
        cNote.c2g__Invoice__c = sInv.Id;
        cNote.c2g__Account__c = sInv.c2g__Account__c;
        cNote.c2g__DueDate__c = Date.today();
        cNote.c2g__CreditNoteCurrency__c = sInv.c2g__InvoiceCurrency__c;
        cNote.c2g__Period__c = sInv.c2g__Period__c;
        cNote.c2g__CreditNoteDate__c = Date.today();
        cNote.JVCO_Credit_Reason__c = 'Invoice amendment';
        insert cNote;

        c2g__codaCreditNoteLineItem__c cNoteLine = new c2g__codaCreditNoteLineItem__c();
        cNoteLine.c2g__CreditNote__c = cNote.Id;
        cNoteLine.c2g__UnitPrice__c = sInv.c2g__NetTotal__c;
        cNoteLine.c2g__Dimension1__c = dim1.Id;
        cNoteLine.c2g__Dimension2__c = dim2.Id;
        cNoteLine.c2g__Dimension3__c = dim3.Id;
        cNoteLine.c2g__Quantity__c = 1;
        cNoteLine.c2g__TaxCode1__c = taxCode.Id;    
        cNoteLine.c2g__DeriveTaxRateFromCode__c = TRUE;
        cNoteLine.c2g__CalculateTaxValueFromRate__c = TRUE;
        cNoteLine.c2g__DeriveLineNumber__c = TRUE;
        cNoteLine.c2g__DeriveUnitPriceFromProduct__c = FALSE;
        cNoteLine.c2g__Product__c = p.Id;
        insert cNoteLine;

        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        List<c2g.CODAAPICommon.Reference> refList1 = new List<c2g.CODAAPICommon.Reference>();
        c2g.CODAAPICommon.Reference reference1 = new c2g.CODAAPICommon.Reference();
        reference1.Id = surSInv.Id;
        refList1.add(reference1);
        
        Test.startTest();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        c2g.CODAAPISalesInvoice_10_0.BulkPostInvoice(context, refList1);
        List<c2g.CODAAPICommon.Reference> refList2 = new List<c2g.CODAAPICommon.Reference>();
        c2g.CODAAPICommon.Reference reference2 = new c2g.CODAAPICommon.Reference();
        reference2.Id = cNote.Id;
        refList2.add(reference2);
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        c2g.CODAAPISalesCreditNote_10_0.BulkPostCreditNote(context, refList2);

        cNote.JVCO_From_Post_And_Match__c = true;
        update cNote;
        Test.stopTest();

    }
    
    @isTest
    static void testCancellationOfSingleOrderWithNewCreditReason()
    {  
        c2g__codaInvoice__c sInv = [SELECT Id, c2g__Account__c, 
                                    c2g__InvoiceCurrency__c,
                                    JVCO_From_Convert_to_Credit__c,
                                    c2g__Period__c, c2g__NetTotal__c        
                                    FROM c2g__codaInvoice__c order by CreatedDate DESC limit 1];
        sInv.JVCO_From_Convert_to_Credit__c = true;
        update sInv;                            
        Product2 p = [SELECT Id FROM Product2 order by CreatedDate DESC limit 1];
        c2g__codaTaxCode__c taxCode = [SELECT Id FROM c2g__codaTaxCode__c order by CreatedDate DESC limit 1];
        c2g__codaDimension1__c dim1 = [SELECT Id FROM c2g__codaDimension1__c order by CreatedDate DESC limit 1];
        c2g__codaDimension2__c dim2 = [SELECT Id FROM c2g__codaDimension2__c order by CreatedDate DESC limit 1];
        c2g__codaDimension3__c dim3 = [SELECT Id FROM c2g__codaDimension3__c order by CreatedDate DESC limit 1];

        Test.startTest();
        c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
        cNote.c2g__Invoice__c = sInv.Id;
        cNote.c2g__Account__c = sInv.c2g__Account__c;
        cNote.c2g__DueDate__c = Date.today();
        cNote.c2g__CreditNoteCurrency__c = sInv.c2g__InvoiceCurrency__c;
        cNote.c2g__Period__c = sInv.c2g__Period__c;
        cNote.c2g__CreditNoteDate__c = Date.today();
        cNote.JVCO_Credit_Reason__c = 'Invoice amendment';
        insert cNote;
        c2g__codaCreditNoteLineItem__c cNoteLine = new c2g__codaCreditNoteLineItem__c();
        cNoteLine.c2g__CreditNote__c = cNote.Id;
        cNoteLine.c2g__UnitPrice__c = sInv.c2g__NetTotal__c;
        cNoteLine.c2g__Dimension1__c = dim1.Id;
        cNoteLine.c2g__Dimension2__c = dim2.Id;
        cNoteLine.c2g__Dimension3__c = dim3.Id;
        cNoteLine.c2g__Quantity__c = 1;
        cNoteLine.c2g__TaxCode1__c = taxCode.Id;
        cNoteLine.c2g__DeriveTaxRateFromCode__c = TRUE;
        cNoteLine.c2g__CalculateTaxValueFromRate__c = TRUE;
        cNoteLine.c2g__DeriveLineNumber__c = TRUE;
        cNoteLine.c2g__DeriveUnitPriceFromProduct__c = FALSE;
        cNoteLine.c2g__Product__c = p.Id;
        insert cNoteLine;
        
        Test.stopTest();
        c2g__codaCreditNote__c cNoteRes = [SELECT Id, c2g__NetTotal__c, JVCO_Credit_Reason__c FROM c2g__codaCreditNote__c order by CreatedDate DESC limit 1];
        cNoteRes.JVCO_Credit_Reason__c = 'Licence cancelled';
        update cNoteRes;
        System.assertEquals(cNoteRes.c2g__NetTotal__c, sInv.c2g__NetTotal__c);
    }
    @isTest
    static void testErrorLogsForSurchargeCancellationBatch(){
        JVCO_SurchargeInvoiceCancellationBatch sicb = new JVCO_SurchargeInvoiceCancellationBatch(new Map<Id, c2g__codaCreditNote__c>());
        Id testId = [SELECT Id FROM Order LIMIT 1].Id;
        ffps_custRem__Custom_Log__c errorLog = sicb.createErrorLog(testId, 'test error', 'test error', 'test error');
    }
}