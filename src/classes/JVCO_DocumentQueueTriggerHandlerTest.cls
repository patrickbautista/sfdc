@isTest
private class JVCO_DocumentQueueTriggerHandlerTest{
    @testSetup static void setTestData(){
        JVCO_TestClassObjectBuilder.createBillingConfig();

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;

        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
            listQueue.add(q2);
            queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
            listQueue.add(q3);
            queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
            listQueue.add(q4);
            queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
            listQueue.add(q5);
            queuesobject q6 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankAccount__c'); 
            listQueue.add(q6);
            queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q7);
            queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatement__c'); 
            listQueue.add(q8);          
            queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntry__c'); 
            listQueue.add(q9);  
            queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='PAYBASE2__Payment__c'); 
            listQueue.add(q10);
            queuesobject q11 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransaction__c'); 
            listQueue.add(q11);
            //queuesobject q12 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransactionLineItem__c'); 
            //listQueue.add(q12);
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }
        
         //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }
        
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        insert dt;
        
        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc= JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert testGeneralLedgerAcc;
      
        c2g__codaCompany__c testCompany= JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert testCompany;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(testGeneralLedgerAcc.Id);
        insert taxCode;
        
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;

        //Create Account               
        Account testAccCust = JVCO_TestClassHelper.setCustAcc(testGeneralLedgerAcc.Id, dim1.Id);
        insert testAccCust ;
        Contact c = JVCO_TestClassHelper.setContact(testAccCust.id);
        insert c;
        Account testLicAcc = JVCO_TestClassHelper.setLicAcc(testAccCust.Id, taxCode.Id, c.Id,testGeneralLedgerAcc.Id);
        testLicAcc.JVCO_Credit_Status__c = 'Debt - 1st stage';
        insert testLicAcc;
        
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(testCompany.Id, testGroup.Id);
        insert accCurrency;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(testCompany.Id, testGroup.Id);
        insert yr;
        
        insert JVCO_TestClassHelper.setPeriod(testCompany.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(testCompany.Id);


        Product2 p = JVCO_TestClassHelper.setProduct(testGeneralLedgerAcc.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(testLicAcc.Id);
        insert opp;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(testLicAcc.Id, opp.Id);

        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;

        Contract contr= new Contract();
        contr.AccountId = testLicAcc.Id;
        contr.Status = 'Draft';
        contr.StartDate = Date.today();
        contr.ContractTerm = 12;
        contr.JVCO_Licence_Period_End_Date__c = Date.today().addDays(28);
        insert contr;
        
        
        Test.startTest();
        Order o = JVCO_TestClassHelper.setOrder(testLicAcc.Id, q.Id);
        insert o;
        
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        OrderItem oi1 = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi1;

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();


        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(testLicAcc.Id, o.Id);
        insert bInv;
        
        //Create Sales Invoice
        c2g__codaInvoice__c testInvoice1 = new c2g__codaInvoice__c();
        testInvoice1.c2g__InvoiceCurrency__c = accCurrency.Id;
        testInvoice1.c2g__InvoiceDate__c = date.today().addDays(-7);
        testInvoice1.c2g__DueDate__c = date.today().addDays(-7);
        testInvoice1.c2g__Account__c = testLicAcc.Id;
        testInvoice1.JVCO_Customer_Type__c = 'New Business';
        testInvoice1.c2g__OwnerCompany__c = testCompany.id;
        testInvoice1.JVCO_Invoice_Legacy_Number__c = 'SIN00001';
        testInvoice1.JVCO_Original_Invoice__c = null;
        insert testInvoice1;
        
        c2g__codaInvoiceLineItem__c testInvoiceItem = new c2g__codaInvoiceLineItem__c();
        testInvoiceItem.c2g__Product__c = p.id;
        testInvoiceItem.c2g__OwnerCompany__c = testCompany.id;
        testInvoiceItem.c2g__Invoice__c = testInvoice1.id;
        testInvoiceItem.c2g__Quantity__c = 1.0;
        testInvoiceItem.c2g__UnitPrice__c = 200.0;
        insert testInvoiceItem;
        
        c2g.CODAAPICommon_10_0.Context contextInvoice = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference invoiceRef = c2g.CODAAPICommon.getRef(testInvoice1.Id, null); 
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(contextInvoice, invoiceRef);
        
        //Create Test Conga Records
        APXTConga4__Conga_Solution__c testCongaSol = new APXTConga4__Conga_Solution__c();
        testCongaSol.name = 'Manual Review Notification Transition Letter';
        testCongaSol.APXTConga4__Master_Object_Type_Validator__c = 'JVCO_Document_Queue__c';
        testCongaSol.APXTConga4__Master_Object_Type__c = 'JVCO_Document_Queue__c';
        insert testCongaSol;

        APXTConga4__Conga_Solution__c testCongaSolGeneric = new APXTConga4__Conga_Solution__c();
        testCongaSolGeneric.name = 'Generic Template Letter';
        testCongaSolGeneric.APXTConga4__Master_Object_Type_Validator__c = 'JVCO_Document_Queue__c';
        testCongaSolGeneric.APXTConga4__Master_Object_Type__c = 'JVCO_Document_Queue__c';
        insert testCongaSolGeneric;
        
        
        APXTConga4__Conga_Solution__c testCongaSol2 = new APXTConga4__Conga_Solution__c();
        testCongaSol2.name = 'Review Application From Holiday Center Letter';
        testCongaSol2.APXTConga4__Master_Object_Type_Validator__c = 'JVCO_Document_Queue__c';
        testCongaSol2.APXTConga4__Master_Object_Type__c = 'JVCO_Document_Queue__c';
        insert testCongaSol2;

        APXTConga4__Conga_Template__c congaTemplate = new APXTConga4__Conga_Template__c();
        congaTemplate.APXTConga4__Name__c = 'Generic Template';
        congaTemplate.APXTConga4__Template_Type__c = 'Document';
        congaTemplate.JVCO_TemplateNameShort__c = 'Generic';
        congaTemplate.JVCO_PageScenario__c = 'Letterhead';
        database.insert(congaTemplate);

        APXTConga4__Conga_Solution_Template__c testCongaTemplate = new APXTConga4__Conga_Solution_Template__c();
        testCongaTemplate.APXTConga4__Conga_Solution__c = testCongaSol.id;
        testCongaTemplate.APXTConga4__Sort_Order__c = 2.0;
        testCongaTemplate.APXTConga4__Conga_Template__c = congaTemplate.Id;
        insert testCongaTemplate;

        APXTConga4__Conga_Solution_Template__c testCongaTemplateGeneric = new APXTConga4__Conga_Solution_Template__c();
        testCongaTemplateGeneric.APXTConga4__Conga_Solution__c = testCongaSolGeneric.id;
        testCongaTemplateGeneric.APXTConga4__Sort_Order__c = 2.0;
        testCongaTemplateGeneric.APXTConga4__Conga_Template__c = congaTemplate.Id;
        insert testCongaTemplateGeneric;

        APXTConga4__Conga_Solution_Email_Template__c testCongaEmailTemplate = new APXTConga4__Conga_Solution_Email_Template__c();
        testCongaEmailTemplate.APXTConga4__Conga_Solution__c  = testCongaSol.id;
        insert testCongaEmailTemplate;

        APXTConga4__Conga_Solution_Email_Template__c testCongaEmailTemplateGeneric = new APXTConga4__Conga_Solution_Email_Template__c();
        testCongaEmailTemplateGeneric.APXTConga4__Conga_Solution__c  = testCongaSolGeneric.id;
        insert testCongaEmailTemplateGeneric;

        APXTConga4__Conga_Solution_Parameter__c testCongaParameter = new APXTConga4__Conga_Solution_Parameter__c();
        testCongaParameter.APXTConga4__Conga_Solution__c = testCongaSol.id;
        insert testCongaParameter;

        APXTConga4__Conga_Solution_Parameter__c testCongaParameterGeneric = new APXTConga4__Conga_Solution_Parameter__c();
        testCongaParameterGeneric.APXTConga4__Conga_Solution__c = testCongaSolGeneric.id;
        insert testCongaParameterGeneric;

        APXTConga4__Conga_Solution_Query__c testCongaSolQuery = new APXTConga4__Conga_Solution_Query__c();
        testCongaSolQuery.APXTConga4__pv0__c = '{!JVCO_Document_Queue__c.Id}';
        testCongaSolQuery.APXTConga4__Conga_Solution__c = testCongaSol.id;
        testCongaSolQuery.APXTConga4__Conga_Query__c = testCongaSolQuery.id;
        insert testCongaSolQuery;

        APXTConga4__Conga_Solution_Query__c testCongaSolQueryGeneric = new APXTConga4__Conga_Solution_Query__c();
        testCongaSolQueryGeneric.APXTConga4__pv0__c = '{!JVCO_Document_Queue__c.Id}';
        testCongaSolQueryGeneric.APXTConga4__Conga_Solution__c = testCongaSolGeneric.id;
        testCongaSolQueryGeneric.APXTConga4__Conga_Query__c = testCongaSolQueryGeneric.id;
        insert testCongaSolQueryGeneric;

        JVCO_DynamicStatement__c testDyStatement = new JVCO_DynamicStatement__c();
        testDyStatement.JVCO_Scenario__c = 'Manual Review Notification Transition';
        testDyStatement.JVCO_Transition_renewal_Scenario__c = 'Single Society - PRS';
        testDyStatement.JVCO_PPL_Gap__c = 'True';
        insert testDyStatement;
        Test.stopTest();
    }

    @isTest
    static void itShould()
    {
        Test.startTest();

        //record type Account
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();

        //record type JVCO_Document_Queue__c
        Id download_DocRT = Schema.SObjectType.JVCO_Document_Queue__c.getRecordTypeInfosByName().get('Download').getRecordTypeId();

        Account testLicAccount = [select id, JVCO_Renewal_Scenario__c from Account where RecordTypeId = :licenceRT limit 1];
        testLicAccount.JVCO_Renewal_Scenario__c = 'Single Society - PRS';
        update testLicAccount;
        Account testCusAccount = [select id from Account where RecordTypeId = :customerRT limit 1];
        Contact testContact = [SELECT ID FROM Contact limit 1];
        c2g__codaAccountingCurrency__c accCurrency = [select id from c2g__codaAccountingCurrency__c limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];
        Product2 p = [select id from Product2 limit 1];
        
        //Create Sales Invoice
        c2g__codaInvoice__c testInvoice = new c2g__codaInvoice__c();
        testInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testInvoice.c2g__InvoiceDate__c = date.today().addDays(-7);
        testInvoice.c2g__DueDate__c = date.today().addMonths(7);
        testInvoice.c2g__Account__c = testLicAccount.Id;
        testInvoice.JVCO_Customer_Type__c = 'New Business';
        testInvoice.c2g__OwnerCompany__c = testCompany.id;
        testInvoice.JVCO_Generate_Payment_Schedule__c = false;
        insert testInvoice;
        
        c2g__codaInvoiceLineItem__c testInvoiceItem = new c2g__codaInvoiceLineItem__c();
        testInvoiceItem.c2g__Product__c = p.id;
        testInvoiceItem.c2g__OwnerCompany__c = testCompany.id;
        testInvoiceItem.c2g__Invoice__c = testInvoice.id;
        testInvoiceItem.c2g__Quantity__c = 1.0;
        testInvoiceItem.c2g__UnitPrice__c = 200.0;
        insert testInvoiceItem;
        
        c2g.CODAAPICommon_10_0.Context contextInvoice = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference invoiceRef = c2g.CODAAPICommon.getRef(testInvoice.Id, null); 
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(contextInvoice, invoiceRef);
        
        JVCO_Document_Queue__c testDocument = new JVCO_Document_Queue__c();
        testDocument.JVCO_Recipient__c = testContact.id;
        testDocument.JVCO_Related_Account__c = testLicAccount.id;
        testDocument.JVCO_Related_Customer_Account__c = testCusAccount.id;
        testDocument.JVCO_Scenario__c = 'Manual Review Notification Transition';
        testDocument.JVCO_Transmission__c = 'Letter';
        insert TestDocument;
        test.stopTest();
    }
    @isTest
    static void itShouldEmail()
    {
        Test.startTest();

        //record type Account
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();

        //record type JVCO_Document_Queue__c
        Id download_DocRT = Schema.SObjectType.JVCO_Document_Queue__c.getRecordTypeInfosByName().get('Download').getRecordTypeId();

        Account testLicAccount = [select id, JVCO_Renewal_Scenario__c from Account where RecordTypeId = :licenceRT limit 1];
        testLicAccount.JVCO_Renewal_Scenario__c = 'Single Society - PRS';
        update testLicAccount;
        Account testCusAccount = [select id from Account where RecordTypeId = :customerRT limit 1];
        Contact testContact = [SELECT ID FROM Contact limit 1];
        c2g__codaAccountingCurrency__c accCurrency = [select id from c2g__codaAccountingCurrency__c limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];
        Product2 p = [select id from Product2 limit 1];
        
        //Create Sales Invoice
        c2g__codaInvoice__c testInvoice = new c2g__codaInvoice__c();
        testInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testInvoice.c2g__InvoiceDate__c = date.today().addDays(-7);
        testInvoice.c2g__DueDate__c = date.today().addMonths(7);
        testInvoice.c2g__Account__c = testLicAccount.Id;
        testInvoice.JVCO_Customer_Type__c = 'New Business';
        testInvoice.c2g__OwnerCompany__c = testCompany.id;
        testInvoice.JVCO_Generate_Payment_Schedule__c = false;
        insert testInvoice;
        
        c2g__codaInvoiceLineItem__c testInvoiceItem = new c2g__codaInvoiceLineItem__c();
        testInvoiceItem.c2g__Product__c = p.id;
        testInvoiceItem.c2g__OwnerCompany__c = testCompany.id;
        testInvoiceItem.c2g__Invoice__c = testInvoice.id;
        testInvoiceItem.c2g__Quantity__c = 1.0;
        testInvoiceItem.c2g__UnitPrice__c = 200.0;
        insert testInvoiceItem;
        
        c2g.CODAAPICommon_10_0.Context contextInvoice = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference invoiceRef = c2g.CODAAPICommon.getRef(testInvoice.Id, null); 
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(contextInvoice, invoiceRef);
        
        JVCO_Document_Queue__c testDocument = new JVCO_Document_Queue__c();
        testDocument.JVCO_Recipient__c = testContact.id;
        testDocument.JVCO_Related_Account__c = testLicAccount.id;
        testDocument.JVCO_Related_Customer_Account__c = testCusAccount.id;
        testDocument.JVCO_Scenario__c = 'Manual Review Notification Transition';
        testDocument.JVCO_Transmission__c = 'Email';
        insert TestDocument;
        test.stopTest();
    }
    @isTest
    static void itShouldGeneric()
    {
        Test.startTest();

        //record type Account
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();

        //record type JVCO_Document_Queue__c
        Id download_DocRT = Schema.SObjectType.JVCO_Document_Queue__c.getRecordTypeInfosByName().get('Download').getRecordTypeId();

        Account testLicAccount = [select id from Account where RecordTypeId = :licenceRT limit 1];
        Account testCusAccount = [select id from Account where RecordTypeId = :customerRT limit 1];
        Contact testContact = [SELECT ID FROM Contact limit 1];
        c2g__codaAccountingCurrency__c accCurrency = [select id from c2g__codaAccountingCurrency__c limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];
        Product2 p = [select id from Product2 limit 1];
        
        //Create Sales Invoice
        c2g__codaInvoice__c testInvoice = new c2g__codaInvoice__c();
        testInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testInvoice.c2g__InvoiceDate__c = date.today().addDays(-7);
        testInvoice.c2g__DueDate__c = date.today().addMonths(7);
        testInvoice.c2g__Account__c = testLicAccount.Id;
        testInvoice.JVCO_Customer_Type__c = 'New Business';
        testInvoice.c2g__OwnerCompany__c = testCompany.id;
        testInvoice.JVCO_Generate_Payment_Schedule__c = false;
        insert testInvoice;
        
        c2g__codaInvoiceLineItem__c testInvoiceItem = new c2g__codaInvoiceLineItem__c();
        testInvoiceItem.c2g__Product__c = p.id;
        testInvoiceItem.c2g__OwnerCompany__c = testCompany.id;
        testInvoiceItem.c2g__Invoice__c = testInvoice.id;
        testInvoiceItem.c2g__Quantity__c = 1.0;
        testInvoiceItem.c2g__UnitPrice__c = 200.0;
        insert testInvoiceItem;
        
        c2g.CODAAPICommon_10_0.Context contextInvoice = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference invoiceRef = c2g.CODAAPICommon.getRef(testInvoice.Id, null); 
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(contextInvoice, invoiceRef);
        
        JVCO_Document_Queue__c testDocument = new JVCO_Document_Queue__c();
        testDocument.JVCO_Recipient__c = testContact.id;
        testDocument.JVCO_Related_Account__c = testLicAccount.id;
        testDocument.JVCO_Related_Customer_Account__c = testCusAccount.id;
        testDocument.JVCO_Scenario__c = 'Generic Template';
        testDocument.JVCO_Transmission__c = 'Letter';
        testDocument.JVCO_Related_SalesInvoice__c = testInvoice.id;
        testDocument.JVCO_Salutation__c = 'Test Salutation';
        testDocument.JVCO_Email_Subject__c = 'Test Subject';
        testDocument.JVCO_Message_Body__c = 'Test body';
        testDocument.JVCO_Signature__c = 'Test Sign';
        insert TestDocument;

        test.stopTest();
    }
    
}