/* ----------------------------------------------------------------------------------------------
    Name: JVCO_WPUtil.cls 
    Description: Logic class for Website Payments

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    16-Apr-2018   0.1        jasper.j.figueroa   Intial creation
  12-Nov-2019   0.2    patrick.t.bautista  GREEN-35046 - removed unecessary query
----------------------------------------------------------------------------------------------- */ 
public class JVCO_WPUtil {

  public static Boolean isRedirected = false;
  public static string redActNumber{get; set;}
    public static string redInvn{get; set;}
    public static string redPCode{get; set;}
  public static Map<Id, List<String>> custActToListOfpCodeMap = new Map<Id, List<String>>();
    /* ----------------------------------------------------------------------------------------------
        Author: jasper.j.figueroa
        Company: Accenture
        Description: Return Accounts based on the inputted Account Number
        Inputs: String
        Returns: List<Account>
        <Date>      Version <Authors Name>    <Brief Description of Change> 
        16-Apr-2018 0.1   jasper.j.figueroa Initial version of function
        12-Nov-2019 0.2   patrick.t.bautista  GREEN-35046 - amended filter for account/invoice/postcode field
    ----------------------------------------------------------------------------------------------- */
    public static List<Account> checkAndGetAccountifValid(String actNumber){
        List<Account> actList = new List<Account>();
        for(Account act : [SELECT Id, AccountNumber, JVCO_Billing_Address__c, //GREEN-30835 jasper.j.figueroa 
                           JVCO_In_Enforcement__c, JVCO_In_Infringement__c,
                           JVCO_Merge_Id__c, JVCO_PPL_Id__c, JVCO_PRS_Id__c,
                           JVCO_Billing_Contact__c, JVCO_Billing_Contact__r.MailingPostalCode,
                           JVCO_Billing_Contact__r.MailingStreet, JVCO_Billing_Contact__r.MailingCountry, 
                           JVCO_Billing_Contact__r.MailingCity, JVCO_Billing_Contact__r.FirstName,
                           JVCO_Customer_Account__c
                           FROM Account
                           WHERE RecordType.Name = 'Licence Account'
                           AND AccountNumber = :actNumber])
        {
            if(!custActToListOfpCodeMap.containsKey(act.JVCO_Customer_Account__c)){
                custActToListOfpCodeMap.put(act.JVCO_Customer_Account__c, new List<String>());
            }
            actList.add(act);
        }
        
        return actList;
    }
    public static List<Account> checkAndGetAccountifValidFromId(String actId){
        List<Account> actList = new List<Account>();
        for(Account act : [SELECT Id, AccountNumber, JVCO_Billing_Address__c, //GREEN-30835 jasper.j.figueroa 
                           JVCO_In_Enforcement__c, JVCO_In_Infringement__c,
                           JVCO_Merge_Id__c, JVCO_PPL_Id__c, JVCO_PRS_Id__c,
                           JVCO_Billing_Contact__c, JVCO_Billing_Contact__r.MailingPostalCode,
                           JVCO_Billing_Contact__r.MailingStreet, JVCO_Billing_Contact__r.MailingCountry, 
                           JVCO_Billing_Contact__r.MailingCity, JVCO_Billing_Contact__r.FirstName,
                           JVCO_Customer_Account__c
                           FROM Account
                           WHERE RecordType.Name = 'Licence Account'
                           AND (Id = :actId OR AccountNumber = : actId)])
        {
            if(!custActToListOfpCodeMap.containsKey(act.JVCO_Customer_Account__c)){
                custActToListOfpCodeMap.put(act.JVCO_Customer_Account__c, new List<String>());
            }
            actList.add(act);
        }
        
        return actList;
    }
    public static List<c2g__codaInvoice__c> getAccountFromActNumber(List<Account> actList, String actNumber, String pCode, String invn){
    
        boolean withPCode = false;
        boolean withInvoice = false;
        
        //Get all related active contacts
        for(Contact cont : [SELECT Id, Account.Id, MailingPostalCode
                            FROM Contact 
                            WHERE Account.Id IN : custActToListOfpCodeMap.keySet()
                            AND SCMC__Active__c = true
                            AND MailingPostalCode != null])
        {
            if(custActToListOfpCodeMap.containsKey(cont.Account.id)
              && !custActToListOfpCodeMap.get(cont.Account.id).contains(cont.MailingPostalCode.replaceAll( '\\s+', '').toUpperCase())){
                custActToListOfpCodeMap.get(cont.Account.id).add(cont.MailingPostalCode.replaceAll( '\\s+', '').toUpperCase());
            }
        }
        //Get all affliation and its related venue contacts(postcode only)
        for(JVCO_Affiliation__c affi : [SELECT Id, JVCO_Account__r.JVCO_Customer_Account__c ,
                                        JVCO_Venue__r.JVCO_Postcode__c
                                        FROM JVCO_Affiliation__c
                                        WHERE JVCO_Venue__r.JVCO_Status__c = 'Active'
                                        AND JVCO_Account__c IN: actList
                                        AND JVCO_Venue__r.JVCO_Postcode__c != null])
        {
            if(custActToListOfpCodeMap.containsKey(affi.JVCO_Account__r.JVCO_Customer_Account__c)
              && !custActToListOfpCodeMap.get(affi.JVCO_Account__r.JVCO_Customer_Account__c).contains(affi.JVCO_Venue__r.JVCO_Postcode__c.replaceAll( '\\s+', '').toUpperCase())){
                custActToListOfpCodeMap.get(affi.JVCO_Account__r.JVCO_Customer_Account__c).add(affi.JVCO_Venue__r.JVCO_Postcode__c.replaceAll( '\\s+', '').toUpperCase());
            }
        }

        // if(actList.size() > 0){

        // }else{
        //     Account acc = new Account();
        //     acc.Id = actNumber;
        //     actList.add(acc);
        // }

        //Get invoices related to account number together with invoice number/postcode
        List<c2g__codaInvoice__c> invList = new List<c2g__codaInvoice__c>();
        for(c2g__codaInvoice__c invoice: [SELECT id, Name, JVCO_Invoice_Legacy_Number__c,
                                          c2g__InvoiceDate__c, c2g__OutstandingValue__c,
                                          c2g__Account__r.JVCO_Customer_Account__r.name,
                                          c2g__Account__r.AccountNumber, JVCO_Payment_Method__c,
                                          c2g__PaymentStatus__c, JVCO_Invoice_Group__c,
                                          c2g__Account__R.JVCO_Billing_Contact__R.FirstName, 
                                          c2g__Account__R.JVCO_Billing_Contact__R.lastName, 
                                          c2g__Account__R.JVCO_Billing_Contact__R.MailingStreet, 
                                          c2g__Account__R.JVCO_Billing_Contact__R.MailingCity, 
                                          c2g__Account__R.JVCO_Billing_Contact__R.MailingState, 
                                          c2g__Account__R.JVCO_Billing_Contact__R.MailingPostalCode,
                                          c2g__Account__R.JVCO_Billing_Contact__R.MailingCountry,
                                          c2g__Account__R.JVCO_Billing_Contact__R.Email,
                                          c2g__Account__R.JVCO_Billing_Address__c,
                                          c2g__Account__R.JVCO_Customer_Account__r.ShippingPostalCode
                                          FROM c2g__codaInvoice__c
                                          WHERE c2g__Account__c IN : actList])
        {
            invList.add(invoice);
            if(!String.isBlank(pCode) && invoice.c2g__Account__R.JVCO_Billing_Contact__R.MailingPostalCode != null
               && (invoice.c2g__Account__R.JVCO_Billing_Contact__R.MailingPostalCode.replaceAll( '\\s+', '').toUpperCase() == pCode
                   || (custActToListOfpCodeMap.containsKey(invoice.c2g__Account__R.JVCO_Customer_Account__c)
                       && custActToListOfpCodeMap.get(invoice.c2g__Account__R.JVCO_Customer_Account__c).contains(pCode))
                   || (invoice.c2g__Account__R.JVCO_Customer_Account__r.ShippingPostalCode != null
                       && invoice.c2g__Account__R.JVCO_Customer_Account__r.ShippingPostalCode.replaceAll( '\\s+', '').toUpperCase() == pCode)))
            {
                withPCode = true;
            }
            else if(String.isBlank(pCode) && !String.isBlank(invn) 
                    && ((!String.isBlank(invoice.JVCO_Invoice_Legacy_Number__c) && invoice.JVCO_Invoice_Legacy_Number__c.contains(invn))
                        || (invoice.Name == invn)
                        || (invoice.Name == 'SIN'+invn)))
            {
                withInvoice = true;
            }
        }
        return withPCode || withInvoice ? invList : null;
    }

    public static List<c2g__codaInvoice__c> getAccountFromAccountId(List<Account> actList, String accountId){
    
        //Get invoices related to account number together with invoice number/postcode
        List<c2g__codaInvoice__c> invList = new List<c2g__codaInvoice__c>();
        for(c2g__codaInvoice__c invoice: [SELECT id, Name, JVCO_Invoice_Legacy_Number__c,
                                          c2g__InvoiceDate__c, c2g__OutstandingValue__c,
                                          c2g__Account__r.JVCO_Customer_Account__r.name,
                                          c2g__Account__r.AccountNumber, JVCO_Payment_Method__c,
                                          c2g__PaymentStatus__c, JVCO_Invoice_Group__c,
                                          c2g__Account__R.JVCO_Billing_Contact__R.FirstName, 
                                          c2g__Account__R.JVCO_Billing_Contact__R.lastName, 
                                          c2g__Account__R.JVCO_Billing_Contact__R.MailingStreet, 
                                          c2g__Account__R.JVCO_Billing_Contact__R.MailingCity, 
                                          c2g__Account__R.JVCO_Billing_Contact__R.MailingState, 
                                          c2g__Account__R.JVCO_Billing_Contact__R.MailingPostalCode,
                                          c2g__Account__R.JVCO_Billing_Contact__R.MailingCountry,
                                          c2g__Account__R.JVCO_Billing_Contact__R.Email,
                                          c2g__Account__R.JVCO_Billing_Address__c,
                                          c2g__Account__R.JVCO_Customer_Account__r.ShippingPostalCode
                                          FROM c2g__codaInvoice__c
                                          WHERE c2g__Account__c IN : actList]){
            invList.add(invoice);
        }
        return invList;
    }

    /* ----------------------------------------------------------------------------------------------
        Author: jasper.j.figueroa
        Company: Accenture
        Description: Return Excluded Credit Notes based on the inputted Account Number
        Inputs: String
        Returns: List<Account>
        <Date>      <Authors Name>      <Brief Description of Change> 
        04-May-2018 jasper.j.figueroa    Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public static Boolean getCredNoteFromActNumber(Id actId){
       
        List<c2g__codaCreditNote__c> cNoteList = new List<c2g__codaCreditNote__c>();
        cNoteList = [SELECT Id, JVCO_Exclude_From_Webpayments__c, c2g__Account__c
                    FROM c2g__codaCreditNote__c
                    WHERE c2g__Account__c =: actId
                    AND JVCO_Exclude_From_Webpayments__c = true
                    ];

        return cNoteList.size() > 0 ? true : false;
    }
  
    /* ----------------------------------------------------------------------------------------------
        Author: jasper.j.figueroa
        Company: Accenture
        Description: Creates error messages
        Inputs: String
        Returns: List<Account>
        <Date>      <Authors Name>      <Brief Description of Change> 
        04-May-2018 jasper.j.figueroa    Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public static void createWebPaymentErrorLogs(String errorMessage, String actNum, String invn, String pCode, List<c2g__codaInvoice__c> selectedInvoices)
    {
        String inputDetails = 'Account Number Input: ' + actNum + '\r\n';
        String inputDetails1 = 'Invoice Number Input: ' + invn + '\r\n';
        String inputDetails2 = 'Post Code Input: ' + pCode + '\r\n';
        String inputDetails3 = 'Selected Invoices: ';
        if(selectedInvoices != null)
        {
           for(c2g__codaInvoice__c sInv : selectedInvoices)
            {
                inputDetails3 += sInv.Name + ', ';
            } 
        }

        JVCO_ErrorUtil errLogs = new JVCO_ErrorUtil();
        errLogs.logErrorFromWebPayments(inputDetails+inputDetails1+inputDetails2+inputDetails3, errorMessage);
    }

}