@isTest
public class JVCO_SmarterPayFailureLogicTest
{
    @testSetup static void createTestData() 
    {
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        //JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        SMP_TriggerSettings__c ts = new SMP_TriggerSettings__c();
        ts.Direct_Debit_History_Trigger__c = true;
        ts.SetupOwnerId = UserInfo.getUserId();
        insert ts;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        Test.startTest();
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        
        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c();
        orderGroup.JVCO_Group_Order_Account__c  = licAcc.id;
        insert orderGroup;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Order_Group__c = orderGroup.id;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        
        Set<Id> orderGroupIdSet = new Set<Id>();  
        orderGroupIdSet.add(orderGroup.Id);

        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_TestClassHelper.setGeneralSettingsCS();
        insert new GLA_for_Cash_Transfer__c(name = '10100 - Bank Cash'); 
        //JVCO_BackgroundMatchingLogic.stopMatching = true;
        JVCO_FFUtil.stopCashTransferLogic = true;
        
        Database.executeBatch(new JVCO_OrderMultipleBillNowLogicBatch(orderGroupIdSet));
        
        Test.stopTest();
        
        c2g__codaInvoice__c invoice = [SELECT Id, Name,c2g__PaymentStatus__c,c2g__OutstandingValue__c,c2g__InvoiceTotal__c,c2g__NetTotal__c,
                                       JVCO_DocGen_Number_of_Invoice_Lines__c
                                       FROM c2g__codaInvoice__c LIMIT 1];
        
        c2g__codaInvoiceInstallmentLineItem__c invoiceInstalmentLine = new c2g__codaInvoiceInstallmentLineItem__c();
        invoiceInstalmentLine.c2g__Amount__c = 12;
        invoiceInstalmentLine.c2g__DueDate__c = Date.today();
        invoiceInstalmentLine.JVCO_Paid_Amount__c = 12;
        invoiceInstalmentLine.JVCO_Payment_Status__c = 'Unpaid';
        invoiceInstalmentLine.c2g__Invoice__c = invoice.id;
        insert invoiceInstalmentLine;
        
        JVCO_Invoice_Group__c invoiceGroup = new JVCO_Invoice_Group__c();
        invoiceGroup.JVCO_Total_Amount__c = 100;
        invoiceGroup.CC_SINs__c = invoice.Name + ',';
        insert invoiceGroup;
        
        invoice.JVCO_Invoice_Group__c = invoiceGroup.Id;
        update invoice;

        Income_Direct_Debit__c incomeDD = new Income_Direct_Debit__c();
        incomeDD.Account__c = licAcc.Id;
        incomeDD.DD_Collection_Day__c = '1';
        incomeDD.DD_Collection_Period__c = 'Monthly';
        incomeDD.DD_Collection_Stretch__c = '1';
        incomeDD.DD_Collection_Type__c = 'Fixed';
        incomeDD.DD_Collection_Reference__c = 'Monthly Subscription';
        incomeDD.DD_Status__c = 'New Instruction';
        incomeDD.DD_Bank_Account_Number__c = '12345678';
        incomeDD.DD_Bank_Sort_Code__c = '123456';
        incomeDD.Number_Of_Payments__c = 2;
        insert incomeDD;
        
        SMP_DirectDebit_GroupInvoice__c IDDInvGroup = new SMP_DirectDebit_GroupInvoice__c();
        IDDInvGroup.Income_Direct_Debit__c = incomeDD.Id;
        IDDInvGroup.Invoice_Group__c = invoiceGroup.Id;
        insert IDDInvGroup;
        
        Income_Debit_History__c incomeDDHistory = new Income_Debit_History__c();
        incomeDDHistory.Amount__c = 100;
        incomeDDHistory.DD_Status__c = 'New Instruction';
        incomeDDHistory.Income_Direct_Debit__c = incomeDD.Id;
        incomeDDHistory.DD_Stage__c = 'Submitted';
        incomeDDHistory.DD_Collection_Date__c = Date.today();
        insert incomeDDHistory;
        
        Database.executeBatch(new JVCO_SmarterPayPaymentBatch('Direct Debit'));
    }

   @isTest
    static void testNonDeliberate()
    {     
        Test.startTest();       
        List<Income_Debit_History__c> incomeDDHistoryList = [SELECT Id, Income_Direct_Debit__c, 
                                                             Income_Direct_Debit__r.Account__c,
                                                             Amount__c,
                                                             DD_Collection_Date__c,
                                                             DD_Status__c,
                                                             DD_Stage__c
                                                             FROM Income_Debit_History__c];
       
        JVCO_Payonomy_Error_Codes__c testPayErrorCode = new JVCO_Payonomy_Error_Codes__c();
        testPayErrorCode.name = 'ARRUD-0';
        testPayErrorCode.JVCO_Deliberate__c = 'Non-Deliberate';
        testPayErrorCode.JVCO_Description__c = 'Error object in testClass';
        testPayErrorCode.JVCO_REASON_CODE__c = 'ARRUD-0';
        testPayErrorCode.JVCO_Cash_Refund__c = true;
        insert testPayErrorCode;         
        
        //UPDATING PAYMENT
        incomeDDHistoryList[0].DD_Status__c = 'First Collection';
        incomeDDHistoryList[0].DD_Stage__c = 'Failed';
        incomeDDHistoryList[0].DD_Code__c = 'ARRUD-0';
        incomeDDHistoryList[0].DD_Failed__c = true;
        update incomeDDHistoryList[0];  
        Test.stopTest();        
    }
    
    @isTest
    static void testDeliberate()
    {      
        
        Test.startTest();       
        List<Income_Debit_History__c> incomeDDHistoryList = [SELECT Id, Income_Direct_Debit__c, 
                                                             Income_Direct_Debit__r.Account__c,
                                                             Amount__c,
                                                             DD_Collection_Date__c,
                                                             DD_Status__c,
                                                             DD_Stage__c
                                                             FROM Income_Debit_History__c];
        
        JVCO_Payonomy_Error_Codes__c testPayErrorCode = new JVCO_Payonomy_Error_Codes__c();
        testPayErrorCode.name = 'ARRUD-1';
        testPayErrorCode.JVCO_Deliberate__c = 'Deliberate';
        testPayErrorCode.JVCO_Description__c = 'Error object in testClass';
        testPayErrorCode.JVCO_REASON_CODE__c = 'ARRUD-1';
        testPayErrorCode.JVCO_Cash_Refund__c = true;
        insert testPayErrorCode;        
        
        //UPDATING PAYMENT
        incomeDDHistoryList[0].DD_Status__c = 'First Collection';
        incomeDDHistoryList[0].DD_Stage__c = 'Failed';
        incomeDDHistoryList[0].DD_Code__c = 'ARRUD-1';
        incomeDDHistoryList[0].DD_Failed__c = true;
        update incomeDDHistoryList[0];        
        Test.stopTest();      
    }
    @isTest
    static void testPayonomyRefundQueueable(){
        List<c2g__codaTransactionLineItem__c> transLineList = [SELECT ID, c2g__AccountOutstandingValue__c 
                                                               FROM c2g__codaTransactionLineItem__c
                                                               WHERE c2g__LineType__c = 'Account'];
        c2g__codaCashEntry__c cashEntry = [SELECT Id FROM c2g__codaCashEntry__c LIMIT 1];
        System.enqueueJob(new JVCO_PayonomyMatchRefundQueueable(cashEntry, transLineList));
    }
}