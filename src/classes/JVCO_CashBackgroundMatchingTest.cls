@isTest
public class JVCO_CashBackgroundMatchingTest
{   
    private static c2g__codaTaxCode__c taxCode;
    private static c2g__codaDimension1__c dim1;
    private static c2g__codaDimension2__c dim2;
    private static c2g__codaDimension3__c dim3;
    private static Account licAcc;
    private static Product2 p;
    private static c2g__codaCashEntry__c cashEntry;

    @testSetup static void createTestData()
    {
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        //Settings
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_codaCreditNoteTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        dim2 = JVCO_TestClassHelper.setDim2('PPL');
        insert dim2;
        dim3 = JVCO_TestClassHelper.setDim3();
        insert dim3;

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;

        c2g__codaCashEntry__c cashEntry = JVCO_TestClassHelper.getCashEntry('Direct Debit');
        insert cashEntry;
        insert JVCO_TestClassHelper.getCashEntryLine(cashEntry.Id, licAcc.Id, 100, 'Direct Debit');
        
        c2g.CODAAPICommon.Reference cashRef = new c2g.CODAAPICommon.Reference();
        cashRef.Id = cashEntry.Id;
        
        Test.startTest();
        c2g.CODAAPICommon_7_0.Context context = new c2g.CODAAPICommon_7_0.Context();
        c2g.CODAAPICashEntry_7_0.PostCashEntry(context, cashRef);
        Test.stopTest();
    }

    static void init()
    {
        taxCode = [SELECT ID FROM c2g__codaTaxCode__c LIMIT 1];
        dim1 = [SELECT ID FROM c2g__codaDimension1__c LIMIT 1];
        dim2 = [SELECT ID FROM c2g__codaDimension2__c LIMIT 1];
        dim3 = [SELECT ID FROM c2g__codaDimension3__c LIMIT 1];
        licAcc = [SELECT Id FROM Account 
                    WHERE RecordTypeId =:Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId()
                    LIMIT 1];
        p = [SELECT Id FROM Product2 LIMIT 1];
        cashEntry = [SELECT Id FROM c2g__codaCashEntry__c LIMIT 1];
    }

    @isTest
    static void testMatchToCreditNoteLogic()
    {
    }

    @isTest
    static void testMatchToJournal()
    {
    }

    @isTest
    static void testMatchToInvoice()
    {
        init();
        Test.startTest();
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        insert sInv;
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);
        c2g.CODAAPICommon.Reference sInvRef = new c2g.CODAAPICommon.Reference();
        sInvRef.Id = sInv.Id;
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(context, sInvRef);

        c2g__codaInvoice__c postedSInv = [SELECT Id, Name FROM c2g__codaInvoice__c LIMIT 1];
        postedSInv.JVCO_Generate_Payment_Schedule__c = true;
        postedSInv.JVCO_Number_of_Payments__c = 3;
        postedSInv.JVCO_First_Due_Date__c = System.today().addDays(5);
        postedSInv.JVCO_Payment_Method__c = 'Direct Debit';
        update postedSInv;

        c2g__codaCashEntryLineItem__c celi = [SELECT Id FROM c2g__codaCashEntryLineItem__c LIMIT 1];
        celi.JVCO_Reference_Document__c = postedSInv.Name;
        update celi;
        
        JVCO_CashBackgroundMatchingBatch d = new JVCO_CashBackgroundMatchingBatch();
        Database.executeBatch(d, 1);
        Test.stopTest();
    }
    
    @isTest
    static void testMatchToInvoiceByCashEntryId()
    {
        init();
        Test.startTest();
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        insert sInv;
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);
        c2g.CODAAPICommon.Reference sInvRef = new c2g.CODAAPICommon.Reference();
        sInvRef.Id = sInv.Id;
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(context, sInvRef);

        c2g__codaInvoice__c postedSInv = [SELECT Id, Name FROM c2g__codaInvoice__c LIMIT 1];
        postedSInv.JVCO_Generate_Payment_Schedule__c = true;
        postedSInv.JVCO_Number_of_Payments__c = 3;
        postedSInv.JVCO_First_Due_Date__c = System.today().addDays(5);
        postedSInv.JVCO_Payment_Method__c = 'Direct Debit';
        update postedSInv;

        c2g__codaCashEntryLineItem__c celi = [SELECT Id FROM c2g__codaCashEntryLineItem__c LIMIT 1];
        celi.JVCO_Reference_Document__c = postedSInv.Name;
        update celi;
        
        Set<Id> cashEntryIdSet = new Set<Id>();
        cashEntryIdSet.add(cashEntry.Id);
        JVCO_CashBackgroundMatchingBatch d = new JVCO_CashBackgroundMatchingBatch(cashEntryIdSet, false, false);
        Database.executeBatch(d, 1);
        Test.stopTest();
    }
    
    @isTest
    static void testMatchToInvoiceByCashEntryIdByBatch()
    {
        init();
        Test.startTest();
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        insert sInv;
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);
        c2g.CODAAPICommon.Reference sInvRef = new c2g.CODAAPICommon.Reference();
        sInvRef.Id = sInv.Id;
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPISalesInvoice_10_0.PostInvoice(context, sInvRef);

        c2g__codaInvoice__c postedSInv = [SELECT Id, Name, c2g__Account__c FROM c2g__codaInvoice__c LIMIT 1];
        postedSInv.JVCO_Generate_Payment_Schedule__c = true;
        postedSInv.JVCO_Number_of_Payments__c = 3;
        postedSInv.JVCO_First_Due_Date__c = System.today().addDays(5);
        postedSInv.JVCO_Payment_Method__c = 'Direct Debit';
        update postedSInv;
        
        c2g__codaCashEntry__c cashEntry = JVCO_TestClassHelper.getCashEntry('Direct Debit');
        insert cashEntry;
        
        c2g__codaCashEntryLineItem__c cashEntryLI = new c2g__codaCashEntryLineItem__c();
        cashEntryLI.c2g__CashEntry__c = cashEntry.Id;
        cashEntryLI.c2g__Account__c = postedSInv.c2g__Account__c;
        cashEntryLI.c2g__CashEntryValue__c = 10;
        cashEntryLI.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cashEntryLI.ffcash__DeriveLineNumber__c = true;
        cashEntryLI.c2g__AccountReference__c = postedSInv.Name;
        insert cashEntryLI;
        
        c2g.CODAAPICommon.Reference cashRef = new c2g.CODAAPICommon.Reference();
        cashRef.Id = cashEntry.Id;
        
        c2g.CODAAPICommon_7_0.Context contextCash = new c2g.CODAAPICommon_7_0.Context();
        c2g.CODAAPICashEntry_7_0.PostCashEntry(contextCash, cashRef);
        
        Set<Id> cashEntryIdSet = new Set<Id>();
        cashEntryIdSet.add(cashEntry.Id);
        JVCO_CashBackgroundMatchingBatch d = new JVCO_CashBackgroundMatchingBatch(cashEntryIdSet, true, true);
        Database.executeBatch(d, 1);
        Test.stopTest();
    }
    
    @isTest
    static void testMatchToCash()
    {
    }

}