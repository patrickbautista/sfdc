@isTest
private class SMP_PaymentPageControllerTestClass{
    static Account createTestFramework() 
    {
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        //Settings
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_codaCreditNoteTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        c2g__codaDimension2__c dim2 = JVCO_TestClassHelper.setDim2('PPL');
        insert dim2;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;

        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;

        custAcc.JVCO_DD_Payee_Contact__c = c.Id;
        //update custAcc;

        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        system.debug('#### account with contact: ' + licAcc);
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;

        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        insert sInv;
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        c2g.CODAAPICommon.Reference sInvRef = new c2g.CODAAPICommon.Reference();
        sInvRef.Id = sInv.Id;
        return licAcc;
    }
 @isTest
 static void testconstructSinglePaymentLink() {
  /*
   Contact contact = new Contact(
      MailingStreet = 'Test Street',
      MailingPostalCode = 'TestPost',
      MailingCity =  'TestCity',
      Email = 'test@test.com',
      FirstName = 'TestName1', 
      LastName = 'TestName2'      
   );
   insert contact;
   string companyRecordId = Schema.SObjectType.c2g__codaCompany__c.getRecordTypeInfosByName().get('VAT').getRecordTypeId();

   c2g__codaCompany__c company = new c2g__codaCompany__c(
      Name = 'test company',                              // Name
      RecordTypeId = companyRecordId,                           // Record Type
      c2g__EliminationCompany__c = false,                   // Elimination Company
      c2g__UseOrgWideExchangeRates__c = false,              // Use Org-Wide Exchange Rates
      Use_Dimension_1__c = false,                           // Use Dimension 1
      c2g__VAT_GST_Group__c = false                        // VAT/GST Group
      //c2g__CashMatchingCurrencyMode__c = 'Account'         // Cash Matching Currency Mode
      //c2g__YearEndMode__c = 'Full Accounting Code'         // Year End Mode
    );
    insert company;

   c2g__codaInvoice__c testSalesInvoice = new c2g__codaInvoice__c(
      JVCO_Customer_Type__c = 'New Business',                   // New Business/Renewed
      JVCO_Billing_Contact__c = contact.ID,                     // Billing Contact
      JVCO_Invoice_Type__c = 'Standard',                        // Invoice Type
      //c2g__OwnerCompany__c = company.Id,                        // Company
      JVCO_Payment_Method__c = 'Direct Debit',                  // Payment Method
      //c2g__Account__c = '0011X00000Tre07',                      // Account
      //JVCO_Invoice_Group__c = 'aD21X0000008Oqv',                // Payment Plan
      JVCO_Protected__c = false,                                // Protected
      //JVCO_Licence_Start_Date__c = Date.valueOf('01-01-2019'),  // Period Start Date
      //JVCO_Licence_End_Date__c = Date.valueOf('31-12-2019'),    // Period End Date
      JVCO_From_Convert_to_Credit__c = false,                   // From Convert to Credit
      //JVCO_Sales_Rep__c = '0052X000008Gobv',                    // Sales Rep
      SalesRep_Profile__c = 'System Administrator',             // Stamped Sales Rep Profile
      SalesRep_Department__c = 'Accenture',                     // Stamped Sales Rep Department
      SalesRep_Manager__c = 'Joanne Lim',                       // Stamped Sales Rep Manager
      SalesRep_Role__c = 'Licensing User',                      // Stamped Sales Rep Role
      //c2g__InvoiceDate__c = Date.valueOf('02-03-2020'),         // Invoice Date
      //c2g__Transaction__c = 'a411X0000004Twi',                  // Transaction
      //c2g__Period__c = 'a3f1X000000218h',                       // Period
      //c2g__DueDate__c = Date.valueOf('30-03-2020'),             // Due Date
      //c2g__InvoiceCurrency__c = 'a2P1X000000HxAY',              // Invoice Currency
      c2g__InvoiceStatus__c = 'Complete',                       // Invoice Status
      JVCO_Cancelled__c = false,                                // Cancelled
      JVCO_Installment_Plan__c = true,                          // Installment Plan
      ffps_custRem__Exclude_From_Reminder_Process__c = true,    // Exclude from Dunning Cycle
      ffps_custRem__Last_Reminder_Severity_Level__c = 1,      // Last Reminder Level
      ffps_custRem__In_Dispute__c = false,                      // In Dispute
      JVCO_Sent_to_DCA__c = false,                              // Sent to DCA
      JVCO_Exempt_from_Surcharge__c = true,                     // Withhold Surcharge
      JVCO_Surcharge_Generated__c = false,                      // Surcharge Generated
      Total_Surchargeable_Amount__c = 0.00,                     // Total Surchargeable Amount
      JVCO_Surcharge_Tariffs__c = 0,                          // Surcharge Tariffs
      //c2g__Dimension1__c = 'a2v1X000000B7nH',                   // Dimension 1
      //c2g__Dimension3__c = 'a2x1X0000005RhZ',                   // Dimension 3
      JVCO_Generate_Payment_Schedule__c = false                // Generate Payment Schedule
    );
    insert testSalesInvoice;

   c2g__codaInvoiceLineItem__c lineitem = new c2g__codaInvoiceLineItem__c(
      c2g__UnitPrice__c = 1,
      c2g__Invoice__c = testSalesInvoice.Id

   );
   insert lineitem;
   */

   //  Account account = [Select id from Account limit 1];

    ApexPages.StandardController sc = new ApexPages.StandardController(createTestFramework());

    SMP_PaymentPageController controller = new SMP_PaymentPageController(sc);
    controller.salesInvoiceWrapperList[0].selected = true;
    controller.inputtedAmount = 10;
    controller.maxAmount = 10;
    controller.recalculateTotals();
    controller.constructSinglePaymentLink();

 }
   @isTest
   static void testNext() {
      // createTestFramework();
      // Account account = [Select id from Account limit 1];

      ApexPages.StandardController sc = new ApexPages.StandardController(createTestFramework());
  
      SMP_PaymentPageController controller = new SMP_PaymentPageController(sc);
      controller.next();
   }
   @isTest
   static void testBack() {
      // createTestFramework();
      // Account account = [Select id from Account limit 1];

      ApexPages.StandardController sc = new ApexPages.StandardController(createTestFramework());
  
      SMP_PaymentPageController controller = new SMP_PaymentPageController(sc);
      controller.back();
   }
   @isTest
   static void testCancel() {
      // createTestFramework();
      // Account account = [Select id from Account limit 1];

      ApexPages.StandardController sc = new ApexPages.StandardController(createTestFramework());
  
      SMP_PaymentPageController controller = new SMP_PaymentPageController(sc);
      controller.cancel();
   }
   @isTest
   static void testrecalculateTotals() {
      // createTestFramework();
      // Account account = [Select id from Account limit 1];

      ApexPages.StandardController sc = new ApexPages.StandardController(createTestFramework());
  
      SMP_PaymentPageController controller = new SMP_PaymentPageController(sc);
      controller.recalculateTotals();
      System.AssertEquals(true,controller.selectedSalesInvoiceList.isEmpty());
   }
   @isTest
   static void testController() {
      // createTestFramework();
      // Account account = [Select id from Account limit 1];

      ApexPages.StandardController sc = new ApexPages.StandardController(createTestFramework());
  
      SMP_PaymentPageController controller = new SMP_PaymentPageController(sc);
      System.AssertEquals(false,controller.salesInvoiceWrapperList.isEmpty());
   }
}