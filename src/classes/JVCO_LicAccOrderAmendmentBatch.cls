/* ----------------------------------------------------------------------------------------------------------
    Name:            JVCO_LicAccOrderAmendmentBatch.cls 
    Description:     Batch class for JVCO_LicAccCompleteAmendmentBatch
    Test class:      

    Date             Version     Author                            Summary of Changes 
    -----------      -------     -----------------                 -------------------------------------------
    25-June-2020       0.1        Accenture-rhys.j.c.dela.cruz         Initial version of the code 
 ---------------------------------------------------------------------------------------------------------- */
global class JVCO_LicAccOrderAmendmentBatch implements Database.Batchable<sObject>
{
    global Set<Id> quoteIds;

    global JVCO_LicAccOrderAmendmentBatch()
    {
        quoteIds = new Set<Id>();
    }

    global JVCO_LicAccOrderAmendmentBatch(Set<Id> quoteIdSet)
    {

        quoteIds = quoteIdSet;
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        List<JVCO_TempInvoiceProcessingData__c> tempDataList = [SELECT Id, JVCO_Batch_Name__c, JVCO_Id1__c, JVCO_Id2__c FROM JVCO_TempInvoiceProcessingData__c WHERE JVCO_Batch_Name__c = 'JVCO_LicAccGenerateAmendmentBatch'];
        Set<Id> tempDataSet = new Set<Id>();

        for(JVCO_TempInvoiceProcessingData__c tempRec : tempDataList){

            tempDataSet.add(tempRec.JVCO_Id1__c);
        }

        String query = 'SELECT Id, SBQQ__Ordered__c, SBQQ__PrimaryQuote__c, SBQQ__PrimaryQuote__r.SBQQ__MasterContract__c FROM Opportunity WHERE SBQQ__Contracted__c = TRUE AND SBQQ__Ordered__c = FALSE AND JVCO_OpportunityCancelled__c = FALSE AND (SBQQ__PrimaryQuote__r.SBQQ__MasterContract__r.JVCO_ContractScenario__c = \'Ends After Lockdown\' OR SBQQ__PrimaryQuote__r.SBQQ__MasterContract__r.JVCO_ContractScenario__c = \'Ends During Lockdown\') AND (SBQQ__PrimaryQuote__r.SBQQ__NetAmount__c != 0 AND SBQQ__PrimaryQuote__r.SBQQ__NetAmount__c != null) AND SBQQ__PrimaryQuote__r.SBQQ__Type__c = \'Amendment\' AND SBQQ__PrimaryQuote__r.JVCO_Recalculated__c = TRUE AND SBQQ__PrimaryQuote__r.SBQQ__LineItemCount__c > 0 AND SBQQ__PrimaryQuote__r.SBQQ__Primary__c = TRUE AND SBQQ__PrimaryQuote__r.NoOfQuoteLinesWithoutSPV__c = 0 ';

        if(!quoteIds.isEmpty()){

            query += ' AND SBQQ__PrimaryQuote__c IN: quoteIds ';
        }
        else{
            query += ' AND SBQQ__PrimaryQuote__r.SBQQ__MasterContract__c IN: tempDataSet ';
        }

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Opportunity> oppToUpdateList)
    {
        Set<Id> tempDataForDeletion = new Set<Id>();

        List<Opportunity> oppListUpdate = new List<Opportunity>();
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        Savepoint sp = Database.setSavepoint();

        for(Opportunity oppRec : oppToUpdateList){

            oppRec.SBQQ__Ordered__c = TRUE;
        }

        if(!oppToUpdateList.isEmpty()){

            List<Database.SaveResult> res = Database.update(oppToUpdateList,false);

            for(Integer i = 0; i < oppToUpdateList.size(); i++)
            {
                Database.SaveResult srOppList = res[i];
                Opportunity origrecord = oppToUpdateList[i];
                if(!srOppList.isSuccess())
                {
                    Database.rollback(sp);
                    for(Database.Error objErr:srOppList.getErrors())
                    {
                        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c(); 

                        errLog.ffps_custRem__Grouping__c = string.valueof(objErr.getStatusCode());
                        errLog.ffps_custRem__Detail__c = string.valueof(objErr.getMessage());
                        errLog.ffps_custRem__Message__c = 'JVCO_LicAccOrderAmendmentBatch';     
                        errLog.ffps_custRem__Related_Object_Key__c = string.valueof(origrecord.ID);
                        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
                        //errLog.Name = string.valueof(origrecord.ID);
                        errLogList.add(errLog);
                    }
                    tempDataForDeletion.add(origrecord.SBQQ__PrimaryQuote__r.SBQQ__MasterContract__c);
                }
                else{
                    tempDataForDeletion.add(origrecord.SBQQ__PrimaryQuote__r.SBQQ__MasterContract__c);
                }
            }
        }

        if(!errLogList.isEmpty()){
            insert errLogList;
        }

        if(!tempDataForDeletion.isEmpty()){

            List<JVCO_TempInvoiceProcessingData__c> tempDataListForDelete = [SELECT Id, JVCO_Batch_Name__c, JVCO_Id1__c, JVCO_Id2__c FROM JVCO_TempInvoiceProcessingData__c WHERE JVCO_Batch_Name__c = 'JVCO_LicAccGenerateAmendmentBatch' AND JVCO_Id1__c IN: tempDataForDeletion];

            if(!tempDataListForDelete.isEmpty()){
                delete tempDataListForDelete;
            }
        }
    }

    global void finish (Database.BatchableContext BC)
    {
        
    }
}