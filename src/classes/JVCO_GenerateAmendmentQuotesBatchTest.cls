@IsTest
public class JVCO_GenerateAmendmentQuotesBatchTest {
    
    @testSetup static void setupTestData() 
    {
        List<JVCO_Primary_Tariff_Mapping__c> settings = new List<JVCO_Primary_Tariff_Mapping__c>();
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Airport'));
        settings.add(new JVCO_Primary_Tariff_Mapping__c(Name = 'Aircraft'));
        insert settings;
        
        JVCO_TestClassObjectBuilder.createBillingConfig();

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        Product2 prod1 = new Product2();
        prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 83202-C';
        a1.Type = 'Key Account';

        c2g__codaDimension1__c dim = new c2g__codaDimension1__c();
        dim.c2g__ReportingCode__c = 'BC008TEST';
        dim.Name = 'General Purpose';
        insert dim;

        a1.c2g__CODADimension1__c = dim.Id;

        insert a1;
        
        Account a2 = new Account();
        a2 = JVCO_TestClassObjectBuilder.createLicenceAccount(a1.Id);
        insert a2;

        Pricebook2 testPB2 = new Pricebook2();
        testPB2.name = 'Standard Price Book';
        testPB2.IsActive = true;
        insert testPB2;
        
        PriceBookEntry pbe = new PriceBookEntry();
        pbe= JVCO_TestClassObjectBuilder.createPriceBookEntry(prod1.Id);
        insert pbe;
        
        Opportunity o = new Opportunity();
        o.Name = 'Test opportunity 83202';
        o.AccountId = a2.Id;
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Quote';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = a2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        insert q;
        
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        insert ql;
        
        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();
        venueRecord.Name = 'test1';              
        Insert  venueRecord;   
        
        Test.startTest();
        JVCO_Affiliation__c  affilRecord = new JVCO_Affiliation__c();
        affilRecord.JVCO_Account__c = a2.Id;
        affilRecord.JVCO_Venue__c = venueRecord.id;   
        affilRecord.JVCO_Start_Date__c = system.today();   
        Insert  affilRecord;   
        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;   

        Opportunity opp = new Opportunity();
        opp.Id = o.Id;
        opp.Amount = 10;
        opp.StageName = 'Closed Won';
        opp.SBQQ__Contracted__c = true;
        update opp;

        String cat = 'AA';
        JVCO_Venue__c nvenue2 = new JVCO_Venue__c();
        nvenue2.Name = 'Test Venue '+cat;
        nvenue2.JVCO_Lead_Source__c = 'Churn';
        nvenue2.JVCO_Venue_Type__c = 'Airport';
        nvenue2.JVCO_Venue_Sub_Type__c = 'Airport';
        nvenue2.JVCO_Budget_Category__c = 'Transports & Terminals';
        nvenue2.JVCO_City__c = 'City'+cat;
        nvenue2.JVCO_Country__c = 'United Kingdom';
        nvenue2.JVCO_Street__c = 'Street'+cat;
        nvenue2.JVCO_Postcode__c = 'N'+cat+' 7TH';
        insert nvenue2;

        JVCO_Affiliation__c  affilRecord2 = new JVCO_Affiliation__c();
        affilRecord2.JVCO_Account__c = a2.Id;
        affilRecord2.JVCO_Venue__c = nvenue2.id;   
        affilRecord2.JVCO_Start_Date__c = system.today().addMonths(3);
        insert affilRecord2;
       
        cat = 'BB';
        JVCO_Venue__c nvenue3 = new JVCO_Venue__c();
        nvenue3.Name = 'Test Venue '+cat;
        nvenue3.JVCO_Lead_Source__c = 'Churn';
        nvenue3.JVCO_Venue_Type__c = 'Airport';
        nvenue3.JVCO_Venue_Sub_Type__c = 'Airport';
        nvenue3.JVCO_Budget_Category__c = 'Transports & Terminals';
        nvenue3.JVCO_City__c = 'City'+cat;
        nvenue3.JVCO_Country__c = 'United Kingdom';
        nvenue3.JVCO_Street__c = 'Street'+cat;
        nvenue3.JVCO_Postcode__c = 'N'+cat+' 7TH';
        insert nvenue3;

        JVCO_Affiliation__c  affilRecord3 = new JVCO_Affiliation__c();
        affilRecord3.JVCO_Account__c = a2.Id;
        affilRecord3.JVCO_Venue__c = nvenue3.id;   
        affilRecord3.JVCO_Start_Date__c = system.today().addMonths(13);
        insert affilRecord3;

        Contract c1 = new Contract();
        c1.SBQQ__RenewalQuoted__c = false;  
        c1.AccountID = a2.Id;
        c1.StartDate = System.today();
        c1.ContractTerm = 12;
        c1.SBQQ__RenewalQuoted__c = false;
        c1.JVCO_Renewal_Generated__c = false;
        c1.JVCO_RenewableQuantity__c = 1;
        c1.EndDate = System.today().addMonths(12);
        c1.Name = 'Test Contract 18023-L';
        c1.SBQQ__Quote__c = q.Id;
        c1.SBQQ__Opportunity__c = o.Id;
        c1.AccountId = a2.Id;
        insert c1;
        Test.stopTest();
    }


    private static testMethod void testMethodGenerateAmendmentQuotesBatch() 
    {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account a1 = [select Id, Name, Licence_Account_Number__c from Account where RecordTypeId =: licenceRT limit 1];
        
        JVCO_Array_Size__c asCS = JVCO_Array_Size__c.getOrgDefaults();
        asCS.JVCO_Contract_Batch_Size__c = 1;
        upsert asCS JVCO_Array_Size__c.Id;
        
        Test.startTest();
        Id batchId = database.executeBatch(new JVCO_GenerateAmendmentQuotesBatch(a1), 1);
        Test.stopTest();
    }
}