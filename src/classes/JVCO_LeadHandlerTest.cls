@isTest
private class JVCO_LeadHandlerTest
{
	@testSetup
    private static void setupData()
    {
    	Lead l = new Lead();
    	l.LastName = 'Lead Test';
    	l.JVCO_Venue_Postcode__c = 'BL2 1AA';
    	l.JVCO_Field_Visit_Requested__c = true;
    	l.JVCO_Field_Visit_Requested_Date__c = date.today();
    	l.JVCO_Venue_Street__c = 'Test Street';
    	l.Company = 'Test Company';
        l.JVCO_Preferred_Communication_Method__c = 'Email';
        l.Email= 'test@email.com';
    	insert l;
    }

	@isTest
	static void itShould()
	{
		lead l = [SELECT JVCO_Venue_Postcode__c FROM lead order by CreatedDate DESC limit 1];
		l.JVCO_Venue_Postcode__c = 'AL2 1AA';
		update l;

	}
}