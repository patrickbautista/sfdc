/* ----------------------------------------------------------------------------------------------
    Name: JVCO_SmarterPayFailureLogic
    Test Class: JVCO_SmarterPayFailureLogicTest
    Description: Logic for failure of Income Direct Debit History(SmarterPay)

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    04-May-2020  0.1         patrick.t.bautista    Intial creation - GREEN-35543
----------------------------------------------------------------------------------------------- */
public class JVCO_SmarterPaymentFailureQueueable implements Queueable
{
    private List<c2g__codaCashMatchingHistory__c> matchingHistList;
    private JVCO_SmarterPaymentFailureQueueable.JVCO_SmarterPaymentFailureWrapper spWrapper;

    public JVCO_SmarterPaymentFailureQueueable(List<c2g__codaCashMatchingHistory__c> matchingHistList, JVCO_SmarterPaymentFailureQueueable.JVCO_SmarterPaymentFailureWrapper spWrapper)
    {
        this.spWrapper = spWrapper;
        this.matchingHistList = matchingHistList;
    }
    
    public void execute(QueueableContext context)
    {
        JVCO_codaCashMatchingHistoryHandler.forceFutureRun = true;
        JVCO_FFUtil.stopCodaTransactionHandlerSMP = true;
        try
        {
            JVCO_CashUnmatchingLogic.undoCashMatching(matchingHistList[0]);
        }catch(Exception e)
        {
            createErrorLogs('PP01', e, 'JVCO_SmarterPaymentFailureQueueable');
        }

        matchingHistList.remove(0);
        if(!matchingHistList.isEmpty())
        {
            System.enqueueJob(new JVCO_SmarterPaymentFailureQueueable(matchingHistList, spWrapper));
        }else
        {
            if(!spWrapper.ppIdToRefundMap.isEmpty())
            {
                try
                {
                    //Insert Refund Cash Entry
                    insertCashEntryList();
                    //Create Refund Line Item
                    createRefundCashEntryLineItemList();
                    update spWrapper.updatedCashEntryLineItemList;
                    //Change latest Installment to Unpaid
                    unpaidLatestInstallmentLine();
                    update new List<Income_Direct_Debit__c>(spWrapper.updatedIncomeDDSet);
                    JVCO_FFUtil.stopCodaInvoiceHandlerAfterUpdate = false;
                    update new List<c2g__codaInvoice__c>(spWrapper.sInvToUpdateSet);
                    
                    //Post Refund Cash
                    postRefundCash();
                    //Match Refund to Receipt Cash
                    matchRefundCash();
                    
                }catch(Exception e)
                {
                    createErrorLogs('PP02', e, 'JVCO_SmarterPaymentFailureQueueable');
                }
            }
        }
    }

    @TestVisible
    private void insertCashEntryList()
    {
        for(Id ppId : spWrapper.ppIdToRefundMap.keySet())
        {
            spWrapper.ppIdToRefundMap.get(ppId).c2g__NetValue__c = spWrapper.ppIdToCLITotalAmtMap.get(ppId);
        }
        insert spWrapper.ppIdToRefundMap.values();
    }

    @TestVisible
    private void createRefundCashEntryLineItemList()
    {
        List<c2g__codaCashEntryLineItem__c> refundCashEntryLineItemList = new List<c2g__codaCashEntryLineItem__c>();
        for(c2g__codaCashEntry__c c : spWrapper.ppIdToRefundMap.values())
        {
            c2g__codaCashEntryLineItem__c cashEntryLineItem = new c2g__codaCashEntryLineItem__c();
            // Populate Cash Entry Line Item fields
            cashEntryLineItem.c2g__CashEntry__c = c.Id;
            cashEntryLineItem.c2g__Account__c = c.c2g__Account__c;
            cashEntryLineItem.c2g__CashEntryValue__c = c.c2g__NetValue__c;
            refundCashEntryLineItemList.add(cashEntryLineItem);
        }
        insert refundCashEntryLineItemList;
    }

    @TestVisible
    private void unpaidLatestInstallmentLine()
    {
        for(c2g__codaInvoiceInstallmentLineItem__c invInstallmentLine : spWrapper.updatedInvInstallmentSet)
        {
            invInstallmentLine.JVCO_Payment_Status__c = 'Unpaid';
            invInstallmentLine.JVCO_Paid_Amount__c = 0;
        }
        update new List<c2g__codaInvoiceInstallmentLineItem__c>(spWrapper.updatedInvInstallmentSet);
    }

    @TestVisible
    private void postRefundCash()
    {
        c2g.CODAAPICommon_7_0.Context context = new c2g.CODAAPICommon_7_0.Context();
        List<c2g.CODAAPICommon.Reference> referenceList = new List<c2g.CODAAPICommon.Reference>();
        for (c2g__codaCashEntry__c cashRefund : spWrapper.ppIdToRefundMap.values())
        {
            c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = cashRefund.Id;
            referenceList.add(reference);
        }
        c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(context, referenceList);
    }

    @TestVisible
    private void matchRefundCash()
    {
        for(c2g__codaCashEntry__c cashRefund : spWrapper.ppIdToRefundMap.values())
        {
            if(spWrapper.accIdToReceiptTransLineListMap.containsKey(cashRefund.c2g__Account__c))
            {
                List<c2g__codaTransactionLineItem__c> transLineList = spWrapper.accIdToReceiptTransLineListMap.get(cashRefund.c2g__Account__c);
                if(!transLineList.isEmpty())
                {
                    Id jobId = System.enqueueJob(new JVCO_PayonomyMatchRefundQueueable(cashRefund, transLineList));
                }
            }
        }
    }

    @TestVisible
    private static void createErrorLogs(String errCode, Exception e, String errorBatchName)
    {              
        List<ffps_custRem__Custom_Log__c> errLogList = new List<ffps_custRem__Custom_Log__c>();
        ffps_custRem__Custom_Log__c errLog = new ffps_custRem__Custom_Log__c();
        errLog.ffps_custRem__Running_User__c = UserInfo.getUserId();
        errLog.ffps_custRem__Detail__c = string.valueof(e.getMessage());
        errLog.ffps_custRem__Message__c = errorBatchName;
        errLog.ffps_custRem__Grouping__c = errCode;
        errLogList.add(errLog); 
        insert errLogList;
    }

    public class JVCO_SmarterPaymentFailureWrapper
    {
        public final Map<String, JVCO_Payonomy_Error_Codes__c> errorCodesMap = new Map<String, JVCO_Payonomy_Error_Codes__c>();
        //Allowable Payment Id for failure logic
        public Set<Id> paymentIdSet = new Set<Id>();
        //Set of Cash Receipt Transaction - Use in getting Transaction Line List
        public Set<Id> transIdSet = new Set<Id>();
        //Set of Account Reference in Cash Entry Line Item
        public Set<String> accReferenceSet = new Set<String>();
        //PayonomyPaymentId to Refund Cash Map
        public Map<Id, c2g__codaCashEntry__c> ppIdToRefundMap = new Map<Id, c2g__codaCashEntry__c>();
        //PayonomyPaymentId to Total Amount Map
        public Map<Id, Double> ppIdToCLITotalAmtMap = new Map<Id, Double>();
        
        //All Account Reference Map
        public Map<String, c2g__codaInvoice__c> acctReferenceToInvIdMap = new Map<String, c2g__codaInvoice__c>();
        public Map<Id, List<c2g__codaTransactionLineItem__c>> accIdToReceiptTransLineListMap = new Map<Id, List<c2g__codaTransactionLineItem__c>>();
        public Map<String, List<c2g__codaInvoiceInstallmentLineItem__c>> accRefToPaidInstallmentLineListMap = new Map<String, List<c2g__codaInvoiceInstallmentLineItem__c>>();
        public Map<String, Date> accRefToCollectionDateMap = new Map<String, Date>();
        
        public List<c2g__codaCashEntryLineItem__c> cashEntryLineItemList = new List<c2g__codaCashEntryLineItem__c>();
        //List of Receipt Cash Transaction Line - Use in getting Matching Histories
        public List<c2g__codaTransactionLineItem__c> receiptTransLineList = new List<c2g__codaTransactionLineItem__c>();
        //List of Matching Histories for Receipt - Use in getting Matching Reference
        public List<c2g__codaCashMatchingHistory__c> matchingHistList = new List<c2g__codaCashMatchingHistory__c>();
        
        //List of records to be updated
        public List<c2g__codaCashEntryLineItem__c> updatedCashEntryLineItemList = new List<c2g__codaCashEntryLineItem__c>();
        public Set<c2g__codaInvoiceInstallmentLineItem__c> installmentlineAdd5daysSet = new Set<c2g__codaInvoiceInstallmentLineItem__c>();
        public Set<c2g__codaInvoiceInstallmentLineItem__c> updatedInvInstallmentSet = new Set<c2g__codaInvoiceInstallmentLineItem__c>();
        public Set<Income_Direct_Debit__c> updatedIncomeDDSet = new Set<Income_Direct_Debit__c>();
        public Set<c2g__codaInvoice__c> sInvToUpdateSet = new Set<c2g__codaInvoice__c>();
    }
}