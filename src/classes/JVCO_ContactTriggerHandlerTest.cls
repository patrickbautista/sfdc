@isTest
private class JVCO_ContactTriggerHandlerTest{

    @testSetup
    static void setupData(){
    
        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;
        
        Contact cnt=new Contact();
        cnt.FirstName= 'Manny';
        cnt.LastName = 'Pacquiao';
        cnt.Email = 'mp@mp.com';
        cnt.MailingStreet= 'Black Prince Road';
        cnt.MailingCity = 'London';
        cnt.MailingPostalCode = 'SE11 6';
        cnt.AccountId=acc.Id; 
        insert cnt;
        
        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        acc2.Type = 'Customer';
        acc2.JVCO_Billing_Contact__c = cnt.Id; 
        acc2.JVCO_Review_Contact__c = cnt.Id;
        acc2.JVCO_Licence_Holder__c = cnt.Id;
        acc2.JVCO_DD_Payee_Contact__c = cnt.Id; 
        insert acc2;
    }
    
    static testMethod void testUpdateAddressAndPhoneValue(){
        Contact con = [SELECT Email,ID FROM Contact WHERE Email='mp@mp.com'];
        con.MailingStreet = 'Black Prince Rd';      
        con.Phone = '987654321';  
        update con;
    }
    
    static testMethod void testDeletingEmailValue(){
        Contact con = [SELECT Email,ID FROM Contact WHERE Email='mp@mp.com'];
        con.Email = '';        
        try{
            update con;         
        }
        catch(Exception e){
            System.Assert(e.getMessage().contains('You cannot delete the Email of the Contact because it is used as a Billing/Review/Licence/DD Payee contact on a licence account'));
        }    
    }
    
    static testMethod void testDeletingAddressValue(){                
        Account licAcct = [SELECT Id from Account WHERE RecordType.Name='Licence Account'];
        licAcct.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
        update licAcct;
        
        Contact con = [SELECT Email,ID FROM Contact WHERE Email='mp@mp.com'];
        con.MailingStreet = '';
        
        try{
            update con;         
        }
        catch(Exception e){
            System.Assert(e.getMessage().contains('You cannot delete the address of the Contact because it is used as a Billing/Review/Licence/DD Payee contact on a licence account'));
        }    
    }
    
    static testMethod void testSettingContactToInactive(){        
        Contact con = [SELECT Email,ID FROM Contact WHERE Email='mp@mp.com'];
        con.JVCO_Contact_Status__c = 'Inactive';        
        try{
            update con;         
        }
        catch(Exception e){
            System.Assert(e.getMessage().contains('You cannot make the Contact inactive because it is used as a Billing/Review/Licence/DD Payee contact on a licence account'));
        }    
    }
    
    
    static testMethod void testDeletingContact(){        
        Contact con = [SELECT Email,ID FROM Contact WHERE Email='mp@mp.com'];                
        try{
            delete con;         
        }
        catch(Exception e){
            System.Assert(e.getMessage().contains('You cannot delete the Contact because it is used as a Billing/Review/Licence/DD Payee contact on a licence account'));
        }    
    }
}