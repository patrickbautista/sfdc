/* --------------------------------------------------------------------------------------------------
   Name:            JVCO_CreditNoteBackgroundMatchingBatch.cls 
   Description:     Batch Class for JVCO_CreditNoteBackgroundMatchingBatch.cls
   Test class:      

   Date            Version     Author              Summary of Changes 
   -----------     -------     -----------------   -----------------------------------
   21-Feb-2019     0.1         franz.g.a.dimaapi   Intial creation
  ------------------------------------------------------------------------------------------------ */
public class JVCO_CreditNoteBackgroundMatchingBatch implements Database.Batchable<sObject>, Database.Stateful
{
    private Boolean isQueryAll;
    private static final JVCO_General_Settings__c generalSettings = JVCO_General_Settings__c.getInstance();
    private Map<Id, c2g__codaCreditNote__c> sInvIdToCNoteMap;
    public Map<Id, Id> licAndCustAccIdMap = new Map<Id, Id>();
    
    public JVCO_CreditNoteBackgroundMatchingBatch(){
        isQueryAll = true;
        sInvIdToCNoteMap = new Map<Id, c2g__codaCreditNote__c>();
    }
    
    public JVCO_CreditNoteBackgroundMatchingBatch(Map<Id, c2g__codaCreditNote__c> sInvIdToCNoteMap){
        isQueryAll = false;
        this.sInvIdToCNoteMap = sInvIdToCNoteMap;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {   
        if(isQueryAll){
            return Database.getQueryLocator([SELECT Id,
                                             c2g__Account__c,
                                             c2g__Account__r.JVCO_Customer_Account__c,
                                             c2g__AccountOutstandingValue__c,
                                             c2g__OwnerCompany__r.c2g__CustomerWriteOff__c,
                                             c2g__Transaction__r.c2g__Period__r.c2g__Closed__c,
                                             c2g__Transaction__r.c2g__TransactionType__c,
                                             c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Reference_Document__c,
                                             c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Reference_Document_Type__c
                                             FROM c2g__codaTransactionLineItem__c
                                             WHERE c2g__AccountOutstandingValue__c != 0
                                             AND c2g__LineType__c IN ('Account')
                                             AND c2g__LineDescription__c != 'Not for Matching'
                                             AND c2g__Account__r.JVCO_In_Infringement__c = FALSE
                                             AND c2g__Account__r.JVCO_In_Enforcement__c = FALSE
                                             AND c2g__Account__r.JVCO_Exclude_From_Background_Matching__c = FALSE
                                             AND c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Reference_Document_Type__c IN ('Invoice', 'Credit', 'Journal', 'Cash')
                                            ]);
        }
        else{
            return Database.getQueryLocator([SELECT Id,
                                             c2g__Account__c,
                                             c2g__Account__r.JVCO_Customer_Account__c,
                                             c2g__AccountOutstandingValue__c,
                                             c2g__OwnerCompany__r.c2g__CustomerWriteOff__c,
                                             c2g__Transaction__r.c2g__Period__r.c2g__Closed__c,
                                             c2g__Transaction__r.c2g__TransactionType__c,
                                             c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Reference_Document__c,
                                             c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Reference_Document_Type__c
                                             FROM c2g__codaTransactionLineItem__c
                                             WHERE c2g__AccountOutstandingValue__c != 0
                                             AND c2g__LineType__c IN ('Account')
                                             AND c2g__LineDescription__c != 'Not for Matching'
                                             AND c2g__Account__r.JVCO_In_Infringement__c = FALSE
                                             AND c2g__Account__r.JVCO_In_Enforcement__c = FALSE
                                             AND c2g__Account__r.JVCO_Exclude_From_Background_Matching__c = FALSE
                                             AND c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Reference_Document_Type__c IN ('Invoice', 'Credit', 'Journal', 'Cash')
                                             AND (c2g__Transaction__r.c2g__SalesInvoice__c IN : sInvIdToCNoteMap.keySet()
                                                  OR c2g__Transaction__r.c2g__SalesCreditNote__c IN : sInvIdToCNoteMap.values())
                                            ]);
        }
    }
    
    public void execute(Database.BatchableContext bc, List<c2g__codaTransactionLineItem__c> scope)
    {
        if(!generalSettings.JVCO_Disable_CNote_Background_Matching__c)
        {
            setMatchingReferenceMapping(scope);
            removeReferenceDocument(scope);
            getAccountToRunQueable(scope);
        }
    }
    
    private void setMatchingReferenceMapping(List<c2g__codaTransactionLineItem__c> transLineList)
    {
        Map<Id, String> cNoteTransLineIdToSInvNameMap = new Map<Id, String>();
        Map<Id, String> cNoteTransLineIdToCNoteNameMap = new Map<Id, String>();
        Map<Id, String> cNoteTransLineIdToJournalNameMap = new Map<Id, String>();
        Map<Id, String> cNoteTransLineIdToReceiptLineNameMap = new Map<Id, String>();
        Map<Id, String> cNoteTransLineIdToRefundLineNameMap = new Map<Id, String>();
        Map<Id, c2g__codaTransactionLineItem__c> transLineMap = new Map<Id, c2g__codaTransactionLineItem__c>();
        
        for(c2g__codaTransactionLineItem__c tli : transLineList)
        {
            String cNoteReferenceDocument = tli.c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Reference_Document__c;
            if(tli.c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Invoice'))
            {
                cNoteTransLineIdToSInvNameMap.put(tli.Id, cNoteReferenceDocument);
            }else if(tli.c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Credit'))
            {
                cNoteTransLineIdToCNoteNameMap.put(tli.Id, cNoteReferenceDocument);
            }else if(tli.c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Journal'))
            {
                cNoteTransLineIdToJournalNameMap.put(tli.Id, cNoteReferenceDocument);
            }else if(tli.c2g__Transaction__r.c2g__SalesCreditNote__r.JVCO_Reference_Document_Type__c.equalsIgnoreCase('Cash') &&
                tli.c2g__AccountOutstandingValue__c > 0)
            {
                cNoteTransLineIdToReceiptLineNameMap.put(tli.Id, cNoteReferenceDocument);
            }else
            {
                cNoteTransLineIdToRefundLineNameMap.put(tli.Id, cNoteReferenceDocument);
            }
            transLineMap.put(tli.Id, tli);
        }
        executeMatching(cNoteTransLineIdToSInvNameMap, cNoteTransLineIdToCNoteNameMap, cNoteTransLineIdToJournalNameMap,
                        cNoteTransLineIdToReceiptLineNameMap, cNoteTransLineIdToRefundLineNameMap, transLineMap);
    }

    private void executeMatching(Map<Id, String> cNoteTransLineIdToSInvNameMap, Map<Id, String> cNoteTransLineIdToCNoteNameMap,
                                 Map<Id, String> cNoteTransLineIdToJournalNameMap, Map<Id, String> cNoteTransLineIdToReceiptLineNameMap,
                                 Map<Id, String> cNoteTransLineIdToRefundLineNameMap, Map<Id, c2g__codaTransactionLineItem__c> transLineMap)
    {
        JVCO_MatchingReferenceDocumentInterface mrdi = null;
        if(!cNoteTransLineIdToSInvNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToInvoiceLogic(cNoteTransLineIdToSInvNameMap, transLineMap);
        }else if(!cNoteTransLineIdToCNoteNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToCreditNoteLogic(cNoteTransLineIdToCNoteNameMap, transLineMap);
        }else if(!cNoteTransLineIdToJournalNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToJournalLogic(cNoteTransLineIdToJournalNameMap, transLineMap);
        }else if(!cNoteTransLineIdToReceiptLineNameMap.isEmpty())
        {
            mrdi = new JVCO_MatchToCashReceiptLogic(cNoteTransLineIdToReceiptLineNameMap, transLineMap);
        }else
        {
            mrdi = new JVCO_MatchToCashRefundLogic(cNoteTransLineIdToRefundLineNameMap, transLineMap);
        }
        mrdi.execute();
    }
    
    private void getAccountToRunQueable(List<c2g__codaTransactionLineItem__c> scope){
        for(c2g__codaTransactionLineItem__c tli : scope)
        {
            if(!licAndCustAccIdMap.containsKey(tli.c2g__Account__c) && tli.c2g__Account__c != null && tli.c2g__Account__r.JVCO_Customer_Account__c != null){
                licAndCustAccIdMap.put(tli.c2g__Account__c, tli.c2g__Account__r.JVCO_Customer_Account__c);
            }
        }
    }
    
    private void removeReferenceDocument(List<c2g__codaTransactionLineItem__c> transLineList)
    {
        Set<c2g__codaCreditNote__c> cNoteSet = new Set<c2g__codaCreditNote__c>();
        for(c2g__codaTransactionLineItem__c transLine : transLineList)
        {
            c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
            cNote.Id = transLine.c2g__Transaction__r.c2g__SalesCreditNote__c;
            cNote.JVCO_Reference_Document__c = null;
            cNoteSet.add(cNote);
        }
        JVCO_FFUtil.stopInvoiceCancellation = true;
        update new List<c2g__codaCreditNote__c>(cNoteSet);
    }

    public void finish(Database.BatchableContext bc)
    {
        if(isQueryAll){
            JVCO_SalesInvoiceBackgroundMatchingBatch sibm = new JVCO_SalesInvoiceBackgroundMatchingBatch();
            sibm.licAndCustAccIdMap = licAndCustAccIdMap;
            Database.executeBatch(sibm, 1);
        }
        else if(!licAndCustAccIdMap.isEmpty() && !Test.isRunningTest()){
            System.enqueueJob(new JVCO_UpdateSalesInvoiceQueueable(licAndCustAccIdMap));
        }
    }
}