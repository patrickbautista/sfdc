/* ----------------------------------------------------------------------------------------------
    Name: JVCO_PostSalesCreditNoteBatch
    Description: Batch for Posting Sales Credit Note

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    20-Jan-2018  0.1         franz.g.a.dimaapi     Intial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_PostSalesCreditNoteBatch implements Database.Batchable<sObject>, Database.Stateful{

    private Set<Id> cNoteIdStatefulSet = new Set<Id>();
    public Map<Id, Id> licAndCustAccIdMap = new Map<Id, Id>();
    public Boolean stopMatching;

    public JVCO_PostSalesCreditNoteBatch(){}
    public JVCO_PostSalesCreditNoteBatch(Set<Id> cNoteIdStatefulSet)
    {
        this.cNoteIdStatefulSet = cNoteIdStatefulSet;
    }

    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT Id,
                                         c2g__Account__c, 
                                         c2g__Account__r.JVCO_Customer_Account__c
                                         FROM c2g__codaCreditNote__c
                                         WHERE Id IN : cNoteIdStatefulSet]);
    }

    public void execute(Database.BatchableContext bc, List<c2g__codaCreditNote__c> scope)
    {   
        bulkPostCreditNote(scope);
        for(c2g__codaCreditNote__c cNote: scope){
            if(!licAndCustAccIdMap.containsKey(cNote.c2g__Account__c) && cNote.c2g__Account__c != null && cNote.c2g__Account__r.JVCO_Customer_Account__c != null){
                licAndCustAccIdMap.put(cNote.c2g__Account__c, cNote.c2g__Account__r.JVCO_Customer_Account__c);
            }
        }
    }

    public void finish(Database.BatchableContext bc)
    {
        if(!licAndCustAccIdMap.isEmpty()){
            System.enqueueJob(new JVCO_UpdateSalesInvoiceQueueable(licAndCustAccIdMap));
        }
    }  
    
    private void bulkPostCreditNote(List<c2g__codaCreditNote__c> cNoteList)
    {
        c2g.CODAAPICommon_10_0.Context context = new c2g.CODAAPICommon_10_0.Context();
        List<c2g.CODAAPICommon.Reference> refList = new List<c2g.CODAAPICommon.Reference>();
        for (c2g__codaCreditNote__c cNote : cNoteList)
        {
            c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = cNote.Id;
            refList.add(reference);
        }
        
        JVCO_BackgroundMatchingLogic.stopMatching = stopMatching;
        c2g.CODAAPISalesCreditNote_10_0.BulkPostCreditNote(context, refList);
    }
}