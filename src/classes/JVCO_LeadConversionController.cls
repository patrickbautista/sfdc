/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_LeadConversionController.cls 
   Description:     Visualforce controller class for page JVCO_LeadConversion.page
   Test class:      JVCO_LeadConversionControllerTest.cls 

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
  16-Aug-2016       0.1         Accenture-joseph.g.barrameda      Initial draft           
  09-Dec-2016       0.2         Accenture-raus.k.b.ablaza         Added creation of quote in "Create Quote Now"
  07-Dec-2017       0.3         filip.bezak@accenture.com         GREEN-26844 - Forwarding Customer Account Type to Licence Account
  ---------------------------------------------------------------------------------------------------------- */


public class JVCO_LeadConversionController {

    public Lead leadRecord {get;set;}
    
    public Boolean isLeadQualified {get;set;}
    public Boolean isRedirectToOpp {get;set;}
    public Boolean isActivityBasedLicence {get;set;}
    
    public Boolean isCreateNewCustomerAccount {get;set;}    
    public Boolean isCreateNewLicenceAccount {get;set;}    
    public Boolean isCreateNewContact {get;set;}
    public Boolean isCreateNewVenue {get;set;}
    
    public Boolean hasDuplicateAccountRecord {get;set;}
    public Boolean hasDuplicateLicenceAccount {get;set;}    
    public Boolean hasDuplicateCustomerAccount {get;set;}
    public Boolean hasDuplicateContactRecord {get;set;}
    public Boolean hasDuplicateVenueRecord {get;set;}    
    
    // Initialize a list to hold any duplicate records
    public List<sObject> duplicateLicenceAccountList {get;set;}
    public List<sObject> duplicateCustomerAccountList {get;set;}
    public List<sObject> duplicateContactList {get;set;}
    public List<sObject> duplicateVenueList   {get;set;}
    
    public String selectedLicenceAccountId {get;set;}
    public String selectedCustomerAccountId {get;set;}
    public String selectedContactId {get;set;}
    public String selectedVenueId {get;set;}
    
    public String leadQuery; 
    public String accountQuery;
    public String venueQuery; 
    public String contactQuery; 
    
    
    public Id defaultVatCode;
                
    public ApexPages.StandardController stdController;
    
    //  START   Raus Ablaza     12/14/2016      Holder for Ids needed in creating venue
    public Id convertAccountId;
    public Id convertContactId;
    public Id convertOpportunityId;
    public Boolean hasErrorInConversion = false;
    //  END     Raus Ablaza     12/14/2016

    //Constructor  
    public JVCO_LeadConversionController(ApexPages.StandardController controller) {
    
        leadRecord= (Lead)controller.getRecord();
        
        //Construct SOQL string in order to minimize calling getAllFieldNames on every method 
        accountQuery = 'SELECT ' + JVCO_ObjectMappingUtils.getAllFieldNames('Account') + ' FROM Account';
        contactQuery = 'SELECT ' + JVCO_ObjectMappingUtils.getAllFieldNames('Contact') + ' FROM Contact'; 
        venueQuery = 'SELECT ' + JVCO_ObjectMappingUtils.getAllFieldNames('JVCO_Venue__c') + ' FROM JVCO_Venue__c';
        leadQuery = 'SELECT ' + JVCO_ObjectMappingUtils.getAllFieldNames('Lead') + ' FROM Lead WHERE Id=\'' + leadRecord.id + '\''; 
        leadRecord= Database.Query(leadQuery);
        
        defaultVatCode = [SELECT Id from c2g__codaTaxCode__c WHERE Name ='GB-O-STD-LIC'].Id;
        
        //Check if Lead has already been converted 
        if (leadRecord.IsConverted == True)
        {
            isLeadQualified = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Lead_has_already_been_converted));
            return;
        }
        else {
            isLeadQualified = true;
        }
    
        //Check if Lead status is Qualified before conversion          
        if (leadRecord.Status != 'Qualified')
        {
            isLeadQualified= false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Lead_information_not_complete));
            return;
        }
        else {
            isLeadQualified = true;
        }
        
        hasDuplicateAccountRecord = false; 
        
        hasDuplicateLicenceAccount = false;
        hasDuplicateCustomerAccount = false;      
        hasDuplicateContactRecord = false;
        hasDuplicateVenueRecord = false;    
        
        isCreateNewLicenceAccount = true;
        isCreateNewCustomerAccount = true ; 
        isCreateNewContact = true;
        isCreateNewVenue = true;
        
        isActivityBasedLicence = leadRecord.JVCO_Activity_Based_Licence__c; 
    }

    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used to call other method to validate duplicate records. 
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-Aug-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public PageReference checkDuplicates(){
        
        try{
            validateDuplicateCustomerAccountRecords();    
            
            //Bypass checking of Venue if Activity Based Licence is checked          
            if(isActivityBasedLicence == false){         
                validateDuplicateVenueRecords(); 
            }
            
            hasDuplicateAccountRecord  = hasDuplicateLicenceAccount || hasDuplicateCustomerAccount;
        }
        catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Validation Error: ' + ex.getMessage()));                        
        }        
        return null;        
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used to validate duplicate Licence Account records. 
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    26-Sep-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/        
    public PageReference validateDuplicateLicenceAccountRecords(){
        
        //Create Savepoint
        Savepoint sp = Database.setSavepoint();
        Account newAccountRecord = new Account();
        
        if(selectedCustomerAccountId != null ){     // Need to populate Billing Contact 
            Contact tempContact = [SELECT Id, MailingStreet,MailingCity, MailingPostalCode, Email from Contact WHERE AccountId=: selectedCustomerAccountId LIMIT 1];
            newAccountRecord= JVCO_LeadMappingLogic.createLicenceAccount(leadRecord.JVCO_Account_Name__c, tempContact.Id, leadRecord.id);
        }else{
            newAccountRecord= JVCO_LeadMappingLogic.createLicenceAccount(leadRecord.JVCO_Account_Name__c, null, leadRecord.id);        
        }     
        
        newAccountRecord.JVCO_Customer_Account__c = selectedCustomerAccountId; 
        newAccountRecord.ffps_custRem__Preferred_Communication_Channel__c='Email';
        newAccountRecord.c2g__codaOutputVatCode__c= defaultVatCode;
        
        system.debug('**** Check Duplicate Licence Account:' + newAccountRecord);        
        Database.SaveResult saveResult = Database.insert(newAccountRecord, false);
        system.debug('**** Save REsult:' + saveResult);
        if (!saveResult.isSuccess()) {        
            Set<Id> duplicateIds = new Set<Id>();
            duplicateLicenceAccountList = new List<sObject>();
            
            for (Database.Error error : saveResult.getErrors()) 
            {             
                if (error instanceof Database.DuplicateError) 
                {
                    System.debug('***** Duplicate Error in Account');
                    Database.DuplicateError duplicateError = (Database.DuplicateError)error;
                    Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                    
                    // Display duplicate error message as defined in the duplicate rule
                    ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Duplicate Error: ' + duplicateResult.getErrorMessage()));

                    // Return only match results of matching rules that find duplicate records
                    Datacloud.MatchResult[] matchResults = duplicateResult.getMatchResults();

                    // Just grab first match result (which contains the duplicate record found and other match info)
                    Datacloud.MatchResult matchResult = matchResults[0];
                    Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();

                    // Add matched record to the duplicate records variable
                    for (Datacloud.MatchRecord matchRecord : matchRecords) {
                        this.duplicateLicenceAccountList.add(matchRecord.getRecord());
                        system.debug('**** MatchRecord Name:' + matchRecord.getRecord());
                        duplicateIds.add(matchRecord.getRecord().Id);
                    }
                    
                }
            }
                        
            hasDuplicateLicenceAccount= !duplicateLicenceAccountList .isEmpty();
            if (!duplicateIds.isEmpty()){
                String recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
                duplicateLicenceAccountList = Database.Query(accountQuery + ' WHERE Id IN:duplicateIds AND RecordTypeId=:recTypeId AND JVCO_Customer_Account__c=:selectedCustomerAccountId');    
            }            
        }                
        //Rollback to the previous transaction
        Database.rollback(sp);     
        return null;    
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used to validate duplicate Customer Account records. 
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-Aug-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/        
    public PageReference validateDuplicateCustomerAccountRecords(){
        
        //Create Savepoint
        Savepoint sp = Database.setSavepoint();        
        Account newAccountRecord= JVCO_LeadMappingLogic.createAccount(leadRecord, null);        
        Database.SaveResult saveResult = Database.insert(newAccountRecord, false);  
        
        if (!saveResult.isSuccess()) {
        
            Set<Id> duplicateIds = new Set<Id>();
            duplicateCustomerAccountList = new List<sObject>();
            for (Database.Error error : saveResult.getErrors()) 
            {             
                if (error instanceof Database.DuplicateError) 
                {
                    System.debug('***** Duplicate Error in Account');
                    Database.DuplicateError duplicateError = (Database.DuplicateError)error;
                    Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                    
                    // Display duplicate error message as defined in the duplicate rule
                    ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Duplicate Error: ' + duplicateResult.getErrorMessage()));

                    // Return only match results of matching rules that find duplicate records
                    Datacloud.MatchResult[] matchResults = duplicateResult.getMatchResults();

                    // Just grab first match result (which contains the duplicate record found and other match info)
                    Datacloud.MatchResult matchResult = matchResults[0];
                    Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();
                    
                    // Add matched record to the duplicate records variable
                    for (Datacloud.MatchRecord matchRecord : matchRecords) {
                        this.duplicateCustomerAccountList .add(matchRecord.getRecord());                        
                        duplicateIds.add(matchRecord.getRecord().Id);
                    }                    
                }
            }
            hasDuplicateCustomerAccount = !duplicateCustomerAccountList.isEmpty();
            if (!duplicateIds.isEmpty()){
                String recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
                duplicateCustomerAccountList = Database.Query(accountQuery + ' WHERE Id IN:duplicateIds AND RecordTypeId=:recTypeId');    
            }
        }                
        //Rollback to the previous transaction
        Database.rollback(sp);     
        return null;    
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used to call other method to validate Contact records. 
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-Aug-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public Pagereference validateDuplicateContactRecords(){
        
        //Create Savepoint
        Savepoint sp = Database.setSavepoint();     
        Contact newContactRecord= JVCO_LeadMappingLogic.createContact(leadRecord, null);         
        
        if(selectedCustomerAccountId  != null && selectedCustomerAccountId  != ''){
            newContactRecord.AccountId = selectedCustomerAccountId; 
        }
        
        Database.SaveResult saveResult = Database.insert(newContactRecord, false);                   
        if (!saveResult.isSuccess()) {
        
            Set<Id> duplicateIds = new Set<Id>();            
            duplicateContactList = new List<sObject>();
            for (Database.Error error : saveResult.getErrors()) 
            {             
                if (error instanceof Database.DuplicateError) 
                {
                    System.debug('***** Duplicate Error in Contact');
                    Database.DuplicateError duplicateError = (Database.DuplicateError)error;
                    Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                    
                    // Display duplicate error message as defined in the duplicate rule
                    ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Duplicate Error: ' + duplicateResult.getErrorMessage()));
                    
                    Datacloud.MatchResult[] matchResults = duplicateResult.getMatchResults();
                    Datacloud.MatchResult matchResult = matchResults[0];
                    Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();

                    // Add matched record to the duplicate records variable
                    for (Datacloud.MatchRecord matchRecord : matchRecords) 
                    {
                        this.duplicateContactList.add(matchRecord.getRecord());                        
                        duplicateIds.add(matchRecord.getRecord().id);
                    }                    
                }
            }
            hasDuplicateContactRecord = !duplicateContactList.isEmpty();                    
            if (!duplicateIds.isEmpty()){
                duplicateContactList = Database.Query(contactQuery + ' WHERE Id IN:duplicateIds AND AccountId=:selectedCustomerAccountId');    
            }
        }                
        //Rollback to the previous transaction
        Database.rollback(sp);     
        return null;
    }
    
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used to call other method to validate Venue records. 
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-Aug-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public PageReference validateDuplicateVenueRecords(){
        
        //Create Savepoint
        Savepoint sp = Database.setSavepoint();   
        
        JVCO_Venue__c newVenueRecord= JVCO_LeadMappingLogic.createVenue(leadRecord,selectedVenueId);        
        Database.SaveResult saveResult = Database.insert(newVenueRecord, false);
        
        if (!saveResult.isSuccess()) {
            for (Database.Error error : saveResult.getErrors()) 
            {             
                if (error instanceof Database.DuplicateError) 
                {
                    System.debug('***** Duplicate Error in Venue');
                    Database.DuplicateError duplicateError = (Database.DuplicateError)error;
                    Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                    
                    // Display duplicate error message as defined in the duplicate rule
                    ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Duplicate Error: ' + duplicateResult.getErrorMessage()));
                    
                    duplicateVenueList = new List<sObject>();

                    Datacloud.MatchResult[] matchResults = duplicateResult.getMatchResults();
                    Datacloud.MatchResult matchResult = matchResults[0];
                    Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();

                    Set<Id> duplicateIds = new Set<Id>();
                    // Add matched record to the duplicate records variable
                    for (Datacloud.MatchRecord matchRecord : matchRecords) 
                    {                        
                        this.duplicateVenueList.add(matchRecord.getRecord());
                        duplicateIds.add(matchRecord.getRecord().Id);
                    }                    
                    this.hasDuplicateVenueRecord = !duplicateVenueList.isEmpty();
                    if (!duplicateIds.isEmpty()){                        
                        duplicateVenueList = Database.Query(venueQuery + ' WHERE Id IN: duplicateIds' );    
                    }
                }
            }
        }                
        //Rollback to the previous transaction
        Database.rollback(sp);        
        return null;
    }
    
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is where the actual LeadConversion is happening.
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-Aug-2016     Accenture-joseph.g.barrameda         Initial version of the code
    30-May-2017     Accenture-joseph.g.barrameda         Added logic that populates the Dimension1 based from the Venue Type
    03-Jul-2017     Accenture-joseph.g.barrameda         Added logic that populates the Licence Account Preferred Communication Channel to Email 
    07-Dec-2017     filip.bezak@accenture.com            GREEN-26844 - Forwarding Customer Account Type to Licence Account
    ----------------------------------------------------------------------------------------------------------*/
    public Pagereference convertLead(){
                
        //Create Savepoint
        Savepoint sp = Database.setSavepoint();  
        //Lead Query
        leadRecord = Database.query(leadQuery);
        
        //Will be removed after debugging        
        System.debug('***** selectedLicenceAccountId  before Conversion:' + selectedLicenceAccountId );
        System.debug('***** selectedCustomerAccountId before Conversion:'  + selectedCustomerAccountId);
        System.debug('***** SelectedContactId before Conversion:' + selectedContactId);
        System.debug('***** SelectedVenueId before Conversion:' + selectedVenueID);
                      
        try{             
            //Manually create an Account record prior to Lead Conversion for record association 
            Account accountRecord = JVCO_LeadMappingLogic.createAccount(leadRecord, null);                    
            
            //GREEN-6708
            if (leadRecord.JVCO_Key_Account__c == true){
                accountRecord.Type = 'Key Account'; 
            }else{
                accountRecord.Type = 'Customer';    
            }
            
            //GREEN-23884
            if (leadRecord.JVCO_Sole_Trader__c){
                accountRecord.JVCO_Business_Type__c='Sole Trader';
            }
                    
            //Added 05/30/2017 
            String reportingCode='';
            
            reportingCode= [SELECT ID, JVCO_Dimension_Reporting_Code__c FROM JVCO_Budget_Category_Dimension_Mapping__c WHERE JVCO_Venue_Type_Label__c =: leadRecord.JVCO_Venue_Type__c].JVCO_Dimension_Reporting_Code__c;

            //Query            
            c2g__codaDimension1__c dimensionRec = [SELECT ID from c2g__codaDimension1__c WHERE c2g__ReportingCode__c=: reportingCode LIMIT 1];
            
            if (dimensionRec != null){
                accountRecord.c2g__CODADimension1__c = dimensionRec.id;
            }
                                                
            if (selectedCustomerAccountId == null){         
                bypassDuplicateThenInsert(accountRecord);
                System.debug('+++++ Create New Customer Account'); 
            } else {    
                accountRecord = [SELECT ID, Name, Type from Account WHERE ID=:selectedCustomerAccountId];
            }   
            
            system.debug('***** Customer Account Record:' + accountRecord);
            
            //Process Contact
            Contact contactRecord = new Contact();            
            if (selectedCustomerAccountId == null && selectedContactId == null){     
                //Create New Contact and associate the New created Account
                contactRecord = JVCO_LeadMappingLogic.createContact(leadRecord, null);
                contactRecord.AccountId = accountRecord.id;
                bypassDuplicateThenInsert(contactRecord);
                System.debug('+++++ Created New Contact');
            } 
            else if(selectedCustomerAccountId != null && selectedContactId == null)
            { 
                //Create New Contact and associate existing selected Account
                contactRecord = JVCO_LeadMappingLogic.createContact(leadRecord, selectedContactId);    
                contactRecord.AccountId = selectedCustomerAccountId;            
                bypassDuplicateThenInsert(contactRecord); 
            }
            else if(selectedCustomerAccountId != null && selectedContactId != null){
                contactRecord = [SELECT Id, AccountId FROM Contact WHERE ID=:selectedContactId];
                contactRecord.AccountId = selectedCustomerAccountId; 
                bypassDuplicateThenUpdate(contactRecord);
            }
            
            //Create New Licence Account
            Account licenceAcct = new Account();

            if(selectedLicenceAccountId == null){
                licenceAcct = JVCO_LeadMappingLogic.createLicenceAccount(accountRecord.Name, contactRecord.id, leadRecord.id);            
                licenceAcct.JVCO_Customer_Account__c = accountRecord.id;             //Associate the created "Customer Account"            
                licenceAcct.Type = accountRecord.Type;                               //Forward Customer Account Type to Licence Account
                
                licenceAcct.c2g__CODAOutputVATCode__c = defaultVatCode;              //Added as fix - 7/27/2017

                //Populate the Dimension 1 upon Lead Conversion 
                if (dimensionRec != null){
                    licenceAcct.c2g__CODADimension1__c = dimensionRec.id;
                }
                
                bypassDuplicateThenInsert(licenceAcct);
                system.debug('+++++ Created New Licence Account:' + licenceAcct);
            }
            else{
                licenceAcct = [SELECT Id, RecordTypeId FROM Account WHERE Id=: selectedLicenceAccountId];            
            }
            
            JVCO_Venue__c venueRecord;            
            //Create Venue and Affiliation if the Activity Based Licence is unchecked 
            if (leadRecord.JVCO_Activity_Based_Licence__c == false){
                //Process Venue
                venueRecord = JVCO_LeadMappingLogic.createVenue (leadRecord, selectedVenueId);
                if (selectedVenueId == null){               
                    this.bypassDuplicateThenInsert(venueRecord);
                    system.debug('++++++ Created New Venue');
                } else { 
                    venueRecord = [SELECT Id FROM JVCO_Venue__c WHERE ID=: selectedVenueId];
                    //bypassDuplicateThenUpdate(venueRecord); 
                }   
                
                //Process Affiliation
                system.debug('**** Licence Acount:' + licenceAcct);
                if (licenceAcct.id != null){
                    JVCO_Affiliation__c affiliationRecord = JVCO_LeadMappingLogic.createAffiliation(leadRecord, licenceAcct.id, venueRecord.id);
                    system.debug('++++++ Created New Affiliation');
                }
            }
                            
            //Task Reassignment
            if (venueRecord != null){
                reassignActivity(leadRecord.id, contactRecord.id, venueRecord.id);
            }
            else{
                reassignActivity(leadRecord.id, contactRecord.id, null);
            }
            
            // Create LeadConvert object
            Database.LeadConvert lc = new Database.LeadConvert();
            
            System.debug('**** AccountRecord:' + accountRecord.Id);
            System.debug('**** Contactrecord:' + contactRecord.id);
                                
            lc.setLeadId(leadRecord.Id);                     
            lc.setAccountId(accountRecord.Id);             
            lc.setContactId(contactRecord.Id);             
            lc.setOwnerId(Userinfo.getUserId());   
            
            lc.setDoNotCreateOpportunity(!isRedirectToOpp);
            
            if(isRedirectToOpp){
                lc.setOpportunityName(leadRecord.JVCO_Account_Name__c);            
            }
                        
            // Set Lead Converted Status
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            
            // Convert
            Database.LeadConvertResult lcr = Database.convertLead(lc);     
            
            if(lcr.isSuccess()){ 
                
                //Redirect Page           
                Pagereference pr;
                if(isRedirectToOpp == true){ 
                    //Update Opp Stage to Quoting
                    Opportunity oppCreated = [SELECT Id, StageName FROM Opportunity WHERE ID=: lcr.getOpportunityId()];
                    oppCreated.StageName = 'Quoting';
                    oppCreated.AccountId = licenceAcct.id;                    
                    oppCreated.LeadSource = leadRecord.LeadSource;
                    oppCreated.Type = 'New Business'; //GREEN-20166
                    update oppCreated;      
                    
                    // START    Raus Ablaza     12/09/2016      Assign Quote to Id holders for creation of Quote
                    convertAccountId = licenceAcct.id;
                    convertContactId = lcr.getContactId();
                    convertOpportunityId = lcr.getOpportunityId();
                    
                    system.debug('***@@@ id values: ' + convertAccountId + ', ' + convertContactId + ', ' + convertOpportunityId);
                    
                    // Checks if all Ids have value
                    if( (String.isBlank(convertAccountId)) || (String.isBlank(convertContactId)) || (String.isBlank(convertOpportunityId)) ){
                        system.debug('***@@@ entered error checking');
                        hasErrorInConversion = true;
                    }
                    // END      Raus Ablaza     12/09/2016
                }
                else{
                    pr = new pagereference('/' + lcr.getAccountId());
                    pr.setRedirect(true);
                    return pr; 
                }
            }else{
                Database.rollBack(sp);
            }            
            hasErrorInConversion = false;
            // return null;
        } catch(Exception ex)            
        {        
            hasErrorInConversion = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
            Database.rollback(sp);            
        }
        
        // START    Raus Ablaza     12/09/2016      Creation of Quote
        if(isRedirectToOpp && !hasErrorInConversion){
            system.debug('***@@@ enetered quote creation');
            Pagereference pr = Page.SBQQ__sb;
            SBQQ__Quote__c quoteRecord = new SBQQ__Quote__c();
            
            system.debug('***@@@ creating quote');
            quoteRecord = JVCO_LeadMappingLogic.createQuote(convertAccountId, convertContactId, convertOpportunityId); 
            system.debug('***@@@ inserting quote');
            insert quoteRecord;
            system.debug('***@@@ quote created');
            
            //system.debug('***@@@ header debug: ' + 'https://sbqq' + ApexPages.currentPage().getHeaders().get('Host').substring(1) + '/apex/sb?id=' + quoteRecord.Id + '&scontrolCaching=1#quote/le?qId=' + quoteRecord.Id);
            //pr = new pagereference('https://sbqq' + ApexPages.currentPage().getHeaders().get('Host').substring(1) + '/apex/sb?id=' + quoteRecord.Id + '&scontrolCaching=1#quote/le?qId=' + quoteRecord.Id);
            pr.getParameters().put('id', quoteRecord.Id);
            pr.getParameters().put('qId', quoteRecord.Id);

            return pr;
        }
        // END      Raus Ablaza     12/09/2016
        
        return null;
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used to redirect the page to LeadConversionFinal page. 
    Inputs:         None
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-Aug-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
   
    public PageReference nextPage(){
    
        System.debug('**** Selected Licence AccountID:' + selectedLicenceAccountId);
        System.debug('**** Selected Customer AccountID:' + selectedCustomerAccountId);
        System.debug('**** SElected Contact Id:' + selectedContactId);
        System.debug('**** Selected Venue Id:' + selectedVenueId);
        
        //Clear variables 
        if(IsCreateNewLicenceAccount == true) selectedLicenceAccountId  = null;
        if(isCreateNewCustomerAccount == true) selectedCustomerAccountId = null;
        if(isCreateNewContact == true) selectedContactId = null;
        if(isCreateNewVenue == true) selectedVenueId = null;
        
        PageReference next=Page.JVCO_LeadConversionFinal;        
        next.getParameters().put('licAccountIdParam',selectedLicenceAccountId);
        next.getParameters().put('cusAccountIdParam',selectedCustomerAccountId);
        next.getParameters().put('contactIdParam',selectedContactId);
        next.getParameters().put('venueIdParam',selectedVenueId);
        
        next.setRedirect(false);
        return next;
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used to ignore the Duplicate Rule when inserting an Account or Contact or Venue record. 
    Inputs:         sObject
    Returns:        Database.SaveResult
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-Aug-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/    
    public Database.SaveResult bypassDuplicateThenInsert (sObject obj){
        
        Database.DMLOptions dml = new Database.DMLOptions(); 
        dml.DuplicateRuleHeader.AllowSave = true;
        Database.SaveResult sr = Database.insert(obj, dml);        
        return sr;
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used to ignore the Duplicate Rule when updating an Account or Contact or Venue record. 
    Inputs:         sObject
    Returns:        Database.SaveResult
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-Aug-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/    
    public Database.SaveResult bypassDuplicateThenUpdate (sObject obj){
        
        Database.DMLOptions dml=new Database.DMLOptions();
        dml.DuplicateRuleHeader.AllowSave = true;
        Database.SaveResult ur = database.update(obj, dml);
        return ur; 
    }

    public void getLicenceAccountId(){
        selectedLicenceAccountId  = apexpages.currentpage().getparameters().get('licAccountIdParam');                
    }
    
    public void getCustomerAccountId(){
        selectedCustomerAccountId  = apexpages.currentpage().getparameters().get('cusAccountIdParam');
        validateDuplicateLicenceAccountRecords();
        validateDuplicateContactRecords();
    }
        
    public void getContactId(){
        selectedContactId = apexpages.currentpage().getparameters().get('contactIdParam');        
    }
    
    public void getVenueId(){
        selectedVenueId = apexpages.currentpage().getparameters().get('venueIdParam');        
    }
    
    public void clearDuplicateList(){
        
        selectedLicenceAccountId =null;
        isCreateNewLicenceAccount =true;
        isCreateNewContact=true;
        
        //Clear Contact List         
        if(duplicateContactList != null){
            duplicateContactList.clear();
        }   
        //Clear Licence Account
        if(duplicateLicenceAccountList != null){
            duplicateLicenceAccountList.clear();        
        }
    }
    
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         joseph.g.barrameda
    Company:        Accenture
    Description:    This method is used to reassign Task and Event associated to Lead upon conversion. 
    Inputs:         Lead.Id , Contact.Id, Venue.Id
    Returns:        None
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-Aug-2016     Accenture-joseph.g.barrameda         Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/
    public void reassignActivity(Id leadId, Id contactId, Id venueId){
        
        List<Task>  updateTaskList = new List<Task>();
        List<Event> updateEventList = new List<Event>();
        
        List<Task>  taskList  =[SELECT Id, WhoId, WhatId FROM Task WHERE WhoId=:leadId];
        List<Event> eventList =[SELECT Id, WhoId, WhatId FROM Event WHERE WhoId=:leadId];
        
        for(Task t: taskList){
            t.WhoId = contactId; 
            if(venueId != null){
                t.WhatId  = venueId; 
            }
            updateTaskList.add(t);
        }
        
        if(!updateTaskList.isEmpty()){
            update updateTaskList;
        }
        
        for(Event e: eventList){
            e.WhoId = contactId;
            if(venueId != null){ 
                e.WhatId  = venueId;
            }
            updateEventList.add(e);
        }
        
        if(!updateEventList.isEmpty()){
            update updateEventList;        
        }        
    }
    
}