/* ----------------------------------------------------------------------------------------------------------
Name:            JVCO_EventTriggerHandler.cls 
Description:     Trigger handler for JVCO_EventTrigger.trigger
Test class:      

Date             Version     Author                            Summary of Changes 
-----------      -------     -----------------                 -------------------------------------------
16-Sep-2017      0.1        Accenture-r.p.valencia.iii         Initial version of code
11-Oct-2017      0.2        Accenture-reymark.j.l.arlos        Refactored concatEventName
25-Oct-2017      0.3        Accenture-rhys.j.c.dela.cruz       GREEN-20955 Enhancement - Duplicate Event
31-Oct-2017      0.4        Accenture-r.p.valencia.iii         Refactored concatEventName - GREEN-25347
24-Jan-2018		 0.5		Accenture-apple.j.a.manayaga	   GREEN-28077 reqDateNull method added 
---------------------------------------------------------------------------------------------------------- */

public class JVCO_EventTriggerHandler {
    
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_EventTrigger__c : false;


    public static void onBeforeInsert(List<JVCO_Event__c> evtList){
        if(!skipTrigger) 
        {
            concatEventName(evtList);
            startDateCopy(evtList);
            reqDateNull(evtList);
        }
    }
    
    public static void onAfterInsert(List<JVCO_Event__c> evtList){
        if(!skipTrigger) 
        {
            //trigger code
        }
    }
    
    public static void onBeforeUpdate(List<JVCO_Event__c> evtList, Map<Id, JVCO_Event__c> oldEvtMap){
        if(!skipTrigger) 
        {
            concatEventName(evtList);
            startDateCopy(evtList);
            reqDateNull(evtList);
        }
    }
    
    public static void onAfterUpdate(List<JVCO_Event__c> evtList, Map<Id, JVCO_Event__c> oldEvtMap){
        if(!skipTrigger) 
        {
            //trigger code
        }
    }

    public static void concatEventName(List<JVCO_Event__c> evtList){

        Set<Id> evtIdSet = new Set<Id>();
        Set<Id> evtIdSet2 = new Set<Id>();
        Map<Id, JVCO_Venue__c> venMap = new Map<Id, JVCO_Venue__c>();
        Map<Id, JVCO_Artist__c> artMap = new Map<Id, JVCO_Artist__c>();
        string evtName;

        for(JVCO_Event__c evt: evtList)
        {
            if(!String.isBlank(evt.JVCO_Artist__c))
            {
                evtIdSet.add(evt.JVCO_Artist__c);
            }
            if(!String.isBlank(evt.JVCO_Venue__c))
            {
                evtIdSet2.add(evt.JVCO_Venue__c);
            }
        }

        if(!evtIdSet.isEmpty())
        {
            artMap = new Map<Id, JVCO_Artist__c>([SELECT Id, Name FROM JVCO_Artist__c WHERE Id in: evtIdSet]);
        }

        if(!evtIdSet2.isEmpty())
        {
            venMap = new Map<Id, JVCO_Venue__c>([SELECT Id, Name FROM JVCO_Venue__c WHERE Id in: evtIdSet2]);
        }

        for(JVCO_Event__c evnt : evtList)
        {
            if(evnt.JVCO_Event_Start_Date__c != NULL)
            {
                evtName = string.valueof(evnt.JVCO_Event_Start_Date__c.Day()) + '/' + string.valueof(evnt.JVCO_Event_Start_Date__c.Month()) + '/' + string.valueof(evnt.JVCO_Event_Start_Date__c.Year());
            }

            if(artMap.containsKey(evnt.JVCO_Artist__c))
            {
                if(!String.isBlank(artMap.get(evnt.JVCO_Artist__c).Name))
                {
                    evtName += '-' + artMap.get(evnt.JVCO_Artist__c).Name;
                }
            }

            if(venMap.containsKey(evnt.JVCO_Venue__c))
            {
                if(!String.isBlank(venMap.get(evnt.JVCO_Venue__c).Name))
                {
                    evtName += '-' + venMap.get(evnt.JVCO_Venue__c).Name;
                }
            }

            evnt.Name = evtName.abbreviate(80);

            //Event Override Part
            if(evnt.JVCO_Event_Name_Override__c != NULL)
            {
                evnt.Name = evnt.JVCO_Event_Name_Override__c.abbreviate(80);
            }
        }       
    }

    //Copies Event Start Date to String Start Date (JVCO_EventStDate_Text__c)
    public static void startDateCopy(List<JVCO_Event__c> evtList){

        if(!evtList.isEmpty()){
            for(JVCO_Event__c event: evtList){
                event.JVCO_EventStDate_Text__c = string.valueof(event.JVCO_Event_Start_Date__c.Day()) + '/' + string.valueof(event.JVCO_Event_Start_Date__c.Month()) + '/' + string.valueof(event.JVCO_Event_Start_Date__c.Year());
            }
        }
    }
    
    //if required date is null and status is requested, default required date to today()
    public static void reqDateNull(List<JVCO_Event__c> evtList)
    {
        for(JVCO_Event__c event: evtList)
        {
        	if (event.JVCO_Event_Classification_Status__c == 'Requested' && event.JVCO_Classification_Required_By_Date__c == NULL)
            {
				event.JVCO_Classification_Required_By_Date__c = date.today() + 7;               
            }
        }
    }
}