/* -------------------------------------------------------------------------------------
    Name: JVCO_AffiliationTriggerHandlerTest
    Description: Test Class for JVCO_AffiliationTriggerHandler

    Date            Version     Author                Summary of Changes 
    -----------     -------     -----------------     ----------------------------------
    ---         0.1         unknown         Intial creation
    07/07/2018      0.2         jules.osberg.a.pablo  Created test methods for different Affiliation scenarios
------------------------------------------------------------------------------------- */
@isTest
private class JVCO_AffiliationTriggerHandlerTest
{
  @testSetup
  public static void testSetup(){
    Profile profile = [SELECT Id FROM Profile WHERE Name='Premier Support User'];
      
    User u = createUser(profile.Id, 1);
    insert u; 

    JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

    Id venueId;
    Id licAccountId;

    system.runAs(u){
      SBQQ.TriggerControl.disable();
      Account customerAccount = JVCO_TestClassObjectBuilder.createCustomerAccount();
      insert customerAccount;

      Contact c = new Contact();
      String uniq = String.valueOf(Math.random());
      c.LastName = 'Doe' + uniq;
      c.FirstName = 'John' + uniq;
      c.Email = 'surveyAppUser@hotmail.com';
      c.AccountId = customerAccount.Id;
      c.MailingStreet = '10 Downing Street';
      c.MailingPostalCode = 'SW1A 2AA';
      insert c;


      Account a = new Account();
      a.Name = 'Account 1'; 
      a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId(); 
      a.JVCO_Customer_Account__c = customerAccount.Id;
      a.JVCO_Review_Type__c = 'Automatic';
      a.Type = 'Key Account';
      a.JVCO_Review_Frequency__c  = 'Annually';
      a.JVCO_Preferred_Contact_Method__c = 'Email';
      a.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
      a.JVCO_Billing_Contact__c = c.id;
      a.JVCO_Licence_Holder__c = c.id;
      a.JVCO_Review_Contact__c = c.id;
      insert a;
      licAccountId = a.id;

      JVCO_Venue__c testVenue2 = new JVCO_Venue__c();
      testVenue2.name = 'Test 2213213';
      testVenue2.JVCO_Email__c = 'JVCO_QuoteLineTriggerHandlerTest2@email.com';
      testVenue2.External_Id__c = 'JVCO_QLTHTest2samplevenexid';
      testVenue2.JVCO_Street__c = 'Test2 Street';
      testVenue2.JVCO_City__c = 'Test2 City';
      testVenue2.JVCO_Country__c = 'Test2 Country';
      testVenue2.JVCO_Postcode__c = 'TN32 5SL';
      insert testVenue2;
      venueId = testVenue2.id;
      test.StartTest();

      JVCO_Affiliation__c a1 = new JVCO_Affiliation__c();
      a1.JVCO_Account__c = licAccountId;
      a1.JVCO_Venue__c = venueId;
      a1.JVCO_Start_Date__c = date.today();
      a1.JVCO_Status__c = 'Trading On Hold';
      a1.JVCO_Omit_Quote_Line_Group__c = false;
      a1.JVCO_Revenue_Type__c = 'New Business';
      insert a1;
      
      Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
          insert p;

      Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
          insert pb2;

          PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
          insert pbe;

      Opportunity opp = JVCO_TestClassHelper.setOpportunity(a.Id);
          insert opp;

          SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
          insert qProc;

          SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(a.Id, opp.Id);
          q.JVCO_Cust_Account__c = customerAccount.Id;
          insert q;

          SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
          ql.Affiliation__c = a1.id;
          //ql.JVCO_Event__c = event.id;
          ql.JVCO_Venue__c = testVenue2.id;
          insert ql;

      Contract contr = JVCO_TestClassHelper.setContract(a.Id, opp.Id, q.Id);
          insert contr;

          SBQQ__Subscription__c s = JVCO_TestClassObjectBuilder.createSubscription(a.id, p.id);
        s.SBQQ__Contract__c = contr.Id;
        s.Affiliation__c = a1.Id;
        s.SBQQ__QuoteLine__c = ql.Id;
        s.SBQQ__RenewalQuantity__c = 1;
        insert s;
        SBQQ.TriggerControl.enable();
    }

      JVCO_Primary_Tariff_Mapping__c setting = new JVCO_Primary_Tariff_Mapping__c(Name = 'Hairdresser/Beauty Salon', JVCO_Primary_Tariff__c = 'HDB_PRS');
    insert setting;

    JVCO_BipassPermissionForEnforcement__c arCS = JVCO_BipassPermissionForEnforcement__c.getOrgDefaults();
        arCS.JVCO_BipassEnforcementVRonAffiliation__c = false;
        upsert arCS JVCO_BipassPermissionForEnforcement__c.Id;
  }

  private static testMethod void testAffiliationInsertNotSysAd() {
    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

      Account licenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT LIMIT 1];

      Profile p = [SELECT Id FROM Profile WHERE Name='Premier Support User'];

      User u = createUser(p.Id, 2);
      insert u;

      JVCO_Affiliation__c checkAff = new JVCO_Affiliation__c();

      test.StartTest();
      system.runAs(u){
      JVCO_Venue__c testVenue = JVCO_TestClassObjectBuilder.createVenue();
      testVenue.name = 'Test 1';
      insert testVenue;

        JVCO_Affiliation__c newAffil = JVCO_TestClassObjectBuilder.createAffiliation(licenceAccount.Id, testVenue.Id);
        newAffil.JVCO_End_Date__c = null; 
        newAffil.JVCO_Closure_Reason__c = null;         
        insert newAffil;

        checkAff = [SELECT Id, JVCO_Venue__r.Name FROM JVCO_Affiliation__c WHERE Id = :newAffil.Id LIMIT 1];
        
      }
      System.assertNotEquals(null, checkAff);

      test.StopTest();
  }

  private static testMethod void testAffiliationInsertVenueEnforceNotSysAd() {
    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

      Account licenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT LIMIT 1];
      
      Account licenceAccount2 = JVCO_TestClassObjectBuilder.createLicenceAccount(licenceAccount.JVCO_Customer_Account__c);
      insert licenceAccount2;

      Profile p = [SELECT Id FROM Profile WHERE Name='Premier Support User'];

      User u = createUser(p.Id, 2);
      insert u;

      JVCO_Venue__c testVenue = JVCO_TestClassObjectBuilder.createVenue();
    testVenue.name = 'Test 1';
    insert testVenue;

    JVCO_Affiliation__c newAffil = JVCO_TestClassObjectBuilder.createAffiliation(licenceAccount.Id, testVenue.Id);  
    newAffil.JVCO_End_Date__c = null; 
    newAffil.JVCO_Closure_Reason__c = null;       
    insert newAffil;

    licenceAccount.JVCO_In_Enforcement__c = TRUE;
    update licenceAccount;

    Boolean expectedExceptionThrown = false;

      test.StartTest();

      try{
          system.runAs(u){        
            JVCO_Affiliation__c newAffil2 = JVCO_TestClassObjectBuilder.createAffiliation(licenceAccount2.Id, testVenue.Id);   
            newAffil2.JVCO_End_Date__c = null; 
            newAffil2.JVCO_Closure_Reason__c = null; 
            newAffil2.Name = 'Test';      
            insert newAffil2;   
          }
    }
    catch(Exception e){
      expectedExceptionThrown =  e.getMessage().contains('You cannot create an affiliation if the Venue is related to a Licence Account in Enforcement') ? true : false;      
    } 

    System.AssertEquals(expectedExceptionThrown, true);

      test.StopTest();
  }

  private static testMethod void testAffiliationInsertVenueBipassEnforceNotSysAd() {
    JVCO_BipassPermissionForEnforcement__c arCS = JVCO_BipassPermissionForEnforcement__c.getOrgDefaults();
      arCS.JVCO_BipassEnforcementVRonAffiliation__c = true;
      upsert arCS JVCO_BipassPermissionForEnforcement__c.Id;

    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

      Account licenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT LIMIT 1];
      
      Account licenceAccount2 = JVCO_TestClassObjectBuilder.createLicenceAccount(licenceAccount.JVCO_Customer_Account__c);
      insert licenceAccount2;

      Profile p = [SELECT Id FROM Profile WHERE Name='Premier Support User'];

      User u = createUser(p.Id, 2);
      insert u;

      JVCO_Venue__c testVenue = JVCO_TestClassObjectBuilder.createVenue();
    testVenue.name = 'Test 1';
    insert testVenue;

    JVCO_Affiliation__c newAffil = JVCO_TestClassObjectBuilder.createAffiliation(licenceAccount.Id, testVenue.Id); 
    newAffil.JVCO_End_Date__c = null;   
    newAffil.JVCO_Closure_Reason__c = null;       
    insert newAffil;

    licenceAccount.JVCO_In_Enforcement__c = TRUE;
    update licenceAccount;

      test.StartTest();
      JVCO_Affiliation__c newAffil2 = new JVCO_Affiliation__c();

      system.runAs(u){        
        newAffil2 = JVCO_TestClassObjectBuilder.createAffiliation(licenceAccount2.Id, testVenue.Id);  
        newAffil2.JVCO_End_Date__c = null; 
        newAffil2.JVCO_Closure_Reason__c = null;  
        newAffil2.Name = 'Test';      
        insert newAffil2;   
      }
    
    JVCO_Affiliation__c checkAff = [SELECT Id, JVCO_Venue__r.Name FROM JVCO_Affiliation__c WHERE Id = :newAffil2.Id LIMIT 1];

    System.AssertNotEquals(null, newAffil2);

      test.StopTest();
  }

  private static testMethod void testAffiliationInsertNotSysAdLongAffName() {
    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

      Account licenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT LIMIT 1];

      Profile p = [SELECT Id FROM Profile WHERE Name='Premier Support User'];

      User u = createUser(p.Id, 2);
      insert u;

      JVCO_Affiliation__c checkAff = new JVCO_Affiliation__c();

      test.StartTest();
      system.runAs(u){
      JVCO_Venue__c testVenue = JVCO_TestClassObjectBuilder.createVenue();
      testVenue.name = 'TestTestTestTestTestTestTestTestTestTestTestTestTestTestTest 1';
      insert testVenue;

        JVCO_Affiliation__c newAffil = JVCO_TestClassObjectBuilder.createAffiliation(licenceAccount.Id, testVenue.Id);
        newAffil.JVCO_End_Date__c = null; 
        newAffil.JVCO_Closure_Reason__c = null;         
        insert newAffil;

        checkAff = [SELECT Id, Name, JVCO_Venue__r.Name FROM JVCO_Affiliation__c WHERE Id = :newAffil.Id LIMIT 1];
        
        System.assertEquals(JVCO_AffiliationTriggerHandler.checkNameLength(licenceAccount.Name + ':' + testVenue.Name), checkNameLength(checkAff.Name));
      }
      

      test.StopTest();
  }

  private static testMethod void testExistingAffiliationInsertNotSysAd() {
    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

      Account licenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT LIMIT 1];

      Profile p = [SELECT Id FROM Profile WHERE Name='Premier Support User'];

      User u = createUser(p.Id, 2);
      insert u;

      JVCO_Venue__c testVenue = JVCO_TestClassObjectBuilder.createVenue();
    testVenue.name = 'Test 1';
    insert testVenue;

    Boolean expectedExceptionThrown = false;
    JVCO_Affiliation__c newAffil = new JVCO_Affiliation__c();
    JVCO_Affiliation__c newAffil2 = new JVCO_Affiliation__c();

      test.StartTest();

      try{
          system.runAs(u){   

        newAffil = JVCO_TestClassObjectBuilder.createAffiliation(licenceAccount.Id, testVenue.Id);
        newAffil.JVCO_End_Date__c = null;  
        newAffil.JVCO_Closure_Reason__c = null;   
        insert newAffil;
              
            newAffil2 = JVCO_TestClassObjectBuilder.createAffiliation(licenceAccount.Id, testVenue.Id);    
            newAffil2.JVCO_End_Date__c = null;
            newAffil2.JVCO_Closure_Reason__c = null; 
            newAffil2.Name = 'Test';      
            insert newAffil2;   
          }
    }
    catch(Exception e){
      expectedExceptionThrown =  e.getMessage().contains('Active affiliation record already exists for Account Id: ' + licenceAccount.Id + ' and Venue Id: ' + testVenue.Id + '.') ? true : false;  
      System.AssertEquals(expectedExceptionThrown, true);
    } 

      test.StopTest();
  }

  private static testMethod void testExistingAffiliationUpdateNotSysAd() {
    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

      Account licenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT LIMIT 1];
        
        Account licenceAccount2 = JVCO_TestClassObjectBuilder.createLicenceAccount(licenceAccount.JVCO_Customer_Account__c);
      insert licenceAccount2;

      Profile p = [SELECT Id FROM Profile WHERE Name='Premier Support User'];

      User u = createUser(p.Id, 2);
      insert u;

      JVCO_Venue__c testVenue = JVCO_TestClassObjectBuilder.createVenue();
    testVenue.name = 'Test 1';
    insert testVenue;

    Boolean expectedExceptionThrown = false;
    JVCO_Affiliation__c newAffil = new JVCO_Affiliation__c();
    JVCO_Affiliation__c newAffil2 = new JVCO_Affiliation__c();

      test.StartTest();

      try{
          system.runAs(u){   

        newAffil = JVCO_TestClassObjectBuilder.createAffiliation(licenceAccount.Id, testVenue.Id);
        newAffil.JVCO_End_Date__c = null;  
        newAffil.JVCO_Closure_Reason__c = null;   
        insert newAffil;
              
            newAffil2 = JVCO_TestClassObjectBuilder.createAffiliation(licenceAccount2.Id, testVenue.Id);    
            newAffil2.JVCO_End_Date__c = null;
            newAffil2.JVCO_Closure_Reason__c = null; 
            newAffil2.Name = 'Test';      
            insert newAffil2;   

            newAffil2.JVCO_Account__c = licenceAccount.Id;
            update newAffil2;
          }
    }
    catch(Exception e){
      expectedExceptionThrown =  e.getMessage().contains('Active affiliation record already exists for Account Id: ' + licenceAccount.Id + ' and Venue Id: ' + testVenue.Id + '.') ? true : false;  
      System.AssertEquals(expectedExceptionThrown, true);
    } 

      test.StopTest();
  }

  private static testMethod void testCloseAffiliationUpdateNotSysAd() {
    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

      Account licenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT LIMIT 1];
        
        Account licenceAccount2 = JVCO_TestClassObjectBuilder.createLicenceAccount(licenceAccount.JVCO_Customer_Account__c);
      insert licenceAccount2;

      Profile p = [SELECT Id FROM Profile WHERE Name='Premier Support User'];

      User u = createUser(p.Id, 2);
      insert u;

      JVCO_Venue__c testVenue = JVCO_TestClassObjectBuilder.createVenue();
    testVenue.name = 'Test 1';
    insert testVenue;

    Boolean expectedExceptionThrown = false;
    JVCO_Affiliation__c newAffil = new JVCO_Affiliation__c();
    JVCO_Affiliation__c newAffil2 = new JVCO_Affiliation__c();

      test.StartTest();

        system.runAs(u){   

      newAffil = JVCO_TestClassObjectBuilder.createAffiliation(licenceAccount.Id, testVenue.Id);
      newAffil.JVCO_End_Date__c = null;  
      newAffil.JVCO_Closure_Reason__c = null;   
      insert newAffil;            

          newAffil.JVCO_End_Date__c = date.today().addDays(20);  
      newAffil.JVCO_Closure_Reason__c = 'Dissolved';
          update newAffil;
        }
    
    JVCO_Affiliation__c checkAff = [SELECT Id, JVCO_End_Date__c, JVCO_Closure_Reason__c FROM JVCO_Affiliation__c WHERE Id = :newAffil.Id LIMIT 1];
        
      System.assertNotEquals(null, checkAff.JVCO_End_Date__c);  
      System.assertNotEquals(null, checkAff.JVCO_Closure_Reason__c);

      test.StopTest();
  }

  private static testMethod void testCloseAffiliationUpdateWithSubsNotSysAd() {
    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

      Account licenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT LIMIT 1];
        
      JVCO_Affiliation__c checkAff = [SELECT Id, JVCO_End_Date__c, JVCO_Closure_Reason__c FROM JVCO_Affiliation__c LIMIT 1];
      
      Profile p = [SELECT Id FROM Profile WHERE Name='Premier Support User'];

      User u = createUser(p.Id, 2);
      insert u;

      JVCO_Venue__c testVenue = JVCO_TestClassObjectBuilder.createVenue();
    testVenue.name = 'Test 1';
    insert testVenue;

    
      test.StartTest();

        system.runAs(u){   
          checkAff.JVCO_End_Date__c = date.today().addDays(20);  
      checkAff.JVCO_Closure_Reason__c = 'Dissolved';
          update checkAff;
        }
    
    SBQQ__Subscription__c checkSubs = [SELECT Id, SBQQ__RenewalQuantity__c FROM SBQQ__Subscription__c WHERE Affiliation__c = :checkAff.Id LIMIT 1];
        
      System.assertEquals(0, checkSubs.SBQQ__RenewalQuantity__c); 

      test.StopTest();
  }

  private static testMethod void testCloseAffiliationUpdatePrimaryTariffNotSysAd() {
    Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();

      Account licenceAccount = [SELECT Id, Name, JVCO_Live__c, JVCO_Customer_Account__c, Licence_Account_Number__c FROM Account WHERE RecordTypeId =: licenceRT LIMIT 1];
        
      //JVCO_Affiliation__c checkAff = [SELECT Id, JVCO_End_Date__c, JVCO_Closure_Reason__c FROM JVCO_Affiliation__c LIMIT 1];
      
      Profile p = [SELECT Id FROM Profile WHERE Name='Premier Support User'];

      User u = createUser(p.Id, 2);
      insert u;

      JVCO_Venue__c testVenue = JVCO_TestClassObjectBuilder.createVenue();
    testVenue.name = 'Test 1';
    testVenue.JVCO_Venue_Type__c = 'Hairdresser/Beauty Salon';
    insert testVenue;

    JVCO_Affiliation__c newAffil = new JVCO_Affiliation__c();
    
      test.StartTest();

        system.runAs(u){   
          newAffil = JVCO_TestClassObjectBuilder.createAffiliation(licenceAccount.Id, testVenue.Id);
          insert newAffil;
        }
    
    JVCO_Affiliation__c checkAff = [SELECT Id, JVCO_Venue__r.JVCO_Primary_Tariff__c, JVCO_End_Date__c, JVCO_Closure_Reason__c FROM JVCO_Affiliation__c WHERE Id = :newAffil.Id LIMIT 1];
    System.assertEquals('H&B_PRS', checkAff.JVCO_Venue__r.JVCO_Primary_Tariff__c);
      test.StopTest();
  }

  private static User createUser(Id profileId, Integer index){
    User u = new User(Alias = 'utest'+index, Email='unit.test'+index+'@unit.test.com',
                EmailEncodingKey='UTF-8', LastName='Unit Test'+index, 
                LanguageLocaleKey='en_US', LocaleSIdKey='en_GB', ProfileId = profileId,
                TimezoneSIdKey='Europe/London', Username='test.test'+index+'@unit.test.com', IsActive = true,
                Division='Legal', Department='Legal');

    return u;
  }

  public static String checkNameLength(String str){
    String reStr = '';

    if(str.length() > 80){
      reStr = str.substring(0,76) + '...';
    }
    else{
      reStr = str;
    }
    return reStr;
  }

  @isTest static void testreopenAffiliation(){

    SBQQ.TriggerControl.disable(); 
    JVCO_Affiliation__c newAffil = [SELECT Id, JVCO_Venue__r.JVCO_Primary_Tariff__c, JVCO_End_Date__c, JVCO_Closure_Reason__c FROM JVCO_Affiliation__c LIMIT 1];
 
    newAffil.JVCO_End_Date__c = date.today().addDays(20);  
    newAffil.JVCO_Closure_Reason__c = 'Dissolved'; 
    update newAffil;
 
    test.StartTest();

    newAffil.JVCO_End_Date__c = null;  
    newAffil.JVCO_Closure_Reason__c = '';
    update newAffil;
 
    test.StopTest();
  }
}