@isTest
private class SMP_PaymentScheduleHelperTest {
    private static Account account;
    private static Contact contact;

    private static void setupConfig()
    {
        String responseBody = '{"ProcessListNewDDFirstCollectionDateResult":[{"Error":"","FirstCollectionDate":"2019-01-06"}]}';
        
        HttpRestCallOutMock fakeResponse = new HttpRestCallOutMock(200, responseBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        account = new Account();

        insert account;

        contact = new Contact();
        contact.FirstName = 'TestFirst';
        contact.LastName = 'TestLast';
        insert contact;
    }
    @isTest static void testcheckDirectDebit() {

        List<RecordType> rtypesCENLIH = [SELECT Name, Id FROM RecordType WHERE sObjectType='JVCO_CashEntryLineItemHistory__c' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> RecordTypesCENLIH  = new Map<String,String>{};
        for(RecordType rt: rtypesCENLIH)
        {
          RecordTypesCENLIH.put(rt.Name,rt.Id);
        }
        List<CashEntryLineItemHistory_CS__c> settingsCENLIH = new List<CashEntryLineItemHistory_CS__c>();
        settingsCENLIH.add(new CashEntryLineItemHistory_CS__c(Name = 'Matched', Value__c = RecordTypesCENLIH.get('Matched Cash')));
        settingsCENLIH.add(new CashEntryLineItemHistory_CS__c(Name = 'Reversed', Value__c = RecordTypesCENLIH.get('Reversed Cash')));
        insert settingsCENLIH;
        
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        //Settings
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_codaCreditNoteTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        insert dt;
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c convGLA = JVCO_TestClassHelper.setGLA('CONV', testGroup.Id);
        glaList.add(convGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        c2g__codaDimension2__c dim2 = JVCO_TestClassHelper.setDim2('PPL');
        insert dim2;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        JVCO_SLA_Constants__c cs = JVCO_TestClassObjectBuilder.createSLAConstants();
        insert cs;

        Entitlement e = JVCO_TestClassObjectBuilder.createEntitlement(licAcc.id);
        insert e;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id, String.valueOf(Date.today().year()));
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Test.startTest();
        List<c2g__codaInvoice__c> sInvList = new List<c2g__codaInvoice__c>();
        
        JVCO_Invoice_Group__c testInvoiceGroup = new JVCO_Invoice_Group__c();           
        //testInvoiceGroup.Name = 'TIG-000001';
        testInvoiceGroup.JVCO_Total_Amount__c = 100.00;
        insert testInvoiceGroup;
        
        c2g__codaInvoice__c sInv = JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        sInv.JVCO_Invoice_Group__c = testInvoiceGroup.Id;
        sInvList.add(sInv);
        insert sInvList;
        
        insert JVCO_TestClassHelper.getSalesInvoiceLine(sInv.Id, dim1.id, dim3.Id, taxCode.Id, dim2.Id, p.Id);

        //Create Cash Entry
        c2g__codaCashEntry__c testCashEntry = new c2g__codaCashEntry__c();
        testCashEntry.c2g__PaymentMethod__c = 'Electronic';
        testCashEntry.c2g__OwnerCompany__c = comp.id;
        testCashEntry.ownerid = testGroup.Id;
        testCashEntry.c2g__Status__c = 'In Progress';
        testCashEntry.c2g__Date__c = Date.today();
        testCashEntry.c2g__Value__c = 100.00;
        insert testCashEntry;
        List<c2g__codaInvoice__c> invoiceList = [select id, Name from c2g__codaInvoice__c limit 2];
        //Create Cash Entry Line Item
        List<c2g__codaCashEntryLineItem__c> cashEntryLIList = new List<c2g__codaCashEntryLineItem__c>();
        c2g__codaCashEntryLineItem__c cashEntryLineItem1 = new c2g__codaCashEntryLineItem__c();
        cashEntryLineItem1.c2g__CashEntry__c = testCashEntry.id;
        cashEntryLineItem1.c2g__Account__c = licAcc.id;
        cashEntryLineItem1.c2g__CashEntryValue__c = 100;
        cashEntryLineItem1.c2g__LineNumber__c = 1;
        cashEntryLineItem1.c2g__AccountReference__c = invoiceList[0].Name;
        cashEntryLineItem1.c2g__AccountPaymentMethod__c = 'Cheque';
        cashEntryLineItem1.c2g__OwnerCompany__c = comp.id;
        cashEntryLineItem1.JVCO_Temp_AccountReference__c = 'SIN00001';
        cashEntryLineItem1.JVCO_CaseRecordType_Insolvency__c = '';
        cashEntryLIList.add(cashEntryLineItem1);
        insert cashEntryLIList;
        Test.stopTest();
        
        //Turn off Background Matching
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        
        SMP_DirectDebit_GroupInvoice__c ddgroup = new SMP_DirectDebit_GroupInvoice__c();

        Income_Direct_Debit__c dd = new Income_Direct_Debit__c();
        // dd.Name = 'Test'; 
        // dd.DD_Last_Collected_Date__c = ''; /
        dd.DD_Ongoing_Collection_Amount__c =  1;
        dd.DD_Account_Number_OK__c = true; 
        dd.DD_Sort_Code_OK__c = true; 
        dd.DD_Bank_Name__c = 'test';
        dd.DD_Branch_Name__c = 'Test';
        dd.DD_Bank_Account_Name__c = 'test';
        dd.DD_Bank_Account_Number__c = '88888888';
        dd.DD_Bank_Sort_Code__c = '666666';
        insert dd;

        List<Income_Direct_Debit__c> ddList = new List<Income_Direct_Debit__c>();
        ddList.add(dd);

        Income_Debit_History__c history = new Income_Debit_History__c();
        history.Income_Direct_Debit__c = dd.Id;
        insert history;

        SMP_DirectDebit_GroupInvoice__c smpgroup = new SMP_DirectDebit_GroupInvoice__c();
        smpgroup.Income_Direct_Debit__c = dd.id;
        smpgroup.Invoice_Group__c = testInvoiceGroup.id;
        insert smpgroup;

        c2g__codaInvoiceInstallmentLineItem__c testSaleInvInsLine = new c2g__codaInvoiceInstallmentLineItem__c();
        //testSaleInvInsLine.name = '111';
        testSaleInvInsLine.c2g__UnitOfWork__c = 358.0;
        testSaleInvInsLine.c2g__Amount__c = 1000.00;
        testSaleInvInsLine.c2g__Invoice__c = sInv.id;
        testSaleInvInsLine.c2g__OwnerCompany__c = comp.id;
        //testSaleInvInsLine.JVCO_Payment_Method__c = 'Direct Debit';
        testSaleInvInsLine.JVCO_Payment_Status__c = 'Unpaid';
        testSaleInvInsLine.c2g__DueDate__c = Date.Today().addDays(+1);
        // testSaleInvInsLine.JVCO_DueDateExtension__c = system.today().addDays(-1);
        
        insert testSaleInvInsLine;

        SMP_PaymentScheduleHelper.checkDirectDebit(ddList);
        SMP_PaymentScheduleHelper.processFailedDDHistories(history);
        
        Map<Id, c2g__codaInvoice__c> invoiceMap = new Map<Id, c2g__codaInvoice__c>([SELECT Id FROM c2g__codaInvoice__c LIMIT 1]);
        List<TriggerHandler_CardPaymentHistory.CardPaymentSalesInvoiceWrapper> wrapList = new List<TriggerHandler_CardPaymentHistory.CardPaymentSalesInvoiceWrapper>();
        TriggerHandler_CardPaymentHistory.CardPaymentSalesInvoiceWrapper wrapper = new TriggerHandler_CardPaymentHistory.CardPaymentSalesInvoiceWrapper();
        wrapper.salesInvoiceIdList = new List<Id>(invoiceMap.keySet());
        wrapper.cardPayment = new Income_Card_Payment__c();
        wrapList.add(wrapper);
        List<TriggerHandler_CardPaymentHistory.CardPaymentSalesInvoiceWrapper> wrapListFinal = SMP_PaymentScheduleHelper.checkCardPaymentHistory(wrapList);
    }
}