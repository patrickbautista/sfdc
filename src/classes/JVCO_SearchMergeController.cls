/*
Copyright (c) 2008 salesforce.com, inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
public without sharing class JVCO_SearchMergeController {

    // private QueryBuilder queryBuilder;
    private List<JVCO_DynamicSObject> results;
    private List<JVCO_DynamicSObject> mergeRecords;
    private Map<String, JVCO_DynamicSObject> selectedFromPagination;
    private List<SObject> sObjects;
    private boolean excludePersonAccFields;
    private String error;
    private Boolean isStandardMerge = false;
    public boolean debug;
    public boolean showQuery;
    public boolean isPrevious = false;
    public boolean isNext = false;
    public Integer offset = 0;
    public boolean isFromPagination = false;

    private String[] childRelationships = new String[]{};
    
    public class DupeElimRedirectException extends Exception {}

    public JVCO_SearchMergeController(){
        queryBuilder = new JVCO_QueryBuilder();
        results = new List<JVCO_DynamicSObject>();
        mergeRecords = new List<JVCO_DynamicSObject>();
        selectedFromPagination = new Map<String, JVCO_DynamicSObject>();
        renderResult = false;
        cloneNonReparentableChild = false;
        showMasterAfterMerge = true;
        excludePersonAccFields = false;
        init();
    }

    private void init() {
        String debugParam = ApexPages.currentPage().getParameters().get('debug');
        debug = (debugParam != null && (debugParam.equals('1') || debugParam.equals('true'))) ? true : false;
        String showQueryParam = ApexPages.currentPage().getParameters().get('showQuery');
        showQuery = (showQueryParam != null && (showQueryParam.equals('1') || showQueryParam.equals('true'))) ? true : false;
        String hideFilterParam = ApexPages.currentPage().getParameters().get('hideFilter');
        hideFilter = (hideFilterParam != null && (hideFilterParam.equals('1') || hideFilterParam.equals('true'))) ? true : false;
        String hideHeaderParam = ApexPages.currentPage().getParameters().get('hideHeader');
        hideHeader = (hideHeaderParam != null && (hideHeaderParam.equals('1') || hideHeaderParam.equals('true'))) ? true : false;
        String hideSidebarParam = ApexPages.currentPage().getParameters().get('hideSidebar');
        hideSidebar = (hideSidebarParam != null && (hideSidebarParam.equals('1') || hideSidebarParam.equals('true'))) ? true : false;

        // Get default query parameters
        String findParam = ApexPages.currentPage().getParameters().get('find');
        boolean find = (findParam != null && (findParam.equals('1') || findParam.equals('true'))) ? true : false;
        String objectName = ApexPages.currentPage().getParameters().get('object');
        if (find && queryBuilder.isValidObject(objectName)) {
            queryBuilder.objectName = objectName;
            String firstParamField = ApexPages.currentPage().getParameters().get('field' + 1);
            String firstParamValue = ApexPages.currentPage().getParameters().get('value' + 1);
            queryBuilder.setObjectFields(objectName, firstParamField == 'RecordTypeId' ? firstParamValue : null);
            if(objectName == 'Account'){
                isStandardMerge = true;
            }
            
            for (Integer i = 1; i <= 5; i++) {
                String field = ApexPages.currentPage().getParameters().get('field' + i);
                String op = ApexPages.currentPage().getParameters().get('op' + i);
                String value = ApexPages.currentPage().getParameters().get('value' + i);
                if (queryBuilder.isValidField(field))
                    queryBuilder.setFieldName(i, field);
                if (queryBuilder.isValidOperator(op))
                    queryBuilder.setOperatorValue(i, op);
                queryBuilder.setInputValue(i, value);

            }
            queryBuilder.recordLimit = ApexPages.currentPage().getParameters().get('limit');
            find();
        }
    }

    public boolean hideHeader  {
        get {
            if (hideHeader == null) hideHeader = false;
            return hideHeader;
         }
        set;
    }
    public boolean hideSidebar  {
        get {
            if (hideSidebar == null) hideSidebar = false;
            return hideSidebar;
         }
        set;
    }
    public boolean hideFilter  {
        get {
            if (hideFilter == null) hideFilter = false;
            return hideFilter;
         }
        set;
    }
    public boolean renderResult  { get; set; }
    public boolean cloneNonReparentableChild  { get; set; }
    public boolean showMasterAfterMerge  { get; set; }


    public boolean getShowQuery() {
        return showQuery;
    }

    public JVCO_QueryBuilder queryBuilder {get; set;}

    public List<JVCO_DynamicSObject> getResults() {
        if (debug) for (JVCO_DynamicSObject s : results) System.debug(Logginglevel.ERROR,'############## result: ' + s);
        return results;
    }

    public String getError() { return error; }

    public PageReference find() {
        System.debug('Deebug find() Start');
        error = null;
        String queryStr = null;
        
        try {
            for (JVCO_DynamicSObject o : getResults()) {
                if (o.getSelected() && !selectedFromPagination.containsKey(o.getID())) {
                    selectedFromPagination.put(o.getID(), o);
                }
                else if(!o.getSelected() && selectedFromPagination.containsKey(o.getID())){
                    selectedFromPagination.remove(o.getID());
                }
            }
            
            results.clear();
        
            if(!isFromPagination){ offset = 0; selectedFromPagination.clear();}
            queryStr = queryBuilder.getQueryString(TRUE);
            if(offset > 0 && offset <= 2000) { queryStr += ' OFFSET ' + offset; }
            List<SObject> sobjectResults = Database.query(queryStr);
            for (SObject s : sobjectResults) {
                results.add(new JVCO_DynamicSObject(s, queryBuilder.objectName, queryBuilder.getQueryFields(), queryBuilder.getReferenceFields()));
                if(selectedFromPagination.containsKey(String.valueOf(s.get('Id')))){
                    results[results.size() - 1].setSelected(true);
                }
            }
            
            if(sobjectResults.size() > 0 && (queryBuilder.objectName == 'Account' || queryBuilder.objectName == 'JVCO_Venue__c') && queryBuilder.objectFields == null){
                String defaultRecordType;
                if(queryBuilder.objectName == 'Account'){
                    try{
                       defaultRecordType = ((Account)sobjectResults[0]).RecordType.Name;
                    }catch(Exception e){
                        if(e.getMessage().contains('Account.RecordType')){
                            results.clear();
                            throw new DupeElimRedirectException('Record Type is required as criteria.');
                        }
                    }
                    if(defaultRecordType == null){
                        defaultRecordType = 'Customer Account';
                    }
                    else{
                        isStandardMerge = true;
                    }
                }
                queryBuilder.setObjectFields(queryBuilder.objectName, defaultRecordType);
            }
            
            renderResult = true;
            isPrevious = (offset == 0) ? false: true;
            isNext = (Integer.valueOf(queryBuilder.recordLimit) > sobjectResults.size()) ? false : true;
            isFromPagination = false;
        } catch (Exception e) {
            if (debug) throw e;
            ApexPages.addMessages(e);
        }

        return null;
    }

    public PageReference cancel() {
        queryBuilder.objectName = null;
        return resetQuery();
    }

    public PageReference cancel2() {
        cancel();
        return Page.JVCO_SearchMerge;
    }
    
    public PageReference next() {
        if(!String.isBlank(queryBuilder.recordLimit)){
            isFromPagination = true;
            offset += Integer.valueOf(queryBuilder.recordLimit);
            find();
        }
        
        return null;
    }
    
    public PageReference prev() {
        if(!String.isBlank(queryBuilder.recordLimit)){
            isFromPagination = true;
            offset -= Integer.valueOf(queryBuilder.recordLimit);
            find();
        }
        
        return null;
    }
    
    public boolean getIsPrevious() {
        return isPrevious;
    }
    
    public boolean getIsNext() {
        return isNext;
    }

    public PageReference resetQuery() {
        queryBuilder.reset();
        results.clear();
        mergeRecords.clear();
        renderResult = false;
        excludePersonAccFields = false;
        cloneNonReparentableChild = false;
        childRelationships.clear();
        error = null;
        selectedFromPagination.clear();
        isPrevious = false;
        isNext = false;
        offset = 0;
        
       
        if(queryBuilder.objectName == 'JVCO_Venue__c'){
            queryBuilder.setFieldName(1, 'JVCO_PPL_Id__c');
            queryBuilder.setOperatorValue(1, 'equals');
            queryBuilder.setInputValue(1, 'null');
            queryBuilder.setFieldName(2, 'JVCO_PRS_Id__c');
            queryBuilder.setOperatorValue(2, 'equals');
            queryBuilder.setInputValue(2, 'null');

        }
        else if(queryBuilder.objectName == 'Account'){
            queryBuilder.setFieldName(1, 'RecordTypeId');
            queryBuilder.setOperatorValue(1, 'equals');
            queryBuilder.setInputValue(1, 'Customer Account');
            queryBuilder.setFieldName(2, '');
            queryBuilder.setOperatorValue(2, '');
            queryBuilder.setInputValue(2, '');
            isStandardMerge = true;
        }
        return null;
    }

    public PageReference previous() {
        error = null;
        excludePersonAccFields = false;
        cloneNonReparentableChild = false;
        childRelationships.clear();
        mergeRecords.clear();
        return Page.JVCO_SearchMerge;
    }

    public JVCO_DynamicSObject getMergeRecord1() { return mergeRecords.size() >= 1 ? mergeRecords[0] : null; }
    public JVCO_DynamicSObject getMergeRecord2() { return mergeRecords.size() >= 2 ? mergeRecords[1] : null; }
    public JVCO_DynamicSObject getMergeRecord3() { return mergeRecords.size() >= 3 ? mergeRecords[2] : null; }

    public PageReference selectMerge() {
        System.debug('selectMerge() --- Start');
        error = null;
        mergeRecords.clear();
        String query = null;
        try {
        
            if(selectedFromPagination.size() > 0){
                if(results.size() > 0){
                    for(JVCO_DynamicSObject sObj : results){
                        if(selectedFromPagination.containsKey(sObj.getID())){
                            selectedFromPagination.remove(sObj.getID());
                        }
                    }
                }
                results.addAll(selectedFromPagination.values());
            }

            List<JVCO_DynamicSObject> selectedSObjects = new List<JVCO_DynamicSObject>();
            for (JVCO_DynamicSObject o : getResults()) {
                if (o.getSelected()) {
                    if (selectedSObjects.size() < 3) {
                        selectedSObjects.add(o);
                    } else {
                        error = 'You may only select up to three records to merge.';
                        return null;
                    }
                }
            }

            if (selectedSObjects.size() < 2) {
                error = 'You must select two or three records to merge.';
                if(queryBuilder.objectName == 'Account'){
                    error += ' When merging licence accounts, ensure the Type field contains the value Customer.';
                }
                return null;
            }

            Boolean isPersonAcc1 = true;
            Boolean isPersonAcc2 = false;
            List<ID> mergeIds = new List<ID>();
            for (JVCO_DynamicSObject o : selectedSObjects) {
                mergeIds.add(o.getID());
                if (queryBuilder.objectName == 'Account' && queryBuilder.isValidField('IsPersonAccount')) {
                    isPersonAcc1 &= o.getIsPersonAcc();
                    isPersonAcc2 |= o.getIsPersonAcc();
                }
            }
			
			if(queryBuilder.objectName == 'JVCO_Venue__c'){
                String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
                Map<Id,PermissionSet> enAdPermSet = new Map<Id,PermissionSet>([SELECT Id FROM PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors']);
                List<PermissionSetAssignment> permissionSetList = [SELECT Id FROM PermissionSetAssignment WHERE 
                                                        AssigneeId = :UserInfo.getUserId() AND PermissionSetId IN :enAdPermSet.keySet()];
                List<JVCO_Affiliation__c> affs = [SELECT Id FROM JVCO_Affiliation__c WHERE JVCO_Venue__c IN :mergeIds AND 
                                                    (JVCO_Account__r.JVCO_In_Enforcement__c = true OR JVCO_Account__r.JVCO_In_Infringement__c = true) AND 
                                                    (JVCO_Account__r.JVCO_Credit_Status__c = 'Enforcement - Debt Recovery' OR 
                                                    JVCO_Account__r.JVCO_Credit_Status__c = 'Enforcement - Infringement')];
                if(affs.size() > 0 && profileName <> 'System Administrator' && permissionSetList.isEmpty()){
                    throw new DupeElimRedirectException('Merging venues marked "In Enforcement" or "In Infringement" is not allowed.');
                }
            }

            if (queryBuilder.objectName == 'Account' && queryBuilder.isValidField('IsPersonAccount') && isPersonAcc1 != isPersonAcc2) {
                error = 'The accounts cannot be merged. Selected records do not have compatible record types and cannot be merged together.';
                return null;
            }

            excludePersonAccFields =  queryBuilder.objectName == 'Account' && queryBuilder.isValidField('IsPersonAccount') && !isPersonAcc1;
            List<SObjectField> fields = queryBuilder.getAllFields(excludePersonAccFields);
            query = queryBuilder.getQueryAllFields(mergeIds, excludePersonAccFields);
            sObjects = Database.query(query);
            Map<ID, JVCO_DynamicSObject> recordMap = new Map<ID, JVCO_DynamicSObject>();
            for (SObject s : sObjects) {
                if(queryBuilder.objectName == 'Account'){
                    Account selectedAcc = (Account) s;
                    if(selectedAcc.JVCO_In_Enforcement__c || selectedAcc.JVCO_In_Infringement__c){
                        throw new DupeElimRedirectException('Merging accounts marked "In Enforcement" or "In Infringement" is not allowed.');
                    }
                    if(selectedAcc.JVCO_Credit_Status__c == 'Passed to DCA - 1st Stage' || selectedAcc.JVCO_Credit_Status__c == 'Passed to DCA – 2nd Stage'){
                        throw new DupeElimRedirectException('There is a Licence Account that is in DCA Stage 1 or DCA Stage 2. Please use an Account that is not in DCA.');   
                    }
                }
                recordMap.put(s.id, new JVCO_DynamicSObject(s, queryBuilder.objectName, fields, queryBuilder.getReferenceFields()));
            }
            for (ID id : mergeIds) {
                mergeRecords.add(recordMap.get(id));
            }

            selectedRecord = mergeIds[0];
            doSelectRecord();
        } catch (Exception e) {
             ApexPages.addMessages(e);
             return null;
        }
        System.debug('selectMerge() --- End');
        return Page.JVCO_MergeRecord;
    }

    public List<String> getFieldLabels() {
        return queryBuilder.getFieldLabels(excludePersonAccFields);
    }

    public String selectedRecord { get; set; }
    public String selectedField { get; set; }

    public PageReference doSelectRecord() {
        for (String f : queryBuilder.getAllFieldNames(excludePersonAccFields)) {
                if(queryBuilder.objectFields <> null && !queryBuilder.objectFields.contains(f))continue;
                for (JVCO_DynamicSObject s : mergeRecords) {
                    if (s.getField(f) <> null && s.getField(f).getValue() <> null && String.valueOf(s.getField(f).getValue()) <> '') {
                        s.selectField(f,true);
                        break;
                    }
                }
        }
        return null;
    }

    public PageReference doSelectField() {
        if (debug) System.debug('############# Selected record: ' + selectedRecord);
        if (debug) System.debug('############# Selected field: ' + selectedField);
        String nameField = null;
        if (selectedField == 'Id') {
            nameField = queryBuilder.getReferenceFields().get('Id');
        }
        for (JVCO_DynamicSObject s : mergeRecords) {
            if (s.getID() == selectedRecord) {
                s.selectField(selectedField, true);
                if (nameField != null) s.selectField(nameField, true);
            } else {
                s.selectField(selectedField, false);
                if (nameField != null) s.selectField(nameField, false);
            }
        }
        return null;
    }

    public String[] getChildRelationships() {
        return childRelationships;
    }

    public void setChildRelationships(String[] childRelationships) {
        this.childRelationships = childRelationships;
    }

    public List<SelectOption> getChildRelationshipSelection() {
        return queryBuilder.getChildRelationshipSelection(excludePersonAccFields);
    }

    public void clearChildSelection() {
        if (!cloneNonReparentableChild && childRelationships != null)
            childRelationships.clear();
    }

    public PageReference doMerge() {
        System.debug('doMerge() ---- Start');
        sObjects.clear();
        JVCO_DynamicSObject master;
        Set<Id> mergeIds = new Set<Id>();
        Savepoint sp = Database.setSavepoint();
        SBQQ.TriggerControl.disable(); //Bypass CPQ triggers for 36236
        
        try {
            for (JVCO_DynamicSObject s : mergeRecords) {
                JVCO_DynamicSObject.Field f = s.getField('Id');
                if (debug) System.assertNotEquals(null, f, 'Cannot find field: Id ');
                if (f.getSelected()) {
                    if (sObjects.isEmpty()) sObjects.add(s.getSObject());
                    else sObjects.add(0, s.getSObject());
                    master = s;
                } else {
                    sObjects.add(s.getSObject());
                }
                
                mergeIds.add(s.getID());
            }
            
            if (master == null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must select a master record to merge.'));
                return null;
            }
            
            Set<String> ownership = new Set<String>();
            for(JVCO_Affiliation__c affil : [SELECT Id, JVCO_Ownership__c FROM JVCO_Affiliation__c WHERE JVCO_Account__c IN: mergeIds]){
                ownership.add(affil.JVCO_Ownership__c);
            }
            
            if(ownership.size() > 1){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                //                    'Associated Affiliation records of the selected Master Account have different ownership.'));
                                    'Please review the Affiliations, it is the Ownership values differing which is preventing this merge. In order to merge successfully all Ownership values need to be the same. Please speak to the technology team for further assistance')); // jules.osberg.a.pablo GREEN-30902 updated error message
                return null;
            }
            
            List<SObjectField> fields = queryBuilder.getAllFields(TRUE);
            String query = queryBuilder.getQueryAllFields(new List<String>{master.getID()}, TRUE);
            if(query.containsIgnoreCase('.Name')){
                String afterFrom = query.substringAfter('FROM');
                String feildsToQuery = query.substringBefore('FROM').substringAfter('SELECT');
                List<String> fieldsListToAdd = new List<String>();
                List<String> fieldsList = feildsToQuery.split(',');
                query = 'SELECT ';
                Boolean isFirst = true;
                for(String fieldParam : fieldsList){
                    if(fieldParam <> '' && !fieldParam.contains('.Name')){
                        if(isFirst){
                            isFirst = false;
                        }
                        else{
                            query += ', ';
                        }
                        query += fieldParam;
                        
                    }
                }
                
                query += ' FROM' + afterFrom;
                
            }
            
            System.debug('Deebug:' + query);
            List<SObject> newSObjectList = Database.query(query);
            JVCO_DynamicSObject newDynamicObject = null;
            Exception exe = null;
            
            if(newSObjectList.size() > 0){
                newDynamicObject = new JVCO_DynamicSObject(newSObjectList[0], queryBuilder.objectName, fields, queryBuilder.getReferenceFields());
            }
            
            for (JVCO_DynamicSObject s : mergeRecords) {
                if (s.getID() != master.getID()) {
                    for (String f : queryBuilder.getAllFieldNames(excludePersonAccFields)) {
                        if(queryBuilder.objectFields <> null && !queryBuilder.objectFields.contains(f))continue;
                        JVCO_DynamicSObject.Field m_field = master.getField(f);
                        JVCO_DynamicSObject.Field s_field = s.getField(f);
                        if (!m_field.getSelected() && s_field.getSelected() && m_field.getValue() != s_field.getValue()) {
                            m_field.setValue(s_field.getValue());
                        }
                        if(newDynamicObject <> null){
                            JVCO_DynamicSObject.Field new_field = newDynamicObject.getField(f);
                            if(m_field.getValue() != new_field.getValue()){
                                new_field.setValue(m_field.getValue());
                            }
                        }
                        
                    }
                }
            }
            
            JVCO_MergeRecord.debug = debug;
            Set<Id> recordsToUpdateMasterSetId = new Set<Id>();
            String accRecordType = '';
            if(queryBuilder.objectName == 'JVCO_Venue__c' || queryBuilder.objectName == 'Account'){
                List<SObject> recordsToUpdateMasterId = new List<SObject>();
                for(Integer i = 1; i < sObjects.size(); i++){
                    sObjects[i].put('JVCO_MasterRecordId__c', master.getID());
                    if(queryBuilder.objectName == 'Account'){
                        accRecordType = ((Account)sObjects[i]).RecordType.Name;
                        recordsToUpdateMasterSetId.add(((Account)sObjects[i]).Id);
                    }
                    recordsToUpdateMasterId.add(sObjects[i]);
                }
                
                if(recordsToUpdateMasterId.size() > 0){
                    Database.update(recordsToUpdateMasterId);
                }
            }    
            
            List<Opportunity> opps = new List<Opportunity>();
			List<Opportunity> oppsToUpdateBack = new List<Opportunity>();
            Map<Id, Contact> contactMaps = new Map<Id, Contact>();
            if(queryBuilder.objectName == 'Account' && recordsToUpdateMasterSetId.size() > 0){
                
                if(accRecordType == 'Licence Account'){
                    for(Opportunity opp : [SELECT Id, AccountId, JVCO_Invoiced__c FROM Opportunity 
                                            WHERE AccountId IN :recordsToUpdateMasterSetId]){						
                        opp.AccountId = master.getID();
						if(opp.JVCO_Invoiced__c){ oppsToUpdateBack.add(opp); }
                        opp.JVCO_Invoiced__c = FALSE;
                        opps.add(opp);
                    }
                    
                    if(opps.size() > 0){
                        Database.update(opps);
                    }
                }
                else if(accRecordType == 'Customer Account'){
                    for(Contact cont : [SELECT Id, AccountId FROM Contact WHERE AccountId IN :recordsToUpdateMasterSetId]){
                        cont.AccountId = master.getID();
                        contactMaps.put(cont.Id, cont);
                    }
                    
                    if(contactMaps.size() > 0){
                        Database.update(contactMaps.values());
                    }
                }
            }
			
			if(queryBuilder.objectName == 'JVCO_Venue__c' || queryBuilder.objectName == 'Account'){
                JVCO_AffiliationTriggerHandler.runAffiliationExistValidation = false;
            }
			
			JVCO_AffiliationMerge.isMerge = TRUE;
                        
            if(isStandardMerge && newDynamicObject <> null){
                List<SObject> slaveObjects = new List<SObject>();
                for(Integer i = 1; i < sObjects.size(); i++){
                    slaveObjects.add(sObjects[i]);
                }
                Database.merge(newDynamicObject.getSObject(), slaveObjects);
                
                if(accRecordType == 'Licence Account'){
                    c2g.BalanceUpdateService.updateBalancesForMergedAccountsAsync(new Set<Id>{master.getID()});
                    JVCO_UpdateAccountBalanceLogic.futureUpdateAccountBalance(new Set<Id>{master.getID()});
					prepareDataToTriggerDLRS(master.getID(), 'Licence' );
                }
				
				if(accRecordType == 'Customer Account'){ updateLicenceAccountBasedOnNewParent(master.getID()); prepareDataToTriggerDLRS(master.getID(), 'Customer' ); }
                
                for(Opportunity opp : oppsToUpdateBack){
                    opp.JVCO_Invoiced__c = TRUE;
                }
                
                Database.update(oppsToUpdateBack);
                
            }
            else{
                if(queryBuilder.objectName == 'JVCO_Venue__c'){
                    cloneNonReparentableChild = true;
                    childRelationships.add('Affiliations__r');
                }
                JVCO_MergeRecord.mergeSObject(sObjects, cloneNonReparentableChild, childRelationships);
            }
			
        } catch (Exception e) {
            Database.rollback( sp );
            if (debug) throw e;
            System.debug(e.getTypeName() + ': ' + e.getMessage() + ' -- ' + e.getCause());
            ApexPages.addMessages(e);
            return null;
        }

        if (showMasterAfterMerge) {
            String viewUrl = '/' + master.getID();
            if (queryBuilder.objectName.equals('Idea')) {
                viewUrl = '/ideas/viewIdea.apexp?id=' + master.getID();
            }
            PageReference recordPage = new PageReference(viewUrl);
            recordPage.setRedirect(true);
            return recordPage;
        } else {
            find();
            return previous();
        }
    }
	
	/* ----------------------------------------------------------------------------------------------------------
    Author:         recuerdo.b.bregente
    Company:        Accenture
    Description:    GREEN-26122
    Params:         Id of the winning Customer Account after merge
    Returns:        void
    <Date>          <Authors Name>                      <Brief Description of Change> 
    21-11-2017      recuerdo.b.bregente                 Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/ 
    public static void updateLicenceAccountBasedOnNewParent(Id parentId){
        Map<Id, Account> accMap = new Map<Id, Account>([SELECT Id, RecordTypeId, Type, c2g__CODADimension1__c FROM Account WHERE Id = :parentId]);
        populateLicenceOnCustomerAfterUpdate(accMap);
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         recuerdo.b.bregente
    Company:        Accenture
    Description:    GREEN-26122
    Params:         Map of new Accounts, Map of Old Accounts
    Returns:        void
    <Date>          <Authors Name>                      <Brief Description of Change> 
    21-11-2017      recuerdo.b.bregente                 Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/ 
    public static void populateLicenceOnCustomerAfterUpdate(Map<Id, Account> newAccMap){
        Set<Id> typeChangedIds = new Set<Id>();
        Set<Id> dimChangedIds = new Set<Id>();
        Id key;
        Id nonKey;
        
        for(c2g__codaDimension3__c dim : [SELECT Id, Name FROM c2g__codaDimension3__c WHERE Name IN ('Key', 'Non Key')]){
            if(dim.Name == 'Key'){
                key = dim.Id;
            }
            else if(dim.Name == 'Non Key'){
                nonKey = dim.Id;
            }
        }
        
        for(Account acc : newAccMap.values()){
            if(acc.RecordTypeId == JVCO_GlobalConstants.ACCOUNT_CUSTOMER_RECORDTYPE_ID){
                typeChangedIds.add(acc.Id);
                dimChangedIds.add(acc.Id);
            }
        }
        
        if(typeChangedIds.size() > 0 || dimChangedIds.size() > 0){
            List<Account> licAccounts = [SELECT Id, Type, c2g__CODADimension1__c, JVCO_Live__c, JVCO_Customer_Account__c, c2g__CODADimension3__c FROM Account WHERE 
                                            RecordTypeId = :JVCO_GlobalConstants.ACCOUNT_LICENCE_RECORDTYPE_ID AND 
                                            (JVCO_Customer_Account__c IN :typeChangedIds OR JVCO_Customer_Account__c IN :dimChangedIds)];
                                            
            for(Account acc : licAccounts){
                if(typeChangedIds.contains(acc.JVCO_Customer_Account__c)){
                    acc.Type = newAccMap.get(acc.JVCO_Customer_Account__c).Type;
                    acc.c2g__CODADimension3__c = (acc.Type == 'Key Account') ? key : nonKey;
                } 
                
                if(!acc.JVCO_Live__c && dimChangedIds.contains(acc.JVCO_Customer_Account__c)){
                    acc.c2g__CODADimension1__c = newAccMap.get(acc.JVCO_Customer_Account__c).c2g__CODADimension1__c;
                } 
            }
            
            Database.update(licAccounts);
        }
    }
	
	/* ----------------------------------------------------------------------------------------------------------
    Author:         recuerdo.b.bregente
    Company:        Accenture
    Description:    GREEN-26233             This will be called to trigger the DLRS for Number of Renewable Licence Accounts
    Params:         accountId               Id of the winning Licence Account
    Returns:        void
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-01-2018      recuerdo.b.bregente                 Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/ 
    public static void prepareDataToTriggerDLRS(Id accountId, String type){
        Id customerAccountId = accountId;
        
        if('Licence' == type){
            List<Account> winningAccount = [SELECT Id, JVCO_Customer_Account__c FROM Account WHERE Id = :accountId AND JVCO_Customer_Account__c != null];
            if(winningAccount.size() > 0){ customerAccountId = winningAccount[0].JVCO_Customer_Account__c; }
        }
        
        Map<Id, Account> accMap = new Map<Id, Account>([SELECT Id, RecordTypeId, Type, c2g__CODADimension1__c, JVCO_Review_Type__c 
                                                        FROM Account 
                                                        WHERE JVCO_Customer_Account__c = :customerAccountId 
                                                        AND JVCO_Review_Type__c != 'Do not renew']);
        
        if(accMap.size() > 0){ triggerDLRSFromLicenceAccount( accMap.values()[0]); }
        else{ 
            List<Account> parentAccount = [SELECT Id, JVCO_Renewable_Licence_Accounts__c FROM Account WHERE Id = :customerAccountId AND JVCO_Renewable_Licence_Accounts__c != 0];
            if(parentAccount.size() > 0){ parentAccount[0].JVCO_Renewable_Licence_Accounts__c = 0; Database.update(parentAccount); }
        }
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         recuerdo.b.bregente
    Company:        Accenture
    Description:    GREEN-26233             Temporarily update the Review Type of Licence Account to 'Do not renew'
    Params:         licenceAccount          Licence Account to update
    Returns:        void
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-01-2018      recuerdo.b.bregente                 Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/ 
    public static void triggerDLRSFromLicenceAccount(Account licenceAccount){
        String reviewType = licenceAccount.JVCO_Review_Type__c;
        licenceAccount.JVCO_Review_Type__c = 'Do not renew';
        Database.update(licenceAccount);
        updateReviewTypeToOriginalValue(licenceAccount.Id, reviewType);
    }
    
    /* ----------------------------------------------------------------------------------------------------------
    Author:         recuerdo.b.bregente
    Company:        Accenture
    Description:    GREEN-26233      Update the Review type of licence account to its original value.
    Params:         licenceId        Licence Account Id to Update
    Params:         reviewType       Original value of review type
    Returns:        void
    <Date>          <Authors Name>                      <Brief Description of Change> 
    16-01-2018      recuerdo.b.bregente                 Initial version of the code
    ----------------------------------------------------------------------------------------------------------*/ 
    @future
    public static void updateReviewTypeToOriginalValue(Id licenceId, String reviewType){
        List<Account> licenceAccounts = [SELECT Id, JVCO_Review_Type__c FROM Account WHERE Id = :licenceId];
        if(licenceAccounts.size() > 0){
            licenceAccounts[0].JVCO_Review_Type__c = reviewType;
            Database.update(licenceAccounts);
        }
    }
}