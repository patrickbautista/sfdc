/* ----------------------------------------------------------------------------------------------
    Name: JVCO_ContractRenewQuotes_QueueableTest
    Description: Test Class for JVCO_ContractRenewQuotes_Queueable

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    08-Aug-2018     0.1         Jules Pablo         Intial creation
----------------------------------------------------------------------------------------------- */
@IsTest
private class JVCO_ContractRenewQuotes_QueueableTest 
{
    
    @testSetup static void setupTestData() 
    {
        SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        insert dt;
        
        Pricebook2 pb = JVCO_TestClassObjectBuilder.createPriceBook();
        insert pb;
        
        Product2 prod1 = JVCO_TestClassObjectBuilder.createProduct();
        insert prod1;
        
        Account acc = JVCO_TestClassObjectBuilder.createCustomerAccount();
        insert acc;

        Account acc2 = JVCO_TestClassObjectBuilder.createLicenceAccount(acc.id);
        insert acc2;

        Opportunity o = JVCO_TestClassObjectBuilder.createOpportunity(acc2);
        o.CloseDate = System.today() - 5;
        o.StageName = 'Draft';
        insert o;

        SBQQ__QuoteProcess__c qProcess = JVCO_TestClassObjectBuilder.createQuoteProcess();
        insert qProcess;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        q.SBQQ__Type__c = 'Renewal';
        q.SBQQ__Status__c = 'Draft';
        q.SBQQ__Primary__c = true;
        q.SBQQ__Account__c = acc2.Id;
        q.SBQQ__Opportunity2__c = o.Id;
        q.SBQQ__StartDate__c = System.today() - 5;
        q.SBQQ__EndDate__c = System.today().addMonths(12);
        q.SBQQ__ExpirationDate__c = System.today().addMonths(12);
        insert q;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
        ql.SBQQ__Quote__c = q.Id;
        ql.SBQQ__Product__c = prod1.Id;
        ql.SBQQ__Quantity__c = 1;
        ql.SBQQ__Number__c = 1;
        ql.SBQQ__PricingMethod__c = 'List';
        ql.SBQQ__ListPrice__c = 10;
        ql.SBQQ__CustomerPrice__c = 10;
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__SpecialPrice__c = 10;
        ql.SBQQ__RegularPrice__c = 10;
        ql.SBQQ__UnitCost__c = 10;
        ql.SBQQ__ProratedListPrice__c = 10;
        ql.SBQQ__SpecialPriceType__c = 'Custom';
        qL.SPVAvailability__c = true;
        insert ql;
        
        JVCO_Venue__c  venueRecord = JVCO_TestClassObjectBuilder.createVenue();           
        Insert  venueRecord;   
 
        JVCO_Affiliation__c  affilRecord = JVCO_TestClassObjectBuilder.createAffiliation(acc2.id, venueRecord.id);  
        Insert  affilRecord;    
        
        SBQQ__QuoteLineGroup__c  quoteLineGroupRecord = new SBQQ__QuoteLineGroup__c();
        quoteLineGroupRecord.JVCO_Affiliated_Venue__c = affilRecord.id;
        quoteLineGroupRecord.JVCO_IsComplete__c = true;    
        quoteLineGroupRecord.SBQQ__Quote__c = q.Id;
        Insert  quoteLineGroupRecord;

        JVCO_Renewal_Settings__c rqcs = new JVCO_Renewal_Settings__c();
        rqcs.Name = 'Automatic Quote Completion';
        rqcs.days__c = 28;
        insert rqcs;

        List<JVCO_Complete_Quote_Settings__c> settings = new List<JVCO_Complete_Quote_Settings__c>();
        settings.add(new JVCO_Complete_Quote_Settings__c(Name = 'ErrorNoSPV', Value__c = 'You can not contract the Quote since there are Quote Line(s) w/o SPV'));
        insert settings;
    }

    private static testMethod void testMethodContactRenewQuotesExecute() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Test.startTest();
        System.enqueueJob(new JVCO_ContractRenewQuotes_Queueable(accountRecord));
        Test.stopTest();
    }

    private static testMethod void testMethodContactRenewQuotesExecuteError() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        JVCO_ContractRenewQuotesHelper.testError = true;

        Test.startTest();
        System.enqueueJob(new JVCO_ContractRenewQuotes_Queueable(accountRecord));
        Test.stopTest();
    }

    private static testMethod void testMethodContactRenewQuotesReExecute() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName FROM Opportunity LIMIT 1]);
        
        Set<Id> opportunityIdSet = new Set<Id>();
        for(Id oppId :opportunityMap.keySet()) {
            opportunityIdSet.add(oppId);
        }
        Test.startTest();       
        System.enqueueJob(new JVCO_ContractRenewQuotes_Queueable(opportunityMap, accountRecord, 1, 0, '', opportunityIdSet, 1, 1));
        Test.stopTest();
    }

    private static testMethod void testMethodContactRenewQuotesReExecute2() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id, Name, CloseDate, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName FROM Opportunity LIMIT 1]);

        Test.startTest();
        Opportunity testOpp = opportunityMap.values()[0].clone(false,false,false,false);
        insert testOpp;

        opportunityMap.put(testOpp.Id, testOpp);
        
        Set<Id> opportunityIdSet = new Set<Id>();
        for(Id oppId :opportunityMap.keySet()) {
            opportunityIdSet.add(oppId);
        }

        System.enqueueJob(new JVCO_ContractRenewQuotes_Queueable(opportunityMap, accountRecord, 1, 0, '', opportunityIdSet, 1, 4));
        Test.stopTest();
    }

    private static testMethod void testMethodContactRenewQuotesReExecute3() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id, Name, CloseDate, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName FROM Opportunity LIMIT 1]);

        Test.startTest();
        Opportunity testOpp = opportunityMap.values()[0].clone(false,false,false,false);
        insert testOpp;

        opportunityMap.put(testOpp.Id, testOpp);
        
        Set<Id> opportunityIdSet = new Set<Id>();
        for(Id oppId :opportunityMap.keySet()) {
            opportunityIdSet.add(oppId);
        }

        System.enqueueJob(new JVCO_ContractRenewQuotes_Queueable(opportunityMap, accountRecord, 1, 0, '', opportunityIdSet, 1, 1));
        Test.stopTest();
    }

    private static testMethod void testMethodContactRenewQuotesReExecute4() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id, Name, CloseDate, AccountId, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName FROM Opportunity LIMIT 1]);

        JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
        kaTempRec.JVCO_AccountId__c = accountRecord.Id;
        kaTempRec.JVCO_OppId__c = opportunityMap.values()[0].Id;
        kaTempRec.JVCO_Contracted__c = true;
        insert kaTempRec;

        Test.startTest();
        Opportunity testOpp = opportunityMap.values()[0].clone(false,false,false,false);
        testOpp.SBQQ__Contracted__c = true;
        insert testOpp;

        opportunityMap.values()[0].SBQQ__Contracted__c = true;
        update opportunityMap.values()[0];

        opportunityMap.put(testOpp.Id, testOpp);
        
        Set<Id> opportunityIdSet = new Set<Id>();
        for(Id oppId :opportunityMap.keySet()) {
            opportunityIdSet.add(oppId);
        }

        System.enqueueJob(new JVCO_ContractRenewQuotes_Queueable(opportunityMap, accountRecord, 1, 0, '', opportunityIdSet, 1, 1));
        Test.stopTest();
    }

    private static testMethod void testMethodContactRenewQuotesReExecute5() {
        Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        
        Account accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c, JVCO_AbortRunningQueueable__c from Account where RecordTypeId =: licenceRT order by CreatedDate DESC limit 1];
        
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id, Name, CloseDate, AccountId, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName FROM Opportunity LIMIT 1]);

        accountRecord.JVCO_AbortRunningQueueable__c = true;
        update accountRecord;

        JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
        kaTempRec.JVCO_AccountId__c = accountRecord.Id;
        kaTempRec.JVCO_OppId__c = opportunityMap.values()[0].Id;
        kaTempRec.JVCO_Contracted__c = true;
        insert kaTempRec;

        Test.startTest();
        Opportunity testOpp = opportunityMap.values()[0].clone(false,false,false,false);
        testOpp.SBQQ__Contracted__c = true;
        insert testOpp;

        opportunityMap.values()[0].SBQQ__Contracted__c = true;
        update opportunityMap.values()[0];

        opportunityMap.put(testOpp.Id, testOpp);
        
        Set<Id> opportunityIdSet = new Set<Id>();
        for(Id oppId :opportunityMap.keySet()) {
            opportunityIdSet.add(oppId);
        }

        System.enqueueJob(new JVCO_ContractRenewQuotes_Queueable(opportunityMap, accountRecord, 1, 0, '', opportunityIdSet, 1, 1));
        Test.stopTest();
    }
}