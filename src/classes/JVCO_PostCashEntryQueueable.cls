public class JVCO_PostCashEntryQueueable Implements System.Queueable {
    
    public Integer batchSize;
    set<Id> cashEntryIds;
    
    //how many queueables to launch in parallel (system limit 50)
    public static Integer defaultQueueableLimit = 50;
    public static Integer defaultBatchSize = 5;

    public static String defaultQuery = 'SELECT Id FROM c2g__codaCashEntry__c WHERE c2g__Status__c = \'In Progress\' AND Id IN (SELECT c2g__CashEntry__c FROM c2g__codaCashEntryLineItem__c WHERE c2g__Account__c != null AND c2g__CashEntry__r.c2g__Status__c = \'In Progress\' ) ORDER BY c2g__Account__c';

    // method to launch a large number of queueable jobs to process posting of all cash entries
    public static void start() {
        start(defaultQuery,defaultBatchSize,defaultQueueableLimit);
    }

    public static void start(String queryString) {
        start(queryString,defaultBatchSize,defaultQueueableLimit);
    }
    
    public static void start(String queryString, Integer overrideBatchSize) {
        start(queryString,overrideBatchSize,defaultQueueableLimit);
    }
    
    public static void start(Integer overrideBatchSize) {
        start(defaultQuery,overrideBatchSize,defaultQueueableLimit);
    }
    
    public static void start(Integer overrideBatchSize, Integer overrideQueueableLimit) {
        start(defaultQuery,overrideBatchSize,overrideQueueableLimit);
    }
    
    public static void start(String queryString, Integer batchSize, Integer queueableLimit) {
        
        //select all cash entries which have lines which are linked to accounts
        list<c2g__codaCashEntry__c> cashEntries = Database.query(queryString);

        //number of records to push into each queue. round up to ensure all records are processed even if some batches are of un
        Integer recordSplit = (Integer)((Decimal)cashEntries.size()/(Decimal)queueableLimit).round(System.RoundingMode.UP);
        
        //loop to create the correct number of queueable jobs whilst we have records
        for(Integer i =0; i < queueableLimit && !cashEntries.isEmpty(); i++) {
            
            JVCO_PostCashEntryQueueable pceq = new JVCO_PostCashEntryQueueable();
            pceq.batchSize = batchSize;
            pceq.cashEntryIds = new set<Id>();
            
            //put an appropriate number of records into the job
            for(Integer j =0; j < recordSplit && !cashEntries.isEmpty(); j++) {
                pceq.cashEntryIds.add(cashEntries.get(0).Id);
                cashEntries.remove(0);
            }
            
            System.enqueueJob(pceq); 
        }
    }
    
    //execute posting
    public void execute(QueueableContext ctx) {

        Integer linesAdded = 0;
        list<c2g__codaCashEntry__c> cashEntriesToPost = new list<c2g__codaCashEntry__c>();
        
        list<c2g__codaCashEntry__c> cashEntries = [SELECT Id, (SELECT Id FROM c2g__CashEntryLineItems__r) 
                                                     FROM c2g__codaCashEntry__c 
                                                    WHERE Id IN :cashEntryIds AND 
                                                          c2g__Status__c = 'In Progress' AND 
                                                          Id IN (SELECT c2g__CashEntry__c
                                                                   FROM c2g__codaCashEntryLineItem__c 
                                                                  WHERE c2g__Account__c != null AND
                                                                        c2g__CashEntry__r.c2g__Status__c = 'In Progress')
                                                 ORDER BY c2g__Account__c]; 
                                                                 
        while(linesAdded < batchSize && !cashEntries.isEmpty()) {
            linesAdded += cashEntries.get(0).c2g__CashEntryLineItems__r.size();
            cashEntriesToPost.add(cashEntries.remove(0));
        }
        
        //create ff objects
        c2g.CODAAPICommon_7_0.Context context = new c2g.CODAAPICommon_7_0.Context();

        List<c2g.CODAAPICommon.Reference> referenceList = new List<c2g.CODAAPICommon.Reference>();

        for (c2g__codaCashEntry__c cashEntry : cashEntriesToPost) {
            // Moved the reference from outside the loop to inside the loop - franz.g.a.dimaapi
            c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
            reference.Id = cashEntry.Id;
            referenceList.add(reference);
        }

        if(!referenceList.isEmpty()) {
            try {
                //Call Bulk Posting for CashEntry API
                c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(context, referenceList);
            }
            //error logging
            catch (Exception e) {
                list<ffps_custRem__Custom_Log__c> errorLogs = new list<ffps_custRem__Custom_Log__c>();
                for (c2g__codaCashEntry__c cashEntry : cashEntriesToPost){
                    errorLogs.add(createErrorLogs(String.valueof(e.getTypeName()), String.valueof(e.getMessage()), cashEntry.Id));
                }
                insert errorLogs;
            }
        }
        
        //launch next queueable
        if(!cashEntries.isEmpty()) {
            JVCO_PostCashEntryQueueable pceq = new JVCO_PostCashEntryQueueable();
            pceq.batchSize = batchSize;
            pceq.cashEntryIds = new set<Id>();
            
            for(c2g__codaCashEntry__c cashEntry : cashEntries) {
                pceq.cashEntryIds.add(cashEntry.Id);
            }
            
            System.enqueueJob(pceq);
        }
    }
    public static ffps_custRem__Custom_Log__c createErrorLogs(String nameStr, String messageStr, Id cashEntry){
        ffps_custRem__Custom_Log__c newErr = new ffps_custRem__Custom_Log__c();
        newErr.ffps_custRem__Running_User__c = UserInfo.getUserId();
        newErr.ffps_custRem__Grouping__c = nameStr;
        newErr.ffps_custRem__Detail__c = messageStr;
        newErr.ffps_custRem__Message__c = 'Posting of Cash Entry';
        newErr.ffps_custRem__Related_Object_Key__c = String.valueOf(cashEntry);
        return newErr;
    }
}