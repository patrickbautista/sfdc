/* ----------------------------------------------------------------------------------------------------------
   Name:            JVCO_ContentDocumentTriggerHandler.cls 
   Description:     Handler Class for JVCO_ContentDocumentTrigger
   Test class:      JVCO_ContentDocumentTriggerHandlerTest.cls 

   Date             Version     Author                            Summary of Changes 
   -----------      -------     -----------------                 -------------------------------------------
  06-Apr-2018       0.1         Accenture-recuerdo.b.bregente     Initial draft           
  ---------------------------------------------------------------------------------------------------------- */
Public Class JVCO_ContentDocumentTriggerHandler{
    
    public static void onBeforeUpdate(Map<Id, ContentDocument> contentDocs){
        restrictUpdateAndDelete(contentDocs, 'Updating');
    }
    
    public static void onBeforeDelete(Map<Id, ContentDocument> contentDocs){
        restrictUpdateAndDelete(contentDocs, 'Deleting');
    }
    
    private static void restrictUpdateAndDelete(Map<Id, ContentDocument> contentDocs, String operation){
        Map<Id, Id> docAccIds = getDocAccountIds(contentDocs.keySet());
        Map<Id, Account> restrictedAccountsMap = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN :docAccIds.values() AND 
                                                    (JVCO_In_Enforcement__c = true OR JVCO_In_Infringement__c = true) AND 
                                                    (JVCO_Credit_Status__c = 'Enforcement - Debt Recovery' OR JVCO_Credit_Status__c = 'Enforcement - Infringement')]);
                                                            
        Boolean isRestricted = (restrictedAccountsMap.size() > 0 && checkUserIfRestricted());
       
        for(ContentDocument contentDoc : contentDocs.values()){
            if(isRestricted && restrictedAccountsMap.containsKey(docAccIds.get(contentDoc.Id))){
                contentDoc.addError(operation + ' Tasks on Accounts marked "In Enforcement" or "In Infringement" is not allowed.');
            }
        }
    }
    
    private static Map<Id, Id> getDocAccountIds(Set<Id> contentDocIds){
        Map<Id, Id> docAccIds = new Map<Id, Id>();
        for(ContentDocumentLink link : [SELECT Id, LinkedEntityId, ContentDocumentId FROM 
                                        ContentDocumentLink WHERE ContentDocumentId IN :contentDocIds]){
            docAccIds.put(link.ContentDocumentId, link.LinkedEntityId);
        }
        
        return docAccIds;
    }
    
    private static Boolean checkUserIfRestricted(){
        String profileName = [SELECT Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
        Map<Id,PermissionSet> enAdPermSet = new Map<Id,PermissionSet>([SELECT Id FROM PermissionSet WHERE Name = 'JVCO_Enforcement_Advisors']);
        List<PermissionSetAssignment> permissionSetList = [SELECT Id FROM PermissionSetAssignment WHERE 
                                                            AssigneeId = :UserInfo.getUserId() AND PermissionSetId IN :enAdPermSet.keySet()];
                                                            
        return (profileName <> 'System Administrator' && permissionSetList.isEmpty());
    }
}