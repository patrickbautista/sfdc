@isTest
private class JVCO_DM_UpdateExcludeFromDunningTest
{
    @testSetup
    static void createTestData()
    {
        JVCO_TestClassObjectBuilder.createBillingConfig();
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue ; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            List<queuesobject >  listQueue = new List<queuesobject >();
            queuesobject q1 = new queuesobject (queueid=testGroup.id, sobjecttype='Case'); 
            listQueue.add(q1);
            queuesobject q2 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaAccountingCurrency__c'); 
            listQueue.add(q2);
            queuesobject q3 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCompany__c'); 
            listQueue.add(q3);
            queuesobject q4 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaYear__c'); 
            listQueue.add(q4);
            queuesobject q5 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaInvoice__c'); 
            listQueue.add(q5);
            queuesobject q6 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankAccount__c'); 
            listQueue.add(q6);
            queuesobject q7 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaGeneralLedgerAccount__c'); 
            listQueue.add(q7);
            queuesobject q8 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaBankStatement__c'); 
            listQueue.add(q8);          
            queuesobject q9 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaCashEntry__c'); 
            listQueue.add(q9);  
            queuesobject q10 = new queuesobject (queueid=testGroup.id, sobjecttype='PAYBASE2__Payment__c'); 
            listQueue.add(q10);
            queuesobject q11 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransaction__c'); 
            listQueue.add(q11);
            //queuesobject q12 = new queuesobject (queueid=testGroup.id, sobjecttype='c2g__codaTransactionLineItem__c'); 
            //listQueue.add(q12);
            insert listQueue;

            GroupMember GroupMemberObj = new GroupMember();
            GroupMemberObj.GroupId = testGroup.id;
            GroupMemberObj.UserOrGroupId = UserInfo.getUserId();
            insert GroupMemberObj;
        }
        
         //Get record type of Account
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }

        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAcc= JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert testGeneralLedgerAcc;
      
        c2g__codaCompany__c testCompany= JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert testCompany;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(testGeneralLedgerAcc.Id);
        insert taxCode;
        
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;

        //Create Account               
        Account testAccCust = JVCO_TestClassHelper.setCustAcc(testGeneralLedgerAcc.Id, dim1.Id);
        insert testAccCust ;
        Contact c = JVCO_TestClassHelper.setContact(testAccCust.id);
        insert c;
        Account testLicAcc = JVCO_TestClassHelper.setLicAcc(testAccCust.Id, taxCode.Id, c.Id,testGeneralLedgerAcc.Id);
        testLicAcc.JVCO_Credit_Status__c = 'Excluded from Dunning';
        insert testLicAcc;
        

        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(testCompany.Id, testGroup.Id);
        insert accCurrency;
        

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(testCompany.Id, testGroup.Id);
        insert yr;
        
        insert JVCO_TestClassHelper.setPeriod(testCompany.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(testCompany.Id);

        
        
        
        
    }
    
    @isTest
    static void thisShould()
    {
        JVCO_DM_UpdateExcludeFromDunning b = new JVCO_DM_UpdateExcludeFromDunning();
        Database.executeBatch(b);
    }
    
}