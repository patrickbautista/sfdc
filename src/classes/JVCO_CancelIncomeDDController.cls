/* ----------------------------------------------------------------------------------------------
    Name: JVCO_CancelIncomeDDController.cls 
    Description: Business logic class selecting Income Direct Debits to be cancelled

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    14-Apr-2020   0.1        robert.j.b.lacatan  Intial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_CancelIncomeDDController{
     public ApexPages.StandardSetController stdCtrl;
     private List<Income_Direct_Debit__c> selectedIncomeDD;
     public integer numberOfincomeDD {get; set;}
     public List<Income_Direct_Debit__c> retrievedIncomeDD {get; set;}
     public Boolean isProcessed {get; set;}
     public String prompt {get; set;}
     public Map<id, String> incomeDDAndInvoiceGroupMap = new Map<id, string>();
     public List<JVCO_Invoice_Group__c> invoiceGroupList = new List<JVCO_Invoice_Group__c>();
     public set<id> updatedIncomeDDSet = new set<id>();
     
     
     public List<selectIncomeDDWrapper> selectIncomeDDWrapperList {get; set;}
/* ----------------------------------------------------------------------------------------------
    Author: robert.j.b.lacatan
    Company: Accenture
    Description: Class constructor
    Inputs: StandardSetController
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    14-Apr-2020 robert.j.b.lacatan  Initial version of function
  ----------------------------------------------------------------------------------------------- */    
     public JVCO_CancelIncomeDDController(ApexPages.StandardSetController ssc)
    {
        stdCtrl = ssc;
        selectedIncomeDD = (List<Income_Direct_Debit__c>)ssc.getSelected();
        numberOfincomeDD = selectedIncomeDD.size();
        
    } 
/* ----------------------------------------------------------------------------------------------
    Author: robert.j.b.lacatan
    Company: Accenture
    Description: Wrapper class to contain records to be displayed in VF page
    Inputs: N/A
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    14-Apr-2020 robert.j.b.lacatan  Initial version of function
  ----------------------------------------------------------------------------------------------- */ 
    public class selectIncomeDDWrapper{
        public id incomeDDId {get;set;}
        public string incomeDDName {get;set;}
        public string paymentSchedule {get;set;}
        public string invoiceGroup {get;set;}
        public double totalAmountCollected {get;set;}
        public string statusIncomeDD {get;set;}
        public double amountLeftToCollect {get;set;}
    } 
    
/* ----------------------------------------------------------------------------------------------
    Author: robert.j.b.lacatan
    Company: Accenture
    Description: Check for selected Income DD record
    Inputs: N/A
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    14-Apr-2020 robert.j.b.lacatan  Initial version of function
  ----------------------------------------------------------------------------------------------- */     
    public PageReference init()
    {
        if(numberOfincomeDD > 0)
        {    system.debug('stdcontroller: '+selectedIncomeDD);
            retrievedIncomeDD = [SELECT Id, DD_Status__c, Name, DD_Payment_Schedule__c, DD_Total_Amount_Debited__c, Send_DD_Documentation__c, Amount_Left_To_Collect__c
                                   FROM Income_Direct_Debit__c
                                   WHERE Id IN :selectedIncomeDD];
            isProcessed = FALSE;
            system.debug('retrieved: '+retrievedIncomeDD);
            populateTablevalue(retrievedIncomeDD);
            system.debug('wrapperList: '+selectIncomeDDWrapperList);
            for(Income_Direct_Debit__c incomeDD : retrievedIncomeDD)
            {
                if(incomeDD.DD_Status__c.equals('Cancelled'))
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.JVCO_SelectCancelledIdd));
                    numberOfincomeDD = 0; // To simulate no records selected
                }
            }
            prompt = System.Label.JVCO_IddPromptPage;
            
        }else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.JVCO_NoPaymentSelected));
        }
        return null;
    }
/* ----------------------------------------------------------------------------------------------
    Author: robert.j.b.lacatan
    Company: Accenture
    Description: Populate wrapper class
    Inputs: Income_Direct_Debit__c
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    14-Apr-2020 robert.j.b.lacatan  Initial version of function
  ----------------------------------------------------------------------------------------------- */ 
    public void populateTablevalue(List<Income_Direct_Debit__c> incomeDDList){

        for(SMP_DirectDebit_GroupInvoice__c smpGroupRec : [SELECT id, Income_Direct_Debit__c, Invoice_Group__c,Invoice_Group__r.Name, Invoice_Group__r.JVCO_Status__c 
                                                            FROM SMP_DirectDebit_GroupInvoice__c where Income_Direct_Debit__c IN:selectedIncomeDD])
        {
            if(incomeDDAndInvoiceGroupMap.containsKey(smpGroupRec.Income_Direct_Debit__c)){
                string appendInvoiceGroup = incomeDDAndInvoiceGroupMap.get(smpGroupRec.Income_Direct_Debit__c);
                appendInvoiceGroup = appendInvoiceGroup + ',' + smpGroupRec.Invoice_Group__r.Name;
                incomeDDAndInvoiceGroupMap.put(smpGroupRec.Income_Direct_Debit__c, appendInvoiceGroup);
            }else{
                incomeDDAndInvoiceGroupMap.put(smpGroupRec.Income_Direct_Debit__c, string.valueOf(smpGroupRec.Invoice_Group__r.Name));
            }
            
            if(smpGroupRec.Invoice_Group__c!=null){
                JVCO_Invoice_Group__c invGroupRec = new JVCO_Invoice_Group__c();
                invGroupRec.id = smpGroupRec.Invoice_Group__c;
                invGroupRec.JVCO_Status__c = smpGroupRec.Invoice_Group__r.JVCO_Status__c;
                invoiceGroupList.add(invGroupRec);

            }
            
            


        }

        
        selectIncomeDDWrapperList = new List <selectIncomeDDWrapper>();
        for(Income_Direct_Debit__c incomeDDRec: incomeDDList){
            system.debug('incomeDDAndInvoiceGroupMap: '+incomeDDAndInvoiceGroupMap);
            if(incomeDDAndInvoiceGroupMap.containsKey(incomeDDRec.id)){
                selectIncomeDDWrapper selectIncomeDDWrapperRec = new selectIncomeDDWrapper();
                selectIncomeDDWrapperRec.incomeDDId = incomeDDRec.id;
                selectIncomeDDWrapperRec.incomeDDName = incomeDDRec.Name;
                selectIncomeDDWrapperRec.paymentSchedule = incomeDDRec.DD_Payment_Schedule__c;
                selectIncomeDDWrapperRec.invoiceGroup = incomeDDAndInvoiceGroupMap.get(incomeDDRec.id);
                selectIncomeDDWrapperRec.totalAmountCollected = incomeDDRec.DD_Total_Amount_Debited__c;
                selectIncomeDDWrapperRec.statusIncomeDD = incomeDDRec.DD_Status__c;
                selectIncomeDDWrapperRec.amountLeftToCollect= incomeDDRec.Amount_Left_To_Collect__c;
                selectIncomeDDWrapperList.add(selectIncomeDDWrapperRec);

            }

        }



    }
/* ----------------------------------------------------------------------------------------------
    Author: robert.j.b.lacatan
    Company: Accenture
    Description: Cancel Income DD
    Inputs: N/A
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    14-Apr-2020 robert.j.b.lacatan  Initial version of function
  ----------------------------------------------------------------------------------------------- */    
    public void cancelIncomeDD()
    {
        for(Income_Direct_Debit__c incomeDD : retrievedIncomeDD)
        {
            incomeDD.DD_Status__c= 'Cancelled';
            incomeDD.Send_DD_Documentation__c = 'Direct Debit Cancellation';
        }
        
        //update retrievedIncomeDD;
        Database.SaveResult[] srList = Database.update(retrievedIncomeDD, false);
        for (Database.SaveResult sr : srList) 
        {
            if(sr.isSuccess()){
                updatedIncomeDDSet.add(sr.getId());
            }
        }
        
        for(selectIncomeDDWrapper rec : selectIncomeDDWrapperList)
        {   
            if(updatedIncomeDDSet.contains(rec.incomeDDId))
            {
                rec.statusIncomeDD = 'Cancelled';
            }
            
        }

        updateInvoiceGroup(invoiceGroupList);
        prompt = System.Label.JVCO_IddProcessed;
        isProcessed = TRUE;
    }
    
   /* 
     public PageReference returnToAccount()
    {
        PageReference page = new PageReference(stdCtrl.cancel().getUrl());
        page.setRedirect(TRUE);
        return page;
    }
    */
/* ----------------------------------------------------------------------------------------------
    Author: robert.j.b.lacatan
    Company: Accenture
    Description: Update invoice group records after cancelling income DD
    Inputs: JVCO_Invoice_Group__c
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    14-Apr-2020 robert.j.b.lacatan  Initial version of function
  ----------------------------------------------------------------------------------------------- */    
    public void updateInvoiceGroup(List<JVCO_Invoice_Group__c> invGroupList)
    {
        for(JVCO_Invoice_Group__c rec: invGroupList){
            if(rec.JVCO_Status__c != 'Retired'){
                rec.JVCO_Status__c = 'Retired';
            }

        update invGroupList;
        updateSaleInvoice();
        }
    }
/* ----------------------------------------------------------------------------------------------
    Author: robert.j.b.lacatan
    Company: Accenture
    Description: Update Sales Invoice records after cancelling income DD
    Inputs: N/A
    Returns: N/A
    <Date>      <Authors Name>      <Brief Description of Change> 
    14-Apr-2020 robert.j.b.lacatan  Initial version of function
  ----------------------------------------------------------------------------------------------- */
    public void updateSaleInvoice(){
        set<id> invGroupIdSet = new set<id>();
        
        for(JVCO_Invoice_Group__c rec : invoiceGroupList){
            invGroupIdSet.add(rec.id);
        }

        List<c2g__codaInvoice__c> salesInvoiceList = [SELECT id, JVCO_Invoice_Group__c, 
                                                      JVCO_Stop_Surcharge__c, 
                                                      ffps_custRem__Exclude_From_Reminder_Process__c, 
                                                      JVCO_Exempt_from_Surcharge__c,
                                                      JVCO_Payment_Method__c
                                                      FROM c2g__codaInvoice__c 
                                                      WHERE JVCO_Invoice_Group__c IN: invGroupIdSet];
        for(c2g__codaInvoice__c rec: salesInvoiceList){
            rec.JVCO_Stop_Surcharge__c = false;
            rec.JVCO_Invoice_Group__c = null;
            rec.JVCO_Exempt_from_Surcharge__c = false;
            rec.ffps_custRem__Exclude_From_Reminder_Process__c = false;
            rec.JVCO_Payment_Method__c = null;
        }

        update salesInvoiceList;
        //populateTablevalue(retrievedIncomeDD);
        //isProcessed = TRUE;

        
    }
    
       
    
}