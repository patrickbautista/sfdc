/* ----------------------------------------------------------------------------------------------
    Name: JVCO_ContractRenewQuotesExtension
    Description: Extension Class for JVCO_ContractRenewQuotes page

    Date            Version     Author              Summary of Changes 
    -----------     -------     -----------------   -----------------------------------------
    13-Mar-2017     0.1         Marlon Ocillos      Intial creation
    10-Oct-2017     0.2         Jules Pablo         Added GREEN-23873 Fix, updated oppsToProcess query
    06-Mar-2018     0.3         Jules Pablo         Added GREEN-25351 Fix, added an error message to be displayed when there are uncontracted Amendment Quotes
    23-May-2018     0.4         jules.osberg.a.pablo  Added GREEN-25966 Enhancement, to prevent KA agent from contracting renewal before sending Venue Summary
    29-Jun-2018     0.5         jules.osberg.a.pablo  GREEN-31760, GREEN-32139 Added requirement of T&C's before Contracting Quotes.
    10-Aug-2018     0.6         jules.osberg.a.pablo Added Queueable processing
    27-Sept-2018    0.7         rhys.j.c.dela.cruz   Added Queueable Status
    19-Nov-2018     0.8         rhys.j.c.dela.cruz   GREEN-34012 - Moved KA Quote Usage Summary Validation to Contract Reniew/Review and Complete Quote Buttons
    22-Mar-2109     0.9         mel.andrei.santos    excluding 0 net amount amendment quotes GREEN-33800
    08-Apr-2019     1.0         Rhys Dela Cruz      GREEN-34460 - include for recalc Opps with blank Primary Quote field
    22-May-2019     1.1         rhys.j.c.dela.cruz   GREEN-33618 - Changes to queueable to accomodate new Temp Object
----------------------------------------------------------------------------------------------- */
public class JVCO_ContractRenewQuotesExtension 
{
    public Account fetchedAccountRecord;
    public Account accountRecord {get; set;}
    public Integer contractBatchSize;
    public Boolean isContinued {get; set;}
    public Boolean isStopForError {get; set;}
    public List<Opportunity> oppsToProcess {get; set;}
    public Integer numberOfRecords {get; set;}
    public Integer uncalculatedQuotesCount {get; set;}
    public List<SBQQ__Quote__c> uncalculatedQuotesList {get; set;}
    public Boolean isProcessing {get; set;}
    public String apexJobId {get; set;}
    public Integer numberOfAmendRecords {get; set;}
    public Boolean venueSummarySent {get; set;}
    public Boolean tcAccepted {get; set;}
    public string selectedTC {get;set;}
    public Boolean queueableRunning {get;set;}
    public Boolean canContinue {get;set;}
    public Boolean kaUsageSummarySent {get;set;}
    private JVCO_TermsAndConditionHelper termsAndConditionHelper;

    public JVCO_ContractRenewQuotesExtension  (ApexPages.StandardController stdController) 
    {
        isStopForError = false;
        isContinued = false;
        oppsToProcess = new List<Opportunity>();
        numberOfRecords = 0;
        uncalculatedQuotesCount = 0;
        uncalculatedQuotesList = new List<SBQQ__Quote__c>();
        fetchedAccountRecord = (Account)stdController.getRecord();
        termsAndConditionHelper = new JVCO_TermsAndConditionHelper();

        accountRecord = [select Id, Name, JVCO_Active__c, JVCO_Billing_Contact__c, JVCO_Billing_Contact__r.MailingStreet, JVCO_Billing_Contact__r.MailingPostalCode, JVCO_Review_Contact__c, JVCO_Review_Contact__r.MailingStreet, JVCO_Review_Contact__r.MailingPostalCode, JVCO_Licence_Holder__c, JVCO_Licence_Holder__r.MailingStreet, JVCO_Licence_Holder__r.MailingPostalCode,ffps_custRem__Preferred_Communication_Channel__c, JVCO_Contract_Batch_Size__c, JVCO_ProcessingContractRenewQuotes__c, JVCO_ContractRenewQuotesApexJob__c,JVCO_Account_Description__c,JVCO_ProcessingOrderRenewQuotes__c,JVCO_OrderRenewQuotesApexJob__c, JVCO_Customer_Account__r.TCs_Accepted__c, JVCO_ContractRenewQuotesQueueable__c, Type from Account where Id =: fetchedAccountRecord.Id];


        if(!accountRecord.JVCO_Active__c){
            ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, 'Account is not active.'));
            isStopForError = true;
        }

        if(accountRecord.JVCO_Billing_Contact__c != null && accountRecord.ffps_custRem__Preferred_Communication_Channel__c == 'Print'){
            if(accountRecord.JVCO_Billing_Contact__r.MailingStreet == null || accountRecord.JVCO_Billing_Contact__r.MailingPostalCode == null){
                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, 'The contact does not have an address. Either change the Preferred Comms Method to Email or add an address to the contact'));
                isStopForError = true;
            }
        }

        if(accountRecord.JVCO_Licence_Holder__c != null && accountRecord.ffps_custRem__Preferred_Communication_Channel__c == 'Print'){
            if(accountRecord.JVCO_Licence_Holder__r.MailingStreet == null|| accountRecord.JVCO_Licence_Holder__r.MailingPostalCode == null){
                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, 'The contact does not have an address. Either change the Preferred Comms Method to Email or add an address to the contact'));
                isStopForError = true;
            }
        }

        if(accountRecord.JVCO_Billing_Contact__c != null && accountRecord.ffps_custRem__Preferred_Communication_Channel__c == 'Print'){
            if(accountRecord.JVCO_Billing_Contact__r.MailingStreet == null || accountRecord.JVCO_Billing_Contact__r.MailingPostalCode == null){
                ApexPages.addmessage( new ApexPages.message(ApexPages.severity.ERROR, 'The contact does not have an address. Either change the Preferred Comms Method to Email or add an address to the contact'));
                isStopForError = true;
            }
        }
        
          // isProcessing = accountRecord.JVCO_ProcessingContractRenewQuotes__c;
         //apexJobId = accountRecord.JVCO_ContractRenewQuotesApexJob__c;
        queueableRunning = false;
        if (accountRecord.JVCO_ProcessingContractRenewQuotes__c == true ){
            isProcessing = true;
            apexJobId = accountRecord.JVCO_ContractRenewQuotesApexJob__c;
        } 
        else if (accountRecord.JVCO_ProcessingOrderRenewQuotes__c == true) {
            isProcessing = true;
            apexJobId = accountRecord.JVCO_OrderRenewQuotesApexJob__c;
        }
        else if(accountRecord.JVCO_ContractRenewQuotesQueueable__c != null){
            queueableRunning = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Contracting of Renew Quotes for ' + accountRecord.Name + ' is currently processing, please wait for the job to finish.'));
        }
        else {
           isProcessing = false;
           apexJobId = NULL;
        }
        
        venueSummarySent = false;
        AggregateResult[] qlLastModifiedDate = [SELECT Max(LastModifiedDate) LastModifiedDate FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__r.JVCO_Invoiced__c = false AND SBQQ__Quote__r.SBQQ__Account__c = :accountRecord.Id];

        AggregateResult[] vsLastModifiedDate = [SELECT Max(LastModifiedDate) LastModifiedDate FROM JVCO_Document_Queue__c WHERE JVCO_Scenario__c = 'Key Account Venue Summary XLS' AND JVCO_Related_Account__c = :accountRecord.Id];
     
        if(Datetime.valueOf(vsLastModifiedDate[0].get('LastModifiedDate')) >= Datetime.valueOf(qlLastModifiedDate[0].get('LastModifiedDate'))) {
            venueSummarySent = true;
        }   

        if (accountRecord.JVCO_Contract_Batch_Size__c != null && accountRecord.JVCO_Contract_Batch_Size__c != 0) 
        {
            contractBatchSize = (Integer)accountRecord.JVCO_Contract_Batch_Size__c;   
        } else {
            contractBatchSize = 1;
        }
        
        //oppsToProcess = [select Id, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName from Opportunity where SBQQ__Contracted__c = false and AccountId =: accountRecord.Id];
        //oppsToProcess = [select Id, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName from Opportunity where SBQQ__Contracted__c = false and SBQQ__RenewedContract__c != NULL and AccountId =: accountRecord.Id];// jules.osberg.a.pablo 11-10-2017 GREEN-23873
        
        //rhys.j.c.dela.cruz - Change query if Queueable or Not
        if(JVCO_KABatchSetting__c.getInstance(UserInfo.getUserId()).JVCO_ContractRenewQuotesQueueable__c){

            oppsToProcess = [select Id, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName, SBQQ__PrimaryQuote__c from Opportunity where SBQQ__PrimaryQuote__r.JVCO_Number_Quote_Lines__c != 0 AND SBQQ__PrimaryQuote__r.JVCO_Number_Quote_Lines__c != null AND (SBQQ__Contracted__c = false OR (SBQQ__Contracted__c = true AND SBQQ__Ordered__c = false)) and ( SBQQ__PrimaryQuote__r.SBQQ__Type__c IN ('Renewal','Quote') or (SBQQ__PrimaryQuote__c = null AND (SBQQ__Renewal__c = true OR (SBQQ__Renewal__c = false AND SBQQ__AmendedContract__c = null) ) ) ) and AccountId =: accountRecord.Id]; 
        }
        else{

            oppsToProcess = [select Id, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName, SBQQ__PrimaryQuote__c from Opportunity where SBQQ__PrimaryQuote__r.JVCO_Number_Quote_Lines__c != 0 AND SBQQ__PrimaryQuote__r.JVCO_Number_Quote_Lines__c != null and SBQQ__Contracted__c = false and ( SBQQ__PrimaryQuote__r.SBQQ__Type__c IN ('Renewal','Quote') or (SBQQ__PrimaryQuote__c = null AND (SBQQ__Renewal__c = true OR (SBQQ__Renewal__c = false AND SBQQ__AmendedContract__c = null) ) ) ) and AccountId =: accountRecord.Id]; 
        }
        // csaba.feher 12/04/2017 GREEN-26653 //// mel.andrei.b.santos 12-Apr-2018 GREEN-31233 - added condition if primary quote = null
        //  mel.andrei.b.santos 29-10-2018 GREEN-32399 - added condition to prevent processing quotes without Quote LInes

        numberOfRecords = oppsToProcess.size();
        //uncalculatedQuotesList = [select Id, Name, SBQQ__Type__c, JVCO_Salesforce_Last_Recalculated_Time__c, Start_Date__c, End_Date__c from SBQQ__Quote__c WHERE JVCO_Recalculated__c = false AND SBQQ__Opportunity2__r.SBQQ__Contracted__c = false AND SBQQ__Account__c =: accountRecord.Id AND SBQQ__Type__c = 'Renewal' LIMIT 30];

        //08-Apr-2019 rhys.j.c.dela.cruz GREEN-34460 - adjusted query, include for recalc Opps with blank Primary Quote field
        uncalculatedQuotesList = [select Id, Name, SBQQ__Type__c, JVCO_Salesforce_Last_Recalculated_Time__c, Start_Date__c, End_Date__c from SBQQ__Quote__c WHERE (JVCO_Recalculated__c = false OR SBQQ__Opportunity2__r.SBQQ__PrimaryQuote__c = null) AND SBQQ__Opportunity2__r.SBQQ__Contracted__c = false AND SBQQ__Account__c =: accountRecord.Id AND SBQQ__Type__c IN ('Renewal','Quote') LIMIT 30]; // csaba.feher 12/04/2017 GREEN-26653
        
        if(!uncalculatedQuotesList.isEmpty()) {
            uncalculatedQuotesCount = uncalculatedQuotesList.size();
            //uncalculatedQuotesCount = [select count()from SBQQ__Quote__c WHERE JVCO_Recalculated__c = false AND SBQQ__Opportunity2__r.SBQQ__Contracted__c = false AND SBQQ__Account__c =: accountRecord.Id AND SBQQ__Type__c = 'Renewal'];
        }

        //START jules.osberg.a.pablo 06-03-2018 GREEN-25351
        numberOfAmendRecords = 0;
        AggregateResult[] amendQuoteCount = [Select count(Id) from SBQQ__Quote__c where SBQQ__Account__c = :accountRecord.Id AND SBQQ__Status__c = 'Draft' AND SBQQ__Type__c = 'Amendment' AND SBQQ__NetAmount__c != 0];
        if(amendQuoteCount[0].get('expr0') != NULL) {
            if(integer.valueof(amendQuoteCount[0].get('expr0')) > 0) 
            {
                numberOfAmendRecords = integer.valueof(amendQuoteCount[0].get('expr0'));
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'You cannot contract Renewal Quotes as there are uncontracted Amendment Quotes on this Account. Please contract these before continuing.'));
                isStopForError = true;
            }
        }
        //END jules.osberg.a.pablo 06-03-2018 GREEN-25351
        // 22-Mar mel.andrei.santos GREEN-33800 - excluding 0 net amount amendment quotes 

        //Batch Job Helper set Boolean, to prevent batches to reach limit
        canContinue = true;
        Boolean isQueueable = JVCO_KABatchSetting__c.getInstance(userinfo.getProfileId()).JVCO_ContractReviewQuotesQueueable__c || JVCO_KABatchSetting__c.getInstance(userinfo.getUserId()).JVCO_ContractReviewQuotesQueueable__c;
        if(!isQueueable){
            canContinue = JVCO_BatchJobHelper.checkLargeJobs(oppsToProcess.size());
            if(!canContinue){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.JVCO_BatchJobHelperCannotProcess));
            }
        }

        //GREEN-34012
        kaUsageSummarySent = false;
        
        List<SBQQ__QuoteLine__c> relatedQuoteLine = [Select Id, CreatedDate, LastModifiedDate from SBQQ__QuoteLine__c where SBQQ__Quote__r.SBQQ__Account__c = :accountRecord.Id order by LastModifiedDate desc limit 1];

        List<JVCO_Document_Queue__c> relatedDQ  = [select id, CreatedDate from JVCO_Document_Queue__c where JVCO_Related_Account__c = :accountRecord.Id AND (JVCO_Scenario__c = 'Key Account Quote Usage Summary PDF' OR JVCO_Scenario__c = 'Key Account Quote Usage Summary XLS' ) order by CreatedDate desc limit 1];

        if(relatedDQ.isEmpty() || ((!relatedQuoteLine.isEmpty() && !relatedDQ.isEmpty()) && (relatedQuoteLine[0].LastModifiedDate > relatedDQ[0].CreatedDate || relatedQuoteLine[0].CreatedDate > relatedDQ[0].CreatedDate))){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Quote Usage Summary must be generated first before Contracting Renew Quotes'));
                kaUsageSummarySent = false;
        }else{
                kaUsageSummarySent = true;
        }

        tcAccepted = termsAndConditionHelper.tcIsYes(accountRecord.JVCO_Customer_Account__r.TCs_Accepted__c);
        tcAccepted = termsAndConditionHelper.bipassTCWindow(oppsToProcess, tcAccepted);
        selectedTC = accountRecord.JVCO_Customer_Account__r.TCs_Accepted__c;
    }
    
    public List<Selectoption> getselectedTCfields(){
        return termsAndConditionHelper.getselectedTCfields(accountRecord.JVCO_Customer_Account__r.TCs_Accepted__c); 
    }

    public PageReference queueContractRenewQuotes()
    {
        if(selectedTC == null && tcAccepted == false){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.TermsAndConditionErrorText));
        }else{
            if(termsAndConditionHelper.updateCustomerTC(selectedTC, accountRecord.JVCO_Customer_Account__r.TCs_Accepted__c, accountRecord.JVCO_Customer_Account__c)){
                tcAccepted = true;

                if(JVCO_KABatchSetting__c.getInstance(userinfo.getUserId()).JVCO_ContractRenewQuotesQueueable__c) {
                    Id queueableId = System.enqueueJob(new JVCO_ContractRenewQuotes_Queueable(accountRecord));
                    //KA Queueable Update Processing Status
                    JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_ContractRenewQuotesQueueable__c', 'Queueable','JVCO_ContractRenewQuotesLastSubmitted__c');

                    JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
                    kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractRenew';
                    kaTempRec.JVCO_AccountId__c = accountRecord.Id;
                    kaTempRec.JVCO_JobId__c = queueableId;

                    upsert kaTempRec Name;

                } else {
                    Id batchId = database.executeBatch(new JVCO_ContractRenewQuotesBatch(accountRecord, numberOfRecords), contractBatchSize);
                    JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_ContractRenewQuotesApexJob__c', batchId,'JVCO_ContractRenewQuotesLastSubmitted__c');
                }

                isContinued = true;
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.TermsAndConditionAccUpdateErrorText));
            }
        }

        return null; 
    }

    public PageReference backToRecord()
    {
        PageReference pRef = new PageReference('/' + accountRecord.Id);
        return pRef;
    }

    //GREEN-33789
    public PageReference jobCheck(){
        
        Id batchId = apexJobId;
        Id accountId = fetchedAccountRecord.Id;

        PageReference tempRef;

        JVCO_ApexJobProgressBarController progBar = new JVCO_ApexJobProgressBarController();

        if(!queueableRunning){

            tempRef = progBar.updateAcc(batchId, accountId);
        }
        else{

            tempRef = progBar.updateAcc(accountId, 'ContractRenew');   
        }

        return tempRef;
    }
}