/* ----------------------------------------------------------------------------------------------
Name: GREEN-20475 - JVCO_PopulateAccountExternalId.cls 
Description: GREEN-16187 Batch class that populates Tax code to GB-O-STD-LIC
Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
09-May-2017  0.1         kristoffer.d.martin   Intial creation
----------------------------------------------------------------------------------------------- */

global class JVCO_PopulateAccountExternalId implements Database.Batchable<sObject> {
	
	private static List<Account> accountList;
	private static List<Account> accountListForUpdate;

    private static void init(){

    	accountListForUpdate = new List<Account>();

        for(Account acc : accountList)
        {
        	acc.c2g__CODAExternalId__c = acc.AccountNumber;
        	accountListForUpdate.add(acc);
        }
        
        if (!accountListForUpdate.isEmpty()) 
        {
        	update accountListForUpdate;
        }
        
    }

    global Database.QueryLocator start(Database.BatchableContext bc){

        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> accountRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {
          accountRecordTypes.put(rt.Name,rt.Id);
        }

        return Database.getQueryLocator([SELECT Id, Name, RecordTypeId, c2g__CODAExternalId__c, AccountNumber
        								 FROM Account
        								 WHERE RecordTypeId =: accountRecordTypes.get('Licence Account')
        								 AND AccountNumber != null]);

    }

    global void execute(Database.BatchableContext bc, List<Account> scope)
    {
        accountList = scope;
        init();   
    }

    global void finish(Database.BatchableContext BC) 
    {  
        System.debug('DONE');
    }
	
}