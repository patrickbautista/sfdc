/* ----------------------------------------------------------------------------------------------
Name: JVCO_ContractReviewQuotesExtension
Description: Extension Class for JVCO_ContractReviewQuotes page

Date            Version     Author              Summary of Changes 
-----------     -------     -----------------   -----------------------------------------
03-Mar-2017     0.1         Marlon Ocillos      Intial creation
17-Oct-2017     0.2         Jules Pablo         Added GREEN-23493 Fix, changed query to pick amendment and quotes
30-Nov-2017     0.3         August Del Rosario  Added GREEN-26415 Fix, updated the where clause query to only pick up Opportunities that has a quote.
12-Apr-2018     0.4         mel.andrei.b.santos GREEN-31233 - removed SBQQ__PrimaryQuote__c from condition
29-Jun-2018     0.5         jules.osberg.a.pablo GREEN-31760, GREEN-32139 Added requirement of T&C's before Contracting Quotes.
10-Aug-2018     0.6         jules.osberg.a.pablo Added Queueable processing
02-Oct-2018     0.7         rhys.j.c.dela.cruz   Added Queueable Status
20-Oct-2018     0.8         rhys.j.c.dela.cruz   GREEN-33712 - Alert user if latest Venue summary not yet generated
19-Nov-2018     0.9         rhys.j.c.dela.cruz   GREEN-34012 - Moved KA Quote Usage Summary Validation to Contract Reniew/Review and Complete Quote Buttons
22-Mar-2019     1.0         mel.andrei.b.santos   excluding 0 net amount amendment quotes GREEN-33800  
08-Apr-2019     1.1         Rhys Dela Cruz      GREEN-34460 - include for recalc Opps with blank Primary Quote field
22-May-2019     1.2         rhys.j.c.dela.cruz           GREEN-33618 - Changes to queueable to accomodate new Temp Object
----------------------------------------------------------------------------------------------- */
public class JVCO_ContractReviewQuotesExtension 
{
    public Account fetchedAccountRecord;
    public Account accountRecord {get; set;}
    public Integer contractBatchSize;
    public Boolean isContinued {get; set;}
    public List<Opportunity> oppsToProcess {get; set;}
    public Integer numberOfRecords {get; set;}
    public Integer uncalculatedQuotesCount {get; set;}
    public List<SBQQ__Quote__c> uncalculatedQuotesList {get; set;}
    public Boolean isProcessing {get; set;}
    public String apexJobId {get; set;}
    public Boolean tcAccepted {get; set;}
    public string selectedTC {get;set;}
    public Boolean queueableRunning {get;set;}
    public Boolean canContinue{get;set;}
    public Boolean venueSummarySent {get; set;}
    public Boolean kaUsageSummarySent {get;set;}
    private JVCO_TermsAndConditionHelper termsAndConditionHelper;

    public JVCO_ContractReviewQuotesExtension (ApexPages.StandardController stdController) 
    {
        isContinued = false;
        queueableRunning = false; //Queueable checker
        oppsToProcess = new List<Opportunity>();
        numberOfRecords = 0;
        uncalculatedQuotesCount = 0;
        uncalculatedQuotesList = new List<SBQQ__Quote__c>();
        termsAndConditionHelper = new JVCO_TermsAndConditionHelper();
        fetchedAccountRecord = (Account)stdController.getRecord();
        accountRecord = [select Id, Name, JVCO_Contract_Batch_Size__c,JVCO_ProcessingContractReviewQuotes__c,JVCO_ContractReviewQuotesApexJob__c,JVCO_ProcessingOrderReviewQuotes__c,JVCO_OrderReviewQuotesApexJob__c, JVCO_Customer_Account__r.TCs_Accepted__c, JVCO_ContractReviewQuotesQueueable__c from Account where Id =: fetchedAccountRecord.Id];
        //isProcessing = accountRecord.JVCO_ProcessingContractReviewQuotes__c;
        //apexJobId = accountRecord.JVCO_ContractReviewQuotesApexJob__c;
            if (accountRecord.JVCO_ProcessingContractReviewQuotes__c == true ){
               isProcessing = true;
               apexJobId = accountRecord.JVCO_ContractReviewQuotesApexJob__c;
            } else if (accountRecord.JVCO_ProcessingOrderReviewQuotes__c == true){
               isProcessing = true;
               apexJobId = accountRecord.JVCO_OrderReviewQuotesApexJob__c;

            } else if (accountRecord.JVCO_ContractReviewQuotesQueueable__c != null){ //Check if Queueable is Processing
                queueableRunning = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Contracting of Review Quotes for ' + accountRecord.Name + ' is currently processing, please wait for the job to finish.'));
            } else {
               isProcessing = false;
               apexJobId = NULL;
            }
        
        if (accountRecord.JVCO_Contract_Batch_Size__c != null && accountRecord.JVCO_Contract_Batch_Size__c != 0) 
        {
            contractBatchSize = (Integer)accountRecord.JVCO_Contract_Batch_Size__c;   
        } else {
            contractBatchSize = 1;
        }

        venueSummarySent = false;
        AggregateResult[] qlLastModifiedDate = [SELECT Max(LastModifiedDate) LastModifiedDate FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__r.JVCO_Invoiced__c = false AND SBQQ__Quote__r.SBQQ__Account__c = :accountRecord.Id];

        AggregateResult[] vsLastModifiedDate = [SELECT Max(LastModifiedDate) LastModifiedDate FROM JVCO_Document_Queue__c WHERE JVCO_Scenario__c = 'Key Account Venue Summary XLS' AND JVCO_Related_Account__c = :accountRecord.Id];
     
        if(Datetime.valueOf(vsLastModifiedDate[0].get('LastModifiedDate')) >= Datetime.valueOf(qlLastModifiedDate[0].get('LastModifiedDate'))) {
            venueSummarySent = true;
        }
        
        //oppsToProcess = [select Id, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName from Opportunity where SBQQ__Contracted__c = false and AccountId =: accountRecord.Id];

        //rhys.j.c.dela.cruz - Change query if Queueable or Not
        if(JVCO_KABatchSetting__c.getInstance(UserInfo.getUserId()).JVCO_ContractReviewQuotesQueueable__c){
        
            oppsToProcess = [select Id, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName, SBQQ__PrimaryQuote__c from Opportunity where SBQQ__PrimaryQuote__r.JVCO_Number_Quote_Lines__c != 0 and SBQQ__PrimaryQuote__r.JVCO_Number_Quote_Lines__c != null and (SBQQ__Contracted__c = false OR (SBQQ__Contracted__c = true AND SBQQ__Ordered__c = false)) and SBQQ__RenewedContract__c = NULL and AccountId =: accountRecord.Id and SBQQ__PrimaryQuote__r.SBQQ__NetAmount__c != 0];
        }
        else{

            oppsToProcess = [select Id, SBQQ__Contracted__c, SBQQ__Ordered__c, StageName, SBQQ__PrimaryQuote__c from Opportunity where SBQQ__PrimaryQuote__r.JVCO_Number_Quote_Lines__c != 0 and SBQQ__PrimaryQuote__r.JVCO_Number_Quote_Lines__c != null and SBQQ__Contracted__c = false and SBQQ__RenewedContract__c = NULL and AccountId =: accountRecord.Id and SBQQ__PrimaryQuote__r.SBQQ__NetAmount__c != 0];
        }
        // jules.osberg.a.pablo 17-10-2017 GREEN-23493 // louis.a.del.rosario 11-30-2017 GREEN-26415 // mel.andrei.b.santos 12-Apr-2018 GREEN-31233 - removed SBQQ__PrimaryQuote__c from condition
        // // mel.andrei.santos 22-03-2019 GREEN-33800

        numberOfRecords = oppsToProcess.size();

        //08-Apr-19 rhys.j.c.dela.cruz Adjusted query to check if Primary Quote of Opp is null
        uncalculatedQuotesList = [select Id, Name, SBQQ__Type__c, JVCO_Salesforce_Last_Recalculated_Time__c, Start_Date__c, End_Date__c from SBQQ__Quote__c WHERE (JVCO_Recalculated__c = false OR SBQQ__Opportunity2__r.SBQQ__PrimaryQuote__c = null) AND SBQQ__Opportunity2__r.SBQQ__Contracted__c = false AND SBQQ__Account__c =: accountRecord.Id AND (SBQQ__Type__c = 'Quote' OR SBQQ__Type__c = 'Amendment') LIMIT 30];
        
        if(!uncalculatedQuotesList.isEmpty()) {
            uncalculatedQuotesCount = uncalculatedQuotesList.size();
            //uncalculatedQuotesCount = [select count() from SBQQ__Quote__c WHERE JVCO_Recalculated__c = false AND SBQQ__Opportunity2__r.SBQQ__Contracted__c = false AND SBQQ__Account__c =: accountRecord.Id AND (SBQQ__Type__c = 'Quote' OR SBQQ__Type__c = 'Amendment')];
        }


        //Batch Job Helper set Boolean, to prevent batches to reach limit
        canContinue = true;
        Boolean isQueueable = JVCO_KABatchSetting__c.getInstance(userinfo.getProfileId()).JVCO_ContractReviewQuotesQueueable__c || JVCO_KABatchSetting__c.getInstance(userinfo.getUserId()).JVCO_ContractReviewQuotesQueueable__c;
        if(!isQueueable){
            canContinue = JVCO_BatchJobHelper.checkLargeJobs(oppsToProcess.size());
            if(!canContinue){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.JVCO_BatchJobHelperCannotProcess));
            }
        }
        System.debug('canContinue' + canContinue);

        //GREEN-34012
        kaUsageSummarySent = false;
        
        List<SBQQ__QuoteLine__c> relatedQuoteLine = [Select Id, CreatedDate, LastModifiedDate from SBQQ__QuoteLine__c where SBQQ__Quote__r.SBQQ__Account__c = :accountRecord.Id order by LastModifiedDate desc limit 1];

        List<JVCO_Document_Queue__c> relatedDQ  = [select id, CreatedDate from JVCO_Document_Queue__c where JVCO_Related_Account__c = :accountRecord.Id AND (JVCO_Scenario__c = 'Key Account Quote Usage Summary PDF' OR JVCO_Scenario__c = 'Key Account Quote Usage Summary XLS' ) order by CreatedDate desc limit 1];

        if(relatedDQ.isEmpty() || ((!relatedQuoteLine.isEmpty() && !relatedDQ.isEmpty()) && (relatedQuoteLine[0].LastModifiedDate > relatedDQ[0].CreatedDate || relatedQuoteLine[0].CreatedDate > relatedDQ[0].CreatedDate))){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Quote Usage Summary must be generated first before Contracting Review Quotes'));
            kaUsageSummarySent = false;
        }else{
            kaUsageSummarySent = true;
        }

        tcAccepted = termsAndConditionHelper.tcIsYes(accountRecord.JVCO_Customer_Account__r.TCs_Accepted__c);
        tcAccepted = termsAndConditionHelper.bipassTCWindow(oppsToProcess, tcAccepted);
        selectedTC = accountRecord.JVCO_Customer_Account__r.TCs_Accepted__c;
    }

    public List<Selectoption> getselectedTCfields(){
        return termsAndConditionHelper.getselectedTCfields(accountRecord.JVCO_Customer_Account__r.TCs_Accepted__c); 
    }
    
    public PageReference queueContractReviewQuotes()
    {
        if(selectedTC == null && tcAccepted == false){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.TermsAndConditionErrorText));
        }
        else{
            if(termsAndConditionHelper.updateCustomerTC(selectedTC, accountRecord.JVCO_Customer_Account__r.TCs_Accepted__c, accountRecord.JVCO_Customer_Account__c)){
                tcAccepted = true;
                
                if(JVCO_KABatchSetting__c.getInstance(userinfo.getUserId()).JVCO_ContractReviewQuotesQueueable__c) {
                    Id queueableId = System.enqueueJob(new JVCO_ContractReviewQuotes_Queueable(accountRecord));
                    //KA Queueable Update Processing Status
                    JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord, 'JVCO_ContractReviewQuotesQueueable__c', 'Queueable', 'JVCO_ContractReviewQuotesLastSubmitted__c');

                    JVCO_KATempHolder__c kaTempRec = new JVCO_KATempHolder__c();
                    kaTempRec.Name = 'JobFor:' + accountRecord.Id + ':ContractReview';
                    kaTempRec.JVCO_AccountId__c = accountRecord.Id;
                    kaTempRec.JVCO_JobId__c = queueableId;

                    upsert kaTempRec Name;
                }
                else {
                    Id batchId = database.executeBatch(new JVCO_ContractReviewQuotesBatch(accountRecord, numberOfRecords), contractBatchSize);
                    JVCO_KeyAccountQuote.updateAccountProcessingStatus(accountRecord,'JVCO_ContractReviewQuotesApexJob__c', batchId,'JVCO_ContractReviewQuotesLastSubmitted__c');
                }
                
                isContinued = true;
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.TermsAndConditionAccUpdateErrorText));
            }
        }

        return null;
    }

    public PageReference backToRecord()
    {
        PageReference pRef = new PageReference('/' + accountRecord.Id);
        return pRef;
    }

    //GREEN-33789
    public PageReference jobCheck(){
        
        Id batchId = apexJobId;
        Id accountId = fetchedAccountRecord.Id;

        PageReference tempRef;

        JVCO_ApexJobProgressBarController progBar = new JVCO_ApexJobProgressBarController();

        if(!queueableRunning){

            tempRef = progBar.updateAcc(batchId, accountId);
        }
        else{

            tempRef = progBar.updateAcc(accountId, 'ContractReview');
        }

        return tempRef;
    }
}