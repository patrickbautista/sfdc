@isTest
public class JVCO_ValidateCreditNoteLogicTest {
    
    @testSetup static void setTestData(){

        JVCO_TestClassHelper.createBillingConfig();
        
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        //SBQQ.TriggerControl.disable();
        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        dt.JVCO_QuoteLineTrigger__c = true;
        dt.JVCO_QuoteTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaCashMatchingHistoryTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        insert dt;

        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        glaList.add(nonConvGLA);
        insert glaList;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;

        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        c2g__codaDimension2__c dim2 = JVCO_TestClassHelper.setDim2('PPL');
        insert dim2;
        c2g__codaDimension3__c dim3 = JVCO_TestClassHelper.setDim3();

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;

        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        Test.startTest();
        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        Test.stopTest();
        
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_BackgroundMatchingLogic.stopMatching = true;
        JVCO_FFUtil.stopCodaTransasctionHandlerAfterUpdate = true;
        insert new GLA_for_Cash_Transfer__c(name = '10040 - Accounts receivables control');

        c2g__codaCreditNote__c cNote = new c2g__codaCreditNote__c();
        cNote.c2g__Account__c = licAcc.Id;
        cNote.c2g__DueDate__c = System.today() + 30;
        cNote.c2g__CreditNoteDate__c = System.today();
        cNote.c2g__DeriveCurrency__c = true;
        cNote.c2g__DerivePeriod__c = true;
        cNote.c2g__DeriveDueDate__c = true;
        insert cNote;
        
        c2g__codaCreditNoteLineItem__c cNoteLine = new c2g__codaCreditNoteLineItem__c();
        cNoteLine.c2g__CreditNote__c = cNote.Id;
        cNoteLine.c2g__Dimension1__c = dim1.Id;
        cNoteLine.c2g__Dimension3__c = dim3.Id;
        cNoteLine.c2g__TaxCode1__c = taxCode.Id;
        cNoteLine.c2g__DeriveLineNumber__c = true;
        cNoteLine.c2g__DeriveUnitPriceFromProduct__c = false;
        cNoteLine.c2g__Quantity__c = 1;
        cNoteLine.c2g__UnitPrice__c = 500;
        cNoteLine.c2g__Dimension2__c = dim2.Id;
        cNoteLine.c2g__Product__c =  p.Id;
        insert cNoteLine;
        
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Sales_Credit_Note__c = cNote.Id;
        insert o;
        
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;

        OrderItem oi1 = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi1;
    }

    
    @isTest
    static void ValidInv()
    {
        //Order testOrder = [select id, Accountid from Order limit 1];
        //OrderItem testOrderItem = [select id from OrderItem limit 1];
        SBQQ__Quote__c testQuote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c != null LIMIT 1];
        PriceBookEntry testPB = [SELECT Id FROM PriceBookEntry LIMIT 1];
        SBQQ__QuoteLine__c testQuoteLine = [SELECT Id FROM SBQQ__QuoteLine__c LIMIT 1];
        //Contract testContract = [select id from Contract limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];
        c2g__codaPeriod__c testPeriod  = [select id from c2g__codaPeriod__c limit 1];
        c2g__codaAccountingCurrency__c accCurrency  = [select id from c2g__codaAccountingCurrency__c limit 1];
        Product2 testProduct = [select id from Product2 limit 1];
        Opportunity testOpp = [select id from Opportunity limit 1];
        //SBQQ__QuoteLineGroup__c testQLG = [select id, JVCO_Affiliated_Venue__c, JVCO_Venue__c from SBQQ__QuoteLineGroup__c limit 1];
        Order testOrder = [select id, accountid from Order limit 1];

        Test.startTest();
        
        c2g__codaInvoice__c testSalesInvoice = new c2g__codaInvoice__c();
        testSalesInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testSalesInvoice.c2g__InvoiceDate__c = date.today();
        testSalesInvoice.c2g__DueDate__c = date.today().addMonths(7);
        testSalesInvoice.c2g__Account__c = testOrder.accountid;
        testSalesInvoice.JVCO_Customer_Type__c = 'New Business';
        testSalesInvoice.c2g__OwnerCompany__c = testCompany.id;
        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = false;
        testSalesInvoice.c2g__Period__c = testPeriod.id;
        insert testSalesInvoice;

        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = true;
        testSalesInvoice.JVCO_Number_of_Payments__c = 5.0 ;
        testSalesInvoice.JVCO_First_Due_Date__c = date.today().addDays(5);
        testSalesInvoice.JVCO_Invoice_Type__c = 'Standard'; 
        testSalesInvoice.JVCO_Payment_Method__c = 'Cheque';
        update testSalesInvoice;

        testQuote.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        update testQuote;

        c2g__codaInvoiceLineItem__c testInvoiceItem = new c2g__codaInvoiceLineItem__c();
        testInvoiceItem.c2g__Product__c = testProduct.id;
        testInvoiceItem.c2g__OwnerCompany__c = testCompany.id;
        testInvoiceItem.c2g__Invoice__c = testSalesInvoice.id;
        //testInvoiceItem.c2g__TaxCode1__c = testTaxCode.id;
        testInvoiceItem.c2g__Quantity__c = 1.0;
        testInvoiceItem.c2g__UnitPrice__c = 200.0;
        insert testInvoiceItem;
        
        Order o = JVCO_TestClassHelper.setOrder(testOrder.accountid, testQuote.Id);
        o.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        o.JVCO_Invoice_Type__c = 'Standard';
        o.JVCO_Original_Invoice_Cancelled__c = true;
        o.AccountId = testOrder.accountid;
        o.EffectiveDate = Date.today();
        insert o;

        List<c2g__codaInvoice__c> invList = new List<c2g__codaInvoice__c>();
        
        invlist = [SELECT Id FROM c2g__codaInvoice__c LIMIT 1];
        
        PageReference pageRef = Page.JVCO_ValidateCreditNote;
        test.setCurrentPage(pageRef);
        apexPages.Currentpage().getParameters().put('Id',testSalesInvoice.id);

        ApexPages.StandardController sc = new ApexPages.StandardController(testSalesInvoice);
        JVCO_ValidateCreditNoteLogic valcNote = new JVCO_ValidateCreditNoteLogic(sc);
        valcNote.init();
        valcNote.submitCreditReason();
        system.assertNotEquals(null, invList, 'Error');
        test.stopTest();

    }
    
    @isTest
    static void ValidSurchargeInv()
    {
        //Order testOrder = [select id, Accountid from Order limit 1];
        //OrderItem testOrderItem = [select id from OrderItem limit 1];
        SBQQ__Quote__c testQuote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c != null LIMIT 1];
        PriceBookEntry testPB = [SELECT Id FROM PriceBookEntry LIMIT 1];
        SBQQ__QuoteLine__c testQuoteLine = [SELECT Id FROM SBQQ__QuoteLine__c LIMIT 1];
        //Contract testContract = [select id from Contract limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];
        c2g__codaPeriod__c testPeriod  = [select id from c2g__codaPeriod__c limit 1];
        c2g__codaAccountingCurrency__c accCurrency  = [select id from c2g__codaAccountingCurrency__c limit 1];
        Product2 testProduct = [select id from Product2 limit 1];
        Opportunity testOpp = [select id from Opportunity limit 1];
        //SBQQ__QuoteLineGroup__c testQLG = [select id, JVCO_Affiliated_Venue__c, JVCO_Venue__c from SBQQ__QuoteLineGroup__c limit 1];
        Order testOrder = [select id, accountid from Order limit 1];
        //Test.startTest();

        c2g__codaInvoice__c originalSalesInvoice = new c2g__codaInvoice__c();
        originalSalesInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        originalSalesInvoice.c2g__InvoiceDate__c = date.today();
        originalSalesInvoice.c2g__DueDate__c = date.today().addMonths(7);
        originalSalesInvoice.c2g__Account__c = testOrder.accountid;
        originalSalesInvoice.JVCO_Customer_Type__c = 'New Business';
        originalSalesInvoice.c2g__OwnerCompany__c = testCompany.id;
        originalSalesInvoice.JVCO_Generate_Payment_Schedule__c = false;
        originalSalesInvoice.c2g__Period__c = testPeriod.id;
        originalSalesInvoice.JVCO_Surcharge_Generated__c = TRUE;
        insert originalSalesInvoice;

        c2g__codaInvoice__c testSalesInvoice = new c2g__codaInvoice__c();
        testSalesInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testSalesInvoice.c2g__InvoiceDate__c = date.today();
        testSalesInvoice.c2g__DueDate__c = date.today().addMonths(7);
        testSalesInvoice.c2g__Account__c = testOrder.accountid;
        testSalesInvoice.JVCO_Customer_Type__c = 'New Business';
        testSalesInvoice.c2g__OwnerCompany__c = testCompany.id;
        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = false;
        testSalesInvoice.c2g__Period__c = testPeriod.id;
        testSalesInvoice.JVCO_Original_Invoice__c = originalSalesInvoice.Id;
        insert testSalesInvoice;

        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = true;
        testSalesInvoice.JVCO_Number_of_Payments__c = 5.0 ;
        testSalesInvoice.JVCO_First_Due_Date__c = date.today().addDays(5);
        testSalesInvoice.JVCO_Invoice_Type__c = 'Surcharge'; 
        testSalesInvoice.JVCO_Payment_Method__c = 'Cheque';
        update testSalesInvoice;

        testQuote.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        update testQuote;

        c2g__codaInvoiceLineItem__c testInvoiceItem = new c2g__codaInvoiceLineItem__c();
        testInvoiceItem.c2g__Product__c = testProduct.id;
        testInvoiceItem.c2g__OwnerCompany__c = testCompany.id;
        testInvoiceItem.c2g__Invoice__c = testSalesInvoice.id;
        //testInvoiceItem.c2g__TaxCode1__c = testTaxCode.id;
        testInvoiceItem.c2g__Quantity__c = 1.0;
        testInvoiceItem.c2g__UnitPrice__c = 200.0;
        insert testInvoiceItem;

        Order o = JVCO_TestClassHelper.setOrder(testOrder.accountid, testQuote.Id);
        o.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        o.JVCO_Invoice_Type__c = 'Surcharge';
        o.JVCO_Original_Invoice_Cancelled__c = true;
        o.AccountId = testOrder.accountid;
        o.EffectiveDate = Date.today();
        insert o;
        
        List<c2g__codaInvoice__c> invList = new List<c2g__codaInvoice__c>();
        
        invlist = [SELECT Id FROM c2g__codaInvoice__c LIMIT 1];
        
        Test.startTest();
        PageReference pageRef = Page.JVCO_ValidateCreditNote;
        test.setCurrentPage(pageRef);
        apexPages.Currentpage().getParameters().put('Id',testSalesInvoice.id);

        ApexPages.StandardController sc = new ApexPages.StandardController(testSalesInvoice);
        JVCO_ValidateCreditNoteLogic valcNote = new JVCO_ValidateCreditNoteLogic(sc);
        valcNote.init();
        system.assertNotEquals(null, invList, 'Error');
        test.stopTest();

    }
    @isTest
    static void ValidMigratedSurchargeInv()
    {
        //Order testOrder = [select id, Accountid from Order limit 1];
        //OrderItem testOrderItem = [select id from OrderItem limit 1];
        SBQQ__Quote__c testQuote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c != null LIMIT 1];
        PriceBookEntry testPB = [SELECT Id FROM PriceBookEntry LIMIT 1];
        SBQQ__QuoteLine__c testQuoteLine = [SELECT Id FROM SBQQ__QuoteLine__c LIMIT 1];
        //Contract testContract = [select id from Contract limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];
        c2g__codaPeriod__c testPeriod  = [select id from c2g__codaPeriod__c limit 1];
        c2g__codaAccountingCurrency__c accCurrency  = [select id from c2g__codaAccountingCurrency__c limit 1];
        Product2 testProduct = [select id from Product2 limit 1];
        Opportunity testOpp = [select id from Opportunity limit 1];
        //SBQQ__QuoteLineGroup__c testQLG = [select id, JVCO_Affiliated_Venue__c, JVCO_Venue__c from SBQQ__QuoteLineGroup__c limit 1];
        Order testOrder = [select id, accountid from Order limit 1];
        //Test.startTest();
        JVCO_FFUtil.stopCodaInvoiceHandlerAfterUpdate = false;

        c2g__codaInvoice__c originalSalesInvoice = new c2g__codaInvoice__c();
        originalSalesInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        originalSalesInvoice.c2g__InvoiceDate__c = date.today();
        originalSalesInvoice.c2g__DueDate__c = date.today().addMonths(7);
        originalSalesInvoice.c2g__Account__c = testOrder.accountid;
        originalSalesInvoice.JVCO_Customer_Type__c = 'New Business';
        originalSalesInvoice.c2g__OwnerCompany__c = testCompany.id;
        originalSalesInvoice.JVCO_Generate_Payment_Schedule__c = false;
        originalSalesInvoice.c2g__Period__c = testPeriod.id;
        originalSalesInvoice.JVCO_Surcharge_Generated__c = TRUE;
        insert originalSalesInvoice;

        c2g__codaInvoice__c testSalesInvoice = new c2g__codaInvoice__c();
        testSalesInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testSalesInvoice.c2g__InvoiceDate__c = date.today();
        testSalesInvoice.c2g__DueDate__c = date.today().addMonths(7);
        testSalesInvoice.c2g__Account__c = testOrder.accountid;
        testSalesInvoice.JVCO_Customer_Type__c = 'New Business';
        testSalesInvoice.c2g__OwnerCompany__c = testCompany.id;
        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = false;
        testSalesInvoice.c2g__Period__c = testPeriod.id;
        insert testSalesInvoice;

        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = true;
        testSalesInvoice.JVCO_Number_of_Payments__c = 5.0 ;
        testSalesInvoice.JVCO_First_Due_Date__c = date.today().addDays(5);
        testSalesInvoice.JVCO_Original_Invoice__c = originalSalesInvoice.Id;
        testSalesInvoice.JVCO_Invoice_Legacy_Number__c = 'PRS:421892180';
        testSalesInvoice.JVCO_Invoice_Type__c = 'Surcharge';
        testSalesInvoice.JVCO_Payment_Method__c = 'Cheque';
        update testSalesInvoice;

        System.assertNotEquals(null, testSalesInvoice.JVCO_Original_Invoice__c, '@@@@Error In JVCO_Original_Invoice__c: ');
        System.assertEquals('PRS:421892180', testSalesInvoice.JVCO_Invoice_Legacy_Number__c, '@@@@Error In JVCO_Invoice_Legacy_Number__c: ');
        System.assertEquals('Surcharge', testSalesInvoice.JVCO_Invoice_Type__c, '@@@@Error In JVCO_Invoice_Type__c: ');

        testQuote.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        update testQuote;

        c2g__codaInvoiceLineItem__c testInvoiceItem = new c2g__codaInvoiceLineItem__c();
        testInvoiceItem.c2g__Product__c = testProduct.id;
        testInvoiceItem.c2g__OwnerCompany__c = testCompany.id;
        testInvoiceItem.c2g__Invoice__c = testSalesInvoice.id;
        //testInvoiceItem.c2g__TaxCode1__c = testTaxCode.id;
        testInvoiceItem.c2g__Quantity__c = 1.0;
        testInvoiceItem.c2g__UnitPrice__c = 200.0;
        insert testInvoiceItem;

        Order o = JVCO_TestClassHelper.setOrder(testOrder.accountid, testQuote.Id);
        o.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        o.JVCO_Invoice_Type__c = 'Surcharge';
        o.JVCO_Original_Invoice_Cancelled__c = true;
        o.AccountId = testOrder.accountid;
        o.EffectiveDate = Date.today();
        insert o;

        List<c2g__codaInvoice__c> invList = new List<c2g__codaInvoice__c>();
        
        invlist = [SELECT Id FROM c2g__codaInvoice__c LIMIT 1];
        
        Test.startTest();
        PageReference pageRef = Page.JVCO_ValidateCreditNote;
        test.setCurrentPage(pageRef);
        apexPages.Currentpage().getParameters().put('Id',testSalesInvoice.id);

        ApexPages.StandardController sc = new ApexPages.StandardController(testSalesInvoice);
        JVCO_ValidateCreditNoteLogic valcNote = new JVCO_ValidateCreditNoteLogic(sc);
        valcNote.init();
        system.assertNotEquals(null, invList, 'Error');
        test.stopTest();

    }

    @isTest
    static void ValidPreContractInv()
    {
        //Order testOrder = [select id, Accountid from Order limit 1];
        //OrderItem testOrderItem = [select id from OrderItem limit 1];
        SBQQ__Quote__c testQuote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c != null LIMIT 1];
        PriceBookEntry testPB = [SELECT Id FROM PriceBookEntry LIMIT 1];
        SBQQ__QuoteLine__c testQuoteLine = [SELECT Id FROM SBQQ__QuoteLine__c LIMIT 1];
        //Contract testContract = [select id from Contract limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];
        c2g__codaPeriod__c testPeriod  = [select id from c2g__codaPeriod__c limit 1];
        c2g__codaAccountingCurrency__c accCurrency  = [select id from c2g__codaAccountingCurrency__c limit 1];
        Product2 testProduct = [select id from Product2 limit 1];
        Opportunity testOpp = [select id from Opportunity limit 1];
        //SBQQ__QuoteLineGroup__c testQLG = [select id, JVCO_Affiliated_Venue__c, JVCO_Venue__c from SBQQ__QuoteLineGroup__c limit 1];
        Order testOrder = [select id, accountid from Order limit 1];
        //Test.startTest();
        
        c2g__codaInvoice__c testSalesInvoice = new c2g__codaInvoice__c();
        testSalesInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testSalesInvoice.c2g__InvoiceDate__c = date.today();
        testSalesInvoice.c2g__DueDate__c = date.today().addMonths(7);
        testSalesInvoice.c2g__Account__c = testOrder.accountid;
        testSalesInvoice.JVCO_Customer_Type__c = 'New Business';
        testSalesInvoice.c2g__OwnerCompany__c = testCompany.id;
        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = false;
        testSalesInvoice.c2g__Period__c = testPeriod.id;
        insert testSalesInvoice;

        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = true;
        testSalesInvoice.JVCO_Number_of_Payments__c = 5.0 ;
        testSalesInvoice.JVCO_First_Due_Date__c = date.today().addDays(5);
        testSalesInvoice.JVCO_Invoice_Type__c = 'Pre-Contract'; 
        testSalesInvoice.JVCO_Payment_Method__c = 'Cheque';
        update testSalesInvoice;

        testQuote.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        update testQuote;

        c2g__codaInvoiceLineItem__c testInvoiceItem = new c2g__codaInvoiceLineItem__c();
        testInvoiceItem.c2g__Product__c = testProduct.id;
        testInvoiceItem.c2g__OwnerCompany__c = testCompany.id;
        testInvoiceItem.c2g__Invoice__c = testSalesInvoice.id;
        //testInvoiceItem.c2g__TaxCode1__c = testTaxCode.id;
        testInvoiceItem.c2g__Quantity__c = 1.0;
        testInvoiceItem.c2g__UnitPrice__c = 200.0;
        insert testInvoiceItem;

        Order o = JVCO_TestClassHelper.setOrder(testOrder.accountid, testQuote.Id);
        o.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        o.JVCO_Invoice_Type__c = 'Pre-Contract';
        o.JVCO_Original_Invoice_Cancelled__c = true;
        o.AccountId = testOrder.accountid;
        o.EffectiveDate = Date.today();
        insert o;

        List<c2g__codaInvoice__c> invList = new List<c2g__codaInvoice__c>();
        
        invlist = [SELECT Id FROM c2g__codaInvoice__c LIMIT 1];
        
        Test.startTest();
        PageReference pageRef = Page.JVCO_ValidateCreditNote;
        test.setCurrentPage(pageRef);
        apexPages.Currentpage().getParameters().put('Id',testSalesInvoice.id);

        ApexPages.StandardController sc = new ApexPages.StandardController(testSalesInvoice);
        JVCO_ValidateCreditNoteLogic valcNote = new JVCO_ValidateCreditNoteLogic(sc);
        valcNote.init();
        system.assertNotEquals(null, invList, 'Error');
        test.stopTest();

    }
    
    @isTest
    static void MigratedInv()
    {
        //Order testOrder = [select id, Accountid from Order limit 1];
        //OrderItem testOrderItem = [select id from OrderItem limit 1];
        SBQQ__Quote__c testQuote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c != null LIMIT 1];
        PriceBookEntry testPB = [SELECT Id FROM PriceBookEntry LIMIT 1];
        SBQQ__QuoteLine__c testQuoteLine = [SELECT Id FROM SBQQ__QuoteLine__c LIMIT 1];
        Contract testContract = [select id from Contract limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];
        c2g__codaPeriod__c testPeriod  = [select id from c2g__codaPeriod__c limit 1];
        c2g__codaAccountingCurrency__c accCurrency  = [select id from c2g__codaAccountingCurrency__c limit 1];
        Product2 testProduct = [select id from Product2 limit 1];
        Opportunity testOpp = [select id from Opportunity limit 1];
        //SBQQ__QuoteLineGroup__c testQLG = [select id, JVCO_Affiliated_Venue__c, JVCO_Venue__c from SBQQ__QuoteLineGroup__c limit 1];
        Order testOrder = [select id, accountid from Order limit 1];
        //Test.startTest();
        
        c2g__codaInvoice__c testSalesInvoice = new c2g__codaInvoice__c();
        testSalesInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testSalesInvoice.c2g__InvoiceDate__c = date.today();
        testSalesInvoice.c2g__DueDate__c = date.today().addMonths(7);
        testSalesInvoice.c2g__Account__c = testOrder.accountid;
        testSalesInvoice.JVCO_Customer_Type__c = 'New Business';
        testSalesInvoice.c2g__OwnerCompany__c = testCompany.id;
        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = false;
        testSalesInvoice.c2g__Period__c = testPeriod.id;
        insert testSalesInvoice;

        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = true;
        testSalesInvoice.JVCO_Number_of_Payments__c = 5.0 ;
        testSalesInvoice.JVCO_First_Due_Date__c = date.today().addDays(5);
        testSalesInvoice.JVCO_Invoice_Type__c = 'Standard';
        testSalesInvoice.JVCO_Invoice_Legacy_Number__c = '12345'; 
        testSalesInvoice.JVCO_Payment_Method__c = 'Cheque';
        update testSalesInvoice;

        testQuote.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        update testQuote;

        c2g__codaInvoiceLineItem__c testInvoiceItem = new c2g__codaInvoiceLineItem__c();
        testInvoiceItem.c2g__Product__c = testProduct.id;
        testInvoiceItem.c2g__OwnerCompany__c = testCompany.id;
        testInvoiceItem.c2g__Invoice__c = testSalesInvoice.id;
        //testInvoiceItem.c2g__TaxCode1__c = testTaxCode.id;
        testInvoiceItem.c2g__Quantity__c = 1.0;
        testInvoiceItem.c2g__UnitPrice__c = 200.0;
        insert testInvoiceItem;

        Order o = JVCO_TestClassHelper.setOrder(testOrder.accountid, testQuote.Id);
        o.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        o.JVCO_Invoice_Type__c = 'Standard';
        o.JVCO_Original_Invoice_Cancelled__c = true;
        o.AccountId = testOrder.accountid;
        o.JVCO_Invoice_Legacy_Number__c = '12345';
        o.EffectiveDate = Date.today();
        insert o;
        
        List<c2g__codaInvoice__c> invList = new List<c2g__codaInvoice__c>();
        
        invlist = [SELECT Id FROM c2g__codaInvoice__c LIMIT 1];
        
        Test.startTest();
        PageReference pageRef = Page.JVCO_ValidateCreditNote;
        test.setCurrentPage(pageRef);
        apexPages.Currentpage().getParameters().put('Id',testSalesInvoice.id);

        ApexPages.StandardController sc = new ApexPages.StandardController(testSalesInvoice);
        JVCO_ValidateCreditNoteLogic valcNote = new JVCO_ValidateCreditNoteLogic(sc);
        valcNote.init();
        valcNote.returnToInvoice();
        system.assertNotEquals(null, invList, 'Error');
        test.stopTest();

    }
    
    @isTest
    static void RenewedInv()
    {
        //Order testOrder = [select id, Accountid from Order limit 1];
        //OrderItem testOrderItem = [select id from OrderItem limit 1];
        SBQQ__Quote__c testQuote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c != null LIMIT 1];
        PriceBookEntry testPB = [SELECT Id FROM PriceBookEntry LIMIT 1];
        SBQQ__QuoteLine__c testQuoteLine = [SELECT Id FROM SBQQ__QuoteLine__c LIMIT 1];
        Contract testContract = [select id from Contract limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];
        c2g__codaPeriod__c testPeriod  = [select id from c2g__codaPeriod__c limit 1];
        c2g__codaAccountingCurrency__c accCurrency  = [select id from c2g__codaAccountingCurrency__c limit 1];
        Product2 testProduct = [select id from Product2 limit 1];
        Opportunity testOpp = [select id from Opportunity limit 1];
        //SBQQ__QuoteLineGroup__c testQLG = [select id, JVCO_Affiliated_Venue__c, JVCO_Venue__c from SBQQ__QuoteLineGroup__c limit 1];
        Order testOrder = [select id, accountid from Order limit 1];
        //Test.startTest();
        
        c2g__codaInvoice__c testSalesInvoice = new c2g__codaInvoice__c();
        testSalesInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testSalesInvoice.c2g__InvoiceDate__c = date.today();
        testSalesInvoice.c2g__DueDate__c = date.today().addMonths(7);
        testSalesInvoice.c2g__Account__c = testOrder.accountid;
        testSalesInvoice.JVCO_Customer_Type__c = 'New Business';
        testSalesInvoice.c2g__OwnerCompany__c = testCompany.id;
        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = false;
        testSalesInvoice.c2g__Period__c = testPeriod.id;
        insert testSalesInvoice;

        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = true;
        testSalesInvoice.JVCO_Number_of_Payments__c = 5.0 ;
        testSalesInvoice.JVCO_First_Due_Date__c = date.today().addDays(5);
        testSalesInvoice.JVCO_Invoice_Type__c = 'Standard'; 
        testSalesInvoice.JVCO_Payment_Method__c = 'Cheque';
        update testSalesInvoice;
        
        
        Opportunity renOpp = new Opportunity();
        renOpp.Name = 'Renewal opportunity 83202';
        renOpp.AccountId = testAccount.Id;
        renOpp.CloseDate = System.today() - 5;
        renOpp.StageName = 'Draft';
        renOpp.Type = 'Renewed';
        insert renOpp;
        
        testOpp.SBQQ__RenewedContract__c = testContract.Id;
        testOpp.SBQQ__Renewal__c = TRUE;
        update testOpp;
        
        testContract.SBQQ__RenewalOpportunity__c = renOpp.Id;
        update testContract;

        testQuote.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        update testQuote;

        c2g__codaInvoiceLineItem__c testInvoiceItem = new c2g__codaInvoiceLineItem__c();
        testInvoiceItem.c2g__Product__c = testProduct.id;
        testInvoiceItem.c2g__OwnerCompany__c = testCompany.id;
        testInvoiceItem.c2g__Invoice__c = testSalesInvoice.id;
        //testInvoiceItem.c2g__TaxCode1__c = testTaxCode.id;
        testInvoiceItem.c2g__Quantity__c = 1.0;
        testInvoiceItem.c2g__UnitPrice__c = 200.0;
        insert testInvoiceItem;

        Order o = JVCO_TestClassHelper.setOrder(testOrder.accountid, testQuote.Id);
        o.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        o.JVCO_Invoice_Type__c = 'Standard';
        o.JVCO_Original_Invoice_Cancelled__c = true;
        o.AccountId = testOrder.accountid;
        o.EffectiveDate = Date.today();
        insert o;

        List<c2g__codaInvoice__c> invList = new List<c2g__codaInvoice__c>();
        
        invlist = [SELECT Id FROM c2g__codaInvoice__c LIMIT 1];
        
        Test.startTest();
        PageReference pageRef = Page.JVCO_ValidateCreditNote;
        test.setCurrentPage(pageRef);
        apexPages.Currentpage().getParameters().put('Id',testSalesInvoice.id);

        ApexPages.StandardController sc = new ApexPages.StandardController(testSalesInvoice);
        JVCO_ValidateCreditNoteLogic valcNote = new JVCO_ValidateCreditNoteLogic(sc);
        valcNote.init();
        valcNote.returnToInvoice();
        system.assertNotEquals(null, invList, 'Error');
        test.stopTest();

    }

    @isTest
    static void checkIfAlreadyConverted()
    {
        //Order testOrder = [select id, Accountid from Order limit 1];
        //OrderItem testOrderItem = [select id from OrderItem limit 1];
        SBQQ__Quote__c testQuote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE JVCO_Customer_Account__c != null LIMIT 1];
        PriceBookEntry testPB = [SELECT Id FROM PriceBookEntry LIMIT 1];
        SBQQ__QuoteLine__c testQuoteLine = [SELECT Id FROM SBQQ__QuoteLine__c LIMIT 1];
        Contract testContract = [select id from Contract limit 1];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];
        c2g__codaPeriod__c testPeriod  = [select id from c2g__codaPeriod__c limit 1];
        c2g__codaAccountingCurrency__c accCurrency  = [select id from c2g__codaAccountingCurrency__c limit 1];
        Product2 testProduct = [select id from Product2 limit 1];
        Opportunity testOpp = [select id from Opportunity limit 1];
        //SBQQ__QuoteLineGroup__c testQLG = [select id, JVCO_Affiliated_Venue__c, JVCO_Venue__c from SBQQ__QuoteLineGroup__c limit 1];
        Order testOrder = [select id, accountid from Order limit 1];
        //Test.startTest();
        
        c2g__codaInvoice__c testSalesInvoice = new c2g__codaInvoice__c();
        testSalesInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testSalesInvoice.c2g__InvoiceDate__c = date.today();
        testSalesInvoice.c2g__DueDate__c = date.today().addMonths(7);
        testSalesInvoice.c2g__Account__c = testOrder.accountid;
        testSalesInvoice.JVCO_Customer_Type__c = 'New Business';
        testSalesInvoice.c2g__OwnerCompany__c = testCompany.id;
        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = false;
        testSalesInvoice.c2g__Period__c = testPeriod.id;
        insert testSalesInvoice;

        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = true;
        testSalesInvoice.JVCO_Number_of_Payments__c = 5.0 ;
        testSalesInvoice.JVCO_First_Due_Date__c = date.today().addDays(5);
        testSalesInvoice.JVCO_Invoice_Type__c = 'Standard'; 
        testSalesInvoice.JVCO_Payment_Method__c = 'Cheque';
        update testSalesInvoice;
        
        
        Opportunity renOpp = new Opportunity();
        renOpp.Name = 'Renewal opportunity 83202';
        renOpp.AccountId = testAccount.Id;
        renOpp.CloseDate = System.today() - 5;
        renOpp.StageName = 'Draft';
        renOpp.Type = 'Renewed';
        insert renOpp;
        
        testOpp.SBQQ__RenewedContract__c = testContract.Id;
        testOpp.SBQQ__Renewal__c = TRUE;
        update testOpp;
        
        testContract.SBQQ__RenewalOpportunity__c = renOpp.Id;
        update testContract;

        testQuote.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        update testQuote;

        c2g__codaInvoiceLineItem__c testInvoiceItem = new c2g__codaInvoiceLineItem__c();
        testInvoiceItem.c2g__Product__c = testProduct.id;
        testInvoiceItem.c2g__OwnerCompany__c = testCompany.id;
        testInvoiceItem.c2g__Invoice__c = testSalesInvoice.id;
        //testInvoiceItem.c2g__TaxCode1__c = testTaxCode.id;
        testInvoiceItem.c2g__Quantity__c = 1.0;
        testInvoiceItem.c2g__UnitPrice__c = 200.0;
        insert testInvoiceItem;

        Order o = JVCO_TestClassHelper.setOrder(testOrder.accountid, testQuote.Id);
        o.JVCO_Sales_Invoice__c = testSalesInvoice.Id;
        o.JVCO_Invoice_Type__c = 'Standard';
        o.JVCO_Original_Invoice_Cancelled__c = true;
        o.AccountId = testOrder.accountid;
        o.EffectiveDate = Date.today();
        insert o;
        
        List<c2g__codaInvoice__c> invList = new List<c2g__codaInvoice__c>();
        
        invlist = [SELECT Id FROM c2g__codaInvoice__c LIMIT 1];

        c2g__codaCreditNote__c cNote = [SELECT Id FROM c2g__codaCreditNote__c LIMIT 1];
        cNote.c2g__Invoice__c = testSalesInvoice.Id;
        update cNote;

        Test.startTest();
        PageReference pageRef = Page.JVCO_ValidateCreditNote;
        test.setCurrentPage(pageRef);
        apexPages.Currentpage().getParameters().put('Id',testSalesInvoice.id);

        ApexPages.StandardController sc = new ApexPages.StandardController(testSalesInvoice);
        JVCO_ValidateCreditNoteLogic valcNote = new JVCO_ValidateCreditNoteLogic(sc);
        valcNote.init();
        valcNote.returnToInvoice();
        system.assertNotEquals(null, invList, 'Error');
        test.stopTest();

    }
    @isTest
    static void checkIfAlreadyHaveIDD()
    {
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c limit 1];
        c2g__codaPeriod__c testPeriod  = [select id from c2g__codaPeriod__c limit 1];
        c2g__codaAccountingCurrency__c accCurrency  = [select id from c2g__codaAccountingCurrency__c limit 1];
        Order testOrder = [select id, accountid from Order limit 1];
        
        c2g__codaInvoice__c testSalesInvoice = new c2g__codaInvoice__c();
        testSalesInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
        testSalesInvoice.c2g__InvoiceDate__c = date.today();
        testSalesInvoice.c2g__DueDate__c = date.today().addMonths(7);
        testSalesInvoice.c2g__Account__c = testOrder.accountid;
        testSalesInvoice.JVCO_Customer_Type__c = 'New Business';
        testSalesInvoice.c2g__OwnerCompany__c = testCompany.id;
        testSalesInvoice.JVCO_Generate_Payment_Schedule__c = false;
        testSalesInvoice.c2g__Period__c = testPeriod.id;
        insert testSalesInvoice;
        
        Income_Direct_Debit__c incomeDD = new Income_Direct_Debit__c();
        incomeDD.Account__c = testOrder.accountid;
        incomeDD.DD_Collection_Day__c = '1';
        incomeDD.DD_Collection_Period__c = 'Monthly';
        incomeDD.DD_Collection_Stretch__c = '1';
        incomeDD.DD_Collection_Type__c = 'Fixed';
        incomeDD.DD_Collection_Reference__c = 'Monthly Subscription';
        incomeDD.DD_Status__c = 'New Instruction';
        incomeDD.DD_Bank_Account_Number__c = '12345678';
        incomeDD.DD_Bank_Sort_Code__c = '123456';
        insert incomeDD;
        
        JVCO_Invoice_Group__c invoiceGroup = new JVCO_Invoice_Group__c();
        invoiceGroup.JVCO_Total_Amount__c = 100;
        insert invoiceGroup;
            
        SMP_DirectDebit_GroupInvoice__c IDDInvGroup = new SMP_DirectDebit_GroupInvoice__c();
        IDDInvGroup.Income_Direct_Debit__c = incomeDD.Id;
        IDDInvGroup.Invoice_Group__c = invoiceGroup.Id;
        insert IDDInvGroup;
        
        JVCO_CC_Outstanding_Mapping__c CCInvoiceGrp = new JVCO_CC_Outstanding_Mapping__c();
        CCInvoiceGrp.JVCO_Sales_Invoice_Name__c = testSalesInvoice.Name;
        CCInvoiceGrp.JVCO_CC_Invoice_Group__c = invoiceGroup.Id;
        insert CCInvoiceGrp;
        
        List<c2g__codaInvoice__c> invList = [SELECT Id FROM c2g__codaInvoice__c LIMIT 1];
        
        Test.startTest();
        PageReference pageRef = Page.JVCO_ValidateCreditNote;
        test.setCurrentPage(pageRef);
        apexPages.Currentpage().getParameters().put('Id',testSalesInvoice.id);

        ApexPages.StandardController sc = new ApexPages.StandardController(testSalesInvoice);
        JVCO_ValidateCreditNoteLogic valcNote = new JVCO_ValidateCreditNoteLogic(sc);
        valcNote.init();
        system.assertNotEquals(null, invList, 'Error');
        test.stopTest();
        
    }
}