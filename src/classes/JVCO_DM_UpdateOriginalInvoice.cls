    /* ----------------------------------------------------------------------------------------------
    Name: JVCO_DM_UpdateOriginalInvoice.cls 
    Description: Batch class that updates the original invoice field of the sales invoice.
    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    09-Oct-2017  0.1         desiree.m.quijada     Initial creation
    ----------------------------------------------------------------------------------------------- */

    public class JVCO_DM_UpdateOriginalInvoice implements Database.Batchable<sObject> {
        
        public static List<blng__Invoice__c> invoicesList;

        public static void init()
        {   
            Map<Id, Id> blgOriginalInvoiceMap = new Map<Id, Id>();
            Set<Id> salesinvoiceSet = new Set<Id>();
            if(!invoicesList.isEmpty()){
                for(blng__Invoice__c bi : invoicesList)
                {
                    blgOriginalInvoiceMap.put(bi.JVCO_Sales_Invoice__c, bi.JVCO_Original_Invoice__r.JVCO_Sales_Invoice__c);
                    salesinvoiceSet.add(bi.JVCO_Sales_Invoice__c);
                }   
            }


            System.debug('Migrated invoices' +blgOriginalInvoiceMap);
            List<c2g__codaInvoice__c> salesInvoiceList = new List<c2g__codaInvoice__c>();
            List<c2g__codaInvoice__c> salesInvoiceUpdateList = new List<c2g__codaInvoice__c>();

            salesInvoiceList = [SELECT ID, JVCO_Original_Invoice__c
                                FROM c2g__codaInvoice__c 
                                WHERE ID IN :salesinvoiceSet];
            


            for(c2g__codaInvoice__c si: salesInvoiceList)
            {
                if(blgOriginalInvoiceMap.containsKey(si.ID))
                {
                    si.JVCO_Original_Invoice__c = blgOriginalInvoiceMap.get(si.ID);
                    salesInvoiceUpdateList.add(si);
                }
            }
         

            if(!salesInvoiceUpdateList.isEmpty()){
                    update salesInvoiceUpdateList; 
                      
            }
        }
        
        public Database.QueryLocator start(Database.BatchableContext BC) {
            return Database.getQueryLocator([SELECT ID, JVCO_Sales_Invoice__c, JVCO_Original_Invoice__r.JVCO_Sales_Invoice__c 
                                            FROM blng__Invoice__c 
                                            WHERE JVCO_Sales_Invoice__c != NULL 
                                            AND JVCO_Original_Invoice__r.JVCO_Sales_Invoice__c != null]);
        }

        public void execute(Database.BatchableContext BC, List<blng__Invoice__c> scope) {

            invoicesList = scope;
            init();
        }
        
        public void finish(Database.BatchableContext BC) {
            System.debug('Done');
        }
        
    }