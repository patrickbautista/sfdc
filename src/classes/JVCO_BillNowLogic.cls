/* ----------------------------------------------------------------------------------------------
    Name: JVCO_BillNowLogic.cls 
    Description: Business logic class for generating Billing Invoice on selected Order records

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    06-Mar-2017   0.1         ryan.i.r.limlingan          Intial creation
    08-Jan-2018   0.2         filip.bezak@accenture.com   GREEN-27940 - Restrict billing orders that are being processed
    22-Jan-2018   0.3         filip.bezak@accenture.com   GREEN-28394 - TC2P - True up report not returning the expected values
    02-May-2018   0.4         franz.g.a.dimaapi           GREEN-3**** - Merge the logic for Single and Multiple BillNow
    03-May-2018   0.5         filip.bezak@accenture.com   GREEN-31676 - TCP - Sales credit notes containing PPL product have PRS populated in the Parent field
    12-Oct-2018   0.6         franz.g.a.dimaapi           GREEN-
----------------------------------------------------------------------------------------------- */
public class JVCO_BillNowLogic
{
    private ApexPages.StandardSetController stdCtrl;
    private List<Order> selectedOrders;
    public Integer numOfSelectedOrders {get; set;}
    public List<Order> retrievedOrders {get; set;}
    
    // Constants used for page messages
    public final String NO_RECORDS_ERR = System.Label.JVCO_OrderNoRecords;
    public String INVALID_RECORDS_ERR;

    //Batch Job Helper
    public Boolean canContinue {get; set;}

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Class constructor
        Inputs: StandardSetController
        Returns: N/A
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Mar-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public JVCO_BillNowLogic(ApexPages.StandardSetController ssc)
    {
        stdCtrl = ssc;
        // Retrieve selected Order records from related list
        selectedOrders = (List<Order>)ssc.getSelected();
        numOfSelectedOrders = selectedOrders.size();

        canContinue = true;
        if(!JVCO_BatchJobHelper.checkLargeJobs(numOfSelectedOrders) && numOfSelectedOrders > 0)
        {
            canContinue = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.JVCO_BatchJobHelperCannotProcess)); 
        }
    }
        
    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: <TBD>
        Inputs: N/A
        Returns: void
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Mar-2017 ryan.i.r.limlingan  Initial version of function
        10-Apr-2017 franz.g.a.dimaapi   Added Functionality for Migrated Multiple Negative Order - GREEN-30591
    ----------------------------------------------------------------------------------------------- */
    public PageReference init()
    {
        if(numOfSelectedOrders > 0)
        {
            retrievedOrders = [SELECT Id, Account.JVCO_In_Enforcement__c, Account.JVCO_In_Infringement__c,
                               Account.JVCO_Active__c, AccountId, TotalAmount, JVCO_Credit_Reason__c, Account.JVCO_DD_Mandate_Checker__c,
                               JVCO_Order_Group__c, blng__BillNow__c, JVCO_Invoiced__c, Status, JVCO_TaggedForBilling__c,
                               SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c, JVCO_Quote_Gross_Total__c,
                               SBQQ__Quote__r.JVCO_Payment_Terms__c, Account.JVCO_DD_Payee_Contact__c, SBQQ__Quote__r.JVCO_Quote_Id_to_Scheduler__c,
                               JVCO_Processed_By_BillNow__c
                               FROM Order WHERE Id IN :selectedOrders];
            
            Integer size = retrievedOrders.size();
            if(isListValid(retrievedOrders))
            {
                try
                {
                    // Create an Order Group with the AccountId from a single Order record
                    Map<String, Map<String, JVCO_Order_Group__c>> contractTypeToPaymentTermOrderGroupMap = new Map<String, Map<String, JVCO_Order_Group__c>>();
                    List<JVCO_Order_Group__c> orderGroupSet = new List<JVCO_Order_Group__c>();
                    for(Order o : retrievedOrders)
                    {
                        JVCO_Order_Group__c orderGroup = new JVCO_Order_Group__c(JVCO_Group_Order_Account__c=retrievedOrders.get(0).AccountId);
                        if(o.SBQQ__Quote__r.JVCO_Quote_Id_to_Scheduler__c != null)
                        {
                            if(!contractTypeToPaymentTermOrderGroupMap.containsKey('nonconsolidated'))
                            {
                                contractTypeToPaymentTermOrderGroupMap.put('nonconsolidated', new Map<String, JVCO_Order_Group__c>());
                            }
                            if(!contractTypeToPaymentTermOrderGroupMap.get('nonconsolidated').containsKey(o.SBQQ__Quote__r.JVCO_Payment_Terms__c))
                            {
                                contractTypeToPaymentTermOrderGroupMap.get('nonconsolidated').put(o.SBQQ__Quote__r.JVCO_Payment_Terms__c, orderGroup);
                                orderGroupSet.add(orderGroup);
                            }
                        }else 
                        {
                            if(!contractTypeToPaymentTermOrderGroupMap.containsKey('consolidated'))
                            {
                                contractTypeToPaymentTermOrderGroupMap.put('consolidated', new Map<String, JVCO_Order_Group__c>());
                            }
                            if(!contractTypeToPaymentTermOrderGroupMap.get('consolidated').containsKey(o.SBQQ__Quote__r.JVCO_Payment_Terms__c))
                            {
                                contractTypeToPaymentTermOrderGroupMap.get('consolidated').put(o.SBQQ__Quote__r.JVCO_Payment_Terms__c, orderGroup);
                                orderGroupSet.add(orderGroup);
                            }
                        }
                    }
                    insert new List<JVCO_Order_Group__c>(orderGroupSet);
                    Set<Id> orderGroupIdSet = new Set<Id>();
                    for (Order o : retrievedOrders)
                    {
                        if(o.SBQQ__Quote__r.JVCO_Quote_Id_to_Scheduler__c != null)
                        {
                            o.JVCO_Order_Group__c = contractTypeToPaymentTermOrderGroupMap.get('nonconsolidated').get(o.SBQQ__Quote__r.JVCO_Payment_Terms__c).Id;
                        }else
                        {
                            o.JVCO_Order_Group__c = contractTypeToPaymentTermOrderGroupMap.get('consolidated').get(o.SBQQ__Quote__r.JVCO_Payment_Terms__c).Id;
                        }
                        orderGroupIdSet.add(o.JVCO_Order_Group__c);
                        //filip.bezak@accenture.com - GREEN-27940 - setting new flag
                        o.JVCO_TaggedForBilling__c = true;
                        o.JVCO_Processed_By_BillNow__c = true;
                    }
                    update retrievedOrders; // To reflect linking
                    //Run billnow batch
                    Database.executeBatch(new JVCO_OrderMultipleBillNowLogicBatch(orderGroupIdSet), 1);
                }catch(DmlException e)
                { //jason.e.mactal GREEN-25338
                    if(e.getMessage().contains('unable to obtain exclusive access'))
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.JVCO_OrderBeingBilledError));
                        numOfSelectedOrders = 0;
                        return null;
                    }else
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.JVCO_ErrorWhileProcessingRequest+ e.getMessage()));
                        numOfSelectedOrders = 0;
                        return null;
                    }
                }
            }else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, INVALID_RECORDS_ERR));
                numOfSelectedOrders = 0; // To simulate no selected invoice records
                return null;
            }
        }else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, NO_RECORDS_ERR));
            return null;
        }
        return new PageReference('/' + retrievedOrders.get(0).AccountId);
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Checks if the selected Order records pass the validation check
        Inputs: List<Order>
        Returns: Boolean
        <Date>      <Authors Name>              <Brief Description of Change> 
        26-Mar-2017 ryan.i.r.limlingan          Initial version of function
        17-Jan-2018 jasper.j.figueroa           Added error messages for OrderLineItem, Venue, and Selected Order size - GREEN-28436
        27-Apr-2018 filip.bezak@accenture.com   GREEN-31676 - TC2P - Sales credit notes containing PPL product have PRS populated in the Parent field
    ----------------------------------------------------------------------------------------------- */
    public Boolean isListValid(List<Order> selectedOrders)
    {
        JVCO_FFUtil.stopOrderItemDLRSHandler = true;
        JVCO_FFUtil.stopOrderAfterUpdateHandler = true;
        JVCO_FFUtil.stopOrderBeforeUpdateHandler = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopContractItemDLRSHandler = true;
        JVCO_FFUtil.stopContractHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopContractHandlerAfterUpdate = true;
        JVCO_FFUtil.stopCodaInvoiceHandlerAfterUpdate = true;
        JVCO_FFUtil.stopCodaCreditNoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopQuoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopQuoteHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopOpportunityHandlerBeforeUpdate = true;
        
        Integer defaultProductSize = (Integer)JVCO_General_Settings__c.getOrgDefaults().JVCO_Default_No_of_Products__c;
        Integer orderProductVenueSize = (Integer)JVCO_General_Settings__c.getOrgDefaults().JVCO_Default_Order_Product_No_of_Venues__c;        
        Set<Id> ordId = new Set<Id>();
        Boolean isAnyOfBilledOrdersRelatedToMigratedContract = false;
        Decimal totalAmountOfBilledOrders = 0;
        
        for(Order o : selectedOrders)
        {
            isAnyOfBilledOrdersRelatedToMigratedContract = o.SBQQ__Quote__r.SBQQ__MasterContract__c != null
                && o.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c != null
                ? true
                : isAnyOfBilledOrdersRelatedToMigratedContract;
            totalAmountOfBilledOrders += o.TotalAmount;
            ordId.add(o.Id);
            // GREEN-21909 - joseph.g.barrameda    
            if(o.Status != 'Activated')
            {
                INVALID_RECORDS_ERR = 'Orders need to be activated before billing';
                return FALSE;
            }
            // Invalidate selection if Order Amount is negative, and Credit Reason is left blank
            if(o.TotalAmount < 0 && o.JVCO_Credit_Reason__c == null)
            {
                INVALID_RECORDS_ERR = System.Label.JVCO_OrderInvalidRecords;
                return FALSE;
            }
            // Added New Checker to check if its already invoiced - GREEN-21496 - 15/08/2017 - franz.g.a.dimaapi
            if(o.JVCO_Invoiced__c)
            {
                INVALID_RECORDS_ERR = 'At least 1 Order has been billed';
                return FALSE;
            }
            //filip.bezak@accenture.com - GREEN-27940 - restrict ability to generate two sales invoices when already being processed
            if(o.JVCO_TaggedForBilling__c)
            {
                INVALID_RECORDS_ERR = 'One or more of selected Orders will be processed by Bill Now';
                return false;
            }
            //GREEN-34345 - joseph.g.barrameda - restrict Bill Now when an Account is in In Enforcement or Infringement
            List<PermissionSetAssignment> psaEnfAdvisor = [SELECT Id FROM PermissionSetAssignment WHERE PermissionSet.Name = 'JVCO_Enforcement_Advisors' AND AssigneeId=: UserInfo.getUserId()];
            if((o.Account.JVCO_In_Enforcement__c || o.Account.JVCO_In_Infringement__c) && psaEnfAdvisor.size()==0){
                INVALID_RECORDS_ERR ='Account is In Infringement/In Enforcement. Only Enforcement Team has authority on this Order.';
                return false;
            }
            //GREEN-35336 Throws error if Account is inactive
            if(!o.Account.JVCO_Active__c)
            {
                INVALID_RECORDS_ERR = Label.BillNowErrorIfInactive;
                return false;
            }
        }

        Set<Id> venueIdSet = new Set<Id>();
        Set<Id> quoteLineIdSet = new Set<Id>();
        for(OrderItem oItem : [SELECT Id, OrderId, JVCO_Venue__c,
                                Order.JVCO_Cancelled__c,
                                Order.SBQQ__Quote__r.SBQQ__MasterContract__r.JVCO_Contract_Temp_External_ID__c,
                                SBQQ__QuoteLine__c, blng__HoldBilling__c
                                FROM OrderItem
                                WHERE OrderId IN :ordId])
        {
            venueIdSet.add(oItem.JVCO_Venue__c);
            quoteLineIdSet.add(oItem.SBQQ__QuoteLine__c);
        }

        if(selectedOrders.size() == 1)
        {
            if(venueIdSet.size() > orderProductVenueSize)
            {
                INVALID_RECORDS_ERR = 'You cannot select bill now for an order with over ' + orderProductVenueSize + ' Venues, please wait for the invoice scheduler to create the Sales Invoice';
                return FALSE;
            }
            if(quoteLineIdSet.size() > defaultProductSize)
            {
                INVALID_RECORDS_ERR = 'You cannot select bill now for an order with over ' + defaultProductSize +' Quote Lines, please wait for the invoice scheduler to create the Sales Invoice';
                return FALSE;
            }
        }

        List<c2g__codaUserCompany__c> userCompany = [SELECT Id FROM c2g__codaUserCompany__c WHERE c2g__User__c = :UserInfo.getUserId()];
        if(userCompany.isEmpty())
        {
            INVALID_RECORDS_ERR = 'You need a User Company in order to select the bill now';
            return FALSE;
        }
        
        return TRUE; 
    }

    /* ----------------------------------------------------------------------------------------------
        Author: ryan.i.r.limlingan
        Company: Accenture
        Description: Returns to Account detail page
        Inputs: N/A
        Returns: PageReference
        <Date>      <Authors Name>      <Brief Description of Change> 
        06-Mar-2017 ryan.i.r.limlingan  Initial version of function
    ----------------------------------------------------------------------------------------------- */
    public PageReference returnToAccount()
    {
        PageReference page = new PageReference(stdCtrl.cancel().getUrl());
        page.setRedirect(TRUE);
        return page;
    }
}