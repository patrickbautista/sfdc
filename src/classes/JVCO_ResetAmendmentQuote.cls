/* ----------------------------------------------------------------------------------------------
   Name: JVCO_ResetAmendmentQuote
   Description: To allow for Amendment Quotes to have their Net Price zero’d

   Date         Version     Author               Summary of Changes 
   -----------  -------     -----------------    -----------------------------------------
  05-Jan-2018   0.1      Robert John B. Lacatan  Intial creation
  23-Jan-2018   0.2      Raus Kenneth Ablaza     Added fix of GREEN-28265
  ----------------------------------------------------------------------------------------------- */

public class JVCO_ResetAmendmentQuote {
    public SBQQ__Quote__c quote { get; set;}
    public boolean checkMe {get;set;}
    public String action {get;set;}
    
    
    
    public JVCO_ResetAmendmentQuote() {
       
    }

    public JVCO_ResetAmendmentQuote (ApexPages.StandardController stdController){
        this.quote = (SBQQ__Quote__c)stdController.getRecord();
        
        
    }
    
    public Map<String, JVCO_ResetAmendQuote__c> customSet(){
        Map<String, JVCO_ResetAmendQuote__c> settings = JVCO_ResetAmendQuote__c.getAll();
        
        return settings;
    }
    
    public void checkQuote(){
        system.debug('@@@checkQuote@@@');
        SBQQ__Quote__c nQuote = [SELECT id, SBQQ__Type__c, SBQQ__status__c from SBQQ__Quote__c where id =: this.quote.id];
        system.debug('@@reset'+nQuote.SBQQ__Type__c+'id'+nQuote.id+nQuote);
        if(nQuote.SBQQ__Type__c != 'Amendment'){
            checkMe = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,customSet().get('ErrorMessage').Value__c));
            
        } else{
            checkMe = true;
            action = 'Execute';
        }
        system.debug('@@@action: ' + action);
    }
    
    public List<SelectOption> getItems() {
        
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Execute',customSet().get('Option1').Value__c));
        options.add(new SelectOption('Cancel',customSet().get('Option2').Value__c));
        return options;
    }

   
    
    public PageReference updateQuoteLine(){
        system.debug('@@@updateQuoteLine@@@');
        List<SBQQ__QuoteLine__c> updateql = new List<SBQQ__QuoteLine__c>();        
        List<SBQQ__QuoteLine__c> lqline = [
            SELECT id, SBQQ__PriorQuantity__c, JVCO_PriorTotal__c, SBQQ__Quantity__c, SBQQ__UpgradedSubscription__c, 
            SBQQ__UpgradedSubscription__r.SBQQ__Quantity__c, SBQQ__UpgradedSubscription__r.JVCO_PriorTotal__c, 
            CustomMinMaxCalcType__c, LimitDeltaTotal__c, SBQQ__ProductCode__c, SBQQ__RequiredBy__r.SBQQ__ProductCode__c,
            SBQQ__RequiredBy__r.LimitDeltaTotal__c
            from SBQQ__QuoteLine__c 
            WHERE SBQQ__Quote__c =: this.quote.id
        ];
        
        if(action == 'Execute'){
            for(SBQQ__QuoteLine__c qREc: lqline){
                qREc.SBQQ__PriorQuantity__c = qREc.SBQQ__Quantity__c;
                
                // START    raus.k.b.ablaza     23/01/2017      GREEN-28265    
                if(qRec.CustomMinMaxCalcType__c != null || qRec.CustomMinMaxCalcType__c != ''){
                    this.quote.JVCO_AmendmentReset__c = true;
                    qRec.JVCO_PriorTotal__c = qRec.SBQQ__RequiredBy__r.LimitDeltaTotal__c;
                }
                // END    raus.k.b.ablaza     23/01/2017      GREEN-28265
                
                updateql.add(qREc);
            }
        
        }else{
            for(SBQQ__QuoteLine__c qRec: lqline){
                // START    raus.k.b.ablaza     23/01/2017      GREEN-28265   
                if(qRec.SBQQ__UpgradedSubscription__c != null){
                    qREc.SBQQ__PriorQuantity__c= qRec.SBQQ__UpgradedSubscription__r.SBQQ__Quantity__c;
                    
                     
                    if(qRec.CustomMinMaxCalcType__c != null || qRec.CustomMinMaxCalcType__c != ''){
                        qRec.JVCO_PriorTotal__c = qRec.SBQQ__UpgradedSubscription__r.JVCO_PriorTotal__c;
                    }
                    
                    updateql.add(qREc);
                }
                // END    raus.k.b.ablaza     23/01/2017      GREEN-28265
            }  
        }
        
        if(!updateql.isEmpty()){
            update this.quote;
            update updateql;
        }
        
        PageReference orderPage = new PageReference('/' + quote.id);
        return orderPage;
    }
    
    


}