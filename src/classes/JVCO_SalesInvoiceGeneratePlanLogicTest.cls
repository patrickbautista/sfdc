@isTest
private class JVCO_SalesInvoiceGeneratePlanLogicTest
{
    @testSetup static void setupTestData(){
        
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        dt.JVCO_OrderProductTrigger__c = true;
        dt.JVCO_OrderTrigger__c = true;
        insert dt;

        Test.startTest();
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        insert glaList;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        List<JVCO_TransitionManualReviewNotifSettings__c> settings = new List<JVCO_TransitionManualReviewNotifSettings__c>();
        settings.add(new JVCO_TransitionManualReviewNotifSettings__c(Name = 'Limit', Limit__c = '3000'));
        insert settings;
        
        
        c2g__codaBankAccount__c testBankAccount = new c2g__codaBankAccount__c();
        testBankAccount.c2g__AccountName__c = 'Test Bank Account';
        testBankAccount.c2g__AccountNumber__c = '987654';
        testBankAccount.c2g__BankName__c = 'Barclays Bank - Current';
        testBankAccount.c2g__GeneralLedgerAccount__c = glaList[0].id;
        testBankAccount.Name = 'Test Bank Account 1';
        testBankAccount.c2g__OwnerCompany__c = comp.Id;
        testBankAccount.c2g__ReportingCode__c = 'BARC-CURR-JV';
        testBankAccount.c2g__BankAccountCurrency__c = accCurrency.id;
        insert testBankAccount;

        comp.c2g__BankAccount__c = testBankAccount.id;
        update comp;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Processed_By_BillNow__c = true;
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        Test.stopTest();
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        blng__Invoice__c bInv = JVCO_TestClassHelper.setBInv(licAcc.Id, o.Id);
        insert bInv;
        
    }

    @isTest
    static void createSalesInvoice()
    {
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
                Map<String,String> accountRecordTypes = new Map<String,String>{};
                for(RecordType rt: rtypes)
                {
                  accountRecordTypes.put(rt.Name,rt.Id);
                }

                String RecordType =  accountRecordTypes.get('Licence Account');
                Account testAccount = [select id from Account WHERE RecordTypeId = :RecordType];
        c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c order by CreatedDate DESC limit 1];
                c2g__codaAccountingCurrency__c accCurrency = [select id from c2g__codaAccountingCurrency__c order by CreatedDate DESC limit 1];
                c2g__codaBankAccount__c testBankAccount = [select id from c2g__codaBankAccount__c order by CreatedDate DESC limit 1];
                Group testGroup = [select id from Group order by CreatedDate DESC limit 1];


        //Create Sales Invoice
                c2g__codaInvoice__c testInvoice = new c2g__codaInvoice__c();
                testInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
                testInvoice.c2g__InvoiceDate__c = date.today().addDays(-7);
                testInvoice.c2g__DueDate__c = date.today().addMonths(7);
                testInvoice.c2g__Account__c = testAccount.Id;
                testInvoice.JVCO_Customer_Type__c = 'New Business';
                testInvoice.c2g__OwnerCompany__c = testCompany.id;
                testInvoice.ownerid = testGroup.Id;

                testInvoice.JVCO_Generate_Payment_Schedule__c = false;
                testInvoice.JVCO_Number_of_Payments__c = 5.0 ;
                testInvoice.JVCO_First_Due_Date__c = date.today().addDays(5);
                //testInvoice.Name = 'SIN000244';
                test.startTest();

                insert testInvoice;

                c2g__codaInvoice__c selectedInvoice = new c2g__codaInvoice__c();
                selectedInvoice = [select id from c2g__codaInvoice__c where id = :testInvoice.id order by CreatedDate DESC limit 1];
                system.assertEquals(testInvoice.id, selectedInvoice.id,'Error not equals');

                test.stopTest();

                c2g__codaBankStatement__c bankStatement = new c2g__codaBankStatement__c();
                bankStatement.c2g__BankAccount__c = testBankAccount.id;
                bankStatement.c2g__OwnerCompany__c = testCompany.id;
                bankStatement.c2g__Reference__c = 'test0001';
                bankStatement.c2g__OpeningBalance__c = 10100;
                bankStatement.c2g__StatementDate__c = Date.today();
                insert bankStatement;

                

    }

        @isTest
        static void updateSalesInvoiceAndCreate()
        {
                List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Account' AND isActive=true];
                Map<String,String> accountRecordTypes = new Map<String,String>{};
                for(RecordType rt: rtypes)
                {
                  accountRecordTypes.put(rt.Name,rt.Id);
                }

                String RecordType =  accountRecordTypes.get('Licence Account');
                Account testAccount = [select id from Account WHERE RecordTypeId = :RecordType];
                c2g__codaCompany__c testCompany = [select id from c2g__codaCompany__c order by CreatedDate DESC limit 1];
                c2g__codaAccountingCurrency__c accCurrency = [select id from c2g__codaAccountingCurrency__c order by CreatedDate DESC limit 1];
                c2g__codaBankAccount__c testBankAccount = [select id from c2g__codaBankAccount__c order by CreatedDate DESC limit 1];
                Group testGroup = [select id from Group order by CreatedDate DESC limit 1];


                //Create Sales Invoice
                c2g__codaInvoice__c testInvoice = new c2g__codaInvoice__c();
                testInvoice.c2g__InvoiceCurrency__c = accCurrency.Id;
                testInvoice.c2g__InvoiceDate__c = date.today().addDays(-7);
                testInvoice.c2g__DueDate__c = date.today().addMonths(7);
                testInvoice.c2g__Account__c = testAccount.Id;
                testInvoice.JVCO_Customer_Type__c = 'New Business';
                testInvoice.c2g__OwnerCompany__c = testCompany.id;
                
                //testInvoice.ownerid = testGroup.Id;

                testInvoice.JVCO_Generate_Payment_Schedule__c = false;
                testInvoice.JVCO_Number_of_Payments__c = 5.0 ;
                testInvoice.JVCO_First_Due_Date__c = date.today().addDays(5);
                //testInvoice.Name = 'SIN000244';
                insert testInvoice;

                //system.assertEquals(null, testInvoice.JVCO_Generate_Payment_Schedule__c);
                //system.assertEquals(null, testInvoice.JVCO_Number_of_Payments__c);
                
                
                Test.startTest();

                testInvoice.JVCO_Generate_Payment_Schedule__c = true;
                testInvoice.JVCO_Number_of_Payments__c = 6.0 ;
                testInvoice.JVCO_First_Due_Date__c = date.today().addDays(30); 
                testInvoice.JVCO_Payment_Method__c = 'Cheque';
                JVCO_FFUtil.stopCodaInvoiceHandlerAfterUpdate = false;
                update testInvoice;

                //system.assertEquals(null, testInvoice);

                //list<c2g__codaInvoice__c> testListSalesInvoice = new list<c2g__codaInvoice__c>();
                //testListSalesInvoice.add(testInvoice);
                //JVCO_SalesInvoiceGeneratePlanLogic.generateInstalmentPlans(testListSalesInvoice);

                list<c2g__codaInvoiceInstallmentLineItem__c> listSelectedInvoiceLines = new list<c2g__codaInvoiceInstallmentLineItem__c>();
                //listSelectedInvoiceLines = [select id from c2g__codaInvoiceInstallmentLineItem__c where c2g__codaInvoiceInstallmentLineItem__c.c2g__Invoice__c = :testInvoice.id];
                //system.assertEquals(testInvoice.JVCO_Number_of_Payments__c, listSelectedInvoiceLines.size(), 'Error in Number of Lines Generated');

                Test.stopTest();

                c2g__codaBankStatement__c bankStatement = new c2g__codaBankStatement__c();
                bankStatement.c2g__BankAccount__c = testBankAccount.id;
                bankStatement.c2g__OwnerCompany__c = testCompany.id;
                bankStatement.c2g__Reference__c = 'test0001';
                bankStatement.c2g__OpeningBalance__c = 10100;
                bankStatement.c2g__StatementDate__c = Date.today();
                insert bankStatement;

                
        }
}