@isTest
private class JVCO_OrderTriggerHandlerTest{

    @testSetup
    private static void testSetup()
    {
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }

        JVCO_FFUtil.stopBlngInvoiceDLRSHandler = true;
        JVCO_FFUtil.stopBlngInvoiceHandlerAfterUpdate = true;
        JVCO_DisableTriggers__c dt = new JVCO_DisableTriggers__c();
        dt.JVCO_AccountTrigger__c = true;
        dt.JVCO_ContractTrigger__c = true;
        dt.JVCO_DocumentQueueTrigger__c = true;
        dt.JVCO_PayonomyPaymentAgreementTrigger__c = true;
        dt.JVCO_PayonomyPaymentTrigger__c = true;
        dt.JVCO_SalesInvoiceTrigger__c = true;
        dt.JVCO_codaTransactionLineItemTrigger__c = true;
        dt.JVCO_codaTransactionTrigger__c = true;
        //dt.JVCO_OrderProductTrigger__c = true;
        //dt.JVCO_OrderTrigger__c = true;
        insert dt;

        Test.startTest();
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;

        List<c2g__codaGeneralLedgerAccount__c> glaList = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        glaList.add(accReceivableGLA);
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        glaList.add(incCtrlGLA);
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        glaList.add(vatGLA);
        insert glaList;

        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;

        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        List<JVCO_TransitionManualReviewNotifSettings__c> settings = new List<JVCO_TransitionManualReviewNotifSettings__c>();
        settings.add(new JVCO_TransitionManualReviewNotifSettings__c(Name = 'Limit', Limit__c = '3000'));
        insert settings;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);

        Product2 p = JVCO_TestClassHelper.setProduct(incCtrlGLA.Id);
        insert p;
        Pricebook2 pb2 = JVCO_TestClassHelper.setPriceBook2();
        insert pb2;
        PriceBookEntry pbe = JVCO_TestClassHelper.setPriceBookEntry(p.Id);
        insert pbe;
        Opportunity opp = JVCO_TestClassHelper.setOpportunity(licAcc.Id);
        insert opp;
        SBQQ__QuoteProcess__c qProc = JVCO_TestClassHelper.setQuoteProcess();
        insert qProc;
        SBQQ__Quote__c q = JVCO_TestClassHelper.setQuote(licAcc.Id, opp.Id);
        insert q;
        SBQQ__QuoteLine__c ql = JVCO_TestClassHelper.setQuoteLine(q.Id, p.Id);
        insert ql;
        
        Contract contr = JVCO_TestClassHelper.setContract(licAcc.Id, opp.Id, q.Id);
        insert contr;
        JVCO_OrderTriggerHandler.approvalCreateOrder = true;
        Order o = JVCO_TestClassHelper.setOrder(licAcc.Id, q.Id);
        o.JVCO_Processed_By_BillNow__c = true;
        o.JVCO_Distribution_Status__c = 'Unpaid';
        insert o;
        OrderItem oi = JVCO_TestClassHelper.setOrderItem(o.Id, pbe.Id, ql.Id);
        insert oi;
        Test.stopTest();
    }

    @isTest
    static void orderPriceCalcTest(){
        
        Order testOrder = [SELECT Id, OrderNumber, Status FROM Order LIMIT 1];
        OrderItem testOrderItem = [SELECT id, OrderId FROM OrderItem LIMIT 1];

        //system.assertEquals(testOrder.id,o.Id, 'ERROR');

        system.assert(!string.isblank(testOrder.OrderNumber));

        testOrder.SBQQ__PriceCalcStatus__c = 'Completed';
        testOrder.Status = 'Activated';

        test.StartTest();
        update testOrder; 
        test.StopTest();

    }

    @isTest
    static void orderPriceCalcFailedTest(){
        
        Order testOrder = [SELECT Id, OrderNumber, Status, SBQQ__PriceCalcStatus__c FROM Order LIMIT 1];
        OrderItem testOrderItem = [SELECT id, OrderId FROM OrderItem LIMIT 1];

        //system.assertEquals(testOrder.id,o.Id, 'ERROR');

        test.StartTest();
        testOrder.SBQQ__PriceCalcStatus__c = 'Failed';
        update testOrder; 
        test.StopTest();
        Order testOrder2 = [SELECT Id, OrderNumber, Status, SBQQ__PriceCalcStatus__c FROM Order LIMIT 1];
        system.assertEquals('Completed', testOrder2.SBQQ__PriceCalcStatus__c);
    }

    @isTest
    static void orderCancelledTest(){
        
        Order testOrder = [SELECT Id, OrderNumber, Status FROM Order LIMIT 1];
        OrderItem testOrderItem = [SELECT id, OrderId FROM OrderItem LIMIT 1];

        //system.assertEquals(testOrder.id,o.Id, 'ERROR');
        test.StartTest();
        testOrder.JVCO_Cancelled__c = true;
        update testOrder; 
        test.StopTest();
        system.assert(!string.isblank(testOrder.OrderNumber));
    }

    @isTest
    static void orderNegativeTest(){

        //id licAcc = acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        product2 productTest = [select id from product2 limit 1];
        SBQQ__Quote__c quoteTest = [select id from SBQQ__Quote__c limit 1];

        PriceBookEntry pbeTest = [select id from PriceBookEntry limit 1];
        

        SBQQ__QuoteLine__c qlTest = JVCO_TestClassHelper.setQuoteLine(quoteTest.Id, productTest.Id);
        qlTest.SBQQ__Quantity__c = -100;
        insert qlTest;

        
        //OrderItem testOrderItem = [SELECT id, OrderId, Quantity, UnitPrice, SBQQ__TotalAmount__c FROM OrderItem LIMIT 1];
        

        //system.assertEquals(testOrder.id,o.Id, 'ERROR');
        test.StartTest();
        Order testOrder = [SELECT Id, OrderNumber, Status, JVCO_Credit_Reason__c FROM Order LIMIT 1];
        OrderItem oiTest = JVCO_TestClassHelper.setOrderItem(testOrder.Id, pbeTest.Id, qlTest.Id);
        oiTest.Quantity = -100;
        oiTest.UnitPrice = 50;
        insert oiTest;
        testOrder .JVCO_Credit_Reason__c = '';
        update testOrder ; 
        test.StopTest();

        Order testOrder2 = [SELECT Id, OrderNumber, Status, JVCO_Credit_Reason__c FROM Order LIMIT 1];
        //system.assert(testOrder2.JVCO_Credit_Reason__c == 'Invoice amendment');
    }
    @isTest
    static void orderPATTest(){

        //id licAcc = acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        product2 productTest = [select id from product2 limit 1];
        SBQQ__Quote__c quoteTest = [select id from SBQQ__Quote__c limit 1];

        PriceBookEntry pbeTest = [select id from PriceBookEntry limit 1];
        

        SBQQ__QuoteLine__c qlTest = JVCO_TestClassHelper.setQuoteLine(quoteTest.Id, productTest.Id);
        qlTest.SBQQ__Quantity__c = -100;
        insert qlTest;

        
        //OrderItem testOrderItem = [SELECT id, OrderId, Quantity, UnitPrice, SBQQ__TotalAmount__c FROM OrderItem LIMIT 1];
        

        //system.assertEquals(testOrder.id,o.Id, 'ERROR');
        test.StartTest();
        Order testOrder = [SELECT Id, OrderNumber, Status, JVCO_Credit_Reason__c FROM Order LIMIT 1];
        testOrder.JVCO_Distribution_Status__c = 'Unmatched';
        OrderItem oiTest = JVCO_TestClassHelper.setOrderItem(testOrder.Id, pbeTest.Id, qlTest.Id);
        oiTest.Quantity = -100;
        oiTest.UnitPrice = 50;
        insert oiTest;
        testOrder.JVCO_Distribution_Status__c = 'Unmatched and Cancelled';
        
        update testOrder ; 
        test.StopTest();

        Order testOrder2 = [SELECT Id, OrderNumber, Status, JVCO_Credit_Reason__c FROM Order LIMIT 1];
        //system.assert(testOrder2.JVCO_Credit_Reason__c == 'Invoice amendment');
    }
}