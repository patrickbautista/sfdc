/* ----------------------------------------------------------------------------------------------
   Name: JVCO_BillNowLogic.cls 
   Description: Business logic class for generating Billing Invoice on selected Order records

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  06-Mar-2017   0.1         eu.rey.t.cadag  Intial creation
  ----------------------------------------------------------------------------------------------- */
public class JVCO_CalculatePrice {
    
    private List<Order> selectedOrders;
    private ApexPages.StandardSetController stdCtrl;
    public Integer numOfSelectedOrders {get; set;}
    
    public JVCO_CalculatePrice(ApexPages.StandardSetController ssc)
    {
        stdCtrl = ssc;
        selectedOrders = new List<Order>();
        selectedOrders = (List<Order>)ssc.getSelected();
        numOfSelectedOrders = selectedOrders.size();
        system.debug('@@@numOfSelectedOrders: ' + numOfSelectedOrders);
    }
    
    public PageReference init()
    {
        List<Order> retrievedOrders = new List<Order>();
        if (numOfSelectedOrders > 0)
        {
                               
            for(Order ord: [SELECT Id, AccountId, TotalAmount, JVCO_Credit_Reason__c, SBQQ__PriceCalcStatus__c, 
                               JVCO_Order_Group__c, blng__BillNow__c, JVCO_Invoiced__c, Status
                               FROM Order WHERE Id IN :selectedOrders])
            {
                String OrdStatus = ord.SBQQ__PriceCalcStatus__c;
                system.debug('@@@Status is ' + OrdStatus);
                if(OrdStatus.equalsIgnoreCase('Failed') || OrdStatus.equalsIgnoreCase('Queued'))
                {
                    retrievedOrders.add(ord);
                }
            }
            system.debug('@@@Retrieved Orders: ' + retrievedOrders);
            system.debug('@@@Number of Retrieved Orders: ' + retrievedOrders.size());
            if(retrievedOrders.size() > 0)
            {
                JVCO_OrderTriggerHandler.priceCalculationFailed(retrievedOrders);
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You cannot re-price a completed order'));
                numOfSelectedOrders = 0; // to render the "Back" button
                return null;
            }
            
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Order record was selected. Click Back to return to previous page.'));
            return null;
        }
        return new PageReference('/' + retrievedOrders.get(0).AccountId);
    }
    
    public PageReference returnToAccount()
    {
        PageReference page = new PageReference(stdCtrl.cancel().getUrl());
        page.setRedirect(TRUE);
        return page;
    }
}