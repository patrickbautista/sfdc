/**
* Created by: Ashok.
* Created Date: 05.09.2017
* Description: Test class for JVCO_TransitionManualReviewNotification class 
**/
@isTest
public class JVCO_TransitionManualReviewNot_Test 
{
    //private static Account accObj;
    @testSetup static void setupTestData() 
    {
        /*Id licenceRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Licence Account').getRecordTypeId();
        Id customerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
         
        Account a1 = new Account();
        a1.RecordTypeId = customerRT;
        a1.Name = 'Test Account 31284-C';
        a1.Type = 'Customer';
        insert a1;
        
        Contact c = new Contact();
        c.LastName = 'DM';
        c.AccountId = a1.id;
        c.Email = 'test@email.com';
        insert c;
        
        c2g__codaGeneralLedgerAccount__c testGeneralLedger = JVCO_TestClassObjectBuilder.createGeneralLedger();
        insert testGeneralLedger;
        
        c2g__codaTaxCode__c testTaxCode = JVCO_TestClassObjectBuilder.createTaxCode(testGeneralLedger.Id);
        insert testTaxCode;
        
        Account a2 = new Account();
        a2.RecordTypeId = licenceRT;
        a2.Name = 'Test Account 31285-L';
        a2.JVCO_Customer_Account__c = a1.Id;
        a2.JVCO_Renewal_Scenario__c= 'PRS First';
        a2.c2g__CODAOutputVATCode__c = testTaxCode.id;
        a2.ffps_custRem__Preferred_Communication_Channel__c = 'Email';
        a2.JVCO_Review_Date__c = System.today() + 42;
        a2.JVCO_Customer_Account__c = a1.id;
        a2.JVCO_Billing_Contact__c = c.id;
        a2.JVCO_Licence_Holder__c = c.id;
        a2.JVCO_Review_Contact__c = c.id;
            
        insert a2;
        accObj    = a2;
        
        System.debug([Select JVCO_Send_Email_Manual_Review_Notifi__c,ffps_custRem__Preferred_Communication_Channel__c , JVCO_Renewal_Scenario__c ,JVCO_Customer_Account__r.Type , JVCO_Review_Date__c from account where id =:a2.id   ]); 
        */
        List<JVCO_TransitionManualReviewNotifSettings__c> settings = new List<JVCO_TransitionManualReviewNotifSettings__c>();
        settings.add(new JVCO_TransitionManualReviewNotifSettings__c(Name = 'Limit', Limit__c = '3000', Batch_Size__c = 50));
        insert settings;
        
        JVCO_TestClassHelper.setCashMatchingCS();
        
    }
    /*public static testMethod void testBatch()
    {
        setupTestData();
        Test.startTest();
        JVCO_TransitionManualReviewNotification  batch = new JVCO_TransitionManualReviewNotification();
        DataBase.executeBatch(batch);             
        Test.stopTest();
    }
    public static testMethod void testBatchWithSingleRecord()
    {
        setupTestData();        
        Test.startTest();
        JVCO_TransitionManualReviewNotification  batch = new JVCO_TransitionManualReviewNotification(accObj.id);
        DataBase.executeBatch(batch);             
        Test.stopTest();
    }
    public static testMethod void testBatchWithMultipleRecord()
    {
        Set<ID> accIDset= new Set<ID>();
        setupTestData();        
        accIDset.add(accObj.id);
        Test.startTest();
        JVCO_TransitionManualReviewNotification  batch = new JVCO_TransitionManualReviewNotification(accIDset);
        DataBase.executeBatch(batch);             
        Test.stopTest();
    }
    public static testMethod void testSechedule()
    {
        List<JVCO_TransitionManualReviewNotifSettings__c> settings = new List<JVCO_TransitionManualReviewNotifSettings__c>();
        settings.add(new JVCO_TransitionManualReviewNotifSettings__c(Name = 'Limit', Limit__c = '3000'));
        insert settings;
        
        Test.StartTest();
        JVCO_TransitionManualReviewNotification   sh1 = new JVCO_TransitionManualReviewNotification();      
        String sch = '0 0 23 * * ?';
        system.schedule('Test check', sch, sh1);
        Test.stopTest();
    }*/

    @isTest
    static void testEmptyConstructor()
    {
        JVCO_TransitionManualReviewNotification batch = new JVCO_TransitionManualReviewNotification();
        Database.QueryLocator ql = batch.start(null);
        batch.execute(null, new List<Account>());
        batch.finish(null);
    }

    @isTest
    static void testEmptyConstructorFail()
    {
        JVCO_TransitionManualReviewNotification batch = new JVCO_TransitionManualReviewNotification();
        Database.QueryLocator ql = batch.start(null);
        Account a = new Account();
        a.JVCO_Sent_Manual_Review_Notifi_Email__c = false;
        List<Account> aList = new List<Account>();
        batch.execute(null, aList);
    }

    @isTest
    static void testSingleParameterInExecute()
    {
        JVCO_TransitionManualReviewNotification batch = new JVCO_TransitionManualReviewNotification();
        Database.QueryLocator ql = batch.start(null);
        batch.execute(null);
    }

    @isTest
    static void testConstuctorWithSetOfId()
    {
        JVCO_TransitionManualReviewNotification batch = new JVCO_TransitionManualReviewNotification(new Set<Id>());
        Database.QueryLocator ql = batch.start(null);
        batch.execute(null, new List<Account>());
        batch.finish(null);
    }

    @isTest
    static void testNonEmptyConstructor()
    {
        JVCO_TransitionManualReviewNotification batch = new JVCO_TransitionManualReviewNotification(UserInfo.getUserId());
    }

}