/* ----------------------------------------------------------------------------------------------
    Name: JVCO_RegenerateStuckDocQueBatch
    Description: Batch Class for updating Preferred Communication Channel in Account to Email and 
                 Regenerating Document Queues stuck at Request Submitted to Conga Status due to
                 Invalid or Null Recipient Email Address -- GREEN-32696 --

    Date                Version     Author              Summary of Changes 
    ---------------     -------     -----------------   -----------------------------------------
    18-January-2019     0.1         hazel.jurna.m.limot      Intial creation
    13-February-2020    0.2         rhys.j.c.dela.cruz       GREEN-35370 - Added new field to prevent invalid recipients being pulled in the batch
    26-March-2020       0.3         rhys.j.c.dela.cruz       GREEN-35370 - Adjusted batch to remove query filter and update DQs to respective Cancellation Reason
----------------------------------------------------------------------------------------------- */
global class JVCO_RegenerateStuckDocQueBatch implements Database.Batchable<Sobject>  {
    
    Set<Id> adHocIdSet = new Set<Id>();

    global JVCO_RegenerateStuckDocQueBatch(){}

    global JVCO_RegenerateStuckDocQueBatch(Set<Id> adHocIdSet){

        this.adHocIdSet = adHocIdSet;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {

        String query = 'SELECT Id, RecordTypeId, JVCO_Processed_By_Batch__c, JVCO_Related_Customer_Account__c, JVCO_Transmission__c, JVCO_OwnerEmail__c, JVCO_Additional_Detail_Field1__c, JVCO_Additional_Detail_Field2__c, JVCO_Additional_Detail_Value1__c, JVCO_Additional_Detail_Value2__c, JVCO_Contract__c, JVCO_Email_Template__c, JVCO_Generation_Status__c, JVCO_Inclusions__c, Related_Lead__c, JVCO_Message_Body__c, JVCO_Print_Status__c, JVCO_Query_Ids__c, JVCO_Query_Ids2__c, JVCO_Query_Ids_Ext__c, JVCO_Query_Ids_Ext_New__c, JVCO_Query_Ids_Ext_New2__c, JVCO_Query_Ids_Ext_New3__c, JVCO_Recipient__c, JVCO_Related_Account__r.Id, JVCO_Related_Account__r.ffps_custRem__Preferred_Communication_Channel__c, JVCO_Related_Account__r.JVCO_Set_Pref_Comm_Channel_to_Print__c, JVCO_Related_Invoice__c, JVCO_Related_Campaign__c, JVCO_Related_Case__c, JVCO_Related_Credit_Note__c, JVCO_Related_Dunning__c, JVCO_Related_Dynamic_Statement__c, JVCO_Related_Payment__c, JVCO_Related_Payment_Media_Summary__c, JVCO_Related_Payonomy_Payment__c, JVCO_Related_Payonomy_Payment_Agreement__c, JVCO_Related_Purchase_Order__c, JVCO_Related_Quote__c, JVCO_Related_Reminder__c, JVCO_Related_Remittance__c, JVCO_Related_SalesInvoice__c, Related_Supplier_Account__c, JVCO_Salutation__c, JVCO_Scenario__c, Scenario_Category__c, JVCO_Sent_Date__c, JVCO_Signature__c, JVCO_Conga_Solution_Paramaters__c, JVCO_Email_Subject__c, JVCO_Sub_Scenario__c, JVCO_Template__c, JVCO_Template_Ids__c, JVCO_Template_Ids2__c, JVCO_Template_Ids_Ext__c, JVCO_CancellationReason__c, JVCO_PrintRecipientValid__c FROM JVCO_Document_Queue__c WHERE JVCO_Generation_Status__c = \'Request Submitted To Conga\' AND JVCO_Transmission__c = \'Email\' AND JVCO_Processed_By_Batch__c = FALSE ';

        //If Set != empty, adhoc test, else use default created date filter
        if(!adHocIdSet.isEmpty()){
            query += 'AND Id IN: adHocIdSet ';
        }
        else{
            query += 'AND CreatedDate = YESTERDAY ';
        }
        System.debug(query);

        return Database.getQueryLocator(query);
        
        //AND JVCO_Related_Account__r.ffps_custRem__Preferred_Communication_Channel__c = 'Email'  AND JVCO_PrintRecipientValid__c = true]);
         
    }

    global void execute(Database.BatchableContext bc, List<JVCO_Document_Queue__c> docList) {
        
        List<Account> accountList = new List<Account>();
        Set<Id> updateAccountId = new Set<Id>();
        //List<JVCO_Document_Queue__c> updateDocQueList = new List<JVCO_Document_Queue__c>();
        Map<Id, JVCO_Document_Queue__c> updateDocQueMap = new Map<Id, JVCO_Document_Queue__c>();
        List<JVCO_Document_Queue__c> documentQueueList = new List<JVCO_Document_Queue__c>();
        List<JVCO_Document_Queue__c> cloneList = new List<JVCO_Document_Queue__c>();
        Set<Id> accountId = new Set<Id>();
        Set<Id> accountWithQuoteRecalcId = new Set<Id>();
    	
        if(!docList.isEmpty()){
            for(JVCO_Document_Queue__c doc : docList){

                doc.JVCO_Processed_By_Batch__c = TRUE;

                //If Else condition to separate DQs and update to respective Cancellation Reason
                if(!doc.JVCO_PrintRecipientValid__c){

                    doc.JVCO_Generation_Status__c = 'Cancelled';
                    doc.JVCO_CancellationReason__c = 'Cancelled - Invalid Email & Invalid Address';
                }
                else{
                    
                    documentQueueList.add(doc);
                    doc.JVCO_Generation_Status__c = 'Cancelled';
                    doc.JVCO_CancellationReason__c = 'Cancelled - Invalid Email & Resent as Print';
                }

                accountId.add(doc.JVCO_Related_Account__c);
                //updateDocQueList.add(doc);
                updateDocQueMap.put(doc.Id, doc);
            }
        }
        
        //Updates Related Account of Document Queue to be Cloned
        if(!accountId.isEmpty()){
           for(Account accRec : [SELECT Id, ffps_custRem__Preferred_Communication_Channel__c, JVCO_Set_Pref_Comm_Channel_to_Print__c FROM Account WHERE Id =: accountId]){
            accRec.ffps_custRem__Preferred_Communication_Channel__c = 'Print';
            //Field used in PB: JVCO_Email_Bounced_Note
            accRec.JVCO_Set_Pref_Comm_Channel_to_Print__c = TRUE;
            accountList.add(accRec);
          }    
        }
		
        //Checks if there is a Quote that needs to be recalculated against an Account
        if(!accountId.isEmpty()){
          for(SBQQ__Quote__c quoteRec : [SELECT Id, SBQQ__Account__c FROM SBQQ__Quote__c WHERE JVCO_Recalculated__c = false and SBQQ__Account__c =: accountId AND SBQQ__Opportunity2__r.SBQQ__Contracted__c = false]){
            accountWithQuoteRecalcId.add(quoteRec.SBQQ__Account__c);
          }  
        }
        
        for(JVCO_Document_Queue__c dq : documentQueueList){
             if(!accountWithQuoteRecalcId.contains(dq.JVCO_Related_Account__c)){
                 JVCO_Document_Queue__c clonedDQ = dq.clone(false, false, false, false);
                 if(String.isBlank(dq.JVCO_Salutation__c)){
                 clonedDQ.JVCO_Salutation__c = ' ';
                 } else{
                     clonedDQ.JVCO_Salutation__c = dq.JVCO_Salutation__c;
                 }
                    
                 if(String.isBlank(dq.JVCO_Email_Subject__c)){
                 clonedDQ.JVCO_Email_Subject__c = ' ';    
                 } else{
                     clonedDQ.JVCO_Email_Subject__c = dq.JVCO_Email_Subject__c;
                 }
                     
                 if(String.isBlank(dq.JVCO_Message_Body__c)){
                 clonedDQ.JVCO_Message_Body__c = ' ';
                 } else{
                     clonedDQ.JVCO_Message_Body__c = dq.JVCO_Message_Body__c;    
                 }
                     
                 if(String.isBlank(dq.JVCO_Signature__c)){
                 clonedDQ.JVCO_Signature__c = '';
                 } else{
             clonedDQ.JVCO_Signature__c = dq.JVCO_Signature__c;
                 }
         
                 if(!String.isBlank(dq.JVCO_Sub_Scenario__c)){
                     if(dq.JVCO_Sub_Scenario__c.contains('Usage summary (Excel)')){
               clonedDQ.JVCO_Sub_Scenario__c = dq.JVCO_Sub_Scenario__c.replace('(Excel)','(PDF)');
           } else{
               clonedDQ.JVCO_Sub_Scenario__c = dq.JVCO_Sub_Scenario__c;
           }
                 }
         
                 clonedDQ.JVCO_Transmission__c = 'Letter';
                 clonedDQ.JVCO_OwnerEmail__c = dq.JVCO_OwnerEmail__c;
                 clonedDQ.JVCO_Additional_Detail_Field1__c = dq.JVCO_Additional_Detail_Field1__c;
                 clonedDQ.JVCO_Additional_Detail_Field2__c = dq.JVCO_Additional_Detail_Field2__c;
                 clonedDQ.JVCO_Additional_Detail_Value1__c = dq.JVCO_Additional_Detail_Value1__c;
                 clonedDQ.JVCO_Additional_Detail_Value2__c = dq.JVCO_Additional_Detail_Value2__c;
                 clonedDQ.JVCO_Contract__c = dq.JVCO_Contract__c;
                 clonedDQ.JVCO_Email_Template__c = dq.JVCO_Email_Template__c;
                 clonedDQ.JVCO_Generation_Status__c = 'To be Generated';
                 clonedDQ.JVCO_Inclusions__c = dq.JVCO_Inclusions__c;
                 clonedDQ.Related_Lead__c = dq.Related_Lead__c;
                 clonedDQ.JVCO_Print_Status__c = 'For Printing';
                 clonedDQ.JVCO_Query_Ids__c = dq.JVCO_Query_Ids__c;
                 clonedDQ.JVCO_Query_Ids2__c = dq.JVCO_Query_Ids2__c;
                 clonedDQ.JVCO_Query_Ids_Ext__c = dq.JVCO_Query_Ids_Ext__c;
                 clonedDQ.JVCO_Query_Ids_Ext_New__c = dq.JVCO_Query_Ids_Ext_New__c;
                 clonedDQ.JVCO_Query_Ids_Ext_New2__c = dq.JVCO_Query_Ids_Ext_New2__c;
                 clonedDQ.JVCO_Query_Ids_Ext_New3__c = dq.JVCO_Query_Ids_Ext_New3__c;
                 clonedDQ.JVCO_Recipient__c = dq.JVCO_Recipient__c;
                 clonedDQ.JVCO_Related_Account__c = dq.JVCO_Related_Account__c;
                 clonedDQ.JVCO_Related_Invoice__c = dq.JVCO_Related_Invoice__c;
                 clonedDQ.JVCO_Related_Campaign__c = dq.JVCO_Related_Campaign__c;
                 clonedDQ.JVCO_Related_Case__c = dq.JVCO_Related_Case__c;
                 clonedDQ.JVCO_Related_Credit_Note__c = dq.JVCO_Related_Credit_Note__c;
                 clonedDQ.JVCO_Related_Customer_Account__c = dq.JVCO_Related_Customer_Account__c;
                 clonedDQ.JVCO_Related_Dunning__c = dq.JVCO_Related_Dunning__c;
                 clonedDQ.JVCO_Related_Dynamic_Statement__c = dq.JVCO_Related_Dynamic_Statement__c;
                 clonedDQ.JVCO_Related_Payment__c = dq.JVCO_Related_Payment__c;
                 clonedDQ.JVCO_Related_Payment_Media_Summary__c = dq.JVCO_Related_Payment_Media_Summary__c;
                 clonedDQ.JVCO_Related_Payonomy_Payment__c = dq.JVCO_Related_Payonomy_Payment__c;
                 clonedDQ.JVCO_Related_Payonomy_Payment_Agreement__c = dq.JVCO_Related_Payonomy_Payment_Agreement__c;
                 clonedDQ.JVCO_Related_Purchase_Order__c = dq.JVCO_Related_Purchase_Order__c;
                 clonedDQ.JVCO_Related_Quote__c = dq.JVCO_Related_Quote__c;
                 clonedDQ.JVCO_Related_Reminder__c = dq.JVCO_Related_Reminder__c;
                 clonedDQ.JVCO_Related_Remittance__c = dq.JVCO_Related_Remittance__c;
                 clonedDQ.JVCO_Related_SalesInvoice__c = dq.JVCO_Related_SalesInvoice__c;
                 clonedDQ.Related_Supplier_Account__c = dq.Related_Supplier_Account__c;
                 clonedDQ.JVCO_Scenario__c = dq.JVCO_Scenario__c;
                 clonedDQ.Scenario_Category__c = dq.Scenario_Category__c;
                 clonedDQ.JVCO_Sent_Date__c = dq.JVCO_Sent_Date__c;
                 clonedDQ.JVCO_Conga_Solution_Paramaters__c = dq.JVCO_Conga_Solution_Paramaters__c;
                 clonedDQ.JVCO_Template__c = dq.JVCO_Template__c;
                 clonedDQ.JVCO_Template_Ids__c = dq.JVCO_Template_Ids__c;
                 clonedDQ.JVCO_Template_Ids2__c = dq.JVCO_Template_Ids2__c;
                 clonedDQ.JVCO_Template_Ids_Ext__c = dq.JVCO_Template_Ids_Ext__c;
                 clonedDQ.JVCO_Format__c = 'PDF';
                 clonedDQ.JVCO_Regenerated_by_Batch__c = TRUE;
                 cloneList.add(clonedDQ);
            }
            else{
                
                if(updateDocQueMap.containsKey(dq.Id)){
                    updateDocQueMap.get(dq.Id).JVCO_CancellationReason__c = 'Cancelled - Invalid Email & Quotes Require Recalculation';
                }
            }
        }
 
        //Insert cloned DQs
        if(!cloneList.isEmpty()){
            Database.insert(cloneList);
        }

        //Update Accounts
        if(!accountList.isEmpty()){
            Database.update(accountList);
        }

        //Update Original DQs
        if(!updateDocQueMap.isEmpty()){
            Database.update(updateDocQueMap.values());
        }
  }

    global void finish(Database.BatchableContext bc){
        
    }    
}