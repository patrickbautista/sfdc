@isTest
public class JVCO_UpdateSalesInvoiceQueueableTest{
    @testSetup 
    static void createTestData() 
    {
        JVCO_TestClassHelper.createBillingConfig();
        Group testGroup = JVCO_TestClassHelper.setGroup();
        insert testGroup; 
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            insert JVCO_TestClassHelper.setFFQueueList(testGroup.Id);
            insert JVCO_TestClassHelper.setGroupMember(testGroup.Id);
        }
        
        List<JVCO_BackgroundMatching__c> bgMatchingList = new List<JVCO_BackgroundMatching__c>();
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Trading Currency', JVCO_Default_Value__c = 'GBP'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Receivable Control', JVCO_Default_Value__c = '10040'));
        bgMatchingList.add(new JVCO_BackgroundMatching__c(Name = 'Output Vat Code', JVCO_Default_Value__c = 'GB-O-STD-LIC'));
        insert bgMatchingList;
        
        c2g__codaCompany__c comp = JVCO_TestClassHelper.setCompany(testGroup.Id);
        insert comp;
        c2g__codaAccountingCurrency__c accCurrency = JVCO_TestClassHelper.setAccCurrency(comp.Id, testGroup.Id);
        insert accCurrency;
        
        c2g__codaGeneralLedgerAccount__c accReceivableGLA = JVCO_TestClassHelper.setGLA('ARC', testGroup.Id);
        insert accReceivableGLA;
        c2g__codaGeneralLedgerAccount__c incCtrlGLA = JVCO_TestClassHelper.setGLA('IC', testGroup.Id);
        insert incCtrlGLA;
        c2g__codaGeneralLedgerAccount__c vatGLA = JVCO_TestClassHelper.setGLA('VAT', testGroup.Id);
        insert vatGLA;
        c2g__codaGeneralLedgerAccount__c nonConvGLA = JVCO_TestClassHelper.setGLA('NONCONV', testGroup.Id);
        insert nonConvGLA;
        
        c2g__codaBankAccount__c bAcc = JVCO_TestClassHelper.setBankAcc(comp.Id, accCurrency.Id, nonConvGLA.Id);
        insert bAcc;
        comp.c2g__BankAccount__c = bAcc.id;
        update comp;
        
        c2g__codaTaxCode__c taxCode = JVCO_TestClassHelper.setTaxCode(vatGLA.Id);
        insert taxCode;
        c2g__codaTaxRate__c taxRate = JVCO_TestClassHelper.setTaxRate(taxCode.Id);
        insert taxRate;
        
        c2g__codaDimension1__c dim1 = JVCO_TestClassHelper.setDim1();
        insert dim1;
        insert JVCO_TestClassHelper.setDim2();
        insert JVCO_TestClassHelper.setDim2('PPL');
        insert JVCO_TestClassHelper.setDim3();
        
        Test.startTest();
        Account custAcc = JVCO_TestClassHelper.setCustAcc(accReceivableGLA.Id, dim1.Id);
        insert custAcc;
        Contact c = JVCO_TestClassHelper.setContact(custAcc.id);
        insert c;
        Account licAcc = JVCO_TestClassHelper.setLicAcc(custAcc.Id, taxCode.Id, c.Id,accReceivableGLA.Id);
        insert licAcc;
        
        c2g__codaYear__c yr = JVCO_TestClassHelper.setYear(comp.Id, testGroup.Id);
        insert yr;
        insert JVCO_TestClassHelper.setPeriod(comp.Id, yr.Id);
        insert JVCO_TestClassHelper.setUserCompany(comp.Id);
        
        JVCO_FFUtil.stopCodaCreditNoteHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerAfterUpdate = true;
        JVCO_FFUtil.stopAccountHandlerBeforeUpdate = true;
        JVCO_FFUtil.stopAccountDLRSHandler = true;
        JVCO_FFUtil.stopCashTransferLogic = true;
        JVCO_FFUtil.stopCodaTransasctionLineItemHandlerAfterUpdate = true;
        JVCO_FFUtil.stopCodaTransasctionHandlerAfterUpdate = true;
        
        //Populate Cash Matching Custom Settings
        JVCO_TestClassHelper.setCashMatchingCS();
        JVCO_TestClassHelper.setGeneralSettingsCS();
        
        insert JVCO_TestClassHelper.getSalesInvoice(licAcc.Id);
        insert JVCO_TestClassHelper.getCreditNote(licAcc.Id);
        
        c2g__codaCashEntry__c cEntry = new c2g__codaCashEntry__c();
        cEntry.c2g__PaymentMethod__c = 'Direct Debit';
        cEntry.c2g__BankAccount__c = bAcc.Id;
        cEntry.c2g__CashEntryCurrency__c = accCurrency.Id;
        insert cEntry;
        List<c2g__codaCashEntryLineItem__c> clineList = new List<c2g__codaCashEntryLineItem__c>();
        c2g__codaCashEntryLineItem__c cLine = new c2g__codaCashEntryLineItem__c();
        cLine.c2g__Account__c = licAcc.Id;
        cLine.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cLine.c2g__CashEntryValue__c = 12000;
        cLine.c2g__CashEntry__c = cEntry.Id;
        cLine.c2g__AccountReference__c = [SELECT Name FROM c2g__codaInvoice__c limit 1].Name;
        clineList.add(cLine);
        c2g__codaCashEntryLineItem__c cLine2 = new c2g__codaCashEntryLineItem__c();
        cLine2.c2g__Account__c = licAcc.Id;
        cLine2.c2g__AccountPaymentMethod__c = 'Direct Debit';
        cLine2.c2g__CashEntryValue__c = 12000;
        cLine2.c2g__CashEntry__c = cEntry.Id;
        cLine2.c2g__AccountReference__c = [SELECT Name FROM c2g__codaCreditNote__c limit 1].Name;
        clineList.add(cLine2);
        insert clineList;
        
        c2g.CODAAPICommon_7_0.Context context = new c2g.CODAAPICommon_7_0.Context();
        List<c2g.CODAAPICommon.Reference> referenceList = new List<c2g.CODAAPICommon.Reference>();
        c2g.CODAAPICommon.Reference reference = new c2g.CODAAPICommon.Reference();
        reference.Id = cEntry.Id;
        referenceList.add(reference);
        c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(context, referenceList);
        
        Test.stopTest();
    }
    
    @isTest
    static void testContructorForTli()
    {
        Map<Id, c2g__codaTransactionLineItem__c> tliMap = new  Map<Id, c2g__codaTransactionLineItem__c>([SELECT Id,
                                                                                                         c2g__Account__c,
                                                                                                         c2g__Account__r.JVCO_Customer_Account__c
                                                                                                         FROM c2g__codaTransactionLineItem__c 
                                                                                                         WHERE c2g__MatchingStatus__c = 'Available'
                                                                                                         AND c2g__LineType__c = 'Account'
                                                                                                         ORDER BY Id ASC
                                                                                                         LIMIT 1]);
        System.enqueueJob(new JVCO_UpdateSalesInvoiceQueueable(tliMap));
    }
    
    @isTest
    static void testContructorForAccountIds()
    {
        Map<Id, Id> licAccToCustMap = new Map<Id, Id>();
        Account acct = [SELECT Id, JVCO_Customer_Account__c FROM Account WHERE RecordType.Name = 'Licence Account'];
        licAccToCustMap.put(acct.Id, acct.JVCO_Customer_Account__c);
        System.enqueueJob(new JVCO_UpdateSalesInvoiceQueueable(licAccToCustMap));
    }
    
    @isTest
    static void createErrorLog()
    {
        JVCO_UpdateSalesInvoiceQueueable upsq = new JVCO_UpdateSalesInvoiceQueueable(new Map<Id, c2g__codaTransactionLineItem__c>());
        upsq.createErrorLog('test', 'test', 'test');
    }
}