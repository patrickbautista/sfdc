/* ----------------------------------------------------------------------------------------------
Name: JVCO_UpdateNumberOfInvOnAccount
Description: Batch Class for updating Account Number of Sales Invoices field

Date            Version     Author              Summary of Changes 
-----------     -------     -----------------   -----------------------------------------
31-May-2018     0.1         patrick.t.bautista      Intial creation
----------------------------------------------------------------------------------------------- */
public class JVCO_UpdateNumberOfInvOnAccount  implements Database.Batchable<sObject>{
    private id Acct;
    public JVCO_UpdateNumberOfInvOnAccount(){}
    public JVCO_UpdateNumberOfInvOnAccount(id Acct)
    {
        this.Acct = Acct;
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        //Getting all Accounts without Number of Oustanding Invoices fields populated
        return Database.getQueryLocator([Select id, JVCO_Number_of_Outstanding_Invoices__c, JVCO_Billing_Contact__c, 
                                         RecordType.Name, JVCO_In_Infringement__c, JVCO_In_Enforcement__c
                                         From Account 
                                         where (JVCO_Number_of_Outstanding_Invoices__c = NULL OR 
                                                JVCO_Number_of_Outstanding_Invoices__c = 0)
                                         AND (JVCO_In_Infringement__c = FALSE AND JVCO_In_Enforcement__c = FALSE)
                                         AND RecordType.Name = 'Licence Account' 
                                         AND JVCO_Billing_Contact__c != NULL]);
    }
    public void execute(Database.BatchableContext BC, List<Account > scope)
    {
        Set<Account> updatedAccountSet = new Set<Account>(); 
        // Looping Sales invoice with Account without Number of Oustanding Invoices fields populated
        for(AggregateResult ar : [SELECT c2g__Account__c accId, 
                                  COUNT(Id) totalOutstandingInvoice, 
                                  c2g__Account__r.JVCO_In_Infringement__c, 
                                  c2g__Account__r.JVCO_In_Enforcement__c 
                                  FROM c2g__codaInvoice__c 
                                  WHERE c2g__PaymentStatus__c != 'Paid' 
                                  AND c2g__Account__c != NULL
                                  AND c2g__Account__c IN: scope
                                  AND (c2g__Account__r.JVCO_In_Infringement__c = FALSE AND 
                                       c2g__Account__r.JVCO_In_Enforcement__c = FALSE) 
                                  GROUP BY c2g__Account__c, c2g__Account__r.JVCO_In_Enforcement__c, 
                                  c2g__Account__r.JVCO_In_Infringement__c]) 
        { 
            Account acc = new Account(); 
            acc.Id = (Id)ar.get('accId'); 
            // If there are Oustanding sales invoice present on the account
            if(acc.JVCO_Number_of_Outstanding_Invoices__c <= 0 || acc.JVCO_Number_of_Outstanding_Invoices__c == null) 
            { 
                acc.JVCO_Number_of_Outstanding_Invoices__c = (Integer)ar.get('totalOutstandingInvoice');
            	updatedAccountSet.add(acc); 
            }
            // if no Oustanding Sales Invoice present in Account
            else 
            { 
                acc.JVCO_Number_of_Outstanding_Invoices__c = 0; 
                updatedAccountSet.add(acc);
            } 
        } 
        //Update Account
        if(!updatedAccountSet.isEmpty()) 
        { 
            update new List<Account>(updatedAccountSet); 
        }
    }
    public void finish(Database.BatchableContext BC){}
}