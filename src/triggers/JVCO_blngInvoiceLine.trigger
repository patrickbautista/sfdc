trigger JVCO_blngInvoiceLine  on blng__InvoiceLine__c (Before Insert, After Update) {

    if(Trigger.isBefore)
    {
        if (Trigger.isInsert)
        {
            JVCO_blngInvoiceLineHandler.beforeInsert(Trigger.new);
        }
    }
    if(Trigger.isAfter)
    {
        if (Trigger.isUpdate)
        {
            JVCO_blngInvoiceLineHandler.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}