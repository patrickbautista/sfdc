/* ----------------------------------------------------------------------------------------------
    Name: JVCO_PayonomyPayment
    Description: Trigger for PAYBASE2__Payment__c

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    15-Dec-2016   0.1         ryan.i.r.limlingan  Intial creation
----------------------------------------------------------------------------------------------- */
trigger JVCO_PayonomyPayment on PAYBASE2__Payment__c (before insert, after insert, before update, after update, before delete)
{
    /*if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
        {
            JVCO_PayonomyPaymentHandler.beforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate)
        {
            JVCO_PayonomyPaymentHandler.beforeUpdate(Trigger.new);
        }
        if(Trigger.isDelete)
        {
            JVCO_PayonomyPaymentHandler.beforeDelete(Trigger.old);
        }
    }else
    {
        if(Trigger.isInsert)
        {
            JVCO_PayonomyPaymentHandler.afterInsert(Trigger.new);
        }
        if(Trigger.isUpdate)
        {
            JVCO_PayonomyPaymentHandler.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }*/
}