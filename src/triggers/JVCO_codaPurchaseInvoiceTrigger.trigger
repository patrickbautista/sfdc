trigger JVCO_codaPurchaseInvoiceTrigger on c2g__codaPurchaseInvoice__c (after update) {
    if(Trigger.isAfter)
    { 
        if(Trigger.isUpdate)
        {
            JVCO_codaPurchaseInvoiceTriggerHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
        }
    }
}