trigger JVCO_EventTrigger on JVCO_Event__c (before insert, after insert, before update, after update) {
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            JVCO_EventTriggerHandler.onBeforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate){
            JVCO_EventTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
        }
 
    }
    else{
        if(Trigger.isInsert){
            JVCO_EventTriggerHandler.onAfterInsert(Trigger.new);
        }
        if(Trigger.isUpdate){
            JVCO_EventTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }

    }
    
}