/* ----------------------------------------------------------------------------------------------
   Name: JVCO_SalesInvoice
   Description: Trigger for c2g__codaInvoice__c

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  26-Jan-2017   0.1         ryan.i.r.limlingan  Initial creation
  26-Jan-2017   0.2         mary.ann.a.ruelan   updated before update block. Fix to Green-24809
  14-Dec-2017   0.3         filip.bezak@accenture.com   GREEN-26914 - created branches for before insert and update
  25-Jun-2018   0.4         jasper.j.figueroa   Changed beforeInsert from trigger.newMap to trigger.new
  ----------------------------------------------------------------------------------------------- */
trigger JVCO_SalesInvoice on c2g__codaInvoice__c (before insert, before update, after insert, after update)
{
    if (Trigger.isAfter)
    {
        if (Trigger.isUpdate)
        {
            JVCO_SalesInvoiceHandler.afterUpdate(Trigger.new, Trigger.oldMap);
            System.debug('@@@Sales Invoice: ' + Trigger.new);
      }
    }
    else
    {
        if(Trigger.isInsert)
        {
            JVCO_SalesInvoiceHandler.beforeInsert(Trigger.new);
        }
        else if(Trigger.isUpdate)
        {
            JVCO_SalesInvoiceHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}