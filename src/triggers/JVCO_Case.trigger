/* ----------------------------------------------------------------------------------------------
   Name: JVCO_Case.trigger 
   Description: Trigger for Case

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  12-Oct-2016   0.1         ryan.i.r.limlingan  Intial creation
  08-Nov-2016   0.2         ryan.i.r.limlingan  Changed call to JVCO_CaseHandler to beforeUpdate
  ----------------------------------------------------------------------------------------------- */

trigger JVCO_Case on Case (before insert, after insert, before update, after update,
    before delete, after delete)
{          
    if (Trigger.isBefore)
    {    
        if (Trigger.isInsert)
        {
            JVCO_CaseHandler.beforeInsert(Trigger.new);
        }

        if (Trigger.isUpdate)
        {     
          JVCO_CaseHandler.beforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
        }
    } 

    if (Trigger.isAfter)
    {
        if (Trigger.isInsert)
        {
            JVCO_CaseHandler.afterInsert(Trigger.new);
        }
        
        if(Trigger.isUpdate)
        {
            JVCO_CaseHandler.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}