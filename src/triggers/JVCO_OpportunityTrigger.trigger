trigger JVCO_OpportunityTrigger on Opportunity (before insert, before update, after insert, after update) {
    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            JVCO_OpportunityTriggerHandler.onAfterInsert(Trigger.new);
        }
        if(Trigger.isUpdate  && !JVCO_FFUtil.stopOpportunityHandlerAfterUpdate){
            JVCO_OpportunityTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }

    if(Trigger.isBefore){
        if(Trigger.isInsert){
            JVCO_OpportunityTriggerHandler.onBeforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate && !JVCO_FFUtil.stopOpportunityHandlerBeforeUpdate){
            JVCO_OpportunityTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }

}