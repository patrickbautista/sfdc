trigger JVCO_OrderTrigger on Order (before insert, after insert, before update, after update, after delete) {
    if(Trigger.isBefore) {
        if(Trigger.isInsert)
        {
            JVCO_OrderTriggerHandler.onBeforeInsert(Trigger.new, Trigger.oldMap);
        }
        
        if(Trigger.isUpdate && !JVCO_FFUtil.stopOrderBeforeUpdateHandler)
        {
            JVCO_OrderTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);            
        }
    }
    
    if(Trigger.isAfter) {
        if(Trigger.isInsert)
        {
            JVCO_OrderTriggerHandler.onAfterInsert(Trigger.new);
        }
        
        if(Trigger.isUpdate && !JVCO_FFUtil.stopOrderAfterUpdateHandler)
        {
            JVCO_OrderTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);            
        }
    }
}