trigger JVCO_AffiliationTrigger on JVCO_Affiliation__c (before insert, after insert, before update, after update, before delete) {
    if(Trigger.isBefore) {
        if (Trigger.isInsert) {
            JVCO_AffiliationTriggerHandler.onBeforeInsert(Trigger.new);
        }

        if (Trigger.isUpdate) {
            JVCO_AffiliationTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
        }
    } else {
        if (Trigger.isUpdate) {
            JVCO_AffiliationTriggerHandler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
        }
    }
}