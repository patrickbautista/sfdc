trigger JVCO_QuoteLineTrigger on SBQQ__QuoteLine__c (before insert, after insert, before update, after update, before delete, after delete) {
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            JVCO_QuoteLineTriggerHandler.onBeforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate){
            JVCO_QuoteLineTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
        }
        if(Trigger.isDelete)
        {
            JVCO_QuoteLineTriggerHandler.onBeforeDelete(Trigger.oldMap);
        }
    }
    else{
        if(Trigger.isInsert){
            JVCO_QuoteLineTriggerHandler.onAfterInsert(Trigger.new);
        }
        if(Trigger.isUpdate){
            JVCO_QuoteLineTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
        //onAfterDelete method added - r.p.valencia.iii - 15MAY2017
        if(Trigger.isDelete){
            JVCO_QuoteLineTriggerHandler.onAfterDelete(Trigger.oldMap);
        }
    }
    
}