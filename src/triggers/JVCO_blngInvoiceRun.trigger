/* ----------------------------------------------------------------------------------------------
	Name: JVCO_blngInvoiceRun
	Description: Trigger for blng__InvoiceRun__c object

	Date         Version     Author                Summary of Changes 
	-----------  -------     -----------------     -----------------------------------------
	27-Dec-2017  0.1         franz.g.a.dimaapi     Intial creation
----------------------------------------------------------------------------------------------- */
trigger JVCO_blngInvoiceRun on blng__InvoiceRun__c (after update) {

    if(Trigger.isBefore)
    {

    }else
    {
    	if(Trigger.isUpdate)
    	{
    		JVCO_blngInvoiceRunHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
    	}
    }
}