trigger JVCO_ContentDocumentTrigger on ContentDocument (before update, before delete) {
	
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            JVCO_ContentDocumentTriggerHandler.onBeforeUpdate(Trigger.NewMap);
        }
        else if(Trigger.isDelete){
            JVCO_ContentDocumentTriggerHandler.onBeforeDelete(Trigger.OldMap);
        }
    }
}