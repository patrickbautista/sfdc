/* ----------------------------------------------------------------------------------------------
   Name: JVCO_CampaignMember.trigger 
   Description: Trigger for CampaignMember

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  02-Sep-2016   0.1         ryan.i.r.limlingan  Intial creation
  ----------------------------------------------------------------------------------------------- */

trigger JVCO_CampaignMember on CampaignMember (before insert, after insert, before update, after update,
    before delete, after delete)
{
    if (Trigger.isAfter)
    {    
        if (Trigger.isInsert)
        {
            JVCO_CampaignMemberHandler.afterInsert(Trigger.new);
        }
    }
}