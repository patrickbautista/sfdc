/* ----------------------------------------------------------------------------------------------
   Name: JVCO_Lead.trigger 
   Description: Trigger for Campaign

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  06-Sep-2016   0.1         mark.glenn.b.ayson  Intial creation
  ----------------------------------------------------------------------------------------------- */

trigger JVCO_Lead on Lead (before insert, before update, after insert) 
{
    if(Trigger.isBefore)
    {
        if(Trigger.isUpdate)
        {
            // Added to prevent the trigger from running more than once
            // The solution was referenced in this URL: http://help.salesforce.com/apex/HTViewSolution?id=000005328&
            if(JVCO_AssignFieldAgentsPostCodeUtil.firstRun)
            {
                //JVCO_AssignFieldAgentsPostCodeUtil.firstRun = false;
                //JVCO_LeadHandler.updateLeadOwnerID(Trigger.New, Trigger.oldMap);
                JVCO_LeadHandler.onBeforeUpdate(Trigger.New, Trigger.oldMap);
            }
        }
        else if(Trigger.isInsert)
        {
            // Added to prevent the trigger from running more than once
            // The solution was referenced in this URL: http://help.salesforce.com/apex/HTViewSolution?id=000005328&
            if(JVCO_AssignFieldAgentsPostCodeUtil.firstRun)
            {
                //JVCO_AssignFieldAgentsPostCodeUtil.firstRun = false;
                //JVCO_LeadHandler.updateLeadOwnerID(Trigger.New);
                JVCO_LeadHandler.onBeforeInsert(Trigger.New, Trigger.oldMap);
            }
        }
        JVCO_LeadHandler.updateBudgetCategory(trigger.new);
    }
    else if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
        {
            // Call the method to create the task details after inserting the record
            // JVCO_LeadHandler.createTaskAfterInsert(Trigger.New);
            // JVCO_LeadHandler.updateOwnerToDefaultQueue(Trigger.New);
            JVCO_LeadHandler.onAfterInsert(Trigger.New, Trigger.oldMap);
        }
    }
}