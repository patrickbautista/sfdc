trigger JVCO_codaCashEntryLineItem on c2g__codaCashEntryLineItem__c (before insert) 
{
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
        {
            JVCO_codaCashEntryLineItemHandler.beforeInsert(Trigger.new);
        }
    }
}