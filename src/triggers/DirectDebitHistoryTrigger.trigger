trigger DirectDebitHistoryTrigger on Income_Debit_History__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.DirectDebitHistoryTrigger__c : false;

    if(!skipTrigger){
        if (Trigger.isBefore){
            if(Trigger.isInsert){
                TriggerHandler_DirectDebitHistory.OnBeforeInsert(Trigger.new);
            }

            if(Trigger.isUpdate){
                TriggerHandler_DirectDebitHistory.OnBeforeUpdate(Trigger.newMap, Trigger.oldMap);
            }

            if(Trigger.isDelete){

            }    
        } 
        else if (Trigger.isAfter){
            if(Trigger.isInsert){
                TriggerHandler_DirectDebitHistory.OnAfterInsert(Trigger.newMap);
            }

            if(Trigger.isUpdate){
                TriggerHandler_DirectDebitHistory.OnAfterUpdate(Trigger.newMap, Trigger.oldMap);
            }

            if(Trigger.isDelete){

            }

            if(Trigger.isUndelete){

            } 
        }
    }
}