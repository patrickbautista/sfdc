/* ----------------------------------------------------------------------------------------------
    Name: JVCO_codaBankStatement.cls 
    Description: Trigger for JVCO_codaBankStatement

    Date         Version     Author                Summary of Changes 
    -----------  -------     -----------------     -----------------------------------------
    28-Jul-2017  0.1         kristoffer.d.martin   Intial creation
    19-Dec-2017  0.2         chun.yin.tang         moving isafterupdate to handler
    ----------------------------------------------------------------------------------------------- */
trigger JVCO_codaBankStatement on c2g__codaBankStatement__c (before update, after update) {
    
    if(Trigger.isBefore)
    {
        if(Trigger.isUpdate)
        {
            JVCO_codaBankStatementHandler.isBeforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }else
    {
        if(Trigger.isUpdate)
        {
            JVCO_codaBankStatementHandler.isAfterUpdate(Trigger.new);
        }
    }
}