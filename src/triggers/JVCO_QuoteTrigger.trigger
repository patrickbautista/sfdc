trigger JVCO_QuoteTrigger on SBQQ__Quote__c (before insert, after insert, before update, after update, before delete, after delete) {
    
    if(Trigger.isBefore) {
        if (Trigger.isInsert) {
            JVCO_QuoteTriggerHandler.onBeforeInsert(Trigger.new);
        }

        if (Trigger.isUpdate && !JVCO_FFUtil.stopQuoteHandlerBeforeUpdate) {
            JVCO_QuoteTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
        }

        if (Trigger.isDelete) {
            JVCO_QuoteTriggerHandler.onBeforeDelete(Trigger.oldMap);
        }
    } else {
        if (Trigger.isInsert) {
            JVCO_QuoteTriggerHandler.onAfterInsert(Trigger.new);
        }

        if (Trigger.isUpdate && !JVCO_FFUtil.stopQuoteHandlerAfterUpdate) {
            JVCO_QuoteTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
        if (Trigger.isDelete) {
            JVCO_QuoteTriggerHandler.onAfterDelete(Trigger.oldMap);   
        }
    
    }
    
}