trigger JVCO_ContentDocumentLinkTrigger on ContentDocumentLink (before insert) {
	
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            JVCO_ContentDocumentLinkTriggerHandler.onBeforeInsert(Trigger.New);
        }
    }
}