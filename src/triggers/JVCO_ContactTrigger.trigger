/* ----------------------------------------------------------------------------------------------
   Name: JVCO_ContactTrigger
   Description: Trigger for Contact

   Date         Version     Author               Summary of Changes
   -----------  -------     -----------------    -----------------------------------------
  14-Sept-2017   0.1        Rhys John C. Dela Cruz   Intial creation
  28-Aug-2018    0.2        joseph.g.barrameda       GREEN-32807
  ----------------------------------------------------------------------------------------------- */

trigger JVCO_ContactTrigger on Contact (before delete, before update, after update) {
    if(Trigger.IsBefore){
        if(Trigger.IsDelete){
            JVCO_ContactTriggerHandler.onBeforeDelete(Trigger.old, Trigger.oldMap);
        }        
        if(Trigger.isUpdate){
            JVCO_ContactTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
        }    
    }
    
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            JVCO_ContactTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}