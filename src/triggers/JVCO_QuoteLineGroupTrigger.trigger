/* ----------------------------------------------------------------------------------------------
   Name:        JVCO_QuoteLineGroupTrigger.trigger 
   Description: Trigger for Quote Line Group

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
   03-Apr-2017   0.1        samuel.a.d.oberes   Intial version
   09-Aug-2018   0.2        rhys.j.c.dela.cruz  GREEN-33006 - Added onBeforeDelete call

   ----------------------------------------------------------------------------------------------- */
trigger JVCO_QuoteLineGroupTrigger on SBQQ__QuoteLineGroup__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    if (Trigger.isBefore) {

        if (Trigger.isInsert) {
            JVCO_QuoteLineGroupTriggerHandler.onBeforeInsert(Trigger.new);
        }

        if (Trigger.isUpdate) {
            JVCO_QuoteLineGroupTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
        }

        if (Trigger.isDelete) {
            JVCO_QuoteLineGroupTriggerHandler.onBeforeDelete(Trigger.oldMap);
        }

    } else {

        if (Trigger.isInsert) {
            JVCO_QuoteLineGroupTriggerHandler.onAfterInsert(Trigger.new);
        }

        if (Trigger.isUpdate) {
            JVCO_QuoteLineGroupTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }

    }
}