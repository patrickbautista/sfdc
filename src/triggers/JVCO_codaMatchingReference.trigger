trigger JVCO_codaMatchingReference on c2g__codaMatchingReference__c (after insert, after update) {

    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
        {   
            JVCO_codaMatchingReferenceHandler.afterInsert(Trigger.newMap);
            System.debug('.....Trigger Insert Done');
        }
    }
}