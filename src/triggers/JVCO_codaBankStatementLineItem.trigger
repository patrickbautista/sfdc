/* ----------------------------------------------------------------------------------------------
Name: JVCO_codaBankStatementLineItem.cls 
Description: Trigger for JVCO_codaBankStatementLineItem

Date         Version     Author                Summary of Changes 
-----------  -------     -----------------     -----------------------------------------
07-Dec-2016  0.1         kristoffer.d.martin   Intial creation
19-Dec-2017  0.2         chun.yin.tang         moving onafterinsert to handler
----------------------------------------------------------------------------------------------- */

trigger JVCO_codaBankStatementLineItem on c2g__codaBankStatementLineItem__c (after insert, after update) {
    
    if (Trigger.isAfter)
    {
        if (Trigger.isInsert)
        {
            JVCO_codaBankStatementLineItemHandler.onAfterInsert(Trigger.new);
        }
    }
}