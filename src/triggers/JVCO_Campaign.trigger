/* ----------------------------------------------------------------------------------------------
   Name: JVCO_Campaign.trigger 
   Description: Trigger for Campaign

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  01-Sep-2016   0.1         ryan.i.r.limlingan  Intial creation
  ----------------------------------------------------------------------------------------------- */

trigger JVCO_Campaign on Campaign (before insert, after insert, before update, after update,
	before delete, after delete)
{
    if (Trigger.isAfter)
    {    
        if (Trigger.isUpdate)
        {
            JVCO_CampaignHandler.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}