trigger JVCO_TaskTrigger on Task (before insert, before update, before delete) {

    if(Trigger.isBefore){
        if(Trigger.isInsert){
            JVCO_TaskTriggerHandler.onBeforeInsert(Trigger.New);
        }
        else if(Trigger.isUpdate){
            JVCO_TaskTriggerHandler.onBeforeUpdate(Trigger.New);
        }
        else if(Trigger.isDelete){
            JVCO_TaskTriggerHandler.onBeforeDelete(Trigger.Old);
        }
    }
}