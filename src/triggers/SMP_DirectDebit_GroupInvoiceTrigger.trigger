trigger SMP_DirectDebit_GroupInvoiceTrigger on SMP_DirectDebit_GroupInvoice__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
    after undelete) {
        public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
        public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.DirectDebitTrigger__c : false;
    
        if(!skipTrigger){
            if (Trigger.isBefore){
                if(Trigger.isInsert){
                }
    
                if(Trigger.isUpdate){
                }
    
                if(Trigger.isDelete){
    
                }    
            } 
            else if (Trigger.isAfter)
            {
                if(Trigger.isInsert){
                    TriggerHandler_DirectDebit_GroupInvoice.OnAfterInsert(Trigger.newMap);
                }
    
                if(Trigger.isUpdate){
                }
    
                if(Trigger.isDelete){
    
                }
    
                if(Trigger.isUndelete){
                    
                } 
            }
        }
}