trigger JVCO_NotesTrigger on Note (before insert, before update, before delete) {
	
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            JVCO_NotesTriggerHandler.onBeforeInsert(Trigger.New);
        }
        else if(Trigger.isUpdate){
            JVCO_NotesTriggerHandler.onBeforeUpdate(Trigger.New);
        }
        else if(Trigger.isDelete){
            JVCO_NotesTriggerHandler.onBeforeDelete(Trigger.Old);
        }
    }
}