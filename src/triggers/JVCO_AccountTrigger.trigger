trigger JVCO_AccountTrigger on Account  (before delete, before insert, before update, after delete, after insert, after undelete, after update) {

    if(Trigger.isBefore){
        if(Trigger.isInsert){
            JVCO_AccountTriggerHandler.onBeforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate && !JVCO_FFUtil.stopAccountHandlerBeforeUpdate){
            JVCO_AccountTriggerHandler.onBeforeUpdate(Trigger.newMap, Trigger.oldMap);
        }
    }
    else{
        if(Trigger.isInsert){
            JVCO_AccountTriggerHandler.onAfterInsert(Trigger.new);
        }
        if(Trigger.isUpdate){
            
            if(!JVCO_FFUtil.stopAccountHandlerAfterUpdate || userInfo.getUserType() != 'Standard')
            {
                JVCO_AccountTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap, Trigger.newMap);
            }    
        }
        // GREEN-16509: 01/06/2017 - Kristoffer.d.martin - START
        if(Trigger.isDelete){
            
            JVCO_AccountTriggerHandler.onAfterDelete(Trigger.oldMap);
        }
        // GREEN-16509: 01/06/2017 - Kristoffer.d.martin - END
    }
}