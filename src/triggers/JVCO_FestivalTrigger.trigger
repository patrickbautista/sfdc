/* ----------------------------------------------------------------------------------------------------------
Name:            JVCO_FestivalTrigger.trigger
Description:     festival trigger
Test class:      

Date             Version     Author                            Summary of Changes 
-----------      -------     -----------------           -------------------------------------------
06-Apr-2018      0.1        robert.j.b.lacatan              Initial version of code
---------------------------------------------------------------------------------------------------------- */

trigger JVCO_FestivalTrigger on JVCO_Festival__c (before insert, before update) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            JVCO_FestivalTriggerHandler.onBeforeInsert(Trigger.new);
        }
        
        if(Trigger.isUpdate){
            JVCO_FestivalTriggerHandler.onBeforeUpdate(Trigger.new);
        }
    }
}