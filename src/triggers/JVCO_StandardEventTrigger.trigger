trigger JVCO_StandardEventTrigger on Event (before insert, before update, before delete) {
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            JVCO_StandardEventTriggerHandler.onBeforeInsert(Trigger.New);
        }
        else if(Trigger.isUpdate){
            JVCO_StandardEventTriggerHandler.onBeforeUpdate(Trigger.New);
        }
        else if(Trigger.isDelete){
            JVCO_StandardEventTriggerHandler.onBeforeDelete(Trigger.Old);
        }
    }
}