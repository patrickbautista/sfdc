trigger JVCO_OrderProductTrigger on OrderItem (before insert, after insert, after update) {
    
    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.JVCO_OrderProductTrigger__c : false;
    
    if(Trigger.isBefore) {
        if (Trigger.isInsert) {
            JVCO_OrderProductTrigger.onBeforeInsert(Trigger.new);
        }
        
    } else {
        if (Trigger.isInsert) {
            JVCO_OrderProductTrigger.onAfterInsert(Trigger.new);
        }else if(Trigger.isUpdate)
        {
            JVCO_OrderProductTrigger.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    if(!JVCO_FFUtil.stopOrderItemDLRSHandler)
    {
        if(!skipTrigger){
            dlrs.RollupService.triggerHandler();
        }
    }    
}