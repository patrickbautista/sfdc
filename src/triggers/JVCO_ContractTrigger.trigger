/* ----------------------------------------------------------------------------------------------
   Name: JVCO_ContractTrigger
   Description: Trigger for Contract

   Date         Version     Author               Summary of Changes 
   -----------  -------     -----------------    -----------------------------------------
  14-Dec-2016   0.1      Robert John B. Lacatan   Intial creation
  02-Jan-2016   0.2      Robert John B. Lacatan   Added comments/Desciption
  24-Sep-2016   0.3      franz.g.a.dimaapi        Added before delete implementation
  16-Nov-2017   0.4      filip.bezak              Changes related to Issue #25950
  ----------------------------------------------------------------------------------------------- */
trigger JVCO_ContractTrigger on Contract (before insert, before update, before delete, after insert, after update, after delete)
{
    if(Trigger.isBefore)
    {
        if (Trigger.isInsert)
        {
            JVCO_ContractTriggerHandler.onBeforeInsert(trigger.new);
        }
        if (Trigger.isUpdate && !JVCO_FFUtil.stopContractHandlerBeforeUpdate)
        {
            JVCO_ContractTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
        }
        if (Trigger.isDelete)
        {
            JVCO_ContractTriggerHandler.onBeforeDelete(trigger.oldMap);
        }
    }
    else
    {
        if (Trigger.isInsert)
        {
            JVCO_ContractTriggerHandler.onAfterInsert(trigger.new);            
        }
        if (Trigger.isUpdate && !JVCO_FFUtil.stopContractHandlerAfterUpdate)
        {
            JVCO_ContractTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
        }
        if(Trigger.isDelete)
        {
            JVCO_ContractTriggerHandler.onAfterDelete(trigger.oldMap);
        }
    }
    
    if(!JVCO_FFUtil.stopContractItemDLRSHandler)
    {
        dlrs.RollupService.triggerHandler();
    }
        
}