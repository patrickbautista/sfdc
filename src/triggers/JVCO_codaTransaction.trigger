trigger JVCO_codaTransaction on c2g__codaTransaction__c (before insert, before update, after insert, after update)
{
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert){
            JVCO_codaTransactionHandler.beforeInsert(Trigger.new);
        }
    }
    else
    {
        if(Trigger.isInsert)
        {
            JVCO_codaTransactionHandler.afterInsert(Trigger.newMap);
        }
        else if(Trigger.isUpdate)
        {
            JVCO_codaTransactionHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
        }
    }
}