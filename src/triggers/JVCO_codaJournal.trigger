/* ----------------------------------------------------------------------------------------------
   Name: JVCO_codaJournal
   Description: Trigger for c2g__codaJournal__c

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
   28-Mar-2019  0.1         patrick.t.bautista  Intial creation - GREEN-34440
  ----------------------------------------------------------------------------------------------- */
trigger JVCO_codaJournal on c2g__codaJournal__c (before insert, before update) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
		JVCO_codaJournalHandler.beforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate){
		JVCO_codaJournalHandler.beforeUpdate(Trigger.new);
        }
    }
}