/* --------------------------------------------------------------------------------------------------
    Name:            JVCO_codaCashMatchingHistory
    Description:     
    Test class:      JVCO_codaCashMatchingHistoryLogicTest

    Date            Version     Author                             Summary of Changes 
    -----------     -------     -----------------                  -----------------------------------
    22-Nov-2017     0.1         jason.e.mactal                     Initial Creation
    02-Feb-2018     0.2         filip.bezak@accenture.com          GREEN-28502 - TC2P - Cash file PPL and PRS not showing expected results for part payment by Credit note and Cash entry
  ------------------------------------------------------------------------------------------------ */
trigger JVCO_codaCashMatchingHistory on c2g__codaCashMatchingHistory__c (before insert, after insert, after Update)
{
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
        {   
            JVCO_codaCashMatchingHistoryHandler.beforeInsert(Trigger.new);
        }
    }
    else
    {
        if(Trigger.isInsert)
        {   
            JVCO_codaCashMatchingHistoryHandler.afterInsert(Trigger.newMap);
        }
        if(Trigger.isUpdate)
        {   
            JVCO_codaCashMatchingHistoryHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
        }
    }
}