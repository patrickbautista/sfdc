trigger JVCO_codaTransactionLineItem on c2g__codaTransactionLineItem__c (Before insert, Before update, After insert, After update) {
    
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
        {
            JVCO_codaTransactionLineItemHandler.afterInsert(Trigger.newMap);
            System.debug('After Insert');
        } 


    if(Trigger.isUpdate)
        {
            JVCO_codaTransactionLineItemHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
            System.debug('@@@@@@@@@@@@@@After Update');
        }
    }

    else if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
        {
            JVCO_codaTransactionLineItemHandler.beforeInsert(Trigger.new);
        } 
        

        if(Trigger.isUpdate)
        {
            JVCO_codaTransactionLineItemHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    
}