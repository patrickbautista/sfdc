trigger JVCO_AttachmentTrigger on Attachment (before insert, before update, before delete) {
	
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            JVCO_AttachmentTriggerHandler.onBeforeInsert(Trigger.New);
        }
        else if(Trigger.isUpdate){
            JVCO_AttachmentTriggerHandler.onBeforeUpdate(Trigger.New);
        }
        else if(Trigger.isDelete){
            JVCO_AttachmentTriggerHandler.onBeforeDelete(Trigger.Old);
        }
    }
}