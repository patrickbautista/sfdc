trigger JVCO_SubscriptionTrigger on SBQQ__Subscription__c (before insert, before update, before delete, after insert, after update, after delete) {
    
    if(Trigger.isBefore && !JVCO_FFUtil.stopSubscriptionTrigger) {
        if (Trigger.isInsert) {
            JVCO_SubscriptionTriggerHandler.onBeforeInsert(Trigger.new);
        }else if (Trigger.isUpdate) {
            JVCO_SubscriptionTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
        }else if(Trigger.isDelete){
            JVCO_SubscriptionTriggerHandler.onBeforeDelete(Trigger.oldMap);
        }else{
            //TODO:
        }    
    } 
    
    else {
        if (Trigger.isInsert && !JVCO_FFUtil.stopSubscriptionTrigger) {
            JVCO_SubscriptionTriggerHandler.onAfterInsert(Trigger.new);
        }

        if (Trigger.isUpdate && !JVCO_FFUtil.stopSubscriptionTrigger) {
            JVCO_SubscriptionTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
        if (Trigger.isDelete && !JVCO_FFUtil.stopSubscriptionTrigger) {
            JVCO_SubscriptionTriggerHandler.onAfterDelete(Trigger.oldMap);   
        }
    }
    if(!JVCO_FFUtil.stopSubscriptionDLRSHandler){
        dlrs.RollupService.triggerHandler();
    }
}