/* ----------------------------------------------------------------------------------------------
   Name: JVCO_JournalLineItem_SalesInvoice
   Description: Trigger for c2g__codaJournalLineItem__c

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  07-Sep-2017   0.1         L.B.Singh  Intial creation
  ----------------------------------------------------------------------------------------------- */
trigger JVCO_codaJournalLineItem on c2g__codaJournalLineItem__c (before insert, before update)
{
    if (Trigger.isBefore)
    {
        if (Trigger.isInsert)
        {
            JVCO_codaJournalLineItemHandler.beforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate)
        {
            JVCO_codaJournalLineItemHandler.beforeUpdate(Trigger.new);
        }
    }
}