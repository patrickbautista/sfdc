/* ----------------------------------------------------------------------------------------------
   Name: JVCO_Venue.trigger 
   Description: Trigger for Venue

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
   07-Sep-2016   0.1        mark.glenn.b.ayson  Intial creation
   03-Apr-2017   0.2        samuel.a.d.oberes   added updatePrimaryTariffIfTypeChanged method call
   06-Jun-2017   0.2        kristoffer.d.martin added after delete
   03-Aug-2017   0.3        joseph.g.barrameda  Added updateBudgetCategory method
   ----------------------------------------------------------------------------------------------- */
trigger JVCO_Venue on JVCO_Venue__c (before insert, before update, after insert, after delete) 
{
    
    if(Trigger.isBefore)
    {
        
        //JVCO_VenueHandler.updateBudgetCategory(trigger.new);
        
        if(Trigger.isUpdate)
        {
            // Added to prevent the trigger from running more than once
            // The solution was referenced in this URL: http://help.salesforce.com/apex/HTViewSolution?id=000005328&
            if(JVCO_AssignFieldAgentsPostCodeUtil.firstRun)
            {
                //JVCO_AssignFieldAgentsPostCodeUtil.firstRun = false;
                //JVCO_VenueHandler.updateVenueOwnerID(Trigger.New, Trigger.oldMap);
                //JVCO_VenueHandler.updatePrimaryTariffIfTypeChanged(Trigger.new, Trigger.oldMap);
                JVCO_VenueHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);

            }
        }
        else if(Trigger.isInsert)
        {
            // Added to prevent the trigger from running more than once
            // The solution was referenced in this URL: http://help.salesforce.com/apex/HTViewSolution?id=000005328&
            if(JVCO_AssignFieldAgentsPostCodeUtil.firstRun)
            {
                //JVCO_AssignFieldAgentsPostCodeUtil.firstRun = false;
                //JVCO_VenueHandler.updateVenueOwnerID(Trigger.New);
                //JVCO_VenueHandler.updatePrimaryTariffIfTypeChanged(Trigger.new, null);
                JVCO_VenueHandler.onBeforeInsert(Trigger.new, Trigger.oldMap);
            }
        }
    }
    else if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
        {
            // Call the method to create the task details after inserting the record
            //JVCO_VenueHandler.createTaskAfterInsert(Trigger.New);
            JVCO_VenueHandler.onAfterInsert(Trigger.new, Trigger.oldMap);
        }

        if(Trigger.isDelete)
        {
            JVCO_VenueHandler.onAfterDelete(Trigger.oldMap);
        }
    }
    
}