/* ----------------------------------------------------------------------------------------------
   Name: JVCO_DocumentQueueTrigger
   Description: Trigger Class for Document Queue

   Date         Version     Author                     Summary of Changes
   -----------  -------     --------------------    -----------------------------------------
  01-June-2017   0.1         Hazel Jurna M. Limot       Initial creation
  ----------------------------------------------------------------------------------------------- */


trigger JVCO_DocumentQueueTrigger on JVCO_Document_Queue__c(before insert, after insert, before update, after update){
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            JVCO_DocumentQueueTriggerHandler.onBeforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate){
            JVCO_DocumentQueueTriggerHandler.isUpdate = true;
            JVCO_DocumentQueueTriggerHandler.onBeforeUpdate(Trigger.new);
            JVCO_DocumentQueueTriggerHandler.isUpdate = false;
        }
    }else{
        if(Trigger.isInsert){
            JVCO_DocumentQueueTriggerHandler.onAfterInsert(Trigger.new);
        }
    }
    
}