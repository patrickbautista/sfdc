/* ----------------------------------------------------------------------------------------------
   Name: JVCO_blngInvoice
   Description: Trigger for c2g__codaCreditNote__c

   Date         Version     Author              Summary of Changes 
   -----------  -------     -----------------   -----------------------------------------
  27-Mar-2017   0.1         ryan.i.r.limlingan  Intial creation
  02-Feb-2018   0.2         mary.ann.a.ruelan added beforeUpdate method
  ----------------------------------------------------------------------------------------------- */
trigger JVCO_codaCreditNote on c2g__codaCreditNote__c (before delete, before insert, before update, after delete, after insert, after undelete, after update)
{
    if (Trigger.isBefore){
        if (Trigger.isUpdate){
           JVCO_codaCreditNoteHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }    
    if (Trigger.isAfter)
    {
        if (Trigger.isInsert) {
            JVCO_codaCreditNoteHandler.afterInsert(Trigger.new);   
        }
        if (Trigger.isUpdate)
        {
            JVCO_codaCreditNoteHandler.afterUpdate(Trigger.new, Trigger.oldMap);
        }
        if (Trigger.isDelete) {
            JVCO_codaCreditNoteHandler.afterDelete(Trigger.oldMap);   
        }
    }

    if(!JVCO_FFUtil.stopAccountDLRSHandler)
    {
        dlrs.RollupService.triggerHandler(c2g__codaCreditNote__c.SObjectType);
    }
}