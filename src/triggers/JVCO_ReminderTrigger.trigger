trigger JVCO_ReminderTrigger on ffps_custRem__Reminder__c (Before insert, Before update, After insert, After update) {
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
        {
            JVCO_ReminderTriggerHandler.afterInsert(Trigger.new);
        } 
    }
}