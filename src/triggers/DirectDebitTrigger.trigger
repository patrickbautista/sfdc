trigger DirectDebitTrigger on Income_Direct_Debit__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

    public static final JVCO_DisableTriggers__c disableTriggers = JVCO_DisableTriggers__c.getInstance();
    public static final Boolean skipTrigger = disableTriggers != null ? disableTriggers.DirectDebitTrigger__c : false;

    if(!skipTrigger){
        if (Trigger.isBefore){
            if(Trigger.isInsert){
                TriggerHandler_DirectDebit.OnBeforeInsert(Trigger.new);
            }

            if(Trigger.isUpdate){
                TriggerHandler_DirectDebit.OnBeforeUpdate(Trigger.newMap, Trigger.oldMap);
            }

            if(Trigger.isDelete){

            }    
        } 
        else if (Trigger.isAfter)
        {
            if(Trigger.isInsert){
                TriggerHandler_DirectDebit.OnAfterInsert(Trigger.newMap);
            }

            if(Trigger.isUpdate){
                TriggerHandler_DirectDebit.OnAfterUpdate(Trigger.newMap, Trigger.oldMap);
            }

            if(Trigger.isDelete){

            }

            if(Trigger.isUndelete){
                
            } 
        }
    }
}