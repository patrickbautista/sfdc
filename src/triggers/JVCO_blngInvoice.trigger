/* ----------------------------------------------------------------------------------------------
   Name: JVCO_blngInvoice
   Description: Trigger for blng__Invoice__c

   Date         Version     Author                      Summary of Changes 
   -----------  -------     -----------------           -----------------------------------------
  09-Mar-2017   0.1         ryan.i.r.limlingan          Intial creation
  13-Dec-2017   0.3         ashok.kumar.ramanna         GREEN-27088- Week and Dimension 1 were empty on the True Up Report for Sales Credit Note.
  18-Jan-2018   0.5         filip.bezak@accenture.com   GREEN-28476 - TC2P - Invoice file PPL showing Incorrect period start date and end date  
  ----------------------------------------------------------------------------------------------- */
trigger JVCO_blngInvoice on blng__Invoice__c (before insert, before update, after update)
{
    /*if(Trigger.isBefore)
    {
        if (Trigger.isInsert)
        {
            JVCO_blngInvoiceHandler.beforeInsert(Trigger.new);
        }else if (Trigger.isUpdate)
        {
            JVCO_blngInvoiceHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }else
    {
        if(Trigger.isUpdate)
        {
            JVCO_blngInvoiceHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
        }
    }*/
}