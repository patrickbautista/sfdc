/* ----------------------------------------------------------------------------------------------
    Name: JVCO_PayonomyPayment
    Description: Trigger for PAYREC2__Payment_Agreement__c

    Date         Version     Author              Summary of Changes 
    -----------  -------     -----------------   -----------------------------------------
    29-Dec-2016   0.1        ryan.i.r.limlingan  Intial creation
----------------------------------------------------------------------------------------------- */
trigger JVCO_PayonomyPaymentAgreement on PAYREC2__Payment_Agreement__c (before insert, after insert, before update, after update, before delete)
{
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
        {
            JVCO_PayonomyPaymentAgreementHandler.beforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate)
        {
            JVCO_PayonomyPaymentAgreementHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }
        if(Trigger.isDelete)
        {
            JVCO_PayonomyPaymentAgreementHandler.beforeDelete(Trigger.old);
        }
    }else
    {
        if(Trigger.isInsert)
        {
            JVCO_PayonomyPaymentAgreementHandler.afterInsert(Trigger.new);
        }
        if(Trigger.isUpdate)
        {
            JVCO_PayonomyPaymentAgreementHandler.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}