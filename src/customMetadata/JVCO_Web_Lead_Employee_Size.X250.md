<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>250+</label>
    <protected>false</protected>
    <values>
        <field>High_Value__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Web_Lead_Queue__c</field>
        <value xsi:type="xsd:string">High_Value_Web_Leads</value>
    </values>
</CustomMetadata>
