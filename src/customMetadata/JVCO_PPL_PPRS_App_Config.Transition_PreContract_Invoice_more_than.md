<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Transition PreContract Invoice more than</label>
    <protected>false</protected>
    <values>
        <field>Controlling_Field_Value__c</field>
        <value xsi:type="xsd:string">Transition PreContract Invoice more than 20 Venues</value>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">JVCO_Transmission__c</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Dependent Picklist Setting</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">Excel</value>
    </values>
</CustomMetadata>
