<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Refused to Provide Date Birth to Account</label>
    <protected>false</protected>
    <values>
        <field>JVCO_RecordType__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>JVCO_Source_Field__c</field>
        <value xsi:type="xsd:string">JVCO_Date_of_Birth_Not_Provided__c</value>
    </values>
    <values>
        <field>JVCO_Target_Field__c</field>
        <value xsi:type="xsd:string">JVCO_Date_of_Birth_Not_Provided_Acc__c</value>
    </values>
    <values>
        <field>JVCO_Target_Object__c</field>
        <value xsi:type="xsd:string">Account</value>
    </values>
</CustomMetadata>
